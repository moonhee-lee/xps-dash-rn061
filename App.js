import React, {Component} from 'react';
import {Text, TouchableHighlight, View} from 'react-native';

import * as Progress from 'react-native-progress';

export default class App extends Component<{}> {
  constructor(props) {
    super(props);
    this.state = {progress: 0.2};
  }

  randomProgress = () => {
    const progress = Math.random();
    this.setState({progress});
  };

  render() {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <View style={{marginBottom: 10}}>
          <Progress.Pie
            borderWidth={2}
            borderColor="#62321B"
            unfilledColor="#F5F5F5"
            progress={this.state.progress}
            size={100}
            color="#D6C598"
          />
        </View>
        <TouchableHighlight
          onPress={this.randomProgress}
          style={{padding: 10, backgroundColor: '#CACACA', borderRadius: 5}}>
          <Text style={{fontSize: 18, fontWeight: 'bold'}}>Apple Pie Me!</Text>
        </TouchableHighlight>
      </View>
    );
  }
}
