//
//  XpsJump.m
//  xps_sport
//
//  Created by Allan Mercado on 2018-08-30.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "XpsJump.h"
#import "XpsConvert.h"
#import "jump_detect.h"

@implementation XpsJump

// To export a module named XpsJump
RCT_EXPORT_MODULE();

NSMutableDictionary *jumpDetectArray = [NSMutableDictionary dictionary];
ft::JumpDetect *jumpDetect = new ft::JumpDetect();
ft::JumpDetect *_jumpDetect;

RCT_EXPORT_METHOD(initialize:(int)tagId)
{
  //ft::JumpDetect *jumpDetect;

  if ([jumpDetectArray objectForKey:@(tagId)]) {
    jumpDetect = (ft::JumpDetect*)[jumpDetectArray[@(tagId)] pointerValue];
    jumpDetect->SetPollingPeriod(ft::kMD_Poll25ms);
  } else {
    jumpDetect = new ft::JumpDetect();
    NSValue *ptr = [NSValue valueWithPointer:(void *)jumpDetect];
    [jumpDetectArray setObject:ptr forKey:@(tagId)];
  }
}

RCT_REMAP_METHOD(processData,
                 int:(int)tagId
                 NSArray:(NSArray *)messages
                 int:(int)jumpOrientation
                 findEventsWithResolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject)
{
  //ft::JumpDetect *jumpDetect;
  NSMutableDictionary *returnValue = [NSMutableDictionary dictionary];

  if ([jumpDetectArray objectForKey:@(tagId)]) {
    _jumpDetect = (ft::JumpDetect*)[jumpDetectArray[@(tagId)] pointerValue];
    //jumpDetect->SetPollingPeriod(ft::kMD_Poll25ms);
    if(jumpOrientation==1) {
      _jumpDetect->SetJumpType(ft::kJD_BroadJump);
    } else {
      _jumpDetect->SetJumpType(ft::kJD_VerticalJump);
    }
  } else {
    [returnValue setValue:@(0) forKey:@"algoState"];
    resolve(returnValue);
    return;
  }

  for (NSDictionary* jsonData in messages)
  {

    // cast the incoming data to a data type the algo expects
    ft::TagEstimateData to_process_data = [XpsConvert jsonToTagEstData:jsonData];

    // pass in the data to process
    _jumpDetect->ProcessData(to_process_data);

    // check if the algo detected an event
    //if (jumpDetect->GetDetectState()==ft::kMDActivityEnd) {
    if (_jumpDetect->HasNewEvent()) {

      NSMutableDictionary *motionJumpData = [NSMutableDictionary dictionary];

      double start = _jumpDetect->GetActivityStartTime();
      double startRecording = _jumpDetect->GetRecordingStartTime();
      double end = _jumpDetect->GetActivityEndTime();
      double endRecording = _jumpDetect->GetRecordingEndTime();
      double length = _jumpDetect->GetJumpDistance();
      double height = _jumpDetect->GetJumpHeight();
      double air_time = _jumpDetect->GetAirTime();
      double dip1 = _jumpDetect->GetDip1();
      double dip2 = _jumpDetect->GetDip2();
      double takeoffAngle = _jumpDetect->GetTakeoffAngle();
      double takeoffAcceleration = _jumpDetect->GetTakeoffAcceleration();
      double standDistance = _jumpDetect->GetStand2StandDistance();

      motionJumpData[@"start"] = @((isinf(start) || isnan(start))?0:start);
      motionJumpData[@"startRecording"] = @((isinf(startRecording) || isnan(startRecording))?0:startRecording);
      motionJumpData[@"end"] = @((isinf(end) || isnan(end))?0:end);
      motionJumpData[@"endRecording"] = @((isinf(endRecording) || isnan(endRecording))?0:endRecording);
      motionJumpData[@"length"] = @((isinf(standDistance) || isnan(standDistance))?0:standDistance);//@((isinf(length) || isnan(length))?0:length);
      motionJumpData[@"height"] = @((isinf(height) || isnan(height))?0:height);
      motionJumpData[@"air_time"] = @((isinf(air_time) || isnan(air_time))?0:air_time);
      motionJumpData[@"dip1"] = @((isinf(dip1) || isnan(dip1))?0:dip1);
      motionJumpData[@"dip2"] = @((isinf(dip2) || isnan(dip2))?0:dip2);
      motionJumpData[@"takeoffAngle"] = @((isinf(takeoffAngle) || isnan(takeoffAngle))?0:takeoffAngle);
      motionJumpData[@"takeoffAcceleration"] = @((isinf(takeoffAcceleration) || isnan(takeoffAcceleration))?0:takeoffAcceleration);
      motionJumpData[@"standDistance"] = @((isinf(standDistance) || isnan(standDistance))?0:standDistance);

      // callback with state = 1, and pass back the jump data.
      [returnValue setValue:@(1) forKey:@"algoState"];
      [returnValue setValue:motionJumpData forKey:@"jumpData"];

      resolve(returnValue);

      // call reset
      _jumpDetect->Reset();
      return;

    } else {
      if ( jsonData == [ messages lastObject ] ) {
        // otherwise, callback with success = 0, meaning no data to read.
        [returnValue setValue:@(0) forKey:@"algoState"];
        resolve(returnValue);
        return;
      }
    }
  }
}

RCT_EXPORT_METHOD(changePollingRate:(int) rate)
{
    switch(rate) {
      case 10:
        jumpDetect->SetPollingPeriod(ft::kMD_Poll10ms);
        break;
      case 13:
        jumpDetect->SetPollingPeriod(ft::kMD_Poll13ms);
        break;
      case 20:
        jumpDetect->SetPollingPeriod(ft::kMD_Poll20ms);
        break;
      case 40:
        jumpDetect->SetPollingPeriod(ft::kMD_Poll40ms);
        break;
      default:
        jumpDetect->SetPollingPeriod(ft::kMD_Poll25ms);
        break;
    }
}

RCT_EXPORT_METHOD(reset:(int) tagId)
{
  //ft::JumpDetect *jumpDetect;

  if ([jumpDetectArray objectForKey:@(tagId)]) {
    jumpDetect = (ft::JumpDetect*)[jumpDetectArray[@(tagId)] pointerValue];
    jumpDetect->Reset();
  }
}

RCT_EXPORT_METHOD(destroy:(int)tagId)
{
  //ft::JumpDetect *jumpDetect;

  if ([jumpDetectArray objectForKey:@(tagId)]) {
    jumpDetect = (ft::JumpDetect*)[jumpDetectArray[@(tagId)] pointerValue];
    [jumpDetectArray removeObjectForKey:@(tagId)];
    delete jumpDetect;
  }
}
@end
