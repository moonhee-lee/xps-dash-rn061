#import "IOSWifiManager.h"
#import <NetworkExtension/NetworkExtension.h>
#import <SystemConfiguration/CaptiveNetwork.h>
// If using official settings URL
//#import <UIKit/UIKit.h>

@implementation IOSWifiManager
RCT_EXPORT_MODULE();

NSString* currentSSID;
NSString* secondarySSID;
bool pollingStarted = false;
bool connectingToWifi = false;
int connectWifiCounter = 0;
int disconnectedCounter = 0;


- (NSArray<NSString *> *)supportedEvents
{
  return @[@"WifiStateChange", @"WifiConnectFailed", @"WifiDisconnected", @"NewAppState"];
}

- (void)checkWifiNetwork:(NSTimer *)timer {

  NSString *kSSID = (NSString*) kCNNetworkInfoKeySSID;
  NSArray *ifs = (__bridge_transfer id)CNCopySupportedInterfaces();
  for (NSString *ifnam in ifs) {
    NSDictionary *info = (__bridge_transfer id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
    if (!info && !connectingToWifi) {
      if(![currentSSID isEqualToString:@""]) {
        disconnectedCounter++;
        if (disconnectedCounter > 1) {
          currentSSID = @"";
          disconnectedCounter = 0;
          dispatch_async(dispatch_get_main_queue(), ^{
            [self sendEventWithName:@"WifiDisconnected" body:@{@"currentSSID": @""}];
          });
        }
      }
      return;
    }
    if (![info[kSSID] isEqualToString:currentSSID]) {
      NSLog(@"WifiStateChange \(currentSSID) to \(info[kSSID])");
      currentSSID = info[kSSID];
      connectingToWifi = false;
      connectWifiCounter = 0;
      if (!currentSSID) {
        return;
      }

      dispatch_async(dispatch_get_main_queue(), ^{
        [self sendEventWithName:@"WifiStateChange" body:@{@"currentSSID": currentSSID}];
        return;
      });
    }
    if (connectingToWifi) {
      connectWifiCounter++;
      if (connectWifiCounter > 5) {
        connectingToWifi = false;
        dispatch_async(dispatch_get_main_queue(), ^{
          [self sendEventWithName:@"WifiConnectFailed" body:@{@"reason": @"timeout"}];
          return;
        });
      }
    }
  }
}

- (void)didEnterBackground {
  // if we are going into background and we are connected to the secondary SSID, disconnect!
  if ([currentSSID isEqualToString:secondarySSID]) {
    [[NEHotspotConfigurationManager sharedManager] removeConfigurationForSSID:currentSSID];
  }
  dispatch_async(dispatch_get_main_queue(), ^{
    [self sendEventWithName:@"NewAppState" body:@{@"state": @"didEnterBackground"}];
  });
}

- (void)willEnterForeground {
  dispatch_async(dispatch_get_main_queue(), ^{
    [self sendEventWithName:@"NewAppState" body:@{@"state": @"willEnterForeground"}];
  });
}

RCT_REMAP_METHOD(startWifiPolling,
                 start_wifi_resolver:(RCTPromiseResolveBlock)resolve
                 start_wifi_rejecter:(RCTPromiseRejectBlock)reject) {
  
  if (pollingStarted == false) {
    dispatch_async(dispatch_get_main_queue(), ^{
      // schedule 1 second timer
      [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(checkWifiNetwork:) userInfo:nil repeats:YES];
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willEnterForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
      pollingStarted = true;
    });
  }
}


RCT_EXPORT_METHOD(connectToProtectedSSID:(NSString*)ssid
                  withPassphrase:(NSString*)passphrase
                  isWEP:(BOOL)isWEP
                  connect_wifi_resolver:(RCTPromiseResolveBlock)resolve
                  connect_wifi_rejecter:(RCTPromiseRejectBlock)reject) {
  
  if (@available(iOS 11.0, *)) {
    secondarySSID = ssid;
    NEHotspotConfiguration* configuration = [[NEHotspotConfiguration alloc] initWithSSID:ssid passphrase:passphrase isWEP:isWEP];
    configuration.joinOnce = false;

    [[NEHotspotConfigurationManager sharedManager] applyConfiguration:configuration completionHandler:^(NSError * _Nullable error) {
      connectWifiCounter = 0;
      if (error != nil) {
        reject(@"nehotspot_error", @"Error while configuring WiFi", error);
      } else {
        connectingToWifi = true;
        resolve(nil);
      }
    }];
  } else {
    reject(@"ios_error", @"Not supported in iOS<11.0", nil);
  }
}


RCT_EXPORT_METHOD(disconnectCurrentSSID:(NSString*)ssid
                  disconnect_wifi_resolver:(RCTPromiseResolveBlock)resolve
                  disconnect_wifi_rejecter:(RCTPromiseRejectBlock)reject) {

  [[NEHotspotConfigurationManager sharedManager] removeConfigurationForSSID:currentSSID];
  connectingToWifi = false;
  disconnectedCounter = 0;
  resolve(currentSSID);
}


- (NSDictionary*)constantsToExport {
  // Officially better to use UIApplicationOpenSettingsURLString
  return @{
    @"settingsURL": @"App-Prefs:root=WIFI"
  };
}

@end
