//
//  XpsConvert.m
//  xps_sport
//
//  Created by Allan Mercado on 2018-09-03.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "XpsConvert.h"
#import "messages.h"

@implementation XpsConvert

+ (ft::TagEstimateData) jsonToTagEstData:(NSDictionary *) jsonData {
  ft::TagEstimateData convertedData;

  // convert all the int fields..
  convertedData.tag_id = [[jsonData objectForKey:@"tagId"] intValue];
  convertedData.time_msec = [[jsonData objectForKey:@"time"] intValue];
  convertedData.heart_rate = [[jsonData objectForKey:@"heartRatePerMin"] intValue];
  convertedData.heart_rate_interval = [[jsonData objectForKey:@"tempC"] intValue];
  convertedData.filter_converge_state = [[jsonData objectForKey:@"kfState"] intValue];
  
  // convert all the double fields..
  NSDictionary *position = [jsonData valueForKeyPath:@"posM"];
  if (position) {
    convertedData.position[0] = [[position objectForKey:@"x"] doubleValue];
    convertedData.position[1] = [[position objectForKey:@"y"] doubleValue];
    convertedData.position[2] = [[position objectForKey:@"z"] doubleValue];
  }
  
  NSDictionary *velocity = [jsonData valueForKeyPath:@"velMPS"];
  if (velocity) {
    convertedData.velocity[0] = [[velocity objectForKey:@"x"] doubleValue];
    convertedData.velocity[1] = [[velocity objectForKey:@"y"] doubleValue];
    convertedData.velocity[2] = [[velocity objectForKey:@"z"] doubleValue];
  }
  
  NSDictionary *acceleration = [jsonData valueForKeyPath:@"accMPS2"];
  if (acceleration) {
    convertedData.acceleration[0] = [[acceleration objectForKey:@"x"] doubleValue];
    convertedData.acceleration[1] = [[acceleration objectForKey:@"y"] doubleValue];
    convertedData.acceleration[2] = [[acceleration objectForKey:@"z"] doubleValue];
  }
  
  NSDictionary *imu_acceleration = [jsonData valueForKeyPath:@"accBodyMPS2"];
  if (imu_acceleration) {
    convertedData.imu_acceleration[0] = [[imu_acceleration objectForKey:@"x"] doubleValue];
    convertedData.imu_acceleration[1] = [[imu_acceleration objectForKey:@"y"] doubleValue];
    convertedData.imu_acceleration[2] = [[imu_acceleration objectForKey:@"z"] doubleValue];
  }
  
  NSDictionary *imu_gyro = [jsonData valueForKeyPath:@"gyroBodyDegPS"];
  if (imu_gyro) {
    convertedData.imu_gyro[0] = [[imu_gyro objectForKey:@"x"] doubleValue];
    convertedData.imu_gyro[1] = [[imu_gyro objectForKey:@"y"] doubleValue];
    convertedData.imu_gyro[2] = [[imu_gyro objectForKey:@"z"] doubleValue];
  }
  
  NSDictionary *deviation = [jsonData valueForKeyPath:@"posDevM"];
  if (deviation) {
    convertedData.deviation[0] = [[deviation objectForKey:@"x"] doubleValue];
    convertedData.deviation[1] = [[deviation objectForKey:@"y"] doubleValue];
    convertedData.deviation[2] = [[deviation objectForKey:@"z"] doubleValue];
  }
  
  // string/char-array fields..
  const char * timestampValue = [[jsonData objectForKey:@"t"] UTF8String];
  if (timestampValue) strncpy(convertedData.timestamp, timestampValue, strlen(timestampValue));
  const char * nameValue = [[jsonData objectForKey:@"name"] UTF8String];
  if (nameValue) strncpy(convertedData.name, nameValue, strlen(nameValue));
  
  return convertedData;
  
}

@end
