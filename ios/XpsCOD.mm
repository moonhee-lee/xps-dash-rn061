//
//  XpsCOD.m
//  xps_sport
//
//  Created by Allan Mercado on 2018-09-04.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "XpsCOD.h"
#import "change_of_direction.h"
#import "XpsConvert.h"

@implementation XpsCOD

RCT_EXPORT_MODULE();


NSMutableDictionary *CODDetectArray = [NSMutableDictionary dictionary];
ft::COD *COD = new ft::COD();
ft::COD *_COD;

RCT_EXPORT_METHOD(initialize:(int)tagId) {
  if ([CODDetectArray objectForKey:@(tagId)]) {
    COD = (ft::COD*)[CODDetectArray[@(tagId)] pointerValue];
  } else {
    COD = new ft::COD();
    NSValue *ptr = [NSValue valueWithPointer:(void *)COD];
    [CODDetectArray setObject:ptr forKey:@(tagId)];
  }
  COD->Reset();
}

RCT_REMAP_METHOD(processData, int:(int)tagId NSArray:(NSArray *)messages findEventsWithResolver:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
  NSMutableDictionary *returnValue = [NSMutableDictionary dictionary];

  if ([CODDetectArray objectForKey:@(tagId)]) {
    _COD = (ft::COD*)[CODDetectArray[@(tagId)] pointerValue];
  } else {
    [returnValue setValue:@(0) forKey:@"algoState"];
    resolve(returnValue);
    return;
  }

  for (NSDictionary* jsonData in messages)
  {
    // cast the incoming data to a data type the algo expects
    ft::TagEstimateData to_process_data = [XpsConvert jsonToTagEstData:jsonData];

    _COD->ProcessData(to_process_data);

    if (_COD->GetDetectState() == ft::kMDActivityEnd) {
    //if (COD->HasNewEvent()) {
      // if new event detected return...
      NSMutableDictionary *CODData = [NSMutableDictionary dictionary];

      double start = _COD->GetActivityStartTime();
      double startRecording = _COD->GetRecordingStartTime();
      double end = _COD->GetActivityEndTime();
      double endRecording = _COD->GetRecordingEndTime();
      double recordingStartTime = _COD->GetRecordingStartTime();
      double newHeadingDeg = _COD->GetNewHeadingDeg();
      double newHeadingTime = _COD->GetNewHeadingTime();
      double oldHeadingDeg = _COD->GetOldHeadingDeg();
      double oldHeadingTime = _COD->GetOldHeadingTime();
      double radiusOfCurvature = _COD->GetRadiusOfCurvature();
      double maxOmegaZDegPS = _COD->GetMaxOmegaZDegPS();
      double averagedOmega = _COD->GetAveragedOmega();
      double timeOfChange = _COD->GetTimeOfChange();
      double entryTime = _COD->GetEntryTime();
      double exitTime = _COD->GetExitTime();
      bool wasSalvaged = _COD->WasSalvaged();

      // Main Meta Data
      CODData[@"start"] = @((isinf(start) || isnan(start))?0:start);
      CODData[@"startRecording"] = @((isinf(startRecording) || isnan(startRecording))?0:startRecording);
      CODData[@"end"] = @((isinf(end) || isnan(end))?0:end);
      CODData[@"endRecording"] = @((isinf(endRecording) || isnan(endRecording))?0:endRecording);
      CODData[@"recordingStartTime"] = @((isinf(recordingStartTime) || isnan(recordingStartTime))?0:recordingStartTime);
      CODData[@"newHeadingDeg"] = @((isinf(newHeadingDeg) || isnan(newHeadingDeg))?0:newHeadingDeg);
      CODData[@"newHeadingTime"] = @((isinf(newHeadingTime) || isnan(newHeadingTime))?0:newHeadingTime);
      CODData[@"oldHeadingDeg"] = @((isinf(oldHeadingDeg) || isnan(oldHeadingDeg))?0:oldHeadingDeg);
      CODData[@"oldHeadingTime"] = @((isinf(oldHeadingTime) || isnan(oldHeadingTime))?0:oldHeadingTime);
      CODData[@"radiusOfCurvature"] = @((isinf(radiusOfCurvature) || isnan(radiusOfCurvature))?0:radiusOfCurvature);
      CODData[@"maxOmegaZDegPS"] = @((isinf(maxOmegaZDegPS) || isnan(maxOmegaZDegPS))?0:maxOmegaZDegPS);
      CODData[@"averagedOmega"] = @((isinf(averagedOmega) || isnan(averagedOmega))?0:averagedOmega);
      CODData[@"timeOfChange"] = @((isinf(timeOfChange) || isnan(timeOfChange))?0:timeOfChange);
      CODData[@"entryTime"] = @((isinf(entryTime) || isnan(entryTime))?0:entryTime);
      CODData[@"exitTime"] = @((isinf(exitTime) || isnan(exitTime))?0:exitTime);
      CODData[@"wasSalvaged"] = @(wasSalvaged);

      double apexArray[2];
      _COD->GetEventApexXY(&apexArray[0]);

      NSMutableDictionary *apexXY = [NSMutableDictionary dictionary];
      apexXY[@"x"] = @(apexArray[0]);
      apexXY[@"y"] = @(apexArray[1]);
      CODData[@"apexXY"] = apexXY;

      // Start Data
      double startPosArray[3];
      _COD->GetActivityStartPosition(&startPosArray[0]);

      NSMutableDictionary *startPos = [NSMutableDictionary dictionary];
      startPos[@"x"] = @(startPosArray[0]);
      startPos[@"y"] = @(startPosArray[1]);
      startPos[@"z"] = @(startPosArray[2]);
      CODData[@"startPosAndTime"] = startPos;

      // End Data
      double endPosArray[3];
      _COD->GetActivityEndPosition(&endPosArray[0]);

      NSMutableDictionary *endPos = [NSMutableDictionary dictionary];
      endPos[@"x"] = @(endPosArray[0]);
      endPos[@"y"] = @(endPosArray[1]);
      endPos[@"z"] = @(endPosArray[2]);
      CODData[@"endPosAndTime"] = endPos;

      // callback with state = 2, and pass back the COD data.
      [returnValue setValue:@(2) forKey:@"algoState"];
      [returnValue setValue:CODData forKey:@"CODData"];

      resolve(returnValue);
      // call reset
      _COD->EventReset();
      return;
    } else {
      if (_COD->GetDetectState() == ft::kMDActivityStart) {
        if ( jsonData == [ messages lastObject ] ) {
          // return the state based on whether or not a distance has been set
          // ie. start of motion has been detected.
          double startTime = _COD->GetActivityStartTime();
            [returnValue setValue:@(1) forKey:@"algoState"];
            [returnValue setValue:@((isnan(startTime) || isinf(startTime))?0:startTime) forKey:@"startTime"];
          resolve(returnValue);
          return;
        }
        // otherwise, just continue on
      }
      if ( jsonData == [ messages lastObject ] ) {
        // otherwise, callback with success = 0, meaning no data to read.
        double numEvents = _COD->GetCandidateEventCount();
        double startTime = _COD->GetActivityStartTime();

        [returnValue setValue:@((isnan(numEvents) || isinf(numEvents))?0:numEvents) forKey:@"numEvents"];
        [returnValue setValue:@(0) forKey:@"algoState"];
        [returnValue setValue:@((isnan(startTime) || isinf(startTime))?0:startTime) forKey:@"startTime"];
        resolve(returnValue);
        return;
      }
    }
  }
}

RCT_EXPORT_METHOD(reset:(int) tagId)
{
  if ([CODDetectArray objectForKey:@(tagId)]) {
    COD = (ft::COD*)[CODDetectArray[@(tagId)] pointerValue];
    COD->Reset();
    COD->ZeroEventCount();
  }
}

RCT_EXPORT_METHOD(setMidpointLength:(int)tagId midpointLength:(double)midpointLength)
{
  if ([CODDetectArray objectForKey:@(tagId)]) {
    COD = (ft::COD*)[CODDetectArray[@(tagId)] pointerValue];
    COD->SetMidpointLength(midpointLength);
  }
}

RCT_EXPORT_METHOD(setEndpointLength:(int)tagId endpointLength:(double)endpointLength)
{
  if ([CODDetectArray objectForKey:@(tagId)]) {
    COD = (ft::COD*)[CODDetectArray[@(tagId)] pointerValue];
    COD->SetEndpointsLength(endpointLength);
  }
}

RCT_EXPORT_METHOD(setHeadingThreshold:(int)tagId thresh:(double)thresh)
{
  if ([CODDetectArray objectForKey:@(tagId)]) {
    COD = (ft::COD*)[CODDetectArray[@(tagId)] pointerValue];
    COD->SetDeltaHeadingThresh(thresh);
  }
}

RCT_EXPORT_METHOD(destroy:(int)tagId)
{
  if ([CODDetectArray objectForKey:@(tagId)]) {
    COD = (ft::COD*)[CODDetectArray[@(tagId)] pointerValue];
    [CODDetectArray removeObjectForKey:@(tagId)];
    delete COD;
  }
}
@end

