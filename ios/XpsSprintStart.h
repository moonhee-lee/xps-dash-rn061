//
//  XpsSprintStart.h
//  xps_sport
//
//  Created by Allan Mercado on 2018-09-03.
//  Copyright © 2018 Facebook. All rights reserved.
//

#ifndef XpsSprintStart_h
#define XpsSprintStart_h

#import <React/RCTBridgeModule.h>

@interface XpsSprintStart : NSObject <RCTBridgeModule>
@end

#endif /* XpsSprintStart_h */
