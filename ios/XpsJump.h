//
//  XpsJump.h
//  xps_sport
//
//  Created by Allan Mercado on 2018-08-30.
//  Copyright © 2018 Facebook. All rights reserved.
//

#ifndef XpsJump_h
#define XpsJump_h

#import <React/RCTBridgeModule.h>

@interface XpsJump : NSObject <RCTBridgeModule>
@end

#endif /* XpsJump_h */
