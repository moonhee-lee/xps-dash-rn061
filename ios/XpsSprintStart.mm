//
//  XpsSprintStart.m
//  xps_sport
//
//  Created by Allan Mercado on 2018-09-03.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "XpsSprintStart.h"
#import "XpsConvert.h"
#import "sprint_detect.h"

@implementation XpsSprintStart

RCT_EXPORT_MODULE();

using SprintDetectGetFunction = void (ft::SprintDetect::*)(double*, int);

SprintDetectGetFunction getCrossingTimes;
SprintDetectGetFunction getCrossingVelocities;
SprintDetectGetFunction getAveragedCrossingVelocities;
SprintDetectGetFunction getAveragedCrossingAccelerations;

int distanceMeasureUnit = 0;
double distanceByMeasureUnit(double distance) {
  return distanceMeasureUnit == 1 ? distance * 0.9144 : distance;
}

NSMutableDictionary *sprintDetectArray = [NSMutableDictionary dictionary];
ft::SprintDetect *sprintDetect = new ft::SprintDetect();
ft::SprintDetect *_sprintDetect;

// distanceMeasureUnit: 0 == Meters, 1 == Yards
RCT_EXPORT_METHOD(initialize:(int)tagId sprintDistance:(double)sprintDistance startDistance:(double)startDistance distanceMeasureUnit:(int) units use3DVelocity: (int) use3DVelocity useTracker: (int) useTracker useWalkingThresholds: (int) useWalkingThresholds)
{
  //ft::SprintDetect *sprintDetect;
  distanceMeasureUnit = units == 1 ? 1 : 0;
  
  if (distanceMeasureUnit == 0) {
    getCrossingTimes = &ft::SprintDetect::GetMeterCrossingTimes;
    getCrossingVelocities = &ft::SprintDetect::GetMeterCrossingVelocities;
    getAveragedCrossingVelocities = &ft::SprintDetect::GetAveragedMeterCrossingVelocities;
    getAveragedCrossingAccelerations = &ft::SprintDetect::GetAveragedMeterCrossingAccelerations;
  } else  {
    getCrossingTimes = &ft::SprintDetect::GetYardCrossingTimes;
    getCrossingVelocities = &ft::SprintDetect::GetYardCrossingVelocities;
    getAveragedCrossingVelocities = &ft::SprintDetect::GetAveragedYardCrossingVelocities;
    getAveragedCrossingAccelerations = &ft::SprintDetect::GetAveragedYardCrossingAccelerations;
  }
  
  double distance = distanceByMeasureUnit(sprintDistance);

  if ([sprintDetectArray objectForKey:@(tagId)]) {
    sprintDetect = (ft::SprintDetect*)[sprintDetectArray[@(tagId)] pointerValue];
    //sprintDetect->SetPollingPeriod(ft::kMD_Poll25ms);
  } else {
    sprintDetect = new ft::SprintDetect();
    NSValue *ptr = [NSValue valueWithPointer:(void *)sprintDetect];
    [sprintDetectArray setObject:ptr forKey:@(tagId)];
  }
  sprintDetect->Reset();
  sprintDetect->SetSprintDistance(distance);
  if (startDistance) {
    sprintDetect->SetStartDistance(distance);
  }
  sprintDetect->Set3DVelocity(use3DVelocity == 1 ? true : false);
  sprintDetect->SetTrackerStart(useTracker == 1 ? true : false);
  sprintDetect->SetWalkingThresholds(useWalkingThresholds == 1 ? true : false);
 
}

RCT_REMAP_METHOD(processData,
                 int:(int)tagId
                 NSArray:(NSArray *)messages
                 int:(int)sprintDistance
                 findEventsWithResolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject)
{
  //ft::SprintDetect *sprintDetect;
  NSMutableDictionary *returnValue = [NSMutableDictionary dictionary];

  if ([sprintDetectArray objectForKey:@(tagId)]) {
    _sprintDetect = (ft::SprintDetect*)[sprintDetectArray[@(tagId)] pointerValue];
    //sprintDetect->SetPollingPeriod(ft::kMD_Poll25ms);
  } else {
    [returnValue setValue:@(0) forKey:@"algoState"];
    resolve(returnValue);
    return;
  }

  for (NSDictionary* jsonData in messages)
  {
    // cast the incoming data to a data type the algo expects
    ft::TagEstimateData to_process_data = [XpsConvert jsonToTagEstData:jsonData];

    _sprintDetect->ProcessData(to_process_data);

    if (_sprintDetect->GetDetectState() == ft::kMDActivityEnd) {
      // if we're done, we're done!  return...
      NSMutableDictionary *sprintStartData = [NSMutableDictionary dictionary];

      double start = _sprintDetect->GetActivityStartTime();
      double startRecording = _sprintDetect->GetRecordingStartTime();
      double end = _sprintDetect->GetActivityEndTime();
      double endRecording = _sprintDetect->GetRecordingEndTime();
      double length = _sprintDetect->GetSprintDistance();
      double startDist = _sprintDetect->GetActivityStartTime();
      double recordingStartTime = _sprintDetect->GetRecordingStartTime();
      bool wasSalvaged = _sprintDetect->WasSalvaged();

      // Main Meta Data
      sprintStartData[@"start"] = @((isinf(start) || isnan(start))?0:start);
      sprintStartData[@"startRecording"] = @((isinf(startRecording) || isnan(startRecording))?0:startRecording);
      sprintStartData[@"end"] = @((isinf(end) || isnan(end))?0:end);
      sprintStartData[@"endRecording"] = @((isinf(endRecording)|| isnan(endRecording))?0:endRecording);
      sprintStartData[@"length"] = @((isinf(length) || isnan(length))?0:length);
      sprintStartData[@"startDist"] = @((isinf(startDist) || isnan(startDist))?0:startDist);
      sprintStartData[@"recordingStartTime"] = @((isinf(recordingStartTime) || isnan(recordingStartTime))?0:recordingStartTime);
      sprintStartData[@"wasSalvaged"] = @(wasSalvaged);

      // Start Data
      double startPosArray[3];
      _sprintDetect->GetActivityStartPosition(&startPosArray[0]);

      NSMutableDictionary *startPos = [NSMutableDictionary dictionary];
      startPos[@"x"] = @(startPosArray[0]);
      startPos[@"y"] = @(startPosArray[1]);
      startPos[@"z"] = @(startPosArray[2]);
      sprintStartData[@"startPosAndTime"] = startPos;

      // End Data
      double endPosArray[3];
      _sprintDetect->GetActivityEndPosition(&endPosArray[0]);

      NSMutableDictionary *endPos = [NSMutableDictionary dictionary];
      endPos[@"x"] = @(endPosArray[0]);
      endPos[@"y"] = @(endPosArray[1]);
      endPos[@"z"] = @(endPosArray[2]);
      sprintStartData[@"endPosAndTime"] = endPos;

      // Stride Data (in Metres)
      NSMutableDictionary *strideDataMetres = [NSMutableDictionary dictionary];

      double xingTimes[sprintDistance];
      NSMutableArray *splitDataTimes = [NSMutableArray array];
      (_sprintDetect->*getCrossingTimes)(xingTimes,sprintDistance);
      for (int i=0; i<sprintDistance; i++) {
        splitDataTimes[i] = @(xingTimes[i]);
      }
      double xingAveragedVelocities[sprintDistance];
      NSMutableArray *splitDataAveragedVelocities = [NSMutableArray array];
      (_sprintDetect->*getAveragedCrossingVelocities)(xingAveragedVelocities,sprintDistance);
      for (int i=0; i<sprintDistance; i++) {
        splitDataAveragedVelocities[i] = @(xingAveragedVelocities[i]);
      }
      double xingVelocities[sprintDistance];
      NSMutableArray *splitDataVelocities = [NSMutableArray array];
      (_sprintDetect->*getCrossingVelocities)(xingVelocities,sprintDistance);
      for (int i=0; i<sprintDistance; i++) {
        splitDataVelocities[i] = @(xingVelocities[i]);
      }
      double xingAccelerations[sprintDistance];
      NSMutableArray *splitDataAccelerations = [NSMutableArray array];
      (_sprintDetect->*getAveragedCrossingAccelerations)(xingAccelerations,sprintDistance);
      for (int i=0; i<sprintDistance; i++) {
        splitDataAccelerations[i] = @(xingAccelerations[i]);
      }

      double stepDistancesM[sprintDistance];
      NSMutableArray *stepDistancesMetres = [NSMutableArray array];
      _sprintDetect->GetStrideLengths(stepDistancesM,sprintDistance);

      // Retrieve all strides and accumulate stride count
      // until the value of distance encounters the first -1 which signifies end of buffer
      int strideCount = 0;
      int firstStrideIndex = 1; // the 0th entry in the array is the rubbish distance to 2nd hump so use the array from index 1 onwards
      for (int i=firstStrideIndex; i<sprintDistance && stepDistancesM[i] != -1; i++) {
          [stepDistancesMetres addObject: @(stepDistancesM[i])];
          strideCount++;
      }
      
      double stepTimesM[strideCount];
      NSMutableArray *stepTimesMetres = [NSMutableArray array];
      _sprintDetect->GetStepTimes(stepTimesM,sprintDistance);
      for (int i=firstStrideIndex; i<=strideCount; i++) {
        [stepTimesMetres addObject: @(stepTimesM[i])];
      }
      double contactDurationsM[strideCount];
      NSMutableArray *contactDurationsMetres = [NSMutableArray array];
      _sprintDetect->GetContactDurations(contactDurationsM,sprintDistance);
      for (int i=firstStrideIndex; i<=strideCount; i++) {
        [contactDurationsMetres addObject: @(contactDurationsM[i])];
      }
      double stepAccelerationsM[strideCount];
      NSMutableArray *stepAccelerationsMetres = [NSMutableArray array];
      _sprintDetect->GetStepGyroSlopes(stepAccelerationsM,sprintDistance);
      for (int i=firstStrideIndex; i<=strideCount; i++) {
        double step = [@(stepAccelerationsM[i]) doubleValue];
        int side = 0;
        if(step>0) side = 1;
        [stepAccelerationsMetres addObject: @(side)];
      }

      strideDataMetres[@"distances"] = stepDistancesMetres;
      strideDataMetres[@"times"] = stepTimesMetres;
      strideDataMetres[@"durations"] = contactDurationsMetres;
      strideDataMetres[@"steps"] = stepAccelerationsMetres;

      // Crossing Times Data
      NSMutableDictionary *splitData = [NSMutableDictionary dictionary];

      splitData[@"times"] = splitDataTimes;
      splitData[@"velocities"] = splitDataAveragedVelocities;
      splitData[@"raw_velocities"] = splitDataVelocities;
      splitData[@"accelerations"] = splitDataAccelerations;

      // callback with state = 2, and pass back the sprint data.
      [returnValue setValue:@(2) forKey:@"algoState"];
      [returnValue setValue:sprintStartData forKey:@"sprintData"];
      [returnValue setValue:splitData forKey:@"splitData"];
      [returnValue setValue:strideDataMetres forKey:@"strideDataMetres"];

      resolve(returnValue);
      // call reset
      _sprintDetect->Reset();
      return;
    } else {
        //if (sprintDetect->HasNewEvent()) {
        if (_sprintDetect->GetDetectState() == ft::kMDActivityStart) {
          double currentDistance = _sprintDetect->GetDistanceFromStart(to_process_data);
          double startTime = _sprintDetect->GetActivityStartTime();
          // if this is the last message in the array.. we can return.
          if ( jsonData == [ messages lastObject ] ) {
            // return the state based on whether or not a distance has been set
            // ie. start of motion has been detected.
            if (currentDistance != 1e6) {
              [returnValue setValue:@(1) forKey:@"algoState"];
              [returnValue setValue:@((isnan(currentDistance) || isinf(currentDistance))?0:currentDistance) forKey:@"currentDistance"];
              [returnValue setValue:@((isnan(startTime) || isinf(startTime))?0:startTime) forKey:@"startTime"];
            } else {
              [returnValue setValue:@(1) forKey:@"algoState"];
              [returnValue setValue:@((isnan(currentDistance) || isinf(currentDistance))?0:currentDistance) forKey:@"currentDistance"];
              [returnValue setValue:@((isnan(startTime) || isinf(startTime))?0:startTime) forKey:@"startTime"];
            }
            resolve(returnValue);
            return;
          }
          // otherwise, just continue on
        }

      if ( jsonData == [ messages lastObject ] ) {
        // otherwise, callback with success = 0, meaning no data to read.
        /*double currentDistance = _sprintDetect->GetDistanceFromStart(to_process_data);
        [returnValue setValue:@(0) forKey:@"algoState"];
        [returnValue setValue:@((isnan(currentDistance) || isinf(currentDistance))?0:currentDistance) forKey:@"currentDistance"];
        resolve(returnValue);*/
        return;
      }
    }
  }
}

RCT_EXPORT_METHOD(changePollingRate:(int) rate)
{
    switch(rate) {
      case 10:
        sprintDetect->SetPollingPeriod(ft::kMD_Poll10ms);
        break;
      case 13:
        sprintDetect->SetPollingPeriod(ft::kMD_Poll13ms);
        break;
      case 20:
        sprintDetect->SetPollingPeriod(ft::kMD_Poll20ms);
        break;
      case 40:
        sprintDetect->SetPollingPeriod(ft::kMD_Poll40ms);
        break;
      default:
        sprintDetect->SetPollingPeriod(ft::kMD_Poll25ms);
        break;
    }
}

RCT_EXPORT_METHOD(setStartDistance:(double)distance tagId:(int)tagId)
{
  if ([sprintDetectArray objectForKey:@(tagId)]) {
    sprintDetect = (ft::SprintDetect*)[sprintDetectArray[@(tagId)] pointerValue];
    sprintDetect->SetStartDistance(distanceByMeasureUnit(distance));
  }
}

RCT_EXPORT_METHOD(reset:(int)tagId)
{
  if ([sprintDetectArray objectForKey:@(tagId)]) {
    sprintDetect = (ft::SprintDetect*)[sprintDetectArray[@(tagId)] pointerValue];
    sprintDetect->Reset();
  }
}

RCT_EXPORT_METHOD(destroy:(int)tagId)
{
  if ([sprintDetectArray objectForKey:@(tagId)]) {
    sprintDetect = (ft::SprintDetect*)[sprintDetectArray[@(tagId)] pointerValue];
    [sprintDetectArray removeObjectForKey:@(tagId)];
    delete sprintDetect;
  }
}
@end
