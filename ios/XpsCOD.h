//
//  XpsCOD.h
//  xps_sport
//
//  Created by Allan Mercado on 2018-09-04.
//  Copyright © 2018 Facebook. All rights reserved.
//

#ifndef XpsCOD_h
#define XpsCOD_h

#import <React/RCTBridgeModule.h>

@interface XpsCOD : NSObject <RCTBridgeModule>
@end

#endif /* XpsCOD_h */
