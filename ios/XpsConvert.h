//
//  XpsConvert.h
//  xps_sport
//
//  Created by Allan Mercado on 2018-09-03.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "messages.h"

#ifndef XpsConvert_h
#define XpsConvert_h

@interface XpsConvert : NSObject

+ (ft::TagEstimateData) jsonToTagEstData:(NSDictionary *) jsonData;

@end

#endif /* XpsConvert_h */
