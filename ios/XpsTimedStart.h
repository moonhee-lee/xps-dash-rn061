//
//  XpsTimedStart.h
//  xps_sport
//
//  Created by Allan Mercado on 2018-10-29.
//  Copyright © 2018 Facebook. All rights reserved.
//

#ifndef XpsTimedStart_h
#define XpsTimedStart_h

#import <React/RCTBridgeModule.h>

@interface XpsTimedStart : NSObject <RCTBridgeModule>
@end

#endif /* XpsTimedStart_h */
