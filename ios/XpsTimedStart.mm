//
//  XpsTimedStart.m
//  xps_sport
//
//  Created by Allan Mercado on 2018-10-24.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

#import <CoreFoundation/CoreFoundation.h>
#import <sys/socket.h>
#import <netinet/in.h>
#include <arpa/inet.h>

#import "XpsTimedStart.h"

@implementation XpsTimedStart

RCT_EXPORT_MODULE();

AVAudioPlayer *player;
CFSocketRef cfsocketout;
struct sockaddr_in addr;

int _bestInitLatency = 1000;
int _maxOffsetDiff = 0;
int _minOffsetDiff = 0;
int _latency = 0;
int _alpha = 0;
int _beta = 0;
int _intE = 0;
double _avgOffset = 0;
int _counter = 0;
int _syncCounter = 0;
int _latencyThreshold = 0;
NSTimeInterval _initSyncInterval = 0.0;
NSTimeInterval _syncEvery = 0.0;
int _retryInterval = 0;
uint32_t _currMessageId = 0;
bool _runSync = false;
bool _valid = false;

NSString* _ipAddress = @("");
int _port = 5555;

typedef struct __attribute__((packed)) {
  uint32_t message_id;
  uint64_t app_time_milliseconds;
} SyncMessageRequest;

typedef struct __attribute__((packed)) {
  uint32_t message_id;
  uint64_t app_time_milliseconds;
  uint64_t decawave_time_milliseconds;
  double offset;
  bool valid;
} SyncMessageResponse;


/**
 * calculateOffset
 *
 * This is pretty much a straight conversion from the xps-tt app's time sync algorithm.
 * The difference is that it is UDP the offset is calculated based on what the hub tells us the EST Time is
 * rather than what an HTTP request tells us the initial connection time is.
 * 
 */
void calculateOffset(uint64_t sendTime, uint64_t receivedTime, uint64_t ESTTime)
{
  // latency - how long (average) is a one way trip. phone to hub, hub to phone.
  double latency = (receivedTime - sendTime) / 2.0;
  _latency = latency;
  // offset from our current system(phone) time and the current EST/Hub time + the travel time from hub to phone.
  double offset = receivedTime - (ESTTime + latency);

  if (latency > _latencyThreshold) return;

  // compare offsets to get maxOffsetDiff
  if(_avgOffset!=0) {
    double offsetDiff = fabs(offset - _avgOffset);
    if(offsetDiff>_maxOffsetDiff) {
      _maxOffsetDiff = offsetDiff;
    }
    if(offsetDiff<_minOffsetDiff || _minOffsetDiff==0) {
      _minOffsetDiff = offsetDiff;
    }
  }

  if (_counter < _syncCounter) {
    // initial burst sync.
    if (latency < _bestInitLatency) {
      _bestInitLatency = latency;
      _avgOffset = offset;
    }
  } else {
    double e = (offset - _avgOffset);
    _intE += e;
    _avgOffset += (_alpha * e) + (_beta * _intE);
  }

  NSLog( @"%f", _avgOffset );

}


/**
 * sendTimeSyncPacket
 *
 * Send out a packet to the hub with our current time.
 * 
 */
void sendTimeSyncPacket() {
  if(_runSync) {
      SyncMessageRequest packet;
      uint32_t msgId = _currMessageId++;
      uint64_t time = getCurrentTimeMS();

      packet.message_id = CFSwapInt32(msgId);
      packet.app_time_milliseconds = (time);

      CFDataRef addr_data = CFDataCreate(NULL, (const UInt8*)&addr, sizeof(addr));
      CFDataRef msg_data  = CFDataCreate(NULL, (const UInt8*)&packet, sizeof(packet));
      CFSocketSendData(cfsocketout, addr_data, msg_data, 0);
      // setup next sync.
      NSTimeInterval nextSyncTime;
      if (_counter < _syncCounter) {
        nextSyncTime = _initSyncInterval;
      } else {
        nextSyncTime = _syncEvery;
      }

      [NSTimer scheduledTimerWithTimeInterval:nextSyncTime repeats:false block:^(NSTimer * _Nonnull timer) {
        sendTimeSyncPacket();
      }];
    }
  }


/**
 * dataAvailableCallback
 *
 * receive UDP data back from the hub. cast data and swap bytes.
 * then call the calculate offset if the packet is valid.
 * 
 */
void receiveTimeSyncResponse(CFSocketRef s, CFSocketCallBackType type, CFDataRef address, const void *data, void *info)
{
  CFDataRef dataRef = (CFDataRef)data;
  SyncMessageResponse *syncMsg = (SyncMessageResponse *)(CFDataGetBytePtr(dataRef));

  uint64_t appTime = (syncMsg->app_time_milliseconds);
  uint64_t decaTime = (syncMsg->decawave_time_milliseconds);
  
  _counter++;

  _valid = syncMsg->valid;

  if (syncMsg->valid) {
    calculateOffset(appTime, getCurrentTimeMS(), decaTime);
  }
}


/**
 * setupSocket method
 *
 * setup the UDP socket to start syncing our time clock.
 * 
 */
void setupSocket(NSString *ipAddress, int UDPPort)
{
  cfsocketout = CFSocketCreate(kCFAllocatorDefault, AF_INET, SOCK_DGRAM, IPPROTO_UDP, kCFSocketDataCallBack, receiveTimeSyncResponse, NULL);
  
  // setup the addr with the UDP server address and port.
  memset(&addr, 0, sizeof(addr));
  addr.sin_len            = sizeof(addr);
  addr.sin_family         = AF_INET;
  addr.sin_port           = htons(UDPPort);
  addr.sin_addr.s_addr    = inet_addr([ipAddress UTF8String]);

  //
  // set runloop for data reciever
  //
  CFRunLoopSourceRef rls = CFSocketCreateRunLoopSource(kCFAllocatorDefault, cfsocketout, 0);
  CFRunLoopAddSource(CFRunLoopGetCurrent(), rls, kCFRunLoopDefaultMode);
  CFRelease(rls);
}


/**
 * getCurrentTimeMS
 *
 * return the current time in milliseconds.
 * 
 */
uint64_t getCurrentTimeMS()
{
  // https://developer.apple.com/documentation/quartzcore/1395996-cacurrentmediatime?language=objc
  double currentTime = CACurrentMediaTime();
  return (uint64_t) (currentTime * 1000);
}


/**
 * getCurrentESTTimeMS
 *
 * return the estimated "EST" time based on our average offsets.
 */
uint64_t getCurrentESTTimeMS()
{
  return (uint64_t)(getCurrentTimeMS() - _avgOffset);
}


/**
 * startSync method
 *
 * called from the application to begin the time sync process between the app
 * and the hub.
 *
 */
RCT_EXPORT_METHOD(startSync:(NSString *)ipAddress
                  port:(int)port
                  estAlpha:(double)estAlpha
                  estBeta:(double)estBeta
                  syncCounter:(int)syncCounter
                  latencyThreshold:(int)latencyThreshold
                  initSyncInterval:(int)initSyncInterval
                  syncEvery:(int)syncEvery
                  retryInterval:(int)retryInterval
                  ) {

  // set up our initial global params
  _counter = 0;
  _alpha = estAlpha;
  _beta = estBeta;
  _syncCounter = syncCounter;
  _latencyThreshold = latencyThreshold;
  _initSyncInterval = initSyncInterval / 1000;
  _syncEvery = syncEvery / 1000;
  _retryInterval = retryInterval;

  _ipAddress = ipAddress;
  _port = port;

  _runSync = true;
  
  // dispatch this method onto the main queue so that the timer will work.
  dispatch_async(dispatch_get_main_queue(), ^{
    // setup the UDP socket.
    setupSocket(ipAddress, port);

    // start the syncing...
    sendTimeSyncPacket();
  });

}

/**
 * stopSync method
 *
 * called from the application to begin the time sync process between the app
 * and the hub.
 *
 */
RCT_EXPORT_METHOD(stopSync) {
  _runSync = false;
}

void syncStop() {
  _runSync = false;
}

/**
 * init method
 *
 * called from the application to begin the time sync process between the app
 * and the hub.
 *
 */
RCT_EXPORT_METHOD(initialize) {
  _bestInitLatency = 1000;
  _maxOffsetDiff = 0;
  _minOffsetDiff = 0;
  _latency = 0;
  _alpha = 0;
  _beta = 0;
  _intE = 0;
  _avgOffset = 0;
  _counter = 0;
  _syncCounter = 0;
  _latencyThreshold = 0;
  _retryInterval = 0;
  _runSync = false;
}


/**
 * testStartSound
 *
 * play the start sound and do nothing else
 *
 */
RCT_REMAP_METHOD(testStartSound,
                 soundFile:(NSString *)soundFile
                 soundFileExtension:(NSString *)soundFileExtension)
{
  
  NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:soundFile ofType:soundFileExtension];
  NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
  
  NSError *error;
  
  // this assumes we have a file that is cut VERY close to the leading edge of the
  // "beep" so that we are returning an EST time that is right around the time we
  // play the beep to the athletes.
  player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:&error];
  [player play];
}

/**
 * playStartSound
 *
 * play the start sound and return the time played
 *
 */
RCT_REMAP_METHOD(playStartSound,
                 soundFile:(NSString *)soundFile
                 soundFileExtension:(NSString *)soundFileExtension
                 findEventsWithResolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject)
{

  NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:soundFile ofType:soundFileExtension];
  NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];

  NSError *error;

  // this assumes we have a file that is cut VERY close to the leading edge of the
  // "beep" so that we are returning an EST time that is right around the time we
  // play the beep to the athletes.
  player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:&error];
  [player play];

  NSMutableDictionary *callbackData = [NSMutableDictionary dictionary];
  callbackData[@"currentTime"] = @(getCurrentESTTimeMS());
  callbackData[@"currentAppTime"] = @(getCurrentTimeMS());
  callbackData[@"timedOffset"] = @(_avgOffset);
  callbackData[@"maxOffsetDiff"] = @(_maxOffsetDiff);
  callbackData[@"minOffsetDiff"] = @(_minOffsetDiff);
  resolve(callbackData);
}


/**
 * getTimingData
 *
 * returns the current timing data
 *
 */
RCT_REMAP_METHOD(getTimingData,
                 resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject)
{
  NSMutableDictionary *callbackData = [NSMutableDictionary dictionary];
  callbackData[@"currentTime"] = @(getCurrentESTTimeMS());
  callbackData[@"currentAppTime"] = @(getCurrentTimeMS());
  callbackData[@"timedOffset"] = @(_avgOffset);
  callbackData[@"maxOffsetDiff"] = @(_maxOffsetDiff);
  callbackData[@"minOffsetDiff"] = @(_minOffsetDiff);
  callbackData[@"timingValid"] = @(_valid);
  resolve(callbackData);
}

@end
