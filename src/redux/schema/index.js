import { schema } from 'normalizr';

export const userSchema = new schema.Entity('user', {});
export const orgSchema = new schema.Entity('org', {});
export const teamSchema = new schema.Entity('team', {});
export const hubSchema = new schema.Entity('hub', {});
export const tagSchema = new schema.Entity('tag', {});

export const hubListSchema = [ hubSchema ];
export const tagListSchema = [ tagSchema ];
export const teamListSchema = [ teamSchema ];
export const userListSchema = [ userSchema ];

export const sessionSchema = new schema.Entity('session', {
    org: orgSchema,
    hubs: hubListSchema,
    tags: tagListSchema,
});
export const sessionListSchema = [ sessionSchema ];

export const activitySchema = new schema.Entity('activity', {});
export const activityListSchema = [ activitySchema ];

export const liveActivitySchema = new schema.Entity('live_activity', {});
export const liveActivityListSchema = [ liveActivitySchema ];

export const activityUserSchema = new schema.Entity('activity_user', {
    activity: activitySchema,
    user_data: userSchema,
    tag_data: tagSchema,
});
export const activityUserListSchema = [ activityUserSchema ];

export const activeUserSchema = new schema.Entity('active_user', {
    user_data: userSchema,
    tag_data: tagSchema,
});
export const activeUserListSchema = [ activeUserSchema ];

export const activitySummarySchema = new schema.Entity('activity_summary', {
    activity: activitySchema,
    user: userSchema
});
export const activitySummaryListSchema = [ activitySummarySchema ];

// add the leader definitions now that activitySummarySchema has been defined
// https://github.com/paularmstrong/normalizr/issues/195
activitySchema.define({
    leader1: activitySummarySchema,
    leader2: activitySummarySchema,
});

export const activityDataSchema = new schema.Entity('activity_data', {}, { idAttribute: 'activity'});
export const activityRecordSchema = new schema.Entity('activity_record', {
    activity: activitySchema
});
export const activityRecordListSchema = [ activityRecordSchema ];

export const orgSystemSchema = new schema.Entity('org_system', {
    orgSchema: orgSchema
});

export const orgSystemListSchema = [orgSystemSchema];

export const orgMemberSchema = new schema.Entity('org_member', {
    teams: teamListSchema,
    user: userSchema,
});

export const orgMemberListSchema = [orgMemberSchema];

export const userRecordSchema = new schema.Entity('user_record', {
    session: sessionSchema,
    activity: activitySchema,
    user_data: userSchema,

});
export const userRecordListSchema = [ userRecordSchema ];

export const firmwareSchema = new schema.Entity('firmware', {});
export const firmwareListSchema = [ firmwareSchema ];

