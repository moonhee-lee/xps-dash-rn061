export const SYSTEM_STATE_CONNECTING = 'connecting';
export const SYSTEM_STATE_CONNECTED = 'connected';
export const SYSTEM_STATE_DISCONNECTING = 'disconnecting';
export const SYSTEM_STATE_DISCONNECTED = 'disconnected';
