import { Client, Message } from 'react-native-paho-mqtt';
import uuidv4 from 'uuid/v4';

export const createClient = (address, port) => {
    // small wrapper around the react-native-storage api
    const localStorage = {
        setItem: (key, item) => {
            global.storage.save({
                key,
                data: item
            });
        },
        getItem: async (key) => {
            return await global.storage.load({
                key,
                autoSync: false,
            })
        },
        removeItem: (key) => {
            global.storage.remove({key});
        },
    };

    // Create a client instance
    return new Client({ uri: `ws://${address}:${port}/`, clientId: uuidv4(), storage: localStorage });
}
