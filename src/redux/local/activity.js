import { localStorage, getSyncID } from "../../lib/local_storage";
import md5 from 'md5';
import moment from "moment";
import _ from 'lodash';

/**
 * local/activity - saveActivity
 *
 * write activity to local storage for later processing
 *
 * @param  {Object} activityData - activity data
 *
 */
export const saveActivity = (activityData) => {
    console.log('saving activity to local');
    const time = moment(new Date()).format('X');
    if(!activityData.id) activityData.id = parseInt(time)+_.random(1,100000);
    activityData.sync_id = getSyncID();
    activityData.localID = md5(time+activityData.type_definition.display_name+_.random(1,100000));
    activityData.created = parseInt(moment(new Date()).format('x'));
    return new Promise((resolve: any, reject: any) => {
        localStorage.setItem('xps:activities', activityData, activityData.localID);
        resolve(activityData);
    });
}

export const fetchActivities = () => {
    return new Promise((resolve: any, reject: any) => {
        localStorage.getAllDataForKey('xps:activities').then((activities) => {
            _.each(activities,(activity) => {
                activity.id = parseInt(activity.id);
            });
            resolve(activities);
        }).catch((e) => {
            resolve([]);
        });
    });
}

export const fetchActivitiesBySession = (sessionId) => {
    return new Promise((resolve: any, reject: any) => {
        localStorage.getAllDataForKey('xps:activities').then((session_activities) => {
            const activities = _.filter(session_activities,(activity) => parseInt(activity.session) === parseInt(sessionId));
            _.each(activities,(activity) => {
                activity.id = parseInt(activity.id);
            });
            resolve(activities);
        }).catch((e) => {
            resolve([]);
        });
    });
}

export const fetchActivityDataByRecord = (record) => {
    return new Promise((resolve: any, reject: any) => {
        localStorage.getItem('xps:activities',record.localID).then((activity) => {
            const activity_data = {activity:activity.id,est:activity.data.est,events:activity.data.events};
            resolve(activity_data);
        }).catch((e) => {
            resolve([]);
        });
    });
}

