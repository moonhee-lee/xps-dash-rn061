import { localStorage } from "../../lib/local_storage";

export const saveAllSystems = (systems) => {
    localStorage.setItem('xps:allSystems',systems);
}

export const fetchAllSystems = () => {
    return new Promise((resolve: any, reject: any) => {
        localStorage.getItem('xps:allSystems').then((data) => {
            console.log('getting all systems from local');
            resolve(data);
        }).catch((err) => {
            resolve([]);
        })
    })
}
