import { getAxiosClient } from './client';

/**
 * local/user - fetchUser
 *
 * GET from API user endpoint
 *
 * @param  {Integer} userId - user to grab
 */
export const fetchUser = (userId) =>
    getAxiosClient().get(`/api/user/${userId}/`)


export const fetchUsersByTeam = (teamId) => {
    let params = {};
    if (teamId) {
        params = {team: teamId}
    }

    return getAxiosClient().get('/api/user/', { params })
}

export const saveUser = (userData) => {
    if (userData.id) {
        return getAxiosClient().put(`/api/user/${userData.id}/`, userData);
    } else {
        return getAxiosClient().post('/api/user/', userData);
    }
}

export const saveUserImage = (userId, imageKey) => {
    const params = {
        user: userId,
        primary: true,
        public_id: imageKey,
    }

    return getAxiosClient().post('/api/user_image/', params )
}