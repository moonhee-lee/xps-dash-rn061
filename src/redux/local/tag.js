import { localStorage } from "../../lib/local_storage";

export const saveAllTags = (tags) => {
    localStorage.setItem('xps:allTags',tags);
}

export const fetchAllTags = () => {
    return new Promise((resolve: any, reject: any) => {
        localStorage.getItem('xps:allTags').then((data) => {
            console.log('getting all tags from local');
            resolve(data);
        }).catch((err) => {
            resolve([]);
        })
    })
}
