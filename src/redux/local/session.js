import { localStorage, getSyncID } from "../../lib/local_storage";
import md5 from 'md5';
import moment from "moment";
import _ from 'lodash';

/**
 * local/session - saveSession
 *
 * save session to local storage
 *
 * @param  {Object} sessionData - session data
 *
 */
export const saveSession = (sessionData) => {
    const dateStamp = moment(new Date()).format('YYYYMMDD');
    const today = moment().format('MMMM DD, YYYY');
    const time = parseInt(moment(new Date()).format('X'));
    return new Promise((resolve: any) => {
        localStorage.getAllDataForKey('xps:sessions').then((org_sessions) => {
            const localSession = _.find(org_sessions,(o) => parseInt(o.id)===parseInt(dateStamp));
            if(!localSession) {
                console.log('saving session to local');
                if (!sessionData.id) {
                    if (sessionData.name === parseInt(today)) sessionData.id = parseInt(dateStamp);
                    else sessionData.id = parseInt(time);
                }
                sessionData.localID = md5(time + sessionData.name);
                sessionData.sync_id = getSyncID();
                sessionData.created = parseInt(moment(new Date()).format('x'));
                localStorage.setItem('xps:sessions', sessionData, sessionData.localID);
                resolve(sessionData);
            } else resolve(localSession);
        }).catch(() => {
            console.log('saving session to local');
            if (!sessionData.id) {
                if (sessionData.name === today) sessionData.id = parseInt(dateStamp);
                else sessionData.id = parseInt(time);
            }
            sessionData.localID = md5(time + sessionData.name);
            sessionData.sync_id = getSyncID();
            sessionData.created = parseInt(moment(new Date()).format('x'));
            localStorage.setItem('xps:sessions', sessionData, sessionData.localID);
            resolve(sessionData);
        });
    });
};

export const fetchSessionsByOrg = (orgId) => {
    return new Promise((resolve: any) => {
        localStorage.getAllDataForKey('xps:sessions').then((org_sessions) => {
            const sessions = _.filter(org_sessions,(session) => session.org === orgId);
            _.each(sessions,(session) => {
                session.id = parseInt(session.id)
            });
            resolve(sessions);
        }).catch(() => {
            resolve([]);
        });
    });
};
