import { localStorage, getSyncID } from "../../lib/local_storage";
import md5 from 'md5';
import moment from "moment";
import _ from 'lodash';

/**
 * local/org_member - saveOrgMember
 *
 * save org member to local storage
 *
 * @param  {Object} orgMemberData - org_member data
 *
 */
export const saveOrgMember = (orgMemberData) => {
    console.log('saving org_member to local');
    const time = moment(new Date()).format('X');
    orgMemberData.id = parseInt(time)+_.random(1,10);
    orgMemberData.user.id = orgMemberData.id;
    orgMemberData.sync_id = getSyncID();
    orgMemberData.localID = md5(time+orgMemberData.user.first_name);
    return new Promise((resolve: any, reject: any) => {
        localStorage.setItem('xps:orgMembers', orgMemberData, orgMemberData.localID);
        resolve(orgMemberData);
    });
}

export const fetchMembersByOrg = (orgId) => {
    return new Promise((resolve: any, reject: any) => {
        localStorage.getAllDataForKey('xps:orgMembers').then((org_members) => {
            const members = _.filter(org_members,(member) => member.org === orgId);
            _.each(members,(member) => {
                member.id = parseInt(member.id)
                if(member.user && !member.user.id) member.user.id = member.id;
            });
            resolve(members);
        }).catch((e) => {
            resolve([]);
        });
    });
}

export const saveAllOrgMembers = (orgMembers) => {
    localStorage.setItem('xps:allOrgMembers',orgMembers);
}

export const fetchAllOrgMembers = () => {
    return new Promise((resolve: any, reject: any) => {
        localStorage.getItem('xps:allOrgMembers').then((data) => {
            console.log('getting all org members from local');
            resolve(data);
        }).catch((err) => {
            resolve([]);
        })
    })
}
