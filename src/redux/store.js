import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducer from '../redux/reducers';


let store;

export const createXPSStore = () => {
    store = createStore(
        reducer,
        applyMiddleware(thunk)
    )
    return store;
}

export const getXPSStore = () => {
    return store;
}
