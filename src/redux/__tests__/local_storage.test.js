import * as loc_storage from '../../redux/local_storage';

describe('local storage suite', () => {
    beforeEach(() => {

    });

    describe('resetMessages method', () => {
        it('deletes liveESTMessages', () => {
            global.liveESTMessages = {
                1: {something: 'value'}
            };
            global.liveRawMessages = {
                1: {something: 'value'}
            };
            global.controlRawMessages = {
                control: [],
                control_response: [],
            };
            loc_storage.resetMessages(1);
            expect(global.liveESTMessages[1]).toBe(undefined);
            expect(global.liveRawMessages[1]).toBe(undefined);
            expect(global.controlRawMessages.control).toEqual([]);
            expect(global.controlRawMessages.control_response).toEqual([]);
        });

        it('leaves other liveESTMessages alone', () => {
            const tagMessages = {something: 'else'};
            global.liveESTMessages = {
                1: {something: 'value'},
                2: tagMessages,
            };
            global.liveRawMessages = {
                1: {something: 'value'}
            };
            loc_storage.resetMessages(1);
            expect(global.liveESTMessages[1]).toBe(undefined);
            expect(global.liveRawMessages[1]).toBe(undefined);
            expect(global.liveESTMessages[2]).toBe(tagMessages);  // unchanged.
        });
    });

    describe('addESTMessage method', () => {
        it('adds new EST message', () => {
            const ESTMsg = {something: 'else', time: 1};
            global.liveESTMessages = {};
            const expected = [ESTMsg];
            loc_storage.addESTMessage(1, ESTMsg);
            expect(global.liveESTMessages[1].newMsgs).toEqual(expected);
        });
        it('adds new EST message to existing', () => {
            const ESTMsg1 = {something: 'else1', time: 1};
            const ESTMsg2 = {something: 'else2', time: 2};
            global.liveESTMessages = {
                1: {
                    newMsgs: [ESTMsg1],
                    processing: [],
                    completed: [],
                }
            };
            const expected = [ESTMsg1,ESTMsg2];
            loc_storage.addESTMessage(1, ESTMsg2);
            expect(global.liveESTMessages[1].newMsgs).toEqual(expected);
        });
    });

    describe('addRawMessage method', () => {
        it('adds new EST message', () => {
            const ESTMsg = {something: 'else', time: 1};
            global.liveRawMessages = {
                1: {
                    est: [],
                }
            };
            const expected = [ESTMsg];
            loc_storage.addRawMessage(1, ESTMsg, 'est');
            expect(global.liveRawMessages[1].est).toEqual(expected);
        });
        it('adds new EST message to existing', () => {
            const ESTMsg1 = {something: 'else1', time: 1};
            const ESTMsg2 = {something: 'else2', time: 2};
            global.liveRawMessages = {
                1: {
                    est: [ESTMsg1],
                }
            };
            const expected = [ESTMsg1, ESTMsg2];
            loc_storage.addRawMessage(1, ESTMsg2, 'est');
            expect(global.liveRawMessages[1].est).toEqual(expected);
        });
    });

    describe('addRawMessage method', () => {
        it('adds new Raw message', () => {
            const RawMsg1 = {something: 'else1', time: 1, destinationName: 'est'};
            const RawMsg4 = {something: 'else1', time: 1, destinationName: 'tag_raw'};
            global.liveRawMessages = {est:[]};
            loc_storage.addRawMessage(1, RawMsg1, 'est');
            loc_storage.addRawMessage(1, RawMsg4, 'tag_raw');
            expect(global.liveRawMessages[1].est).toEqual([RawMsg1]);
            expect(global.liveRawMessages[1].tag_raw).toEqual([RawMsg4]);
        });
        it('adds new Raw message to existing', () => {
            const RawMsg1 = {something: 'else1', time: 1, destinationName: 'est'};
            const RawMsg2 = {something: 'else2', time: 2, destinationName: 'est'};
            global.liveRawMessages = {
                1: {
                    est: [],
                    est_is: [],
                    tag_meas: [],
                    tag_raw: [],
                    control_response: [],
                    control: [],
                }
            };
            const expected = [RawMsg1,RawMsg2];
            loc_storage.addRawMessage(1, RawMsg1, 'est');
            loc_storage.addRawMessage(1, RawMsg2, 'est');
            expect(global.liveRawMessages[1].est).toEqual(expected);
        });
    });

    describe('addControlMessage method', () => {
        it('adds new Raw message', () => {
            const RawMsg1 = {something: 'else1', time: 1, destinationName: 'est'};
            global.controlRawMessages = {control:[],control_response:[]};
            const expected = [RawMsg1];
            loc_storage.addControlMessage(RawMsg1, 'control');
            loc_storage.addControlMessage(RawMsg1, 'control_response');
            expect(global.controlRawMessages.control).toEqual(expected);
            expect(global.controlRawMessages.control_response).toEqual(expected);
        });
        it('adds new Raw message to existing', () => {
            const RawMsg1 = {something: 'else1', time: 1, destinationName: 'est'};
            const RawMsg2 = {something: 'else2', time: 2, destinationName: 'est'};
            global.controlRawMessages = {control:[],control_response:[]};
            const expected = [RawMsg1,RawMsg2];
            loc_storage.addControlMessage(RawMsg1, 'control');
            loc_storage.addControlMessage(RawMsg2, 'control');
            loc_storage.addControlMessage(RawMsg1, 'control_response');
            expect(global.controlRawMessages.control).toEqual(expected);
            expect(global.controlRawMessages.control_response).toEqual([RawMsg1]);
        });
    });

    describe('newMessageCount method', () => {
        it('returns correct count', () => {
            const ESTMsg1 = {something: 'else1', time: 1};
            const ESTMsg2 = {something: 'else2', time: 2};
            global.liveESTMessages = {
                newMsgs: [ESTMsg1,ESTMsg2],
                processing: [],
                completed: [],
            };
            expect(loc_storage.newMessageCount()).toBe(0);
        });
        it('returns zero for unknown tag', () => {
            global.liveESTMessages = {};
            expect(loc_storage.newMessageCount(1)).toBe(0);
        });
    });

    describe('completedMessageCount method', () => {
        it('returns correct count', () => {
            const ESTMsg1 = {something: 'else1', time: 1};
            const ESTMsg2 = {something: 'else2', time: 2};
            global.liveESTMessages = {
                newMsgs: [ESTMsg1,ESTMsg2],
                processing: [],
                completed: [],
            };
            expect(loc_storage.completedMessageCount()).toBe(0);
        });
        it('returns zero for unknown tag', () => {
            global.liveESTMessages = {};
            expect(loc_storage.completedMessageCount(1)).toBe(0);
        });
    });

    describe('getMsgsToProcess method', () => {
        it('moves messages to processing', () => {
            const ESTMsg1 = {something: 'else1', time: 1};
            const ESTMsg2 = {something: 'else2', time: 2};
            global.liveESTMessages = {
                newMsgs: [ESTMsg1,ESTMsg2],
                processing: [],
                completed: [],
            };
            const expected = {
                1: ESTMsg1,
                2: ESTMsg2
            };

            loc_storage.getMsgsToProcess(1);
            expect(global.liveESTMessages.newMsgs).toEqual([{"something": "else1", "time": 1}, {"something": "else2", "time": 2}]);
            //expect(global.liveESTMessages[1].processing).toEqual(expected);
        });
        it('returns zero for unknown tag', () => {
            const ESTMsg1 = {something: 'else1', time: 1};
            const ESTMsg2 = {something: 'else2', time: 2};
            global.liveESTMessages = {
                newMsgs: [ESTMsg1,ESTMsg2],
                processing: [],
                completed: [],
            };
            const expected = [{"something": "else1", "time": 1}, {"something": "else2", "time": 2}];

            loc_storage.getMsgsToProcess(2);
            expect(global.liveESTMessages.newMsgs).toEqual(expected);
            expect(global.liveESTMessages.processing).toEqual([]);
        });
    });

    describe('moveProcessedToCompleted method', () => {
        it('moves messages to processing', () => {
            const ESTMsg1 = {something: 'else1', time: 1};
            const ESTMsg2 = {something: 'else2', time: 2};
            global.liveESTMessages = {
                newMsgs: [ESTMsg1,ESTMsg2],
                processing: [],
                completed: [],
            };
            const expected = [];

            loc_storage.moveProcessedToCompleted(1);
            expect(global.liveESTMessages.processing).toEqual([]);
            expect(global.liveESTMessages.completed).toEqual(expected);
        });
        it('returns zero for unknown tag', () => {
            const ESTMsg1 = {something: 'else1', time: 1};
            const ESTMsg2 = {something: 'else2', time: 2};
            global.liveESTMessages = {
                newMsgs: [ESTMsg1,ESTMsg2],
                processing: [],
                completed: [],
            };
            const expected =[];

            loc_storage.moveProcessedToCompleted(2);
            expect(global.liveESTMessages.processing).toEqual(expected);
            expect(global.liveESTMessages.completed).toEqual([]);
        });
    });

    describe('getTimeRangedMessages method', () => {
        it('returns all messages within range', () => {
            const activityUser = {
                tag: {id: 1, name: '1'},
                meta_data: {start: 100, end: 200}
            };
            const ESTMsg1 = {something: 'else1', time: 100500};
            const ESTMsg2 = {something: 'else2', time: 100};
            global.liveESTMessages = {
                newMsgs: [ESTMsg1,ESTMsg2],
                processing: [],
                completed: [],
            };
            const expected = [];
            expect(loc_storage.getTimeRangedMessages(activityUser)).toEqual(expected);
        });

        it('returns no messages for unknown tag', () => {
            const activityUser = {
                tag: {id: 1},
                meta_data: {start: 100, end: 200}
            };
            global.liveESTMessages = {
            };
            expect(loc_storage.getTimeRangedMessages(activityUser)).toEqual([]);
        });
    });

    describe('getTimeRangedBuffer method', () => {
        it('returns all messages within buffer', () => {
            const activityUser = {
                tag: {id: 1, name: '1'},
                meta_data: {start: 100, end: 200}
            };
            const ESTMsg1 = {something: 'else1', time: 100500};
            const ESTMsg2 = {something: 'else2', time: 100};
            global.liveESTMessages = {
                1: {
                    newMsgs: {},
                    processing: {},
                    completed: {100500: ESTMsg1, 100: ESTMsg2},
                }
            };
            const expected = [ESTMsg2,ESTMsg1];
            expect(loc_storage.getTimeRangedBuffer(activityUser)).toEqual(expected);
        });

        it('returns no messages for unknown tag', () => {
            const activityUser = {
                tag: {id: 1},
                meta_data: {start: 100, end: 200}
            };
            global.liveESTMessages = {
            };
            expect(loc_storage.getTimeRangedBuffer(activityUser)).toEqual([]);
        });
    });

    describe('getRawMessages method', () => {
        it('returns all raw messages within buffer', () => {
            const activityUser = {
                tag: {id: 1, name: '1'},
                meta_data: {start: 100, end: 200}
            };
            const ESTMsg1 = {destinationName: "est",something: 'else1', time: 1};
            const ESTMsg2 = {destinationName: "est",something: 'else2', time: 2};
            global.liveRawMessages = {
                1: {
                    newMsgs: {},
                    processing: {},
                    completed: {100500: ESTMsg1, 100: ESTMsg2},
                }
            };
            const expected = {
                newMsgs: {},
                processing: {},
                completed: {100500: ESTMsg1, 100: ESTMsg2},
                io_event: [],
                control: [ESTMsg1,ESTMsg2],
                control_response: [ESTMsg1],
            };
            expect(loc_storage.getRawMessages(activityUser)).toEqual(expected);
        });

        it('returns no messages for unknown tag', () => {
            const activityUser = {
                tag: {id: 1},
                meta_data: {start: 100, end: 200}
            };
            global.liveRawMessages = {
            };
            expect(loc_storage.getRawMessages(activityUser)).toEqual([]);
        });
    });

    describe('addESTVelocity method', () => {
        it('sets velocity for tag', () => {
            const activityUser = {
                tag: {id: 1, name: '1'},
                meta_data: {start: 100, end: 200}
            };
            expect(loc_storage.addESTVelocity(activityUser.tag.name,50)).toEqual(undefined);
        });
    });

    describe('getESTVelocity method', () => {
        it('gets velocity for tag', () => {
            const activityUser = {
                tag: {id: 1, name: '1'},
                meta_data: {start: 100, end: 200}
            };
            global.liveESTMessages = {
                1: {
                    newMsgs: {},
                    processing: {},
                    completed: {},
                    io_event: [],
                    velocity: 50
                }
            };
            expect(loc_storage.getESTVelocity(activityUser.tag.name)).toEqual(50);
        });
    });

    describe('getESTPosition method', () => {
        it('gets position for tag', () => {
            const activityUser = {
                tag: {id: 1, name: '1'},
                meta_data: {start: 100, end: 200}
            };
            global.liveESTMessages = {
                1: {
                    newMsgs: {},
                    processing: {},
                    completed: {},
                    posM: 50
                }
            };
            expect(loc_storage.getESTPosition(activityUser.tag.name)).toEqual(50);
        });
    });

});
