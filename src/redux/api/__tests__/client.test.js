import axios from 'axios';

import { getAxiosClient } from '../../redux/api/client';
import * as fromStore from '../../redux/store';
import * as fromReducers from '../../redux/reducers';

jest.mock('axios');
jest.mock('../../redux/store');
jest.mock('../../redux/reducers');


describe('apiClient suite', () => {
    let storeMock;
    beforeEach(() => {
        global.xpsConfig = {
            siteUrl: 'http://localhost:8000/'
        };
        storeMock = {
            getState: function() {return 'something';}
        }
    });

    describe('getAxiosClient method', () => {
        it('should set jwtToken', () => {
            const mockAxiosInstance = {
                defaults: {
                    headers: {
                        common: {}
                    }
                }
            };
            const authState = {authToken: 'sometoken'};
            axios.create.mockImplementationOnce(() => mockAxiosInstance);
            fromStore.getXPSStore.mockImplementationOnce(() => storeMock);
            fromReducers.getAuthState.mockImplementationOnce(() => authState);

            const axiosInstance = getAxiosClient();

            expect(axiosInstance.defaults.headers.common['Authorization']).toBe('JWT sometoken');
        });

        it('should set jwtToken even with no baseURL', () => {
            global.xpsConfig = null;
            const mockAxiosInstance = {
                defaults: {
                    headers: {
                        common: {}
                    }
                }
            };
            const authState = {authToken: 'sometoken'};
            axios.create.mockImplementationOnce(() => mockAxiosInstance);
            fromStore.getXPSStore.mockImplementationOnce(() => storeMock);
            fromReducers.getAuthState.mockImplementationOnce(() => authState);

            const axiosInstance = getAxiosClient();

            expect(axiosInstance.defaults.headers.common['Authorization']).toBe('JWT sometoken');
        });

        it('should not set jwtToken when no store', () => {
            const mockAxiosInstance = {
                defaults: {
                    headers: {
                        common: {}
                    }
                }
            };
            axios.create.mockImplementationOnce(() => mockAxiosInstance);
            fromStore.getXPSStore.mockImplementationOnce(() => null);

            const axiosInstance = getAxiosClient();

            expect(axiosInstance.defaults.headers.common).toEqual({});
        });
        it('should not set jwtToken when no auth', () => {
            const mockAxiosInstance = {
                defaults: {
                    headers: {
                        common: {}
                    }
                }
            };
            const authState = {authToken: ''};
            axios.create.mockImplementationOnce(() => mockAxiosInstance);
            fromStore.getXPSStore.mockImplementationOnce(() => storeMock);
            fromReducers.getAuthState.mockImplementationOnce(() => authState);

            const axiosInstance = getAxiosClient();

            expect(axiosInstance.defaults.headers.common).toEqual({});
        });
    });
});
