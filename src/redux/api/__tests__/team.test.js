import * as apiClient from '../../redux/api/client';
import * as teamsApi from '../../redux/api/team';

jest.mock('../../redux/api/client');


describe('teamsApi suite', () => {
    let axiosClient;

    beforeEach(() => {
        axiosClient = {
            get: jest.fn(),
            post: jest.fn(),
            put: jest.fn(),
        }
    });

    describe('fetchTeamsByOrg method', () => {
        it('should call axios get for fetchTeamsByOrg', () => {
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);

            teamsApi.fetchTeamsByOrg(1);
            expect(axiosClient.get.mock.calls.length).toBe(1);
            expect(axiosClient.get.mock.calls[0][1]).toEqual({ params: {org: 1}});
        });
        it('should call axios get for fetchTeamsByOrg with no orgId', () => {
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);

            teamsApi.fetchTeamsByOrg();
            expect(axiosClient.get.mock.calls.length).toBe(1);
            expect(axiosClient.get.mock.calls[0][1]).toEqual({ params: {}});
        });
    });

});
