import * as apiClient from '../../redux/api/client';
import * as orgMemberApi from '../../redux/api/org_member';

jest.mock('../../redux/api/client');


describe('orgMemberApi suite', () => {
    let axiosClient;

    beforeEach(() => {
        axiosClient = {
            get: jest.fn(),
            post: jest.fn(),
            put: jest.fn(),
        }
    });


    describe('saveOrgMember method', () => {
        it('should call axios post for saveOrgMember', () => {
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);

            orgMemberApi.saveOrgMember({id: 1});
            expect(axiosClient.put.mock.calls.length).toBe(0);
            expect(axiosClient.post.mock.calls.length).toBe(1);
        });
        it('should call axios put for saveOrgMember with no orgId', () => {
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);

            orgMemberApi.saveOrgMember({});
            expect(axiosClient.put.mock.calls.length).toBe(0);
            expect(axiosClient.post.mock.calls.length).toBe(1);
        });
    });

    describe('fetchMembersByOrg method', () => {
        it('should call axios get for fetchMembersByOrg', () => {
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);

            orgMemberApi.fetchMembersByOrg(1);
            expect(axiosClient.get.mock.calls.length).toBe(1);
            expect(axiosClient.get.mock.calls[0][1]).toEqual({ params: {org: 1}});
        });
        it('should call axios get for fetchMembersByOrg with no params', () => {
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);

            orgMemberApi.fetchMembersByOrg();
            expect(axiosClient.get.mock.calls.length).toBe(1);
            expect(axiosClient.get.mock.calls[0][1]).toEqual({ params: {}});
        });
    });

});
