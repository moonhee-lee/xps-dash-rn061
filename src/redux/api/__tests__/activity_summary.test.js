import * as apiClient from '../../redux/api/client';
import * as activitySummaryApi from '../../redux/api/activity_summary';

jest.mock('../../redux/api/client');


describe('activitySummaryApi suite', () => {
    let axiosClient;

    beforeEach(() => {
        axiosClient = {
            get: jest.fn(),
            post: jest.fn(),
            put: jest.fn(),
        }
    });

    describe('fetchActivitySummariesByActivity method', () => {
        it('should call axios get for fetchActivitySummariesByActivity', () => {
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);

            activitySummaryApi.fetchActivitySummariesByActivity(1);
            expect(axiosClient.get.mock.calls.length).toBe(1);
            expect(axiosClient.get.mock.calls[0][1]).toEqual({ params: {'activity': 1}});
        });
        it('should call axios get for fetchActivitySummariesByActivity with no activityId', () => {
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);

            activitySummaryApi.fetchActivitySummariesByActivity();
            expect(axiosClient.get.mock.calls.length).toBe(1);
            expect(axiosClient.get.mock.calls[0][1]).toEqual({ params: {}});
        });
    });
});
