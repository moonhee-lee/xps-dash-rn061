import * as apiClient from '../../redux/api/client';
import * as activityRecordsApi from '../../redux/api/activity_record';

jest.mock('../../redux/api/client');


describe('activityRecordsApi suite', () => {
    let axiosClient;

    beforeEach(() => {
        axiosClient = {
            get: jest.fn(),
            post: jest.fn(),
            put: jest.fn(),
        }
    });

    describe('fetchActivityRecords method', () => {
        it('should call axios get for fetchActivityRecords', () => {
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);
            const params = {session: 2, type: 1};
            activityRecordsApi.fetchActivityRecords(1, 2, true);
            expect(axiosClient.get.mock.calls.length).toBe(1);
            expect(axiosClient.get.mock.calls[0][1]).toEqual({ params });
        });
        it('should call axios get for fetchActivityRecords with no activityId', () => {
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);
            const params = {session: 2};
            activityRecordsApi.fetchActivityRecords(null, 2, true);
            expect(axiosClient.get.mock.calls.length).toBe(1);
            expect(axiosClient.get.mock.calls[0][1]).toEqual({ params });
        });
        it('should call axios get for fetchActivityRecords with no userId', () => {
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);
            const params = {type: 1};
            activityRecordsApi.fetchActivityRecords(1, null, true);
            expect(axiosClient.get.mock.calls.length).toBe(1);
            expect(axiosClient.get.mock.calls[0][1]).toEqual({ params });
        });
        it('should call axios get for fetchActivityRecords with no includeData', () => {
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);
            const params = {session: 2, type: 1};
            activityRecordsApi.fetchActivityRecords(1, 2, false);
            expect(axiosClient.get.mock.calls.length).toBe(1);
            expect(axiosClient.get.mock.calls[0][1]).toEqual({ params });
        });
    });

    describe('fetchActivityRecordsByUser method', () => {
        it('should call axios get for fetchActivityRecordsByUser', () => {
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);

            activityRecordsApi.fetchActivityRecordsByUser(1);
            expect(axiosClient.get.mock.calls.length).toBe(1);
            expect(axiosClient.get.mock.calls[0][1]).toEqual({ params: {user: 1}});
        });
        it('should call axios get for fetchActivityRecordsByUser with no activityId', () => {
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);

            activityRecordsApi.fetchActivityRecordsByUser();
            expect(axiosClient.get.mock.calls.length).toBe(1);
            expect(axiosClient.get.mock.calls[0][1]).toEqual({ params: {}});
        });
    });

    describe('saveActivityRecord method', () => {
        it('should call axios post for saveActivityRecord without id', () => {
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);

            activityRecordsApi.saveActivityRecord({});
            expect(axiosClient.post.mock.calls.length).toBe(1);
        });
        it('should call axios put for saveActivityRecord with id', () => {
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);

            activityRecordsApi.saveActivityRecord({id: 1});
            expect(axiosClient.put.mock.calls.length).toBe(1);
        });
    });

});
