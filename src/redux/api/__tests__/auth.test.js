import * as apiClient from '../../redux/api/client';
import * as authApi from '../../redux/api/auth';

jest.mock('../../redux/api/client');


describe('authApi suite', () => {
    let axiosClient;

    beforeEach(() => {
        axiosClient = {
            get: jest.fn(),
            post: jest.fn(),
            put: jest.fn(),
        }
    });

    describe('login method', () => {
        it('should call axios post for login', () => {
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);

            authApi.login('username', 'password');
            expect(axiosClient.post.mock.calls.length).toBe(1);
        });
    });
});
