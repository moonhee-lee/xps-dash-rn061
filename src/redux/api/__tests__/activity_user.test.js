import * as apiClient from '../../redux/api/client';
import * as activityUserApi from '../../redux/api/activity_user';

jest.mock('../../redux/api/client');


describe('activityUserApi suite', () => {
    let axiosClient;

    beforeEach(() => {
        axiosClient = {
            get: jest.fn(),
            post: jest.fn(),
            put: jest.fn(),
            delete: jest.fn(),
        }
    });

    describe('fetchActivityUsersByActivity method', () => {
        it('should call axios get for fetchActivityUsersByActivity', () => {
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);

            activityUserApi.fetchActivityUsersByActivity(1);
            expect(axiosClient.get.mock.calls.length).toBe(1);
            expect(axiosClient.get.mock.calls[0][1]).toEqual({ params: {activity: 1}});
        });
        it('should call axios get for fetchActivityUsersByActivity with no params', () => {
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);

            activityUserApi.fetchActivityUsersByActivity();
            expect(axiosClient.get.mock.calls.length).toBe(1);
            expect(axiosClient.get.mock.calls[0][1]).toEqual({ params: {}});
        });
    });

    describe('saveActivityUser method', () => {
        it('should call axios put for saveActivityUser', () => {
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);
            const activityUserData = {
                user: {id: 1},
                activity: {id: 1},
                id: 1
            };

            activityUserApi.saveActivityUser(activityUserData);
            expect(axiosClient.put.mock.calls.length).toBe(1);
        });
        it('should call axios post for new saveActivityUser', () => {
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);
            const activityUserData = {
                user: 1,
                activity: 1,
            };

            activityUserApi.saveActivityUser(activityUserData);
            expect(axiosClient.post.mock.calls.length).toBe(1);
        });
    });
    describe('deleteActivityUser method', () => {
        it('should call axios get for deleteActivityUser', () => {
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);

            activityUserApi.deleteActivityUser(1);
            expect(axiosClient.delete.mock.calls.length).toBe(1);
        });
    });
});
