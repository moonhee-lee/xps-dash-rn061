import * as apiClient from '../../redux/api/client';
import * as tagApi from '../../redux/api/tag';

jest.mock('../../redux/api/client');


describe('tagApi suite', () => {
    describe('fetchTagsBySystem method', () => {
        it('should call axios get for fetchTagsBySystem', () => {
            const axiosClient = {
                get: jest.fn()
            }
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);

            tagApi.fetchTagsBySystem(1);
            expect(axiosClient.get.mock.calls.length).toBe(1);
            expect(axiosClient.get.mock.calls[0][1]).toEqual({ params: {system: 1}});
        });
        it('should call axios get for fetchTagsBySystem with no params', () => {
            const axiosClient = {
                get: jest.fn()
            }
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);

            tagApi.fetchTagsBySystem();
            expect(axiosClient.get.mock.calls.length).toBe(1);
            expect(axiosClient.get.mock.calls[0][1]).toEqual({ params: {}});
        });
    });
});
