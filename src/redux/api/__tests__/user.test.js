import * as apiClient from '../../redux/api/client';
import * as userApi from '../../redux/api/user';

jest.mock('../../redux/api/client');


describe('userApi suite', () => {
    describe('fetchUser method', () => {
        it('should call axios get for fetchUser', () => {
            const axiosClient = {
                get: jest.fn()
            }
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);

            userApi.fetchUser();
            expect(axiosClient.get.mock.calls.length).toBe(1);
        });
    });

    describe('fetchUsersByTeam method', () => {
        it('should call axios get for fetchUsersByTeam', () => {
            const axiosClient = {
                get: jest.fn()
            }
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);

            userApi.fetchUsersByTeam(1);
            expect(axiosClient.get.mock.calls.length).toBe(1);
            expect(axiosClient.get.mock.calls[0][1]).toEqual({ params: {team: 1}});
        });
        it('should call axios get for fetchUsersByTeam with no params', () => {
            const axiosClient = {
                get: jest.fn()
            }
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);

            userApi.fetchUsersByTeam();
            expect(axiosClient.get.mock.calls.length).toBe(1);
            expect(axiosClient.get.mock.calls[0][1]).toEqual({ params: {}});
        });
    });

    describe('saveUserImage method', () => {
        it('should call axios get for saveUserImage with no params', () => {
            const axiosClient = {
                post: jest.fn()
            }
            const params = {
                user: 1,
                primary: true,
                public_id: 2
            };
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);

            userApi.saveUserImage(1, 2);
            expect(axiosClient.post.mock.calls.length).toBe(1);
            expect(axiosClient.post.mock.calls[0][1]).toEqual( params );
        });
    });

});
