import * as apiClient from '../../redux/api/client';
import * as sessionsApi from '../../redux/api/session';

jest.mock('../../redux/api/client');


describe('sessionsApi suite', () => {
    let axiosClient;

    beforeEach(() => {
        axiosClient = {
            get: jest.fn(),
            post: jest.fn(),
            put: jest.fn(),
        }
    });

    describe('fetchSessionsByOrg method', () => {
        it('should call axios get for fetchSessionsByOrg', () => {
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);

            sessionsApi.fetchSessionsByOrg(1);
            expect(axiosClient.get.mock.calls.length).toBe(1);
            expect(axiosClient.get.mock.calls[0][1]).toEqual({ params: {org: 1}});
        });
        it('should call axios get for fetchSessionsByOrg with no orgId', () => {
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);

            sessionsApi.fetchSessionsByOrg();
            expect(axiosClient.get.mock.calls.length).toBe(1);
            expect(axiosClient.get.mock.calls[0][1]).toEqual({ params: {}});
        });
    });

    describe('saveSession method', () => {
        it('should call axios post for saveSession without id', () => {
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);

            sessionsApi.saveSession({});
            expect(axiosClient.post.mock.calls.length).toBe(1);
        });
        it('should call axios put for saveSession with id', () => {
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);

            sessionsApi.saveSession({id: 1});
            expect(axiosClient.post.mock.calls.length).toBe(1);
        });
    });

});
