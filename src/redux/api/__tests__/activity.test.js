import * as apiClient from '../../redux/api/client';
import * as activitiesApi from '../../redux/api/activity';

jest.mock('../../redux/api/client');


describe('activitiesApi suite', () => {
    let axiosClient;

    beforeEach(() => {
        axiosClient = {
            get: jest.fn(),
            post: jest.fn(),
            put: jest.fn(),
        }
    });

    describe('fetchActivitiesBySession method', () => {
        it('should call axios get for fetchActivitiesBySession', () => {
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);

            activitiesApi.fetchActivitiesBySession(1);
            expect(axiosClient.get.mock.calls.length).toBe(1);
            expect(axiosClient.get.mock.calls[0][1]).toEqual({ params: {session: 1}});
        });
        it('should call axios get for fetchActivitiesBySession with no sessionId', () => {
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);

            activitiesApi.fetchActivitiesBySession();
            expect(axiosClient.get.mock.calls.length).toBe(1);
            expect(axiosClient.get.mock.calls[0][1]).toEqual({ params: {}});
        });
    });

    describe('fetchActivitiesByUser method', () => {
        it('should call axios get for fetchActivitiesByUser', () => {
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);

            activitiesApi.fetchActivitiesByUser(1);
            expect(axiosClient.get.mock.calls.length).toBe(1);
        });
        it('should call axios get for fetchActivitiesByUser with no sessionId', () => {
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);

            activitiesApi.fetchActivitiesByUser();
            expect(axiosClient.get.mock.calls.length).toBe(1);
        });
    });

    describe('saveActivity method', () => {
        it('should call axios post for saveActivity without id', () => {
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);

            activitiesApi.saveActivity({});
            expect(axiosClient.post.mock.calls.length).toBe(1);
        });
        it('should call axios put for saveActivity with id', () => {
            apiClient.getAxiosClient.mockImplementationOnce(() => axiosClient);

            activitiesApi.saveActivity({id: 1});
            expect(axiosClient.post.mock.calls.length).toBe(1);
        });
    });

});
