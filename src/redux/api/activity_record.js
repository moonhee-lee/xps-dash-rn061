import { getAxiosClient } from './client';


/**
 * api/activity_record - fetchActivityRecordsByActivity
 *
 * GET API activity_record endpoint
 * filter by the orgId if provided.
 *
 * @param  [{Object}] activityRecordData - activity_record data
 *
 */
export const fetchActivityRecordById = (id) => {
    return getAxiosClient().get(`/api/activity/${id}/`);
}


export const fetchActivityRecords = (type_hash,sessionId) => {
    let params = {};
    if (type_hash) params.type = type_hash;
    if (sessionId) params.session = sessionId;

    return getAxiosClient().get('/api/activity/', { params });
}

/**
 * api/activity_record - fetchActivityRecordsByUser
 *
 * GET API activity_record endpoint
 * filter by the orgId if provided.
 *
 * @param  [Integer] userId - userId
 *
 */
export const fetchActivityRecordsByUser = (userId) => {
    let params = {};
    if (userId) params.user = userId;

    return getAxiosClient().get('/api/activity/', { params });
}

/**
 * api/activity_record - saveActivityRecord
 *
 * POST to API activity_record endpoint
 * create a new activity_record or save current one (via API url)
 *
 * @param  {Object} activityRecordData - activity_record data
 *
 */
export const saveActivityRecord = (activityRecordData) => {
    if (activityRecordData.id) {
        return getAxiosClient().put(`/api/activity_record/${activityRecordData.id}/`, activityRecordData);
    } else {
        return getAxiosClient().post('/api/activity_record/', activityRecordData);
    }
}