import data from './data';


export const fetchActivityUsersByActivity = jest.fn(() => 
    Promise.resolve({
        data: {
            results: data.activity_users
        }
    })
);


export const saveActivityUser = jest.fn(() => 
    Promise.resolve({
        data: data.activity_users[0]
    })
);


export const deleteActivityUser = jest.fn(() => Promise.resolve());
