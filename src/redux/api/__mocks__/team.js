import data from './data';


export const fetchTeamsByOrg = jest.fn(() => 
    Promise.resolve({
        data: {
            results: data.teams
        }
    })
);
