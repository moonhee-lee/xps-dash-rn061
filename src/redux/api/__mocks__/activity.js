import data from './data';


export const fetchActivitiesBySession = jest.fn(() => 
    Promise.resolve({
        data: {
            results: data.activities
        }
    })
);


export const fetchActivitiesByUser = jest.fn(() => 
    Promise.resolve({
        data: {
            results: data.activities
        }
    })
);


export const fetchActivity = jest.fn(() => 
    Promise.resolve({
        data: data.activities[0]
    })
);


export const saveActivity = jest.fn(() => 
    Promise.resolve({
        data: data.activities[0]
    })
);
