import data from './data';


export const fetchMembersByOrg = jest.fn(() => 
    Promise.resolve({
        data: {
            results: data.org_members
        }
    })
);

export const fetchActiveUsers = jest.fn(() =>
    Promise.resolve({
        data: {
            results: data.active_users
        }
    })
);

export const fetchMemberByUserId = jest.fn(() =>
    Promise.resolve({
        data: {
            results: data.org_members
        }
    })
);

export const saveOrgMember = jest.fn(() => 
    Promise.resolve({
        data: data.org_members[0]
    })
);
