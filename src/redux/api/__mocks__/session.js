import data from './data';

export const fetchSessionsByOrg = jest.fn(() => 
    Promise.resolve({
        data: {
            results: data.sessions
        }
    })
);

export const fetchSessionById = jest.fn(() =>
    Promise.resolve({
        data: {
            results: data.sessions
        }
    })
);


export const saveSession = jest.fn(() => 
    Promise.resolve({
        data: data.sessions[0]
    })
);
