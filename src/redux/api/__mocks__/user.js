import data from './data';


export const fetchUser = jest.fn(() => 
    Promise.resolve({
        data: data.users[0]
    })
);


export const saveUser = jest.fn(() => 
    Promise.resolve({
        data: data.users[0]
    })
);


export const saveUserImage = jest.fn(() => 
    Promise.resolve({
        data: data.users[0]
    })
);


export const fetchUsersByTeam = jest.fn(() => 
    Promise.resolve({
        data: {
            results: data.users
        }
    })
);
