import data from './data';


export const login = jest.fn(() => 
    Promise.resolve({
        data: {
            token: 'sometoken',
            user: data.users[0]
        }
    })
);


export const logout = jest.fn(() => 
    Promise.resolve()
);