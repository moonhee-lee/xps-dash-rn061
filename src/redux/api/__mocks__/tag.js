import data from './data';


export const fetchTagsBySystem = jest.fn(() => 
    Promise.resolve({
        data: {
            results: data.tags
        }
    })
);

export const fetchTagsByOrg = jest.fn(() =>
    Promise.resolve({
        data: {
            results: data.tags
        }
    })
);
