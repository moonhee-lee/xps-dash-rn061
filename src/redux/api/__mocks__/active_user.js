import data from './data';


export const fetchActiveUsersByActivity = jest.fn(() =>
    Promise.resolve({
        data: {
            results: data.active_users
        }
    })
);


export const saveActiveUser = jest.fn(() =>
    Promise.resolve({
        data: data.active_users[0]
    })
);

export const saveUserTag = jest.fn(() =>
    Promise.resolve({
        data: data.active_users[0]
    })
);


export const deleteActiveUser = jest.fn(() => Promise.resolve());
