import data from './data';


export const fetchActivitySummariesByActivity = jest.fn(() => 
    Promise.resolve({
        data: {
            results: data.activity_summaries
        }
    })
);
