import md5 from 'md5';
import moment from "moment";
import _ from 'lodash';

const time = moment(new Date()).format('X');
const rID = parseInt(time)+_.random(1,10);

const data = {
    users: [
        {id:1, first_name:'Test', last_name:'Test',full_name: "Test Test", image_url: "",is_staff: false, org: {id: 1},thumbnail_url: ""},
        {id:2, first_name:'Test', last_name:'Test',full_name: "Test Test2", image_url: "",is_staff: false, org: {id: 1},thumbnail_url: ""},
        {id:3, first_name:'Test', last_name:'Test',full_name: "Test Test3", image_url: "",is_staff: false, org: {id: 1},thumbnail_url: ""},
    ],
    sessions: [
        {id: rID, name: 'Test Session 2', localID: md5(time+'Test Session 2')},
        {id: rID, name: 'Test Session 1', localID: md5(time+'Test Session 1')},
    ],
    activities: [
        {id: 1, name: 'Test Activity 1'},
        {id: 2, name: 'Test Activity 2'},
    ],
    activity_records: [
        {id: 1, activity: 1, user: 1},
        {id: 2, activity: 1, user: 2},
    ],
    activity_users: [
        {id: 1, activity: 1, user: 1},
        {id: 2, activity: 1, user: 2},
    ],
    activity_summaries: [
        {id: 1, activity: 1, user: 1, num_records: 10, top_record: 1},
        {id: 2, activity: 1, user: 2, num_records: 8, top_record: 2},
    ],
    teams: [
        {id: 1, name: 'Volleyball Team'},
        {id: 2, name: 'Basketball Team'},
    ],
    tags: [
        {id: 1, system: 1, name: '1'},
        {id: 2, system: 1, name: '2'},
    ],
    org_members: [
        {id: time, org: 1, user: {id:1,
                first_name:'Test',
                last_name:'Test',
                full_name: "Test Test",
                image_url: "",
                is_staff: false,
                org: {id: 1},
                thumbnail_url: ""
            }, is_admin: false, localID: md5(time+'Test')},
        {id: time, org: 1, user: {id:2,
                first_name:'Test',
                last_name:'Test',
                full_name: "Test Test",
                image_url: "",
                is_staff: false,
                org: {id: 1},
                thumbnail_url: ""
            }, is_admin: false, localID: md5(time+'Test')},
    ]
};

export default data;
