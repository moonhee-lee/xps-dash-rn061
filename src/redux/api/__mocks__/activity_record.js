import data from './data';


export const fetchActivityRecords = jest.fn(() => 
    Promise.resolve({
        data: {
            results: data.activity_records
        }
    })
);


export const fetchActivityRecordsByUser = jest.fn(() => 
    Promise.resolve({
        data: {
            results: data.activity_records
        }
    })
);


export const saveActivityRecord = jest.fn(() => 
    Promise.resolve({
        data: data.activity_records[0]
    })
);
