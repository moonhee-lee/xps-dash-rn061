import { getAxiosClient } from './client';


/**
 * api/org_system - saveOrgSystem
 *
 * POST to API org_system endpoint
 * create a new org_system or save current one (via API url)
 * 
 * @param  {Object} saveOrgSystem - org_system data
 * 
 */
export const saveOrgSystem = (orgSystemData) => {
    if (orgSystemData.id) {
        return getAxiosClient().put(`/api/org_member/${orgSystemData.id}/`, orgSystemData);
    } else {
        return getAxiosClient().post('/api/org_member/', orgSystemData);
    }
}

/**
 * api/org_system - fetchSystemsByOrg
 *
 * GET to API org_system endpoint
 * loads org_systems for specific ORG
 *
 * @param  {int} orgId - org id
 *
 */
export const fetchSystemsByOrg = (orgId) => {
    let params = {};
    if (orgId) {
        params = {org: orgId}
    }
    return getAxiosClient().get('/api/system/', { params })
}
