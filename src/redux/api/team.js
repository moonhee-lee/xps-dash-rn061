import { getAxiosClient } from './client';


/**
 * api/team - fetchTeamsByOrg
 *
 * GET API team endpoint
 * filter by the orgId if provided.
 * 
 * @param  [{Object}] teamData - team data
 * 
 */
export const fetchTeamsByOrg = (orgId) => {
    let params = {};
    if (orgId) {
        params = {org: orgId}
    }

    return getAxiosClient().get('/api/team/', { params });
}