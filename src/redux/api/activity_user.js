import { getAxiosClient } from './client';


/**
 * api/activity_user - fetchActivityUsersByActivity
 *
 * GET API activity_user endpoint
 * filter by the activityId if provided.
 * 
 * @param  [{Object}] activity_user data
 * 
 */
export const fetchActivityUsersByActivity = (activityId) => {
    let params = {};
    if (activityId) {
        params = {activity: activityId};
    }
    return getAxiosClient().get('/api/activity_user/', { params });
}


export const saveActivityUser = (activityUserData) => {
    if (typeof(activityUserData.user) === 'object') {
        activityUserData.user = activityUserData.user.id;
    }
    if (typeof(activityUserData.activity) === 'object') {
        activityUserData.activity = activityUserData.activity.id;
    }

    if (activityUserData.id) {
        return getAxiosClient().put(`/api/activity_user/${activityUserData.id}/`, activityUserData);
    } else {
        return getAxiosClient().post('/api/activity_user/', activityUserData);
    }
}


export const deleteActivityUser = (activityUserId) =>
    getAxiosClient().delete(`/api/activity_user/${activityUserId}/`);
