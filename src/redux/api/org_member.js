import { getAxiosClient } from './client';


/**
 * api/org_member - saveOrgMember
 *
 * POST to API org_member endpoint
 * create a new org_member or save current one (via API url)
 * 
 * @param  {Object} orgMemberData - org_member data
 * 
 */
export const saveOrgMember = (orgMemberData) => {
    console.log(JSON.stringify(orgMemberData));
    if(orgMemberData.user) orgMemberData.user.id = null;
    //if (orgMemberData.id) {
    //    return getAxiosClient().put(`/api/org_member/${orgMemberData.id}/`, orgMemberData);
    //} else {
        return getAxiosClient().post('/api/org_member/', orgMemberData);
    //}
};

/**
 * api/org_member - fetchMembersByOrg
 *
 * GET to API org_member endpoint
 * loads org_members for specific ORG
 *
 * @param  {int} orgId - org id
 *
 */
export const fetchMembersByOrg = (orgId) => {
    let params = {};
    if (orgId) {
        params = {org: orgId}
    }
    return getAxiosClient().get('/api/org_member/', { params });
};


/**
 * api/org_member - fetchMembersByUserId
 *
 * GET to API org_member endpoint
 * loads org_member for specific user
 *
 * @param  {int} userId - user id
 *
 */
export const fetchMemberByUserId = (userId) => {
    let params = {};
    if (userId) {
        params = {user: userId}
    }
    return getAxiosClient().get('/api/org_member/', { params });
};
