import { getAxiosClient } from './client';


/**
 * api/activity_summary - fetchActivitySummariesByActivity
 *
 * GET API activity_summary endpoint
 * filter by the sessionId if provided.
 * 
 * @param  [{Object}] activity_summary data
 * 
 */
export const fetchActivitySummariesByActivity = (activity_id) => {
    let params = {};
    if (activity_id) {
        params = {'activity': activity_id}
    }

    return getAxiosClient().get('/api/activity_summary/', { params });
}
