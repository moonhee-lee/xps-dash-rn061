import { getAxiosClient } from './client';


/**
 * api/auth - login
 *
 * POST to the API login endpoint.
 * 
 * @param  {String} username - username of user attempting to login
 * @param  {String} password - username's password
 */
export const login = (username, password) => 
    // POST to the API method to login the user.
    // the caller of this method handles the POST response or error.
    getAxiosClient().post('/api/login/', {
        username,
        password
    });
