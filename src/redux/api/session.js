import { getAxiosClient } from './client';


/**
 * api/session - fetchSessionsByOrg
 *
 * GET API session endpoint
 * filter by the orgId if provided.
 * 
 * @param  [{Object}] sessionData - session data
 * 
 */
export const fetchSessionsByOrg = (orgId) => {
    let params = {};
    if (orgId) {
        params = {org: orgId}
    }
    return getAxiosClient().get('/api/session/', { params });
}

export const fetchSessionById = (id) => {
    return getAxiosClient().get('/api/session/'+id+'/');
}


/**
 * api/session - saveSession
 *
 * POST to API session endpoint
 * create a new session or save current one (via API url)
 * 
 * @param  {Object} sessionData - session data
 * 
 */
export const saveSession = (sessionData) => {
    return getAxiosClient().post('/api/session/', sessionData);
}