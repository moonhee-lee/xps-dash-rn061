import { getAxiosClient } from './client';


/**
 * api/activity_data - fetchActivityDataById
 *
 * GET API activity_data endpoint
 *
 * @param  [{Object}] activityRecordData - activity_data
 *
 */
export const fetchActivityDataById = (id) => {
    return getAxiosClient().get(`/api/activity_data/${id}/`);
}
