import { getAxiosClient } from './client';

export const fetchFirmwares = () => {
    return getAxiosClient().get('/api/hub_firmware/');
}
