import { getAxiosClient } from './client';


/**
 * api/session - fetchTagsBySystem
 *
 * GET API session endpoint
 * filter by the orgId if provided.
 *
 * @param  [{Object}] sessionData - session data
 *
 */
export const fetchTagsBySystem = (systemId) => {
    let params = {};
    if (systemId) {
        params = {system: systemId}
    }

    return getAxiosClient().get('/api/tag/', { params });
}

/**
 * api/session - fetchTagsByOrg
 *
 * GET API session endpoint
 * filter by the orgId if provided.
 *
 * @param  [{Object}] sessionData - session data
 *
 */
export const fetchTagsByOrg = (orgId) => {
    let params = {};
    if (orgId) {
        params = {org: orgId}
    }

    return getAxiosClient().get('/api/tag/', { params });
}
