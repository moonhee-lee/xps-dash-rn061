import { getAxiosClient } from './client';


/**
 * api/activity - fetchActivitiesBySession
 *
 * GET API activity endpoint
 * filter by the sessionId if provided.
 * 
 * @param  [{Object}] activityData - activity data
 * 
 */
export const fetchActivities = () => {
    return getAxiosClient().get('/api/activity/');
}

export const fetchActivitiesBySession = (sessionId) => {
    let params = {};
    if (sessionId) {
        params = {session: sessionId}
    }

    return getAxiosClient().get('/api/activity/', { params });
}

export const fetchActivitiesByUser = (userId) => getAxiosClient().get('/api/activity/?user='+userId);


export const fetchActivity = (activityId) => getAxiosClient().get(`/api/activity/${activityId}/`);

/**
 * api/activity - saveActivity
 *
 * POST to API activity endpoint
 * create a new activity or save current one (via API url)
 * 
 * @param  {Object} activityData - activity data
 * 
 */
export const saveActivity = (activityData) => {
    //console.log(JSON.stringify(activityData));
    return getAxiosClient(60000).post('/api/activity/', activityData);
}

export const saveLiveActivity = (activityData) => {
    return getAxiosClient(60000).post('/api/activity/', activityData);
}