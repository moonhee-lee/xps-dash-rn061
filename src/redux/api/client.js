import axios from 'axios';

import { getXPSStore } from '../../redux/store';
import { getAuthState } from '../../redux/reducers';


/**
 * api/client - getAxiosClient
 *
 * return an axios instance with JWT headers
 * set up (if user logged in)
 *
 */
export const getAxiosClient = (timeout=3000) => {
    const axiosInstance = axios.create({
        baseURL: global.xpsConfig ? global.xpsConfig.siteUrl : '',
        timeout,
    });
    const store = getXPSStore();
    const state = store ? store.getState() : null;
    const authState = state ? getAuthState(state) : null;

    const jwtToken = authState ? authState.authToken : null;
    if (jwtToken) {
        axiosInstance.defaults.headers.common['Authorization'] = `JWT ${jwtToken}`;
    }

    return axiosInstance;
}
