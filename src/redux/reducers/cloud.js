import { combineReducers } from 'redux';
import {
    CLOUD_CONNECTION_REQUESTED,
    CLOUD_CONNECTION_SUCCESS,
    CLOUD_CONNECTION_FAILED,
    CLOUD_FETCH_FAILED,
    CLOUD_STOP_RETRIES,
    SET_LOCAL_DATA_COUNT,
    CLOUD_RESET_TRIES,
    CLOUD_SAVE_REQUESTED,
    CLOUD_SAVE_COMPLETED,
    CLOUD_SAVE_FAILED
} from '../constants/actionTypes';


export const initialState = {
    isConnected: false,
    isConnectRetryOn: false,
    fetchAttempts: 0,
    connectionAttempts: 0,
    connectionError: '',
    saveFailed: false
}

/**
 * cloudReducer - cloudState
 *
 * contains cloud state.
 * 
 * @param  {Object} state - current 'cloud.cloudState' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const cloudState = (state=initialState, action) => {
    switch (action.type) {
        case CLOUD_CONNECTION_REQUESTED:
            return {
                ...state,
                isConnectRetryOn: true,
                connectionAttempts: action.reset ? 0 : state.connectionAttempts,
                connectionError: ''
            };
        case CLOUD_CONNECTION_SUCCESS:
            return {
                ...state,
                isConnected: true,
                isConnectRetryOn: false,
                connectionAttempts: 0,
                fetchAttempts: 0,
                connectionError: ''
            };
        case CLOUD_CONNECTION_FAILED:
            return {
                ...state,
                isConnected: false,
                connectionAttempts: state.connectionAttempts + 1,
                connectionError: action.error.message
            };
        case CLOUD_STOP_RETRIES:
            return {
                ...state,
                isConnectRetryOn: false,
            };
        case CLOUD_FETCH_FAILED:
            return {
                ...state,
                fetchAttempts: state.fetchAttempts + 1,
            };
        case CLOUD_RESET_TRIES:
            return {
                ...state,
                fetchAttempts: 0,
            };
        case SET_LOCAL_DATA_COUNT:
            return {
                ...state,
                localDataTotal: action.counts.countUsers+action.counts.countSessions+action.counts.countActivities,
                localDataCounts: action.counts,
            };
        case CLOUD_SAVE_REQUESTED:
            return {
                ...state,
                isSaving: true,
                saveFailed: false
            };
        case CLOUD_SAVE_COMPLETED:
            return {
                ...state,
                isSaving: false,
                saveFailed: false
            };
        case CLOUD_SAVE_FAILED:
            return {
                ...state,
                isSaving: false,
                saveFailed: true
            };
        default:
            return state;
    }
}


export default combineReducers({
    cloudState,
});

export const getCloudState = (state) => state.cloudState;
export const getLocalDataTotal = (state) => state.cloudState.localDataTotal;
export const getLocalDataCounts = (state) => state.cloudState.localDataCounts;
