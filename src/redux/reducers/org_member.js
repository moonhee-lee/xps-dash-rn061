import _ from 'lodash';
import { combineReducers } from 'redux';
import {
    FETCH_ORG_MEMBER_BY_ORG_FAILURE,
    FETCH_ORG_MEMBER_BY_ORG_REQUEST,
    FETCH_ORG_MEMBER_BY_ORG_SUCCESS,
    FETCH_ORG_MEMBER_BY_USER_ID_FAILURE,
    FETCH_ORG_MEMBER_BY_USER_ID_REQUEST,
    FETCH_ORG_MEMBER_BY_USER_ID_SUCCESS, REMOVE_LOCAL_ORG_MEMBER, UPDATE_LOCAL_USER
} from '../constants/actionTypes';


/**
 * reducers/org_member - ids
 *
 * contains all of the org_member ids that we know about in the system.
 * 
 * @param  {Object} state - current 'org_member.ids' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const ids = (state = [], action) => {
    switch (action.type) {
        case FETCH_ORG_MEMBER_BY_ORG_SUCCESS:
        case FETCH_ORG_MEMBER_BY_USER_ID_SUCCESS:
            return [...state, ...action.response.result];
        default:
            return state;
    }
}


/**
 * reducers/org_member - byIds
 *
 * contains all of the org_member objects that we know about.
 * these are the fully serialized objects that we retrieve from the API.
 * 
 * @param  {Object} state - current 'org_member.byIds' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const byIds = (state = {}, action) => {
    switch (action.type) {
        case FETCH_ORG_MEMBER_BY_ORG_SUCCESS:
        case FETCH_ORG_MEMBER_BY_USER_ID_SUCCESS:
            if (action.response) {
                return {
                    ...state,
                    ...action.response.entities.org_member,
                };
            }
            return state;

        case REMOVE_LOCAL_ORG_MEMBER:
            if(action.localID) {
                return _.omitBy(state, (o) => o.localID === action.localID);
            } else return state;

        default:
            return state;
    }
}


/**
 * reducers/user - isFetching
 *
 * simple reducer so that we always know if we are fetching an org_member or not
 * 
 * @param  {Object} state - current 'messages.isFetching' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const isFetching = (state = false, action) => {
    switch (action.type) {
        case FETCH_ORG_MEMBER_BY_ORG_REQUEST:
        case FETCH_ORG_MEMBER_BY_USER_ID_REQUEST:
            return true;
        case FETCH_ORG_MEMBER_BY_ORG_SUCCESS:
        case FETCH_ORG_MEMBER_BY_ORG_FAILURE:
        case FETCH_ORG_MEMBER_BY_USER_ID_SUCCESS:
        case FETCH_ORG_MEMBER_BY_USER_ID_FAILURE:
            return false;
    }
    return state;
}

// combine all of these reducers under the 'org_member' namespace.
export default combineReducers({
    ids,
    byIds,
    isFetching,
});


// some 'getters' for our current 'org_member' state.
export const getIds = (state) => state.ids;
export const getOrgMember = (state, id) => {
    if (id in state.byIds) return state.byIds[id];
    return null;
}

export const getOrgMemberByUserId = (state, id) => {
    return _.find(state.byIds,(o) => o.user.id===id);
}

export const getMembersByOrg = (state, orgId) => {
    return _.filter(state.byIds, (t) => t.org === orgId);
}

export const getMembersByUserId = (state, userId) => {
    return _.filter(state.byIds, (t) => t.user === userId);
}
