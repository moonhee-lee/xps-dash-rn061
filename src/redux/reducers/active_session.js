import { combineReducers } from 'redux';
import {
    SET_ACTIVE_SESSION,
    SET_ACTIVE_ACTIVITY,
} from "../constants/actionTypes";

/**
 * authReducer - activeSession
 *
 * contains our tab navigation state.
 * 
 * @param  {Object} state - current 'tabs.tabsInfo' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const activeSession = (state = {}, action) => {
    switch (action.type) {
        case SET_ACTIVE_SESSION:
            return {
                activeSession: action.session
            }
        default:
            return state;
    }
}

/**
 * authReducer - activeActivity
 *
 * contains our tab navigation state.
 *
 * @param  {Object} state - current 'tabs.tabsInfo' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const activeActivity = (state = {}, action) => {
    switch (action.type) {
        case SET_ACTIVE_ACTIVITY:
            return {
                activeActivity: action.activity
            }
        default:
            return state;
    }
}


export default combineReducers({
    activeSession,
    activeActivity,
});

export const getActiveSession = (state) => (state && state.activeSession)?state.activeSession.activeSession:null;
export const getActiveActivity = (state) => (state && state.activeActivity)?state.activeActivity.activeActivity:null;
