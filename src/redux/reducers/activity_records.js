import _ from 'lodash';
import { combineReducers } from 'redux';
import {
    FETCH_ACTIVITY_RECORD_REQUEST,
    FETCH_ACTIVITY_RECORD_SUCCESS,
    FETCH_ACTIVITY_RECORD_FAILURE,
    FETCH_ACTIVITY_RECORD_BY_USER_REQUEST,
    FETCH_ACTIVITY_RECORD_BY_USER_SUCCESS,
    FETCH_ACTIVITY_RECORD_BY_USER_FAILURE,
    SAVE_ACTIVITY_RECORD_REQUEST,
    ACTIVITY_RESET_RECORDING_MODE, REMOVE_LOCAL_ACTIVITY,
} from '../constants/actionTypes';


/**
 * reducers/activity_record - ids
 *
 * contains all of the activity_record ids that we know about in the system.
 *
 * @param  {Object} state - current 'activity_summaries.ids' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const ids = (state = [], action) => {
    switch (action.type) {
        case FETCH_ACTIVITY_RECORD_SUCCESS:
        case FETCH_ACTIVITY_RECORD_BY_USER_SUCCESS:
            return [...state, ...action.response.result];
        default:
            return state;
    }
}


/**
 * reducers/activity_record - byIds
 *
 * contains all of the activity_record objects that we know about.
 * these are the fully serialized objects that we retrieve from the API.
 *
 * @param  {Object} state - current 'activity_summaries.byIds' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const byIds = (state = {}, action) => {
    switch (action.type) {
        case FETCH_ACTIVITY_RECORD_SUCCESS:
        case FETCH_ACTIVITY_RECORD_BY_USER_SUCCESS:
            if (action.response) {
                return {
                    ...state,
                    ...action.response.entities.activity_record,
                };
            }
            return state;

        case REMOVE_LOCAL_ACTIVITY:
            if(action.localID) return _.omitBy(state, (activity) => activity.localID === action.localID);
            else return state;

        default:
            return state;
    }
}


/**
 * reducers/activity_record - isFetching
 *
 * simple reducer so that we always know if we are fetching a activity_record or not
 *
 * @param  {Object} state - current 'messages.isFetching' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const isFetching = (state = false, action) => {
    switch (action.type) {
        case FETCH_ACTIVITY_RECORD_REQUEST:
        case FETCH_ACTIVITY_RECORD_BY_USER_REQUEST:
            return true;
        case FETCH_ACTIVITY_RECORD_SUCCESS:
        case FETCH_ACTIVITY_RECORD_FAILURE:
        case FETCH_ACTIVITY_RECORD_BY_USER_SUCCESS:
        case FETCH_ACTIVITY_RECORD_BY_USER_FAILURE:
            return false;
    }
    return state;
}


export const isSaving = (state = false, action) => {
    switch (action.type) {
        case SAVE_ACTIVITY_RECORD_REQUEST:
            return true;
        case ACTIVITY_RESET_RECORDING_MODE:
            return false;
    }
    return state;
}

// combine all of these reducers under the 'activity_summaries' namespace.
export default combineReducers({
    ids,
    byIds,
    isFetching,
    isSaving,
});


// some 'getters' for our current 'activity_summaries' state.
export const getIds = (state) => state.ids;

export const getActivityRecord = (state, id) => {
    if (id in state.byIds) return state.byIds[id];
    return null;
}

export const getActivityRecords = (state, activityId) => {
    return _.filter(state.byIds, (s) => s.activity === activityId);
}

export const getActivityRecordsByType = (state, type_hash) => {
    return _.filter(state.byIds, (s) => s.type_hash === type_hash);
}

const isEqualActivityType = (a, b) => {
    if (b.type_definition.activity_type === 'jump') {
        let matchedRequiredFields = a.type_definition.activity_type === b.type_definition.activity_type && a.type_definition.orientation === b.type_definition.orientation &&
            a.type_definition.side === b.type_definition.side;
        let matchedOptionalFields = !b.type_definition.triple_jump_type || a.type_definition.triple_jump_type === b.type_definition.triple_jump_type;
        return matchedRequiredFields && matchedOptionalFields;
    } else if (b.type_definition.activity_type === 'sprint') {
        let matchedRequiredFields = (a.type_definition.activity_type === b.type_definition.activity_type && a.type_definition.start_type === b.type_definition.start_type && a.type_definition.distance === b.type_definition.distance &&
            a.type_definition.side === b.type_definition.side &&
            a.type_definition.units === b.type_definition.units);

        let matchedOptionalFields = true;
        if (global.enableSkatingMode === true) {
            matchedOptionalFields = !b.type_definition.skating || a.type_definition.skating === b.type_definition.skating;
        } else if (global.enableBobsledMode === true) {
            matchedOptionalFields = !b.type_definition.bobsled === null || a.type_definition.bobsled === b.type_definition.bobsled;
        } else if (global.enableWalkingMode === true) {
            matchedOptionalFields = !b.type_definition.walking === null || a.type_definition.walking === b.type_definition.walking;
        }
        return matchedRequiredFields && matchedOptionalFields;
    } else {
        return (a.type_definition.activity_type === b.type_definition.activity_type && a.type_definition.start_type === b.type_definition.start_type && a.type_definition.distance === b.type_definition.distance &&
            a.type_definition.units === b.type_definition.units);
    }
}

export const getActivityRecordsBySessionIdAndType = (state, sessionId, type_hash) => {
    let localActivities = [];
    let activities = _.filter(state.byIds, (s) => (s.type_hash === type_hash && parseInt(s.session)===parseInt(sessionId)));
    let cloudActivities = _.filter(state.byIds, (s) => (s.type_hash && s.type_hash === type_hash && parseInt(s.session)===parseInt(sessionId)));
    if(activities.length>0) {
        localActivities = _.filter(state.byIds, (s) => !s.type_hash && parseInt(s.session)===parseInt(sessionId) && isEqualActivityType(s, activities[0]));
    }
    activities = _.concat(cloudActivities,localActivities);
    activities = _.uniqBy(activities,(a) => a.id);
    return activities;
}

export const getActivityRecordsBySessionIdAndActivity = (state, sessionId, activity) => {
    let activities = _.filter(state.byIds, (s) => (s.type_hash && s.type_hash === activity.type_hash && parseInt(s.session)===parseInt(sessionId)));
    let localActivities = _.filter(state.byIds, (s) => !s.type_hash && parseInt(s.session)===parseInt(sessionId) && isEqualActivityType(s, activity));
    activities = _.concat(activities,localActivities);
    activities = _.uniqBy(activities,(a) => a.id);
    return activities;
}

export const getActivityRecordsByUser = (state, userId) => {
    return _.filter(state.byIds, (s) => s.user === userId);
}

export const getActivityRecordById = (state, recordId) => {
    return _.filter(state.byIds, (s) => s.id === recordId);
}

export const getActivityRecordsIsFetching = (state) => state.isFetching;
export const getActivityRecordsIsSaving = (state) => state.isSaving;
