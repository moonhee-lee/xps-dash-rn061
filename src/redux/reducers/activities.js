import _ from 'lodash';
import { combineReducers } from 'redux';
import {
    FETCH_ACTIVITY_REQUEST,
    FETCH_ACTIVITY_SUCCESS,
    FETCH_ACTIVITY_FAILURE,
    FETCH_ACTIVITY_BY_USER_REQUEST,
    FETCH_ACTIVITY_BY_USER_SUCCESS,
    FETCH_ACTIVITY_BY_USER_FAILURE,
    SAVE_ACTIVITY_REQUEST,
    SAVE_ACTIVITY_SUCCESS,
    REMOVE_LOCAL_ACTIVITY,
} from '../constants/actionTypes';

/**
 * reducers/activity - ids
 *
 * contains all of the activity ids that we know about in the system.
 * 
 * @param  {Object} state - current 'activities.ids' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const ids = (state = [], action) => {
    switch (action.type) {
        case FETCH_ACTIVITY_SUCCESS:
        case FETCH_ACTIVITY_BY_USER_SUCCESS:
        case SAVE_ACTIVITY_SUCCESS:
            if (action.response) {
                return [...state, ...action.response.result];
            }
            return state;
        default:
            return state;
    }
}


/**
 * reducers/activity - byIds
 *
 * contains all of the activity objects that we know about.
 * these are the fully serialized objects that we retreive from the API.
 * 
 * @param  {Object} state - current 'activities.byIds' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const byIds = (state = {}, action) => {
    switch (action.type) {
        case FETCH_ACTIVITY_SUCCESS:
        case FETCH_ACTIVITY_BY_USER_SUCCESS:
        case SAVE_ACTIVITY_SUCCESS:
            if (action.response) {
                return {
                    ...state,
                    ...action.response.entities.activity,
                };
            }
            return state;

        case REMOVE_LOCAL_ACTIVITY:
            if(action.localID) return _.omitBy(state, (activity) => activity.localID === action.localID);
            else return state;

        default:
            return state;
    }
}


/**
 * reducers/activity - isFetching
 *
 * simple reducer so that we always know if we are fetching a activity or not
 * 
 * @param  {Object} state - current 'messages.isFetching' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const isFetching = (state = false, action) => {
    switch (action.type) {
        case FETCH_ACTIVITY_REQUEST:
        case FETCH_ACTIVITY_BY_USER_REQUEST:
            return true;
        case FETCH_ACTIVITY_SUCCESS:
        case FETCH_ACTIVITY_FAILURE:
        case FETCH_ACTIVITY_BY_USER_SUCCESS:
        case FETCH_ACTIVITY_BY_USER_FAILURE:
            return false;
    }
    return state;
}

export const isSaving = (state = false, action) => {
    switch (action.type) {
        case SAVE_ACTIVITY_REQUEST:
            return true;
        case SAVE_ACTIVITY_SUCCESS:
            return false;
    }
    return state;
}

// combine all of these reducers under the 'activities' namespace.
export default combineReducers({
    ids,
    byIds,
    isFetching,
    isSaving,
});


// some 'getters' for our current 'activities' state.
export const getIds = (state) => state.ids;
export const getActivity = (state, id) => {
    if (id in state.byIds) return state.byIds[id];
    return null;
}
export const getActivities = (state) => {
    return state.byIds;
}
export const getActivitiesBySession = (state, sessionId) => {
    const activities = _.filter(state.byIds, (s) => parseInt(s.session) === parseInt(sessionId));
    return activities;//_.filter(activities,(a) => a.type_definition.activity_type!=='freeform');
}
export const getActivitiesForSessions = (state, sessions) => {
    const activities = [];
    _.each(sessions,(s) => {
        const _activities = _.filter(state.byIds, (a) => parseInt(a.session) === parseInt(s.id));
        if(_activities) _.each(_activities,(a) => activities.push(a));
    })
    return activities;
}
export const getActivitiesByUserId = (state, userId) => {
    return _.filter(state.byIds, (s) => s.user === userId);
}
export const getActivitiesByUser = (state, userId) => {
    const userActivities = _.filter(state.byIds, function(item) {
        return _.some(item.activity_users, {user:userId});
    });
    return userActivities.sort((activityA, activityB) => {return new Date(activityB.id) - new Date(activityA.id)});
}
export const getLocalActivities = (state) => {
    return _.filter(state.byIds,(a) => a.localID && !a.type_hash);
}
export const getActivityIsFetching = (state) => state.isFetching;
export const getActivityIsSaving = (state) => state.isSaving;
