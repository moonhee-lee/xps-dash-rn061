import { combineReducers } from 'redux';
import {
    SET_AUTH,
    LOGIN_SUCCESS,
    LOGIN_FAILURE,
    LOGOUT_SUCCESS,
} from '../constants/actionTypes';


/**
 * authReducer - authInfo
 *
 * contains our entire auth state.
 *
 * @param  {Object} state - current 'auth.authInfo' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const authInfo = (state = {}, action) => {
    switch (action.type) {
        case SET_AUTH:
            return {
                loggedIn: true,
                userId: action.userId,
                user: action.user,
                authToken: action.authToken
            }
        case LOGIN_SUCCESS:
            return {
                loggedIn: true,
                userId: action.response.result,
                authToken: action.authToken,
            };
        case LOGIN_FAILURE:
        case LOGOUT_SUCCESS:
            return {
                loggedIn: false,
                userId: null,
                authToken: null,
            };
        default:
            return state;
    }
}


export default combineReducers({
    authInfo,
});

export const getAuthState = (state) => state.authInfo;
export const getLoggedInUser = (state) => state.authInfo.user;
export const getLoggedInUserId = (state) => state.authInfo.userId;
