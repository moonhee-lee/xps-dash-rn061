import _ from 'lodash';
import { combineReducers } from 'redux';
import {
    FETCH_ACTIVITY_DATA_REQUEST,
    FETCH_ACTIVITY_DATA_SUCCESS,
    FETCH_ACTIVITY_DATA_FAILURE,
} from '../constants/actionTypes';


/**
 * reducers/activity_data - ids
 *
 * contains all of the activity_data ids that we know about in the system.
 *
 * @param  {Object} state - current 'activity_data.ids' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const ids = (state = [], action) => {
    switch (action.type) {
        case FETCH_ACTIVITY_DATA_SUCCESS:
            if (action.response) {
                return {
                    ...state,
                    ...action.response.entities.activity_data,
                };
            }
            return state;
        default:
            return state;
    }
}


/**
 * reducers/activity_data - byIds
 *
 * contains all of the activity_data objects that we know about.
 * these are the fully serialized objects that we retrieve from the API.
 *
 * @param  {Object} state - current 'activity_data.byIds' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const byIds = (state = {}, action) => {
    switch (action.type) {
        case FETCH_ACTIVITY_DATA_SUCCESS:
            if (action.response) {
                return {
                    ...state,
                    ...action.response.entities.activity_data,
                };
            }
            return state;
        default:
            return state;
    }
}


/**
 * reducers/activity_data - isFetching
 *
 * simple reducer so that we always know if we are fetching a activity_data or not
 *
 * @param  {Object} state - current 'messages.isFetching' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const isFetching = (state = false, action) => {
    switch (action.type) {
        case FETCH_ACTIVITY_DATA_REQUEST:
            return true;
        case FETCH_ACTIVITY_DATA_SUCCESS:
        case FETCH_ACTIVITY_DATA_FAILURE:
            return false;
    }
    return state;
}
// combine all of these reducers under the 'activity_data' namespace.
export default combineReducers({
    ids,
    byIds,
    isFetching,
});


// some 'getters' for our current 'activity_data' state.
export const getIds = (state) => state.ids;
export const getActivityDataById = (state, recordId) => {
    return _.find(state.byIds, (s) => s.activity === recordId);
}
export const getActivityData = (state) => {
    return state.byIds;
}
export const getActivityDataIsFetching = (state) => state.isFetching;
