import _ from 'lodash';
import { combineReducers } from 'redux';
import {
    FETCH_ACTIVITY_SUMMARY_REQUEST,
    FETCH_ACTIVITY_SUMMARY_SUCCESS,
    FETCH_ACTIVITY_SUMMARY_FAILURE,
    FETCH_ACTIVITY_SUCCESS,
} from '../constants/actionTypes';


/**
 * reducers/activity_summary - ids
 *
 * contains all of the activity_summary ids that we know about in the system.
 * 
 * @param  {Object} state - current 'activity_summaries.ids' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const ids = (state = [], action) => {
    switch (action.type) {
        case FETCH_ACTIVITY_SUMMARY_SUCCESS:
        case FETCH_ACTIVITY_SUCCESS:
            return [...state, ...action.response.result];
        default:
            return state;
    }
}


/**
 * reducers/activity_summary - byIds
 *
 * contains all of the activity_summary objects that we know about.
 * these are the fully serialized objects that we retreive from the API.
 * 
 * @param  {Object} state - current 'activity_summaries.byIds' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const byIds = (state = {}, action) => {
    switch (action.type) {
        case FETCH_ACTIVITY_SUMMARY_SUCCESS:
        case FETCH_ACTIVITY_SUCCESS:
            if (action.response) {
                return {
                    ...state,
                    ...action.response.entities.activity_summary,
                };
            }
            return state;
        default:
            return state;
    }
}


/**
 * reducers/activity_summary - isFetching
 *
 * simple reducer so that we always know if we are fetching a activity_summary or not
 * 
 * @param  {Object} state - current 'messages.isFetching' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const isFetching = (state = false, action) => {
    switch (action.type) {
        case FETCH_ACTIVITY_SUMMARY_REQUEST:
            return true;
        case FETCH_ACTIVITY_SUMMARY_SUCCESS:
        case FETCH_ACTIVITY_SUMMARY_FAILURE:
            return false;
    }
    return state;
}

// combine all of these reducers under the 'activity_summaries' namespace.
export default combineReducers({
    ids,
    byIds,
    isFetching,
});


// some 'getters' for our current 'activity_summaries' state.
export const getIds = (state) => state.ids;
export const getActivitySummary = (state, id) => {
    if (id in state.byIds) return state.byIds[id];
    return null;
}
export const getActivitySummariesByActivity = (state, activityId) => {
    return _.filter(state.byIds, (s) => s.activity === activityId);
}
export const getActivitySummariesIsFetching = (state) => state.isFetching;
