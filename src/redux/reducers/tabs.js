import { combineReducers } from 'redux';
import {
    SAVE_ACTIVE_TAB,
} from "../constants/actionTypes";

/**
 * authReducer - tabsInfo
 *
 * contains our tab navigation state.
 * 
 * @param  {Object} state - current 'tabs.tabsInfo' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const tabsInfo = (state = {}, action) => {
    switch (action.type) {
        case SAVE_ACTIVE_TAB:
            return {
                currentTab: action.currentTab
            }
        default:
            return state;
    }
}


export default combineReducers({
    tabsInfo,
});

export const getCurrentTab = (state) => state.tabsInfo.currentTab;