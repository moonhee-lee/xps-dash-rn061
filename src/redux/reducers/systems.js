import _ from 'lodash';
import { combineReducers } from 'redux';

import {
    SYSTEM_CONNECTING,
    SYSTEM_CONNECTED,
    SYSTEM_UPDATED,
    SYSTEM_DISCONNECTING,
    SYSTEM_DISCONNECTED,
    SYSTEM_STOP_RETRIES,
    SYSTEM_FETCH_SUCCESS,
    SYSTEM_MODE_SELECT_CONFIRMED,
    SYSTEM_MODE_SELECT_FAILED,
    APP_STATE_CHANGE,
    SYSTEM_FIRMWARE_UPDATE_SENDING,
    SYSTEM_FIRMWARE_UPDATE_COMPLETE,
    SYSTEM_FIRMWARE_UPDATE_FAILED
} from '../../redux/constants/actionTypes';
import {
    SYSTEM_STATE_CONNECTING,
    SYSTEM_STATE_CONNECTED,
    SYSTEM_STATE_DISCONNECTING,
    SYSTEM_STATE_DISCONNECTED,
} from '../../redux/constants/systemStates';

// TODO
export const ids = (state = [], action) => {
    switch (action.type) {
        default:
            return state;
    }
}

export const byIds = (state = {}, action) => {
    switch (action.type) {
        case SYSTEM_FETCH_SUCCESS:  // TODO...
            return {
                ...state,
                ...action.system,
            };

        case SYSTEM_CONNECTING:
            return _.mapValues(state, (system) => {
                if (system.id === action.systemId) {
                    return {
                        ...system,
                        state: SYSTEM_STATE_CONNECTING,
                        isConnectRetryOn: true,
                        lastState: null
                    };
                } else {
                    return system;
                }
            });

        case SYSTEM_CONNECTED:
            return _.mapValues(state, (system) => {
                if(system.id === action.systemId) {
                    return {
                        ...system,
                        state: SYSTEM_STATE_CONNECTED,
                        isConnectRetryOn: false,
                        lastState: null
                    };
                } else {
                    return system;
                }
            });

        case SYSTEM_UPDATED:
            return _.mapValues(state, (system) => {
                if(system.id) {
                    if (action.hub_config) {
                        return {
                            ...system,
                            hub_config: action.hub_config,
                        };
                    }
                    if (action.selected_tags) {
                        return {
                            ...system,
                            selected_tags: action.selected_tags,
                        };
                    }
                    if (action.sync_timing) {
                        return {
                            ...system,
                            ...action.sync_timing,
                        };
                    }
                    if (action.offset_data) {
                        return {
                            ...system,
                            offset_data: action.offset_data,
                        };
                    }
                }
                return system;
            });

        case SYSTEM_DISCONNECTING:
            return _.mapValues(state, (system) => {
                if (system.id === action.systemId) {
                    return {
                        ...system,
                        state: SYSTEM_STATE_DISCONNECTING,
                        syncStarted: false,
                        lastState: action.lastState,
                        offset_data: null
                    }
                } else {
                    return system;
                }
            });

        case SYSTEM_DISCONNECTED:
            return _.mapValues(state, (system) => {
                if(system.id === action.systemId) {
                    return {
                        ...system,
                        state: SYSTEM_STATE_DISCONNECTED,
                        offset_data: null
                    };
                } else {
                    return system;
                }
            });

        case SYSTEM_STOP_RETRIES:
            return _.mapValues(state, (system) => {
                if(system.id === action.systemId) {
                    return {
                        ...system,
                        isConnectRetryOn: false,
                    };
                } else {
                    return system;
                }
            });

        case SYSTEM_MODE_SELECT_CONFIRMED:
            return _.mapValues(state, (system) => {
                return {
                    ...system,
                    mode: action.mode
                };
            });

        case SYSTEM_MODE_SELECT_FAILED:
            return _.mapValues(state, (system) => {
                return {
                    ...system,
                    mode: 0
                };
            });

        case APP_STATE_CHANGE:
            return _.mapValues(state, (system) => {
                return {
                    ...system,
                    appState: action.state
                };
            });

        case SYSTEM_FIRMWARE_UPDATE_COMPLETE:
        case SYSTEM_FIRMWARE_UPDATE_FAILED:
            return _.mapValues(state, (system) => {
                return {
                    ...system,
                    firmwareVersion: true
                };
            });

        case SYSTEM_FIRMWARE_UPDATE_SENDING:
            return _.mapValues(state, (system) => {
                return {
                    ...system,
                    firmwareVersion: null
                };
            });

        default:
            return state;
    }
}

export const isFirmWareUpdating = (state = false, action) => {
    switch (action.type) {
        case SYSTEM_FIRMWARE_UPDATE_SENDING:
            return true;
        case SYSTEM_FIRMWARE_UPDATE_COMPLETE:
        case SYSTEM_FIRMWARE_UPDATE_FAILED:
            return false;
    }
    return state;
}

export default combineReducers({
    ids,
    byIds,
    isFirmWareUpdating
});

export const getIds = (state) => state.ids;
export const getSystem = (state, id) => {
    if (id in state.byIds) return state.byIds[id];
    return null;
}

export const getIsFirmWareUpdating = (state) => state.isFirmWareUpdating;

