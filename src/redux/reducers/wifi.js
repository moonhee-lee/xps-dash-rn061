import { combineReducers } from 'redux';
import {
    WIFI_CONNECTION_REQUESTED,
    WIFI_CONNECTION_SUCCESS,
    WIFI_CONNECTION_FAILED,
    WIFI_FETCH_FAILED,
    WIFI_STOP_RETRIES,
    WIFI_DISCONNECT_SUCCESS,
    WIFI_RESET_RETRIES
} from '../constants/actionTypes';


export const initialState = {
    deviceIP: '',
    deviceSSID: '',
    isConnected: false,
    isConnectRetryOn: false,
    fetchAttempts: 0,
    connectionAttempts: 0,
    connectionError: '',
}

/**
 * wifiReducer - wifiState
 *
 * contains wifi state.
 * 
 * @param  {Object} state - current 'wifi.wifiState' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const wifiState = (state=initialState, action) => {
    switch (action.type) {
        case WIFI_CONNECTION_REQUESTED:
            return {
                ...state,
                deviceSSID: action.deviceSSID,
                isConnected: false,
                isConnecting: true,
                isConnectRetryOn: true,
                connectionAttempts: action.reset ? 0 : state.connectionAttempts,
                connectionError: ''
            };
        case WIFI_CONNECTION_SUCCESS:
            return {
                ...state,
                deviceIP: action.IPAddress,
                deviceSSID: action.deviceSSID,
                isConnected: true,
                isConnecting: false,
                isConnectRetryOn: false,
                connectionAttempts: 0,
                fetchAttempts: 0,
                connectionError: ''
            };
        case WIFI_DISCONNECT_SUCCESS:
            return {
                ...state,
                deviceSSID: '',
                isConnected: false,
                isConnecting: false,
                connectionAttempts: 0,
                isConnectRetryOn: false,
            };
        case WIFI_CONNECTION_FAILED:
            return {
                ...state,
                isConnected: false,
                isConnecting: false,
                connectionAttempts: state.connectionAttempts + 1,
                connectionError: action.error.message
            };
        case WIFI_STOP_RETRIES:
            return {
                ...state,
                isConnecting: false,
                connectionAttempts: 0,
                isConnectRetryOn: false,
            };
        case WIFI_RESET_RETRIES:
            return {
                ...state,
                isConnecting: false,
                connectionAttempts: 0,
                fetchAttempts: 0,
                isConnectRetryOn: false,
            };
        case WIFI_FETCH_FAILED:
            return {
                ...state,
                deviceSSID: action.deviceSSID,
                fetchAttempts: state.fetchAttempts + 1,
            };
        default:
            return state;
    }
}


export default combineReducers({
    wifiState,
});

export const getWifiState = (state) => state.wifiState;
