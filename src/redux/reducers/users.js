import _ from 'lodash';
import { combineReducers } from 'redux';
import {
    LOGIN_SUCCESS,
    FETCH_USER_REQUEST,
    FETCH_USER_SUCCESS,
    FETCH_USER_FAILURE,
    FETCH_USER_BY_TEAM_REQUEST,
    FETCH_USER_BY_TEAM_SUCCESS,
    FETCH_USER_BY_TEAM_FAILURE,
    FETCH_ACTIVITY_USER_SUCCESS,
    FETCH_ORG_MEMBER_BY_ORG_SUCCESS,
    SAVE_ACTIVITY_USER_SUCCESS,
    FETCH_ACTIVE_USER_SUCCESS,
    SAVE_ACTIVE_USER_SUCCESS,
    SAVE_USER_REQUEST,
    SAVE_USER_SUCCESS,
    SAVE_USER_FAILURE,
    FETCH_ACTIVITY_SUCCESS,
    FETCH_ACTIVITY_SUMMARY_SUCCESS,
    SAVE_ORG_MEMBER_SUCCESS,
    SAVE_USER_IMAGE_SUCCESS,
    SAVE_LOCAL_USER_IMAGE,
    SET_USER_IMAGE,
    REMOVE_LOCAL_ORG_MEMBER,
    UPDATE_LOCAL_USER,
} from '../constants/actionTypes';


/**
 * reducers/user - ids
 *
 * contains all of the user ids that we know about in the system.
 * 
 * @param  {Object} state - current 'users.ids' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const ids = (state = [], action) => {
    switch (action.type) {
        case LOGIN_SUCCESS:
        case FETCH_USER_SUCCESS:
        case FETCH_USER_BY_TEAM_SUCCESS:
        case FETCH_ACTIVITY_SUCCESS:
        case FETCH_ACTIVITY_SUMMARY_SUCCESS:
            return [...state, action.response.result];
        default:
            return state;
    }
}


/**
 * reducers/user - byIds
 *
 * contains all of the user objects that we know about.
 * these are the fully serialized objects that we retreive from the API.
 * 
 * @param  {Object} state - current 'users.byIds' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const byIds = (state = {}, action) => {
    switch (action.type) {
        case LOGIN_SUCCESS:
        case FETCH_USER_SUCCESS:
        case FETCH_USER_BY_TEAM_SUCCESS:
        case FETCH_ACTIVITY_USER_SUCCESS:
        case SAVE_ACTIVITY_USER_SUCCESS:
        case FETCH_ACTIVE_USER_SUCCESS:
        case SAVE_ACTIVE_USER_SUCCESS:
        case SAVE_USER_SUCCESS:
        case SAVE_USER_IMAGE_SUCCESS:
        case FETCH_ACTIVITY_SUCCESS:
        case FETCH_ACTIVITY_SUMMARY_SUCCESS:
        case SAVE_ORG_MEMBER_SUCCESS:
        case FETCH_ORG_MEMBER_BY_ORG_SUCCESS:
            if (action.response) {
                return {
                    ...state,
                    ...action.response.entities.user,
                };
            }
            return state;

        case REMOVE_LOCAL_ORG_MEMBER:
            if(action.localID) {
                return _.omitBy(state, (user,index) => index === action.tempID);
            } else return state;

        case SAVE_LOCAL_USER_IMAGE:
            if(action.imageKey) {
                return _.mapValues(state, (user) => {
                    if (user.id === action.userId) {
                        return {
                            ...user,
                            local_image: action.imageKey,
                        }
                    }
                    return user;
                });
            }
            return state;
        case SET_USER_IMAGE:
            if(action.imageKey) {
                return _.mapValues(state, (user) => {
                    if (user.id === action.userId) {
                        return {
                            ...user,
                            user_image: action.imageKey,
                        }
                    }
                    return user;
                });
            }
            return state;
        default:
            return state;
    }
}


/**
 * reducers/user - isFetching
 *
 * simple reducer so that we always know if we are fetching a user or not
 * 
 * @param  {Object} state - current 'messages.isFetching' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const isFetching = (state = false, action) => {
    switch (action.type) {
        case FETCH_USER_REQUEST:
            return true;
        case FETCH_USER_SUCCESS:
        case FETCH_USER_FAILURE:
            return false;
    }
    return state;
}

// combine all of these reducers under the 'users' namespace.
export default combineReducers({
    ids,
    byIds,
    isFetching,
});


// some 'getters' for our current 'users' state.
export const getIds = (state) => state.ids;
export const getUser = (state, id) => {
    if (id in state.byIds) return state.byIds[id];
    return null;
}
export const getUserIsFetching = (state) => state.isFetching;
