import _ from 'lodash';
import { combineReducers } from 'redux';
import {
    FETCH_ACTIVITY_USER_REQUEST,
    FETCH_ACTIVITY_USER_SUCCESS,
    FETCH_ACTIVITY_USER_FAILURE,
    SAVE_ACTIVITY_USER_SUCCESS,
    DELETE_ACTIVITY_USER_SUCCESS,
    SYSTEM_SPRINT_BEEP_START,
    SYSTEM_SPRINT_MOTION_DISTANCE,
    SYSTEM_SPRINT_ENDED,
    SYSTEM_JUMP_DETECTED,
    SYSTEM_COD_DETECTED,
    ACTIVITY_RESET_RECORDING_MODE,
    ACTIVITY_RESET_LIVE_RECORDING_MODE,
    ACTIVITY_TOGGLE_RECORDING_MODE,
    SET_ACTIVITY_USER_VELOCITY,
    SET_ACTIVITY_USER_VERTICAL,
    SET_ACTIVITY_USER_TAG
} from '../constants/actionTypes';


/**
 * reducers/activity_user - ids
 *
 * contains all of the activity_user ids that we know about in the system.
 * 
 * @param  {Object} state - current 'activity_summaries.ids' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const ids = (state = [], action) => {
    switch (action.type) {
        case FETCH_ACTIVITY_USER_SUCCESS:
        case SAVE_ACTIVITY_USER_SUCCESS:
            return [...state, ...action.response.result];
        case DELETE_ACTIVITY_USER_SUCCESS:
            return _.filter(state, (id) => id !== action.activityUserId);
        default:
            return state;
    }
}


/**
 * reducers/activity_user - byIds
 *
 * contains all of the activity_user objects that we know about.
 * these are the fully serialized objects that we retreive from the API.
 * 
 * @param  {Object} state - current 'activity_summaries.byIds' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const byIds = (state = {}, action) => {
    switch (action.type) {
        case FETCH_ACTIVITY_USER_SUCCESS:
        case SAVE_ACTIVITY_USER_SUCCESS:
            if (action.response) {
                return {
                    ...state,
                    ...action.response.entities.activity_user,
                };
            }
            return state;

        case DELETE_ACTIVITY_USER_SUCCESS:
            return _.pickBy(state, (item) => item.id !== action.activityUserId);

        case SYSTEM_SPRINT_BEEP_START:
            return _.mapValues(state, (activityUser) => {
                if (activityUser.activity === action.activityId) {
                    return {
                        ...activityUser,
                        beepStartTime: action.beepStartTime
                    }
                } else {
                    return activityUser;
                }
            });

        case SYSTEM_SPRINT_MOTION_DISTANCE:
            return _.mapValues(state, (activityUser) => {
                if (activityUser.id === action.activityUserId) {
                    return {
                        ...activityUser,
                        distance: action.currentDistance
                    }
                } else {
                    return activityUser;
                }
            });

        case SET_ACTIVITY_USER_VELOCITY:
            return _.mapValues(state, (activityUser) => {
                if (parseInt(activityUser.tag.name) === parseInt(action.tag.name)) {
                    return {
                        ...activityUser,
                        velocity: action.velocity
                    }
                } else {
                    return activityUser;
                }
            });

        case SET_ACTIVITY_USER_TAG:
            return _.mapValues(state, (activityUser) => {
                if(action.activityUser.id===activityUser.id) {
                    return {
                        ...activityUser,
                        tag: action.tag
                    }
                }
                return activityUser;
            });

        case SET_ACTIVITY_USER_VERTICAL:
            return _.mapValues(state, (activityUser) => {
                if (activityUser.tag === action.tag.name) {
                    return {
                        ...activityUser,
                        vertical: action.vertical
                    }
                } else {
                    return activityUser;
                }
            });

        case SYSTEM_SPRINT_ENDED:
            return _.mapValues(state, (activityUser) => {
                if (activityUser.id === action.activityUserId) {
                    const finish_time = action.sprintData.end - action.sprintData.start;
                    const reaction_time = activityUser.beepStartTime ? action.sprintData.start - (activityUser.beepStartTime/1000) : null;
                    const total_time = finish_time+((!!reaction_time && reaction_time>0)?reaction_time:0);

                    let max_velocity = null, max_acceleration = null;
                    if(action.splitData && action.splitData.velocities && action.splitData.velocities.length>0) {
                        max_velocity = _.max(action.splitData.velocities);
                    }
                    if(action.splitData && action.splitData.accelerations && action.splitData.accelerations.length>0) {
                        max_acceleration = _.max(action.splitData.accelerations);
                    }

                    return {
                        ...activityUser,
                        meta_data: {
                            ...action.sprintData,
                            finish_time,
                            reaction_time,
                            total_time,
                            max_velocity,
                            max_acceleration,
                            split_data: action.splitData
                        },
                        score: total_time,
                        completed: true
                    }
                } else {
                    return activityUser;
                }
            });

        case SYSTEM_JUMP_DETECTED:
            return _.mapValues(state, (activityUser) => {
                if (parseInt(activityUser.id) === parseInt(action.activityUserId) && activityUser.tag && parseInt(activityUser.tag.name) === parseInt(action.tagName)) {
                    return {
                        ...activityUser,
                        meta_data: action.jumpData,
                        completed: true,
                        score: action.jumpData.height > action.jumpData.length ? action.jumpData.height : action.jumpData.length,
                    }
                } else {
                    return activityUser;
                }
            });

        case SYSTEM_COD_DETECTED:
            return _.mapValues(state, (activityUser) => {
                if (parseInt(activityUser.id) === parseInt(action.activityUserId) && activityUser.tag && parseInt(activityUser.tag.name) === parseInt(action.tagName)) {
                    return {
                        ...activityUser,
                        cod_data: action.CODData,
                    }
                } else {
                    return activityUser;
                }
            });

        case ACTIVITY_RESET_RECORDING_MODE:
            return _.mapValues(state, (activityUser) => {
                if (activityUser.activity === action.activityId) {
                    return {
                        ...activityUser,
                        meta_data: {},
                        completed: false,
                        distance: null,
                    }
                }
                return activityUser;
            });

        default:
            return state;
    }
}


/**
 * reducers/activity_user - isFetching
 *
 * simple reducer so that we always know if we are fetching a activity_user or not
 * 
 * @param  {Object} state - current 'messages.isFetching' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const isFetching = (state = false, action) => {
    switch (action.type) {
        case FETCH_ACTIVITY_USER_REQUEST:
            return true;
        case FETCH_ACTIVITY_USER_SUCCESS:
        case FETCH_ACTIVITY_USER_FAILURE:
            return false;
    }
    return state;
}

// combine all of these reducers under the 'activity_summaries' namespace.
export default combineReducers({
    ids,
    byIds,
    isFetching,
});


// some 'getters' for our current 'activity_summaries' state.
export const getIds = (state) => state.ids;
export const getActivityUser = (state, id) => {
    if (id in state.byIds) return state.byIds[id];
    return null;
}


export const getActivityUsersByActivity = (state, activityId) => {
    return _.filter(state.byIds, (s) => s.activity === activityId);
}

export const getActivityUsersIsFetching = (state) => state.isFetching;
