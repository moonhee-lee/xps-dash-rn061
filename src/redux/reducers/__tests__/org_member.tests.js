import * as orgMemberReducer from '../../redux/reducers/org_member';
import {
    FETCH_ORG_MEMBER_BY_ORG_FAILURE,
    FETCH_ORG_MEMBER_BY_ORG_REQUEST,
    FETCH_ORG_MEMBER_BY_ORG_SUCCESS,
} from '../../redux/constants/actionTypes';


describe('orgMemberReducer suite', () => {

    describe('ids reducer', () => {

        it('adds new ids on FETCH_ORG_MEMBER_BY_ORG_SUCCESS', () => {
            const result = [1, 2, 3];
            const action = {
                type: FETCH_ORG_MEMBER_BY_ORG_SUCCESS,
                response: {
                    result
                }
            };
            expect(orgMemberReducer.ids([], action)).toEqual(result);
        });

        it('returns initial state on unknown', () => {
            const action = {
                type: 'unknown',
            };
            expect(orgMemberReducer.ids([], action)).toEqual([]);
        });

    });

    describe('byIds reducer', () => {
        let initialState = {};

        beforeEach(() => {
            initialState = {
                1: {
                    id: 1,
                    user: 1,
                }
            };
        });

        it('adds new org_member on FETCH_ORG_MEMBER_BY_ORG_SUCCESS', () => {
            const org_members = [
                {id: 1, user: 1},
                {id: 2, user: 1},
            ];
            const action = {
                type: FETCH_ORG_MEMBER_BY_ORG_SUCCESS,
                response: {
                    entities: {
                        org_member: org_members
                    }
                }
            };
            const expectedState = {
                ...initialState,
                ...org_members,
            };
            expect(orgMemberReducer.byIds(initialState, action)).toEqual(expectedState);
        });

        it('returns initialState on FETCH_ORG_MEMBER_BY_ORG_SUCCESS with no response', () => {
            const action = {
                type: FETCH_ORG_MEMBER_BY_ORG_SUCCESS,
            };
            expect(orgMemberReducer.byIds(initialState, action)).toEqual(initialState);
        });

        it('returns initialState on unknown', () => {
            const action = {
                type: 'unknown',
            };
            expect(orgMemberReducer.byIds(initialState, action)).toEqual(initialState);
        });
    });

    describe('isFetching reducer', () => {
        it('returns true on FETCH_ORG_MEMBER_BY_ORG_REQUEST', () => {
            const action = { type: FETCH_ORG_MEMBER_BY_ORG_REQUEST };
            expect(orgMemberReducer.isFetching(false, action)).toBe(true);
        });
        it('returns false on FETCH_ORG_MEMBER_BY_ORG_SUCCESS', () => {
            const action = { type: FETCH_ORG_MEMBER_BY_ORG_SUCCESS };
            expect(orgMemberReducer.isFetching(true, action)).toBe(false);
        });
        it('returns false on FETCH_ORG_MEMBER_BY_ORG_FAILURE', () => {
            const action = { type: FETCH_ORG_MEMBER_BY_ORG_FAILURE };
            expect(orgMemberReducer.isFetching(true, action)).toBe(false);
        });
    });

    describe('getters', () => {
        it('returns ids when calling getIds', () => {
            const ids = [1, 2, 3];
            expect(orgMemberReducer.getIds({ ids })).toEqual(ids);
        });

        it('returns org_member when calling getOrgMember', () => {
            const org_member = {id: 1, user: 1};
            expect(orgMemberReducer.getOrgMember({ byIds: { 1: org_member } }, 1)).toEqual(org_member);
        });

        it('returns null when calling getOrgMember not found', () => {
            const org_member = {id: 1, user: 1};
            expect(orgMemberReducer.getOrgMember({ byIds: { 1: org_member } }, 2)).toEqual(null);
        });

        it('returns org_member when calling getMembersByOrg', () => {
            const org_member = {id: 1, org: 1};
            expect(orgMemberReducer.getMembersByOrg({ byIds: { 1: org_member } }, 1)).toEqual([org_member]);
        });

        it('returns null when calling getMembersByOrg not found', () => {
            const org_member = {id: 1, org: 1};
            expect(orgMemberReducer.getMembersByOrg({ byIds: { 1: org_member } }, 2)).toEqual([]);
        });

    });

});
