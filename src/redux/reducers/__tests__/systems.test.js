import * as systemsReducer from '../../redux/reducers/systems';
import {
    SYSTEM_CONNECTED,
    SYSTEM_DISCONNECTED,
    SYSTEM_FETCH_SUCCESS
} from '../../redux/constants/actionTypes';


describe('systemsReducer suite', () => {

    describe('ids reducer', () => {
        it('returns initial state on unknown', () => {
            const action = {
                type: 'unknown',
            };
            expect(systemsReducer.ids([], action)).toEqual([]);
        });
    });

    describe('byIds reducer', () => {
        it('adds new system on SYSTEM_FETCH_SUCCESS', () => {
            const system = {
                1: {
                    id: 1,
                    state: 'disconnected'
                }
            };
            const action = {
                type: SYSTEM_FETCH_SUCCESS,
                system
            };
            expect(systemsReducer.byIds(system, action)).toEqual(system);
        });

        it('updates system on SYSTEM_CONNECTED', () => {
            const system = {
                1: {
                    id: 1,
                    state: 'disconnected'
                },
                2: {
                    id: 2,
                    state: 'disconnected'
                }
            };
            const new_system_state = {id: 1, state: 'connected'};
            const action = {
                type: SYSTEM_CONNECTED,
                system: new_system_state
            };
            const expectedResult = system;
            expectedResult[1] = new_system_state;
            expect(systemsReducer.byIds(system, action)).toEqual(expectedResult);
        });

        it('updates system on SYSTEM_DISCONNECTED', () => {
            const system = {
                1: {
                    id: 1,
                    state: 'connected'
                }
            };
            const new_system_state = {id: 1, state: 'connected'};
            const action = {
                type: SYSTEM_CONNECTED,
                system: new_system_state
            };
            const expectedResult = {1: new_system_state};
            expect(systemsReducer.byIds(system, action)).toEqual(expectedResult);
        });
    });

    describe('getters', () => {
        it('returns ids when calling getIds', () => {
            const ids = [1, 2, 3];
            expect(systemsReducer.getIds({ ids })).toEqual(ids);
        });

        it('returns team when calling getSystem', () => {
            const system = {id: 1, state: 'blah'};
            expect(systemsReducer.getSystem({ byIds: { 1: system } }, 1)).toEqual(system);
        });

        it('returns null when calling getSystem not found', () => {
            const system = {id: 1, state: 'blah'};
            expect(systemsReducer.getSystem({ byIds: { 1: system } }, 2)).toEqual(null);
        });

    });

});
