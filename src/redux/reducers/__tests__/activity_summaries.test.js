import * as activitySummariesReducer from '../../redux/reducers/activity_summaries';
import {
    FETCH_ACTIVITY_SUMMARY_REQUEST,
    FETCH_ACTIVITY_SUMMARY_SUCCESS,
    FETCH_ACTIVITY_SUMMARY_FAILURE
} from '../../redux/constants/actionTypes';


describe('activitySummariesReducer suite', () => {

    describe('ids reducer', () => {

        it('adds new ids on FETCH_ACTIVITY_SUMMARY_SUCCESS', () => {
            const result = [1, 2, 3];
            const action = {
                type: FETCH_ACTIVITY_SUMMARY_SUCCESS,
                response: {
                    result
                }
            };
            expect(activitySummariesReducer.ids([], action)).toEqual(result);
        });

        it('returns initial state on unknown', () => {
            const action = {
                type: 'unknown',
            };
            expect(activitySummariesReducer.ids([], action)).toEqual([]);
        });

    });

    describe('byIds reducer', () => {
        let initialState = {};

        beforeEach(() => {
            initialState = {
                1: {
                    id: 1,
                    body: 'This is a test activity_summary',
                }
            };
        });

        it('adds new activity_summary on FETCH_ACTIVITY_SUMMARY_SUCCESS', () => {
            const activity_summaries = [
                {id: 1, body: 'This is a test activity_summary'},
                {id: 2, body: 'This is a test activity_summary'},
            ];
            const action = {
                type: FETCH_ACTIVITY_SUMMARY_SUCCESS,
                response: {
                    entities: {
                        activity_summary: activity_summaries
                    }
                }
            };
            const expectedState = {
                ...initialState,
                ...activity_summaries,
            };
            expect(activitySummariesReducer.byIds(initialState, action)).toEqual(expectedState);
        });

        it('returns initialState on FETCH_ACTIVITY_SUMMARY_SUCCESS with no response', () => {
            const action = {
                type: FETCH_ACTIVITY_SUMMARY_SUCCESS,
            };
            expect(activitySummariesReducer.byIds(initialState, action)).toEqual(initialState);
        });

        it('returns initialState on unknown', () => {
            const action = {
                type: 'unknown',
            };
            expect(activitySummariesReducer.byIds(initialState, action)).toEqual(initialState);
        });
    });

    describe('isFetching reducer', () => {
        it('returns true on FETCH_ACTIVITY_SUMMARY_REQUEST', () => {
            const action = { type: FETCH_ACTIVITY_SUMMARY_REQUEST };
            expect(activitySummariesReducer.isFetching(false, action)).toBe(true);
        });
        it('returns false on FETCH_ACTIVITY_SUMMARY_SUCCESS', () => {
            const action = { type: FETCH_ACTIVITY_SUMMARY_SUCCESS };
            expect(activitySummariesReducer.isFetching(true, action)).toBe(false);
        });
        it('returns false on FETCH_ACTIVITY_SUMMARY_FAILURE', () => {
            const action = { type: FETCH_ACTIVITY_SUMMARY_FAILURE };
            expect(activitySummariesReducer.isFetching(true, action)).toBe(false);
        });
    });

    describe('getters', () => {
        it('returns ids when calling getIds', () => {
            const ids = [1, 2, 3];
            expect(activitySummariesReducer.getIds({ ids })).toEqual(ids);
        });

        it('returns activity_summary when calling getActivitySummary', () => {
            const activity_summary = {id: 1, activity: 1, body: 'blah'};
            expect(activitySummariesReducer.getActivitySummary({ byIds: { 1: activity_summary } }, 1)).toEqual(activity_summary);
        });

        it('returns null when calling getActivitySummary not found', () => {
            const activity_summary = {id: 1, activity: 1, body: 'blah'};
            expect(activitySummariesReducer.getActivitySummary({ byIds: { 1: activity_summary } }, 2)).toEqual(null);
        });

        it('returns activity_summary when calling getActivitySummariesByActivity', () => {
            const activity_summary = {id: 1, activity: 1, body: 'blah'};
            expect(activitySummariesReducer.getActivitySummariesByActivity({ byIds: { 1: activity_summary } }, 1)).toEqual([activity_summary]);
        });

        it('returns null when calling getActivitySummariesByActivity not found', () => {
            const activity_summary = {id: 1, activity: 1, body: 'blah'};
            expect(activitySummariesReducer.getActivitySummariesByActivity({ byIds: { 1: activity_summary } }, 2)).toEqual([]);
        });

        it('returns false when not fetching', () => {
            expect(activitySummariesReducer.getActivitySummariesIsFetching({isFetching: false})).toBe(false);
        });
    });

});
