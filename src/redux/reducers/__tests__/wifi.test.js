import * as wifiReducer from '../../redux/reducers/wifi';
import {
    WIFI_CONNECTION_REQUESTED,
    WIFI_CONNECTION_SUCCESS,
    WIFI_CONNECTION_FAILED,
    WIFI_FETCH_FAILED,
    WIFI_STOP_RETRIES,
} from '../../redux/constants/actionTypes';


describe('wifiReducer suite', () => {
    it('returns initial state on unknown type', () => {
        expect(wifiReducer.wifiState(undefined, {type: 'unknown'})).toEqual(wifiReducer.initialState);
    });

    it('sets isConnected and SSID on WIFI_CONNECTION_REQUESTED', () => {
        const initialState = {
            ...wifiReducer.initialState,
            connectionAttempts: 3
        }
        const expectedState = {
            ...wifiReducer.initialState,
            deviceSSID: 'test-ssid',
            isConnected: false,
            isConnectRetryOn: true,
            connectionAttempts: 3,
            isConnecting: true
        }
        const newState = wifiReducer.wifiState(initialState, {
            type: WIFI_CONNECTION_REQUESTED,
            deviceSSID: 'test-ssid',
            reset: false
        });
        expect(newState).toEqual(expectedState);
    });

    it('sets connectionAttempts on WIFI_CONNECTION_REQUESTED with reset', () => {
        const initialState = {
            ...wifiReducer.initialState,
            connectionAttempts: 3
        }
        const expectedState = {
            ...wifiReducer.initialState,
            deviceSSID: 'test-ssid',
            isConnected: false,
            isConnectRetryOn: true,
            connectionAttempts: 0,
            isConnecting: true
        }
        const newState = wifiReducer.wifiState(initialState, {
            type: WIFI_CONNECTION_REQUESTED,
            deviceSSID: 'test-ssid',
            reset: true
        });
        expect(newState).toEqual(expectedState);
    });

    it('sets IP/SSID/isConnected on WIFI_CONNECTION_SUCCESS', () => {
        const expectedState = {
            ...wifiReducer.initialState,
            deviceSSID: 'test-ssid',
            deviceIP: '192.168.0.1',
            isConnected: true,
            isConnectRetryOn: false,
            connectionAttempts: 0,
            isConnecting: false
        }
        const newState = wifiReducer.wifiState(undefined, {
            type: WIFI_CONNECTION_SUCCESS,
            deviceSSID: 'test-ssid',
            IPAddress: '192.168.0.1',
        });
        expect(newState).toEqual(expectedState);
    });

    it('sets isConnected, connectionAttempts on WIFI_CONNECTION_FAILED', () => {
        const initialState = {
            ...wifiReducer.initialState,
            connectionAttempts: 3
        }
        const expectedState = {
            ...wifiReducer.initialState,
            isConnected: false,
            connectionAttempts: 4,
            connectionError: 'could not connect to wifi',
            isConnecting: false
        }
        const newState = wifiReducer.wifiState(initialState, {
            type: WIFI_CONNECTION_FAILED,
            error: {
                message: 'could not connect to wifi'
            }
        });
        expect(newState).toEqual(expectedState);
    });

    it('sets isConnectRetryOn on WIFI_STOP_RETRIES', () => {
        const initialState = {
            ...wifiReducer.initialState,
            isConnectRetryOn: true
        }
        const expectedState = {
            ...wifiReducer.initialState,
            isConnectRetryOn: false,
            isConnecting: false
        }
        const newState = wifiReducer.wifiState(initialState, {
            type: WIFI_STOP_RETRIES
        });
        expect(newState).toEqual(expectedState);
    });

    it('increments fetchAttempts on WIFI_FETCH_FAILED', () => {
        const initialState = {
            ...wifiReducer.initialState,
            fetchAttempts: 1
        }
        const expectedState = {
            ...wifiReducer.initialState,
            deviceSSID: undefined,
            fetchAttempts: 2
        }
        const newState = wifiReducer.wifiState(initialState, {
            type: WIFI_FETCH_FAILED
        });
        expect(newState).toEqual(expectedState);
    });

});
