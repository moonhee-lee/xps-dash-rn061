import * as activityUsersReducer from '../../redux/reducers/activity_users';
import {
    FETCH_ACTIVITY_USER_REQUEST,
    FETCH_ACTIVITY_USER_SUCCESS,
    FETCH_ACTIVITY_USER_FAILURE,
    SAVE_ACTIVITY_USER_SUCCESS,
    DELETE_ACTIVITY_USER_SUCCESS,
    SYSTEM_SPRINT_MOTION_DISTANCE,
    SYSTEM_SPRINT_BEEP_START,
    SYSTEM_SPRINT_ENDED,
    SYSTEM_JUMP_DETECTED,
    ACTIVITY_RESET_RECORDING_MODE,
    ACTIVITY_TOGGLE_RECORDING_MODE,
} from '../../redux/constants/actionTypes';


describe('activityUsersReducer suite', () => {

    describe('ids reducer', () => {

        it('adds new ids on FETCH_ACTIVITY_USER_SUCCESS', () => {
            const result = [1, 2, 3];
            const action = {
                type: FETCH_ACTIVITY_USER_SUCCESS,
                response: {
                    result
                }
            };
            expect(activityUsersReducer.ids([], action)).toEqual(result);
        });

        it('adds new ids on SAVE_ACTIVITY_USER_SUCCESS', () => {
            const result = [1, 2, 3];
            const action = {
                type: SAVE_ACTIVITY_USER_SUCCESS,
                response: {
                    result
                }
            };
            expect(activityUsersReducer.ids([], action)).toEqual(result);
        });

        it('removes id on DELETE_ACTIVITY_USER_SUCCESS', () => {
            const initialState = [1, 2, 3];
            const result = [1, 3];
            const action = {
                type: DELETE_ACTIVITY_USER_SUCCESS,
                activityUserId: 2
            };
            expect(activityUsersReducer.ids(initialState, action)).toEqual(result);
        });

        it('returns initial state on unknown', () => {
            const action = {
                type: 'unknown',
            };
            expect(activityUsersReducer.ids([], action)).toEqual([]);
        });

    });

    describe('byIds reducer', () => {
        let initialState = {};

        beforeEach(() => {
            initialState = {
                1: {
                    id: 1,
                    body: 'This is a test activity_user',
                }
            };
        });

        it('adds new activity_user on FETCH_ACTIVITY_USER_SUCCESS', () => {
            const activity_users = [
                {id: 1, body: 'This is a test activity_user'},
                {id: 2, body: 'This is a test activity_user'},
            ];
            const action = {
                type: FETCH_ACTIVITY_USER_SUCCESS,
                response: {
                    entities: {
                        activity_user: activity_users
                    }
                }
            };
            const expectedState = {
                ...initialState,
                ...activity_users,
            };
            expect(activityUsersReducer.byIds(initialState, action)).toEqual(expectedState);
        });

        it('adds new activity_user on SAVE_ACTIVITY_USER_SUCCESS', () => {
            const activity_users = [
                {id: 1, body: 'This is a test activity_user'},
                {id: 2, body: 'This is a test activity_user'},
            ];
            const action = {
                type: SAVE_ACTIVITY_USER_SUCCESS,
                response: {
                    entities: {
                        activity_user: activity_users
                    }
                }
            };
            const expectedState = {
                ...initialState,
                ...activity_users,
            };
            expect(activityUsersReducer.byIds(initialState, action)).toEqual(expectedState);
        });

        it('returns initialState on FETCH_ACTIVITY_USER_SUCCESS with no response', () => {
            const action = {
                type: FETCH_ACTIVITY_USER_SUCCESS,
            };
            expect(activityUsersReducer.byIds(initialState, action)).toEqual(initialState);
        });

        it('removes tag on DELETE_ACTIVITY_USER_SUCCESS', () => {
            const action = {
                type: DELETE_ACTIVITY_USER_SUCCESS,
                activityUserId: 2
            };
            const initialState = {
                1: {id: 1, activity: 1 },
                2: {id: 2, activity: 1 },
                3: {id: 3, activity: 2 },
            };
            const expectedState = {
                1: {id: 1, activity: 1 },
                3: {id: 3, activity: 2 },
            };
            expect(activityUsersReducer.byIds(initialState, action)).toEqual(expectedState);
        });

        it('sets data on SYSTEM_SPRINT_BEEP_START', () => {
            const action = {
                type: SYSTEM_SPRINT_BEEP_START,
                activityId: 2,
                beepStartTime: 36473
            };
            const initialState = {
                1: {id: 1, activity: 1 },
                2: {id: 2, activity: 1 },
                3: {id: 3, activity: 2 },
            };
            const expectedState = {
                1: {id: 1, activity: 1 },
                2: {id: 2, activity: 1 },
                3: {id: 3, activity: 2, beepStartTime: 36473 },
            };
            expect(activityUsersReducer.byIds(initialState, action)).toEqual(expectedState);
        });

        it('sets data on SYSTEM_SPRINT_MOTION_DISTANCE', () => {
            const action = {
                type: SYSTEM_SPRINT_MOTION_DISTANCE,
                activityUserId: 2,
                currentDistance: 1.2
            };
            const initialState = {
                1: {id: 1, activity: 1 },
                2: {id: 2, activity: 1 },
                3: {id: 3, activity: 2 },
            };
            const expectedState = {
                1: {id: 1, activity: 1 },
                2: {id: 2, activity: 1, distance: 1.2  },
                3: {id: 3, activity: 2 },
            };
            expect(activityUsersReducer.byIds(initialState, action)).toEqual(expectedState);
        });

        it('sets data on SYSTEM_SPRINT_ENDED', () => {
            const sprintData = {end: 10.0, start: 5.0};
            const expectedMeta = {
                ...sprintData,
                finish_time: 5.0,
                total_time: 5.0,
                max_velocity_m: null,
                max_acceleration_m: null,
                max_velocity_y: null,
                max_acceleration_y: null,
                reaction_time: null,
                split_data_metres: null,
                split_data_yards: null,
            }
            const action = {
                type: SYSTEM_SPRINT_ENDED,
                activityUserId: 2,
                sprintData,
                splitDataMetres: null,
                splitDataYards: null,
            };
            const initialState = {
                1: {id: 1, activity: 1 },
                2: {id: 2, activity: 1 },
                3: {id: 3, activity: 2 },
            };
            const expectedState = {
                1: {id: 1, activity: 1 },
                2: {id: 2, activity: 1, meta_data: expectedMeta, completed: true, score: 5.0  },
                3: {id: 3, activity: 2 },
            };
            expect(activityUsersReducer.byIds(initialState, action)).toEqual(expectedState);
        });

        it('sets data on SYSTEM_SPRINT_ENDED with reaction time', () => {
            const sprintData = {end: 10.0, start: 5.0};
            const expectedMeta = {
                ...sprintData,
                finish_time: 5.0,
                total_time: 9.9955,
                reaction_time: 4.9955,
                max_velocity_m: null,
                max_acceleration_m: null,
                max_velocity_y: null,
                max_acceleration_y: null,
                split_data_metres: null,
                split_data_yards: null,
            }
            const action = {
                type: SYSTEM_SPRINT_ENDED,
                activityUserId: 2,
                sprintData,
                splitDataMetres: null,
                splitDataYards: null,
            };
            const initialState = {
                1: {id: 1, activity: 1 },
                2: {id: 2, activity: 1, beepStartTime: 4.5 },
                3: {id: 3, activity: 2 },
            };
            const expectedState = {
                1: {id: 1, activity: 1 },
                2: {id: 2, activity: 1, beepStartTime: 4.5, meta_data: expectedMeta, completed: true, score: 9.9955  },
                3: {id: 3, activity: 2 },
            };
            expect(activityUsersReducer.byIds(initialState, action)).toEqual(expectedState);
        });

        it('sets data on SYSTEM_JUMP_DETECTED vertical', () => {
            const jumpData = {height: 10.0, length: 5.0};
            const action = {
                type: SYSTEM_JUMP_DETECTED,
                activityUserId: 2,
                tagName: 1,
                jumpData
            };
            const initialState = {
                1: {id: 1, activity: 1, tag: {id:1,name:1} },
                2: {id: 2, activity: 1, tag: {id:1,name:1} },
                3: {id: 3, activity: 2, tag: {id:1,name:1} },
            };
            const expectedState = {
                1: {id: 1, activity: 1, tag: {id:1,name:1} },
                2: {id: 2, activity: 1, tag: {id:1,name:1}, meta_data: jumpData, completed: true, score: 10.0  },
                3: {id: 3, activity: 2, tag: {id:1,name:1} },
            };
            expect(activityUsersReducer.byIds(initialState, action)).toEqual(expectedState);
        });

        it('sets data on SYSTEM_JUMP_DETECTED horizontal', () => {
            const jumpData = {height: 5.0, length: 10.0};
            const action = {
                type: SYSTEM_JUMP_DETECTED,
                activityUserId: 2,
                tagName: 1,
                jumpData
            };
            const initialState = {
                1: {id: 1, activity: 1, tag: {id:1,name:1} },
                2: {id: 2, activity: 1, tag: {id:1,name:1} },
                3: {id: 3, activity: 2, tag: {id:1,name:1} },
            };
            const expectedState = {
                1: {id: 1, activity: 1, tag: {id:1,name:1} },
                2: {id: 2, activity: 1, tag: {id:1,name:1}, meta_data: jumpData, completed: true, score: 10.0  },
                3: {id: 3, activity: 2, tag: {id:1,name:1} },
            };
            expect(activityUsersReducer.byIds(initialState, action)).toEqual(expectedState);
        });

        it('resets on ACTIVITY_RESET_RECORDING_MODE', () => {
            const action = {
                type: ACTIVITY_RESET_RECORDING_MODE,
                activityId: 2
            };
            const initialState = {
                1: {id: 1, activity: 1, meta_data: {'something': 'value'}, completed: true, distance: 1.0 },
                2: {id: 2, activity: 1, meta_data: {'something': 'value'}, completed: true, distance: 1.0 },
                3: {id: 3, activity: 2, meta_data: {'something': 'value'}, completed: true, distance: 1.0 },
            };
            const expectedState = {
                1: {id: 1, activity: 1, meta_data: {'something': 'value'}, completed: true, distance: 1.0 },
                2: {id: 2, activity: 1, meta_data: {'something': 'value'}, completed: true, distance: 1.0 },
                3: {id: 3, activity: 2, meta_data: {}, completed: false, distance: null},
            };
            expect(activityUsersReducer.byIds(initialState, action)).toEqual(expectedState);
        })

        it('returns initialState on unknown', () => {
            const action = {
                type: 'unknown',
            };
            expect(activityUsersReducer.byIds(initialState, action)).toEqual(initialState);
        });
    });

    describe('isFetching reducer', () => {
        it('returns true on FETCH_ACTIVITY_USER_REQUEST', () => {
            const action = { type: FETCH_ACTIVITY_USER_REQUEST };
            expect(activityUsersReducer.isFetching(false, action)).toBe(true);
        });
        it('returns false on FETCH_ACTIVITY_USER_SUCCESS', () => {
            const action = { type: FETCH_ACTIVITY_USER_SUCCESS };
            expect(activityUsersReducer.isFetching(true, action)).toBe(false);
        });
        it('returns false on FETCH_ACTIVITY_USER_FAILURE', () => {
            const action = { type: FETCH_ACTIVITY_USER_FAILURE };
            expect(activityUsersReducer.isFetching(true, action)).toBe(false);
        });
    });

    describe('getters', () => {
        it('returns ids when calling getIds', () => {
            const ids = [1, 2, 3];
            expect(activityUsersReducer.getIds({ ids })).toEqual(ids);
        });

        it('returns activity_user when calling getActivityUser', () => {
            const activity_user = {id: 1, activity: 1};
            expect(activityUsersReducer.getActivityUser({ byIds: { 1: activity_user } }, 1)).toEqual(activity_user);
        });

        it('returns null when calling getActivityUser not found', () => {
            const activity_user = {id: 1, activity: 1};
            expect(activityUsersReducer.getActivityUser({ byIds: { 1: activity_user } }, 2)).toEqual(null);
        });

        it('returns activity_user when calling getActivityUsersByActivity', () => {
            const activity_user = {id: 1, activity: 1, user: 1};
            expect(activityUsersReducer.getActivityUsersByActivity({ byIds: { 1: activity_user } }, 1)).toEqual([activity_user]);
        });

        it('returns null when calling getActivityUsersByActivity not found', () => {
            const activity_user = {id: 1, activity: 1, user: 1};
            expect(activityUsersReducer.getActivityUsersByActivity({ byIds: { 1: activity_user } }, 2)).toEqual([]);
        });

        it('returns false when not fetching', () => {
            expect(activityUsersReducer.getActivityUsersIsFetching({isFetching: false})).toBe(false);
        });
    });

});
