import * as activeSessionReducer from '../../redux/reducers/active_session';
import {
    SET_ACTIVE_SESSION,
    SET_ACTIVE_ACTIVITY,
} from '../../redux/constants/actionTypes';


describe('activeSessionReducer suite', () => {

    describe('activeSession reducer', () => {

        it('sets activeSession on SET_ACTIVE_SESSION', () => {
            const session = {id: 1};
            const result = {activeSession: session};
            const action = {
                type: SET_ACTIVE_SESSION,
                session
            };
            expect(activeSessionReducer.activeSession([], action)).toEqual(result);
        });

        it('returns initial state on unknown', () => {
            const action = {
                type: 'unknown',
            };
            expect(activeSessionReducer.activeSession({}, action)).toEqual({});
        });

    });
    describe('activeActivity reducer', () => {

        it('sets activeActivity on SET_ACTIVE_ACTIVITY', () => {
            const activity = {id: 1};
            const result = {activeActivity: activity};
            const action = {
                type: SET_ACTIVE_ACTIVITY,
                activity
            };
            expect(activeSessionReducer.activeActivity([], action)).toEqual(result);
        });

        it('returns initial state on unknown', () => {
            const action = {
                type: 'unknown',
            };
            expect(activeSessionReducer.activeActivity({}, action)).toEqual({});
        });

    });

    describe('getters', () => {
        it('returns activeSession when calling getActiveSession', () => {
            const session = {id: 1};
            expect(activeSessionReducer.getActiveSession({activeSession: {activeSession: session}})).toEqual(session);
        });
        it('returns activeActivity when calling getActiveActivity', () => {
            const activity = {id: 1};
            expect(activeSessionReducer.getActiveActivity({activeActivity: {activeActivity: activity}})).toEqual(activity);
        });
        it('returns activeSession when calling getActiveSession with no state', () => {
            expect(activeSessionReducer.getActiveSession()).toEqual(null);
        });
        it('returns activeActivity when calling getActiveActivity with no state', () => {
            const activity = {id: 1};
            expect(activeSessionReducer.getActiveActivity()).toEqual(null);
        });
    });

});
