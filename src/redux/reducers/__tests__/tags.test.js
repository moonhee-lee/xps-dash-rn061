import * as tagsReducer from '../../redux/reducers/tags';
import * as fromStorage from '../../redux/local_storage';
import {
    FETCH_TAG_REQUEST,
    FETCH_TAG_SUCCESS,
    FETCH_TAG_FAILURE,
    SYSTEM_TAG_INITIALIZED,
    SYSTEM_MESSAGE_RECEIVED,
    SYSTEM_SPRINT_MOTION_DISTANCE,
    SYSTEM_SPRINT_ENDED,
    SYSTEM_JUMP_DETECTED,
    ACTIVITY_TOGGLE_RECORDING_MODE,
    ACTIVITY_RESET_RECORDING_MODE,
    SYSTEM_TAG_READY,
    SYSTEM_TAG_NOT_READY,
    SYSTEM_TAG_SELECT_CONFIRMED,
    SYSTEM_TAG_REMOVED,
    FETCH_ACTIVITY_USER_SUCCESS,
    SET_TAG_POSITION,
    SET_TAG_VELOCITY,
    SET_TAG_BATTERY
} from '../../redux/constants/actionTypes';
import {ACTIVITY_RESET_LIVE_RECORDING_MODE} from "../../constants/actionTypes";

jest.mock('../../redux/local_storage');

describe('tagsReducer suite', () => {

    describe('ids reducer', () => {

        it('adds new ids on FETCH_TAG_SUCCESS', () => {
            const result = [1, 2, 3];
            const action = {
                type: FETCH_TAG_SUCCESS,
                response: {
                    result
                }
            };
            expect(tagsReducer.ids([], action)).toEqual(result);
        });

        it('returns initial state on unknown', () => {
            const action = {
                type: 'unknown',
            };
            expect(tagsReducer.ids([], action)).toEqual([]);
        });

    });

    describe('byIds reducer', () => {
        let initialState = {};

        beforeEach(() => {
            initialState = {
                1: {
                    id: 1,
                    name: '1',
                }
            };
        });

        it('adds new tag on FETCH_TAG_SUCCESS', () => {
            const tags = [
                {id: 1, name: '1'},
                {id: 2, name: '2'},
            ];
            const action = {
                type: FETCH_TAG_SUCCESS,
                response: {
                    entities: {
                        tag: tags
                    }
                }
            };
            const expectedState = {
                ...initialState,
                ...tags,
            };
            expect(tagsReducer.byIds(initialState, action)).toEqual(expectedState);
        });

        it('adds new tag on FETCH_ACTIVITY_USER_SUCCESS', () => {
            const tags = [
                {id: 1, name: '1'},
                {id: 2, name: '2'},
            ];
            const action = {
                type: FETCH_ACTIVITY_USER_SUCCESS,
                response: {
                    entities: {
                        tag: tags
                    }
                }
            };
            const expectedState = {
                ...initialState,
                ...tags,
            };
            expect(tagsReducer.byIds(initialState, action)).toEqual(expectedState);
        });

        it('returns initialState on FETCH_TAG_SUCCESS with no response', () => {
            const action = {
                type: FETCH_TAG_SUCCESS,
            };
            expect(tagsReducer.byIds(initialState, action)).toEqual(initialState);
        });

        it('returns initialState on unknown', () => {
            const action = {
                type: 'unknown',
            };
            expect(tagsReducer.byIds(initialState, action)).toEqual(initialState);
        });
    });

    describe('tagLiveData reducer', () => {
        afterEach(() => {
            jest.clearAllMocks();
        });

        it('adds new tag on SYSTEM_TAG_INITIALIZED', () => {
            const action = {
                type: SYSTEM_TAG_INITIALIZED,
                tagId: 1,
                tagName: 2,
                motionAlgo: {},
                CODAlgo: {},
                activityUser: {id: 1},
                activity: {id: 1},
                activity_type: 'sprint'
            };
            const expectedState = {
                2: {
                    id: 1,
                    name: 2,
                    status: 'select_pending',
                    motionAlgo: {},
                    CODAlgo: {},
                    activityUserId: 1,
                    activityId: 1,
                    activityType: 'sprint'
                }
            };
            expect(tagsReducer.tagLiveData({}, action)).toEqual(expectedState);
        });

        it('adds new tag on SYSTEM_TAG_INITIALIZED', () => {
            const action = {
                type: SYSTEM_TAG_INITIALIZED,
                tagId: 1,
                tagName: 2,
                motionAlgo: {},
                CODAlgo: {},
                activityUser: {id: 1},
                activity: null,
                activity_type: 'sprint'
            };
            const expectedState = {
                2: {
                    id: 1,
                    CODAlgo: {},
                    motionAlgo: {},
                    name: 2,
                    status: "select_pending"
                }
            };
            expect(tagsReducer.tagLiveData({}, action)).toEqual(expectedState);
        });

        it('sets tags new state to selected on SYSTEM_TAG_SELECT_CONFIRMED', () => {
            const initialState = {
                1: {status: 'ready'},
                2: {status: 'select_pending'},
                3: {status: 'select_pending'},
            }
            const action = {
                type: SYSTEM_TAG_SELECT_CONFIRMED,
            };
            const expectedState = {
                1: {status: 'ready'},
                2: {status: 'selected'},
                3: {status: 'selected'},
            }
            expect(tagsReducer.tagLiveData(initialState, action)).toEqual(expectedState);
        });
        it('sets tags new state to ready on SYSTEM_TAG_READY', () => {
            const initialState = {
                1: {name: 1, status: 'ready'},
                2: {name: 2, status: 'selected'},
                3: {name: 3, status: 'selected'},
            }
            const action = {
                type: SYSTEM_TAG_READY,
                tagName: 3
            };
            const expectedState = {
                1: {name: 1, status: 'ready'},
                2: {name: 2, status: 'selected'},
                3: {name: 3, status: 'ready'},
            }
            expect(tagsReducer.tagLiveData(initialState, action)).toEqual(expectedState);
        });
        it('sets tags new state to selected on SYSTEM_TAG_NOT_READY', () => {
            const initialState = {
                1: {name: 1, status: 'ready'},
                2: {name: 2, status: 'selected'},
                3: {name: 3, status: 'ready'},
            }
            const action = {
                type: SYSTEM_TAG_NOT_READY,
                tagName: 3
            };
            const expectedState = {
                1: {name: 1, status: 'ready'},
                2: {name: 2, status: 'selected'},
                3: {name: 3, status: 'selected'},
            }
            expect(tagsReducer.tagLiveData(initialState, action)).toEqual(expectedState);
        });
        it('sets tags new state to recording on ACTIVITY_TOGGLE_RECORDING_MODE', () => {
            const initialState = {
                1: {activityId: 1, status: 'ready'},
                2: {activityId: 2, status: 'ready'},
                3: {activityId: 2, status: 'ready'},
            }
            const action = {
                type: ACTIVITY_TOGGLE_RECORDING_MODE,
                activityId: 2,
                isRecording: true
            };
            const expectedState = {
                1: {activityId: 1, status: 'ready'},
                2: {activityId: 2, status: 'recording'},
                3: {activityId: 2, status: 'recording'},
            }
            expect(tagsReducer.tagLiveData(initialState, action)).toEqual(expectedState);
        });
        it('sets tags new state to ready on ACTIVITY_TOGGLE_RECORDING_MODE', () => {
            const initialState = {
                1: {activityId: 1, status: 'recording'},
                2: {activityId: 2, status: 'recording'},
                3: {activityId: 2, status: 'recording'},
            }
            const action = {
                type: ACTIVITY_TOGGLE_RECORDING_MODE,
                activityId: 1,
                isRecording: false
            };
            const expectedState = {
                1: {activityId: 1, status: 'ready'},
                2: {activityId: 2, status: 'recording'},
                3: {activityId: 2, status: 'recording'},
            }
            expect(tagsReducer.tagLiveData(initialState, action)).toEqual(expectedState);
        });
        it('sets tags new state to ready on SYSTEM_SPRINT_ENDED', () => {
            const initialState = {
                1: {activityUserId: 1, status: 'recording'},
                2: {activityUserId: 2, status: 'recording'},
                3: {activityUserId: 2, status: 'recording'},
            }
            const action = {
                type: SYSTEM_SPRINT_ENDED,
                activityUserId: 1
            };
            const expectedState = {
                1: {activityUserId: 1, status: 'ready'},
                2: {activityUserId: 2, status: 'recording'},
                3: {activityUserId: 2, status: 'recording'},
            }
            expect(tagsReducer.tagLiveData(initialState, action)).toEqual(expectedState);
        });
        it('sets tags new state to ready on SYSTEM_JUMP_DETECTED', () => {
            const initialState = {
                1: {activityUserId: 1, status: 'recording'},
                2: {activityUserId: 2, status: 'recording'},
                3: {activityUserId: 2, status: 'recording'},
            }
            const action = {
                type: SYSTEM_JUMP_DETECTED,
                activityUserId: 1
            };
            const expectedState = {
                1: {activityUserId: 1, status: 'ready'},
                2: {activityUserId: 2, status: 'recording'},
                3: {activityUserId: 2, status: 'recording'},
            }
            expect(tagsReducer.tagLiveData(initialState, action)).toEqual(expectedState);
        });
        it('sets tags new state to ready on ACTIVITY_RESET_RECORDING_MODE', () => {
            const motionAlgo = {
                reset: jest.fn()
            };
            const CODAlgo = {
                reset: jest.fn()
            };
            const initialState = {
                1: {activityId: 1, status: 'recording', motionAlgo, CODAlgo},
                2: {activityId: 2, status: 'recording', motionAlgo, CODAlgo},
                3: {activityId: 2, status: 'recording', motionAlgo, CODAlgo},
            }
            const action = {
                type: ACTIVITY_RESET_RECORDING_MODE,
                activityId: 1
            };
            const expectedState = {
                1: {activityId: 1, status: 'selected', motionAlgo, CODAlgo},
                2: {activityId: 2, status: 'recording', motionAlgo, CODAlgo},
                3: {activityId: 2, status: 'recording', motionAlgo, CODAlgo},
            }
            expect(tagsReducer.tagLiveData(initialState, action)).toEqual(expectedState);
            expect(motionAlgo.reset.mock.calls.length).toBe(1);
        });
    });

    describe('isFetching reducer', () => {
        it('returns true on FETCH_TAG_REQUEST', () => {
            const action = { type: FETCH_TAG_REQUEST };
            expect(tagsReducer.isFetching(false, action)).toBe(true);
        });
        it('returns false on FETCH_TAG_SUCCESS', () => {
            const action = { type: FETCH_TAG_SUCCESS };
            expect(tagsReducer.isFetching(true, action)).toBe(false);
        });
        it('returns false on FETCH_TAG_FAILURE', () => {
            const action = { type: FETCH_TAG_FAILURE };
            expect(tagsReducer.isFetching(true, action)).toBe(false);
        });
    });

    describe('tagLiveData reducer', () => {
        it('adds new tag on SYSTEM_TAG_INITIALIZED', () => {
            const motionAlgo = jest.fn();
            const action = {
                type: SYSTEM_TAG_INITIALIZED,
                tagId: 1,
                tagName: 2,
                motionAlgo,
                CODAlgo: {},
                activityUser: {id: 1},
                activity: {id: 1},
                activity_type: 'sprint'
            };
            const expectedState = {
                2: {
                    id: 1,
                    name: 2,
                    status: 'select_pending',
                    motionAlgo,
                    CODAlgo: {},
                    activityUserId: 1,
                    activityId: 1,
                    activityType: 'sprint'
                }
            };
            expect(tagsReducer.tagLiveData({}, action)).toEqual(expectedState);
        });

        it('sets all pending to selected on SYSTEM_TAG_SELECT_CONFIRMED', () => {
            const initialState = {
                1: {id: 1, status: 'select_pending'},
                2: {id: 2, status: 'ready'},
                3: {id: 3, status: 'select_pending'},
            };
            const expectedState = {
                1: {id: 1, status: 'selected'},
                2: {id: 2, status: 'ready'},
                3: {id: 3, status: 'selected'},
            };
            const action = {
                type: SYSTEM_TAG_SELECT_CONFIRMED
            }
            expect(tagsReducer.tagLiveData(initialState, action)).toEqual(expectedState);
        });

        it('sets tag status to ready on SYSTEM_TAG_READY', () => {
            const action = {
                type: SYSTEM_TAG_READY,
                tagName: 2
            };
            const initialState = {
                1: {id: 1, name: 2, status: 'selected'},
                2: {id: 2, name: 1, status: 'ready'},
            };
            const expectedState = {
                1: {id: 1, name: 2, status: 'ready'},
                2: {id: 2, name: 1, status: 'ready'},
            };
            expect(tagsReducer.tagLiveData(initialState, action)).toEqual(expectedState);
        });

        it('sets tag status to selected on SYSTEM_TAG_NOT_READY', () => {
            const action = {
                type: SYSTEM_TAG_NOT_READY,
                tagName: 2
            };
            const initialState = {
                1: {id: 1, name: 2, status: 'ready'},
                2: {id: 2, name: 1, status: 'ready'},
            };
            const expectedState = {
                1: {id: 1, name: 2, status: 'selected'},
                2: {id: 2, name: 1, status: 'ready'},
            };
            expect(tagsReducer.tagLiveData(initialState, action)).toEqual(expectedState);
        });

        it('sets tag status to recording on ACTIVITY_TOGGLE_RECORDING_MODE', () => {
            const action = {
                type: ACTIVITY_TOGGLE_RECORDING_MODE,
                activityId: 1,
                isRecording: true
            };
            const initialState = {
                1: {id: 1, activityId: 1, status: 'ready'},
                2: {id: 2, activityId: 1, status: 'ready'},
                3: {id: 3, activityId: 2, status: 'ready'},
            };
            const expectedState = {
                1: {id: 1, activityId: 1, status: 'recording'},
                2: {id: 2, activityId: 1, status: 'recording'},
                3: {id: 3, activityId: 2, status: 'ready'},
            };
            expect(tagsReducer.tagLiveData(initialState, action)).toEqual(expectedState);
        });

        it('sets tag status to ready on ACTIVITY_TOGGLE_RECORDING_MODE', () => {
            const action = {
                type: ACTIVITY_TOGGLE_RECORDING_MODE,
                activityId: 1,
                isRecording: false
            };
            const initialState = {
                1: {id: 1, activityId: 1, status: 'recording'},
                2: {id: 2, activityId: 1, status: 'recording'},
                3: {id: 3, activityId: 2, status: 'recording'},
            };
            const expectedState = {
                1: {id: 1, activityId: 1, status: 'ready'},
                2: {id: 2, activityId: 1, status: 'ready'},
                3: {id: 3, activityId: 2, status: 'recording'},
            };
            expect(tagsReducer.tagLiveData(initialState, action)).toEqual(expectedState);
        });

        it('sets tag status to recording on ACTIVITY_RESET_RECORDING_MODE', () => {
            const action = {
                type: ACTIVITY_RESET_RECORDING_MODE,
                activityId: 1
            };
            const motionAlgo = {reset: jest.fn()};
            const CODAlgo = {reset: jest.fn()};
            const initialState = {
                1: {id: 1, activityId: 1, status: 'recording', motionAlgo, CODAlgo},
                2: {id: 2, activityId: 1, status: 'recording', motionAlgo, CODAlgo},
                3: {id: 3, activityId: 2, status: 'recording', motionAlgo, CODAlgo},
            };
            const expectedState = {
                1: {id: 1, activityId: 1, status: 'selected', motionAlgo, CODAlgo},
                2: {id: 2, activityId: 1, status: 'selected', motionAlgo, CODAlgo},
                3: {id: 3, activityId: 2, status: 'recording', motionAlgo, CODAlgo},
            };
            expect(tagsReducer.tagLiveData(initialState, action)).toEqual(expectedState);
            expect(motionAlgo.reset.mock.calls.length).toBe(2);
            expect(fromStorage.resetMessages.mock.calls.length).toBe(2);
        });

        it('removes tag on SYSTEM_TAG_REMOVED', () => {
            const action = {
                type: SYSTEM_TAG_REMOVED,
                tagName: 1
            };
            const initialState = {
                1: {id: 1, name: 2, activityId: 1, status: 'ready'},
                2: {id: 2, name: 1, activityId: 1, status: 'ready'},
                3: {id: 3, name: 3, activityId: 2, status: 'ready'},
            };
            const expectedState = {
                1: {id: 1, name: 2, activityId: 1, status: 'ready'},
                3: {id: 3, name: 3, activityId: 2, status: 'ready'},
            };
            expect(tagsReducer.tagLiveData(initialState, action)).toEqual(expectedState);
        });

        it('resets tag on ACTIVITY_RESET_LIVE_RECORDING_MODE', () => {
            const action = {
                type: ACTIVITY_RESET_LIVE_RECORDING_MODE
            };
            const initialState = {
                1: {id: 1, name: 2, activityId: 1, status: 'ready'}
            };
            const expectedState = {
                1: {}
            };
            expect(tagsReducer.tagLiveData(initialState, action)).toEqual(expectedState);
        });

        it('resets tag on ACTIVITY_RESET_LIVE_RECORDING_MODE', () => {
            const action = {
                type: ACTIVITY_RESET_LIVE_RECORDING_MODE
            };
            const initialState = {
                1: {id: 1, name: 2, activityId: 1, status: 'ready', motionAlgo:{reset:jest.fn()}}
            };
            const expectedState = {
                1: {}
            };
            expect(tagsReducer.tagLiveData(initialState, action)).toEqual(expectedState);
        });

        it('resets tag on ACTIVITY_RESET_LIVE_RECORDING_MODE', () => {
            const action = {
                type: ACTIVITY_RESET_LIVE_RECORDING_MODE
            };
            const initialState = {
                1: {id: 1, name: 2, activityId: 1, status: 'ready', CODAlgo:{reset:jest.fn()}}
            };
            const expectedState = {
                1: {}
            };
            expect(tagsReducer.tagLiveData(initialState, action)).toEqual(expectedState);
        });

        it('sets tag position on SET_TAG_POSITION', () => {
            const action = {
                type: SET_TAG_POSITION,
                position: 1
            };
            const initialState = {
                1: {id: 1, name: 2, activityId: 1, status: 'ready'}
            };
            const expectedState = {
                1: {id: 1, name: 2, activityId: 1, status: 'ready', position: 1}
            };
            expect(tagsReducer.tagLiveData(initialState, action)).toEqual(expectedState);
        });

        it('sets tag position on SET_TAG_POSITION', () => {
            const action = {
                type: SET_TAG_POSITION,
            };
            const initialState = {
                1: {id: 1, name: 2, activityId: 1, status: 'ready'}
            };
            const expectedState = {
                1: {id: 1, name: 2, activityId: 1, status: 'ready'}
            };
            expect(tagsReducer.tagLiveData(initialState, action)).toEqual(expectedState);
        });

        it('sets tag position on SET_TAG_VELOCITY', () => {
            const action = {
                type: SET_TAG_VELOCITY,
                velocity: 1,
                tag: {name:2}
            };
            const initialState = {
                1: {id: 1, name: 2, activityId: 1, status: 'ready'}
            };
            const expectedState = {
                1: {id: 1, name: 2, activityId: 1, status: 'ready', velocity: 1}
            };
            expect(tagsReducer.tagLiveData(initialState, action)).toEqual(expectedState);
        });

        it('sets tag position on SET_TAG_VELOCITY', () => {
            const action = {
                type: SET_TAG_VELOCITY,
                velocity: 1,
                tag: {name:3}
            };
            const initialState = {
                1: {id: 1, name: 2, activityId: 1, status: 'ready'}
            };
            const expectedState = {
                1: {id: 1, name: 2, activityId: 1, status: 'ready'}
            };
            expect(tagsReducer.tagLiveData(initialState, action)).toEqual(expectedState);
        });

        it('sets tag position on SET_TAG_BATTERY', () => {
            const action = {
                type: SET_TAG_BATTERY,
                battery: 1,
                tagName:2
            };
            const initialState = {
                1: {id: 1, name: 2, activityId: 1, status: 'ready'}
            };
            const expectedState = {
                1: {id: 1, name: 2, activityId: 1, status: 'ready', battery: 1}
            };
            expect(tagsReducer.tagLiveData(initialState, action)).toEqual(expectedState);
        });

        it('sets tag position on SET_TAG_BATTERY', () => {
            const action = {
                type: SET_TAG_BATTERY,
                battery: 1,
                tagName: 3
            };
            const initialState = {
                1: {id: 1, name: 2, activityId: 1, status: 'ready'}
            };
            const expectedState = {
                1: {id: 1, name: 2, activityId: 1, status: 'ready'}
            };
            expect(tagsReducer.tagLiveData(initialState, action)).toEqual(expectedState);
        });
    });

    describe('getters', () => {
        it('returns ids when calling getIds', () => {
            const ids = [1, 2, 3];
            expect(tagsReducer.getIds({ ids })).toEqual(ids);
        });

        it('returns tag when calling getTags', () => {
            const tag = {id: 1, name: '1'};
            expect(tagsReducer.getTags({ byIds: { 1: tag } })).toEqual({1:tag});
        });

        it('returns tag when calling getTagsByOrg', () => {
            const tag = {id: 1, name: '1'};
            expect(tagsReducer.getTagsByOrg({ byIds: { 1: tag } }, 1)).toEqual({1:tag});
        });

        it('returns tag when calling getTag', () => {
            const tag = {id: 1, name: '1'};
            expect(tagsReducer.getTag({ byIds: { 1: tag } }, 1)).toEqual(tag);
        });

        it('returns null when calling getTag not found', () => {
            const tag = {id: 1, name: '1'};
            expect(tagsReducer.getTag({ byIds: { 1: tag } }, 2)).toEqual(null);
        });

        it('returns tag when calling getTagLiveData', () => {
            const tag = {id: 1, name: '1', system: 1};
            expect(tagsReducer.getTagLiveData({ tagLiveData: { 1: tag } }, 1)).toEqual(tag);
        });

        it('returns null when calling getTagLiveData not found', () => {
            const tag = {id: 1, name: '1', system: 1};
            expect(tagsReducer.getTagLiveData({ tagLiveData: { 1: tag } }, 2)).toEqual(null);
        });

        it('returns tagLiveData', () => {
            const tag = {id: 1, name: '1', system: 1};
            const state = {
                tagLiveData: { 1: tag }
            };
            expect(tagsReducer.getTagsInLiveData(state)).toEqual(state.tagLiveData);
        });

        it('returns tag when calling getTagsBySystem', () => {
            const tag = {id: 1, name: '1', system: 1};
            expect(tagsReducer.getTagsBySystem({ byIds: { 1: tag } }, 1)).toEqual([tag]);
        });

        it('returns null when calling getTagsBySystem not found', () => {
            const tag = {id: 1, name: '1', system: 1};
            expect(tagsReducer.getTagsBySystem({ byIds: { 1: tag } }, 2)).toEqual([]);
        });

        it('returns false when not fetching', () => {
            expect(tagsReducer.getTagsIsFetching({isFetching: false})).toBe(false);
        });
    });

});
