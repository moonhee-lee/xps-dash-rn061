import * as sessionsReducer from '../../redux/reducers/sessions';
import {
    FETCH_SESSION_REQUEST,
    FETCH_SESSION_SUCCESS,
    FETCH_SESSION_FAILURE,
    SAVE_SESSION_SUCCESS
} from '../../redux/constants/actionTypes';


describe('sessionsReducer suite', () => {

    describe('ids reducer', () => {

        it('adds new ids on FETCH_SESSION_SUCCESS', () => {
            const result = [1, 2, 3];
            const action = {
                type: FETCH_SESSION_SUCCESS,
                response: {
                    result
                }
            };
            expect(sessionsReducer.ids([], action)).toEqual(result);
        });

        it('adds new ids on SAVE_SESSION_SUCCESS', () => {
            const result = [1, 2, 3];
            const action = {
                type: SAVE_SESSION_SUCCESS,
                response: {
                    result
                }
            };
            expect(sessionsReducer.ids([], action)).toEqual(result);
        });

        it('returns initial state on unknown', () => {
            const action = {
                type: 'unknown',
            };
            expect(sessionsReducer.ids([], action)).toEqual([]);
        });

    });

    describe('byIds reducer', () => {
        let initialState = {};

        beforeEach(() => {
            initialState = {
                1: {
                    id: 1,
                    body: 'This is a test session',
                }
            };
        });

        it('adds new session on FETCH_SESSION_SUCCESS', () => {
            const sessions = [
                {id: 1, body: 'This is a test session'},
                {id: 2, body: 'This is a test session'},
            ];
            const action = {
                type: FETCH_SESSION_SUCCESS,
                response: {
                    entities: {
                        session: sessions
                    }
                }
            };
            const expectedState = {
                ...initialState,
                ...sessions,
            };
            expect(sessionsReducer.byIds(initialState, action)).toEqual(expectedState);
        });

        it('adds new session on SAVE_SESSION_SUCCESS', () => {
            const sessions = [
                {id: 1, body: 'This is a test session'},
                {id: 2, body: 'This is a test session'},
            ];
            const action = {
                type: SAVE_SESSION_SUCCESS,
                response: {
                    entities: {
                        session: sessions
                    }
                }
            };
            const expectedState = {
                ...initialState,
                ...sessions,
            };
            expect(sessionsReducer.byIds(initialState, action)).toEqual(expectedState);
        });

        it('returns initialState on FETCH_SESSION_SUCCESS with no response', () => {
            const action = {
                type: FETCH_SESSION_SUCCESS,
            };
            expect(sessionsReducer.byIds(initialState, action)).toEqual(initialState);
        });

        it('returns initialState on unknown', () => {
            const action = {
                type: 'unknown',
            };
            expect(sessionsReducer.byIds(initialState, action)).toEqual(initialState);
        });
    });

    describe('isFetching reducer', () => {
        it('returns true on FETCH_SESSION_REQUEST', () => {
            const action = { type: FETCH_SESSION_REQUEST };
            expect(sessionsReducer.isFetching(false, action)).toBe(true);
        });
        it('returns false on FETCH_SESSION_SUCCESS', () => {
            const action = { type: FETCH_SESSION_SUCCESS };
            expect(sessionsReducer.isFetching(true, action)).toBe(false);
        });
        it('returns false on FETCH_SESSION_FAILURE', () => {
            const action = { type: FETCH_SESSION_FAILURE };
            expect(sessionsReducer.isFetching(true, action)).toBe(false);
        });
    });

    describe('getters', () => {
        it('returns ids when calling getIds', () => {
            const ids = [1, 2, 3];
            expect(sessionsReducer.getIds({ ids })).toEqual(ids);
        });

        it('returns session when calling getSession', () => {
            const session = {id: 1, org: 1, body: 'blah'};
            expect(sessionsReducer.getSession({ byIds: { 1: session } }, 1)).toEqual(session);
        });

        it('returns null when calling getSession not found', () => {
            const session = {id: 1, org: 1, body: 'blah'};
            expect(sessionsReducer.getSession({ byIds: { 1: session } }, 2)).toEqual(null);
        });

        it('returns session when calling getSessionsByOrg', () => {
            const session = {id: 1, org: 1, body: 'blah'};
            expect(sessionsReducer.getSessionsByOrg({ byIds: { 1: session } }, 1)).toEqual([session]);
        });

        it('returns null when calling getSessionsByOrg not found', () => {
            const session = {id: 1, org: 1, body: 'blah'};
            expect(sessionsReducer.getSessionsByOrg({ byIds: { 1: session } }, 2)).toEqual([]);
        });

        it('returns false when not fetching', () => {
            expect(sessionsReducer.getSessionsIsFetching({isFetching: false})).toBe(false);
        });
    });

});
