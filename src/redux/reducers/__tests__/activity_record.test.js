import * as activityRecordsReducer from '../../redux/reducers/activity_records';
import {
    FETCH_ACTIVITY_RECORD_REQUEST,
    FETCH_ACTIVITY_RECORD_SUCCESS,
    FETCH_ACTIVITY_RECORD_FAILURE,
    FETCH_ACTIVITY_RECORD_BY_USER_REQUEST,
    FETCH_ACTIVITY_RECORD_BY_USER_SUCCESS,
    FETCH_ACTIVITY_RECORD_BY_USER_FAILURE,
    SAVE_ACTIVITY_RECORD_REQUEST,
    ACTIVITY_RESET_RECORDING_MODE,
} from '../../redux/constants/actionTypes';


describe('activityRecordsReducer suite', () => {

    describe('ids reducer', () => {

        it('adds new ids on FETCH_ACTIVITY_RECORD_SUCCESS', () => {
            const result = [1, 2, 3];
            const action = {
                type: FETCH_ACTIVITY_RECORD_SUCCESS,
                response: {
                    result
                }
            };
            expect(activityRecordsReducer.ids([], action)).toEqual(result);
        });

        it('returns initial state on unknown', () => {
            const action = {
                type: 'unknown',
            };
            expect(activityRecordsReducer.ids([], action)).toEqual([]);
        });

    });

    describe('byIds reducer', () => {
        let initialState = {};

        beforeEach(() => {
            initialState = {
                1: {
                    id: 1,
                    body: 'This is a test activity_record',
                }
            };
        });

        it('adds new activity_record on FETCH_ACTIVITY_RECORD_SUCCESS', () => {
            const activity_records = [
                {id: 1, body: 'This is a test activity_record'},
                {id: 2, body: 'This is a test activity_record'},
            ];
            const action = {
                type: FETCH_ACTIVITY_RECORD_SUCCESS,
                response: {
                    entities: {
                        activity_record: activity_records
                    }
                }
            };
            const expectedState = {
                ...initialState,
                ...activity_records,
            };
            expect(activityRecordsReducer.byIds(initialState, action)).toEqual(expectedState);
        });

        it('returns initialState on FETCH_ACTIVITY_RECORD_SUCCESS with no response', () => {
            const action = {
                type: FETCH_ACTIVITY_RECORD_SUCCESS,
            };
            expect(activityRecordsReducer.byIds(initialState, action)).toEqual(initialState);
        });

        it('returns initialState on unknown', () => {
            const action = {
                type: 'unknown',
            };
            expect(activityRecordsReducer.byIds(initialState, action)).toEqual(initialState);
        });
    });

    describe('isFetching reducer', () => {
        it('returns true on FETCH_ACTIVITY_RECORD_REQUEST', () => {
            const action = { type: FETCH_ACTIVITY_RECORD_REQUEST };
            expect(activityRecordsReducer.isFetching(false, action)).toBe(true);
        });
        it('returns false on FETCH_ACTIVITY_RECORD_SUCCESS', () => {
            const action = { type: FETCH_ACTIVITY_RECORD_SUCCESS };
            expect(activityRecordsReducer.isFetching(true, action)).toBe(false);
        });
        it('returns false on FETCH_ACTIVITY_RECORD_FAILURE', () => {
            const action = { type: FETCH_ACTIVITY_RECORD_FAILURE };
            expect(activityRecordsReducer.isFetching(true, action)).toBe(false);
        });
    });

    describe('isSaving reducer', () => {
        it('returns true on SAVE_ACTIVITY_RECORD_REQUEST', () => {
            const action = { type: SAVE_ACTIVITY_RECORD_REQUEST };
            expect(activityRecordsReducer.isSaving(false, action)).toBe(true);
        });
        it('returns false on ACTIVITY_RESET_RECORDING_MODE', () => {
            const action = { type: ACTIVITY_RESET_RECORDING_MODE };
            expect(activityRecordsReducer.isSaving(true, action)).toBe(false);
        });
    });

    describe('getters', () => {
        it('returns ids when calling getIds', () => {
            const ids = [1, 2, 3];
            expect(activityRecordsReducer.getIds({ ids })).toEqual(ids);
        });

        it('returns activity_record when calling getActivityRecord', () => {
            const activity_record = {id: 1, activity: 1};
            expect(activityRecordsReducer.getActivityRecord({ byIds: { 1: activity_record } }, 1)).toEqual(activity_record);
        });

        it('returns null when calling getActivityRecord not found', () => {
            const activity_record = {id: 1, activity: 1};
            expect(activityRecordsReducer.getActivityRecord({ byIds: { 1: activity_record } }, 2)).toEqual(null);
        });

        it('returns activity_record when calling getActivityRecords', () => {
            const activity_record = {id: 1, activity: 1, user: 1};
            expect(activityRecordsReducer.getActivityRecords({ byIds: { 1: activity_record } }, 1, 1)).toEqual([activity_record]);
        });

        it('returns null when calling getActivityRecords not found', () => {
            const activity_record = {id: 2, activity: 2, user: 1};
            expect(activityRecordsReducer.getActivityRecords({ byIds: { 1: activity_record } }, 1, 2)).toEqual([]);
        });

        it('returns activity_record when calling getActivityRecordsByUser', () => {
            const activity_record = {id: 1, activity: 1, user: 1};
            expect(activityRecordsReducer.getActivityRecordsByUser({ byIds: { 1: activity_record } }, 1)).toEqual([activity_record]);
        });

        it('returns null when calling getActivityRecordsByUser not found', () => {
            const activity_record = {id: 1, activity: 1, user: 1};
            expect(activityRecordsReducer.getActivityRecordsByUser({ byIds: { 1: activity_record } }, 2)).toEqual([]);
        });

        it('returns false when not fetching', () => {
            expect(activityRecordsReducer.getActivityRecordsIsFetching({isFetching: false})).toBe(false);
        });
    });

});
