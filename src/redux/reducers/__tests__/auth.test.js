import * as authReducer from '../../redux/reducers/auth';
import mockData from '../../redux/api/__mocks__/data';
import {
    SET_AUTH,
    LOGIN_SUCCESS,
    LOGIN_FAILURE,
    LOGOUT_SUCCESS,
} from '../../redux/constants/actionTypes';


describe('authReducer suite', () => {
    it('returns initial state on unknown type', () => {
        expect(authReducer.authInfo(undefined, {type: 'unknown'})).toEqual({});
    });

    it('sets user on SET_AUTH', () => {
        const expectedState = {
            loggedIn: true,
            userId: mockData.users[0].id,
            authToken: 'token'
        }
        const newState = authReducer.authInfo(undefined, {
            type: SET_AUTH,
            userId: mockData.users[0].id,
            authToken: 'token'
        });
        expect(newState).toEqual(expectedState);
    });

    it('returns user info on LOGIN_SUCCESS', () => {
        const expectedState = {
            loggedIn: true,
            userId: mockData.users[0].id,
            authToken: 'token'
        };
        const newState = authReducer.authInfo(undefined, {
            type: LOGIN_SUCCESS,
            response: {
                result: mockData.users[0].id
            },
            authToken: 'token'
        });
        expect(newState).toEqual(expectedState);
    });

    it('returns initial state on LOGIN_FAILURE', () => {
        const expectedState = {
            loggedIn: false,
            userId: null,
            authToken: null,
        };
        expect(authReducer.authInfo(undefined, {type: LOGIN_FAILURE})).toEqual(expectedState);
    });

    it('returns initial state on LOGOUT_SUCCESS', () => {
        const expectedState = {
            loggedIn: false,
            userId: null,
            authToken: null,
        };
        expect(authReducer.authInfo(undefined, {type: LOGOUT_SUCCESS})).toEqual(expectedState);
    });
});
