import * as tabsReducer from '../../redux/reducers/tabs';
import * as fromStorage from '../../redux/local_storage';
import {
    SAVE_ACTIVE_TAB,
} from '../../redux/constants/actionTypes';

jest.mock('../../redux/local_storage');

describe('tabsReducer suite', () => {

    describe('tabsInfo reducer', () => {

        it('sets new currentTab on SAVE_ACTIVE_TAB', () => {
            const initialState = {currentTab: 23};
            const action = {
                type: SAVE_ACTIVE_TAB,
                currentTab: 11
            };
            const expectedResult = {currentTab: 11};
            expect(tabsReducer.tabsInfo(initialState, action)).toEqual(expectedResult);
        });

        it('returns initial state on unknown', () => {
            const action = {
                type: 'unknown',
            };
            expect(tabsReducer.tabsInfo({}, action)).toEqual({});
        });

    });
});
