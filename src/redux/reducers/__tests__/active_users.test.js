import * as activeUsersReducer from '../../redux/reducers/active_users';
import {
    FETCH_ACTIVE_USER_REQUEST,
    FETCH_ACTIVE_USER_SUCCESS,
    FETCH_ACTIVE_USER_FAILURE,
    SAVE_ACTIVE_USER_SUCCESS,
    DELETE_ACTIVE_USER_SUCCESS,
    SYSTEM_SPRINT_MOTION_DISTANCE,
    SYSTEM_SPRINT_BEEP_START,
    SYSTEM_SPRINT_ENDED,
    SYSTEM_JUMP_DETECTED,
} from '../../redux/constants/actionTypes';


describe('activeUsersReducer suite', () => {

    describe('ids reducer', () => {

        it('adds new ids on FETCH_ACTIVE_USER_SUCCESS', () => {
            const result = [1, 2, 3];
            const action = {
                type: FETCH_ACTIVE_USER_SUCCESS,
                response: {
                    result
                }
            };
            expect(activeUsersReducer.ids([], action)).toEqual(result);
        });

        it('adds new ids on SAVE_ACTIVE_USER_SUCCESS', () => {
            const result = [1, 2, 3];
            const action = {
                type: SAVE_ACTIVE_USER_SUCCESS,
                response: {
                    result
                }
            };
            expect(activeUsersReducer.ids([], action)).toEqual(result);
        });

        it('removes id on DELETE_ACTIVE_USER_SUCCESS', () => {
            const initialState = [1, 2, 3];
            const result = [1, 3];
            const action = {
                type: DELETE_ACTIVE_USER_SUCCESS,
                activeUserId: 2
            };
            expect(activeUsersReducer.ids(initialState, action)).toEqual(result);
        });

        it('returns initial state on unknown', () => {
            const action = {
                type: 'unknown',
            };
            expect(activeUsersReducer.ids([], action)).toEqual([]);
        });

    });

    describe('byIds reducer', () => {
        let initialState = {};

        beforeEach(() => {
            initialState = {
                1: {
                    id: 1,
                    body: 'This is a test active_user',
                }
            };
        });

        it('adds new active_user on FETCH_ACTIVE_USER_SUCCESS', () => {
            const active_users = [
                {id: 1, body: 'This is a test active_user'},
                {id: 2, body: 'This is a test active_user'},
            ];
            const action = {
                type: FETCH_ACTIVE_USER_SUCCESS,
                response: {
                    entities: {
                        active_user: active_users
                    }
                }
            };
            const expectedState = {
                ...initialState,
                ...active_users,
            };
            expect(activeUsersReducer.byIds(initialState, action)).toEqual(expectedState);
        });

        it('adds new active_user on SAVE_ACTIVE_USER_SUCCESS', () => {
            const active_users = [
                {id: 1, body: 'This is a test active_user'},
                {id: 2, body: 'This is a test active_user'},
            ];
            const action = {
                type: SAVE_ACTIVE_USER_SUCCESS,
                response: {
                    entities: {
                        active_user: active_users
                    }
                }
            };
            const expectedState = {
                ...initialState,
                ...active_users,
            };
            expect(activeUsersReducer.byIds(initialState, action)).toEqual(expectedState);
        });

        it('returns initialState on FETCH_ACTIVE_USER_SUCCESS with no response', () => {
            const action = {
                type: FETCH_ACTIVE_USER_SUCCESS,
            };
            expect(activeUsersReducer.byIds(initialState, action)).toEqual(initialState);
        });

        it('removes tag on DELETE_ACTIVE_USER_SUCCESS', () => {
            const action = {
                type: DELETE_ACTIVE_USER_SUCCESS,
                activeUserId: 2
            };
            const initialState = {
                1: {id: 1, activity: 1 },
                2: {id: 2, activity: 1 },
                3: {id: 3, activity: 2 },
            };
            const expectedState = {
                1: {id: 1, activity: 1 },
                3: {id: 3, activity: 2 },
            };
            expect(activeUsersReducer.byIds(initialState, action)).toEqual(expectedState);
        });

        it('sets data on SYSTEM_SPRINT_BEEP_START', () => {
            const action = {
                type: SYSTEM_SPRINT_BEEP_START,
                activityId: 'live_activity',
                beepStartTime: 36473
            };
            const initialState = {
                1: {id: 1, activity: 1 },
                2: {id: 2, activity: 1 },
                3: {id: 3, activity: 2 },
            };
            const expectedState = {
                1: {id: 1, activity: 1, beepStartTime: 36473 },
                2: {id: 2, activity: 1, beepStartTime: 36473 },
                3: {id: 3, activity: 2, beepStartTime: 36473 },
            };
            expect(activeUsersReducer.byIds(initialState, action)).toEqual(expectedState);
        });

        it('sets data on SYSTEM_SPRINT_BEEP_START with no live activity', () => {
            const action = {
                type: SYSTEM_SPRINT_BEEP_START,
                activityId: null,
                beepStartTime: 36473
            };
            const initialState = {
                1: {id: 1, activity: 1 },
                2: {id: 2, activity: 1 },
                3: {id: 3, activity: 2 },
            };
            const expectedState = {
                1: {id: 1, activity: 1 },
                2: {id: 2, activity: 1 },
                3: {id: 3, activity: 2 },
            };
            expect(activeUsersReducer.byIds(initialState, action)).toEqual(expectedState);
        });

        it('sets data on SYSTEM_SPRINT_MOTION_DISTANCE', () => {
            const action = {
                type: SYSTEM_SPRINT_MOTION_DISTANCE,
                activeUserId: 2,
                currentDistance: 1.2
            };
            const initialState = {
                1: {id: 1, activity: 1 },
                2: {id: 2, activity: 1 },
                3: {id: 3, activity: 2 },
            };
            const expectedState = {
                1: {id: 1, activity: 1 },
                2: {id: 2, activity: 1, distance: 1.2  },
                3: {id: 3, activity: 2 },
            };
            expect(activeUsersReducer.byIds(initialState, action)).toEqual(expectedState);
        });

        it('sets data on SYSTEM_SPRINT_ENDED', () => {
            const sprintData = {end: 10.0, start: 5.0};
            const expectedMeta = {changes_of_direction: undefined, end: 10, finish_time: .005, max_acceleration: null, max_acceleration_m: null, max_acceleration_y: null, max_velocity: null, max_velocity_m: null, max_raw_velocity_y: null, max_velocity_y: null, max_raw_velocity_m: null, reaction_time: null, split_data_metres: null, split_data_yards: null, sprint_time: .005, start: 5, stride_data_metres: [], total_time: .005};
            const expectedEvents = [{time: .005, type: "start"}, {time: .010, type: "end"}, {time: NaN, type: "beep"}, {time: null, type: "reaction_time"}, {time: .005, type: "finish_time"}, {pos: undefined, time: .005, type: "motion_start"}, {pos: undefined, time: .010, type: "sprint_end"}];
            const action = {
                type: SYSTEM_SPRINT_ENDED,
                activeUserId: 2,
                sprintData,
                splitDataMetres: null,
                splitDataYards: null,
            };
            const initialState = {
                1: {id: 1, activity: 1 },
                2: {id: 2, activity: 1 },
                3: {id: 3, activity: 2 },
            };
            const expectedState = {
                1: {id: 1, activity: 1 },
                2: {id: 2, activity: 1, events: expectedEvents, meta_data: expectedMeta, completed: true, score: .0050  },
                3: {id: 3, activity: 2 },
            };
            expect(activeUsersReducer.byIds(initialState, action)).toEqual(expectedState);
        });

        it('sets data on SYSTEM_SPRINT_ENDED with reaction time', () => {
            const sprintData = {end: 10.0, start: 5.0};
            const expectedMeta = {changes_of_direction: undefined, end: 10, finish_time: .005, max_acceleration: null, max_acceleration_m: null, max_acceleration_y: null, max_velocity: null, max_velocity_m: null, max_raw_velocity_y: null, max_velocity_y: null, max_raw_velocity_m: null, reaction_time: 0.0005, split_data_metres: null, split_data_yards: null, sprint_time: .005, start: 5, stride_data_metres: [], total_time: 0.0055};
            const expectedEvents = [{time: .005, type: "start"}, {time: .010, type: "end"}, {time: 0.0045, type: "beep"}, {time: 0.0005, type: "reaction_time"}, {time: .005, type: "finish_time"}, {pos: undefined, time: .005, type: "motion_start"}, {pos: undefined, time: .010, type: "sprint_end"}];
            const action = {
                type: SYSTEM_SPRINT_ENDED,
                activeUserId: 2,
                sprintData,
                splitDataMetres: null,
                splitDataYards: null,
            };
            const initialState = {
                1: {id: 1, activity: 1 },
                2: {id: 2, activity: 1, beepStartTime: 4.5 },
                3: {id: 3, activity: 2 },
            };
            const expectedState = {
                1: {id: 1, activity: 1 },
                2: {id: 2, activity: 1, beepStartTime: 4.5, events: expectedEvents, meta_data: expectedMeta, completed: true, score: 0.0055  },
                3: {id: 3, activity: 2 },
            };
            expect(activeUsersReducer.byIds(initialState, action)).toEqual(expectedState);
        });

        it('sets data on SYSTEM_JUMP_DETECTED vertical', () => {
            const jumpData = {height: 10.0, length: 5.0};
            const action = {
                type: SYSTEM_JUMP_DETECTED,
                activeUserId: 2,
                tagName: 1,
                jumpData
            };
            const initialState = {
                1: {id: 1, activity: 1, tag: {id:1,name:1} },
                2: {id: 2, activity: 1, tag: {id:1,name:1} },
                3: {id: 3, activity: 2, tag: {id:1,name:1} },
            };
            const expectedState = {
                1: {id: 1, activity: 1, tag: {id:1,name:1} },
                2: {id: 2, activity: 1, tag: {id:1,name:1}, meta_data: jumpData, completed: true, score: 10.0  },
                3: {id: 3, activity: 2, tag: {id:1,name:1} },
            };
            expect(activeUsersReducer.byIds(initialState, action)).toEqual(expectedState);
        });

        it('sets data on SYSTEM_JUMP_DETECTED horizontal', () => {
            const jumpData = {height: 5.0, length: 10.0};
            const action = {
                type: SYSTEM_JUMP_DETECTED,
                activeUserId: 2,
                tagName: 1,
                jumpData
            };
            const initialState = {
                1: {id: 1, activity: 1, tag: {id:1,name:1} },
                2: {id: 2, activity: 1, tag: {id:1,name:1} },
                3: {id: 3, activity: 2, tag: {id:1,name:1} },
            };
            const expectedState = {
                1: {id: 1, activity: 1, tag: {id:1,name:1} },
                2: {id: 2, activity: 1, tag: {id:1,name:1}, meta_data: jumpData, completed: true, score: 10.0  },
                3: {id: 3, activity: 2, tag: {id:1,name:1} },
            };
            expect(activeUsersReducer.byIds(initialState, action)).toEqual(expectedState);
        });

        it('returns initialState on unknown', () => {
            const action = {
                type: 'unknown',
            };
            expect(activeUsersReducer.byIds(initialState, action)).toEqual(initialState);
        });
    });

    describe('isFetching reducer', () => {
        it('returns true on FETCH_ACTIVE_USER_REQUEST', () => {
            const action = { type: FETCH_ACTIVE_USER_REQUEST };
            expect(activeUsersReducer.isFetching(false, action)).toBe(true);
        });
        it('returns false on FETCH_ACTIVE_USER_SUCCESS', () => {
            const action = { type: FETCH_ACTIVE_USER_SUCCESS };
            expect(activeUsersReducer.isFetching(true, action)).toBe(false);
        });
        it('returns false on FETCH_ACTIVE_USER_FAILURE', () => {
            const action = { type: FETCH_ACTIVE_USER_FAILURE };
            expect(activeUsersReducer.isFetching(true, action)).toBe(false);
        });
    });

    describe('getters', () => {
        it('returns ids when calling getIds', () => {
            const ids = [1, 2, 3];
            expect(activeUsersReducer.getIds({ ids })).toEqual(ids);
        });

        it('returns active_user when calling getActiveUser', () => {
            const active_user = {id: 1, activity: 1};
            expect(activeUsersReducer.getActiveUser({ byIds: { 1: active_user } }, 1)).toEqual(active_user);
        });

        it('returns null when calling getActiveUser not found', () => {
            const active_user = {id: 1, activity: 1};
            expect(activeUsersReducer.getActiveUser({ byIds: { 1: active_user } }, 2)).toEqual(undefined);
        });

        it('returns active_user when calling getActiveUsers', () => {
            const active_user = {activity: 1, id: 1, user: 1};
            const expected = {1: {activity: 1, id: 1, user: 1}};
            expect(activeUsersReducer.getActiveUsers({ byIds: { 1: active_user } }, 1)).toEqual(expected);
        });

        it('returns null when calling getActiveUsers not found', () => {
            const active_user = {activity: 1, id: 1, user: 1};
            const expected = {1: {activity: 1, id: 1, user: 1}};
            expect(activeUsersReducer.getActiveUsers({ byIds: { 1: active_user } }, 2)).toEqual(expected);
        });

        it('returns false when not fetching', () => {
            expect(activeUsersReducer.getActiveUsersIsFetching({isFetching: false})).toBe(false);
        });
    });

});
