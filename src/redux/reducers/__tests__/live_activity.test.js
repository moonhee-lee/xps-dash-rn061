import * as liveActivityReducer from '../../redux/reducers/live_activity';
import {
    FETCH_LIVE_ACTIVITY_REQUEST,
    FETCH_LIVE_ACTIVITY_SUCCESS,
    FETCH_LIVE_ACTIVITY_FAILURE,
    SAVE_LIVE_ACTIVITY_SUCCESS,
    SET_LIVE_ACTIVITY_SESSION,
    FETCH_LIVE_ACTIVITY_SESSION,
    SET_LIVE_ACTIVITY_TEAM
} from '../../redux/constants/actionTypes';

describe('liveActivityReducer suite', () => {

    describe('ids reducer', () => {

        it('adds new ids on FETCH_LIVE_ACTIVITY_SUCCESS', () => {
            const live_activity = {id:1};
            const entities = {
                activity: {
                    live_activity
                }
            };
            const action = {
                type: FETCH_LIVE_ACTIVITY_SUCCESS,
                response: {
                    entities
                }
            };
            expect(liveActivityReducer.liveActivity([], action)).toEqual(live_activity);
        });

        it('adds new ids on SAVE_LIVE_ACTIVITY_SUCCESS', () => {
            const live_activity = {id:2};
            const entities = {
                activity: {
                    live_activity
                }
            };
            const action = {
                type: SAVE_LIVE_ACTIVITY_SUCCESS,
                response: {
                    entities
                }
            };
            expect(liveActivityReducer.liveActivity([], action)).toEqual(live_activity);
        });

        it('adds new ids on SET_LIVE_ACTIVITY_SESSION', () => {
            const session = {id:1};
            const expected = {"session": 1};
            const action = {
                type: SET_LIVE_ACTIVITY_SESSION,
                session
            };
            expect(liveActivityReducer.liveActivity([], action)).toEqual(expected);
        });

        it('adds new ids on FETCH_LIVE_ACTIVITY_SESSION', () => {
            const session = {id:2};
            const expected = {"session": 2};
            const action = {
                type: FETCH_LIVE_ACTIVITY_SESSION,
                session
            };
            expect(liveActivityReducer.liveActivity([], action)).toEqual(expected);
        });

        it('adds new ids on SET_LIVE_ACTIVITY_TEAM', () => {
            const teamId = 2;
            const expected = {"session": {"team": teamId}, "team": teamId};
            const action = {
                type: SET_LIVE_ACTIVITY_TEAM,
                teamId: teamId
            };
            expect(liveActivityReducer.liveActivity([], action)).toEqual(expected);
        });

        it('returns initial state on unknown', () => {
            const action = {
                type: 'unknown',
            };
            expect(liveActivityReducer.liveActivity([], action)).toEqual([]);
        });

        it('returns state on SET_LIVE_ACTIVITY_SESSION but no session', () => {
            const action = {
                type: SET_LIVE_ACTIVITY_SESSION,
            };
            expect(liveActivityReducer.liveActivity([], action)).toEqual([]);
        });

        it('returns state on FETCH_LIVE_ACTIVITY_SESSION but no session', () => {
            const action = {
                type: FETCH_LIVE_ACTIVITY_SESSION,
            };
            expect(liveActivityReducer.liveActivity([], action)).toEqual([]);
        });

        it('returns state on FETCH_LIVE_ACTIVITY_SUCCESS but no response', () => {
            const action = {
                type: FETCH_LIVE_ACTIVITY_SUCCESS,
            };
            expect(liveActivityReducer.liveActivity([], action)).toEqual([]);
        });

        it('returns state on SAVE_LIVE_ACTIVITY_SUCCESS but no response', () => {
            const action = {
                type: SAVE_LIVE_ACTIVITY_SUCCESS,
            };
            expect(liveActivityReducer.liveActivity([], action)).toEqual([]);
        });

        it('returns state on SET_LIVE_ACTIVITY_TEAM but no team', () => {
            const action = {
                type: SET_LIVE_ACTIVITY_TEAM,
            };
            expect(liveActivityReducer.liveActivity([], action)).toEqual([]);
        });

    });

    describe('getters', () => {
        it('returns activity when calling getLiveActivity', () => {
            const activity = {id: 1, session: 1, name: 'blah'};
            const liveActivity = {liveActivity:activity};
            expect(liveActivityReducer.getLiveActivity({live_activity:liveActivity})).toEqual(activity);
        });

        it('returns null when calling getLiveActivity without live activity', () => {
            const activity = null;
            expect(liveActivityReducer.getLiveActivity({})).toEqual(activity);
        });
    });

});
