import * as orgSystemReducer from '../../redux/reducers/org_system';
import {
    FETCH_ORG_SYSTEM_BY_ORG_FAILURE,
    FETCH_ORG_SYSTEM_BY_ORG_REQUEST,
    FETCH_ORG_SYSTEM_BY_ORG_SUCCESS
} from '../../redux/constants/actionTypes';


describe('orgSystemReducer suite', () => {

    describe('ids reducer', () => {

        it('adds new ids on FETCH_ORG_SYSTEM_BY_ORG_SUCCESS', () => {
            const result = [1, 2, 3];
            const action = {
                type: FETCH_ORG_SYSTEM_BY_ORG_SUCCESS,
                response: {
                    result
                }
            };
            expect(orgSystemReducer.ids([], action)).toEqual(result);
        });

        it('returns initial state on unknown', () => {
            const action = {
                type: 'unknown',
            };
            expect(orgSystemReducer.ids([], action)).toEqual([]);
        });

    });

    describe('byIds reducer', () => {
        let initialState = {};

        beforeEach(() => {
            initialState = {
                1: {
                    id: 1,
                    user: 1,
                }
            };
        });

        it('adds new org_system on FETCH_ORG_SYSTEM_BY_ORG_SUCCESS', () => {
            const org_systems = [
                {id: 1},
                {id: 2},
            ];
            const action = {
                type: FETCH_ORG_SYSTEM_BY_ORG_SUCCESS,
                response: {
                    entities: {
                        org_system: org_systems
                    }
                }
            };
            const expectedState = {
                0: {id: 1},
                1: {id: 2}
            };
            expect(orgSystemReducer.byIds(initialState, action)).toEqual(expectedState);
        });

        it('returns initialState on FETCH_ORG_SYSTEM_BY_ORG_SUCCESS with no response', () => {
            const action = {
                type: FETCH_ORG_SYSTEM_BY_ORG_SUCCESS,
            };
            expect(orgSystemReducer.byIds(initialState, action)).toEqual(initialState);
        });

        it('returns initialState on unknown', () => {
            const action = {
                type: 'unknown',
            };
            expect(orgSystemReducer.byIds(initialState, action)).toEqual(initialState);
        });
    });

    describe('isFetching reducer', () => {
        it('returns true on FETCH_ORG_SYSTEM_BY_ORG_REQUEST', () => {
            const action = { type: FETCH_ORG_SYSTEM_BY_ORG_REQUEST };
            expect(orgSystemReducer.isFetching(false, action)).toBe(true);
        });
        it('returns false on FETCH_ORG_SYSTEM_BY_ORG_SUCCESS', () => {
            const action = { type: FETCH_ORG_SYSTEM_BY_ORG_SUCCESS };
            expect(orgSystemReducer.isFetching(true, action)).toBe(false);
        });
        it('returns false on FETCH_ORG_MEMBER_BY_ORG_FAILURE', () => {
            const action = { type: FETCH_ORG_SYSTEM_BY_ORG_FAILURE };
            expect(orgSystemReducer.isFetching(true, action)).toBe(false);
        });
    });

    describe('getters', () => {
        it('returns ids when calling getIds', () => {
            const ids = [1, 2, 3];
            expect(orgSystemReducer.getIds({ ids })).toEqual(ids);
        });

        it('returns org_system when calling getOrgSystem', () => {
            const org_system = {id: 1};
            expect(orgSystemReducer.getOrgSystem({ byIds: { 1: org_system } }, 1)).toEqual(org_system);
        });

        it('returns null when calling getOrgSystem not found', () => {
            const org_system = {id: 5};
            expect(orgSystemReducer.getOrgSystem({ byIds: { 5: org_system } }, 2)).toEqual(null);
        });

        it('returns org_system when calling getCurrentOrg', () => {
            const org_system = {id: 1};
            expect(orgSystemReducer.getCurrentOrg({ byIds: { 1: org_system } }, 1)).toEqual(org_system);
        });

        it('returns null when calling getCurrentOrg not found', () => {
            const org_system = {id: 5};
            expect(orgSystemReducer.getCurrentOrg({ byIds: { 5: org_system } }, 2)).toEqual({id:1});
        });

        it('returns org_systems when calling getSystemsByOrg', () => {
            const org_system = {1:{id:1, org: 1}};
            expect(orgSystemReducer.getSystemsByOrg({ byIds: { 1: {id:1, org: 1} } }, 1)).toEqual(org_system);
        });

        it('returns null when calling getSystemsByOrg not found', () => {
            const org_system = {id: 1};
            expect(orgSystemReducer.getSystemsByOrg({ byIds: { 1: org_system } }, 2)).toEqual({"1": {"id": 1}});
        });

    });

});
