import * as teamsReducer from '../../redux/reducers/teams';
import {
    LOGIN_SUCCESS,
    FETCH_SESSION_REQUEST,
    FETCH_SESSION_SUCCESS,
    FETCH_SESSION_FAILURE,
    FETCH_TEAM_REQUEST,
    FETCH_TEAM_SUCCESS,
    FETCH_TEAM_FAILURE,
    FETCH_USER_BY_TEAM_SUCCESS,
    SAVE_ORG_MEMBER_SUCCESS,
} from '../../redux/constants/actionTypes';


describe('teamsReducer suite', () => {

    describe('ids reducer', () => {

        it('adds new ids on FETCH_TEAM_SUCCESS', () => {
            const result = [1, 2, 3];
            const action = {
                type: FETCH_TEAM_SUCCESS,
                response: {
                    result
                }
            };
            expect(teamsReducer.ids([], action)).toEqual(result);
        });

        it('adds new ids on FETCH_SESSION_SUCCESS', () => {
            const result = [1, 2, 3];
            const action = {
                type: FETCH_SESSION_SUCCESS,
                response: {
                    result
                }
            };
            expect(teamsReducer.ids([], action)).toEqual(result);
        });

        it('returns initial state on unknown', () => {
            const action = {
                type: 'unknown',
            };
            expect(teamsReducer.ids([], action)).toEqual([]);
        });

    });

    describe('byIds reducer', () => {
        let initialState = {};

        beforeEach(() => {
            initialState = {
                1: {
                    id: 1,
                    body: 'This is a test team',
                }
            };
        });

        it('adds new team on FETCH_TEAM_SUCCESS', () => {
            const teams = [
                {id: 1, body: 'This is a test team'},
                {id: 2, body: 'This is a test team'},
            ];
            const action = {
                type: FETCH_TEAM_SUCCESS,
                response: {
                    entities: {
                        team: teams
                    }
                }
            };
            const expectedState = {
                ...initialState,
                ...teams,
            };
            expect(teamsReducer.byIds(initialState, action)).toEqual(expectedState);
        });

        it('returns initialState on FETCH_TEAM_SUCCESS with no response', () => {
            const action = {
                type: FETCH_TEAM_SUCCESS,
            };
            expect(teamsReducer.byIds(initialState, action)).toEqual(initialState);
        });

        it('adds new team on FETCH_SESSION_SUCCESS', () => {
            const teams = [
                {id: 1, body: 'This is a test team'},
                {id: 2, body: 'This is a test team'},
            ];
            const action = {
                type: FETCH_SESSION_SUCCESS,
                response: {
                    entities: {
                        team: teams
                    }
                }
            };
            const expectedState = {
                ...initialState,
                ...teams,
            };
            expect(teamsReducer.byIds(initialState, action)).toEqual(expectedState);
        });

        it('adds users to team on FETCH_USER_BY_TEAM_SUCCESS with no users', () => {
            const state = {
                1: {id: 1, body: 'This is a test team'},
                2: {id: 2, body: 'This is a test team', users: [10]},
            };
            const action = {
                type: FETCH_USER_BY_TEAM_SUCCESS,
                teamId: 1,
                response: {
                    result: [1, 2, 3]
                }
            };
            const expectedState = {
                1: {id: 1, body: 'This is a test team', users: [1, 2, 3]},
                2: {id: 2, body: 'This is a test team', users: [10]},
            };
            expect(teamsReducer.byIds(state, action)).toEqual(expectedState);
        });

        it('adds users to team on FETCH_USER_BY_TEAM_SUCCESS with existing users', () => {
            const state = {
                1: {id: 1, body: 'This is a test team'},
                2: {id: 2, body: 'This is a test team', users: [10]},
            };
            const action = {
                type: FETCH_USER_BY_TEAM_SUCCESS,
                teamId: 2,
                response: {
                    result: [1, 2, 3]
                }
            };
            const expectedState = {
                1: {id: 1, body: 'This is a test team'},
                2: {id: 2, body: 'This is a test team', users: [10, 1, 2, 3]},
            };
            expect(teamsReducer.byIds(state, action)).toEqual(expectedState);
        });

        it('adds users to team on SAVE_ORG_MEMBER_SUCCESS with no users', () => {
            const state = {
                1: {id: 1, body: 'This is a test team'},
                2: {id: 2, body: 'This is a test team', users: [10]},
                3: {id: 3}
            };
            const action = {
                type: SAVE_ORG_MEMBER_SUCCESS,
                orgMemberData: {
                    teams: [1, 2]
                },
                response: {
                    entities: {
                        user: [{id: 1}]
                    }
                }
            };
            const expectedState = {
                1: {id: 1, body: 'This is a test team', users: [1]},
                2: {id: 2, body: 'This is a test team', users: [10, 1]},
                3: {id: 3},
            };
            expect(teamsReducer.byIds(state, action)).toEqual(expectedState);
        });

        it('returns initialState on unknown', () => {
            const action = {
                type: 'unknown',
            };
            expect(teamsReducer.byIds(initialState, action)).toEqual(initialState);
        });
    });

    describe('isFetching reducer', () => {
        it('returns true on FETCH_TEAM_REQUEST', () => {
            const action = { type: FETCH_TEAM_REQUEST };
            expect(teamsReducer.isFetching(false, action)).toBe(true);
        });
        it('returns false on FETCH_TEAM_SUCCESS', () => {
            const action = { type: FETCH_TEAM_SUCCESS };
            expect(teamsReducer.isFetching(true, action)).toBe(false);
        });
        it('returns false on FETCH_TEAM_FAILURE', () => {
            const action = { type: FETCH_TEAM_FAILURE };
            expect(teamsReducer.isFetching(true, action)).toBe(false);
        });
    });

    describe('getters', () => {
        it('returns ids when calling getIds', () => {
            const ids = [1, 2, 3];
            expect(teamsReducer.getIds({ ids })).toEqual(ids);
        });

        it('returns team when calling getTeam', () => {
            const team = {id: 1, org: 1, body: 'blah'};
            expect(teamsReducer.getTeam({ byIds: { 1: team } }, 1)).toEqual(team);
        });

        it('returns null when calling getTeam not found', () => {
            const team = {id: 1, org: 1, body: 'blah'};
            expect(teamsReducer.getTeam({ byIds: { 1: team } }, 2)).toEqual(null);
        });

        it('returns team when calling getTeamsByOrg', () => {
            const team = {id: 1, org: 1, body: 'blah'};
            expect(teamsReducer.getTeamsByOrg({ byIds: { 1: team } }, 1)).toEqual([team]);
        });

        it('returns null when calling getTeamsByOrg not found', () => {
            const team = {id: 1, org: 1, body: 'blah'};
            expect(teamsReducer.getTeamsByOrg({ byIds: { 1: team } }, 2)).toEqual([]);
        });

        it('returns false when not fetching', () => {
            expect(teamsReducer.getTeamIsFetching({isFetching: false})).toBe(false);
        });
    });

});
