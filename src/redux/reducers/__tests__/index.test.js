import * as reducers from '../../redux/reducers';
import * as fromUsersReducers from '../../redux/reducers/users';
import * as fromSesssionsReducers from '../../redux/reducers/sessions';
import * as fromActivitiesReducer from '../../redux/reducers/activities';
import * as activeSessionReducer from '../../redux/reducers/active_session';
import * as fromActivitySummariesReducers from '../../redux/reducers/activity_summaries';
import * as fromActivityRecordsReducers from '../../redux/reducers/activity_records';
import * as fromActivityUsersReducers from '../../redux/reducers/activity_users';
import * as fromTeamsReducers from '../../redux/reducers/teams';
import * as fromOrgMembersReducers from '../../redux/reducers/org_member';
import * as fromOrgSystemsReducer from '../../redux/reducers/org_system';
import * as fromTabsReducers from '../../redux/reducers/tabs';
import * as fromTagsReducers from '../../redux/reducers/tags';

jest.mock('../../redux/reducers/users');
jest.mock('../../redux/reducers/sessions');
jest.mock('../../redux/reducers/activities');
jest.mock('../../redux/reducers/active_session');
jest.mock('../../redux/reducers/activity_summaries');
jest.mock('../../redux/reducers/activity_records');
jest.mock('../../redux/reducers/activity_users');
jest.mock('../../redux/reducers/teams');
jest.mock('../../redux/reducers/org_member');
jest.mock('../../redux/reducers/org_system');
jest.mock('../../redux/reducers/systems');
jest.mock('../../redux/reducers/tabs');
jest.mock('../../redux/reducers/tags');


describe('reducers(index) suite', () => {

    describe('getAuthState method', () => {
        it('returns current authInfo', () => {
            const expectedAuthInfo = {
                loggedIn: false,
                userId: null,
                authToken: null,
            };
            const state = {
                auth: {
                    authInfo: expectedAuthInfo
                }
            };
            expect(reducers.getAuthState(state)).toEqual(expectedAuthInfo);
        });
    });

    describe('getLoggedInUser method', () => {
        it('returns current user', () => {
            const state = {
                auth: {
                    authInfo: {
                        loggedIn: true,
                        userId: 1
                    }
                }
            };
            const user = {id: 1};
            fromUsersReducers.getUser.mockImplementationOnce(() => user);
            expect(reducers.getLoggedInUser(state)).toEqual(user);
        });
        it('returns null if no user', () => {
            const state = {
                auth: {
                    authInfo: {
                        loggedIn: false,
                        userId: null
                    }
                }
            };
            expect(reducers.getLoggedInUser(state)).toEqual(null);
        });
    });

    describe('getLoggedInUserId method', () => {
        it('returns current user', () => {
            const state = {
                auth: {
                    authInfo: {
                        userId: 1
                    }
                }
            };
            expect(reducers.getLoggedInUserId(state)).toEqual(state.auth.authInfo.userId);
        });
    });

    describe('getCurrentTab method', () => {
        it('returns true', () => {
            fromTabsReducers.getCurrentTab.mockImplementationOnce(() => true);
            expect(reducers.getCurrentTab({})).toEqual(true);
        });
    });

    describe('getActiveSession method', () => {
        it('returns true', () => {
            activeSessionReducer.getActiveSession.mockImplementationOnce(() => true);
            expect(reducers.getActiveSession({})).toEqual(true);
        });
    });

    describe('getActiveActivity method', () => {
        it('returns true', () => {
            activeSessionReducer.getActiveActivity.mockImplementationOnce(() => true);
            expect(reducers.getActiveActivity({})).toEqual(true);
        });
    });

    describe('getSessionsIsFetching method', () => {
        it('calls getSessionsIsFetching', () => {
            fromSesssionsReducers.getSessionsIsFetching.mockImplementationOnce(() => true);
            expect(reducers.getSessionsIsFetching({})).toEqual(true);
        });
    });

    describe('getSessionsByOrg method', () => {
        it('returns true', () => {
            const state = {sessions: []};
            fromSesssionsReducers.getSessionsByOrg.mockImplementationOnce(() => true);
            expect(reducers.getSessionsByOrg(state, 1)).toEqual(true);
        });
    });


    describe('getActivityById method', () => {
        it('returns activity', () => {
            const activity = {id: 1};
            fromActivitiesReducer.getActivity.mockImplementationOnce(() => activity);
            expect(reducers.getActivityById({}, 1)).toEqual(activity);
        });
    });

    describe('getActivityIsFetching method', () => {
        it('returns true', () => {
            const state = {activity_users: []};
            fromActivitiesReducer.getActivityIsFetching.mockImplementationOnce(() => true);
            expect(reducers.getActivityIsFetching(state)).toEqual(true);
        });
    });

    describe('getActivitiesBySession method', () => {
        it('returns true', () => {
            const state = {activities: []};
            fromActivitiesReducer.getActivitiesBySession.mockImplementationOnce(() => true);
            expect(reducers.getActivitiesBySession(state, 1)).toEqual(true);
        });
    });

    describe('getActivitiesByUser method', () => {
        it('returns true', () => {
            const state = {activities: []};
            fromActivitiesReducer.getActivitiesByUser.mockImplementationOnce(() => true);
            expect(reducers.getActivitiesByUser(state, 1)).toEqual(true);
        });
    });

    describe('getActivityUsersByActivity method', () => {
        it('returns true', () => {
            const state = {activities: []};
            fromActivityUsersReducers.getActivityUsersByActivity.mockImplementationOnce(() => true);
            expect(reducers.getActivityUsersByActivity(state, 1)).toEqual(true);
        });
    });

    describe('getActivityUsersIsFetching method', () => {
        it('returns true', () => {
            fromActivityUsersReducers.getActivityUsersIsFetching.mockImplementationOnce(() => true);
            expect(reducers.getActivityUsersIsFetching({})).toEqual(true);
        });
    });

    describe('getActivitySummariesByActivity method', () => {
        it('returns activity_summary', () => {
            const state = {activity_summaries: []};
            const activity_summary = {id: 1};
            fromActivitySummariesReducers.getActivitySummariesByActivity.mockImplementationOnce(() => activity_summary);
            expect(reducers.getActivitySummariesByActivity(state, 1)).toEqual(activity_summary);
        });
    });

    describe('getActivitySummariesIsFetching method', () => {
        it('returns true', () => {
            const state = {activity_summaries: []};
            fromActivitySummariesReducers.getActivitySummariesIsFetching.mockImplementationOnce(() => true);
            expect(reducers.getActivitySummariesIsFetching(state)).toEqual(true);
        });
    });

    describe('getActivityRecords method', () => {
        it('returns activity_record', () => {
            const activity_record = {id: 1};
            fromActivityRecordsReducers.getActivityRecords.mockImplementationOnce(() => activity_record);
            expect(reducers.getActivityRecords({}, 1)).toEqual(activity_record);
        });
    });

    describe('getActivityRecordsByUser method', () => {
        it('returns activity_record', () => {
            const activity_record = {id: 1};
            fromActivityRecordsReducers.getActivityRecordsByUser.mockImplementationOnce(() => activity_record);
            expect(reducers.getActivityRecordsByUser({}, {athlete: {id: 1}})).toEqual(activity_record);
        });
    });

    describe('getActivityRecordsIsFetching method', () => {
        it('returns true', () => {
            fromActivityRecordsReducers.getActivityRecordsIsFetching.mockImplementationOnce(() => true);
            expect(reducers.getActivityRecordsIsFetching({})).toEqual(true);
        });
    });

    describe('getActivityRecordsIsSaving method', () => {
        it('returns true', () => {
            fromActivityRecordsReducers.getActivityRecordsIsSaving.mockImplementationOnce(() => true);
            expect(reducers.getActivityRecordsIsSaving({})).toEqual(true);
        });
    });

    describe('getTagById method', () => {
        it('returns tag', () => {
            const tag = {id: 1};
            fromTagsReducers.getTag.mockImplementationOnce(() => tag);
            expect(reducers.getTagById({}, 1)).toEqual(tag);
        });
    });

    describe('getTagLiveDataById method', () => {
        it('returns tag', () => {
            const tag = {id: 1};
            fromTagsReducers.getTagLiveData.mockImplementationOnce(() => tag);
            expect(reducers.getTagLiveDataById({}, 1)).toEqual(tag);
        });
    });

    describe('getTagsInLiveData method', () => {
        it('returns tag', () => {
            const tag = {id: 1};
            fromTagsReducers.getTagsInLiveData.mockImplementationOnce(() => [tag]);
            expect(reducers.getTagsInLiveData({}, 1)).toEqual([tag]);
        });
    });

    describe('getTagsBySystem method', () => {
        it('returns tag', () => {
            const tag = {id: 1};
            fromTagsReducers.getTagsBySystem.mockImplementationOnce(() => [tag]);
            expect(reducers.getTagsBySystem({}, 1)).toEqual([tag]);
        });
    });

    describe('getTags method', () => {
        it('returns tag', () => {
            const tag = {id: 1};
            fromTagsReducers.getTags.mockImplementationOnce(() => [tag]);
            expect(reducers.getTags({}, 1)).toEqual([tag]);
        });
    });

    describe('getCurrentTags method', () => {
        it('returns tag', () => {
            const tag = {id: 1,org:1};
            const org = {id: 1};
            const state = {tags:{byIds:{1:tag}}};
            fromTagsReducers.getTags.mockImplementationOnce(() => [tag]);
            expect(reducers.getCurrentTags(state)).toEqual([tag]);
        });
    });

    describe('getTeam method', () => {
        it('returns user', () => {
            const team = {id: 1};
            const state = {teams: {1: team}};
            fromTeamsReducers.getTeam.mockImplementationOnce(() => team);
            expect(reducers.getTeam(state, 1)).toEqual(team);
        });
    });

    describe('getTeamIsFetching method', () => {
        it('returns true', () => {
            const state = {teams: []};
            fromTeamsReducers.getTeamIsFetching.mockImplementationOnce(() => true);
            expect(reducers.getTeamIsFetching(state)).toEqual(true);
        });
    });

    describe('getTeamsByCurrentOrg method', () => {
        it('returns true', () => {
            const state = {sessions: []};
            const org = {id: 1};
            fromOrgSystemsReducer.getCurrentOrg.mockImplementationOnce(() => org);
            fromTeamsReducers.getTeamsByOrg.mockImplementationOnce(() => true);
            expect(reducers.getTeamsByCurrentOrg(state, 1)).toEqual(true);
        });
    });

    describe('getMembersByOrg method', () => {
        it('returns true', () => {
            const state = {sessions: []};
            const org = {id: 1};
            fromOrgSystemsReducer.getCurrentOrg.mockImplementationOnce(() => org);
            fromOrgMembersReducers.getMembersByOrg.mockImplementationOnce(() => true);
            expect(reducers.getMembersByOrg(state, 1)).toEqual(true);
        });
    });

    describe('getUserById method', () => {
        it('returns user', () => {
            const user = {id: 1};
            const state = {users: {1: user}};
            fromUsersReducers.getUser.mockImplementationOnce(() => user);
            expect(reducers.getUserById(state, 1)).toEqual(user);
        });
    });

    describe('getUserIsFetching method', () => {
        it('returns true', () => {
            const state = {users: []};
            fromUsersReducers.getUserIsFetching.mockImplementationOnce(() => true);
            expect(reducers.getUserIsFetching(state)).toEqual(true);
        });
    });

});
