import * as activitiesReducer from '../../redux/reducers/activities';
import {
    FETCH_ACTIVITY_REQUEST,
    FETCH_ACTIVITY_SUCCESS,
    FETCH_ACTIVITY_FAILURE,
    SAVE_ACTIVITY_SUCCESS
} from '../../redux/constants/actionTypes';


describe('activitiesReducer suite', () => {

    describe('ids reducer', () => {

        it('adds new ids on FETCH_ACTIVITY_SUCCESS', () => {
            const result = [1, 2, 3];
            const action = {
                type: FETCH_ACTIVITY_SUCCESS,
                response: {
                    result
                }
            };
            expect(activitiesReducer.ids([], action)).toEqual(result);
        });

        it('adds new ids on SAVE_ACTIVITY_SUCCESS', () => {
            const result = [1, 2, 3];
            const action = {
                type: SAVE_ACTIVITY_SUCCESS,
                response: {
                    result
                }
            };
            expect(activitiesReducer.ids([], action)).toEqual(result);
        });

        it('returns initial state on unknown', () => {
            const action = {
                type: 'unknown',
            };
            expect(activitiesReducer.ids([], action)).toEqual([]);
        });

    });

    describe('byIds reducer', () => {
        let initialState = {};

        beforeEach(() => {
            initialState = {
                1: {
                    id: 1,
                    name: 'This is a test activity',
                }
            };
        });

        it('adds new activity on FETCH_ACTIVITY_SUCCESS', () => {
            const activities = [
                {id: 1, name: 'This is a test activity'},
                {id: 2, name: 'This is a test activity'},
            ];
            const action = {
                type: FETCH_ACTIVITY_SUCCESS,
                response: {
                    entities: {
                        activity: activities
                    }
                }
            };
            const expectedState = {
                ...initialState,
                ...activities,
            };
            expect(activitiesReducer.byIds(initialState, action)).toEqual(expectedState);
        });

        it('adds new activity on SAVE_ACTIVITY_SUCCESS', () => {
            const activities = [
                {id: 1, name: 'This is a test activity'},
                {id: 2, name: 'This is a test activity'},
            ];
            const action = {
                type: SAVE_ACTIVITY_SUCCESS,
                response: {
                    entities: {
                        activity: activities
                    }
                }
            };
            const expectedState = {
                ...initialState,
                ...activities,
            };
            expect(activitiesReducer.byIds(initialState, action)).toEqual(expectedState);
        });

        it('returns initialState on FETCH_ACTIVITY_SUCCESS with no response', () => {
            const action = {
                type: FETCH_ACTIVITY_SUCCESS,
            };
            expect(activitiesReducer.byIds(initialState, action)).toEqual(initialState);
        });

        it('returns initialState on unknown', () => {
            const action = {
                type: 'unknown',
            };
            expect(activitiesReducer.byIds(initialState, action)).toEqual(initialState);
        });
    });

    describe('isFetching reducer', () => {
        it('returns true on FETCH_ACTIVITY_REQUEST', () => {
            const action = { type: FETCH_ACTIVITY_REQUEST };
            expect(activitiesReducer.isFetching(false, action)).toBe(true);
        });
        it('returns false on FETCH_ACTIVITY_SUCCESS', () => {
            const action = { type: FETCH_ACTIVITY_SUCCESS };
            expect(activitiesReducer.isFetching(true, action)).toBe(false);
        });
        it('returns false on FETCH_ACTIVITY_FAILURE', () => {
            const action = { type: FETCH_ACTIVITY_FAILURE };
            expect(activitiesReducer.isFetching(true, action)).toBe(false);
        });
    });

    describe('getters', () => {
        it('returns ids when calling getIds', () => {
            const ids = [1, 2, 3];
            expect(activitiesReducer.getIds({ ids })).toEqual(ids);
        });

        it('returns activity when calling getActivity', () => {
            const activity = {id: 1, session: 1, name: 'blah'};
            expect(activitiesReducer.getActivity({ byIds: { 1: activity } }, 1)).toEqual(activity);
        });

        it('returns null when calling getActivity not found', () => {
            const activity = {id: 1, session: 1, name: 'blah'};
            expect(activitiesReducer.getActivity({ byIds: { 1: activity } }, 2)).toEqual(null);
        });

        it('returns activity when calling getActivitiesBySession', () => {
            const activity = {id: 1, session: 1, name: 'blah'};
            expect(activitiesReducer.getActivitiesBySession({ byIds: { 1: activity } }, 1)).toEqual([activity]);
        });

        it('returns null when calling getActivitiesBySession not found', () => {
            const activity = {id: 1, session: 1, name: 'blah'};
            expect(activitiesReducer.getActivitiesBySession({ byIds: { 1: activity } }, 2)).toEqual([]);
        });

        it('returns activity when calling getActivitiesByUser', () => {
            const activity = {id: 1, activity_users: [{user: 1}], name: 'blah'};
            expect(activitiesReducer.getActivitiesByUser({ byIds: { 1: activity } }, 1)).toEqual([activity]);
        });

        it('returns null when calling getActivitiesByUser not found', () => {
            const activity = {id: 1, activity_users: [{user: 1}], name: 'blah'};
            expect(activitiesReducer.getActivitiesByUser({ byIds: { 1: activity } }, 2)).toEqual([]);
        });

        it('returns false when not fetching', () => {
            expect(activitiesReducer.getActivityIsFetching({isFetching: false})).toBe(false);
        });
    });

});
