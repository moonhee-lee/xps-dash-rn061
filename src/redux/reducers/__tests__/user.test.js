import * as usersReducer from '../../redux/reducers/users';
import {
    LOGIN_SUCCESS,
    FETCH_USER_REQUEST,
    FETCH_USER_SUCCESS,
    FETCH_USER_FAILURE,
    SET_USER_IMAGE,
    SAVE_LOCAL_USER_IMAGE,
} from '../../redux/constants/actionTypes';


describe('usersReducer suite', () => {

    describe('ids reducer', () => {
        it('returns initialState on unknown', () => {
            const action = {
                type: 'unknown',
                response: {
                    result: 23,
                    entities: {
                        user: []
                    },
                }
            };
            const expectedState = [];
            expect(usersReducer.ids(undefined, action)).toEqual(expectedState);
        });

        it('adds new ids on LOGIN_SUCCESS', () => {
            const action = {
                type: LOGIN_SUCCESS,
                response: {
                    result: 23,
                    entities: {
                        user: []
                    },
                }
            };
            const expectedState = [23];
            expect(usersReducer.ids(undefined, action)).toEqual(expectedState);
        });

    });

    describe('byIds reducer', () => {
        let initialState = {};

        it('returns initialState on unknown', () => {
            const action = {
                type: 'unknown',
            };
            expect(usersReducer.byIds(undefined, action)).toEqual(initialState);
        });

        it('adds new ids on LOGIN_SUCCESS', () => {
            const users = {2: {id: 2, first_name: 'test name 2'}};
            const action = {
                type: LOGIN_SUCCESS,
                response: {
                    result: users[2].id,
                    entities: {
                        user: users
                    }
                }
            };
            const expectedState = {
                ...initialState,
                ...users,
            };
            expect(usersReducer.byIds(undefined, action)).toEqual(expectedState);
        });

        it('returns initialState on LOGIN_SUCCESS with no user', () => {
            const action = {
                type: LOGIN_SUCCESS
            };
            expect(usersReducer.byIds(undefined, action)).toEqual(initialState);
        });

        it('returns initialState on SAVE_LOCAL_USER_IMAGE with no user image', () => {
            const action = {
                type: SAVE_LOCAL_USER_IMAGE,
                imageKey: null,
                userId:1
            };
            const user = {1:{id:1}};
            expect(usersReducer.byIds(user, action)).toEqual(user);
        });

        it('returns initialState on SAVE_LOCAL_USER_IMAGE without matching user', () => {
            const action = {
                type: SAVE_LOCAL_USER_IMAGE,
                imageKey: 'test',
                userId:2
            };
            const user = {1:{id:1}};
            expect(usersReducer.byIds(user, action)).toEqual(user);
        });

        it('returns initialState on SAVE_LOCAL_USER_IMAGE with user', () => {
            const action = {
                type: SAVE_LOCAL_USER_IMAGE,
                imageKey: 'test',
                userId:1
            };
            const user = {1:{id:1, local_image: 'test'}};
            expect(usersReducer.byIds(user, action)).toEqual(user);
        });

        it('returns initialState on SET_USER_IMAGE with no image', () => {
            const action = {
                type: SET_USER_IMAGE,
                imageKey: null,
                userId:1
            };
            const user = {1:{id:1}};
            expect(usersReducer.byIds(user, action)).toEqual(user);
        });

        it('returns initialState on SET_USER_IMAGE with no matching user', () => {
            const action = {
                type: SET_USER_IMAGE,
                imageKey: 'test',
                userId:2
            };
            const user = {1:{id:1}};
            expect(usersReducer.byIds(user, action)).toEqual(user);
        });

        it('returns initialState on SET_USER_IMAGE with no user', () => {
            const action = {
                type: SET_USER_IMAGE,
                imageKey: 'test',
                userId:1
            };
            const user = {1:{id:1, user_image: 'test'}};
            expect(usersReducer.byIds(user, action)).toEqual(user);
        });

    });

    describe('isFetching reducer', () => {
        it('returns true on FETCH_USER_REQUEST', () => {
            const action = { type: FETCH_USER_REQUEST };
            expect(usersReducer.isFetching(false, action)).toBe(true);
        });
        it('returns false on FETCH_USER_SUCCESS', () => {
            const action = { type: FETCH_USER_SUCCESS };
            expect(usersReducer.isFetching(true, action)).toBe(false);
        });
        it('returns false on FETCH_USER_FAILURE', () => {
            const action = { type: FETCH_USER_FAILURE };
            expect(usersReducer.isFetching(true, action)).toBe(false);
        });
        it('returns state', () => {
            const action = { type:  null };
            expect(usersReducer.isFetching(true, action)).toBe(true);
        });
    });

    describe('getters', () => {
        it('returns ids when calling getIds', () => {
            const ids = [1, 2, 3];
            expect(usersReducer.getIds({ ids: ids })).toEqual(ids);
        });

        it('returns message when calling getUser', () => {
            const user = {id: 1, display_name: 'Test User'};
            expect(usersReducer.getUser({ byIds: { 1: user } }, 1)).toEqual(user);
        });

        it('returns null when calling getUser', () => {
            const user = {id: 1, display_name: 'Test User'};
            expect(usersReducer.getUser({ byIds: { 1: user } }, 3)).toEqual(null);
        });

        it('returns false when not fetching', () => {
            expect(usersReducer.getUserIsFetching({isFetching: false})).toBe(false);
        });

    });

});

