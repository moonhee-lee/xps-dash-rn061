import _ from 'lodash';
import { combineReducers } from 'redux';
import {
    CURRENT_TEAM_FETCH_SUCCESS,
    LOGIN_SUCCESS,
    FETCH_SESSION_REQUEST,
    FETCH_SESSION_SUCCESS,
    FETCH_SESSION_FAILURE,
    FETCH_TEAM_REQUEST,
    FETCH_TEAM_SUCCESS,
    FETCH_TEAM_FAILURE,
    FETCH_USER_BY_TEAM_SUCCESS,
    SAVE_ORG_MEMBER_SUCCESS,
    SYSTEM_TAG_REMOVED,
    REMOVE_LOCAL_ORG_MEMBER
} from '../constants/actionTypes';


/**
 * reducers/team - ids
 *
 * contains all of the team ids that we know about in the system.
 * 
 * @param  {Object} state - current 'teams.ids' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const ids = (state = [], action) => {
    switch (action.type) {
        case FETCH_TEAM_SUCCESS:
        case FETCH_SESSION_SUCCESS:
            return [...state, ...action.response.result];
        default:
            return state;
    }
}


/**
 * reducers/team - byIds
 *
 * contains all of the team objects that we know about.
 * these are the fully serialized objects that we retreive from the API.
 * 
 * @param  {Object} state - current 'teams.byIds' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const byIds = (state = {}, action) => {
    switch (action.type) {
        case FETCH_TEAM_SUCCESS:
        case FETCH_SESSION_SUCCESS:
            if (action.response) {
                return {
                    ...state,
                    ...action.response.entities.team,
                };
            }
            return state;

        case FETCH_USER_BY_TEAM_SUCCESS:
            return _.mapValues(state, (team) => {
                if (team.id === action.teamId) {
                    if (team.users) {
                        return {
                            ...team,
                            users: [...team.users, ...action.response.result]
                        };
                    } else {
                        return {
                            ...team,
                            users: action.response.result
                        };
                    }
                } else {
                    return team;
                }
            });
        case SAVE_ORG_MEMBER_SUCCESS:
            return _.mapValues(state, (team) => {
                if (action.orgMemberData && action.orgMemberData.teams &&action.orgMemberData.teams.includes(team.id)) {
                    const new_users = _.map(action.response.entities.user, (u) => u.id);
                    if (team.users) {
                        return {
                            ...team,
                            users: [...team.users, ...new_users]
                        };
                    } else {
                        return {
                            ...team,
                            users: new_users
                        };
                    }
                } else {
                    return team;
                }
            })

        default:
            return state;
    }
}

export const currentTeam = (state = false, action) => {
    switch (action.type) {
        case CURRENT_TEAM_FETCH_SUCCESS:
            return {
                team: action.currentTeam
            };
        default:
            return state;
    }
}


/**
 * reducers/team - isFetching
 *
 * simple reducer so that we always know if we are fetching a team or not
 * 
 * @param  {Object} state - current 'messages.isFetching' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const isFetching = (state = false, action) => {
    switch (action.type) {
        case FETCH_TEAM_REQUEST:
            return true;
        case FETCH_TEAM_SUCCESS:
        case FETCH_TEAM_FAILURE:
            return false;
    }
    return state;
}

// combine all of these reducers under the 'teams' namespace.
export default combineReducers({
    ids,
    byIds,
    currentTeam,
    isFetching,
});


// some 'getters' for our current 'teams' state.
export const getIds = (state) => state.ids;
export const getTeam = (state, id) => {
    if (id in state.byIds) return state.byIds[id];
    return null;
}
export const getCurrentTeam = (state) => {
    if(state.teams.currentTeam && state.teams.currentTeam.team) return state.teams.currentTeam.team;
    return 1;
}
export const getCurrentTeamData = (state) => {
    const id = state.teams.byIds.currentTeam;
    if (id in state.byIds) return state.byIds[id];
    return null;
}
export const getTeams = (state) => {
    return state.byIds;
}
export const getTeamsByOrg = (state, orgId) => {
    return _.filter(state.byIds, (t) => t.org === orgId);
}
export const getTeamIsFetching = (state) => state.isFetching;
