import _ from 'lodash';
import { combineReducers } from 'redux';
import {
    FETCH_ORG_SYSTEM_BY_ORG_FAILURE,
    FETCH_ORG_SYSTEM_BY_ORG_REQUEST,
    FETCH_ORG_SYSTEM_BY_ORG_SUCCESS,
    SET_CURRENT_ORG
} from '../constants/actionTypes';

export const ids = (state = [], action) => {
    switch (action.type) {
        case FETCH_ORG_SYSTEM_BY_ORG_SUCCESS:
            return [...state, ...action.response.result];
        default:
            return state;
    }
}

export const byIds = (state = {}, action) => {
    switch (action.type) {
        case FETCH_ORG_SYSTEM_BY_ORG_SUCCESS:
            if (action.response) {
                return {
                    ...state,
                    ...action.response.entities.org_system,
                };
            }
            return state;
        default:
            return state;
    }
}

export const currentOrg = (state = {}, action) => {
    switch (action.type) {
        case SET_CURRENT_ORG:
            return {
                currentOrg: action.org
            }
        default:
            return state;
    }
}


export const isFetching = (state = false, action) => {
    switch (action.type) {
        case FETCH_ORG_SYSTEM_BY_ORG_REQUEST:
            return true;
        case FETCH_ORG_SYSTEM_BY_ORG_SUCCESS:
        case FETCH_ORG_SYSTEM_BY_ORG_FAILURE:
            return false;
    }
    return state;
}

// combine all of these reducers under the 'org_system' namespace.
export default combineReducers({
    ids,
    byIds,
    currentOrg,
    isFetching,
});

// some 'getters' for our current 'org_system' state.
export const getIds = (state) => state.ids;
export const getOrgSystem = (state, id) => {
    if (id in state.byIds) return state.byIds[id];
    return null;
}

export const getSystemsByOrg = (state, orgId) => {
    if(global.debugMode) {
        _.assign(state.byIds, {
                999: {
                    hostname: "3.217.124.56",
                    id: 999,
                    name: "AWS Broker",
                    serial_num: "X5BQFXJR808VO2",
                    wlan_password: null,
                    wlan_ssid: null
                }
            }
        )
    }

    return state.byIds;
    //return _.filter(state.byIds, (t) => t.org === orgId);
}

export const getCurrentOrg = (state) => {
    if(state && state.currentOrg && state.currentOrg.currentOrg) return {id:state.currentOrg.currentOrg};
    return {id: 1};
}
