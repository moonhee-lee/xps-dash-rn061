import { combineReducers } from 'redux';

import activitiesReducer, * as fromActivitiesReducers from './activities';
import liveActivityReducer, * as fromLiveActivityReducers from './live_activity';
import activityRecordsReducer, * as fromActivityRecordsReducers from './activity_records';
import activityDataReducer, * as fromActivityDataReducers from './activity_data';
import activityUsersReducer, * as fromActivityUsersReducers from './activity_users';
import activeUsersReducer, * as fromActiveUsersReducers from './active_users';
import activitySummariesReducer, * as fromActivitySummariesReducers from './activity_summaries';
import authReducer, * as fromAuthReducer from './auth';
import systemsReducer, * as fromSystemsReducer from './systems';
import sessionsReducer, * as fromSessionsReducers from './sessions';
import tagsReducer, * as fromTagsReducers from './tags';
import teamsReducer, * as fromTeamsReducers from './teams';
import orgMembersReducer, * as fromOrgMembersReducers from './org_member';
import orgSystemsReducer, * as fromOrgSystemsReducers from './org_system';
import usersReducer, * as fromUsersReducers from './users';
import tabsReducer, * as fromTabsReducer from './tabs';
import wifiReducer, * as fromWifiReducer from './wifi';
import cloudReducer, * as fromCloudReducer from './cloud';
import firmwareReducer, * as fromFirmwareReducer from './firmware';
import activeSessionReducer, * as fromActiveSessionReducer from './active_session';

/**
 * Our 'getter' methods follow. These getters are used throughout our application, 
 * though mostly in the Components. So they all only pass in the entire state.
 * These getters then call the appropriate reducer's getter with the subset of the state
 */


/**
 * reducers - getAuthState
 *
 * returns our entire auth state object.
 * 
 * @param  {Object} state - full application state
 * @return {Object} - our current auth state.
 */
export const getAuthState = (state) =>
    fromAuthReducer.getAuthState(state.auth);


/**
 * reducers - getLoggedInUser
 *
 * returns the user object for the currently logged in user
 * 
 * @param  {Object} state - full application state
 * @return {Object} - our current user
 */
export const getLoggedInUser = (state) => {
    const userId = fromAuthReducer.getLoggedInUserId(state.auth);
    if (userId) return fromUsersReducers.getUser(state.users, userId);
    return null;
}

export const getLoggedInLocalUser = (state) => {
    const user = fromAuthReducer.getLoggedInUser(state.auth);
    return user;
}


/**
 * reducers - getLoggedInUserId
 *
 * returns the userId for the currently logged in user
 * 
 * @param  {Object} state - full application state
 * @return {Integer} - our current user's id
 */
export const getLoggedInUserId = (state) =>
    fromAuthReducer.getLoggedInUserId(state.auth);


/**
 * reducers - getCurrentTab
 *
 * returns the active tab
 *
 * @param  {Object} state - full application state
 * @return {Integer} - the active tab id
 */
export const getCurrentTab = (state) =>
    fromTabsReducer.getCurrentTab(state.tabs);


/**
 * reducers - getActiveSession
 *
 * returns the active session
 *
 * @param  {Object} state - full application state
 * @return {Object} - the active session
 */
export const getActiveSession = (state) =>
    fromActiveSessionReducer.getActiveSession(state.active_session)

/**
 * reducers - getActiveSession
 *
 * returns the active session
 *
 * @param  {Object} state - full application state
 * @return {Object} - the active session
 */
export const getActiveActivity = (state) =>
    fromActiveSessionReducer.getActiveActivity(state.active_session)


export const getCurrentOrg = (state) =>
    fromOrgSystemsReducers.getCurrentOrg(state.org_systems);

export const getCurrentSystem = (state) => {
    // TODO
    return fromSystemsReducer.getSystem(state.systems, 1);
}

export const getSessionsIsFetching = (state) => 
    fromSessionsReducers.getSessionsIsFetching(state.sessions);

export const getSessionsByOrg = (state, orgId) =>
    fromSessionsReducers.getSessionsByOrg(state.sessions, orgId);

export const getActivityById = (state, id) =>
    fromActivitiesReducers.getActivity(state.activities, id);

export const getActivityIsFetching = (state) => 
    fromActivitiesReducers.getActivityIsFetching(state.activities);

export const getActivities = (state) =>
    fromActivitiesReducers.getActivities(state.activities);

export const getActivitiesBySession = (state, sessionId) =>
    fromActivitiesReducers.getActivitiesBySession(state.activities, sessionId);

export const getActivitiesForSessions = (state) => {
    const currentOrg = getCurrentOrg(state);
    const sessions = getSessionsByOrg(state,currentOrg.id);
    return fromActivitiesReducers.getActivitiesForSessions(state.activities, sessions);
}

export const getActivitiesByUser = (state, userId) =>
    fromActivitiesReducers.getActivitiesByUser(state.activities, userId);

export const getLocalActivities = (state) =>
    fromActivitiesReducers.getLocalActivities(state.activities);

export const getActivitiesByUserId = (state, userId) =>
    fromActivitiesReducers.getActivitiesByUserId(state.activities, userId);

export const getActivityUsersByActivity = (state, activityId) =>
    fromActivityUsersReducers.getActivityUsersByActivity(state.activity_users, activityId);

export const getActivityUsersIsFetching = (state) =>
    fromActivityUsersReducers.getActivityUsersIsFetching(state.activity_users);

export const getActiveUsers = (state) =>
    fromActiveUsersReducers.getActiveUsers(state.active_users);

export const getActiveUsersIsFetching = (state) =>
    fromActiveUsersReducers.getActiveUsersIsFetching(state.active_users);

export const getActivitySummariesByActivity = (state, sessionId) =>
    fromActivitySummariesReducers.getActivitySummariesByActivity(state.activity_summaries, sessionId);

export const getActivitySummariesIsFetching = (state) => 
    fromActivitySummariesReducers.getActivitySummariesIsFetching(state.activity_summaries);

export const getActivityDataById = (state, recordId) =>
    fromActivityDataReducers.getActivityDataById(state.activity_data, recordId);

export const getActivityData = (state) =>
    fromActivityDataReducers.getActivityData(state.activity_data);

export const getActivityDataIsFetching = (state) =>
    fromActivityDataReducers.getActivityDataIsFetching(state.activity_data);

export const getActivityRecordById = (state, recordId) =>
    fromActivityRecordsReducers.getActivityRecordById(state.activity_records, recordId);

export const getActivityRecords = (state, activityId, userId) =>
    fromActivityRecordsReducers.getActivityRecords(state.activity_records, activityId, userId);

export const getActivityRecordsByUser = (state, props) =>
    fromActivityRecordsReducers.getActivityRecordsByUser(state.activity_records, props.athlete.id);

export const getActivityRecordsByUserId = (state, userId) =>
    fromActivityRecordsReducers.getActivityRecordsByUser(state.activity_records, userId);

export const getActivityRecordsByType = (state, type_hash) =>
    fromActivityRecordsReducers.getActivityRecordsByType(state.activity_records, type_hash);

export const getActivityRecordsBySessionIdAndType = (state, sessionId, type_hash) =>
    fromActivityRecordsReducers.getActivityRecordsBySessionIdAndType(state.activity_records, sessionId, type_hash);

export const getActivityRecordsBySessionIdAndActivity = (state, sessionId, activity) =>
    fromActivityRecordsReducers.getActivityRecordsBySessionIdAndActivity(state.activity_records, sessionId, activity);

export const getActivityRecordsIsFetching = (state) =>
    fromActivityRecordsReducers.getActivityRecordsIsFetching(state.activity_records);

export const getActivityRecordsIsSaving = (state) => 
    fromActivityRecordsReducers.getActivityRecordsIsSaving(state.activity_records);

export const getActivityIsSaving = (state) => 
    fromActivitiesReducers.getActivityIsSaving(state.activities);

export const getTagById = (state, tagId) =>
    fromTagsReducers.getTag(state.tags, tagId);

export const getTagLiveDataById = (state, tagId) =>
    fromTagsReducers.getTagLiveData(state.tags, tagId);

export const getTagsInLiveData = (state) =>
    fromTagsReducers.getTagsInLiveData(state.tags);

export const getTagsBySystem = (state, systemId) =>
    fromTagsReducers.getTagsBySystem(state.tags, systemId);

export const getTags = (state) =>
    fromTagsReducers.getTags(state.tags);

export const getCurrentTags = (state) =>
    fromTagsReducers.getTags(state.tags);

export const getTeam = (state, teamId) =>
    fromTeamsReducers.getTeam(state.teams, teamId);

export const getTeams = (state, teamId) =>
    fromTeamsReducers.getTeams(state.teams);

export const getCurrentTeam = (state) =>
    fromTeamsReducers.getCurrentTeam(state);

export const getCurrentTeamData = (state) =>
    fromTeamsReducers.getCurrentTeamData(state);

export const getTeamIsFetching = (state) => 
    fromTeamsReducers.getTeamIsFetching(state.teams);

export const getTeamsByCurrentOrg = (state) => {
    const currentOrg = getCurrentOrg(state);
    return fromTeamsReducers.getTeamsByOrg(state.teams, currentOrg.id);
}

export const getMembersByOrg = (state) => {
    const currentOrg = getCurrentOrg(state);
    return fromOrgMembersReducers.getMembersByOrg(state.org_members, currentOrg.id);
}

export const getSystemsByOrg = (state) => {
    const currentOrg = getCurrentOrg(state);
    return fromOrgSystemsReducers.getSystemsByOrg(state.org_systems, currentOrg.id);
}

export const getUserById = (state, id) =>
    fromUsersReducers.getUser(state.users, id);

export const getUserIsFetching = (state) =>
    fromUsersReducers.getUserIsFetching(state.users);

export const getLiveActivity = (state) =>
    fromLiveActivityReducers.getLiveActivity(state);

export const getWifiState = (state) =>
    fromWifiReducer.getWifiState(state.wifi);

export const getCloudState = (state) =>
    fromCloudReducer.getCloudState(state.cloud);

export const getLocalDataTotal = (state) =>
    fromCloudReducer.getLocalDataTotal(state.cloud);

export const getLocalDataCounts = (state) =>
    fromCloudReducer.getLocalDataCounts(state.cloud);

export const getIsFirmWareUpdating = (state) =>
    fromSystemsReducer.getIsFirmWareUpdating(state.systems);

export const getFirmware = (state) =>
    fromFirmwareReducer.getFirmware(state.firmware);


/**
 * reducers - combineReducers
 *
 * our "root" reducer setup, combines our auth, users, .... etc.. reducers
 * this is the reducer that we set up our inital store with.
 * 
 */
export default combineReducers({
    activities: activitiesReducer,
    live_activity: liveActivityReducer,
    activity_records: activityRecordsReducer,
    activity_data: activityDataReducer,
    activity_users: activityUsersReducer,
    active_users: activeUsersReducer,
    activity_summaries: activitySummariesReducer,
    auth: authReducer,
    systems: systemsReducer,
    sessions: sessionsReducer,
    tags: tagsReducer,
    teams: teamsReducer,
    users: usersReducer,
    tabs: tabsReducer,
    active_session: activeSessionReducer,
    org_members: orgMembersReducer,
    org_systems: orgSystemsReducer,
    wifi: wifiReducer,
    cloud: cloudReducer,
    firmware: firmwareReducer,
});
