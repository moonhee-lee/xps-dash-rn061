import _ from 'lodash';
import { combineReducers } from 'redux';
import {
    FETCH_SESSION_REQUEST,
    FETCH_SESSION_SUCCESS,
    FETCH_SESSION_FAILURE,
    SAVE_SESSION_SUCCESS,
    SAVE_LOCAL_SESSION_SUCCESS,
    REMOVE_LOCAL_SESSION
} from '../constants/actionTypes';


/**
 * reducers/session - ids
 *
 * contains all of the session ids that we know about in the system.
 * 
 * @param  {Object} state - current 'sessions.ids' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const ids = (state = [], action) => {
    switch (action.type) {
        case FETCH_SESSION_SUCCESS:
        case SAVE_SESSION_SUCCESS:
        case SAVE_LOCAL_SESSION_SUCCESS:
            return [...state, ...action.response.result];
        default:
            return state;
    }
}


/**
 * reducers/session - byIds
 *
 * contains all of the session objects that we know about.
 * these are the fully serialized objects that we retreive from the API.
 * 
 * @param  {Object} state - current 'sessions.byIds' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const byIds = (state = {}, action) => {
    switch (action.type) {
        case FETCH_SESSION_SUCCESS:
        case SAVE_SESSION_SUCCESS:
        case SAVE_LOCAL_SESSION_SUCCESS:
            if (action.response) {
                return {
                    ...state,
                    ...action.response.entities.session,
                };
            }
            return state;

        case REMOVE_LOCAL_SESSION:
            if(action.localID) {
                const sessions = _.omitBy(state, (session) => session.localID === action.localID);
                return sessions;
            }
            else return state;

        default:
            return state;
    }
}


/**
 * reducers/session - isFetching
 *
 * simple reducer so that we always know if we are fetching a session or not
 * 
 * @param  {Object} state - current 'messages.isFetching' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const isFetching = (state = false, action) => {
    switch (action.type) {
        case FETCH_SESSION_REQUEST:
            return true;
        case FETCH_SESSION_SUCCESS:
        case FETCH_SESSION_FAILURE:
            return false;
    }
    return state;
}

// combine all of these reducers under the 'sessions' namespace.
export default combineReducers({
    ids,
    byIds,
    isFetching,
});


// some 'getters' for our current 'sessions' state.
export const getIds = (state) => state.ids;
export const getSession = (state, id) => {
    if (id in state.byIds) return state.byIds[id];
    return null;
}
export const getSessionsByOrg = (state, orgId) => {
    return _.filter(state.byIds, (s) => s.org === orgId);
}
export const getSessionsIsFetching = (state) => state.isFetching;
