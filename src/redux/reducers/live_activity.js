import _ from 'lodash';
import { combineReducers } from 'redux';
import {
    FETCH_LIVE_ACTIVITY_SUCCESS,
    SAVE_LIVE_ACTIVITY_SUCCESS,
    SET_LIVE_ACTIVITY_SESSION,
    FETCH_LIVE_ACTIVITY_SESSION,
    SET_LIVE_ACTIVITY_TEAM
} from '../constants/actionTypes';

export const liveActivity = (state = {}, action) => {
    switch (action.type) {
        case FETCH_LIVE_ACTIVITY_SUCCESS:
        case SAVE_LIVE_ACTIVITY_SUCCESS:
            if (action.response) {
                return {
                    ...state,
                    ...action.response.entities.activity.live_activity,
                };
            }
            return state;
        case SET_LIVE_ACTIVITY_SESSION:
        case FETCH_LIVE_ACTIVITY_SESSION:
            if(action.session) {
                console.log('reducing live activity session: '+action.session.id);
                return {
                    ...state,
                    session: action.session.id
                }
            }
            return state;
        case SET_LIVE_ACTIVITY_TEAM:
            if(action.teamId) {
                console.log('reducing live activity team: '+action.teamId);
                return {
                    ...state,
                    team: action.teamId,
                    session: {
                        ...state.session,
                        team: action.teamId
                    }
                }
            }
            return state;
        default:
            return state;
    }
}


// combine all of these reducers under the 'activities' namespace.
export default combineReducers({
    liveActivity,
});

export const getLiveActivity = (state) => {
    if(state.live_activity) return state.live_activity.liveActivity;
    return null;
}
