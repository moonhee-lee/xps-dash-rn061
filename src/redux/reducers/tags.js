import _ from 'lodash';
import { combineReducers } from 'redux';
import { resetMessages } from '../../redux/local_storage';
import {
    FETCH_TAG_REQUEST,
    FETCH_TAG_SUCCESS,
    FETCH_TAG_FAILURE,
    SYSTEM_TAG_INITIALIZED,
    SYSTEM_MESSAGE_RECEIVED,
    SYSTEM_SPRINT_MOTION_DISTANCE,
    SYSTEM_SPRINT_ENDED,
    SYSTEM_JUMP_DETECTED,
    SYSTEM_COD_DETECTED,
    ACTIVITY_TOGGLE_RECORDING_MODE,
    ACTIVITY_RESET_RECORDING_MODE,
    SYSTEM_TAG_READY,
    SYSTEM_TAG_NOT_READY,
    SYSTEM_TAG_SELECT_CONFIRMED,
    SYSTEM_TAG_REMOVED,
    FETCH_ACTIVITY_USER_SUCCESS,
    FETCH_ACTIVE_USER_SUCCESS,
    SET_ACTIVITY_USER_VELOCITY,
    SET_TAG_POSITION,
    SET_TAG_VELOCITY,
    ACTIVITY_RESET_LIVE_RECORDING_MODE,
    SET_TAG_BATTERY,
    CLEAR_USER_TAG_DATA
} from '../constants/actionTypes';


/**
 * reducers/tag - ids
 *
 * contains all of the tag ids that we know about in the system.
 *
 * @param  {Object} state - current 'tags.ids' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const ids = (state = [], action) => {
    switch (action.type) {
        case FETCH_TAG_SUCCESS:
            return [...state, ...action.response.result];
        default:
            return state;
    }
}


/**
 * reducers/tag - byIds
 *
 * contains all of the tag objects that we know about.
 * these are the fully serialized objects that we retreive from the API.
 *
 * @param  {Object} state - current 'tags.byIds' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const byIds = (state = {}, action) => {
    switch (action.type) {
        case FETCH_TAG_SUCCESS:
        case FETCH_ACTIVITY_USER_SUCCESS:
        case FETCH_ACTIVE_USER_SUCCESS:
            if (action.response) {
                return {
                    ...state,
                    ...action.response.entities.tag,
                };
            }
            return state;
        default:
            return state;
    }
}


export const tagLiveData = (state = {}, action) => {
    switch (action.type) {
        case SYSTEM_TAG_INITIALIZED:
            const name = action.tagName;
            const newInitData = {};
            if(action.activity) {
                newInitData[name] = {
                    id: action.tagId,
                    name,
                    status: 'select_pending',
                    motionAlgo: action.motionAlgo,
                    CODAlgo: action.CODAlgo,
                    activityUserId: action.activityUser.id,
                    activityId: action.activity.id,
                    activityType: action.activity_type,
                    orientationType: action.orientationType,
                    measurementType: action.measurementType,
                    sprintDistance: action.sprintDistance,
                };
            } else {
                newInitData[name] = {
                    id: action.tagId,
                    name,
                    status: 'select_pending',
                    motionAlgo: action.motionAlgo,
                    CODAlgo: action.CODAlgo,
                };
            }
            return {
                ...state,
                ...newInitData,
            };

        case SYSTEM_TAG_SELECT_CONFIRMED:
            return _.mapValues(state, (tag) => {
                if (tag.status === 'select_pending') {
                    return {
                        ...tag,
                        status: 'selected'
                    };
                }
                return tag;
            });

        case SYSTEM_TAG_READY:
            return _.mapValues(state, (tag) => {
                if (tag.name === action.tagName) {
                    return {
                        ...tag,
                        status: 'ready',
                        time: action.time
                    };
                }
                return tag;
            });

        case SYSTEM_TAG_NOT_READY:
            return _.mapValues(state, (tag) => {
                if (tag.name === action.tagName) {
                    return {
                        ...tag,
                        status: 'selected',
                        time: action.time
                    };
                }
                return tag;
            });

        case CLEAR_USER_TAG_DATA:
            return _.mapValues(state, (tag) => {
                if (tag.name === action.tagName) {
                    return {};
                }
                return tag;
            });

        case ACTIVITY_TOGGLE_RECORDING_MODE:
            return _.mapValues(state, (tag) => {
                if (tag.activityId === action.activityId) {
                    const status = action.isRecording ? 'recording' : 'ready';
                    //if (status === 'recording') resetMessages(tag.name);
                    return {
                        ...tag,
                        status,
                    }
                }
                return tag;
            });

        case SYSTEM_SPRINT_ENDED:
        case SYSTEM_JUMP_DETECTED:
            return _.mapValues(state, (tag) => {
                if (tag.activityUserId === action.activityUserId) {
                    return {
                        ...tag,
                        status: 'ready',
                    }
                }
                return tag;
            });

        case ACTIVITY_RESET_RECORDING_MODE:
            return _.mapValues(state, (tag) => {
                if (tag.activityId === action.activityId) {
                    if(tag.motionAlgo && tag.motionAlgo.reset) tag.motionAlgo.reset(tag.name);
                    if(tag.CODAlgo) tag.CODAlgo.reset(tag.name);
                    resetMessages(tag.name);
                    return {
                        ...tag,
                        status: 'selected',
                    }
                }
                return tag;
            });

        case ACTIVITY_RESET_LIVE_RECORDING_MODE:
            return _.mapValues(state, (tag) => {
                if(tag.motionAlgo && tag.motionAlgo.reset) tag.motionAlgo.reset(tag.name);
                if(tag.CODAlgo) tag.CODAlgo.reset(tag.name);
                resetMessages(tag.name);

                // return blank to effectively remove/reset this tagData
                return {};
            });

        case SYSTEM_TAG_REMOVED:
            const tags = {};
            _.each(state, (tag) => {
                if(tag.name !== action.tagName) tags[tag.id] = tag;
            });
            return tags;

        case SET_TAG_POSITION:
            return _.mapValues(state, (tag) => {
                if (action.position !== undefined) {
                    return {
                        ...tag,
                        position: action.position,
                    }
                }
                return tag;
            });

        case SET_TAG_VELOCITY:
            return _.mapValues(state, (tag) => {
                if (action.velocity && tag.name===action.tag.name) {
                    return {
                        ...tag,
                        velocity: action.velocity,
                    }
                }
                return tag;
            });

        case SET_TAG_BATTERY:
            return _.mapValues(state, (tag) => {
                if (action.battery && tag.name===action.tagName) {
                    return {
                        ...tag,
                        battery: action.battery,
                    }
                }
                return tag;
            });

        default:
            return state;
    }

}


/**
 * reducers/tag - isFetching
 *
 * simple reducer so that we always know if we are fetching a tag or not
 *
 * @param  {Object} state - current 'messages.isFetching' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const isFetching = (state = false, action) => {
    switch (action.type) {
        case FETCH_TAG_REQUEST:
            return true;
        case FETCH_TAG_SUCCESS:
        case FETCH_TAG_FAILURE:
            return false;
    }
    return state;
}

// combine all of these reducers under the 'tags' namespace.
export default combineReducers({
    ids,
    byIds,
    tagLiveData,
    isFetching,
});


// some 'getters' for our current 'tags' state.
export const getIds = (state) => state.ids;
export const getTag = (state, id) => {
    if (id in state.byIds) return state.byIds[id];
    return null;
}
export const getTags = (state) => {
    return state.byIds;
}
export const getTagLiveData = (state, name) => {
    if (name in state.tagLiveData) return state.tagLiveData[name];
    return null;
}
export const getTagsInLiveData = (state) => {
    return state.tagLiveData;
}
export const getTagsBySystem = (state, systemId) => {
    return _.filter(state.byIds, (t) => t.system === systemId);
}
export const getTagsByOrg = (state, orgId) => {
    return state.byIds;
}
export const getTagsIsFetching = (state) => state.isFetching;
