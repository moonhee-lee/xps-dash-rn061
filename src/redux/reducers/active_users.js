import _ from 'lodash';
import { combineReducers } from 'redux';
import {
    FETCH_ACTIVE_USERS_REQUEST,
    FETCH_ACTIVE_USERS_SUCCESS,
    FETCH_ACTIVE_USER_REQUEST,
    FETCH_ACTIVE_USER_SUCCESS,
    FETCH_ACTIVE_USER_FAILURE,
    SAVE_ACTIVE_USER_SUCCESS,
    DELETE_ACTIVE_USER_SUCCESS,
    SYSTEM_SPRINT_BEEP_START,
    SYSTEM_SPRINT_MOTION_DISTANCE,
    SYSTEM_SPRINT_ENDED,
    SYSTEM_JUMP_DETECTED,
    SYSTEM_COD_DETECTED,
    ACTIVE_RESET_RECORDING_MODE,
    ACTIVE_TOGGLE_RECORDING_MODE,
    SET_ACTIVE_USER_VELOCITY,
    SET_ACTIVE_USER_VERTICAL,
    SET_ACTIVITY_USER_TAG,
    SET_ACTIVE_USER_TAG,
    ACTIVITY_RESET_LIVE_RECORDING_MODE,
    UPDATE_LOCAL_USER, REMOVE_LOCAL_ORG_MEMBER,
    REMOVE_USER_FROM_ACTIVITY,
    SAVE_ACTIVE_USER_TAG,
    CLEAR_ACTIVE_USERS
} from '../constants/actionTypes';
import {formatNumber} from "../../lib/format_utils";

/**
 * reducers/active_user - ids
 *
 * contains all of the active_user ids that we know about in the system.
 *
 * @param  {Object} state - current 'active_summaries.ids' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const ids = (state = [], action) => {
    switch (action.type) {
        case FETCH_ACTIVE_USER_SUCCESS:
        case SAVE_ACTIVE_USER_SUCCESS:
            return [...state, ...action.response.result];
        case DELETE_ACTIVE_USER_SUCCESS:
            return _.filter(state, (id) => id !== action.activeUserId);
        default:
            return state;
    }
}


/**
 * reducers/active_user - byIds
 *
 * contains all of the active_user objects that we know about.
 * these are the fully serialized objects that we retreive from the API.
 *
 * @param  {Object} state - current 'active_summaries.byIds' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const byIds = (state = {}, action) => {
    switch (action.type) {
        case FETCH_ACTIVE_USERS_REQUEST:
            return state;

        case CLEAR_ACTIVE_USERS:
           return {};

        case FETCH_ACTIVE_USER_SUCCESS:
        case FETCH_ACTIVE_USERS_SUCCESS:
        case SAVE_ACTIVE_USER_SUCCESS:
            if (action.response) {
                return {
                    ...state,
                    ...action.response.entities.active_user,
                };
            }
            return state;

        case DELETE_ACTIVE_USER_SUCCESS:
            return _.pickBy(state, (item) => item.id !== action.activeUserId);

        case SYSTEM_SPRINT_BEEP_START:
            return _.mapValues(state, (activeUser) => {
                if (activeUser.activity === action.activityId || action.activityId==='live_activity') {
                    return {
                        ...activeUser,
                        beepStartTime: action.beepStartTime
                    }
                } else {
                    return activeUser;
                }
            });

        case SYSTEM_SPRINT_MOTION_DISTANCE:
            return _.mapValues(state, (activeUser) => {
                if (activeUser.id === action.activeUserId) {
                    return {
                        ...activeUser,
                        distance: action.currentDistance
                    }
                } else {
                    return activeUser;
                }
            });

        case SET_ACTIVE_USER_VELOCITY:
            return _.mapValues(state, (activeUser) => {
                if (parseInt(activeUser.tag.name) === parseInt(action.tag.name)) {
                    return {
                        ...activeUser,
                        velocity: action.velocity
                    }
                } else {
                    return activeUser;
                }
            });

        case SET_ACTIVE_USER_TAG:
        case SET_ACTIVITY_USER_TAG:
            return _.mapValues(state, (activeUser) => {
                if(action.activityUser.id && action.activityUser.id===activeUser.id) {
                    return {
                        ...activeUser,
                        tag: action.tag
                    }
                }
                return activeUser;
            });

        case SAVE_ACTIVE_USER_TAG:
            return _.mapValues(state, (activeUser) => {
                if(action.user && action.user===activeUser.id) {
                    return {
                        ...activeUser,
                    }
                }
                return activeUser;
            });

        case SET_ACTIVE_USER_VERTICAL:
            return _.mapValues(state, (activeUser) => {
                if (activeUser.tag === action.tag.name) {
                    return {
                        ...activeUser,
                        vertical: action.vertical
                    }
                } else {
                    return activeUser;
                }
            });

        case SYSTEM_SPRINT_ENDED:
            return _.mapValues(state, (activeUser) => {
                if (activeUser.id === action.activeUserId) {
                    const events = [];
                    const finish_time = action.sprintData.end/1000 - action.sprintData.start/1000;

                    let reaction_time = null;
                    if(activeUser.beepStartTime) {
                        reaction_time = (action.sprintData.start - activeUser.beepStartTime) / 1000;
                    }

                    let max_velocity = null, max_acceleration = null, max_raw_velocity = null;
                    if(action.splitData && action.splitData.velocities && action.splitData.velocities.length>0) {
                        max_velocity = _.max(action.splitData.velocities);
                    }
                    if(action.splitData && action.splitData.raw_velocities && action.splitData.raw_velocities.length>0) {
                        max_raw_velocity = _.max(action.splitData.raw_velocities);
                    }
                    if(action.splitData && action.splitData.accelerations && action.splitData.accelerations.length>0) {
                        max_acceleration = _.max(action.splitData.accelerations);
                    }

                    events.push({type: "start", time: action.sprintData.start/1000});
                    events.push({type: "end", time: action.sprintData.end/1000});
                    events.push({type: "beep", time: activeUser.beepStartTime/1000});
                    events.push({type: "reaction_time", time: reaction_time});
                    events.push({type: "finish_time", time: finish_time});
                    events.push({type: "motion_start", pos: action.sprintData.startPosAndTime, time: action.sprintData.start/1000});
                    events.push({type: "sprint_end", pos: action.sprintData.endPosAndTime, time: action.sprintData.end/1000});

                    const stride_data_processed = [];
                    const stride_times = (action.strideDataMetres && action.strideDataMetres.times)?_.filter((action.strideDataMetres.times),(val) => val>=0):[];
                    const stride_distances = (action.strideDataMetres && action.strideDataMetres.distances)?_.filter((action.strideDataMetres.distances),(val) => val>=0):[];
                    const stride_durations = (action.strideDataMetres && action.strideDataMetres.durations)?_.filter((action.strideDataMetres.durations),(val) => val>=0):[];
                    const stride_steps = (action.strideDataMetres && action.strideDataMetres.steps)?action.strideDataMetres.steps:[];
                    const strideArrayLength = Math.min(_.size(stride_times),_.size(stride_distances),_.size(stride_durations),_.size(stride_steps));

                    //start_time, end_time, contact_time, frequency, length, side
                    for(let i=0;i<strideArrayLength;i++) {
                        const _time = stride_times[i]/1000;
                        const _duration = stride_durations[i]/1000;
                        const _start_time = i===0?action.sprintData.start/1000:stride_times[i]/1000;
                        const _end_time = i===(strideArrayLength-1)?action.sprintData.end/1000:stride_times[i+1]/1000;
                        const _total_time = _end_time/1000-_start_time/1000;
                        const _length = stride_distances[i];
                        const _step = stride_steps[i]>=0?(stride_steps[i]>0?'left':'right'):null;

                        const stride_data = {};
                        stride_data.time = _time;
                        stride_data.start_time = _start_time;
                        stride_data.end_time = _end_time;
                        stride_data.total_time = _total_time;
                        stride_data.contact_time = _duration;
                        stride_data.frequency = (1/_total_time)/1000;
                        stride_data.length = _length;
                        stride_data.side = _step;

                        stride_data_processed.push(stride_data);

                        events.push({
                            type: "stride",
                            time:_time,
                            side: _step,
                            length:_length,
                            end_time:_end_time,
                            frequency: (1/_total_time)/1000,
                            start_time:_start_time,
                            total_time:_total_time,
                            contact_time: _duration
                        });
                    }

                    const split_accelerations = (action.splitData && action.splitData.accelerations)?_.filter((action.splitData.accelerations),(val) => val>=0):[];
                    const split_velocities = (action.splitData && action.splitData.velocities)?_.filter((action.splitData.velocities),(val) => val>=0):[];
                    const split_raw_velocities = (action.splitData && action.splitData.raw_velocities)?_.filter((action.splitData.raw_velocities),(val) => val>=0):[];
                    const split_times = (action.splitData && action.splitData.times)?_.filter((action.splitData.times),(val) => val>=0):[];

                    const splitArrayLength = Math.min(_.size(split_accelerations),_.size(split_velocities),_.size(split_times));

                    for(let i=0;i<splitArrayLength;i++) {
                        const _acceleration = split_accelerations[i];
                        const _velocity = split_velocities[i];
                        const _raw_velocity = split_raw_velocities[i];
                        const _time = split_times[i];
                        let _duration;
                        if(i>0) _duration = split_times[i]/1000-split_times[i-1]/1000;
                        else _duration = split_times[i]/1000-action.sprintData.start/1000;

                        events.push({
                            type: "split",
                            time:_time,
                            cumulative_distance: i+1,
                            duration:_duration,
                            velocity:_velocity,
                            raw_velocity:_raw_velocity,
                            acceleration:_acceleration
                        });
                    }

                    _.each(activeUser.cod_data,(cod) => {
                        events.push({...cod,type:'cod'});
                    });

                    let total_time = _.sum(events
                        .filter(event => event.type === 'split')
                        .map(event => parseFloat(formatNumber(event.duration))));

                    if (reaction_time) {
                        total_time += parseFloat(formatNumber(reaction_time));
                    }

                    return {
                        ...activeUser,
                        meta_data: {
                            ...action.sprintData,
                            finish_time,
                            sprint_time: finish_time,
                            reaction_time,
                            total_time,
                            max_raw_velocity,
                            max_velocity,
                            max_acceleration,
                            changes_of_direction: activeUser.cod_data,
                            split_data: action.splitData,
                            stride_data_metres: stride_data_processed,
                        },
                        events,
                        score: total_time,
                        completed: true
                    }
                } else {
                    return activeUser;
                }
            });

        case SYSTEM_JUMP_DETECTED:
            return _.mapValues(state, (activeUser) => {
                if (parseInt(activeUser.id) === parseInt(action.activeUserId) && activeUser.tag && parseInt(activeUser.tag.name) === parseInt(action.tagName)) {
                    return {
                        ...activeUser,
                        meta_data: action.jumpData,
                        completed: true,
                        score: action.jumpData.height > action.jumpData.length ? action.jumpData.height : action.jumpData.length,
                    }
                } else {
                    return activeUser;
                }
            });

        case SYSTEM_COD_DETECTED:
            return _.mapValues(state, (activityUser) => {
                let CODData;
                console.log(JSON.stringify(action.CODData));
                if(action.CODData) {
                    if(!!activityUser.cod_data && activityUser.cod_data!=={}) {
                        CODData = _.concat(activityUser.cod_data,[action.CODData]);
                    } else {
                        CODData = [action.CODData];
                    }
                }

                if (parseInt(activityUser.id) === parseInt(action.activityUserId) && activityUser.tag && parseInt(activityUser.tag.name) === parseInt(action.tagName)) {
                    return {
                        ...activityUser,
                        cod_data: CODData
                    }
                } else {
                    return activityUser;
                }
            });

        case ACTIVITY_RESET_LIVE_RECORDING_MODE:
            return _.mapValues(state, (activeUser) => {
                return {
                    ...activeUser,
                    meta_data: {},
                    events: [],
                    score: null,
                    cod_data: null,
                    completed: false,
                    distance: null,
                }
            });

        /*case REMOVE_LOCAL_ORG_MEMBER:
            if(action.tempID) {
                console.log(_.omitBy(state, (user,index) => index === action.tempID));
                return _.omitBy(state, (user,index) => index === action.tempID);
            } else return state;*/

        case UPDATE_LOCAL_USER:
            const activeUsers = _.omitBy(state, (user,index) => index === action.tempID);
            return _.mapValues(activeUsers, (activeUser) => {
                if(action.tempID===activeUser.id) {
                    return {
                        ...activeUser,
                        id: action.orgMember.id,
                        user: action.orgMember.user,
                        isInActivity: false
                    }
                } else {
                    return activeUser;

                }
            });

        case REMOVE_USER_FROM_ACTIVITY:
            return _.mapValues(state, (activeUser) => {
                if (parseInt(activeUser.id) === parseInt(action.activeUserId)) {
                    return {
                        ...activeUser,
                        isInActivity: false
                    }
                } else {
                    return activeUser;
                }
            });


        default:
            return state;
    }
}


/**
 * reducers/active_user - isFetching
 *
 * simple reducer so that we always know if we are fetching a active_user or not
 *
 * @param  {Object} state - current 'messages.isFetching' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const isFetching = (state = false, action) => {
    switch (action.type) {
        case FETCH_ACTIVE_USERS_REQUEST:
        case FETCH_ACTIVE_USER_REQUEST:
            return true;
        case FETCH_ACTIVE_USERS_SUCCESS:
        case FETCH_ACTIVE_USER_SUCCESS:
        case FETCH_ACTIVE_USER_FAILURE:
            return false;
    }
    return state;
}

// combine all of these reducers under the 'active_summaries' namespace.
export default combineReducers({
    ids,
    byIds,
    isFetching,
});


// some 'getters' for our current 'active_summaries' state.
export const getIds = (state) => state.ids;
export const getActiveUser = (state, id) => {
    return _.find(state.byIds,(user) => user.id===id);
}


export const getActiveUsers = (state) => (state && state.byIds)?state.byIds:[];

export const getActiveUsersIsFetching = (state) => state.isFetching;
