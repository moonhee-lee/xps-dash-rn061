import _ from 'lodash';
import { combineReducers } from 'redux';
import {
    FETCH_FIRMWARE_REQUEST,
    FETCH_FIRMWARE_SUCCESS,
    FETCH_FIRMWARE_FAILURE,
    SAVE_FIRMWARE_FILENAME
} from '../constants/actionTypes';


/**
 * reducers/session - ids
 *
 * contains all of the session ids that we know about in the system.
 * 
 * @param  {Object} state - current 'sessions.ids' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const ids = (state = [], action) => {
    switch (action.type) {
        case FETCH_FIRMWARE_SUCCESS:
            return [...state, ...action.response.result];
        default:
            return state;
    }
}


/**
 * reducers/session - byIds
 *
 * contains all of the session objects that we know about.
 * these are the fully serialized objects that we retreive from the API.
 * 
 * @param  {Object} state - current 'sessions.byIds' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const byIds = (state = {}, action) => {
    switch (action.type) {
        case FETCH_FIRMWARE_SUCCESS:
            if (action.response) {
                return {
                    ...state,
                    ...action.response.entities.firmware,
                };
            }
            return state;
        case SAVE_FIRMWARE_FILENAME:
            return _.mapValues(state, (firmware) => {
                if (action.firmware && action.firmware.id===firmware.id) {
                    return {
                        ...firmware,
                        filename: action.firmware.filename,
                    }
                }
                return firmware;
            });
        default:
            return state;
    }
}


/**
 * reducers/session - isFetching
 *
 * simple reducer so that we always know if we are fetching a session or not
 * 
 * @param  {Object} state - current 'messages.isFetching' state in our redux store
 * @param  {Object} action - current action being dispatched
 * @return {Object} new state (or returns existing state if not changed.)
 */
export const isFetching = (state = false, action) => {
    switch (action.type) {
        case FETCH_FIRMWARE_REQUEST:
            return true;
        case FETCH_FIRMWARE_SUCCESS:
        case FETCH_FIRMWARE_FAILURE:
            return false;
    }
    return state;
}

// combine all of these reducers under the 'sessions' namespace.
export default combineReducers({
    ids,
    byIds,
    isFetching,
});


// some 'getters' for our current 'sessions' state.
export const getFirmware = (state) => _.values(state.byIds);

