import { NativeEventEmitter, NativeModules } from 'react-native';

import { getWifiState, getCurrentSystem } from '../../redux/reducers';
import { appDidEnterBackground } from '../../redux/actions/system';

import {
    WIFI_CONNECTION_REQUESTED,
    WIFI_CONNECTION_SUCCESS,
    WIFI_CONNECTION_FAILED,
    WIFI_STOP_RETRIES,
    WIFI_FETCH_FAILED,
    WIFI_DISCONNECT_SUCCESS,
    WIFI_RESET_RETRIES,
    APP_STATE_CHANGE,
} from '../constants/actionTypes';


export const fetchCurrentWifiState = (reset=false) => (dispatch, getState) => {
    const wifiStateEmitter = new NativeEventEmitter(NativeModules.IOSWifiManager);
    const wifiSubscription = wifiStateEmitter.addListener(
        'WifiStateChange',
        (data) => {
            console.log('WifiStateChange ' + data.currentSSID);
            dispatch({
                type: WIFI_CONNECTION_SUCCESS,
                deviceSSID: data.currentSSID
            });
    });
    const wifiDisconnectSubscription = wifiStateEmitter.addListener(
        'WifiDisconnected',
        (data) => {
            console.log('WifiDisconnected ');
            dispatch({
                type: WIFI_DISCONNECT_SUCCESS
            });
    });
    const wifiFailureSubscription = wifiStateEmitter.addListener(
        'WifiConnectFailed',
        (data) => {
            console.log('WifiConnectFailed ' + data.reason);
            dispatch({
                type: WIFI_CONNECTION_FAILED,
                error: {message: 'Wifi Timeout'}
            });
    });
    // overloading this a little, but the app states help to figure out what to do
    // with the wifi connection.
    const newAppStateSubscription = wifiStateEmitter.addListener(
        'NewAppState',
        (data) => {
            console.log('NewAppState ' + data.state);

            if (data.state === 'didEnterBackground') {
                dispatch(appDidEnterBackground());
                dispatch({type: APP_STATE_CHANGE, state: 'background'});
            } else if (data.state == 'willEnterForeground') {
                dispatch({type: APP_STATE_CHANGE, state: 'active'});
            }
    });
    return NativeModules.IOSWifiManager.startWifiPolling();
};

export const connectToWifiNetwork = (ssid, password, reset=false, ignoreAttempts=false) => (dispatch, getState) => {
    // reset means let's stop any current retries (if any)
    const _state = getState();
    const wifiState = getWifiState(_state);
    const currentSystem = getCurrentSystem(_state);
    if(wifiState && wifiState.isConnected && wifiState.deviceSSID===currentSystem.ssid) return;

    dispatch({
        type: WIFI_CONNECTION_REQUESTED,
        deviceSSID: ssid,
        reset
    });

    console.log('calling connectToProtectedSSID');
    //if(ignoreAttempts) alert('conn')
    return NativeModules.IOSWifiManager.connectToProtectedSSID(ssid, password, false).then(
        response => {
            console.log('IOS returned ok. Wifi Connection attempt commencing.')
        },
        error => {
            //alert(JSON.stringify(error.userInfo));
            dispatch({
                type: WIFI_CONNECTION_FAILED,
                error
            });
        }
    ).catch((e) => alert(e));
};

export const disconnectFromWifiNetwork = (ssid) => (dispatch, getState) => {
    const _state = getState();
    const wifiState = getWifiState(_state);

    if(wifiState && wifiState.deviceSSID === ssid) {
        return NativeModules.IOSWifiManager.disconnectCurrentSSID(ssid);
    }
}


export const stopWifiReconnect = () => (dispatch, getState) => {
    dispatch({
        type: WIFI_STOP_RETRIES
    });
};
