import { normalize } from 'normalizr';
import * as schema from '../../redux//schema';
import { localStorage } from '../../lib/local_storage';
import { getCurrentTeam, getActiveSession, getCurrentOrg } from '../../redux//reducers';

import {
    FETCH_LIVE_ACTIVITY_REQUEST,
    FETCH_LIVE_ACTIVITY_SUCCESS,
    SAVE_LIVE_ACTIVITY_REQUEST,
    SAVE_LIVE_ACTIVITY_SUCCESS,
} from '../../redux//constants/actionTypes';
import {FETCH_LIVE_ACTIVITY_SESSION, SET_LIVE_ACTIVITY_SESSION, SET_LIVE_ACTIVITY_TEAM} from "../constants/actionTypes";
import moment from "moment";

export const fetchLiveActivity = () => (dispatch, getState) => {
    // dispatch the FETCH_ACTIVITY_REQUEST action.

    const _state = getState();
    const activeSession = getActiveSession(_state);
    const currentOrg = getCurrentOrg(_state);

    if(!_state.live_activity || !_state.live_activity.liveActivity.id) {
        localStorage.getItem('liveActivity').then((data) => {
            if(data.org!==currentOrg.id) data.org = currentOrg.id;
            dispatch(saveLiveActivity(data));
        }).catch((e) =>{
            const activity = {
                id: 'live_activity',
                activity_type:'sprint',
                distance: 10,
                display_name: '10m Motion Sprint',
                orientation: "vertical",
                orientation_type: "vertical",
                session: activeSession,
                start_type: "motion",
                org: currentOrg.id
            }
            dispatch(saveLiveActivity(activity));
        });
    }

    dispatch({
        type: FETCH_LIVE_ACTIVITY_REQUEST,
    });

    dispatch({
        type: FETCH_LIVE_ACTIVITY_SUCCESS,
    });
}

export const fetchLiveActivitySession = () => (dispatch, getState) => {
    console.log('fetching live activity session');
    dispatch({
        type: FETCH_LIVE_ACTIVITY_SESSION,
    });
}

export const setLiveActivitySession = (session, activityData) => (dispatch,getState) => {
    console.log('setting live activity session: '+session.id);
    dispatch({
        type: SET_LIVE_ACTIVITY_SESSION,
        session,
    });

    dispatch(fetchLiveActivitySession());
}

export const setLiveActivityTeam = (teamId, activityData) => (dispatch,getState) => {
    console.log('setting live activity team: '+teamId);
    dispatch({
        type: SET_LIVE_ACTIVITY_TEAM,
        teamId,
    });
}

export const saveLiveActivity = (activityData, callback) => (dispatch, getState) => {
    // dispatch the SAVE_ACTIVITY_REQUEST action.
    dispatch({
        type: SAVE_LIVE_ACTIVITY_REQUEST,
        activityData,
    });

    localStorage.setItem('liveActivity',activityData);

    dispatch({
        type: SAVE_LIVE_ACTIVITY_SUCCESS,
        activityData,
        response: normalize(activityData, schema.activitySchema)
    });

    dispatch(fetchLiveActivity());
}
