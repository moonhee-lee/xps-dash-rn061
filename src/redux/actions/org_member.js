import { normalize } from 'normalizr';
import * as schema from '../../redux//schema';
import * as orgMemberApi from '../../redux//api/org_member';
import * as orgMemberLocal from '../../redux//local/org_member';
import { startAppAnonymous, startAppLoggedIn } from '../../react/screens';
import { fetchUsersByTeam } from '../../redux//actions/user';
import { getMembersByOrg, getOrgMemberByUserId  } from '../../redux//reducers/org_member';
import { getCurrentOrg, getCloudState } from '../../redux//reducers';
import { fetchActiveUsers } from '../../redux//actions/active_user';
import { fetchSystemsByOrg } from '../../redux//actions/org_system';
import { fetchTagsByOrg } from '../../redux//actions/tag';
import { localStorage } from '../../lib/local_storage';
import _ from 'lodash';

import {
    FETCH_USER_SUCCESS,
    SET_CURRENT_ORG,
    CURRENT_TEAM_FETCH_SUCCESS,
    SAVE_ORG_MEMBER_REQUEST,
    SAVE_ORG_MEMBER_SUCCESS,
    SAVE_ORG_MEMBER_FAILURE,
    FETCH_ORG_MEMBER_BY_ORG_FAILURE,
    FETCH_ORG_MEMBER_BY_ORG_REQUEST,
    FETCH_ORG_MEMBER_BY_ORG_SUCCESS,
    FETCH_ORG_MEMBER_BY_USER_ID_FAILURE,
    FETCH_ORG_MEMBER_BY_USER_ID_REQUEST,
    FETCH_ORG_MEMBER_BY_USER_ID_SUCCESS
} from "../constants/actionTypes";

export const saveOrgMember = (orgMemberData, navigator, toggleAthlete) => (dispatch, getState) => {

    // dispatch the SAVE_ORG_MEMBER_REQUEST action.
    dispatch({
        type: SAVE_ORG_MEMBER_REQUEST,
        orgMemberData,
    });
    const _state = getState();

    //if passing in user ID, convert to Org Member ID
    if(orgMemberData.id) {
        const orgMember = getOrgMemberByUserId(_state.org_members, orgMemberData.id);
        if(orgMember) orgMemberData.id = orgMember.id;
    }

    const org = getCurrentOrg(_state);
    if(org) orgMemberData.org = org.id;
    orgMemberData.user.org = org;
    orgMemberData.user.full_name = orgMemberData.user.first_name+' '+orgMemberData.user.last_name;
    orgMemberData.user.image_url = '';
    orgMemberData.user.thumbnail_url = '';
    orgMemberData.user.is_staff = false;
    orgMemberData.is_admin = false;

    // call the API method to create(save)
    return orgMemberLocal.saveOrgMember(orgMemberData).then(
        response => {
            dispatch({
                type: SAVE_ORG_MEMBER_SUCCESS,
                orgMemberData,
                response: normalize(orgMemberData, schema.orgMemberSchema)
            });
            if(orgMemberData.teams && orgMemberData.teams[0]) dispatch(fetchUsersByTeam(orgMemberData.teams[0]));
            dispatch(fetchActiveUsers());
            dispatch(fetchMembersByOrg(org.id));

            if(toggleAthlete) {
                toggleAthlete(orgMemberData.user);
            }
            if (navigator) {
                navigator.dismissModal();
            }
        },
        error => {
            console.log(error);
            dispatch({
                type: SAVE_ORG_MEMBER_FAILURE,
                orgMemberData,
                errorMessage: 'Something went wrong',
            });
        }
    )
}

export const fetchMembersByOrg = (orgId) => (dispatch, getState) => {
    // dispatch the FETCH_ORG_MEMBER_BY_ORG_REQUEST action.
    dispatch({
        type: FETCH_ORG_MEMBER_BY_ORG_REQUEST,
        orgId
    });

    return orgMemberLocal.fetchMembersByOrg(orgId).then((localOrgMembers) => {
        // call the API method to login the user.
        orgMemberApi.fetchMembersByOrg(orgId).then(
            response => {
                const members = _.concat(response.data.results,localOrgMembers);
                const sortedOrgMembers = members.sort((memberA, memberB) => {
                    return memberA.last_name < memberB.last_name;
                });
                // on success, dispatch the FETCH_ORG_MEMBER_BY_ORG_SUCCESS action
                dispatch({
                    type: FETCH_ORG_MEMBER_BY_ORG_SUCCESS,
                    orgId,
                    response: normalize(sortedOrgMembers, schema.orgMemberListSchema)
                });
                orgMemberLocal.saveAllOrgMembers(sortedOrgMembers);
            },
            error => {
                orgMemberLocal.fetchAllOrgMembers(orgId).then((allOrgMembers) => {
                    if(allOrgMembers.length) {
                        const members = _.concat(allOrgMembers,localOrgMembers);
                        const sortedOrgMembers = members.sort((memberA, memberB) => {
                            return memberA.last_name < memberB.last_name;
                        });
                        // on success, dispatch the FETCH_ORG_MEMBER_BY_ORG_SUCCESS action
                        dispatch({
                            type: FETCH_ORG_MEMBER_BY_ORG_SUCCESS,
                            orgId,
                            response: normalize(sortedOrgMembers, schema.orgMemberListSchema)
                        });
                    } else {
                        if(localOrgMembers.length) {
                            // on success, dispatch the FETCH_ORG_MEMBER_BY_ORG_SUCCESS action
                            dispatch({
                                type: FETCH_ORG_MEMBER_BY_ORG_SUCCESS,
                                orgId,
                                response: normalize(localOrgMembers, schema.orgMemberListSchema)
                            });
                            orgMemberLocal.saveAllOrgMembers(localOrgMembers);
                        } else {
                            // on error, dispatch the FETCH_ORG_MEMBER_BY_ORG_FAILURE action with teamId and error message.
                            dispatch({
                                type: FETCH_ORG_MEMBER_BY_ORG_FAILURE,
                                orgId,
                                errorMessage: error,
                            });
                        }
                    }
                }).catch((err)=> {
                    if(localOrgMembers.length) {
                        // on success, dispatch the FETCH_ORG_MEMBER_BY_ORG_SUCCESS action
                        dispatch({
                            type: FETCH_ORG_MEMBER_BY_ORG_SUCCESS,
                            orgId,
                            response: normalize(localOrgMembers, schema.orgMemberListSchema)
                        });
                        orgMemberLocal.saveAllOrgMembers(localOrgMembers);
                    } else {
                        // on error, dispatch the FETCH_ORG_MEMBER_BY_ORG_FAILURE action with teamId and error message.
                        dispatch({
                            type: FETCH_ORG_MEMBER_BY_ORG_FAILURE,
                            orgId,
                            errorMessage: error,
                        });

                    }
                });
            }
        );
    }).catch((e) => {});
}

export const fetchMemberByUserId = (userId, onLogin=false) => (dispatch, getState) => {
    // dispatch the FETCH_ORG_MEMBER_BY_ORG_REQUEST action.
    dispatch({
        type: FETCH_ORG_MEMBER_BY_USER_ID_REQUEST,
        userId
    });

    const _state = getState();
    const cloudState = getCloudState(_state);
    if(!cloudState.isConnected) {
        return handleNoNetwork(userId,onLogin,dispatch);
    } else {
        // call the API method to login the user.
        return orgMemberApi.fetchMemberByUserId(userId).then((response) => {
            if (response.data.results.length === 0) return;

            const orgMember = response.data.results[0];

            // FIXME: it blocks to transit to the loggedIn
            // on success, dispatch the FETCH_ORG_MEMBER_BY_ORG_SUCCESS action
            // dispatch({
            //     type: FETCH_ORG_MEMBER_BY_USER_ID_SUCCESS,
            //     userId,
            //     response: normalize(orgMember, schema.orgMemberSchema)
            // });

            if (onLogin) {
                if (orgMember) {
                    dispatch({
                        type: SET_CURRENT_ORG,
                        org: orgMember.org
                    });

                    if (orgMember.user && typeof(orgMember.user) === 'object') {
                        dispatch({
                            type: FETCH_USER_SUCCESS,
                            userId,
                            response: normalize(orgMember.user, schema.userSchema)
                        });
                    }

                    if (!global.currentTeam && orgMember.teams && orgMember.teams[0]) {
                        const teamId = orgMember.teams[0];

                        localStorage.setItem('xps:currentTeam', teamId);
                        global.currentTeam = teamId;

                        dispatch({
                            type: CURRENT_TEAM_FETCH_SUCCESS,
                            currentTeam: teamId
                        });
                    }

                    dispatch(fetchSystemsByOrg(orgMember.org));
                    dispatch(fetchTagsByOrg(orgMember.org));
                    dispatch(fetchMembersByOrg(orgMember.org));
                    dispatch(fetchActiveUsers());
                    startAppLoggedIn();
                } else {
                    startAppAnonymous();
                }
            }
        }).catch((error) => {
            return handleNoNetwork(userId, onLogin, dispatch, error);
            // on error, dispatch the FETCH_ORG_MEMBER_BY_ORG_FAILURE action with teamId and error message.
        });
    }
}

export const handleNoNetwork = (userId,onLogin,dispatch,error=null) => {
    return new Promise((resolve: any, reject: any) => {
        if (error) {
            dispatch({
                type: FETCH_ORG_MEMBER_BY_USER_ID_FAILURE,
                userId,
                errorMessage: error,
            });
        }
        if (onLogin) {
            orgMemberLocal.fetchAllOrgMembers().then((allOrgMembers) => {
                const orgMember = _.find(allOrgMembers, (om) => om.user.id === userId);
                if (orgMember) {
                    dispatch(fetchSystemsByOrg(orgMember.org));
                    dispatch(fetchTagsByOrg(orgMember.org));
                    dispatch(fetchMembersByOrg(orgMember.org));
                    dispatch(fetchActiveUsers());
                    dispatch({
                        type: SET_CURRENT_ORG,
                        org: orgMember.org
                    });
                    startAppLoggedIn();
                    resolve();
                } else {
                    startAppAnonymous();
                    resolve();  }
            }).catch((err) => {
                startAppAnonymous();
                resolve();});
        } else {
            resolve();
        }
    });
}
