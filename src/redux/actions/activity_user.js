import { normalize } from 'normalizr';
import * as schema from '../../redux//schema';
import * as activityUserApi from '../../redux//api/activity_user';

import {
    FETCH_ACTIVITY_USER_REQUEST,
    FETCH_ACTIVITY_USER_SUCCESS,
    FETCH_ACTIVITY_USER_FAILURE,
    SAVE_ACTIVITY_USER_REQUEST,
    SAVE_ACTIVITY_USER_SUCCESS,
    SAVE_ACTIVITY_USER_FAILURE,
    DELETE_ACTIVITY_USER_REQUEST,
    DELETE_ACTIVITY_USER_SUCCESS,
    DELETE_ACTIVITY_USER_FAILURE,
    SYSTEM_SPRINT_BEEP_START,
    SET_ACTIVITY_USER_VELOCITY,
    SET_ACTIVITY_USER_VERTICAL
} from '../../redux//constants/actionTypes';


/**
 * redux/actions/activity_user - fetchActivityUsersByActivity
 *
 * fetch activity_user via the activityUserApi
 *
 * @param  {Integer} activityId
 *
 */
export const fetchActivityUsersByActivity = (activityId) => (dispatch, getState) => {
    // dispatch the SAVE_ACTIVITY_USER_REQUEST action.
    dispatch({
        type: FETCH_ACTIVITY_USER_REQUEST,
        activityId,
    });

    // call the API method to create(save) this new message.
    return activityUserApi.fetchActivityUsersByActivity(activityId).then(
        response => {
            dispatch({
                type: FETCH_ACTIVITY_USER_SUCCESS,
                activityId,
                response: normalize(response.data.results, schema.activityUserListSchema)
            });
        },
        error => {
            dispatch({
                type: FETCH_ACTIVITY_USER_FAILURE,
                activityId,
                errorMessage: 'Something went wrong',
            });
        }
    )
}


export const saveActivityUser = (activityUserData, navigator) => (dispatch, getState) => {
    // dispatch the SAVE_ACTIVITY_USER_REQUEST action.
    dispatch({
        type: SAVE_ACTIVITY_USER_REQUEST,
        activityUserData,
    });

    // call the API method to create(save)
    return activityUserApi.saveActivityUser(activityUserData).then(
        response => {
            dispatch({
                type: SAVE_ACTIVITY_USER_SUCCESS,
                activityUserData,
                response: normalize(response.data, schema.activityUserSchema)
            });
            if (navigator) {
                navigator.dismissModal();
            }
        },
        error => {
            console.log(error)
            dispatch({
                type: SAVE_ACTIVITY_USER_FAILURE,
                activityUserData,
                errorMessage: 'Something went wrong',
            });
        }
    )
}


export const deleteActivityUser = (activityUserId, navigator) => (dispatch, getState) => {
    // dispatch the SAVE_ACTIVITY_USER_REQUEST action.
    dispatch({
        type: DELETE_ACTIVITY_USER_REQUEST,
        activityUserId,
    });

    // call the API method to delete
    return activityUserApi.deleteActivityUser(activityUserId).then(
        response => {
            dispatch({
                type: DELETE_ACTIVITY_USER_SUCCESS,
                activityUserId
            });
            if (navigator) {
                navigator.dismissModal();
            }
        },
        error => {
            dispatch({
                type: DELETE_ACTIVITY_USER_FAILURE,
                activityUserId,
                errorMessage: 'Something went wrong',
            });
        }
    )
}


export const setActivityBeepStartTime = (activityId, beepStartTime) => (dispatch, getState) => {
    if (!global.client) return;
    dispatch({
        type: SYSTEM_SPRINT_BEEP_START,
        activityId,
        beepStartTime
    });

    const msg = {time:beepStartTime,event_id:'AppStart'};
    global.client.sendMessage(msg, 'io_event');
}

export const clearActivityUsersVertical = (activityUsers) => (dispatch, getState) => {
    activityUsers.map((activityUser) => {
        dispatch({
            type: SET_ACTIVITY_USER_VERTICAL,
            tag:activityUser.tag,
            vertical:0
        });
    });
}
