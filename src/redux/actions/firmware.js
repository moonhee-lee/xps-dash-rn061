import { normalize } from 'normalizr';
import * as schema from '../../redux//schema';
import * as firmwareApi from '../../redux//api/firmware';
import { getCloudState, getCurrentSystem } from '../../redux//reducers';
import { connectSystem } from '../../redux//actions/system';

import {
    FETCH_FIRMWARE_REQUEST,
    FETCH_FIRMWARE_SUCCESS,
    FETCH_FIRMWARE_FAILURE,
    SAVE_FIRMWARE_FILENAME,
    SYSTEM_FIRMWARE_UPDATE_SENDING
} from '../../redux//constants/actionTypes';

import Message from "react-native-paho-mqtt/src/Message";

export const fetchFirmware = () => (dispatch, getState) => {
    // dispatch the SAVE_SESSION_REQUEST action.
    dispatch({
        type: FETCH_FIRMWARE_REQUEST
    });

    firmwareApi.fetchFirmwares().then((response) => {
        dispatch({
            type: FETCH_FIRMWARE_SUCCESS,
            response: normalize(response.data.results, schema.firmwareListSchema)
        });
    }).catch((error) => {
        // on error, dispatch the FETCH_SESSION_FAILURE action with orgId and error message.
        dispatch({
            type: FETCH_FIRMWARE_FAILURE,
            errorMessage: 'Something went wrong',
        });
    });
}

export const updateFirmware = (firmware) => (dispatch, getState) => {
    return new Promise((resolve: any, reject: any) => {
        dispatch({
            type: SYSTEM_FIRMWARE_UPDATE_SENDING
        });
        const msg = {cmd_id: 12, msg_id: 5, blob: firmware};
        msg.msg_id = Math.floor(Math.random() * 1000000000);
        console.log(JSON.stringify(msg));
        let message = new Message(JSON.stringify(msg));
        message.destinationName = 'update';

        try {
            global.client.send(message);
            resolve();
        } catch (err) {
            const system = getCurrentSystem(getState());
            dispatch(connectSystem(system, false));
            reject(err);
        }
    });
}

export const saveFirmware = (firmware) => (dispatch, getState) => {
    dispatch({
        type: SAVE_FIRMWARE_FILENAME,
        firmware
    });
}
