import { normalize } from 'normalizr';
import * as schema from '../../redux/schema';
import * as activitySummaryApi from '../../redux/api/activity_summary';

import {
    FETCH_ACTIVITY_SUMMARY_REQUEST,
    FETCH_ACTIVITY_SUMMARY_SUCCESS,
    FETCH_ACTIVITY_SUMMARY_FAILURE
} from '../../redux/constants/actionTypes';


/**
 * redux/actions/activity_summary - fetchActivitySummariesByActivity
 *
 * fetch activity_summary via the activitySummaryApi
 *
 * @param  {Integer} activityId
 *
 */
export const fetchActivitySummariesByActivity = (activityId) => (dispatch, getState) => {
    // dispatch the SAVE_ACTIVITY_SUMMARY_REQUEST action.
    dispatch({
        type: FETCH_ACTIVITY_SUMMARY_REQUEST,
        activityId,
    });

    // call the API method to create(save) this new message.
    return activitySummaryApi.fetchActivitySummariesByActivity(activityId).then(
        response => {
            dispatch({
                type: FETCH_ACTIVITY_SUMMARY_SUCCESS,
                activityId,
                response: normalize(response.data.results, schema.activitySummaryListSchema)
            });
        },
        error => {
            console.log(error);
            // on error, dispatch the FETCH_ACTIVITY_SUMMARY_FAILURE action with activityId and error message.
            dispatch({
                type: FETCH_ACTIVITY_SUMMARY_FAILURE,
                activityId,
                errorMessage: 'Something went wrong',
            });
        }
    )
}
