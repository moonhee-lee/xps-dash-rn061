import { NativeModules } from 'react-native';
import { normalize } from 'normalizr';
import _ from 'lodash';
import * as schema from '../../redux//schema';
import * as tagApi from '../../redux//api/tag';
import * as tagLocal from '../../redux//local/tag';
import { getTagsInLiveData, getCurrentTags, getCloudState, getActiveUsers, getLiveActivity } from '../../redux//reducers';
import { saveUserTag } from '../../redux//actions/active_user';
import {
    FETCH_TAG_REQUEST,
    FETCH_TAG_SUCCESS,
    FETCH_TAG_FAILURE,
    SYSTEM_TAG_INITIALIZED,
    SYSTEM_TAG_REMOVED,
    SET_ACTIVITY_USER_TAG,
    SET_TAG_VELOCITY,
    SET_TAG_POSITION
} from '../constants/actionTypes';
import {SYSTEM_TAG_SELECT_CONFIRMED} from "../constants/actionTypes";

/**
 * tagActions - fetchTagsBySystem
 *
 * fetch the tag via the tagApi
 *
 * @param  {Integer} tagId
 *
 */
export const fetchTagsBySystem = (system) => (dispatch, getState) => {
    const systemId = system.id;
    // dispatch the SAVE_TAG_REQUEST action.
    dispatch({
        type: FETCH_TAG_REQUEST,
        system,
    });

    // call the API method to create(save) this new tag.
    return tagApi.fetchTagsBySystem(systemId).then(
        response => {
            dispatch({
                type: FETCH_TAG_SUCCESS,
                system,
                response: normalize(response.data.results, schema.tagListSchema)
            });
        },
        error => {
            // on error, dispatch the FETCH_TAG_FAILURE action with systemId and error message.
            dispatch({
                type: FETCH_TAG_FAILURE,
                system,
                errorMessage: 'Something went wrong',
            });
        }
    )
}

/**
 * tagActions - fetchTagsByOrg
 *
 * fetch the tag via the tagApi
 *
 * @param  {Integer} tagId
 *
 */
export const fetchTagsByOrg = (orgId) => (dispatch, getState) => {
    // dispatch the SAVE_TAG_REQUEST action.
    dispatch({
        type: FETCH_TAG_REQUEST,
        orgId,
    });

    const _state = getState();
    const cloudState = getCloudState(_state);
    if(cloudState && cloudState.isConnected) {
        return tagApi.fetchTagsByOrg(orgId).then(
            response => {
                dispatch({
                    type: FETCH_TAG_SUCCESS,
                    orgId,
                    response: normalize(response.data.results, schema.tagListSchema)
                });
                tagLocal.saveAllTags(response.data.results)
            },
            error => {
                console.log('loading tags from local');
                //didn't get tags from API, check to see if we have local copies
                tagLocal.fetchAllTags().then((tags) => {
                    dispatch({
                        type: FETCH_TAG_SUCCESS,
                        orgId,
                        response: normalize(tags, schema.tagListSchema)
                    });
                }).catch((err) => {
                    console.log('could not load tags');
                    // on error, dispatch the FETCH_TAG_FAILURE action with systemId and error message.
                    dispatch({
                        type: FETCH_TAG_FAILURE,
                        orgId,
                        errorMessage: 'Something went wrong',
                    });

                })
            }
        )
    } else {
        return tagLocal.fetchAllTags().then((tags) => {
            dispatch({
                type: FETCH_TAG_SUCCESS,
                orgId,
                response: normalize(tags, schema.tagListSchema)
            });
        }).catch((err) => {
            console.log('could not load tags');
            // on error, dispatch the FETCH_TAG_FAILURE action with systemId and error message.
            dispatch({
                type: FETCH_TAG_FAILURE,
                orgId,
                errorMessage: 'Something went wrong',
            });

        })
    }
}


export const removeTagLiveData = (tagName) => (dispatch, getState) => {
    dispatch({
        type: SYSTEM_TAG_REMOVED,
        tagName
    });
}

export const setupTag = (tag, activity, activityUser, sendSelects=true) => (dispatch, getState) => {
    // obviously we ignore this if we have no tag.
    if (!tag || !tag.name) return;
    // this action sets up the tag for the activity user as necessary.
    console.log('setting up tag: '+tag.name);

    const tagName = parseInt(tag.name, 10);
    let sprintDistance = activity.distance;
    let measureUnitType = activity.units === 'meters' ? 0 : 1;

    let motionAlgo, CODAlgo;
    if(activity) {
        if (activity.activity_type === 'sprint') {
            console.log('setting motion algos to sprint');
            const use3DVelocity = global.enableBobsledMode && activity.bobsled && activity.bobsled === true ? 1 : 0;
            const useWalkingThresholds = global.enableWalkingMode && activity.walking && activity.walking === true ? 1 : 0;
            let useTracker = global.enableSkatingMode && activity.skating && (activity.skating === 'forward' || activity.skating === 'backward') ? 1 : 0;

            if (useWalkingThresholds === 1) { // it also needs to use the tracker velocity/acceleration rather than the IMU
                useTracker = 1;
            }
            console.log("use 3d velocity: " + use3DVelocity);
            console.log("use tracker: " + useTracker);
            console.log("use walking thresholds: " + useWalkingThresholds);
            motionAlgo = NativeModules.XpsSprintStart;
            motionAlgo.initialize(tagName, sprintDistance, 0.0, measureUnitType, use3DVelocity, useTracker, useWalkingThresholds);
        } else if (activity.activity_type === 'jump') {
            if (!activity.triple_jump_type) {
                console.log('setting motion algos to jump');
                motionAlgo = NativeModules.XpsJump;
                motionAlgo.initialize(tagName);
            } else {
                console.log('setting motion algos to freeform if triple jump is selected');
                motionAlgo = 'freeform';
            }
        } else if (activity.activity_type === 'agility') {
            console.log('setting motion algos to agility');
            motionAlgo = NativeModules.XpsSprintStart;
            motionAlgo.initialize(tagName, sprintDistance, 0.0, measureUnitType, 0, 0, 0);
            CODAlgo = NativeModules.XpsCOD;
            CODAlgo.initialize(tagName);
            if(!!activity.endpointDistance) {
                CODAlgo.setEndpointLength(tagName,parseFloat(activity.endpointDistance));
                console.log('setting COD endpointDistance: '+activity.endpointDistance);
            }
            if(!!activity.midpointDistance) {
                CODAlgo.setMidpointLength(tagName, parseFloat(activity.midpointDistance));
                console.log('setting COD midpointDistance: '+activity.midpointDistance);
            }
            if(!!activity.headingThreshold) {
                CODAlgo.setHeadingThreshold(tagName,parseFloat(activity.headingThreshold));
                console.log('setting COD headingThreshold: '+activity.headingThreshold);
            }
        }  else if (activity.activity_type === 'freeform') {
            console.log('setting motion algos to freeform');
            motionAlgo = 'freeform';
        } else {
            return;
        }
    }

    let liveTags = getTagsInLiveData(getState());

    // insert this new tag in case it's not in there.
    liveTags[tagName] = {
        id: tag.id,
        name: tagName
    };

    if (activityUser && activityUser.tag && activityUser.tag.name) {
        const activityTagName = parseInt(activityUser.tag.name, 10);
        if (tagName !== activityTagName) {
            // the coach changed the tag for this user!
            dispatch({
                type: SYSTEM_TAG_REMOVED,
                tagName: activityTagName
            });
            liveTags = _.filter(liveTags, (t) => t.name !== activityTagName);
        }
    }

    dispatch(saveUserTag(activityUser.id,tag));

    if(activity) {
        dispatch({
            type: SYSTEM_TAG_INITIALIZED,
            tagId: tag.id,
            tagName,
            motionAlgo,
            CODAlgo,
            activity,
            activityUser,
            activity_type: activity.activity_type,
            orientationType: activity.activity_type==='jump'?(activity.orientation_type==='horizontal'?0:1):0,
            sprintDistance,
        });
    } else {
        dispatch({
            type: SYSTEM_TAG_INITIALIZED,
            tagId: tag.id,
            tagName,
            motionAlgo: 'measuring',
            CODAlgo
        });
    }

    if(sendSelects) setTimeout(() => {
        dispatch(sendTagSelects())
        },1000);
}

export const sendTagSelects = () => (dispatch, getState) => {
    const _state = getState();
    const activeUsers = getActiveUsers(_state);
    const liveTags = getTagsInLiveData(getState());
    const activity = getLiveActivity(getState());

    _.each(liveTags,(liveTag) => {
        if(!liveTag.status) {
            const activityUser = _.find(activeUsers,(activeUser) => {
                if(activeUser.tag && activeUser.tag.name) return parseInt(activeUser.tag.name)===parseInt(liveTag.name);
                else return false;
            });
            if(activityUser) {
                console.log('dispatching setupTag liveTag ' + liveTag.name);
                dispatch(setupTag(liveTag, activity, activityUser, false));
            }
        }
    });

    let activeLiveTags = [];
    _.each(activeUsers,(activeUser) => {
        if(activeUser.isInActivity && activeUser.tag) activeLiveTags.push(parseInt(activeUser.tag.name));
        if(activeUser.tag && !activeUser.tag_data) {
            console.log('dispatching setupTag activeUser ' + activeUser.tag.name);
            dispatch(setupTag(activeUser.tag, activity, activeUser, false));
        }
    });
    activeLiveTags = _.uniq(activeLiveTags);
    activeLiveTags.sort((a,b) => a-b);

    const tags = _.slice(_.concat(activeLiveTags,_.fill(Array(8-activeLiveTags.length), 0)));

    const msg = {
        'cmd_id': 2,
        'tag_sel': { 'tags': tags },
    }
    if(global.client) global.client.sendMessage(msg);

    dispatch({
        type: SYSTEM_TAG_SELECT_CONFIRMED
    });
}

export const clearTagsVelocities = (activityUsers) => (dispatch, getState) => {
    activityUsers.map((activityUser) => {
        dispatch({
            type: SET_TAG_VELOCITY,
            tag:activityUser.tag_data,
            velocity:0
        });
    });
}

export const setTagsPositions = (activityUsers, position) => (dispatch, getState) => {
    activityUsers.map((activityUser) => {
        dispatch({
            type: SET_TAG_POSITION,
            tag:activityUser.tag_data,
            position
        });
    });
}

export const clearTagsPositions = (activityUsers) => (dispatch, getState) => {
    activityUsers.map((activityUser) => {
        dispatch({
            type: SET_TAG_POSITION,
            tag:activityUser.tag_data,
            position: null
        });
    });
}


export const resetMotionAlgos = (activeUsers) => (dispatch, getState) => {
    console.log('resetting motion algos');
    _.each(activeUsers, (activeUser) => {
        if (activeUser.tag_data) {
            if (activeUser.tag_data.motionAlgo && activeUser.tag_data.motionAlgo.reset) {
                activeUser.tag_data.motionAlgo.reset(parseInt(activeUser.tag_data.name));
            }
            if (activeUser.tag_data.CODAlgo && activeUser.tag_data.CODAlgo.reset) {
                activeUser.tag_data.CODAlgo.reset(parseInt(activeUser.tag_data.name));
            }
        }
    });
}
