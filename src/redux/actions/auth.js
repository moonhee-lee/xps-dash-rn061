import { normalize } from 'normalizr';
import * as schema from '../../redux//schema';
import { startAppAnonymous } from '../../react/screens';
import * as authApi from '../../redux//api/auth';
import { fetchMemberByUserId } from '../../redux//actions/org_member';
import { localStorage } from '../../lib/local_storage';
import { getWifiState, getCurrentSystem } from '../../redux//reducers';
import { disconnectFromWifiNetwork } from '../../redux//actions/wifi';

import {
    LOGIN_REQUEST,
    LOGIN_SUCCESS,
    LOGIN_FAILURE,
    LOGOUT_REQUEST,
    LOGOUT_SUCCESS,
    LOGOUT_FAILURE,
    CLOUD_CONNECTION_SUCCESS,
} from '../../redux//constants/actionTypes';


/**
 * actions/auth - login
 *
 * attempt to login the user via the API
 *
 * @param  {String} username
 * @param  {String} password
 */
export const login = (username, password) => (dispatch, getState) => {
    // dispatch the LOGIN_REQUEST action.
    dispatch({
        type: LOGIN_REQUEST,
        username
    });

    // call the API method to login the user.
    return authApi.login(username, password).then(
        response => {
            // on success, dispatch the LOGIN_SUCCESS action with username attached.
            // and store the new JWT token for this user in local storage.
            localStorage.setItem('xps:loginState', {
                authToken: response.data.token,
                userId: response.data.user.id,
                user: response.data.user
            });

            dispatch({
                type: CLOUD_CONNECTION_SUCCESS
            });

            dispatch({
                type: LOGIN_SUCCESS,
                username,
                response: normalize(response.data.user, schema.userSchema),
                authToken: response.data.token,
            });

            dispatch(fetchMemberByUserId(response.data.user.id, true));
            return response
        },
        error => {
            // on error, dispatch the LOGIN_FAILURE action with username and error message.
            dispatch({
                type: LOGIN_FAILURE,
                username,
                errorMessage: error,
            });
            return error.response;
        }
    );
};


/**
 * actions/auth - logout
 *
 * log out the user by removing their credentials from localstorage
 *
 */
export const logout = () => (dispatch, getState) => {
    const state = getState();
    const wifiState = getWifiState(state);
    const currentSystem = getCurrentSystem(state);

    // if we are connected to the hub, we need to disconnect now.
    if (wifiState && currentSystem && wifiState.deviceSSID === currentSystem.ssid) {
        dispatch(disconnectFromWifiNetwork(wifiState.deviceSSID));
    }

    // dispatch the LOGOUT_REQUEST action.
    dispatch({
        type: LOGOUT_REQUEST,
    });

    // to logout, we simply delete the JWT token
    localStorage.removeItem('xps:loginState');

    startAppAnonymous();

    // on success, dispatch the LOGOUT_SUCCESS.
    dispatch({
        type: LOGOUT_SUCCESS,
    });
};
