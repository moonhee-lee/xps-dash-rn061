import { getCloudState } from '../../redux/reducers';
import { getAxiosClient } from '../../redux/api/client';

import {
    CLOUD_CONNECTION_REQUESTED,
    CLOUD_CONNECTION_SUCCESS,
    CLOUD_CONNECTION_FAILED,
    CLOUD_RESET_TRIES
} from '../constants/actionTypes';

export const fetchCurrentCloudState = (reset=false) => (dispatch, getState) => {
    if (reset) {
        dispatch({
            type: CLOUD_RESET_TRIES,
        });
    }

    const cloudState = getCloudState(getState());
    //alert(cloudState.fetchAttempts);
    if (cloudState.fetchAttempts >= 5) {
        dispatch({
            type: CLOUD_RESET_TRIES
        });
        return;
    }

    dispatch({
        type: CLOUD_CONNECTION_REQUESTED
    });

    return getAxiosClient().head('/api/').then(
        response => {
            //alert(JSON.stringify(response));
            if(response && response.status===200) {
                dispatch({
                    type: CLOUD_CONNECTION_SUCCESS
                });
            } else {
                handleOffline(response.status,dispatch);
            }
        },
        error => {
            //alert(JSON.stringify(error));
            handleOffline(error,dispatch);
        }
    ).catch((err) => {
        //alert(JSON.stringify(err));
        handleOffline(err,dispatch);
    });
};

export const handleOffline = (error,dispatch) => {
    dispatch({
        type: CLOUD_CONNECTION_FAILED,
        error
    });
    // on failure, we try again...
    /*setTimeout(() => {
        dispatch(fetchCurrentCloudState());
    }, 10000);*/

}

export const getCurrentCloudState = () => (dispatch, getState) => {
    return new Promise((resolve,reject) => {
        const cloudState = getCloudState(getState());
        resolve(cloudState);
    });
};
