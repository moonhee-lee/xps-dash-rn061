import { normalize } from 'normalizr';
import * as schema from '../../redux//schema';
import * as orgSystemApi from '../../redux//api/org_system';
import * as systemLocal from '../../redux//local/system';

import {
    FETCH_ORG_SYSTEM_BY_ORG_FAILURE,
    FETCH_ORG_SYSTEM_BY_ORG_REQUEST,
    FETCH_ORG_SYSTEM_BY_ORG_SUCCESS
} from "../constants/actionTypes";


export const fetchSystemsByOrg = (orgId) => (dispatch, getState) => {
    // dispatch the FETCH_ORG_MEMBER_BY_ORG_REQUEST action.
    dispatch({
        type: FETCH_ORG_SYSTEM_BY_ORG_REQUEST,
        orgId
    });

    // call the API method to login the user.
    return orgSystemApi.fetchSystemsByOrg(orgId).then(
        response => {
            // on success, dispatch the FETCH_ORG_MEMBER_BY_ORG_SUCCESS action
            dispatch({
                type: FETCH_ORG_SYSTEM_BY_ORG_SUCCESS,
                orgId,
                response: normalize(response.data.results, schema.orgSystemListSchema)
            });
            systemLocal.saveAllSystems(response.data.results);
        },
        error => {
            console.log('loading systems from local');
            //didn't get systems from API, check to see if we have local copies
            systemLocal.fetchAllSystems().then((systems) => {
                dispatch({
                    type: FETCH_ORG_SYSTEM_BY_ORG_SUCCESS,
                    orgId,
                    response: normalize(systems, schema.orgSystemListSchema)
                });
            }).catch((err) => {
                console.log('could not load systems');
                // on error, dispatch the FETCH_ORG_MEMBER_BY_ORG_FAILURE action with teamId and error message.
                dispatch({
                    type: FETCH_ORG_SYSTEM_BY_ORG_FAILURE,
                    orgId,
                    errorMessage: error,
                });
            })
        }
    );
}
