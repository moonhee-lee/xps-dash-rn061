import _ from 'lodash';

import { normalize } from 'normalizr';
import * as schema from '../../redux/schema';
import { localStorage } from '../../lib/local_storage';

import {
    FETCH_ACTIVE_USERS_REQUEST,
    FETCH_ACTIVE_USERS_SUCCESS,
    FETCH_ACTIVE_USER_REQUEST,
    FETCH_ACTIVE_USER_SUCCESS,
    FETCH_ACTIVE_USER_FAILURE,
    SAVE_ACTIVE_USER_REQUEST,
    SAVE_ACTIVE_USER_SUCCESS,
    SAVE_ACTIVE_USER_FAILURE,
    DELETE_ACTIVE_USER_REQUEST,
    DELETE_ACTIVE_USER_SUCCESS,
    DELETE_ACTIVE_USER_FAILURE,
    SYSTEM_SPRINT_BEEP_START,
    SET_ACTIVE_USER_VELOCITY,
    SET_ACTIVE_USER_VERTICAL,
    SAVE_ACTIVE_USER_TAG,
    CLEAR_ACTIVE_USERS,
    REMOVE_USER_FROM_ACTIVITY, SYSTEM_UPDATED
} from '../../redux/constants/actionTypes';

export const fetchActiveUsers = () => (dispatch, getState) => {
    // dispatch the SAVE_ACTIVE_USER_REQUEST action.
    dispatch({
        type: FETCH_ACTIVE_USERS_REQUEST,
    });
    localStorage.getItem('xps:activeUsers').then((activeUsers) => {
        dispatch({
            type: FETCH_ACTIVE_USERS_SUCCESS,
            response: normalize(activeUsers, schema.activeUserListSchema)
        });
    }).catch((e) => {
        dispatch({
            type: CLEAR_ACTIVE_USERS
        });
        dispatch({
            type: FETCH_ACTIVE_USER_FAILURE,
            errorMessage: 'Something went wrong',
        })
    });
}

const _saveActiveUser = (dispatch, activeUsers, activeUserToSave) => {
    const id = activeUserToSave.user.id;
    const updatedActiveUsers = {
        ...activeUsers,
        [id]: {
            ...activeUserToSave,
            id: id
        }
    }
    localStorage.setItem('xps:activeUsers',updatedActiveUsers).then(() => {
        dispatch({
            type: SAVE_ACTIVE_USER_SUCCESS,
            response: normalize(updatedActiveUsers[id], schema.activeUserSchema)
        });
    }).catch((e) => {
        dispatch({
            type: SAVE_ACTIVE_USER_FAILURE
        });
    });
}

export const saveActiveUser = (activeUserToSave) => (dispatch, getState) => {
    // dispatch the SAVE_ACTIVE_USER_REQUEST action.
    dispatch({
        type: SAVE_ACTIVE_USER_REQUEST,
        activeUserToSave: activeUserToSave.user.id,
    });
    localStorage.getItem('xps:activeUsers').then((activeUsers) => {
        _saveActiveUser(dispatch, activeUsers, activeUserToSave);
    }).catch((e) => {
        _saveActiveUser(dispatch, {}, activeUserToSave);
    });
}

export const saveUserTag = (userId,tag) => (dispatch, getState) => {
    localStorage.setItem('xps:savedTags',tag,userId).then((savedTag) => {
        dispatch({
            type: SAVE_ACTIVE_USER_TAG,
            tag,
            user:userId
        });
    }).catch((e) => {

    });
}

export const getUserSavedTag = (userId) => (dispatch, getState) => {
    return localStorage.getItem('xps:savedTags',userId);
}

export const removeActiveUserFromActivity = (activeUserId) => (dispatch, getState) => {
    dispatch({
        type: REMOVE_USER_FROM_ACTIVITY,
        activeUserId
    });
}

export const updateLocalActiveUserID = (newID,tempID) => (dispatch, getState) => {
    const updatedActiveUsers = {};
    localStorage.getItem('xps:activeUsers').then((activeUsers) => {
        _.each(activeUsers,(activeUser,index) => {
            if(activeUser.id === tempID) {
                activeUser.id = newID;
                activeUser.user.id = newID;
                _.assign(updatedActiveUsers,{[newID]: activeUser});
            } else {
                _.assign(updatedActiveUsers,{[index]: activeUser});
            }
        });
        localStorage.setItem('xps:activeUsers',updatedActiveUsers).then(() => {
            dispatch(fetchActiveUsers());
        }).catch((e) => console.log(e));
    }).catch((e) => console.log(e));
}


export const deleteActiveUser = (activeUserId) => (dispatch, getState) => {
    // dispatch the SAVE_ACTIVE_USER_REQUEST action.
    dispatch({
        type: DELETE_ACTIVE_USER_SUCCESS,
        activeUserId,
    });

    localStorage.getItem('xps:activeUsers').then((activeUsers) => {
        const _activeUsers = _.filter(activeUsers, (item) => item.id !== activeUserId);
        localStorage.setItem('xps:activeUsers',_activeUsers).then(() => {});
    });
}


export const clearActiveBeepStartTime = (activityId) => (dispatch, getState) => {
    dispatch({
        type: SYSTEM_SPRINT_BEEP_START,
        activityId,
        beepStartTime:0
    });
}

export const setActiveBeepStartTime = (activityId, beepStartTime) => (dispatch, getState) => {
    if (!global.client) return;

    dispatch({
        type: SYSTEM_UPDATED,
        offset_data: {
            beepStartTime: beepStartTime
        },
    });

    dispatch({
        type: SYSTEM_SPRINT_BEEP_START,
        activityId,
        beepStartTime
    });

    const msg = {time:beepStartTime,event_id:'AppStart'};
    global.client.sendMessage(msg, 'io_event');

}

export const clearActiveUsersVertical = (activityUsers) => (dispatch, getState) => {
    activityUsers.map((activityUser) => {
        dispatch({
            type: SET_ACTIVE_USER_VERTICAL,
            tag:activityUser.tag,
            vertical:0
        });
    });
}
