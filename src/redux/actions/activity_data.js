import _ from 'lodash';
import { normalize } from 'normalizr';
import * as schema from '../../redux//schema';
import * as activityDataApi from '../../redux//api/activity_data';
import * as activityLocal from '../../redux//local/activity';

import {
    FETCH_ACTIVITY_DATA_REQUEST,
    FETCH_ACTIVITY_DATA_SUCCESS,
    FETCH_ACTIVITY_DATA_FAILURE,
} from '../../redux//constants/actionTypes';


/**
 * redux/actions/activity_data - fetchActivityRecordsByActivity
 *
 * fetch activity_data via the activityDataApi
 *
 * @param  {Integer} activityId
 *
 */
export const fetchActivityDataById = (recordId) => (dispatch, getState) => {
    dispatch({
        type: FETCH_ACTIVITY_DATA_REQUEST,
        recordId,
    });

    return activityDataApi.fetchActivityDataById(recordId).then(
        response => {
            dispatch({
                type: FETCH_ACTIVITY_DATA_SUCCESS,
                recordId,
                response: normalize(response.data, schema.activityDataSchema)
            });
        },
        error => {
            dispatch({
                type: FETCH_ACTIVITY_DATA_FAILURE,
                recordId,
                errorMessage: 'Something went wrong',
            });
        }
    )
}

export const fetchActivityDataByRecord = (record) => (dispatch, getState) => {
    dispatch({
        type: FETCH_ACTIVITY_DATA_REQUEST,
        recordId: record.id,
    });

    if(record.localID) {
        return activityLocal.fetchActivityDataByRecord(record).then((localRecords) => {
                dispatch({
                    type: FETCH_ACTIVITY_DATA_SUCCESS,
                    recordId: record.id,
                    response: normalize(localRecords, schema.activityDataSchema)
                });
        }).catch((err) => {console.log(err)});
    } else {
        return activityDataApi.fetchActivityDataById(record.id).then(
            response => {
                dispatch({
                    type: FETCH_ACTIVITY_DATA_SUCCESS,
                    recordId: record.id,
                    response: normalize(response.data, schema.activityDataSchema)
                });
            },
            error => {
                console.log(error);
                dispatch({
                    type: FETCH_ACTIVITY_DATA_FAILURE,
                    recordId: record.id,
                    errorMessage: 'Something went wrong',
                });
            });
    }
}
