import { normalize } from 'normalizr';
import * as schema from '../../redux/schema';
import * as activityApi from '../../redux/api/activity';
import * as activityLocal from '../../redux/local/activity';
import * as sessionLocal from '../../redux/local/session';
import { setActiveSession } from '../../redux/actions/active_session';
import { getSessionsByOrg, getActiveSession } from '../../redux/reducers';
import { getTimeRangedMessages, getTimeRangedBuffer, getRawMessages, hasActivityDataInBuffer, hasActivityRawDataInBuffer } from '../../redux/local_storage';
import { getTagPosition } from '../../redux/local_storage';
import * as formatUtils from '../../lib/format_utils';

import _ from 'lodash';
import moment from 'moment';

import {
    FETCH_ACTIVITY_REQUEST,
    FETCH_ACTIVITY_SUCCESS,
    FETCH_ACTIVITY_FAILURE,
    FETCH_ACTIVITY_BY_USER_REQUEST,
    FETCH_ACTIVITY_BY_USER_SUCCESS,
    FETCH_ACTIVITY_BY_USER_FAILURE,
    SAVE_ACTIVITY_REQUEST,
    SAVE_ACTIVITY_SUCCESS,
    SAVE_ACTIVITY_FAILURE,
    SAVE_LOCAL_ACTIVITY_SUCCESS,
    SAVE_LOCAL_ACTIVITY_FAILURE,
    SAVE_SESSION_REQUEST,
    SAVE_SESSION_SUCCESS,
    SAVE_SESSION_FAILURE,
    SAVE_LOCAL_SESSION_SUCCESS,
    SAVE_LOCAL_SESSION_FAILURE,
    ACTIVITY_TOGGLE_RECORDING_MODE,
    ACTIVITY_RESET_RECORDING_MODE,
    ACTIVITY_RESET_LIVE_RECORDING_MODE,
} from '../../redux/constants/actionTypes';


/**
 * activityActions - fetchActivitiesBySession
 *
 * fetch the activity via the activityApi
 *
 * @param  {Integer} sessionId
 *
 */
export const fetchActivities = () => (dispatch, getState) => {
    // dispatch the SAVE_ACTIVITY_REQUEST action.
    dispatch({
        type: FETCH_ACTIVITY_REQUEST
    });

    return activityLocal.fetchActivities().then((localActivities) => {
        // call the API method to create(save) this new session.
        activityApi.fetchActivities().then(
            response => {
                const activities = _.concat(response.data.results,localActivities);
                dispatch({
                    type: FETCH_ACTIVITY_SUCCESS,
                    response: normalize(activities, schema.activityListSchema)
                });
            },
            error => {
                if(localActivities) {
                    dispatch({
                        type: FETCH_ACTIVITY_SUCCESS,
                        response: normalize(localActivities, schema.activityListSchema)
                    });
                } else {
                    // on error, dispatch the FETCH_ACTIVITY_FAILURE action with sessionId and error message.
                    dispatch({
                        type: FETCH_ACTIVITY_FAILURE,
                        errorMessage: 'Something went wrong',
                    });
                }
            }
        );
    }).catch((e) => {});
}

export const fetchActivitiesBySession = (sessionId) => (dispatch, getState) => {
    // dispatch the SAVE_ACTIVITY_REQUEST action.
    dispatch({
        type: FETCH_ACTIVITY_REQUEST,
        sessionId,
    });

    return activityLocal.fetchActivitiesBySession(sessionId).then((localActivities) => {
        // call the API method to create(save) this new session.
        activityApi.fetchActivitiesBySession(sessionId).then(
            response => {
                const activities = _.concat(response.data.results,localActivities);
                dispatch({
                    type: FETCH_ACTIVITY_SUCCESS,
                    sessionId,
                    response: normalize(activities, schema.activityListSchema)
                });
            },
            error => {
                if(localActivities) {
                    dispatch({
                        type: FETCH_ACTIVITY_SUCCESS,
                        sessionId,
                        response: normalize(localActivities, schema.activityListSchema)
                    });
                } else {
                    // on error, dispatch the FETCH_ACTIVITY_FAILURE action with sessionId and error message.
                    dispatch({
                        type: FETCH_ACTIVITY_FAILURE,
                        sessionId,
                        errorMessage: 'Something went wrong',
                    });
                }
            }
        );
    }).catch((e) => {});
}

export const fetchActivitiesByUser = (userId) => (dispatch, getState) => {
    // dispatch the FETCH_ACTIVITY_BY_USER_REQUEST action.
    dispatch({
        type: FETCH_ACTIVITY_BY_USER_REQUEST,
        userId,
    });

    // call the API method to create(save) this new session.
    return activityApi.fetchActivitiesByUser(userId).then(
        response => {
            dispatch({
                type: FETCH_ACTIVITY_BY_USER_SUCCESS,
                userId,
                response: normalize(response.data.results, schema.activityListSchema)
            });
        },
        error => {
            // on error, dispatch the FETCH_ACTIVITY_BY_USER_FAILURE action with sessionId and error message.
            dispatch({
                type: FETCH_ACTIVITY_BY_USER_FAILURE,
                userId,
                errorMessage: 'Something went wrong',
            });
        }
    )
}

export const fetchActivity = (activityId) => (dispatch, getState) => {
    // dispatch the FETCH_ACTIVITY_REQUEST action.
    dispatch({
        type: FETCH_ACTIVITY_REQUEST,
        activityId,
    });

    // call the API method to create(save) this new session.
    return activityApi.fetchActivity(activityId).then(
        response => {
            dispatch({
                type: FETCH_ACTIVITY_SUCCESS,
                activityId,
                response: normalize(response.data, schema.activitySchema)
            });
        },
        error => {
            // on error, dispatch the FETCH_ACTIVITY_FAILURE action with activityId and error message.
            dispatch({
                type: FETCH_ACTIVITY_FAILURE,
                activityId,
                errorMessage: 'Something went wrong',
            });
        }
    )
}

export const calculateDistance = (activeUser) => {
    let tagName = parseInt(activeUser.tag.name);
    let startPositions = getTagPosition(tagName, 'start');
    let endPositions = getTagPosition(tagName, 'end');

    let startDistances = [0];
    if (startPositions !== undefined && startPositions !== null) {
        startDistances = startPositions.map(pos => formatUtils.getDistance(pos));
    }

    let endDistances = [0];
    if (endPositions !== undefined && endPositions !== null) {
        endPositions.map(pos => formatUtils.getDistance(pos));
    }

    let startMedDistance = formatUtils.getMedian(startDistances);
    let endMedDistance = formatUtils.getMedian(endDistances);

    return endMedDistance - startMedDistance;
}

export const saveActivity = (liveActivity, activeUsers, incomplete=false) => (dispatch, getState) => {
    // dispatch the SAVE_LIVE_ACTIVITY_RECORD_REQUEST action.
    dispatch({
        type: SAVE_ACTIVITY_REQUEST,
    });

    const state = getState();
    let session = null;

    if (!session) {
        const sessions = getSessionsByOrg(state, liveActivity.org);
        const today = moment().format('MMMM DD, YYYY');

        // or try to find one already named today's date
        session = _.find(sessions, (s) => s.name === today);

        if (!session) {
            // or create one with today's date.
            session = {
                id: parseInt(moment(new Date()).format('YYYYMMDD')),
                name: today,
                org: liveActivity.org
            }

            const sessionData = {...session, org: liveActivity.org};

            sessionLocal.saveSession(sessionData).then(
                response => {
                    dispatch({
                        type: SAVE_LOCAL_SESSION_SUCCESS,
                        sessionData,
                        response: normalize(sessionData, schema.sessionSchema)
                    });
                    //dispatch(fetchSessionsByOrg(liveActivity.org));
                    dispatch(setActiveSession(sessionData));
                },
                error => {
                    console.log(error);
                    dispatch({
                        type: SAVE_LOCAL_SESSION_FAILURE,
                        sessionData,
                        errorMessage: 'Something went wrong',
                    });
                }
            );
        }
    }

    const sessionData = {...session, org: liveActivity.org};

    let promises = [];
    let errors = [];

    const isManualHorizontalJump = (global.enableManualHorizontalJump && liveActivity && liveActivity.activity_type==='jump' && liveActivity.orientation==='horizontal');

    _.each(activeUsers, (activityUser) => {
        let timeRangedESTMessages = null, data_summary = null;
        if(incomplete) timeRangedESTMessages = getTimeRangedBuffer(activityUser, true);
        else timeRangedESTMessages = getTimeRangedMessages(activityUser, true);

        const events = activityUser.events;

        const raw = getRawMessages(activityUser);

        if(!activityUser.meta_data) activityUser.meta_data = {};

        let reaction_time = 0, peak_frequency = {}, peak_contact = {}, peak_length = {}, stride_summary = {}, cod_summary = {}, count, peak_angular_velocity = {}, peak_duration = {}, peak_degChange = {};
        if(!!activityUser.meta_data.reaction_time) reaction_time = parseFloat(activityUser.meta_data.reaction_time);

        if(liveActivity.activity_type!=='jump') {
            data_summary = {
                start: activityUser.meta_data.start,
                end: activityUser.meta_data.end,
                sprint_time: parseFloat(activityUser.meta_data.total_time) - parseFloat(reaction_time),
                total_time: activityUser.meta_data.total_time,
                max_velocity: activityUser.meta_data.max_velocity,
                max_acceleration: activityUser.meta_data.max_acceleration,
                reaction_time: reaction_time,
            }

            if (liveActivity.activity_type === 'sprint') {
                if (!!activityUser.meta_data.stride_data_metres && _.size(activityUser.meta_data.stride_data_metres) > 0) {
                    // peak_frequency = _.maxBy(activityUser.meta_data.stride_data_metres, (stride) => stride.frequency);
                    // peak_contact = _.maxBy(activityUser.meta_data.stride_data_metres, (stride) => stride.contact_time);
                    // peak_length = _.maxBy(activityUser.meta_data.stride_data_metres, (stride) => stride.length);
                    data_summary = {...data_summary, stride_summary};
                }
            }

            if (liveActivity.activity_type === 'agility') {
                if (!!activityUser.meta_data.changes_of_direction && _.size(activityUser.meta_data.changes_of_direction) > 0) {
                    count = _.size(activityUser.meta_data.changes_of_direction);
                    peak_angular_velocity = _.maxBy(activityUser.meta_data.changes_of_direction, (cod) => cod.averagedOmega);
                    peak_duration = _.maxBy(activityUser.meta_data.changes_of_direction, (cod) => cod.end - cod.start);
                    peak_degChange = _.maxBy(activityUser.meta_data.changes_of_direction, (cod) => cod.newHeadingDeg - cod.oldHeadingDeg);
                    const wasSalvaged = _.filter(activityUser.meta_data.changes_of_direction, (cod) => cod.wasSalvaged);

                    cod_summary = {
                        count,
                        peak_angular_velocity: peak_angular_velocity.averagedOmega,
                        peak_duration: peak_duration.end - peak_duration.start,
                        peak_degChange: peak_degChange.newHeadingDeg - peak_degChange.oldHeadingDeg,
                        wasSalvaged
                    };

                    if(!_.isEmpty(wasSalvaged))
                        data_summary = {...data_summary, cod_summary, hasSalvagedCod: true};
                    else
                        data_summary = {...data_summary, cod_summary};
                }
            }
        } else {
            let distance = activityUser.meta_data.length;
            if (isManualHorizontalJump === true) {
                distance = calculateDistance(activityUser);
            }

            data_summary = {
                start: activityUser.meta_data.start,
                end: activityUser.meta_data.end,
                height: parseFloat(activityUser.meta_data.height),
                distance,
                pushoff_acceleration: activityUser.meta_data.takeoffAcceleration,
                takeoff_angle: activityUser.meta_data.takeoffAngle,
                air_time: activityUser.meta_data.air_time,
            }
        }

        const activityRecordData = {
            user: activityUser.user.id,
            session: sessionData.id,
            type_definition: liveActivity,
            labels: [],
            data_summary: {...data_summary, events},
            data: {est: timeRangedESTMessages, events: events, ...raw}
        };

        if(incomplete) activityRecordData.labels.push('incomplete');
        if(activityUser.meta_data.wasSalvaged) activityRecordData.labels.push('backstop');

        const salvagedCoDs = _.filter(activityUser.meta_data.changes_of_direction, (cod) => cod.wasSalvaged);
        if(!_.isEmpty(salvagedCoDs)) activityRecordData.labels.push('hasSalvagedCoD');

        const pl = activityLocal.saveActivity(activityRecordData).then(
            response => {
                dispatch({
                    type: SAVE_LOCAL_ACTIVITY_SUCCESS,
                    liveActivity,
                    response: normalize(activityRecordData, schema.activitySchema)
                });
            },
            error => {
                // on error, dispatch the SAVE_ACTIVITY_RECORD_FAILURE action with activityRecordData and error message.
                //errors.push(error);
                dispatch({
                    type: SAVE_LOCAL_ACTIVITY_FAILURE,
                    liveActivity,
                    errorMessage: 'Something went wrong',
                });
            }
        );
        promises.push(pl);
    });

    return Promise.all(promises).then(() => {
        if (errors.length === 0) {
            // then reset this activity in case they want to record again.
            dispatch({
                type: ACTIVITY_RESET_LIVE_RECORDING_MODE,
                activityId: liveActivity.id,
            });
            dispatch({
                type: SAVE_LOCAL_ACTIVITY_SUCCESS
            });
        }
    });

}

export const saveFreeformActivity = (liveActivity, activeUsers) => (dispatch, getState) => {
    // dispatch the SAVE_LIVE_ACTIVITY_RECORD_REQUEST action.
    dispatch({
        type: SAVE_ACTIVITY_REQUEST,
    });

    const state = getState();
    let session = null;//getActiveSession(state);

    if (!session) {
        //const sessions = getSessionsByOrg(state, liveActivity.org);
        const today = moment().format('MMMM DD, YYYY');
        //const sessionId = typeof(liveActivity.session) === 'object' ? liveActivity.session.id : liveActivity.session;

        // try to find the session based on id
        //session = _.find(sessions, (s) => s.id === sessionId);
        //if (!session) {
            // or try to find one already named today's date
        //    session = _.find(sessions, (s) => s.name === today);
        //}
        if (!session) {
            // or create one with today's date.
            session = {
                id: parseInt(moment(new Date()).format('YYYYMMDD')),
                name: today,
                org: liveActivity.org
            }

            const sessionData = {...session, org: liveActivity.org};

            sessionLocal.saveSession(sessionData).then(
                response => {
                    dispatch({
                        type: SAVE_LOCAL_SESSION_SUCCESS,
                        sessionData,
                        response: normalize(sessionData, schema.sessionSchema)
                    });
                    //dispatch(fetchSessionsByOrg(liveActivity.org));
                    dispatch(setActiveSession(sessionData));
                },
                error => {
                    console.log(error);
                    dispatch({
                        type: SAVE_LOCAL_SESSION_FAILURE,
                        sessionData,
                        errorMessage: 'Something went wrong',
                    });
                }
            );
        }
    }

    const sessionData = {...session, org: liveActivity.org};

    let promises = [];
    let errors = [];

    _.each(activeUsers, (activityUser) => {
        const raw = getRawMessages(activityUser);

        activityUser.meta_data = {};

        const first = _.first(raw.est);
        const last = _.last(raw.est);
        const score = (last.time-first.time)/1000;

        const activityRecordData = {
            user: activityUser.user.id,
            session: sessionData.id,
            type_definition: liveActivity,
            labels: [],
            data_summary: {},
            data: {...raw}
        };

        const pl = activityLocal.saveActivity(activityRecordData).then(
            response => {
                dispatch({
                    type: SAVE_LOCAL_ACTIVITY_SUCCESS,
                    liveActivity,
                    response: normalize(activityRecordData, schema.activitySchema)
                });
            },
            error => {
                // on error, dispatch the SAVE_ACTIVITY_RECORD_FAILURE action with activityRecordData and error message.
                //errors.push(error);
                dispatch({
                    type: SAVE_LOCAL_ACTIVITY_FAILURE,
                    liveActivity,
                    errorMessage: 'Something went wrong',
                });
            }
        );
        promises.push(pl);
    });

    dispatch(resetRecording(liveActivity.id));

    return Promise.all(promises).then(() => {
        if (errors.length === 0) {
            // then reset this activity in case they want to record again.
            dispatch({
                type: ACTIVITY_RESET_LIVE_RECORDING_MODE,
                activityId: liveActivity.id,
            });
            dispatch({
                type: SAVE_LOCAL_ACTIVITY_SUCCESS
            });
        }
    });

}

export const toggleRecording = (activityId, tags, isRecording) => (dispatch, getState) => {
    dispatch({
        type: ACTIVITY_TOGGLE_RECORDING_MODE,
        activityId,
        tags,
        isRecording
    });
}


export const resetRecording = (activityId) => (dispatch, getState) => {
    console.log('resetting recording mode');
    dispatch({
        type: ACTIVITY_RESET_RECORDING_MODE,
        activityId,
    });
    dispatch({
        type: ACTIVITY_RESET_LIVE_RECORDING_MODE,
        activityId,
    });
}

export const resetRecordingAll = () => (dispatch, getState) => {
    dispatch({
        type: ACTIVITY_RESET_LIVE_RECORDING_MODE,
    });
}

export const getHasActivityDataInBuffer = (activeUsers) => (dispatch, getState) => {
    return hasActivityDataInBuffer(activeUsers);
}

export const getHasActivityRawDataInBuffer = (activeUsers) => (dispatch, getState) => {
    return hasActivityRawDataInBuffer(activeUsers);
}
