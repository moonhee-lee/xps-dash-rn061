import {
    SET_ACTIVE_SESSION,
    SET_ACTIVE_ACTIVITY,
} from '../../redux/constants/actionTypes';


/**
 * actions/active_session
 *
 * save the active session
 *
 * @param  {Object} session
 */
export const setActiveSession = (session) => (dispatch, getState) => {
    dispatch({
        type: SET_ACTIVE_SESSION,
        session,
    });
};
/**
 * actions/active_session
 *
 * save the active activity
 *
 * @param  {Object} activity
 */
export const setActiveActivity = (activity) => (dispatch, getState) => {
    dispatch({
        type: SET_ACTIVE_ACTIVITY,
        activity
    });
};
