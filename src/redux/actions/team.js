import { normalize } from 'normalizr';
import * as schema from '../../redux/schema';
import * as teamApi from '../../redux/api/team';

import {
    FETCH_TEAM_REQUEST,
    FETCH_TEAM_SUCCESS,
    FETCH_TEAM_FAILURE,
    FETCH_DEFAULT_TEAM_SUCCESS,
    FETCH_DEFAULT_TEAM_FAILURE,
    CURRENT_TEAM_FETCH_SUCCESS
} from '../../redux/constants/actionTypes';

import { localStorage } from '../../lib/local_storage';


/**
 * teamActions - fetchTeamsByOrg
 *
 * fetch the team via the teamApi
 *
 * @param  {Integer} teamId
 *
 */
export const fetchTeamsByOrg = (orgId) => (dispatch, getState) => {
    // dispatch the SAVE_TEAM_REQUEST action.
    dispatch({
        type: FETCH_TEAM_REQUEST,
        orgId,
    });

    // call the API method to create(save) this new team.
    return teamApi.fetchTeamsByOrg(orgId).then(
        response => {
            dispatch({
                type: FETCH_TEAM_SUCCESS,
                orgId,
                response: normalize(response.data.results, schema.teamListSchema)
            });
        },
        error => {
            // on error, dispatch the FETCH_TEAM_FAILURE action with orgId and error message.
            dispatch({
                type: FETCH_TEAM_FAILURE,
                orgId,
                errorMessage: 'Something went wrong',
            });
        }
    )
}

export const setCurrentTeam = (teamId) => (dispatch, getState) => {
    console.log('changing team: '+teamId)
    localStorage.setItem('xps:currentTeam', teamId);
    global.currentTeam = teamId;
    dispatch({
        type: CURRENT_TEAM_FETCH_SUCCESS,
        currentTeam: teamId
    });
}
