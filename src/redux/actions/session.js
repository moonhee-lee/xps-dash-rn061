import { normalize } from 'normalizr';
import * as schema from '../../redux//schema';
import * as sessionApi from '../../redux//api/session';
import * as sessionLocal from '../../redux//local/session';
import { getCloudState } from '../../redux//reducers';
import _ from 'lodash';
import { getAxiosClient } from '../../redux/api/client';

import {
    FETCH_SESSION_REQUEST,
    FETCH_SESSION_SUCCESS,
    FETCH_SESSION_FAILURE,
    SAVE_SESSION_REQUEST,
    SAVE_SESSION_SUCCESS,
    SAVE_SESSION_FAILURE
} from '../constants/actionTypes';


/**
 * sessionActions - fetchSessionsByOrg
 *
 * fetch the session via the sessionApi
 *
 * @param  {Integer} sessionId
 *
 */
export const fetchSessionsByOrg = (orgId) => (dispatch, getState) => {

    // dispatch the SAVE_SESSION_REQUEST action.
    dispatch({
        type: FETCH_SESSION_REQUEST,
        orgId,
    });

    return sessionLocal.fetchSessionsByOrg(orgId).then((localSessions) => {
        // call the API method to create(save) this new session.
        sessionApi.fetchSessionsByOrg(orgId).then((response) => {
            const sessions = _.concat(response.data.results, localSessions);
            const sortedSessions = sessions.sort((memberA, memberB) => {
                return memberA.name < memberB.name;
            });
            dispatch({
                type: FETCH_SESSION_SUCCESS,
                orgId,
                response: normalize(sortedSessions, schema.sessionListSchema)
            });
        }).catch((error) => {
            if (localSessions.length) {
                dispatch({
                    type: FETCH_SESSION_SUCCESS,
                    orgId,
                    response: normalize(localSessions, schema.sessionListSchema)
                });
            } else {
                // on error, dispatch the FETCH_SESSION_FAILURE action with orgId and error message.
                dispatch({
                    type: FETCH_SESSION_FAILURE,
                    orgId,
                    errorMessage: 'Something went wrong',
                });
            }
        });
    }).catch((e) => {
        sessionApi.fetchSessionsByOrg(orgId).then(
            response => {
                const sortedSessions = response.data.results.sort((memberA, memberB) => {
                    return memberA.name < memberB.name;
                });
                dispatch({
                    type: FETCH_SESSION_SUCCESS,
                    orgId,
                    response: normalize(sortedSessions, schema.sessionListSchema)
                });
            },
            error => {
                // on error, dispatch the FETCH_SESSION_FAILURE action with orgId and error message.
                dispatch({
                    type: FETCH_SESSION_FAILURE,
                    orgId,
                    errorMessage: 'Something went wrong',
                });
            }
        )
    });
}


export const saveSession = (sessionData, navigator, modalProps, setActiveSession) => (dispatch, getState) => {
    // dispatch the SAVE_SESSION_REQUEST action.
    dispatch({
        type: SAVE_SESSION_REQUEST,
        sessionData,
    });

    // call the API method to create(save) this new session.
    return sessionLocal.saveSession(sessionData).then(
        response => {
            dispatch({
                type: SAVE_SESSION_SUCCESS,
                sessionData,
                response: normalize(sessionData, schema.sessionSchema)
            });
            if(setActiveSession) setActiveSession(sessionData);
            if (navigator) {
                navigator.dismissModal({
                    animationType: 'none'
                }).then(()=>{
                    if(modalProps) {
                        modalProps.passProps = {session:sessionData};
                        navigator.showModal(modalProps);
                    }
                });
            }
        },
        error => {
            console.log(error);
            // on error, dispatch the SAVE_SESSION_FAILURE action with sessionSlug and error message.
            dispatch({
                type: SAVE_SESSION_FAILURE,
                sessionData,
                errorMessage: 'Something went wrong',
            });
        }
    )
}
