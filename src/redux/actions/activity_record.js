import _ from 'lodash';
import { normalize } from 'normalizr';
import * as schema from '../../redux//schema';
import * as activityApi from '../../redux//api/activity';
import * as activityRecordApi from '../../redux//api/activity_record';
import * as activityLocal from '../../redux//local/activity';
import { fetchActivity, saveActivity } from '../../redux//actions/activity';
import { fetchActivityDataByRecord } from '../../redux//actions/activity_data';
import { fetchActivitySummariesByActivity } from '../../redux//actions/activity_summary';
import { getTimeRangedMessages, getTimeRangedBuffer, getRawMessages } from '../../redux//local_storage';
import { localStorage } from '../../lib/local_storage';

import {
    FETCH_ACTIVITY_RECORD_REQUEST,
    FETCH_ACTIVITY_RECORD_SUCCESS,
    FETCH_ACTIVITY_RECORD_FAILURE,
    FETCH_SINGLE_ACTIVITY_RECORD_REQUEST,
    FETCH_SINGLE_ACTIVITY_RECORD_SUCCESS,
    FETCH_SINGLE_ACTIVITY_RECORD_FAILURE,
    FETCH_ACTIVITY_RECORD_BY_USER_REQUEST,
    FETCH_ACTIVITY_RECORD_BY_USER_SUCCESS,
    FETCH_ACTIVITY_RECORD_BY_USER_FAILURE,
    SAVE_ACTIVITY_RECORD_REQUEST,
    SAVE_ACTIVITY_RECORD_SUCCESS,
    SAVE_ACTIVITY_RECORD_FAILURE,
    SAVE_ACTIVITY_REQUEST,
    SAVE_ACTIVITY_SUCCESS,
    SAVE_ACTIVITY_FAILURE,
    FETCH_LIVE_ACTIVITY_RECORD_REQUEST,
    FETCH_LIVE_ACTIVITY_RECORD_SUCCESS,
    FETCH_LIVE_ACTIVITY_RECORD_FAILURE,
    FETCH_LIVE_ACTIVITY_RECORD_BY_USER_REQUEST,
    FETCH_LIVE_ACTIVITY_RECORD_BY_USER_SUCCESS,
    FETCH_LIVE_ACTIVITY_RECORD_BY_USER_FAILURE,
    SAVE_LIVE_ACTIVITY_RECORD_REQUEST,
    SAVE_LIVE_ACTIVITY_RECORD_SUCCESS,
    SAVE_LIVE_ACTIVITY_RECORD_FAILURE,
    ACTIVITY_RESET_RECORDING_MODE,
    ACTIVITY_RESET_LIVE_RECORDING_MODE
} from '../../redux//constants/actionTypes';


/**
 * redux/actions/activity_record - fetchActivityRecordsByActivity
 *
 * fetch activity_record via the activityRecordApi
 *
 * @param  {Integer} activityId
 *
 */
export const fetchActivityRecordById = (recordId) => (dispatch, getState) => {
    console.log('getting activity record');
    // dispatch the SAVE_ACTIVITY_RECORD_REQUEST action.
    dispatch({
        type: FETCH_SINGLE_ACTIVITY_RECORD_REQUEST,
        recordId,
    });

    // call the API method to create(save) this new message.
    return activityRecordApi.fetchActivityRecordById(recordId).then(
        response => {
            response.data.id = response.data.activity;
            dispatch({
                type: FETCH_SINGLE_ACTIVITY_RECORD_SUCCESS,
                recordId,
                response: normalize(response.data, schema.activityRecordSchema)
            });
        },
        error => {
            // on error, dispatch the FETCH_ACTIVITY_RECORD_FAILURE action with activitySummary and error message.
            console.log(error);
            dispatch({
                type: FETCH_SINGLE_ACTIVITY_RECORD_FAILURE,
                recordId,
                errorMessage: 'Something went wrong',
            });
        }
    )
}

export const fetchActivityRecords = (type_hash, sessionId) => (dispatch, getState) => {
    // dispatch the SAVE_ACTIVITY_RECORD_REQUEST action.
    dispatch({
        type: FETCH_ACTIVITY_RECORD_REQUEST,
        type_hash
    });

    return activityLocal.fetchActivitiesBySession(sessionId).then((localActivities) => {
        // call the API method to create(save) this new message.
        activityRecordApi.fetchActivityRecords(type_hash,sessionId).then(
            response => {
                const activities = _.concat(response.data.results,localActivities);
                dispatch({
                    type: FETCH_ACTIVITY_RECORD_SUCCESS,
                    type_hash,
                    response: normalize(activities, schema.activityRecordListSchema)
                });
                _.each(activities,(a) => {
                    dispatch(fetchActivityDataByRecord(a));
                });

            },
            error => {
                if(localActivities) {
                    dispatch({
                        type: FETCH_ACTIVITY_RECORD_SUCCESS,
                        sessionId,
                        response: normalize(localActivities, schema.activityRecordListSchema)
                    });
                } else {
                    // on error, dispatch the FETCH_ACTIVITY_RECORD_FAILURE action with activitySummary and error message.
                    dispatch({
                        type: FETCH_ACTIVITY_RECORD_FAILURE,
                        type_hash,
                        errorMessage: 'Something went wrong',
                    });
                }
            }
        );
    }).catch((e)=>{});
}

/**
 * redux/actions/activity_record - fetchActivityRecordsByUser
 *
 * fetch activity_record via the activityRecordApi
 *
 * @param  {Integer} activityId
 *
 */
export const fetchActivityRecordsByUser = (userId) => (dispatch, getState) => {
    // dispatch the FETCH_ACTIVITY_RECORD_BY_USER_REQUEST action.
    dispatch({
        type: FETCH_ACTIVITY_RECORD_BY_USER_REQUEST,
        userId,
    });

    // call the API method to create(save) this new message.
    return activityRecordApi.fetchActivityRecordsByUser(userId).then(
        response => {
            dispatch({
                type: FETCH_ACTIVITY_RECORD_BY_USER_SUCCESS,
                userId,
                response: normalize(response.data.results, schema.activityRecordListSchema)
            });
        },
        error => {
            // on error, dispatch the FETCH_ACTIVITY_RECORD_BY_USER_FAILURE action with activitySummary and error message.
            dispatch({
                type: FETCH_ACTIVITY_RECORD_BY_USER_FAILURE,
                userId,
                errorMessage: 'Something went wrong',
            });
        }
    )
}


export const saveActivityRecords = (currentActivity, activityUsers, navigator) => (dispatch, getState) => {
    // dispatch the SAVE_ACTIVITY_RECORD_REQUEST action.
    dispatch({
        type: SAVE_ACTIVITY_RECORD_REQUEST,
    });

    let promises = [];
    let errors = [];

    _.each(activityUsers, (activityUser) => {
        const timeRangedESTMessages = getTimeRangedMessages(activityUser, true);

        const activityRecordData = {
            activity: currentActivity.id,
            user: activityUser.user.id,
            tag: activityUser.tag.id,
            meta: activityUser.meta_data,
            score: activityUser.score,
            data: timeRangedESTMessages,
        }

        const p = activityRecordApi.saveActivityRecord(activityRecordData).then(
            response => {
                dispatch({
                    type: SAVE_ACTIVITY_RECORD_SUCCESS,
                    activityUserId: activityUser.id,
                    response: normalize(response.data, schema.activityRecordSchema)
                });
            },
            error => {
                // on error, dispatch the SAVE_ACTIVITY_RECORD_FAILURE action with activityRecordData and error message.
                errors.push(error);
                dispatch({
                    type: SAVE_ACTIVITY_RECORD_FAILURE,
                    activityUserId: activityUser.id,
                    errorMessage: 'Something went wrong',
                });
            }
        );

        promises.push(p);
    });

    return Promise.all(promises).then(() => {
        if (errors.length === 0) {
            // then reset this activity in case they want to record again.
            dispatch({
                type: ACTIVITY_RESET_LIVE_RECORDING_MODE,
                activityId: currentActivity.id,
            });
            dispatch(fetchActivity(currentActivity.id));
            dispatch(fetchActivitySummariesByActivity(currentActivity.id));
            if(navigator) navigator.dismissModal();
        }
    });
}

export const saveLiveActivityRecords = (liveActivity, activeUsers, incomplete=false) => (dispatch, getState) => {
    // dispatch the SAVE_LIVE_ACTIVITY_RECORD_REQUEST action.
    dispatch({
        type: SAVE_LIVE_ACTIVITY_RECORD_REQUEST,
    });

    let promises = [];
    let errors = [];

    _.each(activeUsers, (activityUser) => {
        let timeRangedESTMessages;
        if(incomplete) timeRangedESTMessages = getTimeRangedBuffer(activityUser, true);
        else timeRangedESTMessages = getTimeRangedMessages(activityUser, true);

        const events = [];


        const activityRecordData = {
            user: activityUser.user.id,
            session: liveActivity.session.id,
            type_definition: liveActivity,
            labels: '',
            data_summary: activityUser.meta_data,
            data: {est: timeRangedESTMessages, events: events, raw: null}
        }

        //if(global.debugMode) {
        if(incomplete) activityRecordData.incomplete = true;
        if(global.debugMode) {
            activityRecordData.data.raw = getRawMessages(activityUser);
        }

        //TODO setup local storage for offline mode
        localStorage.getItem('savedActivityRecords').then((data) => {
            const activityRecords = _.concat(data,activityRecordData)
            //localStorage.setItem('savedActivityRecords', activityRecords);
        }).catch((e) => {
            if (e) {
                let activityRecords = [activityRecordData];
                //localStorage.setItem('savedActivityRecords', activityRecords);
            }
        });

        const p = activityApi.saveActivity(activityRecordData).then(
            response => {
                dispatch({
                    type: SAVE_ACTIVITY_SUCCESS,
                    liveActivity,
                    response: normalize(response.data, schema.activitySchema)
                });
            },
            error => {
                // on error, dispatch the SAVE_ACTIVITY_RECORD_FAILURE action with activityRecordData and error message.
                errors.push(error);
                dispatch({
                    type: SAVE_ACTIVITY_FAILURE,
                    liveActivity,
                    errorMessage: 'Something went wrong',
                });
            }
        );

        promises.push(p);
    });

    return Promise.all(promises).then(() => {
        if (errors.length === 0) {
            // then reset this activity in case they want to record again.
            dispatch({
                type: ACTIVITY_RESET_LIVE_RECORDING_MODE,
                activityId: liveActivity.id,
            });
        }
    });

}
