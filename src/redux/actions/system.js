import _ from 'lodash';
import { AppState, NativeModules } from 'react-native';
import * as mqtt from '../../redux//mqtt';
import { processEstMessages } from '../../lib/motion_utils';
import { addESTMessage, newMessageCount, getMsgsToProcess, moveProcessedToCompleted, completedMessageCount, addESTVelocity, setESTPosition, addRawMessage, addControlMessage, addEventMessage, addTagPosition } from '../../redux//local_storage';
import { getTagLiveDataById, getCurrentSystem, getTagsInLiveData, getWifiState, getCurrentOrg, getActiveSession, getActiveUsers, getLiveActivity } from '../../redux//reducers';

import moment from "moment";
import { Alert } from 'react-native';
import Message from "react-native-paho-mqtt/src/Message";

import {
    SYSTEM_CONNECTING,
    SYSTEM_CONNECTED,
    SYSTEM_UPDATED,
    SYSTEM_DISCONNECTING,
    SYSTEM_DISCONNECTED,
    SYSTEM_STOP_RETRIES,
    SYSTEM_TAG_READY,
    SYSTEM_TAG_NOT_READY,
    SYSTEM_TAG_SELECT_CONFIRMED,
    SYSTEM_FETCH_SUCCESS,
    SET_TAG_BATTERY,
    WIFI_DISCONNECT_SUCCESS,
    CLEAR_USER_TAG_DATA,
    SYSTEM_MODE_SELECT_CONFIRMED,
    SYSTEM_MODE_SELECT_FAILED,
    APP_STATE_CHANGE,
    SYSTEM_FIRMWARE_UPDATE_SENDING,
    SYSTEM_FIRMWARE_UPDATE_COMPLETE,
    SYSTEM_FIRMWARE_UPDATE_FAILED
} from '../constants/actionTypes';
import {
    SYSTEM_STATE_CONNECTING,
    SYSTEM_STATE_CONNECTED,
    SYSTEM_STATE_DISCONNECTING,
    SYSTEM_STATE_DISCONNECTED,
} from '../../redux//constants/systemStates';

const handleEstMessage = (dispatch, state, message, messageJSON) => {

    // make sure the message has a tagId
    const tagName = parseInt(messageJSON.tagId, 10);
    if (!tagName) return;


    const tag = getTagLiveDataById(state, tagName);
    if (!tag || !tag.motionAlgo) return;

    if (!!tag && tag.status === 'recording') {
        //addRawMessage(tagName, messageJSON, message.destinationName);
    }

    if(messageJSON.tagBatt && messageJSON.tagBatt!==tag.battery) {
        dispatch({
            type: SET_TAG_BATTERY,
            tagName,
            battery: messageJSON.tagBatt
        });
    }

    if (global.enableTagPosition === true) {
        addTagPosition(tagName, messageJSON);
    }

    if (global.enableManualHorizontalJump === true) {
        addTagPosition(tagName, messageJSON, tag.position);
    }

    if(tag.motionAlgo === 'measuring') {
        addESTMessage(tagName, messageJSON, true);
        return;
    }

    switch (tag.status) {
        case 'inactive':
        case 'select_pending':
            // for EST messages in inactive or select_pending state
            // we just ignore.
            break;
        case 'selected':
            // for a tag that is 'selected', we wait for kfState 2 to transition to ready
            if (parseInt(messageJSON.kfState, 10) === 2) {
                dispatch({
                    type: SYSTEM_TAG_READY,
                    tagName,
                    time: moment(new Date()).format('X')
                });
            }
            break;
        case 'ready':
            // for a tag that is 'ready', any kfState not equal to 2 means we're not ready anymore
            if (parseInt(messageJSON.kfState, 10) !== 2) {
                dispatch({
                    type: SYSTEM_TAG_NOT_READY,
                    tagName,
                    time: moment(new Date()).format('X')
                });
            }
            break;
        case 'recording':
            if(tag.motionAlgo !== 'freeform') {
                // in recording state.. let's process it!
                // let's store these locally... until a buffer fills up, then send it through
                // the algorithm..
                addESTMessage(tagName, messageJSON);

                if (tag.activityType !== 'jump' && completedMessageCount(tagName) % 3 === 0) {
                    if (messageJSON.velMPS && messageJSON.velMPS.y && parseInt(messageJSON.velMPS.y) >= 0.5) {
                        addESTVelocity(tagName, Math.abs(messageJSON.velMPS.y));
                    } else {
                        addESTVelocity(tagName, 0);
                    }
                }

                if (newMessageCount(tagName) === 5) {
                    const msgs = getMsgsToProcess(tagName);
                    // call processEstMessages with requestAnimationFrame to not block...
                    requestAnimationFrame(() => {
                        processEstMessages(msgs, tag, dispatch)
                    });
                }
            } else {
                addRawMessage(tagName, messageJSON, message.destinationName);
            }
            break;
        default:
            break;
    }
}

const handleRawMessage = (dispatch, state, message, messageJSON) => {
    const tagName = parseInt(messageJSON.tag_id, 10);
    if (!tagName) return;

    const tag = getTagLiveDataById(state, tagName);
    if (!tag || !tag.motionAlgo) return;

    switch (tag.status) {
        case 'recording':
            if(tag.motionAlgo === 'freeform') {
                addRawMessage(tagName, messageJSON, message.destinationName);
            }
        default:
            break;
    }
}

export const processMessage = (message, dispatch, state) => {
    if (!message.payloadString) return;
    const messageJSON = JSON.parse(message.payloadString);
    const cmdId = parseInt(messageJSON.cmd_id);
    const msgId = parseInt(messageJSON.msg_id);

    if (global.debugMode) {
        switch (message.destinationName) {
            case 'hub_config':
                dispatch({
                    type: SYSTEM_UPDATED,
                    hub_config: messageJSON
                });
                break;

            case 'control':
            case 'control_response':
                addControlMessage(messageJSON, message.destinationName);
                break;
            default:
                break;
        }
    }

    switch (message.destinationName) {
        case 'control':
            if (cmdId === 2 && messageJSON.tag_sel && messageJSON.tag_sel.tags) {
                dispatch({
                    type: SYSTEM_UPDATED,
                    selected_tags: messageJSON.tag_sel.tags
                });
            }
            break;

        case 'control_response':
            if (cmdId === 101) {
                if (global.sentMsgs[msgId]) {
                    const msg = global.sentMsgs[msgId];
                    if (parseInt(msg.cmd_id) === 2) {
                        dispatch({
                            type: SYSTEM_TAG_SELECT_CONFIRMED
                        });
                    }
                    if (parseInt(msg.cmd_id) === 10) {
                        dispatch({
                            type: SYSTEM_MODE_SELECT_CONFIRMED,
                            mode: msg.mode_sel.mode
                        });
                    }
                    delete global.sentMsgs[msgId];
                    delete global.sentMsgCounters[msgId];
                }
            }
            if (cmdId === 102 || cmdId === 103) {
                //alert(global.sentMsgs[msgId]);
                if (global.sentMsgs[msgId]) {
                    const msg = global.sentMsgs[msgId];
                    if (parseInt(msg.cmd_id) === 2) {

                    }
                    if (parseInt(msg.cmd_id) === 10) {
                        dispatch({
                            type: SYSTEM_MODE_SELECT_FAILED
                        });
                    }
                    delete global.sentMsgs[msgId];
                    delete global.sentMsgCounters[msgId];
                    if (global.sentMsgCounters[msgId]) {
                        if (global.sentMsgCounters[msgId] < 3) {
                            global.sentMsgCounters[msgId] = global.sentMsgCounters[msgId] + 1;
                            global.client.sendMessage(msg, 'control', msgId);
                        }
                    } else {
                        global.sentMsgCounters[msgId] = 1;
                        global.client.sendMessage(msg, 'control', msgId);
                    }
                }
            }
            break;

        case 'update_response':
            if (cmdId === 101) {
                dispatch({
                    type: SYSTEM_FIRMWARE_UPDATE_COMPLETE
                });
            }
            if(cmdId === 102 || cmdId === 103) {
                dispatch({
                    type: SYSTEM_FIRMWARE_UPDATE_FAILED
                });
            }
            break;

        case 'io_event':
            addEventMessage(messageJSON, message.destinationName);
            break;

        case 'tag_raw':
            handleRawMessage(dispatch, state, message, messageJSON);
            break;

        case 'est':
            handleEstMessage(dispatch, state, message, messageJSON);
            break;

        default:
            break;
    }
}

export const unReadyTag = (tagName) => (dispatch, getState) => {
    console.log('lost tag '+tagName);
    dispatch({
        type: SYSTEM_TAG_NOT_READY,
        tagName,
        time: moment(new Date()).format('X')
    });
}

export const setupMqttClient = (system, dispatch, getState) => {
    if (global.client) return;
    global.client = mqtt.createClient(system.ip_address, system.port);
    global.client.messageQueue = [];

    global.client.on('connectionLost', (responseObject) => {
        const state = getState();
        const system = getCurrentSystem(state);

        console.log(responseObject);

        if (system.state === SYSTEM_STATE_DISCONNECTING) {
            console.log('mqtt disconnected');
        } else if (system.state === SYSTEM_STATE_CONNECTING || system.state === SYSTEM_STATE_CONNECTED) {
            dispatch(connectSystem(system,false));
        }
        dispatch({
            type: SYSTEM_DISCONNECTED,
            systemId: system.id
        });
    });

    global.client.on('messageReceived', (message) => {
        processMessage(message, dispatch, getState());
    });

    global.client.sendMessage = (msg, dest='control', msg_id=null) => {
        if(!global.client) return;
        const existingMsg = _.pickBy(global.sentMsgs,(sMsg) => {
            if(msg.cmd_id===2) {
                return _.isEqual(msg.tag_sel,sMsg.tag_sel);
            }
            if(msg.cmd_id===10) {
                return _.isEqual(msg.mode_sel,sMsg.mode_sel);
            }
        });
        if(!_.isEmpty(existingMsg)) return;
        msg.msg_id = msg_id?msg_id:Math.floor(Math.random() * 1000000000);
        const message = new Message(JSON.stringify(msg));
        message.destinationName = dest;
        //alert(JSON.stringify(global.sentMsgs));
        try {
            global.client.send(message);
            if (dest === 'control') global.sentMsgs[msg.msg_id] = msg;
        } catch (err) {
            //alert(err);
            const system = getCurrentSystem(getState());
            dispatch(connectSystem(system,false));
        }
    }
}

export const connectSystem = (system, reset=false) => (dispatch, getState) => {
    // reset means let's stop any current retries (if any)
    try {
        //dispatch(setupWSListener(system));
        const currentSystem = getCurrentSystem(getState());

        if (reset) {
            dispatch({
                type: SYSTEM_STOP_RETRIES
            });
        }

        //if(wifiState.deviceSSID !== currentSystem.ssid) return;
        if(!!currentSystem && currentSystem.state === SYSTEM_STATE_CONNECTED) return;

        setupMqttClient(system, dispatch, getState);

        dispatch({
            type: SYSTEM_CONNECTING,
            systemId: system.id,
        });

        const willMessage = new Message(JSON.stringify({
            cmd_id: 2,
            msg_id: Math.floor(Math.random() * 1000000000),
            'tag_sel': {'tags': _.fill(Array(8), 0)},
            lwt: true
        }));
        willMessage.destinationName = 'control';

        //console.log(JSON.stringify(global.client));

        //disconnect existing client prior to connecting
        return global.client.connect({
            timeout: 1000,
            onFailure: (error) => {
                alert(error.errorMessage)
            }, reconnect: false,
            cleanSession: true,
            willMessage
        }).then(() => {
            global.client.subscribe('est');
            global.client.subscribe('control_response');
            global.client.subscribe('control');
            // global.client.subscribe('tag_raw');
            // global.client.subscribe('io_event');
            global.client.subscribe('update');
            global.client.subscribe('update_response');
            //if(global.debugMode) global.client.subscribe('tag_raw');

            // Set Power Level
            const message = {cmd_id: 4, pwr_lvl: {pwrlvl: global.uwbTransmitPowerLevel}};
            global.client.sendMessage(message, 'control');

            if(!_.isEmpty(global.sentMsgs)) {
                console.log('resending last '+_.size(global.sentMsgs)+' messages');
                _.each(global.sentMsgs,(msg,key) => {
                    delete global.sentMsgs[key];
                    global.client.sendMessage(msg);
                })
            }

            dispatch({
                type: SYSTEM_UPDATED,
                sync_timing: {
                    syncStarted: false,
                    initialOffset: false,
                    timingValid: true,
                    isLocal: system.ip_address && (system.ip_address.indexOf('192.168') >= 0 || system.ip_address.indexOf('10.3') >= 0)
                },
            });

            dispatch({
                type: SYSTEM_CONNECTED,
                systemId: system.id,
            });
        }).catch((err) => {
            //console.log(err);
            // on failure, we try again... unless told to stop
            setTimeout(() => {
                const system = getCurrentSystem(getState());
                if (system.isConnectRetryOn) {
                    dispatch(connectSystem(system, false));
                }
            }, 5000);
        });
    } catch(err) {
        console.log(err);
    }
}

export const disconnectSystem = (system,fromAppStateChange=false) => (dispatch, getState) => {
    //if (!system || system.state !== SYSTEM_STATE_CONNECTED || !global.client) return;
    if (!system) return;

    if(fromAppStateChange) {
        dispatch({
            type: SYSTEM_DISCONNECTING,
            systemId: system.id,
            lastState: system.state
        });
    } else {
        dispatch({
            type: SYSTEM_DISCONNECTING,
            systemId: system.id,
            lastState: null
        });
    }
    dispatch(stopSync(system));
    dispatch(clearTags(true));
    //setTimeout(() => {
    try {
        global.client.disconnect();
    } catch(err) {
        //alert(err);
    }
    //},1000);
}

export const stopWifiReconnect = () => (dispatch, getState) => {
    dispatch({
        type: SYSTEM_STOP_RETRIES
    });
};

export const sendModeSelect = (mode) => (dispatch, getState) => {
    const system = getCurrentSystem(getState());
    if (!global.client || !system.isLocal) return;
    dispatch({type: SYSTEM_MODE_SELECT_CONFIRMED, mode: 0});
    console.log('sending mode select message - mode: ' + mode);
    const message = {cmd_id: 10, mode_sel: {mode: mode}};
    global.client.sendMessage(message, 'control', Math.floor(Math.random() * 1000000000));
}

const _clearTags = () => {
    const clearTagsMsg = {cmd_id: 2, 'tag_sel': {'tags': _.fill(Array(8), 0)}};
    global.client.sendMessage(clearTagsMsg);
}
export const clearTags = (disconnect=false) => (dispatch, getState) => {
    if (!global.client) return;
    _clearTags();
}

// TODO...review SYNC code..
export const getTimingData = (system, dispatch, getState) => {
    if (system.isLocal && system.syncStarted) {
        console.log('getting timing data');
        global.timedStart.getTimingData().then(
            response => {
                console.log('EST time: '+response.currentTime);
                console.log('app time: '+response.currentAppTime);
                console.log('offset: '+response.timedOffset);
                console.log('max offset diff: '+response.maxOffsetDiff);
                console.log('min offset diff: '+response.minOffsetDiff);
                console.log('timing valid: '+response.timingValid);

                let timingValid = true;
                let initialOffset = system.initialOffset;

                if (response.maxOffsetDiff > 200) {
                    timingValid = false;
                }
                if(parseInt(response.maxOffsetDiff)>10000) {
                    timingValid = false;
                }
                if(parseInt(response.currentTime)===parseInt(response.currentAppTime)) {
                    timingValid = false;
                }

                if (!initialOffset) initialOffset = response.timedOffset;
                if (Math.abs(response.timedOffset - system.timedOffset) > 10) {
                    timingValid = false;
                }

                dispatch({
                    type: SYSTEM_UPDATED,
                    sync_timing: {
                        timingValid,
                        initialOffset
                    }
                });
            },
            error => {
                console.error(error);
            });
    }
}

export const initSync = () => {
    if (!global.timedStart) global.timedStart = NativeModules.XpsTimedStart;
    global.timedStart.initialize();
}

export const startSync = (system) => (dispatch, getState) => {
    if ((system.isLocal || global.debugMode) && !system.syncStarted) {
        syncStart(system,dispatch,getState);
        clearInterval(global.timeSyncOngoingInterval);
        global.timeSyncOngoingInterval = setInterval(() => {
            console.log('re-syncing for new timing data');
            resetSync(system, dispatch, getState);
        },30000);
    }
}

export const stopSync = (system) => (dispatch, getState) => {
    clearInterval(global.timeSyncInterval);
    clearInterval(global.timeSyncOngoingInterval);
    syncStop(system,dispatch,getState);
}

export const syncStop = (system,dispatch,getState) => {
    if ((system.isLocal || global.debugMode)) {
        console.log('stopping sync');
        dispatch({
            type: SYSTEM_UPDATED,
            sync_timing: {
                syncStarted: false
            }
        });
        if (global.timedStart) global.timedStart.stopSync();
    }
}

export const resetSync = (system,dispatch, getState) => {
    if(system.state !== SYSTEM_STATE_CONNECTED) return;
    console.log('resetting the time sync');
    clearInterval(global.timeSyncInterval);
    dispatch({
        type: SYSTEM_DISCONNECTED,
        system
    });
    //clearInterval(system.syncOngoingInterval);
    syncStart(system, dispatch, getState);
}

export const syncStart = (system,dispatch,getState) => {
    system = getCurrentSystem(getState());
    if(system.state !== SYSTEM_STATE_CONNECTED) return;
    initSync();

    dispatch({
        type: SYSTEM_DISCONNECTED,
        system
    });
    dispatch({
        type: SYSTEM_UPDATED,
        sync_timing: {
            syncStarted: true,
            initialOffset: false,
        }
    });
    console.log('starting sync');

    // TODO - leave these default values???
    const port = 5555;
    const estAlpha = 0.025;
    const estBeta = 0.025;
    const syncCounter = 10;
    const latencyThreshold = 100;
    const initSyncInterval = 100;
    const syncEvery = 1000;
    const retryInterval = 2000;

    global.timedStart.startSync(
        system.ip_address,    // '34.234.106.159'
        port,
        estAlpha,
        estBeta,
        syncCounter,
        latencyThreshold,
        initSyncInterval,
        syncEvery,
        retryInterval,
    );

    let counter=0;
    clearInterval(global.timeSyncInterval);
    global.timeSyncCounter = 0;
    global.timeSyncInterval = setInterval(() => {
        const _system = getCurrentSystem(getState());
        getTimingData(_system,dispatch,getState);
        counter++;
        if(counter>=syncCounter) {
            console.log('clearing timing data interval');
            clearInterval(global.timeSyncInterval);
            if (!_system.timingValid) {
                //console.log(global.timeSyncCounter);
                if(global.timeSyncCounter<10) {
                    resetSync(_system, dispatch, getState);
                    global.timeSyncCounter++;
                } else {
                    syncStop(_system, dispatch, getState);
                    Alert.alert(
                        'Time Sync Error',
                        "We were unable to sync the App's time - please reboot the Hub and App and try again.",
                        [
                            {text: 'OK'}
                        ],
                        {cancelable: true},
                    );
                }
            } else {
                syncStop(_system, dispatch, getState);
            }
        }
    },200);
}

export const testStartSound = (sound) => (dispatch, getState) => {
    initSync();
    global.timedStart.testStartSound(sound.file,sound.type);
}

export const saveSystemInfo = (system) => (dispatch, getState) => {
    dispatch({
        type: SYSTEM_FETCH_SUCCESS,
        system
    });
}

export const fetchSystemState = () => (dispatch, getState) => {
    const _state = getState();
    if(!_state) return false;
    const system = getCurrentSystem(_state);
    return system.state;
}

export const setTagsToNotReady = (dispatch,state) => {
    console.log('setting tags to not ready');
    const activeUsers = getActiveUsers(state);
    _.each(activeUsers,(activeUser) => {
        if(activeUser.tag && activeUser.tag.name) {
            //alert('un-readying tag: '+activeUser.tag.name)
            dispatch({
                type: CLEAR_USER_TAG_DATA,
                tagName: parseInt(activeUser.tag.name)
            });
        }
    });
}


export const appDidEnterBackground = () => (dispatch, getState) => {
    const state = getState();
    const system = getCurrentSystem(state);
    const wifiState = getWifiState(state);

    // if the system is connected, we need to clean up before we go into background state.
    if (system && wifiState && system.state === SYSTEM_STATE_CONNECTED && wifiState.deviceSSID === system.ssid) {

        _clearTags();
        setTagsToNotReady(dispatch,state);

        dispatch({
            type: SYSTEM_DISCONNECTED,
            systemId: system.id
        });
        dispatch(stopSync(system));

        global.client.disconnect();

    }
}
