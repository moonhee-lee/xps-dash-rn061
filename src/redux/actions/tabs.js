import {
    SAVE_ACTIVE_TAB,
} from '../../redux/constants/actionTypes';



/**
 * actions/tabs
 *
 * save the active tab
 *
 * @param  {Integer} index
 */
export const setActiveTab = (index) => (dispatch, getState) => {
    // dispatch the SAVE_ACTIVE_TAB action.
    dispatch({
        type: SAVE_ACTIVE_TAB,
        currentTab: index,
    });
};
