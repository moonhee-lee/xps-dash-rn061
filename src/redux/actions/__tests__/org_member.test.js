import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { normalize } from 'normalizr';

import * as api from '../../redux/api/org_member';
import * as actions from '../../redux/actions/org_member';
import * as schema from '../../redux/schema';
import mockData from '../../redux/api/__mocks__/data';

import {
    SAVE_ORG_MEMBER_REQUEST,
    SAVE_ORG_MEMBER_SUCCESS,
    SAVE_ORG_MEMBER_FAILURE,
    FETCH_ORG_MEMBER_BY_ORG_FAILURE,
    FETCH_ORG_MEMBER_BY_ORG_REQUEST,
    FETCH_ORG_MEMBER_BY_ORG_SUCCESS,
    FETCH_ORG_MEMBER_BY_USER_ID_REQUEST,
    FETCH_ORG_MEMBER_BY_USER_ID_SUCCESS,
    FETCH_ORG_MEMBER_BY_USER_ID_FAILURE,
    FETCH_ACTIVE_USERS_SUCCESS,
    FETCH_ACTIVE_USERS_REQUEST,
    FETCH_TAG_REQUEST,
    SET_CURRENT_ORG,
    FETCH_ORG_SYSTEM_BY_ORG_REQUEST
} from '../../redux/constants/actionTypes';
import {FETCH_USER_SUCCESS} from "../../constants/actionTypes";

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

jest.mock('../../redux/api/org_member');

jest.mock('moment', () => () => ({format: () => '123456789'}));
jest.mock("react-native-background-timer", () => {});
jest.mock("rn-ios-user-agent", () => {});

describe('actions/org_member suite', () => {
    const orgId = 1;
    const userId = 1;
    let orgMemberData;
    let store;

    beforeEach(() => {
        orgMemberData = {
            org: 1,
            user: {id:1,
                first_name:'Test',
                last_name:'Test',
                full_name: "Test Test",
                image_url: "",
                is_staff: false,
                org: {id: 1},
                thumbnail_url: ""
            }
        };
        store = mockStore({ org_members: {} });
        global.storage = {
            load: jest.fn(),
            save: jest.fn(),
            remove: jest.fn()
        }
    });

    describe('saveOrgMember action', () => {
        it('dispatches FETCH_ACTIVITY_SUMMARY_SUCCESS when saveOrgMember is complete', () => {
            const expectedActions = [
                { type: SAVE_ORG_MEMBER_REQUEST, orgMemberData },
                { type: SAVE_ORG_MEMBER_SUCCESS, orgMemberData, response: normalize(mockData.org_members[0], schema.orgMemberSchema)},
                { type: FETCH_ACTIVE_USERS_REQUEST },
                { type: FETCH_ORG_MEMBER_BY_ORG_REQUEST, orgId },
                //{ type: FETCH_ORG_MEMBER_BY_ORG_SUCCESS, orgId, response: normalize(mockData.org_members, schema.orgMemberListSchema)},
            ]
            return store.dispatch(actions.saveOrgMember(orgMemberData)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });

        it('dispatches FETCH_ACTIVITY_SUMMARY_SUCCESS and dismissModal when saveOrgMember is complete', () => {
            const navigator = {
                dismissModal: jest.fn()
            };
            const expectedActions = [
                { type: SAVE_ORG_MEMBER_REQUEST, orgMemberData },
                { type: SAVE_ORG_MEMBER_SUCCESS, orgMemberData, response: normalize(mockData.org_members[0], schema.orgMemberSchema)},
                { type: FETCH_ACTIVE_USERS_REQUEST },
                { type: FETCH_ORG_MEMBER_BY_ORG_REQUEST, orgId },
                //{ type: FETCH_ORG_MEMBER_BY_ORG_SUCCESS, orgId, response: normalize(mockData.org_members, schema.orgMemberListSchema)},
            ]
            return store.dispatch(actions.saveOrgMember(orgMemberData, navigator)).then(() => {
                expect(store.getActions()).toEqual(expectedActions);
                expect(navigator.dismissModal.mock.calls.length).toBe(1);
            });
        });

        it('dispatches FETCH_ACTIVITY_SUMMARY_SUCCESS and toggleAthlete when saveOrgMember is complete', () => {
            const toggleAthlete = jest.fn();
            const expectedActions = [
                { type: SAVE_ORG_MEMBER_REQUEST, orgMemberData },
                { type: SAVE_ORG_MEMBER_SUCCESS, orgMemberData, response: normalize(mockData.org_members[0], schema.orgMemberSchema)},
                { type: FETCH_ACTIVE_USERS_REQUEST },
                { type: FETCH_ORG_MEMBER_BY_ORG_REQUEST, orgId },
                //{ type: FETCH_ORG_MEMBER_BY_ORG_SUCCESS, orgId, response: normalize(mockData.org_members, schema.orgMemberListSchema)},
            ]
            return store.dispatch(actions.saveOrgMember(orgMemberData, null, toggleAthlete)).then(() => {
                expect(store.getActions()).toEqual(expectedActions);
                expect(toggleAthlete.mock.calls.length).toBe(1);
            });
        });

        it('dispatches FETCH_ACTIVITY_SUMMARY_FAILURE when saveOrgMember fails', () => {
            const expectedError = 'Something went wrong';
            const expectedActions = [
                { type: SAVE_ORG_MEMBER_REQUEST, orgMemberData },
                { type: SAVE_ORG_MEMBER_SUCCESS, orgMemberData, response: normalize(mockData.org_members[0], schema.orgMemberSchema)},
                { type: FETCH_ACTIVE_USERS_REQUEST },
                { type: FETCH_ORG_MEMBER_BY_ORG_REQUEST, orgId },
                //{ type: SAVE_ORG_MEMBER_FAILURE, orgMemberData, errorMessage: expectedError }
            ];

            api.saveOrgMember.mockImplementationOnce(() => Promise.reject(expectedError));

            return store.dispatch(actions.saveOrgMember(orgMemberData, false)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });
    });

    describe('fetchMembersByOrg action', () => {
        it('dispatches FETCH_ORG_MEMBER_BY_ORG_SUCCESS when fetchMembersByOrg is complete', () => {
            const expectedActions = [
                { type: FETCH_ORG_MEMBER_BY_ORG_REQUEST, orgId },
                { type: FETCH_ORG_MEMBER_BY_ORG_SUCCESS, orgId, response: normalize(mockData.org_members, schema.orgMemberListSchema)},
            ]
            return store.dispatch(actions.fetchMembersByOrg(orgId)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });

        it('dispatches FETCH_ORG_MEMBER_BY_ORG_FAILURE when fetchMembersByOrg fails', () => {
            const expectedError = 'Something went wrong';
            const expectedActions = [
                { type: FETCH_ORG_MEMBER_BY_ORG_REQUEST, orgId },
                { type: FETCH_ORG_MEMBER_BY_ORG_FAILURE, orgId, errorMessage: expectedError }
            ];

            api.fetchMembersByOrg.mockImplementationOnce(() => Promise.reject(expectedError));

            return store.dispatch(actions.fetchMembersByOrg(orgId)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });
    });

    describe('fetchMemberByUserId action', () => {
        it('dispatches FETCH_ORG_MEMBER_BY_USER_ID_SUCCESS when fetchMemberByUserId is complete', () => {
            const expectedActions = [
                { type: FETCH_ORG_MEMBER_BY_USER_ID_REQUEST, userId },
                { type: FETCH_ORG_MEMBER_BY_USER_ID_SUCCESS, userId, response: normalize(mockData.org_members[0], schema.orgMemberSchema)},
            ]
            return store.dispatch(actions.fetchMemberByUserId(userId)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });

        it('dispatches FETCH_ORG_MEMBER_BY_ORG_FAILURE when fetchMemberByUserId fails', () => {
            const expectedError = 'Something went wrong';
            const expectedActions = [
                { type: FETCH_ORG_MEMBER_BY_USER_ID_REQUEST, userId },
                { type: FETCH_ORG_MEMBER_BY_USER_ID_FAILURE, userId, errorMessage: expectedError }
            ];

            api.fetchMemberByUserId.mockImplementationOnce(() => Promise.reject(expectedError));

            return store.dispatch(actions.fetchMemberByUserId(userId)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });
        it('dispatches FETCH_ORG_MEMBER_BY_USER_ID_SUCCESS when fetchMemberByUserId is complete on login', () => {

            const expectedActions = [
                { type: FETCH_ORG_MEMBER_BY_USER_ID_REQUEST, userId },
                { type: FETCH_ORG_MEMBER_BY_USER_ID_SUCCESS, userId, response: normalize(mockData.org_members[0], schema.orgMemberSchema)},
                {org: 1, type: SET_CURRENT_ORG},
                { type: FETCH_USER_SUCCESS, userId:1, response: normalize(mockData.users[0], schema.userSchema)},
                {orgId: 1, type: FETCH_ORG_SYSTEM_BY_ORG_REQUEST},
                {orgId: 1, type: FETCH_TAG_REQUEST},
                {orgId: 1, type: FETCH_ORG_MEMBER_BY_ORG_REQUEST},

            ]
            return store.dispatch(actions.fetchMemberByUserId(userId,true)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });

        it('dispatches FETCH_ORG_MEMBER_BY_ORG_FAILURE when fetchMemberByUserId fails on login', () => {
            const expectedError = 'Something went wrong';
            const expectedActions = [
                { type: FETCH_ORG_MEMBER_BY_USER_ID_REQUEST, userId },
                { type: FETCH_ORG_MEMBER_BY_USER_ID_FAILURE, userId, errorMessage: expectedError }
            ];
            api.fetchMemberByUserId.mockImplementationOnce(() => Promise.reject(expectedError));

            return store.dispatch(actions.fetchMemberByUserId(userId,true)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });
    });
});
