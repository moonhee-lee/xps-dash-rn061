import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { normalize } from 'normalizr';

import * as actions from '../../redux/actions/live_activity';
import * as schema from '../../redux/schema';
import mockData from '../../redux/api/__mocks__/data';

import {
    FETCH_ACTIVITY_REQUEST,
    FETCH_ACTIVITY_SUCCESS,
    FETCH_ACTIVITY_FAILURE,
    FETCH_ACTIVITY_BY_USER_REQUEST,
    FETCH_ACTIVITY_BY_USER_SUCCESS,
    FETCH_ACTIVITY_BY_USER_FAILURE,
    SAVE_ACTIVITY_REQUEST,
    SAVE_ACTIVITY_SUCCESS,
    SAVE_ACTIVITY_FAILURE,
    ACTIVITY_TOGGLE_RECORDING_MODE,
    ACTIVITY_RESET_RECORDING_MODE,
} from '../../redux/constants/actionTypes';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

jest.mock("react-native-background-timer", () => {});
jest.mock("rn-ios-user-agent", () => {});

describe('actions/live_activity suite', () => {
    const sessionId = 1;
    const userId = 1;
    const activityId = 1;
    let store;

    beforeEach(() => {
        store = mockStore({ activities: {} });
    });

    describe('fetchLiveActivity action', () => {
        it('dispatches FETCH_ACTIVITY_SUCCESS when fetchLiveActivity is complete', () => {
            const expectedActions = [
                { type: FETCH_ACTIVITY_REQUEST },
                { type: FETCH_ACTIVITY_SUCCESS, response: normalize(mockData.activities, schema.activityListSchema)},
            ]
            actions.fetchLiveActivity();
        });

        it('dispatches FETCH_ACTIVITY_FAILURE when fetchLiveActivity fails', () => {
            const expectedError = 'Something went wrong';
            const expectedActions = [
                { type: FETCH_ACTIVITY_REQUEST },
                { type: FETCH_ACTIVITY_FAILURE, errorMessage: expectedError }
            ];

            actions.fetchLiveActivity();
        });
    });

    describe('saveLiveActivity action', () => {
        let activityData, navig, modalProps;
        beforeEach(() => {
            activityData = {

            };
            navig = {
                dismissModal: jest.fn(),
                showModal: jest.fn()
            };
            modalProps = {something: 'value'};
        });

        const localStorage = {
            setItem: jest.fn(),
        };

        // TODO fix save activity tests

        /*it('dispatches SAVE_ACTIVITY_SUCCESS when saveLiveActivity is complete', () => {
            const expectedActions = [
                { type: SAVE_ACTIVITY_REQUEST, activityData },
                { type: SAVE_ACTIVITY_SUCCESS, activityData, response: normalize(mockData.activities[0], schema.activitySchema)},
            ]
            localStorage.setItem.mockImplementationOnce(() => null);
            return store.dispatch(actions.saveLiveActivity(activityData, navig, modalProps)).then(() => {
                expect(store.getActions()).toEqual(expectedActions);
                expect(navig.dismissModal.mock.calls.length).toBe(1);
            });
        });

        it('dispatches SAVE_ACTIVITY_SUCCESS when saveLiveActivity is complete and showModal', () => {
            const expectedActions = [
                { type: SAVE_ACTIVITY_REQUEST, activityData },
                { type: SAVE_ACTIVITY_SUCCESS, activityData, response: normalize(mockData.activities[0], schema.activitySchema)},
            ]
            localStorage.setItem.mockImplementationOnce(() => null);
            return store.dispatch(actions.saveLiveActivity(activityData, navig, modalProps, true)).then(() => {
                expect(store.getActions()).toEqual(expectedActions);
                expect(navig.dismissModal.mock.calls.length).toBe(1);
                expect(navig.showModal.mock.calls.length).toBe(1);
            });
        });

        it('dispatches SAVE_ACTIVITY_SUCCESS when saveLiveActivity is complete without calling dismissModal', () => {
            const expectedActions = [
                { type: SAVE_ACTIVITY_REQUEST, activityData },
                { type: SAVE_ACTIVITY_SUCCESS, activityData, response: normalize(mockData.activities[0], schema.activitySchema)},
            ]
            localStorage.setItem.mockImplementationOnce(() => null);
            return store.dispatch(actions.saveLiveActivity(activityData, null, modalProps)).then(() => {
                expect(store.getActions()).toEqual(expectedActions);
                expect(navig.dismissModal.mock.calls.length).toBe(0);
            });
        });

        it('dispatches SAVE_ACTIVITY_FAILURE when saveLiveActivity fails', () => {
            const expectedError = 'Something went wrong';
            const expectedActions = [
                { type: SAVE_ACTIVITY_REQUEST, activityData },
                { type: SAVE_ACTIVITY_FAILURE, activityData, errorMessage: expectedError }
            ];

            localStorage.setItem.mockImplementationOnce(() => null);
            return store.dispatch(actions.saveLiveActivity(activityData, navig, modalProps)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });*/
    });
});
