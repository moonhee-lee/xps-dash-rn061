    import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { normalize } from 'normalizr';

import * as api from '../../redux/api/activity_record';
import * as actions from '../../redux/actions/activity_record';
import * as schema from '../../redux/schema';
import * as fromStorage from '../../redux/local_storage';
import mockData from '../../redux/api/__mocks__/data';

import {
    FETCH_ACTIVITY_RECORD_REQUEST,
    FETCH_ACTIVITY_RECORD_SUCCESS,
    FETCH_ACTIVITY_RECORD_FAILURE,
    FETCH_ACTIVITY_RECORD_BY_USER_REQUEST,
    FETCH_ACTIVITY_RECORD_BY_USER_SUCCESS,
    FETCH_ACTIVITY_RECORD_BY_USER_FAILURE,
    SAVE_ACTIVITY_RECORD_REQUEST,
    SAVE_ACTIVITY_RECORD_SUCCESS,
    SAVE_ACTIVITY_RECORD_FAILURE,
    ACTIVITY_RESET_RECORDING_MODE,
    FETCH_ACTIVITY_REQUEST,
    FETCH_ACTIVITY_SUMMARY_REQUEST,
    ACTIVITY_RESET_LIVE_RECORDING_MODE
} from '../../redux/constants/actionTypes';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

jest.mock('../../redux/api/activity_record');
jest.mock('../../redux/local_storage');
    jest.mock("react-native-background-timer", () => {});
    jest.mock("rn-ios-user-agent", () => {});

describe('actions/activity_record suite', () => {
    const activityId = 1;
    const userId = 1;
    let store;

    beforeEach(() => {
        store = mockStore({ activity_records: {} });
        global.storage = {
            save: jest.fn()
        };

    });

    describe('fetchActivityRecords action', () => {
        it('dispatches FETCH_ACTIVITY_RECORD_SUCCESS when fetchActivityRecords is complete', () => {
            const expectedActions = [
                {type: FETCH_ACTIVITY_RECORD_REQUEST, type_hash: 1},
                {response: {entities: {activity_record: {1: {activity: 1, id: 1, user: 1}, 2: {activity: 1, id: 2, user: 2}}}, result: [1, 2]}, type: FETCH_ACTIVITY_RECORD_SUCCESS, type_hash: 1},
                {recordId: 1, type: "FETCH_ACTIVITY_DATA_REQUEST"}, {recordId: 2, type: "FETCH_ACTIVITY_DATA_REQUEST"}
                ]
            return store.dispatch(actions.fetchActivityRecords(activityId, userId)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });
    });

    describe('fetchActivityRecordsByUser action', () => {
        it('dispatches FETCH_ACTIVITY_RECORD_BY_USER_SUCCESS when fetchActivityRecordsByUser is complete', () => {
            const expectedActions = [
                { type: FETCH_ACTIVITY_RECORD_BY_USER_REQUEST, userId },
                { type: FETCH_ACTIVITY_RECORD_BY_USER_SUCCESS, userId, response: normalize(mockData.activity_records, schema.activityRecordListSchema)},
            ]
            return store.dispatch(actions.fetchActivityRecordsByUser(userId)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });

        it('dispatches FETCH_ACTIVITY_RECORD_FAILURE when fetchActivityRecordsByUser fails', () => {
            const expectedError = 'Something went wrong';
            const expectedActions = [
                { type: FETCH_ACTIVITY_RECORD_BY_USER_REQUEST, userId },
                { type: FETCH_ACTIVITY_RECORD_BY_USER_FAILURE, userId, errorMessage: expectedError }
            ];

            api.fetchActivityRecordsByUser.mockImplementationOnce(() => Promise.reject(expectedError));

            return store.dispatch(actions.fetchActivityRecordsByUser(userId)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });
    });

    describe('saveActivityRecords action', () => {
        let activity, activityUsers, navig;
        beforeEach(() => {
            activity = {
                id: 1
            };
            activityUsers = [
                {
                    id: 1,
                    user: {id: 1},
                    tag: {id: 1},
                    meta: [],
                    data: [],
                }
            ];
        });

        it('dispatches SAVE_ACTIVITY_RECORD_SUCCESS when saveActivityRecords is complete', () => {
            const expectedActions = [
                { type: SAVE_ACTIVITY_RECORD_REQUEST },
                { type: SAVE_ACTIVITY_RECORD_SUCCESS, activityUserId: activityUsers[0].id, response: normalize(mockData.activity_records[0], schema.activityRecordSchema)},
                { type: ACTIVITY_RESET_LIVE_RECORDING_MODE, activityId: activity.id },
                { type: FETCH_ACTIVITY_REQUEST, activityId: activity.id },
                { type: FETCH_ACTIVITY_SUMMARY_REQUEST, activityId: activity.id },
            ]

            fromStorage.getTimeRangedMessages.mockImplementationOnce(() => []);

            return store.dispatch(actions.saveActivityRecords(activity, activityUsers)).then(() => {
                expect(store.getActions()).toEqual(expectedActions);
            });
        });

        it('dispatches SAVE_ACTIVITY_RECORD_SUCCESS when saveActivityRecords is complete without calling dismissModal', () => {
            const expectedActions = [
                { type: SAVE_ACTIVITY_RECORD_REQUEST },
                { type: SAVE_ACTIVITY_RECORD_SUCCESS, activityUserId: activityUsers[0].id, response: normalize(mockData.activity_records[0], schema.activityRecordSchema)},
                { type: ACTIVITY_RESET_LIVE_RECORDING_MODE, activityId: activity.id },
                { type: FETCH_ACTIVITY_REQUEST, activityId: activity.id },
                { type: FETCH_ACTIVITY_SUMMARY_REQUEST, activityId: activity.id },
            ]

            fromStorage.getTimeRangedMessages.mockImplementationOnce(() => []);

            return store.dispatch(actions.saveActivityRecords(activity, activityUsers)).then(() => {
                expect(store.getActions()).toEqual(expectedActions);
            });
        });

        it('dispatches SAVE_ACTIVITY_RECORD_FAILURE when saveActivityRecords fails', () => {
            const expectedError = 'Something went wrong';
            const expectedActions = [
                { type: SAVE_ACTIVITY_RECORD_REQUEST },
                { type: SAVE_ACTIVITY_RECORD_FAILURE, activityUserId: activityUsers[0].id, errorMessage: expectedError },
            ];

            fromStorage.getTimeRangedMessages.mockImplementationOnce(() => []);
            api.saveActivityRecord.mockImplementationOnce(() => Promise.reject(expectedError));

            return store.dispatch(actions.saveActivityRecords(activity, activityUsers)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });
    });
});
