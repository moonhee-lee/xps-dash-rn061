import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { normalize } from 'normalizr';

import * as actions from '../../redux/actions/tabs';

import {
    SAVE_ACTIVE_TAB
} from '../../redux/constants/actionTypes';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
jest.mock("react-native-background-timer", () => {});
jest.mock("rn-ios-user-agent", () => {});


describe('actions/tabs suite', () => {
    let store;

    beforeEach(() => {
        store = mockStore({ tabs: {} });
    });

    describe('setActiveTab action', () => {
        it('dispatches SAVE_ACTIVE_TAB when setActiveTab is complete', () => {
            const currentTab = 1;
            const expectedActions = [
                { type: SAVE_ACTIVE_TAB, currentTab }
            ]
            store.dispatch(actions.setActiveTab(currentTab));
            expect(store.getActions()).toEqual(expectedActions);
        });
    });
});
