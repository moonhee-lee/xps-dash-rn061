import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { NativeModules } from 'react-native';
import { NetworkInfo } from 'react-native-network-info';

import * as actions from '../../redux/actions/wifi';
import * as reducers from '../../redux/reducers';

import {
    WIFI_CONNECTION_REQUESTED,
    WIFI_CONNECTION_SUCCESS,
    WIFI_CONNECTION_FAILED,
    WIFI_STOP_RETRIES,
    WIFI_FETCH_FAILED,
} from '../../redux/constants/actionTypes';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('actions/wifi suite', () => {
    let store;

    beforeEach(() => {
        store = mockStore({ wifi: {wifiState: {isConnected: true}}, systems:{byIds: {1: {id:1}} }});
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    describe('fetchCurrentWifiState action', () => {
        let IOSWifiManager;
        beforeEach(() => {
            IOSWifiManager = {
                currentSSID: jest.fn()
            }
            NativeModules.IOSWifiManager = IOSWifiManager;
            NetworkInfo.getIPAddress = jest.fn();
            reducers.getWifiState = jest.fn();
            jest.clearAllMocks();
            jest.useFakeTimers();
        });

        it('does not dispatch anything when fetchAttempts > 5', () => {
            const wifiReturn = {fetchAttempts: 5,isConnected: true};
            reducers.getWifiState.mockImplementationOnce(() => wifiReturn);
            store.dispatch(actions.fetchCurrentWifiState());
            expect(store.getActions()).toEqual([]);
        });

        it('dispatches WIFI_CONNECTION_SUCCESS when fetchCurrentWifiState is complete', () => {
            const expectedActions = [
                { type: WIFI_CONNECTION_SUCCESS, IPAddress: 'loading', deviceSSID: 'test-ssid'},
            ]
            const wifiReturn = {fetchAttempts: 0,isConnected: true};
            IOSWifiManager.currentSSID.mockImplementationOnce(() => Promise.resolve('test-ssid'));
            reducers.getWifiState.mockImplementationOnce(() => wifiReturn);

            return store.dispatch(actions.fetchCurrentWifiState()).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });

        it('dispatches WIFI_FETCH_FAILED when fetchCurrentWifiState returns error', () => {
            const expectedActions = [
                { type: WIFI_FETCH_FAILED, error: 'wifi did not connect', deviceSSID: null},
            ]
            const wifiReturn = {fetchAttempts: 0,isConnected: true};
            IOSWifiManager.currentSSID.mockImplementationOnce(() => Promise.reject('wifi did not connect'));
            reducers.getWifiState.mockImplementationOnce(() => wifiReturn);

            return store.dispatch(actions.fetchCurrentWifiState()).then(() => {
                expect(store.getActions()).toEqual(expectedActions);
                expect(setTimeout).toHaveBeenCalledTimes(1);
            });
        });
    });

    describe('connectToProtectedSSID action', () => {
        let IOSWifiManager;
        beforeEach(() => {
            IOSWifiManager = {
                connectToProtectedSSID: jest.fn()
            }
            NativeModules.IOSWifiManager = IOSWifiManager;
            NetworkInfo.getIPAddress = jest.fn();
            jest.clearAllMocks();
            jest.useFakeTimers();
        });

        it('dispatches WIFI_CONNECTION_SUCCESS when connectToWifiNetwork is complete', () => {
            const expectedActions = [
                { type: WIFI_CONNECTION_REQUESTED, deviceSSID: 'test-ssid', reset: false},
                { type: WIFI_CONNECTION_SUCCESS, IPAddress: 'loading', deviceSSID: 'test-ssid'},
            ]
            IOSWifiManager.connectToProtectedSSID.mockImplementationOnce(() => Promise.resolve());

            return store.dispatch(actions.connectToWifiNetwork('test-ssid', 'asdfasdf')).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });

        it('dispatches WIFI_CONNECTION_FAILED when connectToWifiNetwork returns error', () => {
            const expectedActions = [
                { type: WIFI_CONNECTION_REQUESTED, deviceSSID: 'test-ssid', reset: false},
                { type: WIFI_CONNECTION_FAILED, error: 'wifi did not connect'},
            ]
            const wifiReturn = {fetchAttempts: 0,isConnected: true};
            IOSWifiManager.connectToProtectedSSID.mockImplementationOnce(() => Promise.reject('wifi did not connect'));
            reducers.getWifiState.mockImplementationOnce(() => wifiReturn);

            return store.dispatch(actions.connectToWifiNetwork('test-ssid', 'asdfasdf')).then(() => {
                expect(store.getActions()).toEqual(expectedActions);
                expect(setTimeout).toHaveBeenCalledTimes(1);
            });
        });
    });

    describe('stopWifiReconnect action', () => {
        it('dispatches WIFI_STOP_RETRIES', () => {
            const expectedActions = [
                {type: WIFI_STOP_RETRIES}
            ];
            store.dispatch(actions.stopWifiReconnect());
            expect(store.getActions()).toEqual(expectedActions);
        });
    });

});
