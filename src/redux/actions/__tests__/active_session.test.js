import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { normalize } from 'normalizr';

import * as actions from '../../redux/actions/active_session';

import {
    SET_ACTIVE_SESSION,
    SET_ACTIVE_ACTIVITY,
} from '../../redux/constants/actionTypes';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);


describe('actions/active_session suite', () => {
    const session = {something: 'value'};
    let store;

    beforeEach(() => {
        store = mockStore({ activity_records: {} });
    });

    describe('setActiveSession action', () => {
        it('dispatches SET_ACTIVE_SESSION when setActiveSession is complete', () => {
            const expectedActions = [
                { type: SET_ACTIVE_SESSION, session }
            ]
            store.dispatch(actions.setActiveSession(session));
            expect(store.getActions()).toEqual(expectedActions);
        });
    });

    describe('setActiveActivity action', () => {
        const activity = {id: 1};

        it('dispatches SET_ACTIVE_ACTIVITY when setActiveActivity is complete', () => {
            const expectedActions = [
                { type: SET_ACTIVE_ACTIVITY, activity }
            ]
            store.dispatch(actions.setActiveActivity(activity));
            expect(store.getActions()).toEqual(expectedActions);
        });
    });
});
