import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { normalize } from 'normalizr';
import { NativeModules } from 'react-native';

import * as api from '../../redux/api/tag';
import * as actions from '../../redux/actions/tag';
import * as schema from '../../redux/schema';
import mockData from '../../redux/api/__mocks__/data';
import { getTagsInLiveData } from '../../redux/reducers';

import {
    FETCH_TAG_REQUEST,
    FETCH_TAG_SUCCESS,
    FETCH_TAG_FAILURE,
    SYSTEM_TAG_REMOVED,
    SYSTEM_TAG_INITIALIZED,
    SET_ACTIVITY_USER_TAG
} from '../../redux/constants/actionTypes';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

jest.mock('../../redux/api/tag');
jest.mock('../../redux/reducers');
jest.mock('../../redux/actions/system');

jest.mock("react-native-background-timer", () => {});
jest.mock("rn-ios-user-agent", () => {});

describe('actions/tag suite', () => {
    let store;
    let system = {id:1};
    let org = {id:1};

    beforeEach(() => {
        store = mockStore({ tags: {}, cloudState: {isConnected:true} });
        global.storage = {
            save: jest.fn()
        };
        navigator = {
            setOnNavigatorEvent: jest.fn()
        };
    });

    describe('fetchTagsBySystem action', () => {
        it('dispatches FETCH_TAG_SUCCESS when fetchTagsBySystem is complete', () => {
            const expectedActions = [
                { type: FETCH_TAG_REQUEST, system:system.id },
                { type: FETCH_TAG_SUCCESS, system:system.id, response: normalize(mockData.tags, schema.tagListSchema)},
            ]
            return store.dispatch(actions.fetchTagsBySystem(system.id)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });

        it('dispatches FETCH_TAG_FAILURE when fetchTagsBySystem fails', () => {
            const expectedError = 'Something went wrong';
            const expectedActions = [
                { type: FETCH_TAG_REQUEST, system:system.id },
                { type: FETCH_TAG_FAILURE, system:system.id, errorMessage: expectedError }
            ];

            api.fetchTagsBySystem.mockImplementationOnce(() => Promise.reject(expectedError));

            return store.dispatch(actions.fetchTagsBySystem(system.id, false)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });
    });

    describe('fetchTagsByOrg action', () => {
        it('dispatches FETCH_TAG_SUCCESS when fetchTagsByOrg is complete', () => {
            const expectedActions = [
                { type: FETCH_TAG_REQUEST, orgId:org.id },
                { type: FETCH_TAG_SUCCESS, orgId:org.id, response: {"entities": {}, "result": []}}//normalize(mockData.tags, schema.tagListSchema)},
            ]
            return store.dispatch(actions.fetchTagsByOrg(org.id)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });

        it('dispatches FETCH_TAG_FAILURE when fetchTagsByOrg fails', () => {
            const expectedError = 'Something went wrong';
            const expectedActions = [
                { type: FETCH_TAG_REQUEST, orgId:org.id },
                {"orgId": 1, "response": {"entities": {}, "result": []}, "type": "FETCH_TAG_SUCCESS"}
                //{ type: FETCH_TAG_FAILURE, orgId:org.id, errorMessage: expectedError }
            ];

            api.fetchTagsByOrg.mockImplementationOnce(() => Promise.reject(expectedError));

            return store.dispatch(actions.fetchTagsByOrg(org.id, false)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });
    });

    describe('removeTagLiveData action', () => {
        it('dispatches SYSTEM_TAG_REMOVED when removeTagLiveData is complete', () => {
            const tagName = '123';
            const expectedActions = [
                { type: SYSTEM_TAG_REMOVED, tagName },
            ]
            store.dispatch(actions.removeTagLiveData(tagName));
            expect(store.getActions()).toEqual(expectedActions);
        });
    });

    describe('setupTag action', () => {
        let motionAlgo, CODAlgo;
        beforeEach(() => {
            global.client = { send: jest.fn(), sendMessage: jest.fn() };
            system = {state: 'connected'};
            motionAlgo = { initialize: jest.fn() };
            CODAlgo = { initialize: jest.fn() };
            NativeModules.XpsSprintStart = motionAlgo;
            NativeModules.XpsJump = motionAlgo;
            NativeModules.XpsCOD = motionAlgo;

            jest.clearAllMocks();
        });

        it('does nothing if no tag ', () => {
            store.dispatch(actions.setupTag(null, null));
            expect(store.getActions()).toEqual([]);
        });

        it('sets up the tag motion algo and sends control message', () => {
            const tag = {id: 1, name: '1'};
            const tags = {1: tag};
            const activity = {distance: 10.0, activity_type: 'sprint', measurement_type: 'meters', sprintDistance: 10.0};
            const activityUser = {tag};

            const expectedActions = [
                {
                    type: SET_ACTIVITY_USER_TAG,
                    activityUser,
                    tag
                },
                {
                    type: SYSTEM_TAG_INITIALIZED,
                    tagId: 1,
                    tagName: 1,
                    motionAlgo,
                    activity,
                    activityUser,
                    activity_type: 'sprint',
                    measurementType: 'meters',
                    orientationType: 0,
                    sprintDistance: 10.0
                }
            ];

            getTagsInLiveData.mockImplementationOnce(() => tags);
            global.client.send.mockImplementationOnce(() => Promise.resolve());

            store.dispatch(actions.setupTag(tag, activity, activityUser));

            expect(store.getActions()).toEqual(expectedActions);
            expect(global.client.sendMessage.mock.calls.length).toBe(1)
        });

        it('sets up the tag motion algo and sends control message while replacing tag', () => {
            const tag1 = {id: 1, name: '1'};
            const oldMotionAlgo = { initialize: jest.fn() };
            const tag2 = {id: 2, name: '2', motionAlgo: oldMotionAlgo};
            const tags = {1: tag1};
            const activity = {distance: 10.0, activity_type: 'sprint', measurement_type: 'yards', sprintDistance: 10.0};
            const activityUser = {tag: tag1};
            const expectedActions = [
                {
                    type: SYSTEM_TAG_REMOVED,
                    tagName: 1
                },
                {
                    activityUser,
                    tag:tag2,
                    type: SET_ACTIVITY_USER_TAG
                },
                {
                    activity,
                    activity_type: 'sprint',
                    measurementType: 'yards',
                    motionAlgo,
                    activityUser,
                    sprintDistance: 9.144,
                    tagId: 2,
                    tagName: 2,
                    orientationType: 0,
                    type: SYSTEM_TAG_INITIALIZED
                },
            ];


            getTagsInLiveData.mockImplementationOnce(() => tags);
            global.client.send.mockImplementationOnce(() => Promise.resolve());

            store.dispatch(actions.setupTag(tag2, activity, activityUser));

            expect(store.getActions()).toEqual(expectedActions);
            expect(global.client.sendMessage.mock.calls.length).toBe(1)
        });

        it('sets up the jump algo', () => {
            const tag = {id: 1, name: '1'};
            const tags = {1: tag};
            const activity = {distance: 10.0, activity_type: 'jump'};
            const activityUser = {tag};
            const expectedActions = [
                {
                    type: SET_ACTIVITY_USER_TAG,
                    activityUser,
                    tag
                },
                {
                    activity,
                    activityUser,
                    type: SYSTEM_TAG_INITIALIZED,
                    tagId: 1,
                    tagName: 1,
                    motionAlgo,
                    activity_type: 'jump',
                    measurementType: undefined,
                    orientationType: 1,
                    sprintDistance: 0
                },
            ];

            getTagsInLiveData.mockImplementationOnce(() => tags);
            global.client.send.mockImplementationOnce(() => Promise.resolve());

            store.dispatch(actions.setupTag(tag, activity, activityUser));
            expect(store.getActions()).toEqual(expectedActions);
        });

        it('sets up the agility algo', () => {
            const tag = {id: 1, name: '1'};
            const tags = {1: tag};
            const activity = {distance: 10.0, activity_type: 'agility'};
            const activityUser = {tag};
            const expectedActions = [
                {
                    type: SET_ACTIVITY_USER_TAG,
                    activityUser,
                    tag
                },
                {
                    type: SYSTEM_TAG_INITIALIZED,
                    tagId: 1,
                    tagName: 1,
                    motionAlgo,
                    CODAlgo,
                    activity,
                    activityUser,
                    activity_type: 'agility',
                    measurementType: undefined,
                    orientationType: 0,
                    sprintDistance: 9.144
                },
            ];

            getTagsInLiveData.mockImplementationOnce(() => tags);
            global.client.send.mockImplementationOnce(() => Promise.resolve());

            store.dispatch(actions.setupTag(tag, activity, activityUser));
            expect(JSON.stringify(store.getActions())).toEqual(JSON.stringify(expectedActions));
        });

        it('does nothing if activity_type is unknown ', () => {
            const tag = {id: 1, name: '1'};
            const activity = {distance: 10.0, activity_type: 'unknown'};
            store.dispatch(actions.setupTag(tag, activity, null));
            expect(store.getActions()).toEqual([]);
        });

    });


});

