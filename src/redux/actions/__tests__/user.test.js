import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { normalize } from 'normalizr';

import * as api from '../../redux/api/user';
import * as actions from '../../redux/actions/user';
import * as schema from '../../redux/schema';
import mockData from '../../redux/api/__mocks__/data';

import {
    FETCH_USER_REQUEST,
    FETCH_USER_SUCCESS,
    FETCH_USER_FAILURE,
    FETCH_USER_BY_TEAM_REQUEST,
    FETCH_USER_BY_TEAM_SUCCESS,
    FETCH_USER_BY_TEAM_FAILURE,
    SAVE_USER_REQUEST,
    SAVE_USER_SUCCESS,
    SAVE_USER_FAILURE,
    SAVE_USER_IMAGE_REQUEST,
    SAVE_USER_IMAGE_SUCCESS,
    SAVE_USER_IMAGE_FAILURE,
    SAVE_LOCAL_USER_IMAGE,
    SET_USER_IMAGE,
} from '../../redux/constants/actionTypes';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

jest.mock('../../redux/api/user');

describe('actions/user suite', () => {
    const userId = 1;
    const teamId = 1;
    let store;

    beforeEach(() => {
        store = mockStore({ user: {} });
    });

    describe('fetchUser action', () => {
        it('dispatches FETCH_USER_SUCCESS when fetchUser is complete', () => {
            const expectedActions = [
                { type: FETCH_USER_REQUEST, userId },
                { type: FETCH_USER_SUCCESS, userId, response: normalize(mockData.users[0], schema.userSchema)},
            ]
            return store.dispatch(actions.fetchUser(userId)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });

        it('dispatches FETCH_USER_FAILURE when fetchUser fails', () => {
            const expectedError = 'Something went wrong';
            const expectedActions = [
                { type: FETCH_USER_REQUEST, userId },
                { type: FETCH_USER_FAILURE, userId, errorMessage: expectedError }
            ];

            api.fetchUser.mockImplementationOnce(() => Promise.reject(expectedError));

            return store.dispatch(actions.fetchUser(userId)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });
    });

    describe('fetchUsersByTeam action', () => {
        it('dispatches FETCH_USER_SUCCESS when fetchUsersByTeam is complete', () => {
            const expectedActions = [
                { type: FETCH_USER_BY_TEAM_REQUEST, teamId },
                { type: FETCH_USER_BY_TEAM_SUCCESS, teamId, response: normalize(mockData.users, schema.userListSchema)},
            ]
            return store.dispatch(actions.fetchUsersByTeam(teamId)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });

        it('dispatches FETCH_USER_FAILURE when fetchUsersByTeam fails', () => {
            const expectedError = 'Something went wrong';
            const expectedActions = [
                { type: FETCH_USER_BY_TEAM_REQUEST, teamId },
                { type: FETCH_USER_BY_TEAM_FAILURE, teamId, errorMessage: expectedError }
            ];

            api.fetchUsersByTeam.mockImplementationOnce(() => Promise.reject(expectedError));

            return store.dispatch(actions.fetchUsersByTeam(teamId)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });
    });

    describe('saveUser action', () => {
        const userData = {id: 1};

        it('dispatches FETCH_USER_SUCCESS when saveUser is complete', () => {
            const expectedActions = [
                { type: SAVE_USER_REQUEST, userData },
                { type: SAVE_USER_SUCCESS, userData, response: normalize(mockData.users[0], schema.userSchema)},
                { type: FETCH_USER_REQUEST, userId },
                { type: FETCH_USER_SUCCESS, userId, response: normalize(mockData.users[0], schema.userSchema)},
            ]
            return store.dispatch(actions.saveUser(userData)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });

        it('dispatches FETCH_USER_SUCCESS and dismisses modal when saveUser is complete', () => {
            const navigator = {
                dismissModal: jest.fn()
            };
            const expectedActions = [
                { type: SAVE_USER_REQUEST, userData },
                { type: SAVE_USER_SUCCESS, userData, response: normalize(mockData.users[0], schema.userSchema)},
                { type: FETCH_USER_REQUEST, userId },
                { type: FETCH_USER_SUCCESS, userId, response: normalize(mockData.users[0], schema.userSchema)},
            ]
            return store.dispatch(actions.saveUser(userData, navigator)).then(() => {
                expect(store.getActions()).toEqual(expectedActions);
                expect(navigator.dismissModal.mock.calls.length).toBe(1);
            });
        });

        it('dispatches FETCH_USER_FAILURE when saveUser fails', () => {
            const expectedError = 'Something went wrong';
            const expectedActions = [
                { type: SAVE_USER_REQUEST, userData },
                { type: SAVE_USER_FAILURE, userData, errorMessage: expectedError }
            ];

            api.saveUser.mockImplementationOnce(() => Promise.reject(expectedError));

            return store.dispatch(actions.saveUser(userData)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });
    });

    describe('saveUserImage action', () => {
        const imageKey = '1';

        it('dispatches FETCH_USER_SUCCESS when saveUserImage is complete', () => {
            const expectedActions = [
                { type: SAVE_USER_IMAGE_REQUEST, userId },
                { type: SAVE_USER_IMAGE_SUCCESS, userId, response: normalize(mockData.users[0], schema.userSchema)},
                { type: FETCH_USER_REQUEST, userId },
                { type: FETCH_USER_SUCCESS, userId, response: normalize(mockData.users[0], schema.userSchema)},
            ]
            return store.dispatch(actions.saveUserImage(userId, imageKey)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });

        it('dispatches FETCH_USER_FAILURE when saveUserImage fails', () => {
            const expectedError = 'Something went wrong';
            const expectedActions = [
                { type: SAVE_USER_IMAGE_REQUEST, userId },
                { type: SAVE_USER_IMAGE_FAILURE, userId, errorMessage: expectedError }
            ];

            api.saveUserImage.mockImplementationOnce(() => Promise.reject(expectedError));

            return store.dispatch(actions.saveUserImage(userId, imageKey)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });
    });

    describe('saveLocalUserImage action', () => {
        const imageKey = '1';

        it('dispatches SAVE_LOCAL_USER_IMAGE when saveUserImage is complete', () => {
            const expectedActions = [
                { type: SAVE_LOCAL_USER_IMAGE, userId, imageKey },
            ]
            return store.dispatch(actions.saveLocalUserImage(userId, imageKey));
        });
    });

    describe('setUserImage action', () => {
        const imageKey = '1';

        it('dispatches SET_USER_IMAGE when saveUserImage is complete', () => {
            const expectedActions = [
                { type: SET_USER_IMAGE, userId, imageKey },
            ]
            return store.dispatch(actions.setUserImage(userId, imageKey));
        });
    });
});
