import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { normalize } from 'normalizr';

import * as api from '../../redux/api/activity_user';
import * as actions from '../../redux/actions/activity_user';
import * as schema from '../../redux/schema';
import mockData from '../../redux/api/__mocks__/data';

import {
    FETCH_ACTIVITY_USER_REQUEST,
    FETCH_ACTIVITY_USER_SUCCESS,
    FETCH_ACTIVITY_USER_FAILURE,
    SAVE_ACTIVITY_USER_REQUEST,
    SAVE_ACTIVITY_USER_SUCCESS,
    SAVE_ACTIVITY_USER_FAILURE,
    DELETE_ACTIVITY_USER_REQUEST,
    DELETE_ACTIVITY_USER_SUCCESS,
    DELETE_ACTIVITY_USER_FAILURE,
    SYSTEM_SPRINT_BEEP_START,
} from '../../redux/constants/actionTypes';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

jest.mock('../../redux/api/activity_user');

describe('actions/activity_user suite', () => {
    const activityId = 1;
    const activityUserId = 1;
    let store;

    beforeEach(() => {
        store = mockStore({ activity_users: {} });
    });

    describe('fetchActivityUsersByActivity action', () => {
        it('dispatches FETCH_ACTIVITY_USER_SUCCESS when fetchActivityUsersByActivity is complete', () => {
            const expectedActions = [
                { type: FETCH_ACTIVITY_USER_REQUEST, activityId },
                { type: FETCH_ACTIVITY_USER_SUCCESS, activityId, response: normalize(mockData.activity_users, schema.activityUserListSchema)},
            ]
            return store.dispatch(actions.fetchActivityUsersByActivity(activityId)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });

        it('dispatches FETCH_ACTIVITY_USER_FAILURE when fetchActivityUsersByActivity fails', () => {
            const expectedError = 'Something went wrong';
            const expectedActions = [
                { type: FETCH_ACTIVITY_USER_REQUEST, activityId },
                { type: FETCH_ACTIVITY_USER_FAILURE, activityId, errorMessage: expectedError }
            ];

            api.fetchActivityUsersByActivity.mockImplementationOnce(() => Promise.reject(expectedError));

            return store.dispatch(actions.fetchActivityUsersByActivity(activityId, false)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });
    });

    describe('saveActivityUser action', () => {
        let activityUserData, navig, modalProps;
        beforeEach(() => {
            activityUserData = {

            };
            navig = {
                dismissModal: jest.fn(),
                showModal: jest.fn()
            };
            modalProps = {something: 'value'};
        });

        it('dispatches SAVE_ACTIVITY_USER_SUCCESS when saveActivityUser is complete', () => {
            const expectedActions = [
                { type: SAVE_ACTIVITY_USER_REQUEST, activityUserData },
                { type: SAVE_ACTIVITY_USER_SUCCESS, activityUserData, response: normalize(mockData.activity_users[0], schema.activityUserSchema)},
            ]
            return store.dispatch(actions.saveActivityUser(activityUserData, navig, modalProps)).then(() => {
                expect(store.getActions()).toEqual(expectedActions);
                expect(navig.dismissModal.mock.calls.length).toBe(1);
            });
        });

        it('dispatches SAVE_ACTIVITY_USER_SUCCESS when saveActivityUser is complete without calling dismissModal', () => {
            const expectedActions = [
                { type: SAVE_ACTIVITY_USER_REQUEST, activityUserData },
                { type: SAVE_ACTIVITY_USER_SUCCESS, activityUserData, response: normalize(mockData.activity_users[0], schema.activityUserSchema)},
            ]
            return store.dispatch(actions.saveActivityUser(activityUserData, null, modalProps)).then(() => {
                expect(store.getActions()).toEqual(expectedActions);
                expect(navig.dismissModal.mock.calls.length).toBe(0);
            });
        });

        it('dispatches SAVE_ACTIVITY_USER_FAILURE when saveActivityUser fails', () => {
            const expectedError = 'Something went wrong';
            const expectedActions = [
                { type: SAVE_ACTIVITY_USER_REQUEST, activityUserData },
                { type: SAVE_ACTIVITY_USER_FAILURE, activityUserData, errorMessage: expectedError }
            ];

            api.saveActivityUser.mockImplementationOnce(() => Promise.reject(expectedError));

            return store.dispatch(actions.saveActivityUser(activityUserData, navig, modalProps)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });
    });

    describe('deleteActivityUser action', () => {
        let navig, modalProps;
        beforeEach(() => {
            navig = {
                dismissModal: jest.fn(),
                showModal: jest.fn()
            };
            modalProps = {something: 'value'};
        });

        it('dispatches DELETE_ACTIVITY_USER_SUCCESS when deleteActivityUser is complete', () => {
            const expectedActions = [
                { type: DELETE_ACTIVITY_USER_REQUEST, activityUserId },
                { type: DELETE_ACTIVITY_USER_SUCCESS, activityUserId },
            ]
            return store.dispatch(actions.deleteActivityUser(activityUserId, navig, modalProps)).then(() => {
                expect(store.getActions()).toEqual(expectedActions);
                expect(navig.dismissModal.mock.calls.length).toBe(1);
            });
        });

        it('dispatches DELETE_ACTIVITY_USER_SUCCESS when deleteActivityUser is complete without calling dismissModal', () => {
            const expectedActions = [
                { type: DELETE_ACTIVITY_USER_REQUEST, activityUserId },
                { type: DELETE_ACTIVITY_USER_SUCCESS, activityUserId },
            ]
            return store.dispatch(actions.deleteActivityUser(activityUserId, null, modalProps)).then(() => {
                expect(store.getActions()).toEqual(expectedActions);
                expect(navig.dismissModal.mock.calls.length).toBe(0);
            });
        });

        it('dispatches DELETE_ACTIVITY_USER_FAILURE when deleteActivityUser fails', () => {
            const expectedError = 'Something went wrong';
            const expectedActions = [
                { type: DELETE_ACTIVITY_USER_REQUEST, activityUserId },
                { type: DELETE_ACTIVITY_USER_FAILURE, activityUserId, errorMessage: expectedError }
            ];

            api.deleteActivityUser.mockImplementationOnce(() => Promise.reject(expectedError));

            return store.dispatch(actions.deleteActivityUser(activityUserId, navig, modalProps)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });
    });

    describe('setActivityBeepStartTime action', () => {
        beforeEach(() => {
            global.client = {
                siteUrl: 'http://localhost:8000/',
                send: jest.fn(),
                sendMessage: jest.fn(),
            };

        });
        const beepStartTime = 12345;
        it('dispatches SYSTEM_SPRINT_BEEP_START when setActivityBeepStartTime is complete', () => {
            const expectedActions = [
                { type: SYSTEM_SPRINT_BEEP_START, activityId, beepStartTime }
            ]
            store.dispatch(actions.setActivityBeepStartTime(activityId, beepStartTime));
            expect(store.getActions()).toEqual(expectedActions);
        });
    });

});
