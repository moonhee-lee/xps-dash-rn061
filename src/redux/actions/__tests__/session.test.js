import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { normalize } from 'normalizr';

import * as api from '../../redux/api/session';
import * as actions from '../../redux/actions/session';
import * as schema from '../../redux/schema';
import mockData from '../../redux/api/__mocks__/data';

import {
    FETCH_SESSION_REQUEST,
    FETCH_SESSION_SUCCESS,
    FETCH_SESSION_FAILURE,
    SAVE_SESSION_REQUEST,
    SAVE_SESSION_SUCCESS,
    SAVE_SESSION_FAILURE
} from '../../redux/constants/actionTypes';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

jest.mock('../../redux/api/session');

jest.mock('moment', () => () => ({format: () => '123456789'}));
jest.mock("react-native-background-timer", () => {});
jest.mock("rn-ios-user-agent", () => {});

describe('actions/session suite', () => {
    const orgId = 1;
    const teamId = 1;
    let store;

    beforeEach(() => {
        store = mockStore({ session: {} });
    });

    describe('fetchSessionsByOrg action', () => {
        it('dispatches FETCH_SESSION_SUCCESS when fetchSessionsByOrg is complete', () => {
            const expectedActions = [
                { type: FETCH_SESSION_REQUEST, orgId },
                { type: FETCH_SESSION_SUCCESS, orgId, response: normalize(mockData.sessions, schema.sessionListSchema)},
            ]
            return store.dispatch(actions.fetchSessionsByOrg(orgId)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });

        it('dispatches FETCH_SESSION_FAILURE when fetchSessionsByOrg fails', () => {
            const expectedError = 'Something went wrong';
            const expectedActions = [
                { type: FETCH_SESSION_REQUEST, orgId },
                { type: FETCH_SESSION_FAILURE, orgId, errorMessage: expectedError }
            ];

            api.fetchSessionsByOrg.mockImplementationOnce(() => Promise.reject(expectedError));

            return store.dispatch(actions.fetchSessionsByOrg(orgId, false)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });
    });

    describe('saveSession action', () => {
        let sessionData, navig, modalProps;
        beforeEach(() => {
            sessionData = {
                name: 'Test Session 2'
            };
            navig = {
                showModal: jest.fn(),
                dismissModal: jest.fn(),
            };
            modalProps = {something: 'value'};
            global.storage = {
                save: jest.fn()
            };
        });
        const setActiveSession = jest.fn();

        it('dispatches SAVE_SESSION_SUCCESS when saveSession is complete', () => {
            const expectedActions = [
                { type: SAVE_SESSION_REQUEST, sessionData },
                { type: SAVE_SESSION_SUCCESS, sessionData, response: normalize(mockData.sessions[0], schema.sessionSchema)},
            ]
            navig.dismissModal.mockImplementationOnce(() => Promise.resolve());

            return store.dispatch(actions.saveSession(sessionData, navig, modalProps, setActiveSession)).then(() => {
                expect(store.getActions()).toEqual(expectedActions);
                expect(navig.showModal.mock.calls.length).toBe(1);
            });
        });

        it('dispatches SAVE_SESSION_SUCCESS when saveSession is complete without calling showModal', () => {
            const expectedActions = [
                { type: SAVE_SESSION_REQUEST, sessionData },
                { type: SAVE_SESSION_SUCCESS, sessionData, response: normalize(mockData.sessions[0], schema.sessionSchema)},
            ]
            return store.dispatch(actions.saveSession(sessionData, null, modalProps, setActiveSession)).then(() => {
                expect(store.getActions()).toEqual(expectedActions);
                expect(navig.showModal.mock.calls.length).toBe(0);
            });
        });

        it('dispatches SAVE_SESSION_FAILURE when saveSession fails', () => {
            const expectedError = 'Something went wrong';
            const expectedActions = [
                { type: SAVE_SESSION_REQUEST, sessionData },
               // { type: SAVE_SESSION_FAILURE, sessionData, errorMessage: expectedError }
                //always passes now that we're writing to local storage...
                // TODO need to rebuild for save case from offline mode
                { type: SAVE_SESSION_SUCCESS, sessionData, response: normalize(mockData.sessions[0], schema.sessionSchema)},
            ];

            api.saveSession.mockImplementationOnce(() => Promise.reject(expectedError));
            const navig = false;
            return store.dispatch(actions.saveSession(sessionData, navig, modalProps, setActiveSession)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });
    });
});
