import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { NetworkInfo } from 'react-native-network-info';

import * as actions from '../../redux/actions/cloud';
import * as reducers from '../../redux/reducers';
import { getAxiosClient } from '/redux/api/client';

import {
    CLOUD_CONNECTION_REQUESTED,
    CLOUD_CONNECTION_SUCCESS,
    CLOUD_CONNECTION_FAILED,
    CLOUD_STOP_RETRIES,
} from '../../redux/constants/actionTypes';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('actions/cloud suite', () => {
    let store;

    beforeEach(() => {
        store = mockStore({ cloud: {} });
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    describe('fetchCurrentCloudState action', () => {
        beforeEach(() => {
            axiosClient = {
                get: jest.fn(),
                post: jest.fn(),
                put: jest.fn(),
            };
            jest.clearAllMocks();
            jest.useFakeTimers();
        });

        it('does not dispatch anything when fetchAttempts > 5', () => {
            const cloudReturn = {fetchAttempts: 5};
            reducers.getCloudState.mockImplementationOnce(() => cloudReturn);
            store.dispatch(actions.fetchCurrentCloudState());
            expect(store.getActions()).toEqual([]);
        });

        it('dispatches CLOUD_CONNECTION_SUCCESS when fetchCurrentCloudState is complete', () => {
            const expectedActions = [
                { type: CLOUD_CONNECTION_SUCCESS, IPAddress: 'loading', deviceSSID: 'test-ssid'},
            ]
            const cloudReturn = {fetchAttempts: 0};
            reducers.getCloudState.mockImplementationOnce(() => cloudReturn);

            return store.dispatch(actions.fetchCurrentCloudState()).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });

    });

    describe('handleOffline action', () => {
        it('dispatches CLOUD_CONNECTION_FAILED', () => {
            const expectedActions = [
                {type: CLOUD_CONNECTION_FAILED}
            ];
            store.dispatch(actions.handleOffline());
            expect(store.getActions()).toEqual(expectedActions);
        });
    });

});
