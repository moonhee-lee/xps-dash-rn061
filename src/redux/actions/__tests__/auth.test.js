import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { normalize } from 'normalizr';

import * as api from '../../redux/api/auth';
import * as actions from '../../redux/actions/auth';
import * as schema from '../../redux/schema';
import mockData from '../../redux/api/__mocks__/data';

import {
    LOGIN_REQUEST,
    LOGIN_SUCCESS,
    LOGIN_FAILURE,
    LOGOUT_REQUEST,
    LOGOUT_SUCCESS,
    LOGOUT_FAILURE,
    FETCH_ORG_MEMBER_BY_USER_ID_REQUEST
} from '../../redux/constants/actionTypes';

jest.mock('../../redux/api/auth');
jest.mock('../../react/screens');
jest.mock('react-native-camera', () => 'Camera')
jest.mock('react-native-aws3', () => 'FormDatas')
jest.mock("react-native-background-timer", () => {});
jest.mock("rn-ios-user-agent", () => {});

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);


describe('actions/auth suite', () => {
    let store;

    beforeEach(() => {
        store = mockStore({ auth: {}, cloud: {cloudState: {isConnected:true}}});
        global.storage = {
            save: jest.fn(),
            remove: jest.fn(),
        }
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    describe('login action', () => {
        it('dispatches LOGIN_SUCCESS when login auth is complete', () => {
            const username = 'testuser';
            const password = 'asdfasdf';
            const expectedActions = [
                { type: LOGIN_REQUEST, username },
                { type: LOGIN_SUCCESS, username, response: normalize(mockData.users[0], schema.userSchema), authToken: 'sometoken' },
                { type: FETCH_ORG_MEMBER_BY_USER_ID_REQUEST, userId: 1 }
            ]

            return store.dispatch(actions.login(username, password)).then(() => {
                expect(store.getActions()).toEqual(expectedActions);
                expect(global.storage.save.mock.calls.length).toBe(1);
            });
        });

        it('dispatches LOGIN_FAILURE when login auth fails', () => {
            const username = 'testuser';
            const password = 'asdfasdf';
            const expectedError = 'Something went wrong';
            const expectedActions = [
                { type: LOGIN_REQUEST, username },
                { type: LOGIN_FAILURE, username, errorMessage: expectedError }
            ];

            api.login.mockImplementationOnce(() => Promise.reject(expectedError));

            return store.dispatch(actions.login(username, password)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });
    });

    describe('logout action', () => {
        it('dispatches LOGOUT_SUCCESS when logout auth is complete', () => {
            const expectedActions = [
                { type: LOGOUT_REQUEST },
                { type: LOGOUT_SUCCESS },
            ]
            store.dispatch(actions.logout());
            expect(store.getActions()).toEqual(expectedActions)
        });
    });
});
