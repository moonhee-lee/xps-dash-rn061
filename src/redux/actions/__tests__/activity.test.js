import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { normalize } from 'normalizr';

import * as api from '../../redux/api/activity';
import * as actions from '../../redux/actions/activity';
import * as schema from '../../redux/schema';
import mockData from '../../redux/api/__mocks__/data';

import {
    FETCH_ACTIVITY_REQUEST,
    FETCH_ACTIVITY_SUCCESS,
    FETCH_ACTIVITY_FAILURE,
    FETCH_ACTIVITY_BY_USER_REQUEST,
    FETCH_ACTIVITY_BY_USER_SUCCESS,
    FETCH_ACTIVITY_BY_USER_FAILURE,
    SAVE_ACTIVITY_REQUEST,
    SAVE_ACTIVITY_SUCCESS,
    SAVE_ACTIVITY_FAILURE,
    ACTIVITY_TOGGLE_RECORDING_MODE,
    ACTIVITY_RESET_RECORDING_MODE,
} from '../../redux/constants/actionTypes';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

jest.mock('../../redux/api/activity');
jest.mock('../../redux/reducers/sessions');
jest.useFakeTimers();

jest.mock('moment', () => () => ({format: () => '123456789'}));
jest.mock("react-native-background-timer", () => {});
jest.mock("rn-ios-user-agent", () => {});

describe('actions/activity suite', () => {
    const sessionId = 1;
    const userId = 1;
    const activityId = 1;
    let store;

    beforeEach(() => {
        store = mockStore({ activities: {} })
        global.storage = {
            load: jest.fn(),
            save: jest.fn(),
            remove: jest.fn()
        }
    });

    describe('fetchActivitiesBySession action', () => {
        it('dispatches FETCH_ACTIVITY_SUCCESS when fetchActivitiesBySession is complete', () => {
            const expectedActions = [
                { type: FETCH_ACTIVITY_REQUEST, sessionId },
                { type: FETCH_ACTIVITY_SUCCESS, sessionId, response: normalize(mockData.activities, schema.activityListSchema)},
            ]
            return store.dispatch(actions.fetchActivitiesBySession(sessionId)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });

        it('dispatches FETCH_ACTIVITY_FAILURE when fetchActivitiesBySession fails', () => {
            const expectedError = 'Something went wrong';
            const expectedActions = [
                { type: FETCH_ACTIVITY_REQUEST, sessionId },
                { type: FETCH_ACTIVITY_SUCCESS, sessionId, response: {entities: {}, result:[]}},
            ];

            api.fetchActivitiesBySession.mockImplementationOnce(() => Promise.reject(expectedError));

            return store.dispatch(actions.fetchActivitiesBySession(sessionId, false)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });
    });

    describe('fetchActivitiesByUser action', () => {
        it('dispatches FETCH_ACTIVITY_SUCCESS when fetchActivitiesByUser is complete', () => {
            const expectedActions = [
                { type: FETCH_ACTIVITY_BY_USER_REQUEST, userId },
                { type: FETCH_ACTIVITY_BY_USER_SUCCESS, userId, response: normalize(mockData.activities, schema.activityListSchema)},
            ]
            return store.dispatch(actions.fetchActivitiesByUser(userId)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });

        it('dispatches FETCH_ACTIVITY_FAILURE when fetchActivitiesByUser fails', () => {
            const expectedError = 'Something went wrong';
            const expectedActions = [
                { type: FETCH_ACTIVITY_BY_USER_REQUEST, userId },
                { type: FETCH_ACTIVITY_BY_USER_FAILURE, userId, errorMessage: expectedError }
            ];

            api.fetchActivitiesByUser.mockImplementationOnce(() => Promise.reject(expectedError));

            return store.dispatch(actions.fetchActivitiesByUser(userId, false)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });
    });

    describe('fetchActivity action', () => {
        it('dispatches FETCH_ACTIVITY_SUCCESS when fetchActivity is complete', () => {
            const expectedActions = [
                { type: FETCH_ACTIVITY_REQUEST, activityId },
                { type: FETCH_ACTIVITY_SUCCESS, activityId, response: normalize(mockData.activities[0], schema.activitySchema)},
            ]
            return store.dispatch(actions.fetchActivity(activityId)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });

        it('dispatches FETCH_ACTIVITY_FAILURE when fetchActivity fails', () => {
            const expectedError = 'Something went wrong';
            const expectedActions = [
                { type: FETCH_ACTIVITY_REQUEST, activityId },
                { type: FETCH_ACTIVITY_FAILURE, activityId, errorMessage: expectedError }
            ];

            api.fetchActivity.mockImplementationOnce(() => Promise.reject(expectedError));

            return store.dispatch(actions.fetchActivity(activityId, false)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });
    });

    describe('saveActivity action', () => {
        let activityData, activeUsers;
        beforeEach(() => {
            activityData = {
                id:1, type_definition: {}
            };
            activeUsers = [
                {id:1,tag:{name:1},user:{id:1}},
                {id:2,tag:{name:1},user:{id:1}}
                ];
            global.liveESTMessages = {}
            global.liveRawMessages = {}
        });

        it('dispatches SAVE_ACTIVITY_SUCCESS when saveActivity is complete', () => {
            const expectedActions = [{ type: SAVE_ACTIVITY_REQUEST }]
            store.dispatch(actions.saveActivity(activityData, activeUsers, true))
            expect(store.getActions()).toEqual(expectedActions);
        });

        it('dispatches SAVE_ACTIVITY_SUCCESS when saveActivity is complete and showModal', () => {
            const expectedActions = [{ type: SAVE_ACTIVITY_REQUEST }]
            store.dispatch(actions.saveActivity(activityData, activeUsers, true))
            expect(store.getActions()).toEqual(expectedActions);
        });

        it('dispatches SAVE_ACTIVITY_SUCCESS when saveActivity is complete without calling dismissModal', () => {
            const expectedActions = [{ type: SAVE_ACTIVITY_REQUEST }]
            store.dispatch(actions.saveActivity(activityData, activeUsers, true))
            expect(store.getActions()).toEqual(expectedActions);
        });

        it('dispatches SAVE_ACTIVITY_FAILURE when saveActivity fails', () => {
            const expectedError = 'Something went wrong';
            const expectedActions = [{ type: SAVE_ACTIVITY_REQUEST }]

            api.saveActivity.mockImplementationOnce(() => Promise.reject(expectedError));

            store.dispatch(actions.saveActivity(activityData, activeUsers, true))
            expect(store.getActions()).toEqual(expectedActions);
        });
    });

    describe('toggleRecording action', () => {
        it('dispatches ACTIVITY_TOGGLE_RECORDING_MODE when toggleRecording called', () => {
            const tags = [];
            const isRecording = true;
            const expectedActions = [
                { type: ACTIVITY_TOGGLE_RECORDING_MODE, activityId, tags, isRecording }
            ]
            store.dispatch(actions.toggleRecording(activityId, tags, isRecording));
            expect(store.getActions()).toEqual(expectedActions);
        });
    });

    describe('resetRecording action', () => {
        it('dispatches ACTIVITY_RESET_RECORDING_MODE when resetRecording called', () => {
            const expectedActions =  [
                {activityId: 1, type: "ACTIVITY_RESET_RECORDING_MODE"},
                {activityId: 1, type: "ACTIVITY_RESET_LIVE_RECORDING_MODE"}
                ]
            store.dispatch(actions.resetRecording(activityId));
            expect(store.getActions()).toEqual(expectedActions);
        });
    });
});
