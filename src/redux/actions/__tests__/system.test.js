import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { normalize } from 'normalizr';

import * as actions from '../../redux/actions/system';
import * as schema from '../../redux/schema';
import * as mqtt from '../../redux/mqtt';
import { getTagLiveDataById, getCurrentSystem } from '../../redux/reducers';
import { addESTMessage, newMessageCount, getMsgsToProcess, addESTVelocity, completedMessageCount } from '../../redux/local_storage';
import { processEstMessages } from '../../lib/motion_utils';

import {
    SYSTEM_CONNECTED,
    SYSTEM_DISCONNECTED,
    SYSTEM_UPDATED,
    SYSTEM_CONNECTING,
    SYSTEM_DISCONNECTING,
    SYSTEM_SPRINT_MOTION_DISTANCE,
    SYSTEM_SPRINT_ENDED,
    SYSTEM_TAG_READY,
    SYSTEM_TAG_NOT_READY,
    SYSTEM_TAG_SELECT_CONFIRMED,
    SYSTEM_FETCH_SUCCESS,
} from '../../redux/constants/actionTypes';
import {
    SYSTEM_STATE_CONNECTING,
    SYSTEM_STATE_CONNECTED,
    SYSTEM_STATE_DISCONNECTING,
    SYSTEM_STATE_DISCONNECTED,
} from '../../redux/constants/systemStates';

import moment from "moment";

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

jest.mock('../../redux/mqtt');
jest.mock('../../redux/reducers');
jest.mock('../../redux/local_storage');
jest.mock('../../lib/motion_utils');
jest.useFakeTimers();
jest.mock("react-native-background-timer", () => {});
jest.mock("rn-ios-user-agent", () => {});

describe('actions/system suite', () => {
    let store;
    let system;
    let mockClient;

    beforeEach(() => {
        system = {
            id: 1,
            syncStarted:false,
            ip_address:'10.3.1.1',
            state: SYSTEM_STATE_CONNECTED
        };
        store = mockStore({ session: {}, systems: {1: {state: SYSTEM_STATE_CONNECTED}} });
        mockClient = {
            on: jest.fn(),
            connect: jest.fn(),
            subscribe: jest.fn()
        }
        mqtt.createClient = () => {
            return mockClient;
        }
        global.sentMsgs = [];
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    describe('processMessage method', () => {
        let message, dispatch;
        beforeEach(() => {
            dispatch = jest.fn();
            message = {
                destinationName: 'est',
                payloadString: '{"tagId":"1", "kfState":"2"}'
            };
            global.client = { messageQueue:[] };
        });
        afterEach(() => {
            jest.clearAllMocks();
        });

        it('does nothing on empty payload', () => {
            message = {payloadString: ''};
            actions.processMessage(message, '', '');
            expect(dispatch.mock.calls.length).toBe(0);
        });

        it('ignores EST message with no tagId', () => {
            message.payloadString = '{"tagId":""}';
            actions.processMessage(message, '', '');
            expect(dispatch.mock.calls.length).toBe(0);
        });

        it('ignores EST messages for tags not found', () => {
            getTagLiveDataById.mockImplementationOnce(() => null);
            actions.processMessage(message, '', '');
            expect(dispatch.mock.calls.length).toBe(0);
        });

        it('ignores EST messages for tag with no motion algo', () => {
            getTagLiveDataById.mockImplementationOnce(() => {motionAlgo: null});
            actions.processMessage(message, '', '');
            expect(dispatch.mock.calls.length).toBe(0);
        });

        it('ignores EST messages for tags in inactive status', () => {
            const tag = {motionAlgo: 1, status: 'inactive'};
            getTagLiveDataById.mockImplementationOnce(() => tag);
            actions.processMessage(message, '', '');
            expect(dispatch.mock.calls.length).toBe(0);
        });

        it('ignores EST messages for tags in select_pending status', () => {
            const tag = {motionAlgo: 1, status: 'select_pending'};
            getTagLiveDataById.mockImplementationOnce(() => tag);
            actions.processMessage(message, '', '');
            expect(dispatch.mock.calls.length).toBe(0);
        });

        it('ignores EST messages for tags in selected status when kfState not equal 2', () => {
            const tag = {motionAlgo: 1, status: 'selected'};
            message.payloadString = '{"tagId":"1", "kfState":"1"}'
            getTagLiveDataById.mockImplementationOnce(() => tag);
            actions.processMessage(message, '', '');
            expect(dispatch.mock.calls.length).toBe(0);
        });

        it('dispatches SYSTEM_TAG_READY when kfState = 2 and in selected status', () => {
            const tag = {motionAlgo: 1, status: 'selected'};
            const expectedAction = {type: SYSTEM_TAG_READY, tagName: 1,time:moment(new Date()).format('X')};
            getTagLiveDataById.mockImplementationOnce(() => tag);
            actions.processMessage(message, dispatch, '');
            expect(dispatch.mock.calls[0][0]).toEqual(expectedAction);
        });

        it('ignores EST messages for tags in ready status when kfState 2', () => {
            const tag = {motionAlgo: 1, status: 'ready'};
            getTagLiveDataById.mockImplementationOnce(() => tag);
            actions.processMessage(message, '', '');
            expect(dispatch.mock.calls.length).toBe(0);
        });

        it('dispatches SYSTEM_TAG_NOT_READY when kfState != 2 and in ready status', () => {
            const tag = {motionAlgo: 1, status: 'ready'};
            const expectedAction = {type: SYSTEM_TAG_NOT_READY, tagName: 1,time:moment(new Date()).format('X')};

            message.payloadString = '{"tagId":"1", "kfState":"1"}'
            getTagLiveDataById.mockImplementationOnce(() => tag);
            actions.processMessage(message, dispatch, '');
            expect(dispatch.mock.calls[0][0]).toEqual(expectedAction);
        });

        it('calls addESTMessage when in recording status', () => {
            const tag = {motionAlgo: 1, status: 'recording'};
            getTagLiveDataById.mockImplementationOnce(() => tag);
            addESTMessage.mockImplementationOnce(() => null);
            newMessageCount.mockImplementationOnce(() => 1);

            actions.processMessage(message, dispatch, '');

            expect(dispatch.mock.calls.length).toBe(0);
            expect(addESTMessage.mock.calls.length).toBe(1);
            expect(getMsgsToProcess.mock.calls.length).toBe(0);
        });

        it('calls addESTMessage when in recording status', () => {
            const tag = {motionAlgo: 1, status: 'recording'};
            getTagLiveDataById.mockImplementationOnce(() => tag);
            addESTMessage.mockImplementationOnce(() => null);
            newMessageCount.mockImplementationOnce(() => 10);
            getMsgsToProcess.mockImplementationOnce(() => {stuff: 'yeah'});

            jest.spyOn(global, 'requestAnimationFrame').mockImplementation(cb => cb());

            actions.processMessage(message, dispatch, '');

            expect(dispatch.mock.calls.length).toBe(0);
            expect(addESTMessage.mock.calls.length).toBe(1);
            expect(getMsgsToProcess.mock.calls.length).toBe(0);
            expect(processEstMessages.mock.calls.length).toBe(0)
        });

        it('calls addESTVelocity when in recording status', () => {
            message.payloadString = '{"tagId":"1", "kfState":"2","velMPS":{"y":5}}';
            const tag = {motionAlgo: 1, status: 'recording'};
            getTagLiveDataById.mockImplementationOnce(() => tag);
            addESTVelocity.mockImplementationOnce(() => 10);
            completedMessageCount.mockImplementationOnce(() => 3);

            jest.spyOn(global, 'requestAnimationFrame').mockImplementation(cb => cb());

            actions.processMessage(message, dispatch, '');

            expect(dispatch.mock.calls.length).toBe(0);
            expect(addESTMessage.mock.calls.length).toBe(1);
            expect(addESTVelocity.mock.calls.length).toBe(1);
            expect(getMsgsToProcess.mock.calls.length).toBe(0);
            expect(processEstMessages.mock.calls.length).toBe(0)
        });

        it('calls addESTVelocity when in recording status and no velocity', () => {
            message.payloadString = '{"tagId":"1", "kfState":"2","velMPS":null}';
            const tag = {motionAlgo: 1, status: 'recording'};
            getTagLiveDataById.mockImplementationOnce(() => tag);
            addESTVelocity.mockImplementationOnce(() => 0);
            completedMessageCount.mockImplementationOnce(() => 3);

            jest.spyOn(global, 'requestAnimationFrame').mockImplementation(cb => cb());

            actions.processMessage(message, dispatch, '');

            expect(dispatch.mock.calls.length).toBe(0);
            expect(addESTMessage.mock.calls.length).toBe(1);
            expect(addESTVelocity.mock.calls.length).toBe(1);
            expect(getMsgsToProcess.mock.calls.length).toBe(0);
            expect(processEstMessages.mock.calls.length).toBe(0)
        });

        it('ignores EST messages for tags in unknown status', () => {
            message.velMPS = {y:1};
            const tag = {motionAlgo: 1, status: 'unknown'};
            getTagLiveDataById.mockImplementationOnce(() => tag);
            actions.processMessage(message, '', '');
            expect(dispatch.mock.calls.length).toBe(0);
        });

        it('ignores control_response cmd_id not 101', () => {
            message.destinationName = 'control_response';
            message.payloadString = '{"cmd_id": "1"}';
            actions.processMessage(message, dispatch, '');
            expect(dispatch.mock.calls.length).toBe(0);
        });

        it('dispatches SYSTEM_TAG_SELECT_CONFIRMED on control_response 101', () => {
            message.destinationName = 'control_response';
            message.payloadString = '{"cmd_id": "101","msg_id": "101"}';
            const expectedAction = {type: SYSTEM_TAG_SELECT_CONFIRMED};

            actions.processMessage(message, dispatch, '');
            expect(dispatch.mock.calls.length).toBe(0);
        });

        it('ignores unknown destinationName', () => {
            message.destinationName = 'unknown';
            actions.processMessage(message, dispatch, '');
            expect(dispatch.mock.calls.length).toBe(0);
        });

    });

    describe('connectSystem action', () => {
        beforeEach(() => {
            global.client = {
                on: jest.fn(),
                connect: jest.fn(),
                subscribe: jest.fn(),
                _client: {connected:false}
            };
        });
        it('calls client connect', () => {
            const expectedActions = [
                {systemId: system.id, type: SYSTEM_CONNECTING},
                {sync_timing: {
                        initialOffset: false,
                        isLocal: true,
                        syncStarted: false,
                        timingValid: true
                    }, type: SYSTEM_UPDATED},
                {systemId: system.id, type: SYSTEM_CONNECTED}
            ]

            global.client.connect.mockImplementationOnce(() => Promise.resolve());

            store.dispatch(actions.connectSystem(system,true)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });

        });
    });

    describe('disconnectSystem action', () => {
        beforeEach(() => {
            global.client = {
                disconnect: jest.fn(),
                _client: {connected:false},
                sendMessage: jest.fn(),
            };
        });

        it('ignores call with no system', () => {
            store.dispatch(actions.disconnectSystem(null));
            expect(store.getActions()).toEqual([]);
        });

        it('ignores call with no client', () => {
            global.client = null;
            const expected = [{"lastState": null, "systemId": 1, "type": "SYSTEM_DISCONNECTING"}];
            store.dispatch(actions.disconnectSystem(system));
            expect(store.getActions()).toEqual(expected);
        });

        it('calls client disconnect', () => {
            const expectedActions = [
                { type: SYSTEM_DISCONNECTING, systemId:system.id, lastState: null}
            ]

            global.client.disconnect.mockImplementationOnce(() => Promise.resolve());

            store.dispatch(actions.disconnectSystem(system));
            expect(store.getActions()).toEqual(expectedActions);
        });
    });

    describe('saveSystemInfo action', () => {
        it('dispatches SYSTEM_FETCH_SUCCESS when saveSystemInfo is complete', () => {
            const expectedActions = [
                { type: SYSTEM_FETCH_SUCCESS, system }
            ]
            store.dispatch(actions.saveSystemInfo(system));
            expect(store.getActions()).toEqual(expectedActions);
        });
    });

    describe('getTimingData action', () => {
        beforeEach(() => {
            global.timedStart = {
                getTimingData: jest.fn(),
            };
        });
        it('calls the timing native module after sync started, loads timing data', () => {
            system = {syncStarted:true,isLocal:true};
            global.timedStart.getTimingData.mockImplementationOnce(() => Promise.resolve());
            actions.getTimingData(system);
            expect(global.timedStart.getTimingData.mock.calls.length).toBe(1);
        });
        it('calls the timing native module without sync started, loads nothing', () => {
            system = {syncStarted:false,isLocal:true};
            global.timedStart.getTimingData.mockImplementationOnce(() => Promise.resolve());
            actions.getTimingData(system);
            expect(global.timedStart.getTimingData.mock.calls.length).toBe(0);
        });
    });

    describe('startSync action', () => {
        it('sets up the timed start native module, starts the time syncing', () => {
            store.dispatch(actions.startSync(system));
            expect(store.getActions()).toEqual([]);
        });
    });

    describe('stopSync action', () => {
        it('stops the time syncing', () => {
            store.dispatch(actions.stopSync(system));
            expect(store.getActions()).toEqual([]);
        });
    });

    describe('clearTags action', () => {
        beforeEach(() => {
            global.client = {
                send: jest.fn(),
                _client: {connected:false},
                sendMessage: jest.fn(),
                disconnect: jest.fn()
            };
        });
        it('sends a zeroed out control message to deselect all active tags', () => {
            global.client.sendMessage.mockImplementationOnce(() => Promise.resolve());
            global.client.disconnect.mockImplementationOnce(() => Promise.resolve());
            store.dispatch(actions.clearTags(false));
            expect(global.client.sendMessage.mock.calls.length).toBe(1);
            expect(global.client.disconnect.mock.calls.length).toBe(0);
        });
        it('sends a zeroed out control message to deselect all active tags and disconnects', () => {
            global.client.sendMessage.mockImplementationOnce(() => Promise.resolve());
            global.client.disconnect.mockImplementationOnce(() => Promise.resolve());
            store.dispatch(actions.clearTags(true));
            expect(global.client.sendMessage.mock.calls.length).toBe(1);
            expect(global.client.disconnect.mock.calls.length).toBe(0);
        });
    });

    describe('handleAppStateChange action', () => {
        beforeEach(() => {
            global.client = {
                send: jest.fn(),
                _client: {connected:false},
                sendMessage: jest.fn(),
                connect: jest.fn(),
                disconnect: jest.fn()
            };
        });
        it('handles app state change from active to inactive', () => {
            global.appState = 'active';
            const expected =  [{"lastState": "connected", "systemId": 1, "type": "SYSTEM_DISCONNECTING"}, {"type": "WIFI_DISCONNECT_SUCCESS"}, {"state": "inactive", "type": "APP_STATE_CHANGE"}];
            const systemState = {byIds:{1:{id:1,ip_address:'10.3.1.1',state:SYSTEM_STATE_CONNECTED}}};
            global.client.disconnect.mockImplementationOnce(() => Promise.resolve());
            getCurrentSystem.mockImplementationOnce(() => systemState.byIds[1]);
            actions.handleAppStateChange('inactive',store.dispatch,systemState);
            expect(store.getActions()).toEqual(expected);
        });
    });

});
