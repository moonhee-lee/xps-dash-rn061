import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { normalize } from 'normalizr';

import * as api from '../../redux/api/activity_summary';
import * as actions from '../../redux/actions/activity_summary';
import * as schema from '../../redux/schema';
import mockData from '../../redux/api/__mocks__/data';

import {
    FETCH_ACTIVITY_SUMMARY_REQUEST,
    FETCH_ACTIVITY_SUMMARY_SUCCESS,
    FETCH_ACTIVITY_SUMMARY_FAILURE
} from '../../redux/constants/actionTypes';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

jest.mock('../../redux/api/activity_summary');

describe('actions/activity_summary suite', () => {
    const activityId = 1;
    let store;

    beforeEach(() => {
        store = mockStore({ activity_summaries: {} });
    });

    describe('fetchActivitySummariesByActivity action', () => {
        it('dispatches FETCH_ACTIVITY_SUMMARY_SUCCESS when fetchActivitySummariesByActivity is complete', () => {
            const expectedActions = [
                { type: FETCH_ACTIVITY_SUMMARY_REQUEST, activityId },
                { type: FETCH_ACTIVITY_SUMMARY_SUCCESS, activityId, response: normalize(mockData.activity_summaries, schema.activitySummaryListSchema)},
            ]
            return store.dispatch(actions.fetchActivitySummariesByActivity(activityId)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });

        it('dispatches FETCH_ACTIVITY_SUMMARY_FAILURE when fetchActivitySummariesByActivity fails', () => {
            const expectedError = 'Something went wrong';
            const expectedActions = [
                { type: FETCH_ACTIVITY_SUMMARY_REQUEST, activityId },
                { type: FETCH_ACTIVITY_SUMMARY_FAILURE, activityId, errorMessage: expectedError }
            ];

            api.fetchActivitySummariesByActivity.mockImplementationOnce(() => Promise.reject(expectedError));

            return store.dispatch(actions.fetchActivitySummariesByActivity(activityId, false)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });
    });
});
