import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { normalize } from 'normalizr';

import * as api from '../../redux/api/team';
import * as actions from '../../redux/actions/team';
import * as schema from '../../redux/schema';
import mockData from '../../redux/api/__mocks__/data';

import {
    FETCH_TEAM_REQUEST,
    FETCH_TEAM_SUCCESS,
    FETCH_TEAM_FAILURE
} from '../../redux/constants/actionTypes';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

jest.mock('../../redux/api/team');
jest.mock("react-native-background-timer", () => {});
jest.mock("rn-ios-user-agent", () => {});

describe('actions/team suite', () => {
    const orgId = 1;
    let store;

    beforeEach(() => {
        store = mockStore({ teams: {} });
    });

    describe('fetchTeamsByOrg action', () => {
        it('dispatches FETCH_TEAM_SUCCESS when fetchTeamsByOrg is complete', () => {
            const expectedActions = [
                { type: FETCH_TEAM_REQUEST, orgId },
                { type: FETCH_TEAM_SUCCESS, orgId, response: normalize(mockData.teams, schema.teamListSchema)},
            ]
            return store.dispatch(actions.fetchTeamsByOrg(orgId)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });

        it('dispatches FETCH_TEAM_FAILURE when fetchTeamsByOrg fails', () => {
            const expectedError = 'Something went wrong';
            const expectedActions = [
                { type: FETCH_TEAM_REQUEST, orgId },
                { type: FETCH_TEAM_FAILURE, orgId, errorMessage: expectedError }
            ];

            api.fetchTeamsByOrg.mockImplementationOnce(() => Promise.reject(expectedError));

            return store.dispatch(actions.fetchTeamsByOrg(orgId)).then(() => {
                expect(store.getActions()).toEqual(expectedActions)
            });
        });
    });
});
