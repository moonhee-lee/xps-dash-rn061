import { normalize } from 'normalizr';
import * as schema from '../../redux//schema';
import * as userApi from '../../redux//api/user';
import {
    FETCH_USER_REQUEST,
    FETCH_USER_SUCCESS,
    FETCH_USER_FAILURE,
    FETCH_USER_BY_TEAM_REQUEST,
    FETCH_USER_BY_TEAM_SUCCESS,
    FETCH_USER_BY_TEAM_FAILURE,
    SAVE_USER_REQUEST,
    SAVE_USER_SUCCESS,
    SAVE_USER_FAILURE,
    SAVE_USER_IMAGE_REQUEST,
    SAVE_USER_IMAGE_SUCCESS,
    SAVE_USER_IMAGE_FAILURE,
    SAVE_LOCAL_USER_IMAGE,
    SET_USER_IMAGE,
} from '../constants/actionTypes';


export const fetchUser = (userId) => (dispatch, getState) => {
    // dispatch the FETCH_USER_REQUEST action.
    dispatch({
        type: FETCH_USER_REQUEST,
        userId
    });

    // call the API method to login the user.
    return userApi.fetchUser(userId).then(
        response => {
            // on success, dispatch the FETCH_USER_SUCCESS action
            dispatch({
                type: FETCH_USER_SUCCESS,
                userId,
                response: normalize(response.data, schema.userSchema)
            });
        },
        error => {
            // on error, dispatch the FETCH_USER_FAILURE action with userId and error message.
            dispatch({
                type: FETCH_USER_FAILURE,
                userId,
                errorMessage: error,
            });
        }
    );
};


export const fetchUsersByTeam = (teamId) => (dispatch, getState) => {
    // dispatch the FETCH_USER_REQUEST action.
    dispatch({
        type: FETCH_USER_BY_TEAM_REQUEST,
        teamId
    });

    // call the API method to login the user.
    return userApi.fetchUsersByTeam(teamId).then(
        response => {
            // on success, dispatch the FETCH_USER_BY_TEAM_SUCCESS action
            dispatch({
                type: FETCH_USER_BY_TEAM_SUCCESS,
                teamId,
                response: normalize(response.data.results, schema.userListSchema)
            });
        },
        error => {
            // on error, dispatch the FETCH_USER_BY_TEAM_FAILURE action with teamId and error message.
            dispatch({
                type: FETCH_USER_BY_TEAM_FAILURE,
                teamId,
                errorMessage: error,
            });
        }
    );
}

export const saveUser = (userData, navigator) => (dispatch, getState) => {
    // dispatch the SAVE_USER_REQUEST action.
    dispatch({
        type: SAVE_USER_REQUEST,
        userData,
    });

    // call the API method to create(save) this new user.
    return userApi.saveUser(userData).then(
        response => {
            dispatch({
                type: SAVE_USER_SUCCESS,
                userData,
                response: normalize(response.data, schema.userSchema)
            });
            if (navigator) {
                navigator.dismissModal();
            }
            dispatch(fetchUser(userData.id));
        },
        error => {
            // on error, dispatch the SAVE_USER_FAILURE action with sessionSlug and error message.
            dispatch({
                type: SAVE_USER_FAILURE,
                userData,
                errorMessage: 'Something went wrong',
            });
        }
    )
}

export const saveUserImage = (userId, imageKey, teamId) => (dispatch, getState) => {
    // dispatch the SAVE_USER_IMAGE_REQUEST action.
    dispatch({
        type: SAVE_USER_IMAGE_REQUEST,
        userId,
    });

    // call the API method to create(save) this new user image.
    return userApi.saveUserImage(userId,imageKey).then(
        response => {
            dispatch({
                type: SAVE_USER_IMAGE_SUCCESS,
                userId,
                response: normalize(response.data, schema.userSchema)
            });
            dispatch(fetchUser(userId));
        },
        error => {
            // on error, dispatch the SAVE_USER_FAILURE action with sessionSlug and error message.
            dispatch({
                type: SAVE_USER_IMAGE_FAILURE,
                userId,
                errorMessage: 'Something went wrong',
            });
        }
    )
}

export const saveLocalUserImage = (userId, imageKey) => (dispatch, getState) => {
    dispatch({
        type: SAVE_LOCAL_USER_IMAGE,
        userId,
        imageKey
    });
    //dispatch(fetchUser(userId));
}

export const setUserImage = (userId, imageKey) => (dispatch, getState) => {
    dispatch({
        type: SET_USER_IMAGE,
        userId,
        imageKey
    });
    //dispatch(fetchUser(userId));
}
