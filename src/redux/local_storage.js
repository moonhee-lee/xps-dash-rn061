import Storage from 'react-native-storage';
import _ from 'lodash';
import { AsyncStorage } from 'react-native';


export const setupLocalStorage = () => {
    var storage = new Storage({
        // maximum capacity, default 1000
        size: 10000,

        // Use AsyncStorage for RN, or window.localStorage for web.
        // If not set, data would be lost after reload.
        storageBackend: AsyncStorage,

        // expire time, default 1 day(1000 * 3600 * 24 milliseconds).
        // can be null, which means never expire.
        defaultExpires: null,

        // cache data in the memory. default is true.
        enableCache: true,

        // if data was not found in storage or expired,
        // the corresponding sync method will be invoked and return
        // the latest data.
        sync : {
            // we'll talk about the details later.
        }
    })

    global.storage = storage;

    // we're also going to setup a local buffer here to store EST messages
    // this may get stored to local storage later.. but for now we keep it in memory

    global.systemInterval = null;
    global.wsInterval = null;
    global.modeSelectMsgId = null;
    global.modeSelectMsg = null;
    global.sentMsgs = {};
    global.sentMsgCounters = {};
    global.modeSelectIntervals = [];
    global.liveESTMessages = {};
    global.liveTagPositions = {};
    global.liveRawMessages = {};
    global.controlRawMessages = {
        control: [],
        control_response: [],
    };
    global.eventRawMessages = {
        io_event: [],
    };
}


// everything below is, i guess, "cheating" and not doing it the redux way...
// but the redux way in this case is too slow.


export const resetMessages = (tagName) => {
    console.log('clearing message buffers');
    if (tagName in global.liveESTMessages) {
        delete global.liveESTMessages[tagName];
    }
    if (tagName in global.liveRawMessages) {
        delete global.liveRawMessages[tagName];
    }
    if(tagName in global.liveTagPositions) {
        delete global.liveTagPositions[tagName];
    }
    global.controlRawMessages = {
        control: [],
        control_response: [],
    };
    global.eventRawMessages = {
        io_event: [],
    };
}

export const addTagPosition = (tagName, message, posName=null) => {
    let tagPositions = {
        named: {}
    };
    if (tagName in global.liveTagPositions) {
        tagPositions = global.liveTagPositions[tagName];
    } else {
        global.liveTagPositions[tagName] = tagPositions;
    }

    tagPositions.posM = message.posM;

    if (posName !== null) {
        let positions = [];
        if (posName in tagPositions.named) {
            positions = tagPositions.named[posName]
        } else {
            tagPositions.named[posName] = positions;
        }
        positions.push(message.posM);
        if (positions.length > 50) {
            positions.shift();
        }
    }
}

export const addESTMessage = (tagName, message, measuring=false) => {
    let tagESTMessages = {
        newMsgs: [],
        processing: {},
        completed: [],
    };
    if (tagName in global.liveESTMessages) {
        tagESTMessages = global.liveESTMessages[tagName];
    } else {
        global.liveESTMessages[tagName] = tagESTMessages;
    }

    if(measuring) {
        tagESTMessages.posM = message.posM;
    }
    else {
        tagESTMessages.newMsgs.push(message);
        tagESTMessages.completed.push(message);
        //tagESTMessages.buffer[message.time] = message;
    }
}

export const addRawMessage = (tagName, message, topic) => {
    let tagRawMessages = {
        est: [],
        tag_raw: [],
        io_event: [],
    };
    if (tagName in global.liveRawMessages) {
        tagRawMessages = global.liveRawMessages[tagName];
    } else {
        global.liveRawMessages[tagName] = tagRawMessages;
    }

    switch(topic) {
        case 'est':
            tagRawMessages.est.push(message);
            break
        case 'tag_raw':
            tagRawMessages.tag_raw.push(message);
            break
        case 'io_event':
            tagRawMessages.io_event.push(message);
            break
    }
}


export const addControlMessage = (message, topic) => {
    switch(topic) {
        case 'control':
            global.controlRawMessages.control.push(message);
            break
        case 'control_response':
            global.controlRawMessages.control_response.push(message);
            break
    }
}

export const addEventMessage = (message, topic) => {
    switch(topic) {
        case 'io_event':
            global.eventRawMessages.io_event.push(message);
            break
    }
}


export const newMessageCount = (tagName) => {
    if (tagName in global.liveESTMessages) {
        return global.liveESTMessages[tagName].newMsgs.length;
    }
    return 0;
}

export const completedMessageCount = (tagName) => {
    if (tagName in global.liveESTMessages) {
        return global.liveESTMessages[tagName].completed.length;
    }
    return 0;
}


export const getMsgsToProcess = (tagName) => {
    if (tagName in global.liveESTMessages) {
        const tagESTMessages = global.liveESTMessages[tagName];
        const toProcess = tagESTMessages.newMsgs;
        tagESTMessages.newMsgs = [];
        return toProcess;
    }
}


export const moveProcessedToCompleted = (tagName) => {
    if (tagName in global.liveESTMessages) {
        const tagESTMessages = global.liveESTMessages[tagName];
        _.each(tagESTMessages.processing, (msg, time) => {
            // move the messages from newMsgs to processing
            tagESTMessages.completed[time] = msg;
            delete tagESTMessages.processing[time];
        })
    }
}

export const getTimeRangedMessages = (activityUser, includeBuffer) => {
    //console.log('getting getTimeRangedMessages')
    const tagName = parseInt(activityUser.tag.name, 10);

    if(!activityUser.meta_data) return;

    if (tagName in global.liveESTMessages) {
        const tagESTMessages = global.liveESTMessages[tagName];
        const startTime = includeBuffer ? ((activityUser.meta_data.start) - 1000) : (activityUser.meta_data.start);
        const endTime = includeBuffer ? ((activityUser.meta_data.end) + 1000) : (activityUser.meta_data.end);

        return _.filter(tagESTMessages.completed, (msg) => (msg.time > startTime && msg.time < endTime) );
    }
    return [];
}

export const getTimeRangedBuffer = (activityUser) => {
    const tagName = parseInt(activityUser.tag.name, 10);

    if (tagName in global.liveESTMessages) {
        const tagESTMessages = global.liveESTMessages[tagName];
        return _.map(tagESTMessages.completed, (msg) => msg);
    }
    return [];
}

export const hasActivityDataInBuffer = (activeUsers) => {
    let hasData = false;
    _.each(activeUsers,(activeUser) => {
        const tagName = parseInt(activeUser.tag.name, 10);

        if (tagName in global.liveESTMessages) {
            const tagESTMessages = global.liveESTMessages[tagName];
            if(tagESTMessages.completed.length>0) hasData = true;
        }
    });
    return hasData;
}

export const hasActivityRawDataInBuffer = (activeUsers) => {
    let hasData = false;
    _.each(activeUsers,(activeUser) => {
        const tagName = parseInt(activeUser.tag.name, 10);

        if (tagName in global.liveRawMessages) {
            const tagESTMessages = global.liveRawMessages[tagName];
            if(tagESTMessages.est.length>0) hasData = true;
            else if(tagESTMessages.est_is.length>0) hasData = true;
            else if(tagESTMessages.tag_meas.length>0) hasData = true;
            else if(tagESTMessages.tag_raw.length>0) hasData = true;
            else if(tagESTMessages.system_config.length>0) hasData = true;
            else if(tagESTMessages.est.length>0) hasData = true;
        }
    });
    return hasData;
}

export const getRawMessages = (activityUser) => {
    //console.log('getting liveRawMessages')
    const tagName = parseInt(activityUser.tag.name, 10);

    if (tagName in global.liveRawMessages) {
        const tagRawMessages = global.liveRawMessages[tagName];
        return _.merge(tagRawMessages,global.controlRawMessages, global.eventRawMessages);
    }
    return [];
}

export const addESTVelocity = (tagName,velocity) => {
    const tagESTMessages = global.liveESTMessages[tagName];
    if(tagESTMessages) tagESTMessages.velocity = velocity;
}

export const getESTVelocity = (tagName) => {
    const tagESTMessages = global.liveESTMessages[tagName];
    if(tagESTMessages) return tagESTMessages.velocity;
    return 0;
}

export const getESTCount = (tagName) => {
    const tagESTMessages = global.liveESTMessages[tagName];
    if(tagESTMessages) return tagESTMessages.completed.length;
    return 0;
}

export const getTagPosition = (tagName, posName=null) => {
    const tagPositions = global.liveTagPositions[tagName];
    if (tagPositions) {
        if (posName === null) {
            return tagPositions.posM;
        } else {
            return tagPositions.named[posName];
        }
    }
    return null;
}

export const getESTPosition = (tagName) => {
    const tagESTMessages = global.liveESTMessages[tagName];
    if(tagESTMessages) return tagESTMessages.posM;
    return null;
}
