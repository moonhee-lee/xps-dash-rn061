import _ from 'lodash';
import { createSelector } from 'reselect';
import { getTeam, getCurrentTeam } from '../../redux/reducers';

export const userSelector = state => (state && state.users)?state.users.byIds:[];
export const orgMembersSelector = state => (state && state.org_members)?state.org_members.byIds:[];


export const getAthleteUsers = (users, org_members) => {
    //changed to pull users from org member list instead of user list
    const userList = [];
    _.each(org_members,(o) => {
        const user = _.find(users, (u) => u.id === o.user);
        if (user) userList.push(user);
    });

    return userList.sort((userA, userB) => {
        if(userA.first_name < userB.first_name) return -1;
        if(userA.first_name > userB.first_name) return 1;
        return 0;
    });
}

export const teamAthletesSelector = createSelector(
    userSelector,
    orgMembersSelector,
    getAthleteUsers
);
