import _ from 'lodash';
import { createSelector } from 'reselect';
import { getActivityById, getUserById, getActivityRecords, getActivityRecordsByType, getActivityRecordsBySessionIdAndType, getActivityRecordsBySessionIdAndActivity } from '../../redux/reducers';
import { getUnit } from '../../lib/format_utils';

export const userSelector = (state) => (state && state.users)?state.users.byIds:[];

export const getActivityRecordsSelector = (state, props) => {
    const activities = getActivityRecordsBySessionIdAndActivity(state, props.session.id, props.activity);
    return activities;//_.filter(activities,(activity) => activity.type_definition.activity_type!=='freeform');
};

export const getActivityRecordsWithActivityAndUsers = (act_users, activity_records) => {
    let activity_type;

    if(!activity_records) return [];

    const _sortedActivities = activity_records.sort((summaryA, summaryB) => {
        if (summaryA.type_definition && summaryA.type_definition.activity_type === 'jump') {
            if(summaryA.type_definition.orientation==='vertical')
                return parseFloat(summaryB.data_summary.height) - parseFloat(summaryA.data_summary.height);
            else
                return parseFloat(summaryB.data_summary.distance) - parseFloat(summaryA.data_summary.distance);
        } else {
            return parseFloat(summaryB.data_summary.total_time) - parseFloat(summaryA.data_summary.total_time);
        }
        return 0;
    });

    //const _completedActivities = _.filter(_sortedActivities,(sa) => !_.find(sa.labels,(l)=>l==='incomplete'));

    //const activities = _.uniqBy(_sortedActivities,'user')

    const activityRecordList = _.map(_sortedActivities, (activity_record) => {
        let user = activity_record.user;
        if (activity_record.user && typeof(activity_record.user) !== 'object') {
            if (!!act_users && typeof(act_users) === 'object') user = _.find(act_users,(user) => user.id===activity_record.user);
        }

        let score;
        if (activity_record.type_definition && activity_record.type_definition.activity_type === 'jump') {
            if(activity_record.type_definition.orientation==='vertical')
                score = activity_record.data_summary.height;
            else score = activity_record.data_summary.distance;
            if(!score) score = 0;
        } else {
            score = activity_record.data_summary.total_time;
        }

        activity_type = activity_record.type_definition.activity_type;

        let unit = getUnit(activity_record.type_definition.units);
        let display_name = activity_record.type_definition.distance+unit+' '+ activity_record.type_definition.start_type+' '+ activity_record.type_definition.activity_type;
        if(activity_type==='jump') display_name =  activity_record.type_definition.orientation+' '+ activity_record.type_definition.activity_type;

        return {
            ...activity_record,
            user,
            score,
            display_name
        }
    });

    if (activity_type === 'jump') {
        return activityRecordList.sort((actA, actB) => actB.score - actA.score);
    }
    return activityRecordList.sort((actA, actB) => actA.score - actB.score);
}

export const activityRecordSelector = createSelector(
    userSelector,
    getActivityRecordsSelector,
    getActivityRecordsWithActivityAndUsers
);
