import _ from 'lodash';
import { getSessionsByOrg, getCurrentOrg } from '../../redux/reducers';
import { getActiveUsers, getActiveUsersIsFetching } from '../../redux/reducers';

import { createSelectorCreator, defaultMemoize } from 'reselect'

export const userSelector = state => state.users.byIds;
export const tagSelector = state => state.tags.byIds;
export const tagDataSelector = state => state.tags.tagLiveData;
export const orgMembersSelector = state => state.org_members.byIds;

export const getAllUsersInActivity = (state, props) => {
    return getActiveUsers(state);
}

export const getActiveUsersInActivity = (state, props) => {
    const activeUsers = getActiveUsers(state);
    return _.filter(activeUsers, (activeUser) => activeUser.isInActivity === true);
}

export const processActiveUsers = (users, tags, tagsData, active_users) => {
    return _.map(active_users, (active_user) => {
        const user = active_user.user;
        let tag = active_user.tag;
        let tag_data = active_user.tag_data;

        if (!tag && active_user.tag && typeof(active_user.tag) !== 'object') {
            tag = _.find(tags, {id: parseInt(active_user.tag)});
        }
        if (!tag && active_user.tag && typeof(active_user.tag) === 'object' && active_user.tag.id) {
            tag = _.find(tags, {id: parseInt(active_user.tag.id)});
        }
        if(!tag && user.current_tag) {
            tag = _.find(tags, {id: parseInt(user.current_tag)});
        }

        if(tag && typeof(active_user.tag) !== 'object') tag = _.find(tags, {id: parseInt(active_user.tag)});
        if(tag) tag_data = _.find(tagsData, {id: parseInt(tag.id)});

        return {
            ...active_user,
            user,
            tag,
            tag_data,
        }
    });
}

const createDeepEqualSelector = createSelectorCreator(
    defaultMemoize,
    _.isEqual
)

export const allActiveUsersSelector = createDeepEqualSelector(
    userSelector,
    tagSelector,
    tagDataSelector,
    orgMembersSelector,
    getAllUsersInActivity,
    processActiveUsers
);

export const activityActiveUsersSelector = createDeepEqualSelector(
    userSelector,
    tagSelector,
    tagDataSelector,
    getActiveUsersInActivity,
    processActiveUsers
);
