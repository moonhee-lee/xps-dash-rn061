import * as fromReducers from '../../redux/reducers';
import * as teamAthleteSelectors from '../../redux/selectors/team_athletes';

jest.mock('../../redux/reducers');


describe('userSelector suite', () => {

    it('returns users byIds', () => {
        const byIds = {
            1: {id: 1, name: 'Test User'},
            2: {id: 2, name: 'Test User 2'}
        };
        const state = {
            users: {
                byIds
            }
        };
        expect(teamAthleteSelectors.userSelector(state)).toEqual(byIds);
    });

});

describe('orgMembersSelector suite', () => {

    it('returns users byIds', () => {
        const byIds = {
            1: {id: 1, name: 'Test User'},
            2: {id: 2, name: 'Test User 2'}
        };
        const state = {
            org_members: {
                byIds
            }
        };
        expect(teamAthleteSelectors.orgMembersSelector(state)).toEqual(byIds);
    });

});

describe('getTeamUsers suite', () => {
    const users = [
        {id: 1, first_name: 'B User'},
        {id: 2, first_name: 'B User'},
        {id: 3, first_name: 'A User'},
        {id: 4, first_name: 'C User with long name'},
    ];
    const org_members = [
        {id: 1, first_name: 'B User', user: 1},
        {id: 2, first_name: 'B User', user: 2},
        {id: 3, first_name: 'A User', user: 3},
        {id: 4, first_name: 'C User with long name', user: 4},
    ];
    const userIds = [1, 2, 3, 4, 5];

    it('returns sorted users', () => {
        const sortedSessionsWithUsers = teamAthleteSelectors.getAthleteUsers(users, org_members);
        expect(sortedSessionsWithUsers[0].id).toEqual(3);
        expect(sortedSessionsWithUsers[0].first_name).toEqual('A User');
        expect(sortedSessionsWithUsers[1].id).toEqual(1);
    });

    it('returns empty with no org_members', () => {
        const sortedSessionsWithUsers = teamAthleteSelectors.getAthleteUsers(users, null);
        expect(sortedSessionsWithUsers.length).toBe(0);
    });
});
