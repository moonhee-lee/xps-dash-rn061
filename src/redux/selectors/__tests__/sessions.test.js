import * as fromReducers from '../../redux/reducers';
import * as sessionsSelectors from '../../redux/selectors/sessions';

jest.mock('../../redux/reducers');
jest.mock("react-native-background-timer", () => {});
jest.mock("rn-ios-user-agent", () => {});

describe('getSessionsByOrgSelector suite', () => {

    it('calls getCurrentOrg and getSessionsByOrg', () => {
        const currOrg = {id: 1};
        const expectedValue = [{id: 1}, {id: 2}]
        fromReducers.getCurrentOrg.mockImplementationOnce(() => currOrg);
        fromReducers.getSessionsByOrg.mockImplementationOnce(() => expectedValue);
        expect(sessionsSelectors.getSessionsByOrgSelector({})).toEqual(expectedValue);
    });

});

describe('getSessionsWithOrgs suite', () => {
    const orgs = [
        {id: 1, name: 'Org 1'},
        {id: 2, name: 'Org 2'},
        {id: 3, name: 'Org 3'},
    ];
    // date 1 is yesterday
    const date1 = new Date();
    date1.setDate(date1.getDate() - 1);
    // date 2 is now.
    const date2 = new Date();
    // sessions is in the wrong order.
    let sessions = [
        {id: 1, org: 2, modified: date1},
        {id: 2, org: 1, modified: date2},
    ];

    it('returns sorted sessions', () => {
        const sortedSessionsWithOrgs = sessionsSelectors.getSessionsWithOrgs(orgs, sessions);
        expect(sortedSessionsWithOrgs[0].id).toEqual(3);
        expect(sortedSessionsWithOrgs[0].name).toEqual('Org 3');
        expect(sortedSessionsWithOrgs[1].id).toEqual(2);
        expect(sortedSessionsWithOrgs[1].name).toEqual('Org 2');
        expect(sortedSessionsWithOrgs[2].id).toEqual(1);
        expect(sortedSessionsWithOrgs[2].name).toEqual('Org 1');
    });

    it('returns sorted sessions with already filled org', () => {
        sessions[0].org = orgs[0];
        const sortedSessionsWithOrgs = sessionsSelectors.getSessionsWithOrgs(orgs, sessions);
        expect(sortedSessionsWithOrgs[0].id).toEqual(3);
        expect(sortedSessionsWithOrgs[0].name).toEqual('Org 3');
        expect(sortedSessionsWithOrgs[1].id).toEqual(2);
        expect(sortedSessionsWithOrgs[1].name).toEqual('Org 2');
        expect(sortedSessionsWithOrgs[2].id).toEqual(1);
        expect(sortedSessionsWithOrgs[2].name).toEqual('Org 1');
    });

    it('returns sorted sessions with non-existent org', () => {
        sessions[2] = {id: 3, org: 99};
        const sortedSessionsWithOrgs = sessionsSelectors.getSessionsWithOrgs(orgs, sessions);
        expect(sortedSessionsWithOrgs[0].id).toEqual(3);
        expect(sortedSessionsWithOrgs[0].name).toEqual('Org 3');
        expect(sortedSessionsWithOrgs[1].id).toEqual(2);
        expect(sortedSessionsWithOrgs[1].name).toEqual('Org 2');
        expect(sortedSessionsWithOrgs[2].id).toEqual(1);
        expect(sortedSessionsWithOrgs[2].name).toEqual('Org 1');
    });

});
