import * as fromReducers from '../../redux/reducers';
import * as activityUsersSelectors from '../../redux/selectors/activity_users';

jest.mock('../../redux/reducers');


describe('userSelector suite', () => {
    it('returns users byIds', () => {
        const byIds = {
            1: {id: 1},
            2: {id: 2, name: 'Test User 2'}
        };
        const state = {
            users: {
                byIds
            }
        };
        expect(activityUsersSelectors.userSelector(state)).toEqual(byIds);
    });
});

describe('tagSelector suite', () => {
    it('returns tags byIds', () => {
        const byIds = {
            1: {id: 1, name: 'Test Tag'},
            2: {id: 2, name: 'Test Tag 2'}
        };
        const state = {
            tags: {
                byIds
            }
        };
        expect(activityUsersSelectors.tagSelector(state)).toEqual(byIds);
    });

});

describe('tagDataSelector suite', () => {

    it('returns users tagLiveData', () => {
        const tagLiveData = {
            1: {id: 1},
            2: {id: 2}
        };
        const state = {
            tags: {
                tagLiveData
            }
        };
        expect(activityUsersSelectors.tagDataSelector(state)).toEqual(tagLiveData);
    });
});

describe('getActivityUsers suite', () => {
    it('calls getActivityUsersByActivity', () => {
        const activity = {id: 1};
        fromReducers.getActivityUsersByActivity.mockImplementationOnce(() => activity);
        expect(activityUsersSelectors.getActivityUsers({}, {activityId: 1})).toEqual(activity);
    });

    it('calls getActivityUsersByActivity with no activity', () => {
        fromReducers.getActivityUsersByActivity.mockImplementationOnce(() => []);
        expect(activityUsersSelectors.getActivityUsers({}, {activityId: 2})).toEqual([]);
    });
});

describe('getActivityUsersWithUsers suite', () => {
    let users, tags, tagData, activityUsers, orgMembers;

    beforeEach(() => {
        users = [
            {id: 1, first_name: 'User 1'},
            {id: 2, first_name: 'User 2'},
        ];
        tags = [
            {id: 1, name: '1'},
            {id: 2, name: '2'},
        ];
        tagData = [
            {id: 1, name: 1, isRecording: true},
            {id: 2, name: 2, isRecording: true},
        ];
        activityUsers = [
            {id: 1, user: 1, tag: 1},
            {id: 2, user: 2, tag: 2},
        ];
        orgMembers = [
            {id: 1, user: 1, default_tag: 1},
            {id: 2, user: 2, default_tag: 2},
        ];
    })

    it('returns activity_users with found users', () => {
        const returnedActivityUsers = activityUsersSelectors.getActivityUsersWithUsers(users, tags, tagData, activityUsers);
        expect(returnedActivityUsers[0].user).toEqual(users[0]);
        expect(returnedActivityUsers[0].tag).toEqual(tags[0]);
        expect(returnedActivityUsers[1].tag_data).toEqual(tagData[1]);
    });

    it('returns activity_users with found users and org_members', () => {
        const returnedActivityUsers = activityUsersSelectors.getActivityUsersWithUsers(users, tags, tagData, activityUsers, orgMembers);
        expect(returnedActivityUsers[0].user).toEqual(users[0]);
        expect(returnedActivityUsers[0].tag).toEqual(tags[0]);
        expect(returnedActivityUsers[1].tag_data).toEqual(tagData[1]);
    });

    it('returns activity_users with not found users', () => {
        activityUsers[0].user = 5;
        const returnedActivityUsers = activityUsersSelectors.getActivityUsersWithUsers(users, tags, tagData, activityUsers, orgMembers);
        expect(returnedActivityUsers[0].user).toEqual(undefined);
        expect(returnedActivityUsers[0].tag).toEqual(tags[0]);
        expect(returnedActivityUsers[1].tag_data).toEqual(tagData[1]);
    });

    it('returns activity_users with not found tags and no orgMember', () => {
        activityUsers[0].tag = 5;
        const returnedActivityUsers = activityUsersSelectors.getActivityUsersWithUsers(users, tags, tagData, activityUsers);
        expect(returnedActivityUsers[0].user).toEqual(users[0]);
        expect(returnedActivityUsers[0].tag).toEqual(undefined);
        expect(returnedActivityUsers[1].tag_data).toEqual(tagData[1]);
    });

    it('returns activity_users with not found tags and with orgMember', () => {
        activityUsers[0].tag = 5;
        const returnedActivityUsers = activityUsersSelectors.getActivityUsersWithUsers(users, tags, tagData, activityUsers, orgMembers);
        expect(returnedActivityUsers[0].user).toEqual(users[0]);
        expect(returnedActivityUsers[0].tag).toEqual(tags[0]);
        expect(returnedActivityUsers[1].tag_data).toEqual(tagData[1]);
    });

    it('returns activity_users with already found users', () => {
        activityUsers[1].user = users[1];
        const returnedActivityUsers = activityUsersSelectors.getActivityUsersWithUsers(users, tags, tagData, activityUsers);
        expect(returnedActivityUsers[0].user).toEqual(users[0]);
        expect(returnedActivityUsers[0].tag).toEqual(tags[0]);
        expect(returnedActivityUsers[1].tag_data).toEqual(tagData[1]);
    });

    it('returns activity_users with already found users and org_members', () => {
        activityUsers[1].user = users[1];
        const returnedActivityUsers = activityUsersSelectors.getActivityUsersWithUsers(users, tags, tagData, activityUsers, orgMembers);
        expect(returnedActivityUsers[0].user).toEqual(users[0]);
        expect(returnedActivityUsers[0].tag).toEqual(tags[0]);
        expect(returnedActivityUsers[1].tag_data).toEqual(tagData[1]);
    });

    it('returns activity_users with already found tags', () => {
        activityUsers[1].tag = tags[1];
        const returnedActivityUsers = activityUsersSelectors.getActivityUsersWithUsers(users, tags, tagData, activityUsers, orgMembers);
        expect(returnedActivityUsers[0].user).toEqual(users[0]);
        expect(returnedActivityUsers[0].tag).toEqual(tags[0]);
        expect(returnedActivityUsers[1].tag_data).toEqual(tagData[1]);
    });

    it('returns activity_users with not found tags and no orgMember', () => {
        activityUsers[1].tag = null;
        activityUsers[1].tag_data = null;
        const returnedActivityUsers = activityUsersSelectors.getActivityUsersWithUsers(users, tags, tagData, activityUsers);
        expect(returnedActivityUsers[0].user).toEqual(users[0]);
        expect(returnedActivityUsers[1].tag).toEqual(null);
        expect(returnedActivityUsers[1].tag_data).toEqual(null);
    });

    it('returns activity_users with not found tags and with orgMember', () => {
        activityUsers[1].tag = null;
        activityUsers[1].tag_data = null;
        const returnedActivityUsers = activityUsersSelectors.getActivityUsersWithUsers(users, tags, tagData, activityUsers, orgMembers);
        expect(returnedActivityUsers[0].user).toEqual(users[0]);
        expect(returnedActivityUsers[1].tag).toEqual(tags[1]);
        expect(returnedActivityUsers[1].tag_data).toEqual(tagData[1]);
    });

    it('returns activity_users with default tag', () => {
        orgMembers[0].default_tag = 1;
        const returnedActivityUsers = activityUsersSelectors.getActivityUsersWithUsers(users, tags, tagData, activityUsers, orgMembers);
        expect(returnedActivityUsers[0].user).toEqual(users[0]);
        expect(returnedActivityUsers[0].tag).toEqual(tags[0]);
        expect(returnedActivityUsers[1].tag_data).toEqual(tagData[1]);
    });


});

