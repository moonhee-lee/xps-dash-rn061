import * as fromReducers from '../../redux/reducers';
import * as activeUsersSelectors from '../../redux/selectors/active_users';

jest.mock('../../redux/reducers');


describe('userSelector suite', () => {
    it('returns users byIds', () => {
        const byIds = {
            1: {id: 1},
            2: {id: 2, name: 'Test User 2'}
        };
        const state = {
            users: {
                byIds
            }
        };
        expect(activeUsersSelectors.userSelector(state)).toEqual(byIds);
    });
});

describe('tagSelector suite', () => {
    it('returns tags byIds', () => {
        const byIds = {
            1: {id: 1, name: 'Test Tag'},
            2: {id: 2, name: 'Test Tag 2'}
        };
        const state = {
            tags: {
                byIds
            }
        };
        expect(activeUsersSelectors.tagSelector(state)).toEqual(byIds);
    });

});

describe('tagDataSelector suite', () => {

    it('returns users tagLiveData', () => {
        const tagLiveData = {
            1: {id: 1},
            2: {id: 2}
        };
        const state = {
            tags: {
                tagLiveData
            }
        };
        expect(activeUsersSelectors.tagDataSelector(state)).toEqual(tagLiveData);
    });
});

describe('getActiveUsers suite', () => {
    it('calls getActiveUsers', () => {
        const activity = {id: 1};
        fromReducers.getActiveUsers.mockImplementationOnce(() => activity);
        expect(activeUsersSelectors.getAllUsersInActivity({}, {activityId: 1})).toEqual(activity);
    });

    it('calls getActiveUsers with no activity', () => {
        fromReducers.getActiveUsers.mockImplementationOnce(() => []);
        expect(activeUsersSelectors.getAllUsersInActivity({}, {})).toEqual([]);
    });
});

describe('processActiveUsers suite', () => {
    let users, tags, tagData, activeUsers, orgMembers;

    beforeEach(() => {
        users = [
            {id: 1, first_name: 'User 1'},
            {id: 2, first_name: 'User 2'},
        ];
        tagData = [
            {id: 1, name: '1'},
            {id: 2, name: '2'},
        ];
        tags = [
            {id: 1, name: 1, isRecording: true},
            {id: 2, name: 2, isRecording: true},
        ];
        activeUsers = [
            {id: 1, user: 1, tag: 1, current_tag: 1},
            {id: 2, user: 2, tag: 2, current_tag: 2},
        ];
        orgMembers = [
            {id: 1, user: {id:1}, current_tag: 1},
            {id: 2, user: {id:2}, current_tag: 2},
        ];
    })


    it('returns activity_users with found users', () => {
        const returnedactiveUsers = activeUsersSelectors.processActiveUsers(users, tags, tagData, activeUsers);
        expect(returnedactiveUsers[0].user).toEqual(1);
        expect(returnedactiveUsers[0].tag).toEqual(tags[0]);
        expect(returnedactiveUsers[1].tag_data).toEqual(tagData[1]);
    });

    it('returns activity_users with found users and org_members', () => {
        const returnedactiveUsers = activeUsersSelectors.processActiveUsers(users, tags, tagData, activeUsers);
        expect(returnedactiveUsers[0].user).toEqual(1);
        expect(returnedactiveUsers[0].tag).toEqual(tags[0]);
        expect(returnedactiveUsers[1].tag_data).toEqual(tagData[1]);
    });

    it('returns activity_users with found users and org_members but no default tag on org member', () => {
        orgMembers[0].current_tag = null;
        orgMembers[1].current_tag = null;
        const returnedactiveUsers = activeUsersSelectors.processActiveUsers(users, tags, tagData, activeUsers);
        expect(returnedactiveUsers[0].user).toEqual(1);
        expect(returnedactiveUsers[0].tag).toEqual(tags[0]);
        expect(returnedactiveUsers[1].tag_data).toEqual(tagData[1]);
    });

    it('returns activity_users with not found users', () => {
        activeUsers[0].user = 5;
        const returnedactiveUsers = activeUsersSelectors.processActiveUsers(users, tags, tagData, activeUsers);
        expect(returnedactiveUsers[0].user).toEqual(5);
        expect(returnedactiveUsers[0].tag).toEqual(tags[0]);
        expect(returnedactiveUsers[1].tag_data).toEqual(tagData[1]);
    });

    it('returns activity_users with not found tags and with orgMember', () => {
        activeUsers[0].tag = 5;
        const returnedactiveUsers = activeUsersSelectors.processActiveUsers(users, tags, tagData, activeUsers);
        expect(returnedactiveUsers[0].user).toEqual(1);
        expect(returnedactiveUsers[0].tag).toEqual(undefined);
        expect(returnedactiveUsers[1].tag_data).toEqual(tagData[1]);
    });

    it('returns activity_users with already found users', () => {
        activeUsers[1].user = users[1];
        const returnedactiveUsers = activeUsersSelectors.processActiveUsers(users, tags, tagData, activeUsers);
        expect(returnedactiveUsers[0].user).toEqual(1);
        expect(returnedactiveUsers[0].tag).toEqual(tags[0]);
        expect(returnedactiveUsers[1].tag_data).toEqual(tagData[1]);
    });

    it('returns activity_users with already found users and org_members', () => {
        activeUsers[1].user = users[1];
        const returnedactiveUsers = activeUsersSelectors.processActiveUsers(users, tags, tagData, activeUsers);
        expect(returnedactiveUsers[0].user).toEqual(1);
        expect(returnedactiveUsers[0].tag).toEqual(tags[0]);
        expect(returnedactiveUsers[1].tag_data).toEqual(tagData[1]);
    });

    it('returns activity_users with already found tags', () => {
        activeUsers[1].tag = tags[1];
        const returnedactiveUsers = activeUsersSelectors.processActiveUsers(users, tags, tagData, activeUsers);
        expect(returnedactiveUsers[0].user).toEqual(1);
        expect(returnedactiveUsers[0].tag).toEqual(tags[0]);
        expect(returnedactiveUsers[1].tag_data).toEqual(tagData[1]);
    });

    it('returns activity_users with not found tags and with orgMember', () => {
        activeUsers[1].tag = null;
        activeUsers[1].tag_data = null;
        const returnedactiveUsers = activeUsersSelectors.processActiveUsers(users, tags, tagData, activeUsers);
        expect(returnedactiveUsers[0].user).toEqual(1);
        expect(returnedactiveUsers[1].tag).toEqual(null);
        expect(returnedactiveUsers[1].tag_data).toEqual(null);
    });

    it('returns activity_users with default tag', () => {
        orgMembers[0].current_tag = 1;
        const returnedactiveUsers = activeUsersSelectors.processActiveUsers(users, tags, tagData, activeUsers);
        expect(returnedactiveUsers[0].user).toEqual(1);
        expect(returnedactiveUsers[0].tag).toEqual(tags[0]);
        expect(returnedactiveUsers[1].tag_data).toEqual(tagData[1]);
    });


});

