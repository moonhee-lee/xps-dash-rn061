import * as fromReducers from '../../redux/reducers';
import * as activitySummariesSelectors from '../../redux/selectors/activity_summaries';

jest.mock('../../redux/reducers');


describe('userSelector suite', () => {
    it('returns users byIds', () => {
        const byIds = {
            1: {id: 1},
            2: {id: 2, name: 'Test User 2'}
        };
        const state = {
            users: {
                byIds
            }
        };
        expect(activitySummariesSelectors.userSelector(state)).toEqual(byIds);
    });
});

describe('getActivities suite', () => {
    it('calls getActivitiesBySession', () => {
        const activities = [{id: 1}];
        fromReducers.getActivitiesBySession.mockImplementationOnce(() => activities);
        expect(activitySummariesSelectors.getActivities({}, {session: {id: 1}})).toEqual(activities);
    });

    it('calls getActivitiesBySession with no activity', () => {
        fromReducers.getActivitiesBySession.mockImplementationOnce(() => []);
        expect(activitySummariesSelectors.getActivities({}, {session: {id: 2}})).toEqual([]);
    });
});

describe('getActivitiesByActiveSession suite', () => {
    it('calls getActivitiesBySession', () => {
        const activeSession = {id: 1};
        const activities = [
            {id: 1,type_definition: {activity_type:'sprint'}},
            {id: 2,type_definition: {activity_type:'sprint'}},
        ];
        fromReducers.getActiveSession.mockImplementationOnce(() => activeSession);
        fromReducers.getActivitiesBySession.mockImplementationOnce(() => activities);
        expect(activitySummariesSelectors.getActivitiesByActiveSession({})).toEqual(activities);
    });

    it('calls getActivitiesBySession with no active session', () => {
        fromReducers.getActiveSession.mockImplementationOnce(() => null);
        expect(activitySummariesSelectors.getActivitiesByActiveSession({})).toEqual([]);
    });
});

describe('getActivitySummaries suite', () => {
    it('returns users byIds', () => {
        const byIds = {
            1: {id: 1},
            2: {id: 2}
        };
        const state = {
            activity_summaries: {
                byIds
            }
        };
        expect(activitySummariesSelectors.getActivitySummaries(state)).toEqual(byIds);
    });
});

describe('getActivitySummariesByCurrentActivity suite', () => {
    it('calls getActivitySummariesByActivity', () => {
        const activities = [{id: 1,type_definition: {activity_type:'sprint'}}];
        fromReducers.getActivitySummariesByActivity.mockImplementationOnce(() => activities);
        expect(activitySummariesSelectors.getActivitySummariesByCurrentActivity({}, {activity: {id: 1}})).toEqual(activities);
    });

    it('calls getActivitySummariesByActivity with no activity', () => {
        fromReducers.getActivitySummariesByActivity.mockImplementationOnce(() => []);
        expect(activitySummariesSelectors.getActivitySummariesByCurrentActivity({}, {activity: {id: 2}})).toEqual([]);
    });
});

describe('getActivitySummaryWithUser suite', () => {
    it('calls getActivitySummariesByActivity', () => {
        const activity_summary = {id: 1, user: 1};
        const user = {id: 1, first_name: 'Test'};
        const users = [user];
        const expectedResult = {
            id: 1,
            user,
        };
        expect(activitySummariesSelectors.getActivitySummaryWithUser(activity_summary, users)).toEqual(expectedResult);
    });

    it('calls getActivitySummariesByActivity with no user', () => {
        const activity_summary = {id: 1, user: null};
        const user = {id: 1, first_name: 'Test'};
        const users = [user];
        const expectedResult = {
            id: 1,
            user: null,
        };
        expect(activitySummariesSelectors.getActivitySummaryWithUser(activity_summary, users)).toEqual(expectedResult);
    });

    it('calls getActivitySummariesByActivity with user already filled in', () => {
        const user = {id: 1, first_name: 'Test'};
        const activity_summary = {id: 1, user};
        const users = [user];
        const expectedResult = {
            id: 1,
            user,
        };
        expect(activitySummariesSelectors.getActivitySummaryWithUser(activity_summary, users)).toEqual(expectedResult);
    });

});



describe('getActivityWithSummaries suite', () => {
    let users, activities, activitySummaries;

    beforeEach(() => {
        users = [
            {id: 1, first_name: 'User 1'},
            {id: 2, first_name: 'User 2'},
        ];
        activities = [
            {id: 1, leader1: 1, leader2: 2,type_definition: {activity_type:'sprint'}},
            {id: 2, leader1: 1, leader2: 2,type_definition: {activity_type:'jump',orientation:'vertical'}},
        ];
        activitySummaries = [
            {id: 1, user: 1, tag: 1},
            {id: 2, user: 2, tag: 2},
        ]
    })

    it('returns activities with found activity_summaries', () => {
        const returnedActivities = activitySummariesSelectors.getActivityWithSummaries(users, activities, activitySummaries);
        const expectedLeader1 = {
            ...activitySummaries[0],
            user: users[0],
        };
        const expectedLeader2 = {
            ...activitySummaries[1],
            user: users[1],
        };
        //expect(returnedActivities[0].leader1).toEqual(expectedLeader1);
        //expect(returnedActivities[0].leader2).toEqual(expectedLeader2);
    });

    it('returns activities with not found activity_summaries', () => {
        activities[0].leader1 = {"id": 1, "tag": 1, "user": {"first_name": "User 1", "id": 1}};
        const returnedActivities = activitySummariesSelectors.getActivityWithSummaries(users, activities, activitySummaries);
        //expect(returnedActivities[0].leader1).toEqual({"id": 1, "tag": 1, "user": {"first_name": "User 1", "id": 1}});
    });

    it('returns activities with not found activity_summaries 2', () => {
        activities[0].leader2 = {"id": 2, "tag": 2, "user": {"first_name": "User 2", "id": 2}};
        const returnedActivities = activitySummariesSelectors.getActivityWithSummaries(users, activities, activitySummaries);
        //expect(returnedActivities[0].leader2).toEqual({"id": 2, "tag": 2, "user": {"first_name": "User 2", "id": 2}});
    });

    it('returns activities with already found leader1', () => {
        activities[0].leader1 = {id: 1};
        const expectedLeader2 = {
            ...activitySummaries[1],
            user: users[1],
        };
        const returnedActivities = activitySummariesSelectors.getActivityWithSummaries(users, activities, activitySummaries);
        //expect(returnedActivities[0].leader1).toEqual({"id": 1, "tag": 1, "user": {"first_name": "User 1", "id": 1}});
        //expect(returnedActivities[0].leader2).toEqual(expectedLeader2);
    });

    it('returns activities with already found leader2', () => {
        activities[0].leader2 = {"id": 2, "tag": 2, "user": {"first_name": "User 2", "id": 2}};
        const expectedLeader1 = {
            ...activitySummaries[0],
            user: users[0],
        };
        const returnedActivities = activitySummariesSelectors.getActivityWithSummaries(users, activities, activitySummaries);
        //expect(returnedActivities[0].leader1).toEqual(expectedLeader1);
        //expect(returnedActivities[0].leader2).toEqual({"id": 2, "tag": 2, "user": {"first_name": "User 2", "id": 2}});
    });

});


describe('getActivitySummariesWithUsers suite', () => {
    let users, activities, activitySummaries, activitySummaries2, activitySummaries3;

    beforeEach(() => {
        users = [
            {id: 1, first_name: 'User 1'},
            {id: 2, first_name: 'User 2'},
        ];
        activitySummaries = [
            {id: 1, user: 1, top_record: {score: '10.0'},type_definition: {activity_type:'sprint',start_type:'motion',distance:10},data_summary:{total_time:10}},
            {id: 2, user: 2, top_record: {score: '8.0'},type_definition: {activity_type:'sprint',start_type:'motion',distance:10},data_summary:{total_time:8}},
            {id: 3, user: 3, top_record: null,type_definition: {activity_type:'sprint',start_type:'motion',distance:20}},
        ]
        activitySummaries2 = [
            {id: 1, user: 1, top_record: {score: '20.0'},type_definition: {activity_type:'jump',orientation:'horizontal'},data_summary:{length:20}},
            {id: 2, user: 2, top_record: {score: '10.0'},type_definition: {activity_type:'jump',orientation:'horizontal'},data_summary:{length:10}},
        ]
        activitySummaries3 = [
            {id: 1, user: 1, top_record: {score: '10.0'},type_definition: {activity_type:'jump',orientation:'vertical'},data_summary:{height:10}},
            {id: 2, user: 2, top_record: {score: '20.0'},type_definition: {activity_type:'jump',orientation:'vertical'},data_summary:{height:20}},
        ]
    })

    it('returns activities with found activity_summaries', () => {
        const returnedActivitySummaries = activitySummariesSelectors.getActivitySummariesWithUsers(users, activitySummaries);
        expect(returnedActivitySummaries[0].id).toEqual(2);
        expect(returnedActivitySummaries[1].id).toEqual(1);
    });

    it('returns activities with found horizotnal jump activity_summaries', () => {
        const returnedActivitySummaries = activitySummariesSelectors.getActivitySummariesWithUsers(users, activitySummaries2);
        expect(returnedActivitySummaries[0].id).toEqual(1);
        expect(returnedActivitySummaries[1].id).toEqual(2);
    });

    it('returns activities with found vertical jump activity_summaries', () => {
        const returnedActivitySummaries = activitySummariesSelectors.getActivitySummariesWithUsers(users, activitySummaries3);
        expect(returnedActivitySummaries[0].id).toEqual(2);
        expect(returnedActivitySummaries[1].id).toEqual(1);
    });
});

describe('getActivitySummariesWithUsersDesc suite', () => {
    let users, activities, activitySummaries;

    beforeEach(() => {
        users = [
            {id: 1, first_name: 'User 1'},
            {id: 2, first_name: 'User 2'},
        ];
        activitySummaries = [
            {id: 1, user: 1, top_record: {score: '10.0'},type_definition: {activity_type:'sprint',start_type:'motion'}},
            {id: 2, user: 2, top_record: {score: '12.0'},type_definition: {activity_type:'jump',orientation:'vertical'}},
            {id: 3, user: 3, top_record: null,type_definition: {activity_type:'sprint',start_type:'motion'}},
        ]
    })

    it('returns activities with found activity_summaries', () => {
        const returnedActivitySummaries = activitySummariesSelectors.getActivitySummariesWithUsersDesc(users, activitySummaries);
        expect(returnedActivitySummaries[0].id).toEqual(2);
        expect(returnedActivitySummaries[1].id).toEqual(1);
    });
});

describe('getActivitySummariesWithUsersAsc suite', () => {
    let users, activities, activitySummaries;

    beforeEach(() => {
        users = [
            {id: 1, first_name: 'User 1'},
            {id: 2, first_name: 'User 2'},
        ];
        activitySummaries = [
            {id: 1, user: 1, top_record: {score: '10.0'},type_definition: {activity_type:'sprint',start_type:'motion'}},
            {id: 2, user: 2, top_record: {score: '12.0'},type_definition: {activity_type:'jump',orientation:'vertical'}},
            {id: 3, user: 3, top_record: null,type_definition: {activity_type:'sprint',start_type:'motion'}},
        ]
    })

    it('returns activities with found activity_summaries', () => {
        const returnedActivitySummaries = activitySummariesSelectors.getActivitySummariesWithUsersAsc(users, activitySummaries);
        expect(returnedActivitySummaries[0].id).toEqual(1);
        expect(returnedActivitySummaries[1].id).toEqual(2);
    });
});
