import * as fromReducers from '../../redux/reducers';
import * as activityRecordSelectors from '../../redux/selectors/activity_records';

jest.mock('../../redux/reducers');

describe('userSelector suite', () => {
    it('returns users byIds', () => {
        const user = {1:{id: 1}};
        fromReducers.getUserById.mockImplementationOnce(() => user);
        expect(activityRecordSelectors.userSelector({users:{byIds:user}})).toEqual(user);
    });
});


describe('activitySelector suite', () => {
    it('returns users byIds', () => {
        const users = [{id: 1}];
        const activity = {id:1,type_hash:2};
        const activity_records = [
            {id: 1, user: 1, activity: 1, score: 2.0, data_summary:{total_time:12}, type_definition:{activity_type:'sprint',distance:5,start_type:'motion'}},
            {id: 2, user: 1, activity: 1, score: 1.0, data_summary:{total_time:12}, type_definition:{activity_type:'sprint',distance:5,start_type:'motion'}},
            {id: 3, user: 1, activity: 1, score: 3.0, data_summary:{total_time:12}, type_definition:{activity_type:'sprint',distance:5,start_type:'motion'}},
        ];
        const expected = [{activity: 1, data_summary: {total_time: 12}, display_name: "5m motion sprint", id: 1, score: 12, type_definition: {activity_type: "sprint",distance:5,start_type:'motion'}, user: {id: 1}}, {activity: 1, data_summary: {total_time: 12}, display_name: "5m motion sprint", id: 2, score: 12, type_definition: {activity_type: "sprint",distance:5,start_type:'motion'}, user: {id: 1}}, {activity: 1, data_summary: {total_time: 12}, display_name: "5m motion sprint", id: 3, score: 12, type_definition: {activity_type: "sprint",distance:5,start_type:'motion'}, user: {id: 1}}]

        fromReducers.getActivityById.mockImplementationOnce(() => activity);
        expect(activityRecordSelectors.getActivityRecordsWithActivityAndUsers(users, activity_records)).toEqual(expected);
    });
});


describe('getActivityRecordsWithActivityAndUsers suite', () => {
    it('returns users byIds', () => {
        const users = [{id: 1}];
        const activity_record = {id: 1};
        const activity = {id:1,type_hash:2};
        const activity_records = [
            {id: 1, user: 1, activity: 1, score: 2.0, data_summary:{total_time:12}, type_definition:{activity_type:'sprint',distance:5,start_type:'motion'}},
            {id: 2, user: 1, activity: 1, score: 1.0, data_summary:{total_time:12}, type_definition:{activity_type:'sprint',distance:5,start_type:'motion'}},
            {id: 3, user: 1, activity: 1, score: 3.0, data_summary:{total_time:12}, type_definition:{activity_type:'sprint',distance:5,start_type:'motion'}},
        ];
        const expected = [{activity: 1, data_summary: {total_time: 12}, display_name: "5m motion sprint", id: 1, score: 12, type_definition: {activity_type: "sprint",distance:5,start_type:'motion'}, user: {id: 1}}, {activity: 1, data_summary: {total_time: 12}, display_name: "5m motion sprint", id: 2, score: 12, type_definition: {activity_type: "sprint",distance:5,start_type:'motion'}, user: {id: 1}}, {activity: 1, data_summary: {total_time: 12}, display_name: "5m motion sprint", id: 3, score: 12, type_definition: {activity_type: "sprint",distance:5,start_type:'motion'}, user: {id: 1}}]

        fromReducers.getActivityRecords.mockImplementationOnce(() => activity_record);
        expect(activityRecordSelectors.getActivityRecordsWithActivityAndUsers(users, activity_records)).toEqual(expected);
    });
});

describe('getActivityRecordsWithActivityAndUsers suite', () => {
    let act_users, act_activity, activity_records;

    beforeEach(() => {
        act_users = [{id: 1, first_name: 'User 1'}];
        act_activity = {id: 1, activity_type: 'jump'};
        activity_records = [
            {id: 1, user: 1, activity: 1, score: 2.0, data_summary:{total_time:12}, type_definition:{activity_type:'sprint',distance:5,start_type:'motion'}},
            {id: 2, user: 1, activity: 1, score: 1.0, data_summary:{total_time:12}, type_definition:{activity_type:'sprint',distance:5,start_type:'motion'}},
            {id: 3, user: 1, activity: 1, score: 3.0, data_summary:{total_time:12}, type_definition:{activity_type:'sprint',distance:5,start_type:'motion'}},
        ]
    })

    it('returns activities with found user/activity', () => {
        const returnedActivityRecords = activityRecordSelectors.getActivityRecordsWithActivityAndUsers(act_users, activity_records);
        expect(returnedActivityRecords[0].user).toEqual(act_users[0]);
        expect(returnedActivityRecords[1].activity).toEqual(1);
    });

    it('returns activities with not found user', () => {
        activity_records[0].user = null;
        activity_records[1].user = null;
        activity_records[2].user = null;
        const returnedActivityRecords = activityRecordSelectors.getActivityRecordsWithActivityAndUsers(act_users, activity_records);
        expect(returnedActivityRecords[1].user).toEqual(null);
        expect(returnedActivityRecords[1].activity).toEqual(1);
    });

    it('returns activities with no act_user', () => {
        act_users = null;
        const returnedActivityRecords = activityRecordSelectors.getActivityRecordsWithActivityAndUsers(act_users, activity_records);
        expect(returnedActivityRecords[0].user).toEqual(1);
        expect(returnedActivityRecords[1].activity).toEqual(1);
    });

    it('returns activities with not found activity', () => {
        activity_records[0].activity = null;
        activity_records[1].activity = null;
        activity_records[2].activity = null;
        const returnedActivityRecords = activityRecordSelectors.getActivityRecordsWithActivityAndUsers(act_users, activity_records);
        expect(returnedActivityRecords[2].activity).toEqual(null);
        expect(returnedActivityRecords[1].activity).toEqual(null);
        expect(returnedActivityRecords[0].activity).toEqual(null);
    });

    it('returns activities with no act_activity', () => {
        act_activity = null;
        const returnedActivityRecords = activityRecordSelectors.getActivityRecordsWithActivityAndUsers(act_users, activity_records);
        expect(returnedActivityRecords[0].activity).toEqual(1);
        expect(returnedActivityRecords[1].activity).toEqual(1);
        expect(returnedActivityRecords[2].activity).toEqual(1);
    });

    it('returns sorted jumps', () => {
        activity_records = [
            {id: 1, user: 1, activity: 1, data_summary:{total_time:10,length:10}, type_definition:{activity_type:'jump',orientation:'vertical'}},
            {id: 2, user: 1, activity: 1, data_summary:{total_time:11,length:11}, type_definition:{activity_type:'jump',orientation:'vertical'}},
            {id: 3, user: 1, activity: 1, data_summary:{total_time:12,length:12}, type_definition:{activity_type:'jump',orientation:'vertical'}},
        ]
        const returnedActivityRecords = activityRecordSelectors.getActivityRecordsWithActivityAndUsers(act_users, activity_records);
        expect(returnedActivityRecords[0].id).toEqual(1);
        expect(returnedActivityRecords[0].score).toEqual(0);
        expect(returnedActivityRecords[1].id).toEqual(2);
        expect(returnedActivityRecords[2].id).toEqual(3);
    });

    it('returns sorted non jumps', () => {
        const returnedActivityRecords = activityRecordSelectors.getActivityRecordsWithActivityAndUsers(act_users, activity_records);
        expect(returnedActivityRecords[0].id).toEqual(1);
        expect(returnedActivityRecords[0].score).toEqual(12);
        expect(returnedActivityRecords[1].id).toEqual(2);
        expect(returnedActivityRecords[2].id).toEqual(3);
    });
});
