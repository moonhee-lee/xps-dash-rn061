import _ from 'lodash';
import { createSelector } from 'reselect';
import { getActivitiesBySession, getActivitySummariesByActivity, getActiveSession, getActivityRecordsByType } from '../../redux/reducers';


export const userSelector = state => state.users.byIds;

export const getActivities = (state, props) =>
    getActivitiesBySession(state, props.session.id);

export const getActivitiesByActiveSession = (state) => {
    const activeSession = getActiveSession(state);
    if (activeSession) return getActivitiesBySession(state, activeSession.id);
    return [];
}

export const getActivityRecordsSelector = (state, props) => getActivityRecordsByType(state, props.activity.type_hash);

export const getActivitySummaries = state => state.activity_summaries.byIds

export const getActivitySummariesByCurrentActivity = (state, props) => {
    return getActivitySummariesByActivity(state, props.activity.id);
}

export const getActivityRecordsByCurrentActivity = (state, props) => {
    return getActivitySummariesByActivity(state, props.activity.id);
}


export const getActivitySummaryWithUser = (activity_summary, users) => {
    let user = activity_summary.user;
    if (activity_summary.user && typeof(activity_summary.user) !== 'object') {
        user = _.find(users, {id: activity_summary.user});
    }
    return {
        ...activity_summary,
        user,
    }
}

export const getActivityWithSummaries = (users, activities, activity_summaries) => {
    let activity_type;
    const processedActivities = _.map(activities, (activity) => {
       let leader1 = activity.type_definition.leader1;
        let leader2 = activity.type_definition.leader2;

        if (activity.type_definition.leader1 && typeof(activity.type_definition.leader1) !== 'object') {
            const activity_summary1 = _.find(activity_summaries, {id: activity.type_definition.leader1});
            if (activity_summary1) leader1 = getActivitySummaryWithUser(activity_summary1, users);
        }
        if (activity.type_definition.leader2 && typeof(activity.type_definition.leader2) !== 'object') {
            const activity_summary2 = _.find(activity_summaries, {id: activity.type_definition.leader2});
            if (activity_summary2) leader2 = getActivitySummaryWithUser(activity_summary2, users);
        }

        activity_type = activity.type_definition.activity_type;

        let display_name = activity.type_definition.distance+'m '+ activity.type_definition.start_type+' '+ activity.type_definition.activity_type;
        if(activity_type==='jump') display_name =  activity.type_definition.orientation+' '+ activity.type_definition.activity_type;

        return {
            ...activity,
            leader1,
            leader2,
            display_name
        }
    });
    return processedActivities.sort((activityA, activityB) => {return activityB.id - activityA.id});
}

export const getActivitySummariesWithUsersDesc = (users, activity_summaries) => {
    const populatedSummaries = _.map(activity_summaries, (activity_summary) =>{
        return getActivitySummaryWithUser(activity_summary, users);
    });

    return populatedSummaries.sort((summaryA, summaryB) => {
        if (summaryA.top_record && summaryB.top_record) {
            return parseFloat(summaryB.top_record.score) - parseFloat(summaryA.top_record.score);
        }
        return 0;
    });
}

export const getActivitySummariesWithUsers = (users, activity_records) => {
    let activity_type;
    const populatedSummaries = [];
    const userCounts = {};

    _.each(activity_records, (activity_record) =>{
        activity_type = activity_record.type_definition.activity_type;

        let user = activity_record.user;

        if (activity_record.user && typeof(activity_record.user) !== 'object') {
            if (users && typeof(users) === 'object') user = _.find(users,(user) => user.id===activity_record.user);
        }

        if(typeof(user) === 'object') {
            if (userCounts[user.id]) userCounts[user.id] = userCounts[user.id]+1;
            else userCounts[user.id] = 1;

            let score;
            if (activity_record.type_definition && activity_record.type_definition.activity_type === 'jump') {
                if(activity_record.type_definition.orientation==='vertical') score = activity_record.data_summary.height;
                else score = activity_record.data_summary.distance;
                if(!score) score = 0;
            } else {
                score = activity_record.data_summary.total_time;
            }

            const userSummary = _.find(populatedSummaries, (summary) => summary.user.id === user.id);

            let display_name = activity_record.type_definition.distance+'m '+ activity_record.type_definition.start_type+' '+ activity_record.type_definition.activity_type;
            if(activity_type==='jump') display_name =  activity_record.type_definition.orientation+' '+ activity_record.type_definition.activity_type;

            if (activity_type === 'jump') {
                if (!userSummary || userSummary.score < score) {
                    populatedSummaries.push({
                        ...activity_record,
                        score,
                        user,
                        count: userCounts[user.id],
                        display_name
                    })
                } else {
                    userSummary.count = userCounts[user.id];
                }
            } else {
                if (!userSummary || userSummary.score > score) {
                    populatedSummaries.push({
                        ...activity_record,
                        score,
                        user,
                        count: userCounts[user.id],
                        display_name
                    })
                } else {
                    userSummary.count = userCounts[user.id];
                }
            }
        }
    });

    _.each(populatedSummaries,(sum) => sum.count = userCounts[sum.user.id]);

    if (activity_type === 'jump') {
        const _sortedSummaries = populatedSummaries.sort((summaryA, summaryB) => {
            if (summaryA.score && summaryB.score) {
                return parseFloat(summaryB.score) - parseFloat(summaryA.score);
            }
            return 0;
        });
        return _.uniqBy(_sortedSummaries,(summary) => summary.user.id);
    }

    const _sortedSummaries = populatedSummaries.sort((summaryA, summaryB) => {
        if (summaryA.top_record && summaryB.top_record) {
            return parseFloat(summaryA.score) - parseFloat(summaryB.score);
        }
        return 0;
    });

    return _.uniqBy(_sortedSummaries,(summary) => summary.user.id);
}

export const getActivitySummariesWithUsersAsc = (users, activity_summaries) => {
    const populatedSummaries = _.map(activity_summaries, (activity_summary) =>{
        return getActivitySummaryWithUser(activity_summary, users);
    });

    return populatedSummaries.sort((summaryA, summaryB) => {
        if (summaryA.top_record && summaryB.top_record) {
            return parseFloat(summaryA.top_record.score) - parseFloat(summaryB.top_record.score);
        }
        return 0;
    });
}

export const activitiesSummariesSelector = createSelector(
    userSelector,
    getActivityRecordsSelector,
    getActivitySummariesWithUsers
)

export const activitiesSummariesSelectorDesc = createSelector(
    userSelector,
    getActivitySummariesByCurrentActivity,
    getActivitySummariesWithUsersDesc
)


export const activitiesSummariesSelectorAsc = createSelector(
    userSelector,
    getActivitySummariesByCurrentActivity,
    getActivitySummariesWithUsersAsc
)


export const activitiesWithLeadersSelector = createSelector(
    userSelector,
    getActivities,
    getActivitySummaries,
    getActivityWithSummaries
);
