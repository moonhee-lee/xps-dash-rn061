import _ from 'lodash';
import { createSelector } from 'reselect';
import { getSessionsByOrg, getCurrentOrg, getLocalActivities } from '../../redux/reducers';
import { fetchActivitiesBySession } from '../../redux/local/activity';
import moment from "moment";

export const getSessionsByOrgSelector = state => {
    const currOrg = getCurrentOrg(state);
    return getSessionsByOrg(state, currOrg.id);
}

export const getSessionsWithOrgs = (sessions) => {
    // return descending modified-time values
    _.each(sessions,(s) => {
        if(!s.modified && s.localID) s.modified = moment(s.id,moment.ISO_8601);
    })
    return _.orderBy(sessions, ['created'], ['desc']);
    //return sessions.sort((sessionA, sessionB) => {return new Date(sessionB.name) - new Date(sessionA.name)});
}

export const sessionsSelector = createSelector(
    getSessionsByOrgSelector,
    getSessionsWithOrgs
)
