import _ from 'lodash';
import { createSelector } from 'reselect';
import { getSessionsByOrg, getCurrentOrg } from '../../redux/reducers';
import { getActivityUsersByActivity, getActivityUsersIsFetching } from '../../redux/reducers';

import { createSelectorCreator, defaultMemoize } from 'reselect'


export const userSelector = state => state.users.byIds;
export const tagSelector = state => state.tags.byIds;
export const tagDataSelector = state => state.tags.tagLiveData;
export const orgMembersSelector = state => state.org_members.byIds;

export const getActivityUsers = (state, props) => {
    return getActivityUsersByActivity(state, props.activityId);
}


export const getActivityUsersWithUsers = (users, tags, tagsData, activity_users, org_members) => {
    return _.map(activity_users, (activity_user) => {
        let user = activity_user.user;
        let tag = activity_user.tag;
        let tag_data = activity_user.tag_data;
        let org_member;

        if(org_members) org_member = _.find(org_members,{user: activity_user.user});

        if (activity_user.user && typeof(activity_user.user) !== 'object') {
            user = _.find(users, {id: activity_user.user});
            if(org_members) org_member = _.find(org_members,{user: activity_user.user});
        }
        if (activity_user.user && typeof(activity_user.user) === 'object' && activity_user.user.id) {
            user = _.find(users, {id: activity_user.user.id});
            if(org_members) org_member = _.find(org_members,{user: activity_user.user.id});
        }

        if (activity_user.tag && typeof(activity_user.tag) !== 'object') {
            tag = _.find(tags, {id: parseInt(activity_user.tag)});
        }
        if (!tag && activity_user.tag && typeof(activity_user.tag) === 'object' && activity_user.tag.id) {
            tag = _.find(tags, {id: parseInt(activity_user.tag.id)});
        }
        if (!tag && org_member && org_member.default_tag) {
            tag = _.find(tags,{id:org_member.default_tag});
        }

        if(tag) tag_data = _.find(tagsData, {id: parseInt(tag.id)});

        return {
            ...activity_user,
            user,
            tag,
            tag_data,
        }
    });
}

const createDeepEqualSelector = createSelectorCreator(
  defaultMemoize,
  _.isEqual
)

export const activityUsersSelector = createDeepEqualSelector(
    userSelector,
    tagSelector,
    tagDataSelector,
    getActivityUsers,
    orgMembersSelector,
    getActivityUsersWithUsers
);
