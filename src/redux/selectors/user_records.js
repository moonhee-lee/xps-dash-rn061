import _ from 'lodash';
import { createSelector } from 'reselect';
import { getActivityById, getUserById, getActivityRecords, getActivityRecordsByUser, getSessionsByOrg } from '../../redux/reducers';


export const userSelector = (state, props) =>
    getUserById(state, props.athlete.id);

export const getActivityRecordsSelector = (state, props) =>
    getActivityRecordsByUser(state, props);

export const getSessions = (state, props) =>
    getSessionsByOrg(state, 1);


export const getUserRecordsWithActivitiesAndSessions = (act_user, act_sessions, activity_records) => {
    return _.map(activity_records, (activity_record) => {
        let user = activity_record.user;
        let activity = activity_record.activity;
        let session = activity_record.session;

        if (activity_record.user && typeof(activity_record.user) !== 'object') {
            if (act_user && typeof(act_user) === 'object') user = act_user;
        }
        if (activity_record.session && typeof(activity_record.session) !== 'object') {
            if (act_sessions && act_sessions.length>0) {
                session = _.filter(act_sessions, (s) => s.id === activity_record);
            }
        }
        return {
            ...activity_record,
            user,
            activity,
            session,
        }
    });
}

export const userRecordSelector = createSelector(
    userSelector,
    getSessions,
    getActivityRecordsSelector,
    getUserRecordsWithActivitiesAndSessions
);
