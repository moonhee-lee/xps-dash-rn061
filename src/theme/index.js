import { StyleSheet } from 'react-native';
import React from "react";
import vars from './variables/common.js'


/**
 * Shared (global) styles.
 *
 * TODO - rename for generic elements once styles have been finalized.
 * TODO - determine if we need to customize the NativeBase theme in the
 *         theme/components and theme/variables folders.
 *
 */


const styles = StyleSheet.create({
    contentStyle: {
        backgroundColor:vars.grayLighter,
        elevation:1
    },
    contentStyleNoBottom: {
        marginBottom:0,
        backgroundColor:vars.white,
        elevation:1
    },
    sessionCard: {
        shadowColor: null,
        shadowOffset: null,
        shadowOpacity: null,
        elevation: null,
        paddingHorizontal:10
    },
    sessionTitle: {
        // fontFamily: 'OpenSans-Bold',
        fontSize:16,
        textAlign:'left',
        alignSelf: 'flex-start'
    },
    activityGroupItemRight: {
        flex: 1,
        textAlign:'right',
    },
    activityGroupItemRow: {
        paddingTop: 20,
        paddingBottom: 20,
        width:'100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth:1,
        borderBottomColor:'#d7d7d7'
    },
    activityTitle: {
        fontFamily: 'OpenSans-Regular',
        fontSize:14,
        textAlign:'left',
        alignSelf: 'flex-start'
    },
    typeLosengeTitle: {
        width:'100%',
        paddingHorizontal: 20,
        flexDirection:'row'
    },
    typeLosengePrimary: {
      // fontFamily: 'OpenSans-Bold',
        fontSize:14,
        textAlign:'left',
        paddingHorizontal: 5,
        paddingVertical: 2,
        // letterSpacing:-1,
        fontWeight:'900'
    },
    typeLosengeSecondary: {
      fontFamily: 'OpenSans-Regular',
        fontSize:14,
        textAlign:'left',
        backgroundColor:vars.white,
        borderRadius:4,
        borderWidth:1,
        borderColor:vars.grayLight,
        paddingHorizontal: 5,
        marginLeft: 5,
        fontWeight:'600',
        color:vars.gray
    },
    typeLosengeTertiary: {
      fontFamily: 'OpenSans-Regular',
        fontSize:14,
        textAlign:'left',
        // backgroundColor:vars.grayLight,
        borderRadius:4,
        paddingHorizontal: 5,
        // marginLeft: 5,
        fontWeight:'600',
        color:vars.grayLight
    },
    syncIconSynced: {
      color:vars.green
    },
    syncIconUnsynced: {
      color:vars.black
    },
    hasTopBar: {
        marginTop:70
    },
    barWrapper: {
        height:"100%",
        width:"100%",
        paddingHorizontal: 20,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    sessionList: {
        padding: 20,
        width:'100%',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomColor: '#e5e5e5',
        borderBottomWidth: 1,
    },
    listItem: {
        paddingTop: 10,
        paddingBottom: 10,
        paddingRight: 20,
        paddingLeft: 20,
        width:'100%',
    },
    listItem85: {
        paddingTop: 10,
        paddingBottom: 10,
        paddingRight: 20,
        paddingLeft: 10,
        width:'85%'
    },
    listItemLeft85: {
        paddingTop: 10,
        paddingBottom: 10,
        paddingRight: 10,
        paddingLeft: 20,
        width:'85%'
    },
    listItem75: {
        paddingTop: 10,
        paddingBottom: 10,
        paddingRight: 20,
        paddingLeft: 10,
        width:'75%'
    },
    listItem70: {
        paddingTop: 10,
        paddingBottom: 10,
        paddingRight: 20,
        paddingLeft: 10,
        width:'70%'
    },
    listItemLeft70: {
        paddingTop: 10,
        paddingBottom: 10,
        paddingRight: 10,
        paddingLeft: 20,
        width:'70%'
    },
    listItem60: {
        paddingTop: 10,
        paddingBottom: 10,
        paddingRight: 20,
        paddingLeft: 10,
        width:'60%',
    },
    listItem50: {
        paddingTop: 10,
        paddingBottom: 10,
        paddingRight: 20,
        paddingLeft: 20,
        width:'50%',
    },
    listItem25: {
        paddingTop: 10,
        paddingBottom: 10,
        paddingRight: 20,
        paddingLeft: 20,
        width:'25%',
    },
    title: {
        fontFamily: 'OpenSans-Bold',
        fontSize:18,
        textAlign:'left',
        alignSelf: 'flex-start'
    },
    tableHeader: {
        fontFamily: 'OpenSans-Bold',
        fontSize:20,
        textAlign:'center',
        alignSelf: 'center',
        borderLeftWidth:1,borderRightWidth:1,borderBottomWidth:1,borderColor:'#d6d6d6'
    },
    tableRow: {
        fontFamily: 'OpenSans-Regular',
        fontSize:18,
        textAlign:'center',
        alignSelf: 'center',
        borderLeftWidth:1,borderRightWidth:1,borderLeftColor:'#d6d6d6',borderRightColor:'#d6d6d6'
    },
    description: {
        paddingTop: 5,
        paddingBottom: 10,
        paddingLeft: 10,
        fontSize:12,
        fontFamily: 'OpenSans-Regular'
    },
    badge: {
        backgroundColor: 'rgba(0,0,0,.2)',
        marginTop:7,
        borderRadius:4,
        alignSelf: 'flex-end'
    },
    badgeLeft: {
        backgroundColor: 'rgba(0,0,0,.2)',
        marginTop:7,
        borderRadius:4,
        alignSelf: 'flex-start'
    },
    badgeText: {
        color:'#000000',
        fontSize:13,
    },
    footer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomColor: '#e5e5e5',
        borderBottomWidth: 1,
        paddingTop: 10,
        paddingBottom: 10,
    },
    activitySummary: {
        // width:'100%',
        // flexDirection: 'row',
        marginBottom: 16,
        justifyContent: 'center',

        alignItems: 'center'
    },
    activitySummaryMajorRow: {
        width:'100%',
        justifyContent: 'center',
        paddingVertical: 36
    },
    activitySummaryMinorGroup: {

    },
    activitySummaryMinorRow: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        width:'100%',
    },
    viewRow: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 1,
        paddingBottom: 1,
    },
    viewRowNoPadding: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    viewRowBlue: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor:'rgba(46,156,235,.7)',
        borderTopWidth:2,
        borderBottomWidth:3,
        borderTopColor:'#d7d7d7',
        borderBottomColor:'#676767',
    },
    viewRowCenter: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        width:'100%',
    },
    viewRowCenter200: {
        flex: 0,
        height:150,
        flexDirection: 'row',
        justifyContent: 'center',
        width:'100%',
    },
    viewRowGrey: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor:'#e5e5e5',
        borderBottomColor: '#E7E7E7',
        borderBottomWidth: 1,
    },
    viewRowSpaced: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 10,
        paddingBottom: 10,
        borderBottomColor: '#E7E7E7',
        borderBottomWidth: 1,
    },
    detailTabs: {
        height:50,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 10,
        paddingBottom: 10,
        paddingRight:20,
        paddingLeft:20,
        borderBottomColor: '#E7E7E7',
        borderBottomWidth: 1,
    },
    viewRowLeft: {
        flex: 1,
        flexDirection: 'row',
        paddingTop: 10,
        paddingBottom: 10,
    },
    viewRowLeftSpaced: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 10,
        paddingBottom: 10,
        paddingRight:0,
        paddingLeft:0,
    },
    titleRow: {backgroundColor:'#666'},
    titleRowText: {color:'#fff'},
    recordListItem1: {width:'15%',textAlign:'center',alignSelf:'center'},
    recordListItem2: {width:'30%',textAlign:'center',alignSelf:'center'},
    recordListItem3: {width:'30%',textAlign:'center',alignSelf:'center'},
    recordListItem4: {width:'25%',textAlign:'center',alignSelf:'center'},

    jumpListItem1: {width:'70%',textAlign:'left',alignSelf:'center',paddingLeft:20},
    jumpListItem2: {width:'20%',textAlign:'center',alignSelf:'center'},
    jumpListItem3: {width:'10%',textAlign:'center',alignSelf:'center'},
    motionListItem1: {width:'40%',textAlign:'left',alignSelf:'center',paddingLeft:20},
    motionListItem2: {width:'30%',textAlign:'center',alignSelf:'center'},
    motionListItem3: {width:'20%',textAlign:'center',alignSelf:'center'},
    motionListItem4: {width:'10%',textAlign:'center',alignSelf:'center'},
    beepListItem1: {width:'30%',textAlign:'left',alignSelf:'center',paddingLeft:20},
    beepListItem2: {width:'20%',textAlign:'center',alignSelf:'center'},
    beepListItem3: {width:'20%',textAlign:'center',alignSelf:'center'},
    beepListItem4: {width:'20%',textAlign:'center',alignSelf:'center'},
    beepListItem5: {width:'10%',textAlign:'center',alignSelf:'center'},

    footer_grey: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 0,
        paddingRight: 20,
        paddingLeft: 20,
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor:'#e7e7e7'
    },
    rightAlign: {
        textAlign: 'right'
    },
    grey: {
        color: '#A9A9A9'
    },
    dateGrey: {
        fontSize:12,
        textAlign:'right',
        color: '#111',
        fontFamily: 'OpenSans-Light'
    },
    runningMan: {
        fontSize: 20
    },
    errorStyle: {
        color:'#E31414',
        backgroundColor:'rgba(255,255,255,.9)',
        width:'70%',height:56,
        padding:15,
        marginLeft:'15%',marginRight:'15%',marginBottom:14
    },
    inputErrorStyle: {
        color:'#E31414',
        padding:5,
        paddingLeft:30,
    },
    loginLogoBlock: {
        marginBottom: 24
    },
    loginItemStyle: {
        backgroundColor:'#ffffff',
        borderWidth:0,
        borderColor:'#ffffff',
        borderRadius:5,
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 10,
        width:'70%',height:56,
        padding:15,
        marginLeft:'15%',marginRight:'15%',marginTop:14
    },
    loginInputStyle: {
        borderBottomWidth:0,borderColor:'#ffffff'
    },
    loginContainerStyle: {
        width:'80%',borderBottomWidth:0
    },
    loginTextStyle: {
        borderWidth:0,borderColor:'#ffffff'
    },
    loginButtonText: {
        color: vars.white,

    },
    loginButtonStyle: {
        marginTop: 30,
        borderColor: vars.blue,
        backgroundColor:vars.blue,
        borderWidth: 1,
        borderRadius: 5,
        width:'50%',
        marginLeft:'25%',
        // shadowColor: '#000000',
        // shadowOffset: { width: 0, height: 2 },
        // shadowOpacity: 0.5,
        // shadowRadius: 10,
                justifyContent:'center',
        alignItems: 'center',
    },
    buttonStyle: {
        marginTop: 20,
        borderColor: '#2E9CEB',
        backgroundColor:'#ffffff',
        borderWidth: 1,
        borderRadius: 5,
        width:'50%',
        marginLeft:'25%'
    },
    activityIconStyle: {flexGrow: 1,
        justifyContent:'center',
        alignItems: 'center',
        paddingLeft:50,
        paddingRight:50,
        paddingTop:25,
        paddingBottom:25,
        backgroundColor:'#fff'
    },
    activityListIconStyle: {
        width:'20%',
        justifyContent:'center',
        paddingLeft:20,
        paddingRight:20,
    },
    activityIconRowStyle: {
        width:'25%',
        padding:10,
        justifyContent:'center',
        alignItems: 'center',
        backgroundColor:'#fff'
    },
    activityListIconRowStyle: {
        width:'20%',
        justifyContent:'center',
        paddingLeft:20,
        paddingRight:20,
    },
    buttonGroupStyle: {
        borderRadius: 6,
        backgroundColor:'#E7E7E7',
        marginRight:4,
        marginLeft:4,
    },
    buttonGroupSelectedStyle: {
        borderRadius: 6,
        backgroundColor:'#2E9CEB',
        marginRight:4,
        marginLeft:4,
    },
    liveCancelBar: {
        width: '100%',
        position: 'absolute',
        bottom: 70,
        height: 50,
        backgroundColor: '#d7d7d7',
    },
    liveRecordBar: {
        width: '100%',
        position: 'absolute',
        bottom: 0,
        height: 70,
        backgroundColor: '#6AC893',
    },
    liveOptionBar: {
        width: '100%',
        position: 'absolute',
        bottom: 70,
        height: 70,
        backgroundColor: '#2E9CEB',
        borderBottomWidth:2,
        borderBottomColor:'#fff'
    },
    saveDiscardBar: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
        position: 'absolute',
        bottom:0,
        height: 70,
        backgroundColor: '#d7d7d7',
    },
    doneBarActive: {
        width: '100%',
        position: 'absolute',
        bottom: 0,
        height: 70,
        backgroundColor: '#6AC893',
    },
    doneBarInactive: {
        width: '100%',
        position: 'absolute',
        bottom: 0,
        height: 70,
        backgroundColor: '#e7e7e7',
    },
    doneBarActiveText:{color:'#fff'},
    doneBarInactiveText: {color:'#000'},
    liveActiveText:{color:'#fff'},
    liveInactiveText: {color:'#666'},
    createBar: {
        position:'absolute',left:0,top:0,elevation:2,
        width: '100%',
        height: 70,
        backgroundColor: '#333',

    },
    SessionSelectBar: {
        position:'absolute',left:0,elevation:3,
        width: '100%',
        height: 400,
        backgroundColor:'#fff'
    },
    createAthleteBar: {
        position:'absolute',left:0,elevation:2,
        width: '100%',
        height: 100,

    },
    createBarText: {
        color:'#fff',
        fontSize:18
    },
    messageBar: {
        position:'absolute',left:0,top:0,
        width: '100%',
        height: 40,
        backgroundColor: '#eb5a5a',
        alignItems:'center'
    },
    messageBarText: {
        color:'#fff',
        fontSize:14,
        lineHeight:40
    },
    avatarInitials: {
        height:45,
        width:45,
        borderRadius:40,
        backgroundColor:'#666',
        color:'#fff',
        padding:13
    },
    tabActiveStyle: {color: '#2E9CEB', fontSize: 19, paddingTop: 25, paddingLeft:10, paddingRight:10},
    tabInactiveStyle: {color: '#666666', fontSize: 19, paddingTop: 25, paddingLeft:10, paddingRight:10},
    lightBadge :{
        backgroundColor:'#fff',
        borderColor:'rgba(231, 231, 231, 0.5)',
        color:'rgba(231, 231, 231, 0.5)',
        padding:5
    },
    darkBadge :{
        backgroundColor:'#e5e5e5',
        borderColor:'#f7f7f7',
        color:'#f7f7f7',
        padding:5,
    },
    listItemStyle: {
        paddingTop:20,
        paddingBottom:20,
        paddingRight:20,
        paddingLeft:20
    },
    cameraPreview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    cameraCapture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20
    },
    iconLarge: {
        fontSize:80
    },
    iconMed: {
        fontSize:70
    },
    iconLarge2: {
        fontSize:50
    },
    iconMed2: {
        fontSize:40
    },
    dividerLine: {
        height:10,width:1,borderLeftWidth:1,borderLeftColor:'#cccccc',backgroundColor:'#0ff'
    },
    flyDistanceMarker : {flex:1,fontSize:12,color:'#999999',textAlign:"center"},
    saveOverlay: {
        backgroundColor: '#000000',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'center',
        alignItems: 'center',
        opacity: 0.8
    },
    saveOverlayText: {
        color: '#fff'
    },
    loading: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    },
    overlay: {
        backgroundColor: 'rgba(0,0,0,.6)',
        height: '100%'
    },
    centerContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    }

});

export default styles;
