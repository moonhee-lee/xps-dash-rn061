export default {
	white: '#ffffff',
	blue: '#0ca3d4',
	black: '#000',
	grayDarker: '#2d3235',
	grayDark: '#394145',
	gray: '#495b5b',
	grayLight: '#919299',
	grayLighter: '#f4f6f7',
	mushroom: '#f4f6f7',
	avo1: '#bee1cd',
	avo4: '#6cd1c0',
	avo5: '#44c3a1',
	green: '#729c24',
	greenLight: '#bfe1cd'
};