const faIcons = require('react-native-vector-icons/FontAwesome');

const icons = {
    "home": [faIcons, 19, '#FFFFFF'],
    "search":  [faIcons, 19, '#FFFFFF'],
    "user":  [faIcons, 19, '#FFFFFF'],
    "bars":  [faIcons, 19, '#FFFFFF'],
    "plus":  [faIcons, 19, '#FFFFFF'],
    "plus-circle":  [faIcons, 19, '#FFFFFF'],
    "plus-square":  [faIcons, 60, '#FFFFFF'],
};

let iconsMap = {};
let iconsLoaded = new Promise((resolve, reject) => {
    new Promise.all(
        Object.keys(icons).map(iconName =>
            icons[iconName][0].getImageSource(
                iconName,
                icons[iconName][1],
                icons[iconName][2]
            ))
    ).then(sources => {
        Object.keys(icons)
            .forEach((iconName, idx) => iconsMap[iconName] = sources[idx]);
        resolve(true);
    })
});

export {
    iconsMap,
    iconsLoaded
};