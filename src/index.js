import { Provider } from 'react-redux';
import { Navigation } from 'react-native-navigation';

import _ from 'lodash';
import {setJSExceptionHandler} from 'react-native-exception-handler';

import { setNativeExceptionHandler } from "react-native-exception-handler";

import { registerScreens, startAppAnonymous } from './react/screens';
import { createXPSStore } from './redux/store';
import { setupLocalStorage } from './redux/local_storage';
import { localStorage } from './lib/local_storage';
import { setCurrentTeam } from './redux/actions/team';
import { fetchCurrentWifiState } from './redux/actions/wifi';
import { fetchCurrentCloudState } from './redux/actions/cloud';
import { fetchMemberByUserId } from './redux/actions/org_member';

import {
    SET_AUTH,
    SYSTEM_FETCH_SUCCESS
} from './redux/constants/actionTypes';

Navigation.events().registerAppLaunchedListener(() => {

});

// hack to show network requests in Chrome while debugging.
GLOBAL.XMLHttpRequest = GLOBAL.originalXMLHttpRequest || GLOBAL.XMLHttpRequest;

global.xpsConfig = {
    siteUrl: 'https://xps.xco.io',
    system: {
        1: {
            id: 1,
            system: 1,
            name: 'AWS Broker',
            ssid: 'unassigned',
            serial_num: 'unassigned',
            //ip_address: '192.168.93.104',
            ip_address: '3.217.124.56',
            port: '1884',
            state: 'disconnected',
        }
    },
    connected: false
};

//add message queue handler for deselect/rate change/reselect (outside of redux for speed)
global.tagSelectQueues = {selects:{},deselects:{},rates:{}};
global.timeSyncInterval = 0;
global.timeSyncOngoingInterval = 0;
global.timeSyncCounter = 0;
global.messageInterval = 0;
global.fasttrackVersion = require('../fasttract-version.json');
global.uwbTransmitPowerLevel = 12;

// create the main redux store.
store = createXPSStore()
setupLocalStorage();

// register all of our screens.
registerScreens(store, Provider);

store.dispatch(fetchCurrentWifiState());

localStorage.getItem('xps:appConfig').then((data) => {
    if (data.siteUrl) global.xpsConfig.siteUrl = data.siteUrl;
    if (data.system) global.xpsConfig.system = data.system;

    store.dispatch({
        type: SYSTEM_FETCH_SUCCESS,
        system: global.xpsConfig.system
    });
    global.xpsConfig.connected = true;

    // find out the current wifi status/info
    store.dispatch(fetchCurrentCloudState());

    doLogin();
}).catch(err => {
    localStorage.setItem('xps:appConfig',global.xpsConfig).then((data) => {
        store.dispatch({
            type: SYSTEM_FETCH_SUCCESS,
            system: global.xpsConfig.system
        });
        global.xpsConfig.connected = true;
        doLogin();
    });
});

localStorage.getItem('xps:currentTeam').then((data) => {
    if (data) {
        console.log('setting team from load: '+data)
        store.dispatch(setCurrentTeam(data));
    }
}).catch((e) => {
    console.log('setting team from error: 1')
    store.dispatch(setCurrentTeam(1));
});

localStorage.getItem('xps:debugMode').then((data) => {
    console.log('setting debug mode from load: '+data)
    global.debugMode = data;
}).catch((e) => {
    console.log(e);
    console.log('setting default debug mode: 0')
    global.debugMode = 0;
});

localStorage.getItem('xps:enableWalkingMode').then((data) => {
    console.log('setting enable walking mode from load: '+data);
    global.enableWalkingMode = data;
}).catch((e) => {
    console.log(e);
    console.log('setting default enable walking mode: false');
    global.enableWalkingMode = false;
});

localStorage.getItem('xps:enableSingleLegMode').then((data) => {
    console.log('setting enable single leg mode from load: '+data);
    global.enableSingleLegMode = data;
}).catch((e) => {
    console.log(e);
    console.log('setting default enable single leg mode: false');
    global.enableSingleLegMode = false;
});

localStorage.getItem('xps:enableSkatingMode').then((data) => {
    console.log('setting enable skating mode from load: '+data);
    global.enableSkatingMode = data;
}).catch((e) => {
    console.log(e);
    console.log('setting default enable skating mode: false');
    global.enableSkatingMode = false;
});

localStorage.getItem('xps:enableBobsledMode').then((data) => {
    console.log('setting enable bobsled mode from load: '+data);
    global.enableBobsledMode = data;
}).catch((e) => {
    console.log(e);
    console.log('setting default enable bobsled mode: false');
    global.enableBobsledMode = false;
});

localStorage.getItem('xps:enableStrideLengthView').then((data) => {
    console.log('setting enable stride length view from load: '+data);
    global.enableStrideLengthView = data;
}).catch((e) => {
    console.log(e);
    console.log('setting default enable stride length mode: false');
    global.enableStrideLengthView = false;
});

localStorage.getItem('xps:enableTagPosition').then((data) => {
    console.log('setting enable tag positionfrom load: '+data);
    global.enableTagPosition = data;
}).catch((e) => {
    console.log(e);
    console.log('setting default enable tag position : false');
    global.enableTagPosition = false;
});

localStorage.getItem('xps:enableManualHorizontalJump').then((data) => {
    console.log('setting enable manual horizontal jump load: '+data);
    global.enableManualHorizontalJump = data;
}).catch((e) => {
    console.log(e);
    console.log('setting default manual horizontal jump : false');
    global.enableManualHorizontalJump = false;
});

localStorage.getItem('xps:beepSound').then((data) => {
    console.log('setting beep sound from load: '+data.name)
    global.beepSound = data;
}).catch((e) => {
    console.log(e);
    console.log('setting default beep sound')
    global.beepSound = {id:1,file:"swimStartBeep",type:"m4a",name:"Swim Start"};
});

localStorage.getAllDataForKey('xps:orgMembers').then(activities => {
    // console.log(activities);
}).catch(()=>{});
localStorage.getAllDataForKey('xps:sessions').then(activities => {
    // console.log(activities);
}).catch(()=>{});
localStorage.getAllDataForKey('xps:activities').then(activities => {
    // console.log(activities);
}).catch(()=>{});
localStorage.getItem('xps:activeUsers').then(activeUsers => {
    const _activeUsers = _.pickBy(activeUsers,(a) => a.isInActivity);
    localStorage.setItem('xps:activeUsers',_activeUsers).then(() => {});
}).catch(()=>{});

setJSExceptionHandler((error, isFatal) => {
    if(isFatal) console.log(JSON.stringify(error));
});

setNativeExceptionHandler(exceptionString => {
    console.log(JSON.stringify(exceptionString));
});

// get rid of the annoying warnings during development.
console.disableYellowBox = true;

const doLogin = () => {
    localStorage.getItem('xps:loginState').then((data) => {
        if (data.authToken && data.userId) {
            const userId = data.userId;
            const user = data.user;
            const authToken = data.authToken;

            // set the auth so we can try to fetch this user.
            store.dispatch({
                type: SET_AUTH,
                userId,
                user,
                authToken,
            });
            store.dispatch(fetchMemberByUserId(userId, true));
        } else {
            localStorage.removeItem('xps:loginState').then((data) => {
                startAppAnonymous();
            });
        }
    }).catch((err) => {
        startAppAnonymous();
    });
};
