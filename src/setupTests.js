import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

global.xpsConfig = {
    siteUrl: 'http://localhost:8000/'
};

global.timedStart = {
    startSync: jest.fn()
}

jest.mock('react-native-cached-image', () => {
    return {
        DocumentDir: () => {},
        ImageCache: {
            get: {
                clear: () => {}
            }
        }
    }
})
jest.mock('react-native-aws3', () => {
    return {
        DocumentDir: () => {},
        ImageCache: {
            get: {
                clear: () => {}
            }
        }
    }
})
