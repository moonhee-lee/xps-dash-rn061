import React, { Component } from 'react';
import { View, TouchableOpacity, Text, Animated, Easing } from 'react-native';
import {
    Icon,
} from 'native-base';


class ActivitySprintReadyBar extends Component {

    state = {
        beepStartButtonAnim: new Animated.Value(200),
        cancelButtonAnim: new Animated.Value(200),
        beepReady: false,
    }

    componentDidUpdate(prevProps,prevState) {
        const { isRecording } = this.props;
        if(prevProps.isRecording !== isRecording && !isRecording) {
            this.moveBeepButtons(isRecording, true);
        }
    }

    moveBeepButtons = (isRecording,forceCancel) => {
        let cancelY, startY;
        if(isRecording || forceCancel) {
            cancelY = 200;
            startY = 200;
        } else {
            cancelY = -200;
            startY = -20;
        }

        Animated.timing(
            this.state.cancelButtonAnim,
            {
                toValue: cancelY,
                duration: 300,
                easing: Easing.inOut(Easing.quad),
                useNativeDriver: true
            }
        ).start();
        Animated.timing(
            this.state.beepStartButtonAnim,
            {
                toValue: startY,
                duration: 300,
                easing: Easing.inOut(Easing.quad),
                useNativeDriver: true
            }
        ).start();
    }

    toggleRecording = () => {
        const { toggleRecordingState, isRecording } = this.props;

        this.setState({beepReady:!isRecording});

        this.moveBeepButtons(isRecording,false);

        toggleRecordingState();
    }

    handleSprintBeepStart = () => {
        const { handleSprintBeepStart } = this.props;
        handleSprintBeepStart();
        Animated.timing(
            this.state.cancelButtonAnim,
            {
                toValue: 20,
                duration: 300,
                easing: Easing.inOut(Easing.quad),
                useNativeDriver: true
            }
        ).start();
        Animated.timing(
            this.state.beepStartButtonAnim,
            {
                toValue: 200,
                duration: 300,
                easing: Easing.inOut(Easing.quad),
                useNativeDriver: true
            }
        ).start();
    }

    render () {
        const { onPressAthleteSelect, onPressHomeButton, recordReady } = this.props;

        return (
            <View style={{
                width: '100%',
                position: 'absolute',
                bottom: 0,
                height: 86,
                backgroundColor: '#2E9CEB',
                paddingLeft: 30,
                paddingRight: 30,
            }}>
                {!recordReady &&
                <View style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                }}>
                    <Icon type='FontAwesome' name='users' style={{color: '#bedbf7', fontSize: 19, paddingTop: 25}} onPress={onPressAthleteSelect} />
                    <Icon type='Foundation' name='record' style={{color: '#cccccc', fontSize: 70, paddingTop: 5}} />
                    <Icon type='FontAwesome' name='home' style={{color: '#bedbf7', fontSize: 19, paddingTop: 25}} onPress={onPressHomeButton} />
                </View>
                }

                {!!recordReady &&
                <View style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                }}>
                    <Icon type='FontAwesome' name='users' style={{color: '#bedbf7', fontSize: 19, paddingTop: 25}} onPress={onPressAthleteSelect} />
                    <Icon type='Foundation' name='record' style={{color: '#bedbf7', fontSize: 70, paddingTop: 5}} onPress={this.toggleRecording}/>
                    <Icon type='FontAwesome' name='home' style={{color: '#bedbf7', fontSize: 19, paddingTop: 25}} onPress={onPressHomeButton} />
                    <View style={{position:'absolute',left:0,bottom:0,width:'100%',alignItems:'center'}}>
                        <View style={{width:200}}>
                            <TouchableOpacity style={{position:'absolute',transform: [{translateY: this.state.cancelButtonAnim}],bottom:0,left:0,height:110,width:200,borderRadius:10,backgroundColor:'#363636',alignItems:'center'}} onPress={() => this.toggleRecording()}>
                                <Text style={{color:'#fff',lineHeight:100}}>Cancel</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{position:'absolute',transform: [{translateY: this.state.beepStartButtonAnim}],bottom:0,left:0,height:200,width:200,borderRadius:10,backgroundColor:'#6AC893',alignItems:'center'}} onPress={this.handleSprintBeepStart}>
                                <Text style={{color:"#fff",lineHeight:200}}>Start</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                }
            </View>
        )
    }
}

export default ActivitySprintReadyBar;
