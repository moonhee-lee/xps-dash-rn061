import React, { Component } from 'react';

import {  View, Dimensions, StyleSheet , Text} from 'react-native';

import { LineChart, Grid, Path, XAxis, YAxis } from 'react-native-svg-charts'
import styleVars from '../../theme/variables/common.js'
import { G, Line } from 'react-native-svg'

const dimensionWindow = Dimensions.get('window');
const width = Math.round(dimensionWindow.width);
import * as shape from 'd3-shape'
import _ from 'lodash';

class ActivityRecordGraph extends Component {
    static WIDTH = width;
    state = {
        lineData: null
    };

    render() {
        const { itemData, height, codData } = this.props;

        if(!itemData) return (<View></View>)

        const axesSvg = { fontSize: 10, fill: 'grey' };

        const Shadow = ({ line }) => (
            <Path
                key={'shadow'}
                y={1}
                d={line}
                fill={'none'}
                strokeWidth={2}
                stroke={'rgba(0, 0, 0, 0)'}
            />
        )

        const DistanceGrid = ({ x, y, data, ticks }) => (
            <G>
                {
                    // Horizontal grid
                    ticks.map(tick => (
                        <Line
                            key={ tick }
                            x1={ '0%' }
                            x2={ '100%' }
                            y1={ y(tick) }
                            y2={ y(tick) }
                            stroke={ 'rgba(0,0,0,0.05)' }
                        />
                    ))
                }
                {
                    // Vertical grid
                    data.map((_, index) => (
                        <Line
                            key={ index }
                            y1={ '0%' }
                            y2={ '100%' }
                            x1={ x(index) }
                            x2={ x(index) }
                            stroke={ 'rgba(0,0,0,0)' }
                        />
                    ))
                }
            </G>
        )

        const CoDLines = ({ data, x, y, cod, index }) => (
            <G key={index}>
                <Line
                    y1={'0%'}
                    y2={'100%'}
                    x1={x(cod[0]-1)}
                    x2={x(cod[0]-1)}
                    stroke={'red'}
                    strokeWidth={2}
                />
                <Line
                    y1={'0%'}
                    y2={'100%'}
                    x1={x(cod[1]-1)}
                    x2={x(cod[1]-1)}
                    stroke={'red'}
                    strokeWidth={2}
                />
            </G>
        )

        const showAxes = true;
        const showLines = true;

        let insetStyle;
        if(showAxes) {
            insetStyle = {top: 10, bottom: 10, left:30, right:50};
        } else {
            insetStyle = {top: 10, bottom: 10};
        }

        let graphData = itemData;
        const maxGraphDataPoint = Math.round(Math.max(...graphData)) + 2;

        return (
            <View style={{height: height, width: width, flexDirection: 'row', backgroundColor:styleVars.white }}>
                {!!showAxes && graphData &&
                <YAxis
                    data={graphData}
                    min={0}
                    max={maxGraphDataPoint}
                    style={{marginBottom: 5, width: 30}}
                    contentInset={{top: 6, bottom: 4}}
                    svg={axesSvg}
                />
                }

                {!!graphData && graphData.length &&
                <LineChart
                    style={{height: height, width: showAxes?width-30:width}}
                    data={graphData}
                    gridMin={ 0 }
                    gridMax={maxGraphDataPoint}
                    codData={codData}
                    svg={{stroke: styleVars.blue, strokeWidth: 2}}
                    contentInset={insetStyle}
                    curve={shape.curveNatural}
                >
                    {!!showLines &&
                    <DistanceGrid belowChart={true} />
                    }
                    {!!codData && codData.map((cod, index) =>
                        <CoDLines cod={cod} index={index} belowChart={true} key={index} />
                    )}
                    
                </LineChart>
                }

                {!!showAxes && graphData &&
                <XAxis
                    style={{ marginHorizontal: -10, marginLeft: 60 }}
                    data={graphData}
                    formatLabel={(value, index) => index+1}
                    contentInset={{ left: 0, right:0, bottom:60 }}
                    svg={axesSvg}
                />
                }

            </View>
        )
    }
}

export default ActivityRecordGraph;
