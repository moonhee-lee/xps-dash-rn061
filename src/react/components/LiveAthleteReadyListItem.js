import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Icon, Text, View} from 'native-base';
import {Animated, TouchableOpacity} from 'react-native';
import {UserAvatar} from '../../react/components/UserAvatar';
import styles from '../../theme';
import {getESTVelocity, getTagPosition} from '../../redux/local_storage';
import * as Progress from 'react-native-progress';
import * as formatUtils from '../../lib/format_utils';

export class LiveAthleteReadyListItem extends Component {
    constructor(props) {
        super(props);
        this.velocityInterval = null;
        this.tagPositionInterval = null;
        this.state = {
            buttonAnim: new Animated.Value(1),
            recording: false,
            position: null,
            enableTagPosition: global.enableTagPosition
        }
    }

    componentDidMount() {
        const {enableTagPosition} = this.state;

        if (enableTagPosition) {
            if (!this.tagPositionInterval) {
                this.tagPositionInterval = setInterval(this.updatePosition, 1000);
            }
        }
    }

    componentDidUpdate(prevProps, prevState) {
        const {isRecording, activeUser} = this.props;

        if (!this.velocityInterval && isRecording) {
            this.startAnimation();
            console.log('starting velocity listener')
            this.velocityInterval = setInterval(() => {
                if (activeUser && activeUser.tag && activeUser.tag.name) {
                    const velocity = getESTVelocity(activeUser.tag.name);
                    if (velocity) {
                        this.setState({velocity: velocity});
                    } else {
                        this.setState({velocity: 0});
                    }
                }
            }, 100);
        }
        if (!!this.velocityInterval && !isRecording) {
            console.log('stopping velocity listener')
            clearInterval(this.velocityInterval);
            this.velocityInterval = null;
            this.stopAnimation();
        }

    }

    componentWillUnmount() {
        const {enableTagPosition} = this.state;

        console.log('stopping velocity listener')
        clearInterval(this.velocityInterval);
        this.velocityInterval = null;

        if (enableTagPosition) {
            clearInterval(this.tagPositionInterval);
            this.tagPositionInterval = null;
        }

        this.stopAnimation();
    }

    updatePosition = () => {
        const {activeUser} = this.props;
        const {enableTagPosition} = this.state;

        if (enableTagPosition && activeUser && activeUser.tag && activeUser.tag.name) {
            const position = getTagPosition(activeUser.tag.name);
            this.setState({position});
        }
    }

    startAnimation() {
        console.log('starting animation');
        Animated.loop(
            Animated.sequence([
                Animated.timing(this.state.buttonAnim, {
                    toValue: .5,
                    duration: 500
                }),
                Animated.timing(this.state.buttonAnim, {
                    toValue: 1,
                    duration: 500,
                }),
            ])
        ).start()
    }

    stopAnimation() {
        console.log('stopping animation');
        Animated.loop(
            Animated.sequence([
                Animated.timing(this.state.buttonAnim, {
                    toValue: .5,
                    duration: 500
                }),
                Animated.timing(this.state.buttonAnim, {
                    toValue: 1,
                    duration: 500,
                }),
            ])
        ).stop()
    }

    render() {
        const {activeUser, currentActivity, navigator, callSetupTag, isRecording} = this.props;
        const {velocity, position, enableTagPosition} = this.state;

        if (!activeUser || !activeUser.user) return (<View></View>)

        const battery = activeUser.tag_data ? activeUser.tag_data.battery : undefined;
        const distance = formatUtils.getDistance(position);

        const statusColors = {
            'ready': '#27AE60',
            'select_pending': '#F2C94C',
            'selected': '#FF8203',
            'recording': '#2F80ED',
            'unavailable': '#E0E0E0',
        };

        return (
            <View style={styles.viewRowCenter}>
                <View style={{flex: 1, alignItems: 'center', borderBottomWidth: 0, justifyContent: 'center'}}>
                    <UserAvatar size={45} user={activeUser.user} navigator={navigator}/>
                </View>
                <View style={{flex: 4, height: 60, borderBottomWidth: 0, elevation: 2}}>
                    <Text style={styles.title}>{activeUser.user.full_name}</Text>
                    {!!activeUser.tag &&
                    <Text>
                        <Text style={styles.description}>{activeUser.tag.name}</Text>
                        {!!activeUser.tag_data &&
                        <Text style={styles.description}> - {activeUser.tag_data.status}</Text>
                        }
                        {enableTagPosition && position && distance &&
                        <Text
                            style={styles.description}> {formatUtils.formatPosition(position) + ' dist=' + formatUtils.formatDistance(distance)}</Text>
                        }
                    </Text>
                    }
                    {!activeUser.tag &&
                    <Text style={styles.description}>{}
                        <Text style={styles.description}>{}</Text>
                    </Text>
                    }
                    {activeUser.score > 0 && currentActivity.activity_type === 'jump' &&
                    <View style={{position: "absolute", bottom: 5, right: 0}}>
                        <Text>{formatUtils.formatDistance(activeUser.score)}</Text>
                    </View>
                    }
                    {isRecording && velocity > 0 && currentActivity.activity_type !== 'jump' &&
                    <View style={{position: "absolute", bottom: 5, right: 0}}>
                        <Text>{formatUtils.formatSpeed(velocity)}</Text>
                    </View>
                    }
                    {battery !== undefined && battery >= 80 &&
                    <Icon name="battery-full" type="FontAwesome"
                          style={{fontSize: 18, color: '#27AE60'}}/>
                    }
                    {battery !== undefined  && battery < 80 && battery >= 60 &&
                    <Icon name="battery-three-quarters" type="FontAwesome"
                          style={{fontSize: 18, color: '#F2C94C'}}/>
                    }
                    {battery !== undefined && battery < 60 && battery >= 40 &&
                    <Icon name="battery-half" type="FontAwesome"
                          style={{fontSize: 18, color: '#ff9900'}}/>
                    }
                    {battery !== undefined && battery < 40 && battery >= 10 &&
                    <Icon name="battery-quarter" type="FontAwesome"
                          style={{fontSize: 18, color: '#FF5100'}}/>
                    }
                    {battery !== undefined && battery < 10 &&
                    <Icon name="battery-empty" type="FontAwesome"
                          style={{fontSize: 18, color: '#E31414'}}/>
                    }
                </View>
                {!!isRecording &&
                <Animated.View style={{opacity: this.state.buttonAnim, alignItems: 'flex-end',
                    justifyContent: 'center',
                    flex: 1,
                    borderBottomWidth: 0,
                    elevation: 2}}>
                    {!!activeUser.tag_data &&
                    <Icon name="circle" type="FontAwesome"
                          style={{fontSize: 45, color: statusColors[activeUser.tag_data.status]}}/>
                    }
                </Animated.View>
                }
                {!isRecording &&
                <TouchableOpacity style={{
                    alignItems: 'flex-end',
                    justifyContent: 'center',
                    flex: 1,
                    borderBottomWidth: 0,
                    elevation: 2,
                    opacity:.8
                }} onLongPress={callSetupTag}>
                    {!!activeUser.tag_data &&
                    <Icon name="circle" type="FontAwesome"
                          style={{fontSize: 45, color: statusColors[activeUser.tag_data.status]}}/>
                    }
                    {!activeUser.tag_data &&
                    <Icon name="circle" type="FontAwesome"
                          style={{fontSize: 45, color: statusColors['unavailable']}}/>
                    }
                </TouchableOpacity>
                }
                {isRecording && velocity !== undefined && currentActivity.activity_type!=='jump' &&
                <Progress.Bar
                    progress={velocity/12}
                    width={null}
                    height={10}
                    useNativeDriver={true}
                    borderWidth={0}
                    borderRadius={0}
                    style={{position:'absolute',bottom:-13,left:0,width:'100%',alignSelf:'center'}}
                />
                }
            </View>
        )
    }
}

export default connect(
    null,
    {getESTVelocity}
)(LiveAthleteReadyListItem);
