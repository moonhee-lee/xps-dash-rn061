import React, { Component } from 'react';
import _ from 'lodash';
import { TouchableOpacity, View } from 'react-native';
import { H3, Text, Icon } from 'native-base';

import styles from '../../theme';
import CustomIcon from '../../theme/components/XPSIcon';
import * as formatUtils from '../../lib/format_utils';

class ActivityItem extends Component {

    textOfTripleJumpType = (type) => {
        if (type.endsWith('_hop')) {
            return type.substring(0, type.length - 4);
        }
        return type;
    };

    render() {
        const { activity, index, navigator } = this.props;

        if(!activity) return (<View></View>);

        const isSynced = !activity.localID;

        const title = formatUtils.getActivityTitle(activity);
        const bgColor = index%2?'#ffffff':'#f7f7f7';
        const unit = formatUtils.getUnit(activity.type_definition.units);
        return (
            <View style={styles.activityGroupItemRow}>
                <View>
                    {activity.type_definition.activity_type=='sprint' &&
                    <View style={styles.typeLosengeTitle}>
                        <CustomIcon name='sprint' style={{color:'#666',fontSize:18}} />
                        <Text style={styles.typeLosengePrimary}>{activity.type_definition.activity_type.toUpperCase()}</Text>
                        <Text style={styles.typeLosengeSecondary}>{activity.type_definition.distance+unit}</Text>
                        {(activity.type_definition.bobsled === true) &&
                        <Text style={styles.typeLosengeSecondary}>bobsled</Text>
                        }
                        {(activity.type_definition.walking === true) &&
                        <Text style={styles.typeLosengeSecondary}>walking</Text>
                        }
                        {(activity.type_definition.skating === 'forward' || activity.type_definition.skating === 'backward') &&
                        <Text style={styles.typeLosengeSecondary}>skating</Text>
                        }
                        {(activity.type_definition.skating === 'forward' || activity.type_definition.skating === 'backward') &&
                        <Text style={styles.typeLosengeSecondary}>{activity.type_definition.skating}</Text>
                        }
                        {(activity.type_definition.side === 'left' || activity.type_definition.side === 'right') &&
                        <Text style={styles.typeLosengeSecondary}>{activity.type_definition.side}</Text>
                        }
                       <Text style={styles.typeLosengeTertiary}>{activity.type_definition.start_type}</Text>
                    </View>
                    }
                    {activity.type_definition.activity_type=='jump' &&
                    <View style={styles.typeLosengeTitle}>
                        <CustomIcon name='jump' style={{color:'#666',fontSize:18}} />
                        <Text style={styles.typeLosengePrimary}>{activity.type_definition.activity_type.toUpperCase()}</Text>
                        {(activity.type_definition.side==='left' || activity.type_definition.side==='right') &&
                        <Text style={styles.typeLosengeSecondary}>{activity.type_definition.side}</Text>
                        }
                        {(activity.type_definition.triple_jump_type==='triple_hop' || activity.type_definition.triple_jump_type==='crossover_hop') &&
                        <Text style={styles.typeLosengeSecondary}>{this.textOfTripleJumpType(activity.type_definition.triple_jump_type)}</Text>
                        }
                        <Text style={styles.typeLosengeTertiary}>{activity.type_definition.orientation}</Text>
                    </View>
                    }
                    {activity.type_definition.activity_type=='agility' &&
                    <View style={styles.typeLosengeTitle}>
                        <Icon name="random" type="FontAwesome" style={{color:'#666',fontSize:18}} />
                        <Text style={styles.typeLosengePrimary}>{activity.type_definition.activity_type.toUpperCase()}</Text>
                        <Text style={styles.typeLosengeSecondary}>{activity.type_definition.distance+unit}</Text>
                        <Text style={styles.typeLosengeTertiary}>{activity.type_definition.start_type}</Text>
                    </View>
                    }
                    {activity.type_definition.activity_type=='freeform' &&
                    <View style={styles.typeLosengeTitle}>
                        <Icon name="cloud-upload" type="FontAwesome" style={{color:'#666',fontSize:18}} />
                        <Text style={styles.typeLosengePrimary}>{activity.type_definition.activity_type.toUpperCase()}</Text>
                    </View>
                    }
                </View>
            </View>
        )

    }

}

export default ActivityItem;
