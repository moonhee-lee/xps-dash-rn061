import React from 'react';
import { shallow } from 'enzyme';

import * as fromReducers from '../../redux/reducers';
import ActivityItem  from '../../react/components/ActivityItem';
import ActivitySummaryListItem from '../../react/components/ActivitySummaryListItem';
import { View } from 'react-native';

describe('ActivityItem Component', () => {

    it('renders correctly on load', () => {
        const props = {
            activity: {id: 1,type_definition:{activity_type:'sprint',start_type:"motion"}},
            index: 1
        };
        const enzymeWrapper = shallow(<ActivityItem {...props}/>);
        expect(enzymeWrapper.find(View).length).toBe(5);
    });

    it('renders ActivitySummaryListItem for a jump when activity has 1 leader.', () => {
        const props = {
            activity: {id: 1,activity_type:'jump',display_name:'jump',orientation_type:'vertical',leader1:{id:1,org:1,activity:13,user:{id:2,username:'user1',email:'',full_name:'Michael Kedda',first_name:'Michael',last_name:'Kedda',bio:'',is_staff:false},num_records:12,top_record:{id:1,org:1,activity:13,user:2,tag:3,meta:{end:2553.277,dip1:-0.17978291666666657,dip2:-0.7176719166666665,start:2551.102,height:0.5354580833333333,length:2.1386480000000003,air_time:0.626783424575357},score:null}},type_definition:{activity_type:'sprint',start_type:"motion"}},
            index: 1
        };
        const enzymeWrapper = shallow(<ActivityItem {...props}/>);
        expect(enzymeWrapper.find(ActivitySummaryListItem).length).toBe(0);
    });

    it('renders ActivitySummaryListItem for a jump when activity has 2 leaders.', () => {
        const props = {

            activity: {id: 1,activity_type:'jump',display_name:'jump',orientation_type:'vertical',leader1:{id:1,org:1,activity:13,user:{id:2,username:'user1',email:'',full_name:'Michael Kedda',first_name:'Michael',last_name:'Kedda',bio:'',is_staff:false},num_records:12,top_record:{id:1,org:1,activity:13,user:2,tag:3,meta:{end:2553.277,dip1:-0.17978291666666657,dip2:-0.7176719166666665,start:2551.102,height:0.5354580833333333,length:2.1386480000000003,air_time:0.626783424575357},score:null}},leader2:{id:1,org:1,activity:13,user:{id:2,username:'user1',email:'',full_name:'Michael Kedda',first_name:'Michael',last_name:'Kedda',bio:'',is_staff:false},num_records:12,top_record:{id:1,org:1,activity:13,user:2,tag:3,meta:{end:2553.277,dip1:-0.17978291666666657,dip2:-0.7176719166666665,start:2551.102,height:0.5354580833333333,length:2.1386480000000003,air_time:0.626783424575357},score:null}},type_definition:{activity_type:'sprint',start_type:"motion"}},
            index: 1
        };
        const enzymeWrapper = shallow(<ActivityItem {...props}/>);
        expect(enzymeWrapper.find(ActivitySummaryListItem).length).toBe(0);
    });

    it('renders ActivitySummaryListItem for a sprint when activity has 1 leader.', () => {
        const props = {
            activity: {id: 1,activity_type:'sprint',display_name:'20 m sprint',leader1:{id:1,org:1,activity:13,user:{id:2,username:'user1',email:'',full_name:'Michael Kedda',first_name:'Michael',last_name:'Kedda',bio:'',is_staff:false},num_records:12,top_record:{id:1,org:1,activity:13,user:2,tag:3,meta:{end:2553.277,dip1:-0.17978291666666657,dip2:-0.7176719166666665,start:2551.102,height:0.5354580833333333,length:2.1386480000000003,air_time:0.626783424575357},score:null}},type_definition:{activity_type:'sprint',start_type:"motion"}},
            index: 1
        };
        const enzymeWrapper = shallow(<ActivityItem {...props}/>);
        expect(enzymeWrapper.find(ActivitySummaryListItem).length).toBe(0);
    });

    it('renders ActivitySummaryListItem for a sprint when activity has 2 leaders.', () => {
        const props = {

            activity: {id: 1,activity_type:'sprint',display_name:'20 m sprint',leader1:{id:1,org:1,activity:13,user:{id:2,username:'user1',email:'',full_name:'Michael Kedda',first_name:'Michael',last_name:'Kedda',bio:'',is_staff:false},num_records:12,top_record:{id:1,org:1,activity:13,user:2,tag:3,meta:{end:2553.277,dip1:-0.17978291666666657,dip2:-0.7176719166666665,start:2551.102,height:0.5354580833333333,length:2.1386480000000003,air_time:0.626783424575357},score:null}},leader2:{id:1,org:1,activity:13,user:{id:2,username:'user1',email:'',full_name:'Michael Kedda',first_name:'Michael',last_name:'Kedda',bio:'',is_staff:false},num_records:12,top_record:{id:1,org:1,activity:13,user:2,tag:3,meta:{end:2553.277,dip1:-0.17978291666666657,dip2:-0.7176719166666665,start:2551.102,height:0.5354580833333333,length:2.1386480000000003,air_time:0.626783424575357},score:null}},type_definition:{activity_type:'sprint',start_type:"motion"}},
            index: 1
        };
        const enzymeWrapper = shallow(<ActivityItem {...props}/>);
        expect(enzymeWrapper.find(ActivitySummaryListItem).length).toBe(0);
    });

    it('does not render ActivitySummaryListItem when when activity does not have leaders.', () => {
        const props = {
            activity: {id: 1,leader1:null,leader2:null,type_definition:{activity_type:'sprint',start_type:"motion"}},
            index: 1
        };
        const enzymeWrapper = shallow(<ActivityItem {...props}/>);
        expect(enzymeWrapper.find(ActivitySummaryListItem).length).toBe(0);
    });

    it('does not render ActivitySummaryListItem when when activity is null.', () => {
        const props = {
            activity: null,
            index: 1
        };
        const enzymeWrapper = shallow(<ActivityItem {...props}/>);
        expect(enzymeWrapper.find(ActivitySummaryListItem).length).toBe(0);
    });

});
