import React from 'react';
import { shallow } from 'enzyme';

import SessionListItem  from '../../react/components/SessionListItem';
import { View } from 'react-native';

describe('SessionListItem Component', () => {

    it('renders correctly on load', () => {
        const props = {
            session: {name:'test session',description:'test description',team: {id:1,name:'test team'}}
        };
        const enzymeWrapper = shallow(<SessionListItem {...props}/>);
        expect(enzymeWrapper.find(View).length).toBe(5);
    });

    it('renders correctly on load without session', () => {
        const props = {
            session: {}
        };
        const enzymeWrapper = shallow(<SessionListItem {...props}/>);
        expect(enzymeWrapper.find(View).length).toBe(1);
    });

});
