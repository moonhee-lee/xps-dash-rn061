import React from 'react';
import { shallow } from 'enzyme';

import ListCheckboxItem  from '../../react/components/ListCheckboxItem';
import { CheckBox, ListItem, Body, Text } from 'native-base';

describe('ListCheckboxItem Component', () => {

    it('renders correctly on load', () => {
        const props = {
            id:1,
            label:'',
            isChecked:true,
            index:1
        };
        const enzymeWrapper = shallow(<ListCheckboxItem {...props}/>);
        expect(enzymeWrapper.find(ListItem).length).toBe(1);
    });

});
