import React from 'react';
import { shallow } from 'enzyme';

import ActivityRecordDetailList from '../../react/components/ActivityRecordDetailList';
import ActivityRecordDetailListItem from '../../react/components/ActivityRecordDetailListItem';
import { View, FlatList } from 'react-native';

describe('ActivityRecordDetailList Component', () => {

    it('renders correctly with data', () => {
        const props = {
            listData: [{label:'test',value:'test'}],
            index: 1
        };
        const enzymeWrapper = shallow(<ActivityRecordDetailList {...props}/>);
        expect(enzymeWrapper.find(View).length).toBe(1);
        expect(enzymeWrapper.find(FlatList).length).toBe(1);
        expect(enzymeWrapper.find(ActivityRecordDetailListItem).length).toBe(0);
    });

    it('renders correctly without data', () => {
        const props = {
            listData: [],
            index: 1
        };
        const enzymeWrapper = shallow(<ActivityRecordDetailList {...props}/>);
        expect(enzymeWrapper.find(View).length).toBe(1);
        expect(enzymeWrapper.find(ActivityRecordDetailListItem).length).toBe(0);
    });
});
