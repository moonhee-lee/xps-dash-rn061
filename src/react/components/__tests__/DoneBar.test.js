import React from 'react';
import { shallow } from 'enzyme';

import DoneBar  from '../../react/components/DoneBar';
import { TouchableOpacity, View } from 'react-native';
import { Text, Icon } from 'native-base';

describe('DoneBar Component', () => {

    it('renders correctly on load if isActive', () => {
        const props = {
            onPressSave:jest.fn(),
            isActive: true,
            message: 'test'
        };
        const enzymeWrapper = shallow(<DoneBar {...props}/>);
        expect(enzymeWrapper.find(View).length).toBe(1);
        expect(enzymeWrapper.find(Text).length).toBe(1);
        expect(enzymeWrapper.find(Icon).length).toBe(1);
        expect(enzymeWrapper.find(TouchableOpacity).length).toBe(1);
    });

    it('renders correctly on load if not isActive', () => {
        const props = {
            isActive: false,
            message: 'test'
        };
        const enzymeWrapper = shallow(<DoneBar {...props}/>);
        expect(enzymeWrapper.find(View).length).toBe(2);
        expect(enzymeWrapper.find(Text).length).toBe(1);
        expect(enzymeWrapper.find(Icon).length).toBe(0);
        expect(enzymeWrapper.find(TouchableOpacity).length).toBe(0);
    });

});
