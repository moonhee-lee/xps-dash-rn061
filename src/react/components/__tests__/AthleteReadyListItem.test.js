import React from 'react';
import { shallow } from 'enzyme';

import { AthleteReadyListItem } from '../../react/components/AthleteReadyListItem';
import { View } from 'native-base';

jest.mock("react-native-background-timer", () => {});
jest.mock("rn-ios-user-agent", () => {});

describe('AthleteReadyListItem Component', () => {

    it('renders correctly on load', () => {
        const props = {
            activityUser: {id:125,org:1,activity:13,user:{full_name:"Allan Mercado"},tag:null},
            currentActivity: {id:13,org:1,session:1,display_name:"Jump",activity_type:"jump",distance:0,measurement_type:"meters",start_type:"motion",orientation_type:"vertical"}
        };
        const enzymeWrapper = shallow(<AthleteReadyListItem {...props}/>);
        expect(enzymeWrapper.find(View).length).toBe(3);
    });

    it('renders correctly on load without an activityUser', () => {
        const props = {
            activityUser: null,
            currentActivity: {id:13,org:1,session:1,display_name:"Jump",activity_type:"jump",distance:0,measurement_type:"meters",start_type:"motion",orientation_type:"vertical"}
        };
        const enzymeWrapper = shallow(<AthleteReadyListItem {...props}/>);
        expect(enzymeWrapper.find(View).length).toBe(1);
    });

    it('renders correctly on load without a currentActivity', () => {
        const props = {
            activityUser: {id:125,org:1,activity:13,user:{full_name:"Allan Mercado"},tag:null},
            currentActivity: null
        };
        const enzymeWrapper = shallow(<AthleteReadyListItem {...props}/>);
        expect(enzymeWrapper.find(View).length).toBe(1);
    });

});
