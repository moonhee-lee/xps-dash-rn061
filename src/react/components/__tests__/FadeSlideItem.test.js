import React from 'react';
import { shallow } from 'enzyme';

import { Animated, View, Easing } from 'react-native';
import FadeSlideItem  from '../../react/components/FadeSlideItem';

jest.useFakeTimers();


describe('FadeSlideItem Component', () => {

    it('renders correctly on load', () => {
        const props = {
            onPressSave:() => {},
            isActive: true
        };
        const enzymeWrapper = shallow(<FadeSlideItem {...props}/>);
        expect(enzymeWrapper.find(Animated.View).length).toBe(1);
    });

});
