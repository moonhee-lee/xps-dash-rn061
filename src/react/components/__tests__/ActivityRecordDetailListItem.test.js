import React from 'react';
import { shallow } from 'enzyme';

import ActivityRecordDetailListItem from '../../react/components/ActivityRecordDetailListItem';
import { View } from 'react-native';
import FadeSlideItem from '../../react/components/FadeSlideItem';

describe('ActivityRecordDetailListItem Component', () => {

    it('renders correctly on load', () => {
        const props = {
            label:'test',
            text:'test',
            item: {id:1,value:1},
            rowColor:'',
            index: 1
        };
        const enzymeWrapper = shallow(<ActivityRecordDetailListItem {...props}/>);
        expect(enzymeWrapper.find(FadeSlideItem).length).toBe(1);
        expect(enzymeWrapper.find(View).length).toBe(3);
    });

    it('renders correctly on load without and item', () => {
        const props = {
            label:'test',
            text:'test',
            item: null,
            rowColor:'',
            index: 1
        };
        const enzymeWrapper = shallow(<ActivityRecordDetailListItem {...props}/>);
        expect(enzymeWrapper.find(FadeSlideItem).length).toBe(0);
        expect(enzymeWrapper.find(View).length).toBe(1);
    });

});
