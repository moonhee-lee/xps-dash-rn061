import React from 'react';
import { shallow } from 'enzyme';

import TopCreateBar  from '../../react/components/TopCreateBar';
import { View } from 'react-native';

describe('TopCreateBar Component', () => {

    it('renders correctly on load', () => {
        const props = {
            onPressCreate:() => {},
            message:'testing'
        };
        const enzymeWrapper = shallow(<TopCreateBar {...props}/>);
        expect(enzymeWrapper.find(View).length).toBe(1);
    });

});
