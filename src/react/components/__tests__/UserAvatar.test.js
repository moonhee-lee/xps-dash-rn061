import React from 'react';
import { shallow } from 'enzyme';

import { UserAvatar } from '../../react/components/UserAvatar';
import {
    Text, View
} from 'native-base';
import { Image, TouchableOpacity } from 'react-native';
import CachedImage from 'react-native-cached-image';

describe('UserAvatar Component', () => {

    it('renders initials correctly on load', () => {
        const props = {
            size:null,
            user:{full_name:'Test 1',first_name:'Test',last_name:'Test'},
            setUserImage: jest.fn()
    };
        const enzymeWrapper = shallow(<UserAvatar {...props}/>);
        expect(enzymeWrapper.find(View).length).toBe(1);
        expect(enzymeWrapper.find(TouchableOpacity).length).toBe(1);
        expect(enzymeWrapper.find(Text).length).toBe(1);

    });

    it('renders correctly on load with user image', () => {
        const props = {
            size:null,
            user:{full_name:'Test 2',first_name:'Test',last_name:'Test',user_image:'https://xco.io/static/assets/heroes/broad-002.jpg'},
            setUserImage: jest.fn()
        };
        const enzymeWrapper = shallow(<UserAvatar {...props}/>);
        expect(enzymeWrapper.find(View).length).toBe(1);
        expect(enzymeWrapper.find(TouchableOpacity).length).toBe(1);
    });

    it('renders correctly on load with local image', () => {
        const props = {
            size:null,
            user:{full_name:'Test 3',first_name:'Test',last_name:'Test',local_image:'https://xco.io/static/assets/heroes/broad-002.jpg'},
            setUserImage: jest.fn()
        };
        const enzymeWrapper = shallow(<UserAvatar {...props}/>);
        expect(enzymeWrapper.find(View).length).toBe(1);
        expect(enzymeWrapper.find(TouchableOpacity).length).toBe(1);
    });

    it('renders correctly on load with thumb image', () => {
        const props = {
            size:null,
            user:{full_name:'Test 4',first_name:'Test',last_name:'Test',thumbnail_url:'https://xco.io/static/assets/heroes/broad-002.jpg'},
            setUserImage: jest.fn()
        };
        const enzymeWrapper = shallow(<UserAvatar {...props}/>);
        expect(enzymeWrapper.find(View).length).toBe(1);
        expect(enzymeWrapper.find(TouchableOpacity).length).toBe(1);
    });

    it('renders correctly on load with image', () => {
        const props = {
            size:null,
            user:{full_name:'Test 5',first_name:'Test',last_name:'Test',image_url:'https://xco.io/static/assets/heroes/broad-002.jpg'},
            setUserImage: jest.fn()
        };
        const enzymeWrapper = shallow(<UserAvatar {...props}/>);
        expect(enzymeWrapper.find(View).length).toBe(1);
        expect(enzymeWrapper.find(TouchableOpacity).length).toBe(1);
    });

    it('renders correctly on load with full image', () => {
        const props = {
            size:null,
            user:{full_name:'Test 6',first_name:'Test',last_name:'Test',full_image_url:'https://xco.io/static/assets/heroes/broad-002.jpg'},
            setUserImage: jest.fn()
        };
        const enzymeWrapper = shallow(<UserAvatar {...props}/>);
        expect(enzymeWrapper.find(View).length).toBe(1);
        expect(enzymeWrapper.find(TouchableOpacity).length).toBe(1);
    });

    it('renders correctly on load with no user', () => {
        const props = {
            size:null,
            user:null,
            setUserImage: jest.fn()
        };
        const enzymeWrapper = shallow(<UserAvatar {...props}/>);
        expect(enzymeWrapper.find(View).length).toBe(1);
    });

});
