import React from 'react';
import { shallow } from 'enzyme';

import ActivityRecordGraph from '../../react/components/ActivityRecordGraph';
import { LineChart, Grid, Path } from 'react-native-svg-charts'
import { View } from 'react-native';

describe('ActivityRecordGraph Component', () => {

    it('renders correctly with no data', () => {
        const props = {
            itemData: [],
            index: 1
        };
        const enzymeWrapper = shallow(<ActivityRecordGraph {...props}/>);
        expect(enzymeWrapper.find(View).length).toBe(1);
        expect(enzymeWrapper.find(LineChart).length).toBe(0);
        expect(enzymeWrapper.find(Path).length).toBe(0);
    });


    it('renders correctly with data', () => {
        const props = {
            itemData: [{id:1}],
            index: 1
        };
        const enzymeWrapper = shallow(<ActivityRecordGraph {...props}/>);
        expect(enzymeWrapper.find(View).length).toBe(1);
        expect(enzymeWrapper.find(LineChart).length).toBe(1);
        expect(enzymeWrapper.find(Path).length).toBe(0);
    });
});

