import React from 'react';
import { shallow } from 'enzyme';

import * as fromReducers from '../../redux/reducers';
import { mapStateToProps, BottomTabBar }  from '../../react/components/BottomTabBar';
import { View } from 'react-native';

jest.mock('../../redux/reducers');
jest.mock("react-native-background-timer", () => {});
jest.mock("rn-ios-user-agent", () => {});

describe('mapStateToProps method', () => {

    it('returns currentOrg, sessions and isFetching', () => {
        const activeSession = [
            {id: 1, name: 'Test Session 1'},
        ]
        const activeActivity = [
            {id: 1, name: 'Test Activity 1'},
        ]
        const currentTab = 1
        const expectedResult = {
            currentTab,
            activeSession,
            activeActivity
        };

        fromReducers.getCurrentTab.mockImplementationOnce(() => currentTab);
        fromReducers.getActiveSession.mockImplementationOnce(() => activeSession);
        fromReducers.getActiveActivity.mockImplementationOnce(() => activeActivity);
        expect(mapStateToProps({})).toEqual(expectedResult);
    });

});

describe('BottomTabBar Component', () => {

    it('renders correctly on load', () => {
        const props = {
            currentTab:1,
        };
        const enzymeWrapper = shallow(<BottomTabBar {...props}/>);
        expect(enzymeWrapper.find(View).length).toBe(2);
    });

});
