import React from 'react';
import { shallow } from 'enzyme';

import ActivityRecordDetailLists from '../../react/components/ActivityRecordDetailLists';
import ActivityRecordDetailList from '../../react/components/ActivityRecordDetailList';
import { View } from 'react-native';
import SideSwipe from 'react-native-sideswipe';
import Carousel from "react-native-snap-carousel";

describe('ActivityRecordDetailLists Component', () => {

    it('renders correctly for jumps', () => {
        const props = {
            activityRecord: {id:1,org:1,activity:{id:13,org:1,session:1,display_name:"Jump",activity_type:"jump",distance:0,measurement_type:"meters",start_type:"motion",orientation_type:"vertical",leader1:1,leader2:null},user:{id:2,username:"user1",email:"",full_name:"Michael Kedda",first_name:"Michael",last_name:"Kedda",bio:"",image_url:"https://www.script-up.com/public/assets/img/avatar/default.png",is_staff:false},tag:3,meta:{end:2553.277,dip1:-0.17978291666666657,dip2:-0.7176719166666665,start:2551.102,height:0.5354580833333333,length:2.1386480000000003,air_time:0.626783424575357},score:null,data:{id:1,org:1,activity_record:1,data:[]}},
            listData: [{id:1}],
            index: 1
        };
        const enzymeWrapper = shallow(<ActivityRecordDetailLists {...props}/>);
        expect(enzymeWrapper.find(View).length).toBe(1);
    });

    it('renders correctly for sprints', () => {
        const props = {
            activityRecord: {id:1,org:1,activity:{id:13,org:1,session:1,display_name:"Jump",activity_type:"sprint",distance:0,measurement_type:"meters",start_type:"motion",orientation_type:"vertical",leader1:1,leader2:null},user:{id:2,username:"user1",email:"",full_name:"Michael Kedda",first_name:"Michael",last_name:"Kedda",bio:"",image_url:"https://www.script-up.com/public/assets/img/avatar/default.png",is_staff:false},tag:3,meta:{end:2553.277,dip1:-0.17978291666666657,dip2:-0.7176719166666665,start:2551.102,height:0.5354580833333333,length:2.1386480000000003,air_time:0.626783424575357},score:null,data:{id:1,org:1,activity_record:1,data:[]}},
            listData: [{id:1}],
            index: 1
        };
        const enzymeWrapper = shallow(<ActivityRecordDetailLists {...props}/>);
        expect(enzymeWrapper.find(View).length).toBe(1);
    });

    it('renders correctly without an activity', () => {
        const props = {
            activityRecord: {},
            listData: [{id:1}],
            index: 1
        };
        const enzymeWrapper = shallow(<ActivityRecordDetailLists {...props}/>);
        expect(enzymeWrapper.find(View).length).toBe(1);
    });

    it('renders correctly without listData', () => {
        const props = {
            activityRecord: {},
            listData: [],
            index: 1
        };
        const enzymeWrapper = shallow(<ActivityRecordDetailLists {...props}/>);
        expect(enzymeWrapper.find(View).length).toBe(1);
    });
});
