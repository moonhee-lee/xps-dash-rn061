import React from 'react';
import { shallow } from 'enzyme';

import ActivitySummaryListItem from '../../react/components/ActivitySummaryListItem';
import { View } from 'react-native';

describe('ActivitySummaryListItem Component', () => {

    it('renders grey row correctly on load', () => {
        const props = {
            activitySummary: {id:1,org:1,activity:13,user:{id:2,username:'user1',email:'',full_name:'Michael Kedda',first_name:'Michael',last_name:'Kedda',bio:'',is_staff:false},num_records:12,top_record:{id:1,org:1,activity:13,user:2,tag:3,meta:{end:2553.277,dip1:-0.17978291666666657,dip2:-0.7176719166666665,start:2551.102,height:0.5354580833333333,length:2.1386480000000003,air_time:0.626783424575357},score:null},type_definition: {activity_type:'sprint'}},
            session:{id:1,team:{name:'Team 1'}},
            index: 1,
            theme:'grey'
        };
        const enzymeWrapper = shallow(<ActivitySummaryListItem {...props}/>);
        expect(enzymeWrapper.find(View).length).toBe(6);
    });


    it('renders grey row correctly on load without an activity', () => {
        const props = {
            activitySummary: {},
            session:{id:1,team:{name:'Team 1'}},
            index: 1,
            theme:'grey'
        };
        const enzymeWrapper = shallow(<ActivitySummaryListItem {...props}/>);
        expect(enzymeWrapper.find(View).length).toBe(1);
    });

    it('renders white row correctly on load', () => {
        const props = {
            activitySummary: {id:1,org:1,activity:13,user:{id:2,username:'user1',email:'',full_name:'Michael Kedda',first_name:'Michael',last_name:'Kedda',bio:'',is_staff:false},num_records:12,top_record:{id:1,org:1,activity:13,user:2,tag:3,meta:{end:2553.277,dip1:-0.17978291666666657,dip2:-0.7176719166666665,start:2551.102,height:0.5354580833333333,length:2.1386480000000003,air_time:0.626783424575357},score:null},type_definition: {activity_type:'sprint'}},
            session:{id:1,team:{name:'Team 1'}},
            index: 1,
            theme:''
        };
        const enzymeWrapper = shallow(<ActivitySummaryListItem {...props}/>);
        expect(enzymeWrapper.find(View).length).toBe(6);
    });

    it('renders white row correctly on load without an activity', () => {
        const props = {
            activitySummary: {},
            session:{id:1,team:{name:'Team 1'}},
            index: 1,
            theme:''
        };
        const enzymeWrapper = shallow(<ActivitySummaryListItem {...props}/>);
        expect(enzymeWrapper.find(View).length).toBe(1);
    });

});
