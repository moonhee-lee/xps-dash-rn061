import React from 'react';
import { shallow } from 'enzyme';

import ActivityReadyBar  from '../../react/components/ActivityReadyBar';
import { View } from 'react-native';

describe('ActivityReadyBar Component', () => {

    it('renders grey row correctly on load', () => {
        const props = {
            toggleRecordingState:() => {},
            onPressAthleteSelect:() => {},
            onPressActivitySetup:() => {},
            onPressHomeButton:() => {},
            isRecording:false,
            recordReady:false
        };
        const enzymeWrapper = shallow(<ActivityReadyBar {...props}/>);
        expect(enzymeWrapper.find(View).length).toBe(2);
    });

    it('renders grey row correctly on load when recording', () => {
        const props = {
            toggleRecordingState:() => {},
            onPressAthleteSelect:() => {},
            onPressActivitySetup:() => {},
            onPressHomeButton:() => {},
            isRecording:true,
            recordReady:true
        };
        const enzymeWrapper = shallow(<ActivityReadyBar {...props}/>);
        expect(enzymeWrapper.find(View).length).toBe(2);
    });

    it('renders grey row correctly when not recording but is recordReady', () => {
        const props = {
            toggleRecordingState:() => {},
            onPressAthleteSelect:() => {},
            onPressActivitySetup:() => {},
            onPressHomeButton:() => {},
            isRecording:false,
            recordReady:true
        };
        const enzymeWrapper = shallow(<ActivityReadyBar {...props}/>);
        expect(enzymeWrapper.find(View).length).toBe(2);
    });

});
