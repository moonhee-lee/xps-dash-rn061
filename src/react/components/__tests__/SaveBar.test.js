import React from 'react';
import { shallow } from 'enzyme';

import SaveBar  from '../../react/components/SaveBar';
import { View } from 'react-native';

describe('SaveBar Component', () => {

    it('renders correctly on load', () => {
        const props = {
            onPressSave:() => {},
        };
        const enzymeWrapper = shallow(<SaveBar {...props}/>);
        expect(enzymeWrapper.find(View).length).toBe(1);
    });

});
