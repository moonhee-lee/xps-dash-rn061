import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    ListItem,
    View,
    Thumbnail,
    Text,
} from 'native-base';


class ActivityReviewItem extends Component {
    render() {
        const { activityUser, currentActivity } = this.props;
        const { meta_data } = activityUser;

        if (currentActivity.activity_type === 'sprint') {
            return (
                <View>
                    <Text>
                        {activityUser.user.full_name}
                    </Text>

                    {!!meta_data && 
                        <Text>
                            {meta_data.finish_time} sec
                        </Text>
                    }
                </View>
            )            
        } else if (currentActivity.activity_type === 'jump') {
            const { height, length } = meta_data;
            const jumpMeasurement = height > length ? height : length;
            return (
                <View>
                    <Text>
                        {activityUser.user.full_name}
                    </Text>

                    {!!meta_data && 
                        <Text>
                            {jumpMeasurement} m
                        </Text>
                    }
                </View>
            )
        }
    }
}

export default ActivityReviewItem;