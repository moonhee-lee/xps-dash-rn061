    import React, { Component } from 'react';

import { View, TouchableOpacity } from 'react-native';
import {H3, Badge, Text, Icon, Content} from 'native-base';

import styles from '../../theme';

class SessionListItem extends Component {

    render() {
        const { team, index, onPressTeam, currentTeam } = this.props;
        const bgColor = index%2?'#ffffff':'#f7f7f7';

        if (!team || !team.name) return (
            <View></View>
        )

        return (
            <TouchableOpacity style={(!!currentTeam && currentTeam===team.id)?{backgroundColor:'#6AC893'}:{backgroundColor:bgColor}} onPress={onPressTeam}>
                <View style={styles.sessionList}>
                    <View style={{width:'100%',flex: 1,flexDirection: 'row'}}>
                        <Text style={[{flex:10},styles.title,(!!currentTeam && currentTeam===team.id)?{color:'#fff'}:{}]}>{team.name}</Text>
                        {!!currentTeam && currentTeam === team.id &&
                        <Icon type="FontAwesome" name="check" style={{flex:1,alignSelf:"flex-end",color:"#fff"}} />
                        }
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}

export default SessionListItem;
