import React, { Component } from 'react';
import {TouchableOpacity, View} from 'react-native';
import { Text, Icon } from 'native-base';
import styles from '../../theme';

class LiveRecordBar extends Component {

    constructor(props) {
        super(props);
        this.state = {syncRan:false};
    }

    handleSprintBeepStart() {
        const { handleSprintBeepStart } = this.props;
        handleSprintBeepStart();
    }

    toggleRecordingState() {
        const { toggleRecordingState } = this.props;
        toggleRecordingState();
    }

    render () {
        const {
            onPressSave, message, isVideoActive, toggleRecordingState, isRecording, recordReady,
            isBeepSprint, handleSprintBeepStart, isBeepStart, syncRunning, timingValid, modeSelect
        } = this.props;
        const { syncRan } = this.state;

        if (modeSelect==='waiting') {
            return (
                <View style={[styles.viewRowNoPadding,styles.liveRecordBar]}>
                    <View style={{flex: 3, backgroundColor:'#808080'}}>
                        <TouchableOpacity style={[styles.barWrapper,{backgroundColor:'#808080'}]}>
                            <Text style={styles.doneBarActiveText}>
                                <Icon name="clock-o" type="FontAwesome" style={{color: "#fff", fontSize: 20}}
                                      containerStyle={{paddingRight: 20}}/>
                                {'  '}{!!message ? message : 'Setting Recording Mode...'}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        }
        // if we're not ready to record.. we just disable the button.
        if (!recordReady) {
            return (
                <View style={[styles.viewRowNoPadding,styles.liveRecordBar]}>
                    <View style={{flex: 3, backgroundColor:'#808080'}}>
                        <TouchableOpacity style={[styles.barWrapper,{backgroundColor:'#808080'}]}>
                            <Text style={styles.doneBarActiveText}>
                                <Icon name="clock-o" type="FontAwesome" style={{color: "#fff", fontSize: 20}}
                                      containerStyle={{paddingRight: 20}}/>
                                {'  '}{!!message ? message : 'Calibrating...'}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        }

        // if this is a time based sprint..
        if (isBeepSprint) {

            if (syncRunning) {
                return (
                    <View style={[styles.viewRowNoPadding,styles.liveRecordBar]}>
                        <View style={{flex: 3}}>
                            <TouchableOpacity style={[styles.barWrapper,{backgroundColor:'#FF8203'}]}>
                                <Text style={styles.doneBarActiveText}>
                                    <Icon name="clock-o" type="FontAwesome" style={{color: "#fff", fontSize: 20}}
                                          containerStyle={{paddingRight: 20}}/>
                                    {'  '}{!!message ? message : 'Syncing with XPS hub'}
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                )
            }

            // timing valid means the time sync is good to go.
            if (timingValid) {

                // are we in "recording" mode? no means we can show the "ready button."
                if (!isRecording) {
                    return (
                        <View style={[styles.viewRowNoPadding,styles.liveRecordBar]}>
                            <View style={{flex: 3}}>
                                <TouchableOpacity style={[styles.barWrapper]} onPress={() => this.toggleRecordingState()}>
                                    <Text style={styles.doneBarActiveText}>
                                        <Icon name="arrow-circle-right" type="FontAwesome" style={{color: "#fff", fontSize: 20}}
                                              containerStyle={{paddingRight: 20}}/>
                                        {'  '}{!!message ? message : 'Ready'}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    )
                }

                // if we ARE in recording mode, that's basically an ARMED mode.. ready to start the beep and race
                // but only if the isBeepStart isn't set, which means we havent't played the sound yet!
                if (!isBeepStart) {
                    return (
                        <View>
                            <View style={[styles.viewRowNoPadding,styles.liveCancelBar]}>
                                <View style={{flex: 3}}>
                                    <TouchableOpacity style={[styles.barWrapper,{backgroundColor:'#d7d7d7'}]} onPress={() => this.toggleRecordingState()}>
                                        <Text style={[styles.doneBarActiveText,{color:'#222'}]}>
                                            <Icon name="check" type="FontAwesome" style={{color: "#222", fontSize: 20}}
                                                  containerStyle={{paddingRight: 20}}/>
                                            {'  '}{!!message ? message : 'Cancel'}
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={[styles.viewRowNoPadding,styles.liveRecordBar]}>
                                <View style={{flex: 3}}>
                                    <TouchableOpacity style={[styles.barWrapper,{backgroundColor:'#bbffbb'}]} onPress={() => this.handleSprintBeepStart()}>
                                        <Text style={[styles.doneBarActiveText,{color:'#222'}]}>
                                            <Icon name="check" type="FontAwesome" style={{color: "#222", fontSize: 20}}
                                                  containerStyle={{paddingRight: 20}}/>
                                            {'  '}{!!message ? message : 'Start'}
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    )
                }

                // otherwise, we are in race mode..
                return (
                    <View style={[styles.viewRowNoPadding,styles.liveRecordBar]}>
                        <View style={{flex: 3, backgroundColor: '#eb5a5a'}}>
                            <TouchableOpacity style={styles.barWrapper} onPress={() => this.toggleRecordingState()}>
                                <Text style={styles.doneBarActiveText}>
                                    <Icon name="square" type="FontAwesome" style={{color: "#fff", fontSize: 20}}
                                          containerStyle={{paddingRight: 20}}/>
                                    {'  '}{!!message ? message : 'Stop'}
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                )
            }

        }

        // if not in beepsprint, things are much simpler.
        if (!isRecording) {
            return (
                <View style={[styles.viewRowNoPadding,styles.liveRecordBar]}>
                    <View style={{flex: 3}}>
                        <TouchableOpacity style={styles.barWrapper} onPress={() => this.toggleRecordingState()}>
                            <Text style={styles.doneBarActiveText}>
                                <Icon name="check" type="FontAwesome" style={{color: "#fff", fontSize: 20, lineHeight:16}}
                                      containerStyle={{paddingRight: 20}}/>
                                {'  '}{!!message ? message : 'Start'}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        }

        return (
            <View style={[styles.viewRowNoPadding,styles.liveRecordBar]}>
                <View style={{flex: 4, backgroundColor: '#eb5a5a'}}>
                    <TouchableOpacity style={styles.barWrapper} onPress={() => this.toggleRecordingState()}>
                        <Text style={styles.doneBarActiveText}>
                            <Icon name="square" type="FontAwesome" style={{color: "#fff", fontSize: 20}}
                                  containerStyle={{paddingRight: 20}}/>
                            {'  '}{!!message ? message : 'Stop'}
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

export default LiveRecordBar;
