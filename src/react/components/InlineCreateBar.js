import React, { Component } from 'react';
import {TouchableOpacity, View} from 'react-native';
import {Text,Icon} from 'native-base';
import styles from '../../theme';

class InlineCreateBar extends Component {
    render () {
        const { onPressCreate, message, style } = this.props;

        let tmpStyle = {};
        if(style) tmpStyle = style;

        return (
            <View style={[styles.createBar,tmpStyle]}>
                <TouchableOpacity style={styles.barWrapper} onPress={onPressCreate}>
                    <Text style={styles.createBarText}>
                        <Icon name="plus-circle" type="FontAwesome" style={{color:"#fff",fontSize:16}} />
                        {"  "}{message}
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }
}

export default InlineCreateBar;
