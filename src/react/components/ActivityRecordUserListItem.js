import React, { Component } from 'react';

import moment from "moment";
import {Dimensions, StyleSheet, TouchableOpacity, View} from 'react-native';
import { H3, Badge, Text, Icon } from 'native-base';
import styles from '../../theme';
import styleVars from '../../theme/variables/common.js'
import { UserAvatar } from '../../react/components/UserAvatar';

import * as formatUtils from '../../lib/format_utils';

const dimensionWindow = Dimensions.get('window');
const width = Math.round(dimensionWindow.width);

class ActivityRecordUserListItem extends Component {

    render() {
        const { item, navigator, user, activity } = this.props;
        return (
            <View style={{
                width: width,paddingHorizontal: 24,paddingVertical: 8, borderBottomColor:styleVars.grayLighter,borderBottomWidth:1, backgroundColor:styleVars.grayLighter
            }}>
                <View style={[styles.viewRow]}>
                    <View style={{flex:3, elevation:1,justifyContent:'center'}}>
                        <Text style={{fontSize: 16, fontWeight: '600'}}>{user.full_name}</Text>
                    </View>
                    <View style={{flex:2, alignItems: 'flex-end', elevation:1}}>
                        <View style={{flex: 1,flexDirection: 'column'}}>
                            <Text style={{
                                fontSize: 16,
                                fontWeight: '600',
                            }}>{formatUtils.formatDateTime(activity.created)}</Text>
                            <Text style={{fontSize: 14, textAlign: 'right'}}>ID:{activity.id}</Text>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}
export default ActivityRecordUserListItem;
