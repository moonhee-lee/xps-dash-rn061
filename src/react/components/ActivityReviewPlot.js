import React, { Component } from 'react';
import _ from 'lodash';
import {
    ART,
    Dimensions,
} from 'react-native';
import {
    View,
    Text, Content,
} from 'native-base';
import * as graphUtils from '../../lib/graph_utils';
import { getTimeRangedMessages } from '../../redux/local_storage';
import { LineChart, Grid } from 'react-native-svg-charts'
import * as shape from 'd3-shape'

const dimensionWindow = Dimensions.get('window');
const width = Math.round(dimensionWindow.width);
const height = width*.6;

class ActivityReviewPlot extends Component {

    render() {
        const { activityUsers, currentActivity, colors } = this.props;

        if (!activityUsers || _.size(activityUsers) === 0) {
            return (
                <View></View>
            )
        }

        let lineGraphs = [];
        let graphDataSet = [];

        _.each(activityUsers, (activityUser) => {
            // let's trim the data before plotting.
            const toGraphESTData = getTimeRangedMessages(activityUser, false);

            if (toGraphESTData.length > 0) {
                const graphData = graphUtils.getLineGraphData({
                    data: _.values(toGraphESTData),
                    meta: null,
                    width: width,
                    height: height,
                    activityType: currentActivity.activity_type,
                });
                graphDataSet.push(graphData);
            }
        });

        if (graphDataSet.length === 0) {
            return (
                <View></View>
            )
        }

        return (
            <View style={{ height: height, width: width }}>
                {!!graphDataSet && graphDataSet.length && graphDataSet.map( (lineGraph,index) =>
                    <LineChart
                        key={index}
                        style={{ height: height, width: width, position:'absolute' }}
                        data={ lineGraph.distance }
                        svg={{ stroke: colors[index] }}
                        contentInset={{ top: 20, bottom: 20 }}
                        curve={shape.curveNatural}
                    >
                        {index == 0 &&
                        <Grid/>
                        }
                    </LineChart>
                )}
            </View>
        )
    }

}

export default ActivityReviewPlot;
