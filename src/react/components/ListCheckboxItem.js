import React, { Component } from 'react';

import {CheckBox, ListItem, Body, Text} from 'native-base';
import { UserAvatar } from '../../react/components/UserAvatar';

class ListCheckboxItem extends Component {
    constructor(props) {
        super(props);
    }

    handlePress = () => {
        const { handlePressCallback, item, isChecked } = this.props;
        handlePressCallback(item, !isChecked);
    }

    render() {
        const { id, label, isChecked, index, handleLongPressCallback, showAvatar } = this.props;
        const bgColor = index%2?'#ffffff':'#f7f7f7';

        return (
            <ListItem noIndent key={id} onPress={this.handlePress} style={{
                paddingTop:10,
                paddingBottom:10,
                paddingRight:20,
                paddingLeft:20,
                backgroundColor:bgColor
            }} onLongPress={handleLongPressCallback}>
                {!!showAvatar &&
                <Body style={{flex: 1,flexDirection: 'row'}}>
                    <UserAvatar size={45} user={showAvatar} />
                    <Text style={{lineHeight:47}}>{label}</Text>
                </Body>
                }
                {!showAvatar &&
                <Body>
                    <Text>{label}</Text>
                </Body>
                }
                <CheckBox
                    onPress={this.handlePress}
                    checked={isChecked}
                />
            </ListItem>
        )
    }
}

export default ListCheckboxItem;
