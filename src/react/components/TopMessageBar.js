import React, { Component } from 'react';
import {View} from 'react-native';
import {Text,Icon} from 'native-base';
import styles from '../../theme';

class TopMessageBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showComponent: true
        };
    }

    componentDidMount() {
        const { hideAfterMS } = this.props;
        if (hideAfterMS > 0) {
            setTimeout(() => {
                this.setState({showComponent: false})
            }, hideAfterMS);
        }
    }

    render () {
        const { onPressReload, message, type, onPressSettings } = this.props;
        const { showComponent } = this.state;

        let bgColor = '#ccc';
        switch(type) {
            case 'error':
                bgColor = '#eb5a5a';
                break;
            case 'success':
                bgColor = '#6AC893';
                break;
            case 'working':
                bgColor = '#3b3c3c';
                break;
        }

        if (!showComponent) return null;

        return (
            <View style={[styles.messageBar,{backgroundColor:bgColor}]}>
                    <Text style={styles.messageBarText}>
                        {type === 'success' &&
                        <Icon name="check" type="FontAwesome" style={{color: "#fff", fontSize: 16, lineHeight:40}}/>
                        }
                        {type === 'error' &&
                        <Icon name="exclamation-triangle" type="FontAwesome" style={{color: "#fff", fontSize: 16, lineHeight:40}}/>
                        }
                        {"          "}{message}{"     "}
                        {!!onPressReload &&
                        <Icon name="refresh" type="FontAwesome" style={{color: "#fff", fontSize: 20, lineHeight:40}}
                              onPress={() => onPressReload()}/>
                        }
                        {!!onPressSettings && type === 'error' &&
                        "     "
                        }
                        {!!onPressSettings && type === 'error' &&
                            <Icon name="cog" type="FontAwesome" style={{color: "#fff", fontSize: 20, lineHeight:40}}
                            onPress={onPressSettings}/>
                        }
                        {!!global.debugMode &&
                        "     "
                        }
                        {!!global.debugMode &&
                        <Icon name="bug" type="FontAwesome" style={{color: "#fff", fontSize: 20, lineHeight:40}}/>
                        }
                    </Text>
            </View>
        )
    }
}

export default TopMessageBar;
