import React from 'react';

import {View, ScrollView, Dimensions} from 'react-native'
import {BarChart, Grid} from 'react-native-svg-charts'
import {Text} from 'react-native-svg'
import _ from 'lodash';
import {formatNumber} from '../../lib/format_utils';

class ActivityRecordStrideChart extends React.PureComponent {
    constructor(props) {
        super(props);
    }

    render() {
        const {frequencies, lengths, times, sides, typeDefinition} = this.props;
        const data = lengths.map((value) => {
            return value * 100;
        });

        const CUT_OFF_LENGTH = 40;

        const maxStrideLength = _.max(data);
        const barChartHeight = 280;
        let screenWith = Math.round(Dimensions.get('window').width);
        let barChartWidth = frequencies.length * 58;
        let insetLeftRight = 10;
        if (barChartWidth < screenWith) {
            insetLeftRight = (screenWith - barChartWidth - insetLeftRight * 2) / 2;
            barChartWidth = screenWith;
        }

        const Lengths = ({x, y, bandwidth, data}) => (
            data.map((value, index) => {
                let height = value / maxStrideLength * barChartHeight;
                let length = formatNumber(value / 100);
                return <Text
                    key={index}
                    x={x(index) + (bandwidth / 2)}
                    y={height < CUT_OFF_LENGTH ? y(value) - 10 : y(value) + 15}
                    fontSize={14}
                    fill={height >= CUT_OFF_LENGTH ? 'white' : 'black'}
                    alignmentBaseline={'middle'}
                    textAnchor={'middle'}
                >
                    {index === 0 ? length + 'm' : length}
                </Text>
            })
        );

        const Strides = ({x, y, bandwidth, data}) => (
            data.map((value, index) => (
                <Text
                    key={index}
                    x={x(index) + (index === 0 ? 32 : (bandwidth / 2))}
                    y={y(0) + 15}
                    fontSize={14}
                    fill={'black'}
                    alignmentBaseline={'middle'}
                    textAnchor={'middle'}
                >
                    {index === 0 ? 'Stride 1' : '' + (index + 1)}
                </Text>
            ))
        );

        const Times = ({x, y, bandwidth, data}) => (
            data.map((value, index) => (
                <Text
                    key={index}
                    x={x(index) + (index === 0 ? 32 : (bandwidth / 2))}
                    y={y(0) + 30}
                    fontSize={14}
                    fill={'black'}
                    alignmentBaseline={'middle'}
                    textAnchor={'middle'}
                >
                    {(index === 0 ? 'Cont ' : '') + times[index]}
                </Text>
            ))
        );

        const Sides = ({x, y, bandwidth, data}) => (
            data.map((value, index) => (
                <Text
                    key={index}
                    x={x(index) + (index === 0 ? 32 : (bandwidth / 2))}
                    y={y(0) + 45}
                    fontSize={14}
                    fill={'black'}
                    alignmentBaseline={'middle'}
                    textAnchor={'middle'}
                >
                    {(index === 0 ? 'Side ' : '') + sides[index]}
                </Text>
            ))
        );

        return (
            <View noIndent style={{flex: 1}}>
                <ScrollView horizontal={true} contentContainerStyle={{flexGrow: 1, justifyContent: 'center'}}>
                    <View style={{
                        flexDirection: 'column',
                        height: barChartHeight,
                        width: barChartWidth,
                        paddingVertical: 16
                    }}>
                        <BarChart
                            style={{flex: 1}}
                            data={data}
                            svg={{fill: 'rgba(134, 65, 244, 0.8)'}}
                            contentInset={{top: 10, bottom: 60, left: insetLeftRight, right: insetLeftRight}}
                            spacing={0.2}
                            gridMin={0}
                        >
                            <Lengths/>
                            <Strides/>
                            <Times />
                            {!(typeDefinition.side && (typeDefinition.side === 'left' || typeDefinition.side === 'right')) &&
                            <Sides/>
                            }
                        </BarChart>
                    </View>
                </ScrollView>
            </View>
        )
    }

}

export default ActivityRecordStrideChart;
