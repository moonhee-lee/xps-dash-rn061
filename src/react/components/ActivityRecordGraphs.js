import React, { Component } from 'react';

import { View, Dimensions } from 'react-native';
import ActivityRecordGraph from '../../react/components/ActivityRecordGraph';

const dimensionWindow = Dimensions.get('window');
const width = Math.round(dimensionWindow.width);
//const height = width*.6;

import * as colorUtils from '../../lib/color_utils';
import { Text } from "native-base";
import Carousel, { Pagination } from 'react-native-snap-carousel';

class ActivityRecordGraphs extends Component {
    static WIDTH = width;
    state = {
        currentIndex: 0,
        graphData: null
    };

    render() {
        const { graphData, height, offsetTop } = this.props;

        return (
            <View style={{height:height, marginTop:offsetTop}}>
                {graphData &&
                    <ActivityRecordGraph
                        itemData={graphData}
                        height={height}
                    />
                }

            </View>
        )
    }
}

export default ActivityRecordGraphs;
