import React, { Component } from 'react';

import { View } from 'react-native';
import {Badge, Icon, Text} from 'native-base';
import { UserAvatar } from '../../react/components/UserAvatar';

import styles from '../../theme';
import * as formatUtils from '../../lib/format_utils';
import moment from "moment";
import _ from 'lodash';

class ActivityUserRecordListItem extends Component {

    render() {
        const { activityRecord, activityData, theme, session, index, navigator, incomplete } = this.props;

        if(!activityRecord || !activityRecord.user) return (<View></View>)

        const bgColor = index%2?'#ffffff':'#f7f7f7';

        let score;
        let reaction_time;
        let max_velocity;

        const isSynced = !activityRecord.localID;

        if(activityRecord.created) activityRecord.formatted_date = moment(activityRecord.created).format('MM/DD/YY h:mma');

        if(activityRecord.type_definition.activity_type==='freeform' ||
            (activityRecord.type_definition.activity_type==='jump' && activityRecord.type_definition.triple_jump_type)) {
            let length = null;
            if(activityRecord.data) {
                const first = _.first(activityRecord.data.est);
                const last = _.last(activityRecord.data.est);
                length = formatUtils.formatTime((last.time - first.time) / 1000);
            }
            if(activityData && activityData[0]) {
                const first = _.first(activityData[0].est);
                const last = _.last(activityData[0].est);
                length = formatUtils.formatTime((last.time - first.time) / 1000);
            }

            return (
                <View style={{width:'100%'}}>
                    <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        paddingTop: 20,
                        paddingBottom: 20,
                        borderBottomColor: '#E7E7E7',
                        borderBottomWidth: 1,
                        backgroundColor:bgColor,
                        width:'100%',
                    }}>
                        <Text style={[styles.motionListItem1,{fontSize:20,fontWeight:'bold'}]}>
                            {activityRecord.user.full_name}{"\n"}
                            <Text style={{fontSize:12,color:"#999999"}}>{activityRecord.formatted_date}</Text>
                        </Text>
                        {!!length &&
                        <Text style={[styles.jumpListItem2, {fontSize: 20, fontWeight: 'bold'}]}>{length}</Text>
                        }
                        {isSynced &&
                        <View style={{paddingRight: 20, flexDirection: 'column'}}>
                            <Icon type="FontAwesome" name="cloud" style={{alignSelf:"center",fontSize:18,color:"#6AC893"}} />
                            <Text style={{fontSize: 14, textAlign: 'right'}}>ID:{activityRecord.id}</Text>
                        </View>
                        }
                        {!isSynced &&
                        <Icon type="FontAwesome" name="cloud-upload" style={[styles.jumpListItem3,{paddingBottom:10,fontSize:18,alignSelf:"flex-end",color:"#d7d7d7"}]} />
                        }
                    </View>
                </View>
            )
        } else if(activityRecord.type_definition.activity_type==='jump') {
            score = formatUtils.formatDistance(activityRecord.score);

            return (
                <View style={{width:'100%'}}>
                    <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        paddingTop: 20,
                        paddingBottom: 20,
                        borderBottomColor: '#E7E7E7',
                        borderBottomWidth: 1,
                        backgroundColor:bgColor,
                        width:'100%',
                    }}>
                        <Text style={[styles.motionListItem1,{fontSize:20,fontWeight:'bold'}]}>
                            {activityRecord.user.full_name}{"\n"}
                            <Text style={{fontSize:12,color:"#999999"}}>{activityRecord.formatted_date}</Text>
                        </Text>
                        {!incomplete &&
                        <Text style={[styles.jumpListItem2, {fontSize: 20, fontWeight: 'bold'}]}>{score}</Text>
                        }
                        {incomplete &&
                        <Text style={[{fontSize: 15, color:'#666666', paddingTop:10}]}>Not Completed</Text>
                        }
                        {isSynced &&
                        <View style={{paddingRight: 20, flexDirection: 'column'}}>
                            <Icon type="FontAwesome" name="cloud" style={{alignSelf:"center",fontSize:18,color:"#6AC893"}} />
                            <Text style={{fontSize: 14, textAlign: 'right'}}>ID:{activityRecord.id}</Text>
                        </View>
                        }
                        {!isSynced &&
                        <Icon type="FontAwesome" name="cloud-upload" style={[styles.jumpListItem3,{fontSize:18,alignSelf:"flex-end",color:"#d7d7d7"}]} />
                        }
                    </View>
                </View>
            )
        } else {
            score = formatUtils.formatTime(activityRecord.score);
            max_velocity = formatUtils.formatSpeed(activityRecord.data_summary.max_velocity);

            if (activityRecord.type_definition.start_type === 'beep') {
                if (activityRecord.data_summary.reaction_time !== undefined &&
                    activityRecord.data_summary.reaction_time !== null) {

                    reaction_time = formatUtils.formatTime(activityRecord.data_summary.reaction_time);
                }

                return (
                    <View style={{width:'100%'}}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            paddingTop: 20,
                            paddingBottom: 20,
                            borderBottomColor: '#E7E7E7',
                            borderBottomWidth: 1,
                            backgroundColor:bgColor,
                            width:'100%',
                        }}>
                            <Text style={[styles.motionListItem1,{fontSize:20,fontWeight:'bold'}]}>
                                {activityRecord.user.full_name}{"\n"}
                                <Text style={{fontSize:12,color:"#999999"}}>{activityRecord.formatted_date}</Text>
                            </Text>
                            {!incomplete &&
                            <Text style={styles.beepListItem2}>{reaction_time}</Text>
                            }
                            {!incomplete &&
                                <Text style={styles.beepListItem3}>{max_velocity}</Text>
                            }
                            {!incomplete &&
                                <Text style={[styles.beepListItem4, {fontSize: 20, fontWeight: 'bold'}]}>{score}</Text>
                            }
                            {incomplete &&
                            <Text style={[{fontSize: 15, color:'#666666', paddingTop:10}]}>Not Completed</Text>
                            }
                            {isSynced &&
                            <View style={{paddingRight: 20, flexDirection: 'column'}}>
                                <Icon type="FontAwesome" name="cloud" style={{alignSelf:"center",fontSize:18,color:"#6AC893"}} />
                                <Text style={{fontSize: 14, textAlign: 'right'}}>ID:{activityRecord.id}</Text>
                            </View>
                            }
                            {!isSynced &&
                            <Icon type="FontAwesome" name="cloud-upload" style={[styles.beepListItem5,{fontSize:18,alignSelf:"flex-end",color:"#d7d7d7"}]} />
                            }
                        </View>
                    </View>
                )
            } else {
                return (
                    <View style={{width:'100%'}}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            paddingTop: 20,
                            paddingBottom: 20,
                            borderBottomColor: '#E7E7E7',
                            borderBottomWidth: 1,
                            backgroundColor:bgColor,
                            width:'100%',
                        }}>
                            <Text style={[styles.motionListItem1,{fontSize:20,fontWeight:'bold'}]}>
                                {activityRecord.user.full_name}{"\n"}
                                <Text style={{fontSize:12,color:"#999999"}}>{activityRecord.formatted_date}</Text>
                            </Text>
                            {!incomplete &&
                            <Text style={styles.motionListItem2}>{max_velocity}</Text>
                            }
                            {!incomplete &&
                            <Text style={[styles.motionListItem3, {fontSize: 20, fontWeight: 'bold'}]}>{score}</Text>
                            }
                            {incomplete &&
                            <Text style={[{fontSize: 15, color:'#666666', paddingTop:10}]}>Not Completed</Text>
                            }
                            {isSynced &&
                            <View style={{paddingRight: 20, flexDirection: 'column'}}>
                                <Icon type="FontAwesome" name="cloud" style={{alignSelf:"center",fontSize:18,color:"#6AC893"}} />
                                <Text style={{fontSize: 14, textAlign: 'right'}}>ID:{activityRecord.id}</Text>
                            </View>
                            }
                            {!isSynced &&
                            <Icon type="FontAwesome" name="cloud-upload" style={[styles.motionListItem4,{fontSize:18,alignSelf:"flex-end",color:"#d7d7d7"}]} />
                            }
                        </View>
                    </View>
                )
            }
        }

    }
}

export default ActivityUserRecordListItem
