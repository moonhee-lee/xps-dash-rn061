import React, { Component } from 'react';
import {Animated, Easing, TouchableOpacity, View, PanResponder} from 'react-native';
import {Text, Icon, Item, Button} from 'native-base';
import styles from '../../theme';
import { Input } from "react-native-elements";
import validate from "validate.js";

import { saveOrgMember } from '../../redux/actions/org_member';
import { connect } from 'react-redux';

class AthleteCreateBar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name:'',
            panelAnim: new Animated.Value(-70),
            panelActive: false,
            moving:false,
            startMoveY: 0,
            lastMoveY: 0
        }
        this.openPanel = this.openPanel.bind(this);
        this.closePanel = this.closePanel.bind(this);
        this.togglePanel = this.togglePanel.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        this._panResponder = PanResponder.create({
            // Ask to be the responder:
            onStartShouldSetPanResponder: (evt, gestureState) => true,
            onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
            onMoveShouldSetPanResponder: (evt, gestureState) => true,
            onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,

            onPanResponderGrant: (evt, gestureState) => {
                // The gesture has started. Show visual feedback so the user knows
                // what is happening!

                // gestureState.d{x,y} will be set to zero now
                this.setState({startMoveY:gestureState.moveY});
            },
            onPanResponderMove: (evt, gestureState) => {
                // The most recent move distance is gestureState.move{X,Y}

                // The accumulated gesture distance since becoming responder is
                // gestureState.d{x,y}
                if(this.state.panelActive) {
                    if(gestureState.moveY-this.state.startMoveY<=50) {
                        this.setState({lastMoveY:gestureState.moveY,moving:true});
                        this.closePanel();
                    } else {
                        this.setState({lastMoveY:gestureState.moveY});
                    }
                } else {
                    if(gestureState.moveY-this.state.startMoveY>=100) {
                        this.setState({lastMoveY:gestureState.moveY,moving:true});
                        this.openPanel();
                    } else {
                        this.setState({lastMoveY:gestureState.moveY});
                    }
                }
            },
            onPanResponderTerminationRequest: (evt, gestureState) => true,
            onPanResponderRelease: (evt, gestureState) => {
                // The user has released all touches while this view is the
                // responder. This typically means a gesture has succeeded
                if(!this.state.moving && gestureState.moveY===this.state.lastMoveY) this.togglePanel();
                this.setState({startMoveY:0,moving:false});
            },
            onPanResponderTerminate: (evt, gestureState) => {
                // Another component has become the responder, so this gesture
                // should be cancelled
                this.setState({startMoveY:0});
            },
            onShouldBlockNativeResponder: (evt, gestureState) => {
                // Returns whether this component should block native components from becoming the JS
                // responder. Returns true by default. Is currently only supported on android.
                return true;
            },
        });
    }


    togglePanel() {
        if(this.state.panelActive) this.closePanel();
        else this.openPanel();
    }

    openPanel() {
        Animated.timing(
            this.state.panelAnim,
            {
                toValue: 0,
                duration: 300,
                easing: Easing.inOut(Easing.quad),
                useNativeDriver: true
            }
        ).start();
        this.setState({panelActive:true});
    }

    closePanel() {
        Animated.timing(
            this.state.panelAnim,
            {
                toValue: -70,
                duration: 300,
                easing: Easing.inOut(Easing.quad),
                useNativeDriver: true
            }
        ).start();
        this.setState({panelActive:false});
    }

    validateName = (name) => {
        return validate.single(name, {
            presence: {
                allowEmpty: false,
                message: 'A name is required'
            }
        });
    }

    handleSubmit = () => {
        const { name } = this.state;
        const { saveOrgMember, toggleAthlete, currentOrg, currentTeam } = this.props;
        const nameError = this.validateName(name);

        this.setState({
            nameError: nameError ? nameError[0] : '',
        });

        if (!nameError) {
            const nameSplit = name.split(' ');
            const first_name = nameSplit.slice(0, 1).join(' ');
            const last_name = nameSplit.slice(1, nameSplit.length).join(' ');

            saveOrgMember({
                user:{
                    first_name: first_name,
                    last_name: last_name
                },
                org: currentOrg.id,
                teams: [currentTeam],
            }, false, toggleAthlete).then(() => {
                this.closePanel();
            }).catch((e) => {
                console.log(e);
            });
        }
    }

    render () {
        const { onPressCreate } = this.props;
        const { panelAnim, opening, name } = this.state;

        return (
            <Animated.View style={[styles.createAthleteBar,{alignItems:'center',transform: [{translateY: panelAnim}]}]}>
                <View style={{flex:1,flexDirection:'row',justifyContent: 'center',alignItems:'center',width:'100%',backgroundColor:'#fefefe',borderBottomWidth:4,borderBottomColor:'#2E9CEB'}}>
                    <Input
                        value={name}
                        onChangeText={(name) => this.setState({ name: name.trim() })}
                        placeholder={"Enter athlete's name"}
                        placeholderTextColor={'#666'}
                        inputStyle={styles.loginInputStyle}
                        containerStyle={[styles.loginContainerStyle,{flex:3}]}
                        textStyle={styles.loginTextStyle}
                    />
                    <Button success style={{height:40,flex:1,justifyContent: 'center',alignItems:'center',marginLeft:10,marginRight:10,marginTop:12}} onPress={this.handleSubmit}>
                        <Icon type="FontAwesome" name="check" style={{alignSelf:'center'}} />
                    </Button>
                    <Button danger style={{height:40,flex:1,justifyContent: 'center',alignItems:'center',marginLeft:10,marginRight:10,marginTop:12}} onPress={() => this.closePanel}>
                        <Icon type="FontAwesome" name="times" style={{alignSelf:'center',paddingLeft:3}} />
                    </Button>
                </View>
                <View {...this._panResponder.panHandlers}
                                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    borderBottomRightRadius:30,borderBottomLeftRadius:30,backgroundColor:'#2E9CEB',width:140,height:30,alignItems:'center'}}>
                    <Icon name="plus" type="FontAwesome" style={{color:'#fff',fontSize:13,paddingRight:10}} />
                    <Text style={{fontSize:13,color:'#fff'}}>Add Athlete</Text>
                </View>
            </Animated.View>
        )
    }
}

export default connect(
    null,
    { saveOrgMember }
)(AthleteCreateBar);

