import React, { Component } from 'react';

import { View , TouchableOpacity } from 'react-native';
import { Text } from 'native-base';
import { UserAvatar } from '../../react/components/UserAvatar';

class AthleteListItem extends Component {

    render() {
        const { athlete, index, onPressAthlete, onLongPressAthlete, navigator } = this.props;

        const bgColor = index%2?'#ffffff':'#f7f7f7';

        return (
            <TouchableOpacity style={{width:'100%'}} onPress={onPressAthlete} onLongPress={onLongPressAthlete}>
                <View style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    paddingTop: 10,
                    paddingBottom: 10,
                    borderBottomColor: '#E7E7E7',
                    borderBottomWidth: 1,
                    backgroundColor:bgColor,
                    width:'100%'
                }}>
                    <View style={{width: '20%', paddingRight: 15,paddingLeft:15}}>
                        <UserAvatar size={65} user={athlete} navigator={navigator} />
                    </View>
                    <View style={{width: '70%',paddingTop:20}}>
                        <Text style={{fontSize: 20}}>{athlete.full_name}</Text>
                    </View>
                </View>

            </TouchableOpacity>
        )
    }
}

export default AthleteListItem
