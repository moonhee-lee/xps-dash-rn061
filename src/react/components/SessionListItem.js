import React, { Component } from 'react';

import { View } from 'react-native';
import {H3, Badge, Text, Icon, Card, CardItem, Content} from 'native-base';

import styles from '../../theme';

class SessionListItem extends Component {

    render() {
        const { session, index } = this.props;
        const bgColor = index%2?'#ffffff':'#f7f7f7';

        const isSynced = !session.localID;

        if (!session || !session.name) return (
            <View></View>
        )

        return (
            <View style={styles.sessionCard}>
                <View style={styles.sessionList}>
                    <View style={{flex:3}}>
                        <Text style={styles.sessionTitle}>{session.name}</Text>
                        {!!session.description &&
                        <Text style={styles.description}>{session.description}</Text>
                        }
                    </View>
                    <View style={{flex:1}}>
                        {!!session.team_data && session.team_data.id &&
                        <Badge style={styles.badge} key={session.team_data.id}>
                            <Text style={styles.badgeText}>{session.team_data.name}</Text>
                        </Badge>
                        }
                    </View>
                    <View style={{flex:1}}>
                        <Icon type="FontAwesome" name="chevron-right" style={[{fontSize:12,alignSelf:"flex-end",color:"#d7d7d7"}]} />
                    </View>

                </View>
            </View>
        )
    }
}

export default SessionListItem;
