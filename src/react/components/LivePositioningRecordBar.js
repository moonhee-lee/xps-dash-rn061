import React, { Component } from 'react';
import {TouchableOpacity, View} from 'react-native';
import { Text, Icon } from 'native-base';
import styles from '../../theme';
import {connect} from "react-redux";
import { setTagsPositions, clearTagsPositions } from '../../redux/actions/tag';
import { activityActiveUsersSelector } from '../../redux/selectors/active_users';
import { getTagPosition } from '../../redux/local_storage';
import * as formatUtils from '../../lib/format_utils';

export const mapStateToProps = (state, ownProps) => {
    return {
        activeUsers: activityActiveUsersSelector(state, ownProps),
    }
};

class LivePositioningRecordBar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isPositioning: false,
        };
    }

    recordPosition(position) {
        const { activeUsers, toggleRecordingState, setTagsPositions, clearTagsPositions, showSaveScreen } = this.props;

        setTagsPositions(activeUsers, position);
        this.setState({
            isPositioning: true
        });
        setTimeout(() => {
            this.setState({
                isPositioning: false
            });
            clearTagsPositions(activeUsers);

            if (showSaveScreen === null) {
                toggleRecordingState();
            } else {
                showSaveScreen();
            }
        }, 1000)
    }

    componentWillUnmount() {
        const { activeUsers, clearTagsPositions } = this.props;
        clearTagsPositions(activeUsers);
    }

    render () {
        const { message, toggleRecordingState, isRecording, recordReady, modeSelect, showSaveScreen } = this.props;
        const { isPositioning } = this.state;

        if (modeSelect==='waiting') {
            return (
                <View style={[styles.viewRowNoPadding,styles.liveRecordBar]}>
                    <View style={{flex: 3, backgroundColor:'#808080'}}>
                        <TouchableOpacity style={[styles.barWrapper,{backgroundColor:'#808080'}]}>
                            <Text style={styles.doneBarActiveText}>
                                <Icon name="clock-o" type="FontAwesome" style={{color: "#fff", fontSize: 20}}
                                      containerStyle={{paddingRight: 20}}/>
                                {'  '}{!!message ? message : 'Setting Recording Mode...'}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        }
        // if we're not ready to record.. we just disable the button.
        if (!recordReady) {
            return (
                <View style={[styles.viewRowNoPadding,styles.liveRecordBar]}>
                    <View style={{flex: 3, backgroundColor:'#808080'}}>
                        <TouchableOpacity style={[styles.barWrapper,{backgroundColor:'#808080'}]}>
                            <Text style={styles.doneBarActiveText}>
                                <Icon name="clock-o" type="FontAwesome" style={{color: "#fff", fontSize: 20}}
                                      containerStyle={{paddingRight: 20}}/>
                                {'  '}{!!message ? message : 'Calibrating...'}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        }

        if (!isRecording) {
            if (isPositioning) {
                return (
                    <View style={[styles.viewRowNoPadding,styles.liveRecordBar]}>
                        <View style={{flex: 3, backgroundColor:'#808080'}}>
                            <TouchableOpacity style={[styles.barWrapper,{backgroundColor:'#808080'}]}>
                                <Text style={styles.doneBarActiveText}>
                                    <Icon name="clock-o" type="FontAwesome" style={{color: "#fff", fontSize: 20}}
                                          containerStyle={{paddingRight: 20}}/>
                                    {'  '}{!!message ? message : 'Positioning...'}
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                )
            }

            if (showSaveScreen === null) {
                return (
                    <View style={[styles.viewRowNoPadding,styles.liveRecordBar]}>
                        <View style={{flex: 3}}>
                            <TouchableOpacity style={styles.barWrapper} onPress={() => this.recordPosition('start')}>
                                <Text style={styles.doneBarActiveText}>
                                    `   <Icon name="check" type="FontAwesome" style={{color: "#fff", fontSize: 20, lineHeight:16}}
                                              containerStyle={{paddingRight: 20}}/>
                                    {'  '}{!!message ? message : 'Measure Start'}
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                )
            } else {
                return (
                    <View style={[styles.viewRowNoPadding,styles.liveRecordBar]}>
                        <View style={{flex: 3}}>
                            <TouchableOpacity style={styles.barWrapper} onPress={() => this.recordPosition('end')}>
                                <Text style={styles.doneBarActiveText}>
                                    `   <Icon name="check" type="FontAwesome" style={{color: "#fff", fontSize: 20, lineHeight:16}}
                                              containerStyle={{paddingRight: 20}}/>
                                    {'  '}{!!message ? message : 'Measure End'}
                                </Text>
                            </TouchableOpacity>
                        </View>
                        }
                    </View>
                )
            }
        }

        return (
            <View style={[styles.viewRowNoPadding,styles.liveRecordBar]}>
                <View style={{flex: 4, backgroundColor: '#eb5a5a'}}>
                    <TouchableOpacity style={styles.barWrapper} onPress={() => toggleRecordingState()}>
                        <Text style={styles.doneBarActiveText}>
                            <Icon name="square" type="FontAwesome" style={{color: "#fff", fontSize: 20}}
                                  containerStyle={{paddingRight: 20}}/>
                            {'  '}{!!message ? message : 'Stop'}
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

export default connect(
    mapStateToProps,
    {
        setTagsPositions,
        clearTagsPositions,
    }
)(LivePositioningRecordBar);
