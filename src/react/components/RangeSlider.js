import React, { Component } from 'react';
import {Text, View} from "native-base";
import {Slider} from "react-native-elements";

import styles from '../../theme';

class RangeSlider extends Component {
    render () {
        const { onPressSave, message, isActive } = this.props;

        return (
            <View>
                <Text style={{width: '100%', textAlign: 'center', color: '#666', fontWeight: 'bold', padding: 10}}>Distance (m)</Text>
                <View style={{flexDirection: 'row',height:80}}>
                    <View style={{position:'absolute',top:60,left:7,width:sliderWidth,height:20,elevation:10}}>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', width: '100%'}}>
                            <Text style={{fontSize:12,color:'#999999'}}>1</Text>
                            <View style={styles.dividerLine}>{ }</View>
                            <Text style={{fontSize:12,color:'#999999'}}>10</Text>
                            <View style={styles.dividerLine}>{ }</View>
                            <Text style={{fontSize:12,color:'#999999'}}>20</Text>
                            <View style={styles.dividerLine}>{ }</View>
                            <Text style={{fontSize:12,color:'#999999'}}>30</Text>
                            <View style={styles.dividerLine}>{ }</View>
                            <Text style={{fontSize:12,color:'#999999'}}>40</Text>
                            <View style={styles.dividerLine}>{ }</View>
                            <Text style={{fontSize:12,color:'#999999'}}>50</Text>
                        </View>
                    </View>
                    <View style={{ flex: 1, alignItems: 'stretch', justifyContent: 'center' }}>
                        <Slider
                            value={this.state.distance}
                            minimumValue={1}
                            maximumValue={50}
                            step={1}
                            onSlidingComplete={distance => this.setState({ distance })}
                            onValueChange={sliderValue => this.setState({ sliderValue })}
                            style={{height:30,backgroundColor:'#2E9CEB',borderRadius:5}}
                            thumbTintColor={"#c6c6c6"}
                            minimumTrackTintColor={'#2E9CEB'}
                            maximumTrackTintColor={'#2E9CEB'}
                            thumbStyle={{height:30,width:40,padding:0,marginTop:-5,borderRadius:0,borderBottomLeftRadius:5,borderBottomRightRadius:5}}
                        />
                        <Text style={{position:'absolute',left:sliderValueLeft,top:0,borderRadius:0,borderTopRightRadius:5,borderTopLeftRadius:5,fontSize:20,width:40,backgroundColor:'#2E9CEB',textAlign:"center",color:'#ffffff'}}>{this.state.sliderValue}</Text>
                    </View>
                </View>
            </View>
        )
    }
}

export default RangeSlider;
