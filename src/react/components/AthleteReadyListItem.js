import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    View,
    Thumbnail,
    Text, Icon
} from 'native-base';
import {Easing, TouchableOpacity, Animated} from 'react-native';
import { setupTag } from '../../redux/actions/tag';
import { UserAvatar } from '../../react/components/UserAvatar';
import styles from '../../theme';
import { getESTVelocity } from '../../redux/local_storage';
import * as Progress from 'react-native-progress';
import { getUnit } from '../../lib/format_utils';

export class AthleteReadyListItem extends Component {
    constructor(props) {
        super(props);
        this.velocityInterval = null;
        this.state = {
            buttonAnim: new Animated.Value(1),
            recording: false
        }
    }

    componentDidMount() {
        // on mount, we check to see if the activityUser has a tag associated
        // and if so, we set that tag up for recording.
        const { currentActivity, activityUser, setupTag } = this.props;
        if(activityUser && activityUser.tag) {
            console.log('setting up tag from athlete ready list item');
            setupTag(activityUser.tag, currentActivity, activityUser);
        }
    }

    componentDidUpdate(prevProps,prevState) {
        const { isRecording, activityUser } = this.props;
        if(!this.velocityInterval && isRecording) {
            this.startAnimation();
            console.log('starting velocity listener')
            this.velocityInterval = setInterval(() => {
                if (activityUser && activityUser.tag && activityUser.tag.name) {
                    const velocity = getESTVelocity(activityUser.tag.name);
                    if (velocity) {
                        this.setState({velocity: velocity});
                    } else {
                        this.setState({velocity:0});
                    }
                }
            },100);
        }
        if(!!this.velocityInterval && !isRecording) {
            console.log('stopping velocity listener')
            clearInterval(this.velocityInterval);
            this.velocityInterval = null;
            this.stopAnimation();
        }
    }

    componentWillUnmount() {
        console.log('stopping velocity listener')
        clearInterval(this.velocityInterval);
        this.velocityInterval = null;
        this.stopAnimation();
    }

    startAnimation() {
        console.log('starting animation');
        Animated.loop(
            Animated.sequence([
                Animated.timing(this.state.buttonAnim, {
                    toValue: .5,
                    duration: 500
                }),
                Animated.timing(this.state.buttonAnim, {
                    toValue: 1,
                    duration: 500,
                }),
            ])
        ).start()
    }

    stopAnimation() {
        console.log('stopping animation');
        Animated.loop(
            Animated.sequence([
                Animated.timing(this.state.buttonAnim, {
                    toValue: .5,
                    duration: 500
                }),
                Animated.timing(this.state.buttonAnim, {
                    toValue: 1,
                    duration: 500,
                }),
            ])
        ).stop()
    }

    render() {
        const { activityUser, currentActivity, navigator, callSetupTag, isRecording } = this.props;

        const { velocity } = this.state;

        if(!activityUser || !currentActivity || !activityUser.user) return (<View></View>)

        const distance = activityUser.distance / currentActivity.distance * 100;

        let unit = getUnit(currentActivity.units);

        const statusColors = {
            'ready':'#27AE60',
            'select_pending':'#F2C94C',
            'selected':'#FF8203',
            'recording':'#2F80ED',
            'unavailable': '#E0E0E0',
        };

        return (
            <View style={styles.viewRowCenter}>
                <View style={{flex:1,alignItems:'center',borderBottomWidth:0,justifyContent: 'center'}} >
                    <UserAvatar size={45} user={activityUser.user} navigator={navigator} />
                </View>
                <View style={{flex:4,height:60,borderBottomWidth:0,elevation:2}}>
                    <Text style={styles.title}>{activityUser.user.full_name}</Text>
                    {!!activityUser.tag &&
                    <Text style={styles.description}>{activityUser.tag.name}
                        {!!activityUser.tag_data &&
                        <Text style={styles.description}> - {activityUser.tag_data.status}</Text>
                        }
                    </Text>
                    }
                    {!activityUser.tag &&
                    <Text style={styles.description}>{ }
                        <Text style={styles.description}>{ }</Text>
                    </Text>
                    }
                    {1 === 2 &&
                    <View style={{elevation: 1, flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
                        {distance &&
                        <View style={{elevation: 1}}>
                            <Text>{parseInt(distance)} % done </Text>
                        </View>
                        }
                        {activityUser.distance &&
                        <View style={{elevation: 1}}>
                            <Text>{parseFloat(activityUser.distance).toFixed(2)}{unit} / {currentActivity.distance}{unit}</Text>
                        </View>
                        }
                    </View>
                    }
                    {activityUser.score && currentActivity.activity_type==='jump' &&
                    <View style={{position:"absolute",bottom:5,right:0}}>
                        <Text>{parseFloat(activityUser.score).toFixed(2)}{unit}</Text>
                    </View>
                    }
                    {isRecording && velocity && currentActivity.activity_type!=='jump' &&
                    <View style={{position:"absolute",bottom:5,right:0}}>
                        <Text>{parseFloat(velocity).toFixed(2)}m/s</Text>
                    </View>
                    }
                </View>
                {!!isRecording &&
                <Animated.View style={{opacity: this.state.buttonAnim, alignItems: 'flex-end',
                    justifyContent: 'center',
                    flex: 1,
                    borderBottomWidth: 0,
                    elevation: 2}}>
                    {!!activityUser.tag_data &&
                    <Icon name="circle" type="FontAwesome"
                          style={{fontSize: 45, color: statusColors[activityUser.tag_data.status]}}/>
                    }
                </Animated.View>
                }
                {!isRecording &&
                <TouchableOpacity style={{
                    alignItems: 'flex-end',
                    justifyContent: 'center',
                    flex: 1,
                    borderBottomWidth: 0,
                    elevation: 2,
                    opacity:.8
                }} onLongPress={callSetupTag}>
                    {!!activityUser.tag_data &&
                    <Icon name="circle" type="FontAwesome"
                          style={{fontSize: 45, color: statusColors[activityUser.tag_data.status]}}/>
                    }
                    {!activityUser.tag_data &&
                    <Icon name="circle" type="FontAwesome"
                          style={{fontSize: 45, color: statusColors['unavailable']}}/>
                    }
                </TouchableOpacity>
                }
                {isRecording && velocity && currentActivity.activity_type!=='jump' &&
                <Progress.Bar
                    progress={velocity/12}
                    width={null}
                    height={10}
                    useNativeDriver={true}
                    borderWidth={0}
                    borderRadius={0}
                    style={{position:'absolute',bottom:-13,left:0,width:'100%',alignSelf:'center'}}
                />
                }
            </View>
        )
    }
}

export default connect(
    null,
    { setupTag, getESTVelocity }
)(AthleteReadyListItem);
