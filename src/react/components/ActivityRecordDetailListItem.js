import React, { Component } from 'react';
import {TouchableOpacity, View} from "react-native";
import { Text } from 'native-base';
import FadeSlideItem from '../../react/components/FadeSlideItem';
import SprintSplitItem from '../../react/components/SprintSplitItem';
import ActivityRecordStrideChart from '../../react/components/ActivityRecordStrideChart';
import _ from 'lodash';

import * as formatUtils from '../../lib/format_utils';

class ActivityRecordDetailListItem extends Component {

    constructor(props) {
        super(props);
        this.state = {
            rangeStart: -1,
            rangeEnd: -1
        };
    }

    render() {
        const { item, icon, iconColor, rowColor, index, itemIndex, typeDefinition } =  this.props;
        const { rangeStart, rangeEnd } = this.state;

        if(!item) return (<View></View>);

        if(item.value) {
            const { label, value, unit } = item;

            let bgColor = '#ffffff';
            if(rowColor) {
                bgColor = rowColor;
            }
            if(itemIndex>0 && (index+1)%5===0) bgColor = '#d7d7d7';

            return (
                <FadeSlideItem
                    index={index}
                    style={{height:40}}
                >
                    <View icon noIndent style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        backgroundColor:bgColor,
                        width:'100%',
                    }}>
                        <View style={{width:'50%',alignItems:'flex-start',paddingTop:10,paddingBottom:10,paddingLeft:20}}>
                            <Text style={{fontSize:16,fontWeight:'bold'}}>{label}</Text>
                        </View>
                        <View style={{width:'50%',alignItems:'flex-end',paddingTop:10,paddingBottom:10,paddingRight:20}}>
                            <Text style={{fontSize:16}}>{value}</Text>
                        </View>
                    </View>
                </FadeSlideItem>
            )
        }

        if(item.distances && item.distances.length) {
            const { distances, rates, times } = item;

            return (
                <View noIndent style={{
                    flex: 1,
                }}>
                    <View noIndent style={{
                        height:40,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        backgroundColor:'#d7d7d7',
                        width:'100%',
                    }}>
                        <View style={{padding:10,alignItems:'flex-start',width:'25%',height:40}}>
                            <Text style={{fontSize:16,fontWeight:'bold'}}>Stride</Text>
                        </View>
                        <View style={{padding:10,alignItems:'flex-end',width:'25%',height:40}}>
                            <Text style={{fontSize:16,fontWeight:'bold'}}>Distance</Text>
                        </View>
                        <View style={{padding:10,alignItems:'flex-end',width:'25%',height:40}}>
                            <Text style={{fontSize:16,fontWeight:'bold'}}>Rate</Text>
                        </View>
                        <View style={{padding:10,alignItems:'flex-end',width:'25%',height:40}}>
                            <Text style={{fontSize:16,fontWeight:'bold'}}>Contact</Text>
                        </View>
                    </View>

                    {distances && distances.length && distances.map((val,index) =>
                        <FadeSlideItem key={index} index={index}
                                       style={{height:40}}
                        >
                            <View noIndent style={{
                                flex: 1,
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                backgroundColor:index%2?'#f7f7f7':'#fff',
                                width:'100%',
                            }}>
                                <View style={{padding:10,alignItems:'flex-start',width:'25%'}}>
                                    <Text style={{fontSize:16}}>{index+1}</Text>
                                </View>
                                <View style={{padding:10,alignItems:'flex-end',width:'25%',borderLeftWidth:1,borderLeftColor:'#d7d7d7'}}>
                                    <Text style={{fontSize:16}}>{formatUtils.formatDistance(val)}</Text>
                                </View>
                                <View style={{padding:10,alignItems:'flex-end',width:'25%',borderLeftWidth:1,borderLeftColor:'#d7d7d7'}}>
                                    <Text style={{fontSize:16}}>{Math.abs(rates[index]).toFixed(2)}/s</Text>
                                </View>
                                <View style={{padding:10,alignItems:'flex-end',width:'25%',borderLeftWidth:1,borderLeftColor:'#d7d7d7'}}>
                                    <Text style={{fontSize:16}}>{formatUtils.formatTime(times[index])}</Text>
                                </View>
                            </View>
                        </FadeSlideItem>
                    )}
                </View>
            )
        }

        if(item.velocities && item.velocities.length) {
            const { velocities, times } = item;

            let v = velocities.map(v => parseFloat(v));
            let t = times.map(t => parseFloat(t));
            let splits = _.chunk(_.range(0, v.length), 5).map(splits => {
                return splits.map(distance => {
                   return {
                       distance: distance + 1,
                       velocity: v[distance],
                       time: t[distance],
                       total: _.sum(t.slice(0, distance + 1))
                   }
                });
            });

            return (
                <View noIndent style={{
                    flex: 1,
                }}>

                    <View noIndent style={{
                        height:40,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        backgroundColor:'#d7d7d7',
                        width:'100%',
                    }}>
                        <View style={{padding:10,alignItems:'center',width:'25%',height:40}}>
                            <Text style={{fontSize:16,fontWeight:'bold'}}>{'Dist. (' + formatUtils.getUnit(typeDefinition.units) +')'}</Text>
                        </View>
                        <View style={{padding:10,alignItems:'flex-end',width:'25%',height:40}}>
                            <Text style={{fontSize:16,fontWeight:'bold'}}>Velocity (m/s)</Text>
                        </View>
                        <View style={{padding:10,alignItems:'flex-end',width:'25%',height:40}}>
                            <Text style={{fontSize:16,fontWeight:'bold'}}>Split (s)</Text>
                        </View>
                        <View style={{padding:10,alignItems:'flex-end',width:'25%',height:40}}>
                            <Text style={{fontSize:16,fontWeight:'bold'}}>Total time (s)</Text>
                        </View>
                    </View>

                    {splits.length && splits.map((data, index) =>
                        <SprintSplitItem index={index} data={data}/>
                    )}
                </View>
            )
        }

        if(item.frequencies && item.frequencies.length && global.enableStrideLengthView === true) {
            const { frequencies, lengths, times, sides } = item;

            return (
                <View noIndent style={{
                    flex: 1,
                }}>
                    {frequencies && frequencies.length &&
                        <ActivityRecordStrideChart frequencies={frequencies} lengths={lengths} times={times} sides={sides} typeDefinition={typeDefinition}/>
                    }
                </View>
            )
        }

        if(item.row && item.row.length) {
            return (
                <View noIndent style={{
                    flex: 1,
                }}>
                    <FadeSlideItem key={index} index={index}
                                   style={{height:40}}
                    >
                        <View noIndent style={{
                            flex: 1,
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            backgroundColor:index%2?'#f7f7f7':'#fff',
                            width:'100%',
                        }}>
                            <View style={{padding:10,alignItems:'flex-start',width:'40%',borderLeftWidth:1,borderLeftColor:'#d7d7d7'}}>
                                <Text style={{fontSize:16}}>Change {item.row[0]}</Text>
                            </View>
                            <View style={{padding:10,alignItems:'flex-end',width:'20%',borderLeftWidth:1,borderLeftColor:'#d7d7d7'}}>
                                <Text style={{fontSize:16}}>{item.row[1]}</Text>
                            </View>
                            <View style={{padding:10,alignItems:'flex-end',width:'20%',borderLeftWidth:1,borderLeftColor:'#d7d7d7'}}>
                                <Text style={{fontSize:16}}>{item.row[2]}</Text>
                            </View>
                            <View style={{padding:10,alignItems:'flex-end',width:'20%',borderLeftWidth:1,borderLeftColor:'#d7d7d7'}}>
                                <Text style={{fontSize:16}}>{item.row[3]}</Text>
                            </View>
                        </View>
                    </FadeSlideItem>
                </View>
            )
        }

        if(item.segment && item.segment.length) {
            return (
                <View noIndent style={{
                    flex: 1,
                }}>
                    <FadeSlideItem key={index} index={index}
                                   style={{height:40}}
                    >
                        <View noIndent style={{
                            height:40,
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            backgroundColor:'#d7d7d7',
                            width:'100%',
                        }}>
                            <View style={{flex:1,padding:10,alignItems:'flex-start'}}>
                                <Text style={{fontSize:16}}>{item.segment[0]}</Text>
                            </View>
                            <View style={{flex:1,padding:10,alignItems:'flex-end'}}>
                                <Text style={{fontSize:16}}>{item.segment[1]}</Text>
                            </View>
                        </View>
                    </FadeSlideItem>
                </View>
            )
        }

        return (<View></View>)

    }

}

export default ActivityRecordDetailListItem;

