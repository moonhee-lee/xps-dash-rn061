import React, { Component } from 'react';
import { TouchableOpacity, View } from 'react-native';
import {Icon, Text} from 'native-base';
import styles from '../../theme';

class SaveDiscardBar extends Component {
    render () {
        const { onPressSave, onPressDiscard } = this.props;

        return (
            <View style={[styles.saveDiscardBar]}>
                <TouchableOpacity style={[styles.barWrapper,{backgroundColor:'#6AC893'}]} onPress={onPressSave}>
                    <Icon name="save" type="FontAwesome" color='#fff' style={{fontSize:20,paddingTop:8,paddingBottom:7,color:'#fff',paddingRight:5}} />
                    <Text style={{color:'#ffffff',padding:30, marginTop:-4}}>Save</Text>
                </TouchableOpacity>
                <TouchableOpacity style={[styles.barWrapper,{backgroundColor:'#eb5a5a'}]} onPress={onPressDiscard}>
                    <Icon name="trash" type="FontAwesome" color='#fff' style={{fontSize:20,paddingTop:8,paddingBottom:7,color:'#fff',paddingRight:5}} />
                    <Text style={{color:'#ffffff',padding:30, marginTop:-4}}>Discard</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

export default SaveDiscardBar;
