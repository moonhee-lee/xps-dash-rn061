import React, { Component } from 'react';
import { View, Image, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { Navigation } from 'react-native-navigation';
import {
    Container,
    Content,
    Text,
    Icon,
} from 'native-base';

import { setActiveTab } from '../../redux/actions/tabs';
import { getCurrentTab, getActiveSession, getActiveActivity, getLocalDataTotal, getLocalDataCounts } from '../../redux/reducers';
import styles from '../../theme';
import { localStorage, fetchLocalDataCount } from '../../lib/local_storage';

export const mapStateToProps = (state) => {
    return {
        currentTab: getCurrentTab(state),
        activeSession: getActiveSession(state),
        activeActivity: getActiveActivity(state),
    }
}

export class BottomTabBar extends Component {
    constructor(props) {
        super(props);
    }

    componentDidUpdate(prevProps,prevState) {
    }


    onTabPress = (v) => {
        const { navigator, currentTab, setActiveTab, activeSession, activeActivity } = this.props;
        let stack = [
            {
                screen: 'SessionListScreen',
                title: 'Sessions',
                animated: true,
                animationType: 'fade',
            },
            {
                screen: 'SearchScreen',
                title: 'Search',
                animated: true,
                animationType: 'fade',
            },
            {
                screen: 'LiveModeScreen',
                title: 'Live Mode',
                animated: true,
                animationType: 'slide',
            },
            {
                screen: 'TeamsScreen',
                title: 'Teams',
                animated: true,
                animationType: 'fade',
            },
            {
                screen: 'MoreScreen',
                title: 'Configuration',
                animated: true,
                animationType: 'fade',
            },
        ];
        if(v===2) {
                this.props.navigator.showModal({
                    title: 'Live Mode',
                    screen: 'LiveModeScreen',
                    passProps: {
                    },
                    navigatorStyle: {
                        navBarBackgroundColor: '#ffffff',
                        navBarTextColor: '#000000',
                    },
                    navigatorButtons: {
                        leftButtons: [{
                            title: 'Close',
                            id: 'cancel',
                            buttonColor: 'black'
                        }]
                    }
                });
        } else {
            if(v===currentTab) return;
            setActiveTab(v);
            navigator.resetTo(stack[v]);
        }
    }

    render () {
        const { currentTab } = this.props;

        return (
            <View style={{
                width: '100%',
                position: 'absolute',
                bottom: 0,
                height: 70,
                backgroundColor: '#fbfbfb',
                paddingLeft: 20,
                paddingRight: 20,
            }}>
                <View style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                }}>
                    <Icon type='FontAwesome' name='home' style={currentTab===0?styles.tabActiveStyle:styles.tabInactiveStyle} onPress={() => this.onTabPress(0)}  />
                    <Icon type='FontAwesome' name='circle-o' style={styles.tabInactiveStyle} onPress={() => this.onTabPress(2)}  />
                    <Icon type='FontAwesome' name='ellipsis-h' style={currentTab===4?styles.tabActiveStyle:styles.tabInactiveStyle} onPress={() => this.onTabPress(4)} />
                </View>
            </View>
        )
    }
}

export default connect(
    mapStateToProps,
    { setActiveTab, fetchLocalDataCount }
)(BottomTabBar);
