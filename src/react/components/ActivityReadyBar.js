import React, { Component } from 'react';
import { View } from 'react-native';
import {
    Icon,
} from 'native-base';


class ActivityReadyBar extends Component {
    toggleRecording = () => {
        const { toggleRecordingState } = this.props;
        toggleRecordingState();
    }

    render () {
        const { onPressAthleteSelect, onPressActivitySetup, onPressHomeButton, isRecording, recordReady, activityType } = this.props;

        return (
            <View style={{
                width: '100%',
                position: 'absolute',
                bottom: 0,
                height: 86,
                backgroundColor: '#2E9CEB',
                paddingLeft: 30,
                paddingRight: 30,
            }}>
                {isRecording && 
                    <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'center',
                    }}>
                        <Icon type='FontAwesome' name='stop' style={{color: '#E31414', fontSize: 75, paddingTop: 5}} onPress={this.toggleRecording}/>
                    </View>
                }

                {!isRecording && !recordReady && 
                    <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                    }}>
                        <Icon type='FontAwesome' name='users' style={{color: '#bedbf7', fontSize: 19, paddingTop: 25}} onPress={onPressAthleteSelect} />
                        <Icon type='Foundation' name='record' style={{color: '#cccccc', fontSize: 70, paddingTop: 5}} />
                        <Icon type='FontAwesome' name='home' style={{color: '#bedbf7', fontSize: 19, paddingTop: 25}} onPress={onPressHomeButton} />
                    </View>
                }

                {!isRecording && recordReady && 
                    <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                    }}>
                        <Icon type='FontAwesome' name='users' style={{color: '#bedbf7', fontSize: 19, paddingTop: 25}} onPress={onPressAthleteSelect} />
                        <Icon type='Foundation' name='record' style={{color: '#bedbf7', fontSize: 70, paddingTop: 5}} onPress={this.toggleRecording}/>
                        <Icon type='FontAwesome' name='home' style={{color: '#bedbf7', fontSize: 19, paddingTop: 25}} onPress={onPressHomeButton} />
                    </View>
                }
            </View>
        )
    }
}

export default ActivityReadyBar;
