import React, { Component } from 'react';
import {Animated, Easing, TouchableOpacity, View, PanResponder, FlatList, Dimensions} from 'react-native';
import {Text, Icon, Item, Button} from 'native-base';
import styles from '../../theme';
import { Input } from "react-native-elements";
import validate from "validate.js";

import { connect } from 'react-redux';
import { saveSession } from '../../redux/actions/session';
import { setLiveActivitySession } from '../../redux/actions/live_activity';
import { setActiveSession } from '../../redux/actions/active_session';

const dimensionWindow = Dimensions.get('window');
const width = Math.round(dimensionWindow.width);

class SessionSelectBar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name:'',
            panelAnim: new Animated.Value(-340),
            panelActive: false,
            moving:false,
            startMoveY: 0,
            lastMoveY: 0,
        }
        this.openPanel = this.openPanel.bind(this);
        this.closePanel = this.closePanel.bind(this);
        this.togglePanel = this.togglePanel.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this._setActiveSession = this._setActiveSession.bind(this);

        this._panResponder = PanResponder.create({
            // Ask to be the responder:
            onStartShouldSetPanResponder: (evt, gestureState) => true,
            onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
            onMoveShouldSetPanResponder: (evt, gestureState) => true,
            onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,

            onPanResponderGrant: (evt, gestureState) => {
                // The gesture has started. Show visual feedback so the user knows
                // what is happening!

                // gestureState.d{x,y} will be set to zero now
                this.setState({startMoveY:gestureState.moveY});
            },
            onPanResponderMove: (evt, gestureState) => {
                // The most recent move distance is gestureState.move{X,Y}

                // The accumulated gesture distance since becoming responder is
                // gestureState.d{x,y}
                if(this.state.panelActive) {
                    if(gestureState.moveY-this.state.startMoveY<=50) {
                        this.setState({lastMoveY:gestureState.moveY,moving:true});
                        this.closePanel();
                    } else {
                        this.setState({lastMoveY:gestureState.moveY});
                    }
                } else {
                    if(gestureState.moveY-this.state.startMoveY>=100) {
                        this.setState({lastMoveY:gestureState.moveY,moving:true});
                        this.openPanel();
                    } else {
                        this.setState({lastMoveY:gestureState.moveY});
                    }
                }
            },
            onPanResponderTerminationRequest: (evt, gestureState) => true,
            onPanResponderRelease: (evt, gestureState) => {
                // The user has released all touches while this view is the
                // responder. This typically means a gesture has succeeded
                if(!this.state.moving && gestureState.moveY===this.state.lastMoveY) this.togglePanel();
                this.setState({startMoveY:0,moving:false});
            },
            onPanResponderTerminate: (evt, gestureState) => {
                // Another component has become the responder, so this gesture
                // should be cancelled
                this.setState({startMoveY:0});
            },
            onShouldBlockNativeResponder: (evt, gestureState) => {
                // Returns whether this component should block native components from becoming the JS
                // responder. Returns true by default. Is currently only supported on android.
                return true;
            },
        });
    }

    componentDidMount() {

    }

    componentDidUpdate() {
        const { activeSession } = this.props;

    }

    togglePanel() {
        if(this.state.panelActive) this.closePanel();
        else this.openPanel();
    }

    openPanel() {
        Animated.timing(
            this.state.panelAnim,
            {
                toValue: 0,
                duration: 300,
                easing: Easing.inOut(Easing.quad),
                useNativeDriver: true
            }
        ).start();
        this.setState({panelActive:true});
    }

    closePanel() {
        Animated.timing(
            this.state.panelAnim,
            {
                toValue: -340,
                duration: 300,
                easing: Easing.inOut(Easing.quad),
                useNativeDriver: true
            }
        ).start();
        this.setState({panelActive:false});
    }

    validateName = (name) => {
        return validate.single(name, {
            presence: {
                allowEmpty: false,
                message: 'A name is required'
            }
        });
    }

    handleSubmit() {
        const { name } = this.state;
        const { saveSession, currentTeam, currentOrg } = this.props;
        const nameError = this.validateName(name);

        this.setState({
            nameError: nameError ? nameError[0] : '',
        });

        if (!nameError) {
            const sessionData = {
                name,
                description:'',
                team: currentTeam,
                org: currentOrg.id
            }

            saveSession(sessionData, false, {}, this._setActiveSession).then(() => {
                setTimeout(this.closePanel,500);
            }).catch((err)=>{
                console.log(err);
            });
        }
    }

    _setActiveSession(session) {
        const { setActiveSession, setLiveActivitySession, liveActivity } = this.props;
        setActiveSession(session);
        setLiveActivitySession(session,liveActivity);
        setTimeout(this.closePanel,500);
    }

    render () {
        const { onPressCreate, sessions, teams, currentTeam, activeSession } = this.props;
        const { panelAnim, opening, name } = this.state;

        return (
            <Animated.View style={[styles.SessionSelectBar,{alignItems:'center',transform: [{translateY: panelAnim}]}]}>
                    {!!sessions &&
                    <FlatList
                        style={{width:width}}
                        data={sessions}
                        enableScroll={true}
                        renderItem={({item,index}) =>
                            <TouchableOpacity
                                onPress={() => this._setActiveSession(item)}
                                style={{flexDirection: 'row',justifyContent: 'center',height:50,width:width,paddingLeft:20,paddingRight:20,borderBottomWidth:1,borderBottomColor:'#d7d7d7',backgroundColor:(!!activeSession && activeSession.id===item.id)?'#6AC893':(index%2===0?'#f7f7f7':'#fafafa')}}>
                                <Text style={{lineHeight:50,color:(!!activeSession && activeSession.id===item.id)?'#fff':'#000'}}>{item.name}</Text>
                            </TouchableOpacity>
                        }
                    >

                    </FlatList>
                    }
                    <View {...this._panResponder.panHandlers}
                          style={{
                              flexDirection: 'row',
                              justifyContent: 'center',
                              backgroundColor:'#2E9CEB',width:'100%',height:60,alignItems:'center'}}>
                        <Icon name="list" type="FontAwesome" style={{color:'#fff',fontSize:15,paddingRight:10}} />
                        <Text style={{fontSize:15,color:'#fff'}}>{activeSession && activeSession.name?'Session: '+activeSession.name:'Select Session'}</Text>
                    </View>
            </Animated.View>
        )
    }
}

export default connect(
    null,
    { saveSession, setLiveActivitySession, setActiveSession }
)(SessionSelectBar);

