import React, { Component } from 'react';

import ActivitySummaryListItem from '../../react/components/ActivitySummaryListItem';


class ActivityLeaderboardListItem extends Component {
    render() {
        const { activitySummary, session, index, navigator } = this.props;

        return (
            <ActivitySummaryListItem
                activitySummary={activitySummary}
                session={session}
                theme={'grey'}
                navigator={navigator}
                index={index}
            />
        )

    }

}

export default ActivityLeaderboardListItem;
