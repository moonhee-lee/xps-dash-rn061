import React, { Component } from 'react';

import { getCurrentTab, getActiveSession, getActiveActivity, getLocalDataTotal, getLocalDataCounts } from '../../redux/reducers';
import { localStorage, fetchLocalDataCount } from '../../lib/local_storage';
import { connect } from 'react-redux';
import {TouchableOpacity, View} from "react-native";
import {Icon, Text} from "native-base";
import styles from '../../theme';
import {Navigation} from 'react-native-navigation';

export const mapStateToProps = (state) => {
    return {
        currentTab: getCurrentTab(state),
        activeSession: getActiveSession(state),
        activeActivity: getActiveActivity(state),
        localDataTotal: getLocalDataTotal(state),
        localDataCounts: getLocalDataCounts(state)
    }
}

class SyncButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
            syncItems: 0,
        };
        this.openSyncScreen = this.openSyncScreen.bind(this);
    }

    componentDidMount() {
        const { fetchLocalDataCount } = this.props;
        fetchLocalDataCount();
        this.setState({syncItems:0});
    }

    componentDidUpdate(prevProps,prevState) {
        const { localDataTotal, setSyncItems } = this.props;
        const { syncItems } = this.state;

        if(syncItems!==localDataTotal) {
            this.setState({syncItems:localDataTotal});
            setSyncItems(localDataTotal);
        }
    }

    openSyncScreen() {
        Navigation.showModal({
            stack: {
                children: [{
                    component: {
                        name: 'SyncScreen',
                        passProps: {
                        },
                        options: {
                            topBar: {
                                title: {
                                    text: 'Sync to Cloud'
                                },
                                leftButtons: [{
                                    id: 'cancelSyncScreen',
                                    text: 'Cancel',
                                    color: 'black',
                                }]
                            }
                        }
                    }
                }]
            }
        });
    }

    render () {
        const { localDataTotal } = this.props;
        const { syncItems } = this.state;

        if(!localDataTotal) return (null)
        const s = syncItems===1?'':'s';

        return (
            <TouchableOpacity style={[styles.messageBar,{backgroundColor:'#6AC893'}]} onPress={this.openSyncScreen}>
                <Text style={styles.messageBarText}>
                    <Icon name="cloud-upload" type="FontAwesome" style={{color: "#fff", fontSize: 16, lineHeight:40}}/>
                    {'   '}{syncItems}{' Item'+s+' to Upload'}
                </Text>
            </TouchableOpacity>
        )
    }
}

export default connect(
    mapStateToProps,
    { fetchLocalDataCount }
)(SyncButton);
