import React, { Component } from 'react';
import {TouchableOpacity, View} from 'react-native';
import {
    Container,
    Content,
    Text,
    Icon,
} from 'native-base';
import styles from '../../theme';

class SaveBar extends Component {
    render () {
        const { onPressSave, message, bgColor } = this.props;
         const msg = !!message?message:'Save';

        return (
            <View style={{
                width: '100%',
                position: 'absolute',
                bottom: 0,
                height: 86,
                backgroundColor: '#2E9CEB',
            }}>
                <TouchableOpacity style={[styles.barWrapper,!!bgColor?{backgroundColor:bgColor}:'']} onPress={onPressSave}>
                    <Text style={{color:'#ffffff',padding:30}}>{msg}</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

export default SaveBar;
