import React, { Component } from 'react';

import {FlatList, TouchableOpacity, View} from 'react-native';
import ActivityRecordDetailListItem from '../../react/components/ActivityRecordDetailListItem';
import _ from 'lodash';
import {Text} from "native-base";

class ActivityRecordDetailList extends Component {

    render() {
        const { listData, itemWidth, itemIndex, typeDefinition } = this.props;

        if(!listData || _.isEmpty(listData) || !listData[0]) return (<View></View>);

        return (
            <View style={{paddingBottom:40}}>
                {1===2 && listData.length && listData[0].segment &&
                <View noIndent style={{
                    height:40,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    backgroundColor:'#d7d7d7',
                    width:'100%',
                }}>
                    <View style={{padding:10,alignItems:'flex-start',width:'25%',height:40}}>
                        <Text style={{fontSize:16,fontWeight:'bold'}}>{ }</Text>
                    </View>
                    <View style={{padding:10,alignItems:'flex-start',width:'25%',height:40}}>
                        <Text style={{fontSize:16,fontWeight:'bold'}}>° Change</Text>
                    </View>
                    <View style={{padding:10,alignItems:'flex-end',width:'25%',height:40}}>
                        <Text style={{fontSize:16,fontWeight:'bold'}}>° Rate</Text>
                    </View>
                    <View style={{padding:10,alignItems:'flex-end',width:'25%',height:40}}>
                        <Text style={{fontSize:16,fontWeight:'bold'}}>Time</Text>
                    </View>
                </View>
                }
                <FlatList
                    style={{width:itemWidth}}
                    data={listData}
                    enableScroll={true}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({item,index}) =>
                        <ActivityRecordDetailListItem
                            item={item}
                            rowColor={index % 2 ? "#f7f7f7" : '#fff'}
                            index={index}
                            itemIndex={itemIndex}
                            typeDefinition={typeDefinition}
                        />
                    }
                >
                </FlatList>
            </View>
        )
    }

}

export default ActivityRecordDetailList;
