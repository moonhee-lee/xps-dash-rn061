import React, { Component } from 'react';

import { View } from 'react-native';
import {Thumbnail, Text, Badge} from 'native-base';

import styles from '../../theme';


class ActivityRecordListItem extends Component {
    render() {
        const { activityRecord } = this.props;
        const activityType = activityRecord.activity.activity_type;

        // TODO - what to put in the individual record items?

        return (
            <View>
                {!!activityRecord && activityRecord.user &&
                <View style={styles.footer}>
                    <View style={{width:'25%',alignItems:'center',borderBottomWidth:0}} >
                        <Thumbnail style={{ alignContent: 'center',height:70, width:70, borderRadius:40}} source={{uri:activityRecord.user.image_url}} />
                    </View>
                    <View style={{width:'50%',borderBottomWidth:0}}>
                        <Text style={styles.title}>{activityRecord.user.full_name}</Text>
                    </View>

                    {activityType === 'sprint' &&
                        <View style={{width:'25%',alignItems:'center',borderBottomWidth:0}} >
                            <Text>{activityRecord.meta.total_time}</Text>
                        </View>
                    }

                    {activityType === 'jump' &&
                        <View style={{width:'25%',alignItems:'center',borderBottomWidth:0}} >
                            <Text>{activityRecord.meta.score}</Text>
                        </View>
                    }
                </View>
            }
            </View>


        )
    }
}

export default ActivityRecordListItem;
