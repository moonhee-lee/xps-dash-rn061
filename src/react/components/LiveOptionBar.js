import React, { Component } from 'react';
import {TouchableOpacity, View} from 'react-native';
import { Text, Icon } from 'native-base';
import styles from '../../theme';
import CustomIcon from '../../theme/components/XPSIcon';
import { UserAvatar } from '../../react/components/UserAvatar';

class LiveOptionBar extends Component {
    render () {
        const { onPressAthleteSelect, onPressActivitySetup, liveActivity, activeUsers } = this.props;
        const title = (liveActivity && liveActivity.display_name)?liveActivity.display_name:'Activity';

        return (
            <View style={[styles.viewRowNoPadding,styles.liveOptionBar]}>
                <View style={[{flex:1,borderRightWidth:2,borderRightColor:'#fff'}]}>
                    <TouchableOpacity style={styles.barWrapper} onPress={onPressAthleteSelect}>
                        {!!activeUsers && activeUsers.length>0 &&
                        <View style={styles.viewRowNoPadding}>
                            <View style={{flex:1,alignItems:'center',borderBottomWidth:0,justifyContent: 'center'}} >
                                {activeUsers.map( (activeUser,index) =>
                                    <View style={{position:'absolute',right:10*index,paddingRight:10}} key={index}>
                                        <UserAvatar size={45} user={activeUser.user} navigator={navigator} />
                                    </View>
                                )}
                            </View>
                            <Text style={[{flex:1,alignSelf:'center'},styles.doneBarActiveText]}>
                                {"Athletes"}
                            </Text>
                        </View>
                        }
                        {activeUsers.length===0 &&
                        <Text style={[styles.doneBarActiveText]}>
                            {"Athletes"}
                        </Text>
                        }
                    </TouchableOpacity>
                </View>
                <View style={{flex:1}}>
                    <TouchableOpacity style={styles.barWrapper} onPress={onPressActivitySetup}>
                        {!!liveActivity && liveActivity.activity_type==='sprint' &&
                        <CustomIcon name='sprint' style={{fontSize:30,color:'#fff',paddingRight:5}} />
                        }
                        {!!liveActivity && liveActivity.activity_type==='jump' &&
                        <CustomIcon name='jump' style={{fontSize:30,color:'#fff',paddingRight:5}} />
                        }
                        {!!liveActivity && liveActivity.activity_type==='free' &&
                        <Icon name="random" type="FontAwesome" color='#fff' style={{fontSize:20,paddingTop:8,paddingBottom:7,color:'#fff',paddingRight:5}} />
                        }
                        <Text style={[styles.doneBarActiveText,{flexWrap:'wrap'}]}>
                            {title}
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

export default LiveOptionBar;
