import React, { Component } from 'react';

import { Animated } from 'react-native';


class FadeSlideItem extends Component {

    state = {
        fadeAnim: new Animated.Value(0),
    }

    componentDidMount() {
        Animated.delay(100);
        Animated.timing(
            this.state.fadeAnim,
            {
                toValue: 1,
                duration: 500,
            },
        ).start();
    }

    render() {
        const { index } = this.props;
        const { fadeAnim } = this.state;

        return (
            <Animated.View
                style={{
                    ...this.props.style,
                    opacity: fadeAnim,
                    transform: [{
                        translateY: this.state.fadeAnim.interpolate({
                            inputRange: [0, 1],
                            outputRange: [((index+1)*10), 0]
                        }),
                    }],
                    elevation:1,
                }}
            >
                {this.props.children}
            </Animated.View>
        )
    }
}

export default FadeSlideItem;