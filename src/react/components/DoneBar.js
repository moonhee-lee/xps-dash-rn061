import React, { Component } from 'react';
import {TouchableOpacity, View} from 'react-native';
import { Text, Icon } from 'native-base';
import styles from '../../theme';

class DoneBar extends Component {
    render () {
        const { onPressSave, message, isActive } = this.props;

        return (
            <View style={!!isActive?styles.doneBarActive:styles.doneBarInactive}>
                {!!isActive &&
                    <TouchableOpacity style={styles.barWrapper} onPress={() => onPressSave()}>
                        <Text style={styles.doneBarActiveText}>
                            <Icon name="check" type="FontAwesome" style={{color:"#fff", fontSize:16}} containerStyle={{paddingRight:20}} />
                            {'  '}{!!message ? message : 'Done'}
                        </Text>
                    </TouchableOpacity>
                }
                {!isActive &&
                <View style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <Text style={styles.doneBarInactiveText}>{'  '}{!!message ? message : 'Done'}</Text>
                </View>
                }
            </View>
        )
    }
}

export default DoneBar;
