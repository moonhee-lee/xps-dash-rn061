import React, { Component } from 'react';
import { View } from 'react-native';

class ActivityVelocityBar extends Component {

    render () {
        const { velocity } = this.props;

        return (
            <View style={{
                elevation: 2,
                backgroundColor: 'rgba(0,255,0,.5)',
                height: 20,
                width: parseFloat((velocity / 12) * 100).toFixed(1) + '%',
                position: 'absolute',
                bottom: 0,
                left: 0
            }} />
        )
    }
}

export default ActivityVelocityBar;
