import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
    Text, View
} from 'native-base';

import { TouchableOpacity } from 'react-native';
import * as colorUtils from '../../lib/color_utils';
import { makeHttpRequest } from '../../lib/httprequest';
import { setUserImage } from "../../redux/actions/user";
import { getAxiosClient } from '../../redux/api/client';

export class UserAvatar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user_image:null,
            checkedForImages:false,
            mounted: false
        }
    }

    componentDidMount() {
        this.setState({mounted:true});
    }

    componentDidUpdate(prevProps,prevState) {
        const { user } = this.props;

        if(this.state.mounted && user) {
            if (!this.state.user_image) this.loadUserImages();

            if (!!user.local_image && user.local_image !== prevProps.user.local_image) this.loadUserImages();
            if (!!user.thumbnail_url && user.thumbnail_url !== prevProps.user.thumbnail_url) this.loadUserImages();
            if (!!user.image_url && user.image_url !== prevProps.user.image_url) this.loadUserImages();
            if (!!user.full_image_url && user.full_image_url !== prevProps.user.full_image_url) this.loadUserImages();
        }
    }

    loadUserImages() {
        const { user } = this.props;
        const { checkedForImages } = this.state;

        if(!!user.user_image) {
            this.setState({
                user_image: user.user_image,
                checkedForImages: true
            });
            return;
        }

        if(user && !checkedForImages) {
            if (user.local_image) {
                this.setUserImage(user,user.local_image);
                return;
            }
            if (user.thumbnail_url) {
                getAxiosClient(1000).head(user.thumbnail_url).then((data) => {
                    if (data) {
                        this.setUserImage(user, user.thumbnail_url);
                        return;
                    }
                });
                //return;
            }
            if (user.image_url) {
                getAxiosClient(1000).head(user.image_url).then((data) => {
                    if (data) {
                        this.setUserImage(user, user.image_url);
                        return;
                    }
                });
                //return;
            }
            if (user.full_image_url) {
                getAxiosClient(1000).head(user.full_image_url).then((data) => {
                    if(data) {
                        this.setUserImage(user,user.full_image_url);
                        return;
                    }
                });
                //return;
            }
            this.setState({
                checkedForImages:true
            });
        }
    }

    setUserImage(user, imageURL, size) {
        //console.log('setting user image: '+user.full_name+' - '+imageURL)
        const { setUserImage } = this.props;
        this.setState({
            user_image: imageURL,
            checkedForImages:true
        });
        setUserImage(user.id, imageURL);
    }


    showAthleteProfile(athlete) {
        this.props.navigator.showModal({
            title: 'Profile for '+athlete.full_name,
            screen: 'AthleteProfileScreen',
            passProps: {
                athlete:athlete,
            },
            navigatorButtons: {
                leftButtons: [{
                    title: 'Close',
                    id: 'cancel',
                    buttonColor: 'black'
                }]
            },
            animationType: 'slide'
        });
    }

    renderImage(user,avatarSize) {
        //console.log('got user image: '+user.full_name+' - '+this.state.user_image)
        return (
            <TouchableOpacity onLongPress={() => this.showAthleteProfile(user)}>
                <View style={{
                    height:avatarSize,width:avatarSize,
                    borderRadius:avatarSize/2,
                    backgroundColor:'transparent',elevation:2,
                    borderColor:'#fff',borderWidth:2,
                    position:'absolute',left:0,top:0
                }}/>
            </TouchableOpacity>
        )
    }

    renderInitials(user,avatarSize) {
        let fontSize = 16;
        let padding = 6;
        let paddingTop = 6;
        let colors;

        if(avatarSize>=65) {
            fontSize = 24;
            padding = 4;
            paddingTop = 16;
        } else if(avatarSize>35) {
            fontSize = 18;
            padding = 10;
            paddingTop = 10;
        }

        if(user.full_name) {
            colors = colorUtils.createUserColor(user.full_name);
        } else {
            colors = colorUtils.createUserColor('');
        }

        return (
            <TouchableOpacity onLongPress={() => this.showAthleteProfile(user)}>
                <View style={{
                    height:avatarSize,width:avatarSize,
                    borderRadius:avatarSize/2,
                    backgroundColor:colors.color,
                    borderColor:'#fff',borderWidth:2
                }}>
                    {user.first_name !== undefined && user.last_name !== undefined &&
                    <Text style={{
                        color:colors.fontColor,
                        fontSize:fontSize,
                        paddingTop:paddingTop,
                        paddingBottom:padding,
                        textAlign:'center'
                    }}>
                        {user.first_name.substr(0, 1)}{user.last_name.substr(0, 1)}
                    </Text>
                    }
                    {user.first_name !== undefined && !user.last_name &&
                    <Text style={{
                        color:colors.fontColor,
                        fontSize:fontSize,
                        paddingTop:paddingTop,
                        paddingBottom:padding,
                        textAlign:'center'
                    }}>
                        {user.first_name.substr(0, 1)}
                    </Text>
                    }
                </View>
            </TouchableOpacity>
        )
    }

    render () {
        const { size, user } = this.props;
        const { user_image, checkedForImages } = this.state;

        if(!user) return (<View></View>);

        let avatarSize = size;
        if(!size) avatarSize = 40;

        if(user_image) return this.renderImage(user,avatarSize);
        else if(checkedForImages) return this.renderInitials(user,avatarSize)
        else return (
                <View style={{
                    height:avatarSize,width:avatarSize,
                    borderRadius:avatarSize/2,
                    backgroundColor:'#fff',
                    borderColor:'#fff',borderWidth:2
                }}>{ }</View>
            )
    }
}

export default connect(
    null,
    { setUserImage }
)(UserAvatar);
