import React, { Component } from 'react';

import {Dimensions, StyleSheet, TouchableOpacity, View} from 'react-native';
import { H3, Badge, Text, Icon } from 'native-base';
import styles from '../../theme';

import * as formatUtils from '../../lib/format_utils';

const dimensionWindow = Dimensions.get('window');
const width = Math.round(dimensionWindow.width);

class ActivityRecordSummary extends Component {

    render() {
        const { summaryData } = this.props;
        // primary, secondary, listItems
        if(!summaryData || !summaryData.primary) return (<View></View>);

        return (

            <View style={styles.activitySummary}>

                <View style={styles.activitySummaryMajorRow}>

                    <View style={{}}>
                        <Text style={{textAlign:'center',fontSize:16,opacity:0.7,fontWeight:'bold'}}>{summaryData.primary.label}</Text>
                        <Text style={{marginTop:-10, textAlign:'center',fontSize:64,fontWeight:'900',letterSpacing:-2,marginVertical:0}}>{summaryData.primary.value}</Text>
                        {!!summaryData.secondary.value &&
                        <Text style={{fontSize:48,fontWeight:'900',textAlign:'center', color:'#ccc',marginTop:-10}}>
                            {!!summaryData.secondary.label &&
                            <Text>{summaryData.secondary.label}: </Text>
                            }
                            {summaryData.secondary.value}
                        </Text>
                        }
                    </View>

                </View>

                <View style={{width:width,flex:1,paddingHorizontal: 24}}>
                    {summaryData.listItems && summaryData.listItems.map((item,index) =>

                        <View style={[styles.activitySummaryMinorRow]}>
                            <View style={{width:'50%',alignItems:'flex-start',paddingVertical:1}}>
                                <Text style={{fontSize:16,fontWeight:'bold'}}>{item.label}</Text>
                            </View>
                            <View style={{width:'50%',alignItems:'flex-end',paddingVertical:1}}>
                                <Text style={{fontSize:16}}>{item.value}</Text>
                            </View>
                        </View>
                    )}
                </View>


            </View>
        )
    }
}
export default ActivityRecordSummary;
