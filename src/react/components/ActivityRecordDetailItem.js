import React, { Component } from 'react';

import { TouchableOpacity, View, ART, Dimensions, StyleSheet } from 'react-native';
import { Container, Content, Card, CardItem, Button, Text } from 'native-base';
import * as graphUtils from '../../lib/graph_utils';
import ActivityRecordDetailListItem from '../../react/components/ActivityRecordDetailListItem';

import { Grid, LineChart, XAxis, YAxis } from 'react-native-svg-charts'

const { Group, Shape, Surface } = ART;

const dimensionWindow = Dimensions.get('window');
const width = Math.round(dimensionWindow.width);

class ActivityRecordDetailItem extends Component {
    static WIDTH = width;
    state = {
        lineData: null
    };

    componentDidMount() {
        const { meta, data } = this.props;
        if(data && data.data) {
            const startTime = meta.start * 1000;
            const endTime = meta.end * 1000;
            const dataValues = _.filter(data.data, (msg) => (msg.time > startTime && msg.time < endTime));
            //this.setState({lineData: dataValues.map((v) => (v.posM.z))});
        }
    }

    render() {
        const { activityRecord, index } = this.props;
        const { meta, data, activity, user } = activityRecord;

        const graphWidth = width - 10;
        const graphHeight = width - 10;
        const startTime = meta.start * 1000;
        const endTime = meta.end * 1000;
        const dataValues =  _.filter(data.data, (msg) => (msg.time > startTime && msg.time < endTime) );

        const lineGraph = graphUtils.createLineGraph({
            data: _.values(data.data),
            tagId: data.id,  // not really tagId, but we don't use it in this view.
            width: graphWidth,
            height: graphHeight,
            activityType: activity.activity_type,
        });

        // TODO - what to put in the individual record items?

        const axesSvg = { fontSize: 10, fill: 'grey' };
        const verticalContentInset = { top: 10, bottom: 10 }
        const xAxisHeight = 30

        return (
            <Content>
                {1==this.state.lineData && this.state.lineData.length &&
                <View style={{height: 200, padding: 20, flexDirection: 'row'}}>
                    <YAxis
                        data={this.state.lineData}
                        style={{marginBottom: xAxisHeight}}
                        contentInset={verticalContentInset}
                        svg={axesSvg}
                    />
                    <View style={{flex: 1, marginLeft: 10}}>
                        <LineChart
                            style={{flex: 1}}
                            data={this.state.lineData}
                            contentInset={verticalContentInset}
                            svg={{stroke: 'rgb(134, 65, 244)'}}
                        >
                            <Grid/>
                        </LineChart>
                        <XAxis
                            style={{marginHorizontal: -10, height: xAxisHeight}}
                            data={this.state.lineData}
                            formatLabel={(value, index) => index}
                            contentInset={{left: 10, right: 10}}
                            svg={axesSvg}
                        />
                    </View>
                </View>
                }
                <View style={{width:'100%',borderWidth:0}}>
                    <Surface width={graphWidth} height={graphHeight}>
                        {!!lineGraph &&
                        <Group x={0} y={0}>
                            <Shape
                                d={lineGraph.path}
                                stroke={'#263248'}
                                strokeWidth={1}
                                key={lineGraph.tagId}
                            />
                        </Group>
                        }
                    </Surface>
                </View>
                }
            </Content>
        )

    }

}

export default ActivityRecordDetailItem;

const styles = StyleSheet.create({
    container: {
        width: width,
        height: width,
        justifyContent: 'center',
        alignItems: 'center',
        overflow: 'visible',
    },
});
