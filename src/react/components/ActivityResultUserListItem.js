import React, { Component } from 'react';

import {Dimensions, StyleSheet, TouchableOpacity, View} from 'react-native';
import { H3, Badge, Text, Icon } from 'native-base';
import styles from '../../theme';
import { UserAvatar } from '../../react/components/UserAvatar';

import * as formatUtils from '../../lib/format_utils';

const dimensionWindow = Dimensions.get('window');
const width = Math.round(dimensionWindow.width);

class ActivityResultUserListItem extends Component {

    render() {
        const { item, activityIndex, total, color, navigator, activity } = this.props;

        if(!item || !item.user || !item.meta) return (<View></View>)

        let jump = false;

        if(item && item.meta && activity && activity.activity_type==='jump') {
            if (activity.orientation_type==='vertical' || activity.orientation==='vertical') {
                jump = item.meta.height
            } else {
                jump = item.meta.length
            }
        }

        return (
            <View style={{
                width: width,
                borderTopWidth: 1,
                borderBottomWidth: 1,
                borderColor: '#e6e6e6',
                borderLeftWidth:6,borderLeftColor:color,
                borderRightWidth:6,borderRightColor:color,
                marginTop:30

            }}>
                <View style={styles.viewRow}>


                    <View style={{flex:1, alignItems: 'flex-start', paddingRight: 10, paddingLeft: 20, elevation:1}}>
                        <UserAvatar size={65} user={item.user} navigator={navigator} />
                    </View>
                    <View style={{flex:3, elevation:1,justifyContent:'center',height:65}}>
                        <Text style={{fontSize: 18, lineHeight: 18, paddingRight: 10, paddingLeft: 10, color:'#000'}}>{item.user.full_name}</Text>
                    </View>
                    {!!item.meta &&
                    <View style={{flex:2, paddingRight: 20, paddingLeft: 0, alignItems: 'flex-end', elevation:1}}>
                        {item.meta.finish_time && item.meta.reaction_time && item.meta.reaction_time>0 &&
                        <View style={{flex: 1,flexDirection: 'column'}}>
                            <Text style={{fontSize: 12,lineHeight: 30,alignSelf:'flex-end'}}>Reaction: <Text style={{fontWeight:'bold',fontSize:16,lineHeight: 30}}>{formatUtils.formatTime(item.meta.reaction_time,3)}</Text></Text>
                            <Text style={{fontSize: 12,lineHeight: 30,alignSelf:'flex-end'}}>Time: <Text style={{fontWeight:'bold',fontSize:16,lineHeight: 30}}>{formatUtils.formatTime(item.meta.finish_time+item.meta.reaction_time)}</Text></Text>
                        </View>
                        }
                        {item.meta.finish_time && item.meta.reaction_time && item.meta.reaction_time<0 &&
                        <View style={{flex: 1,flexDirection: 'column'}}>
                            <Text style={{fontSize: 16,lineHeight: 30,alignSelf:'flex-end',color:'#E31414',fontWeight:'bold'}}>False Start</Text>
                            <Text style={{fontSize: 12,lineHeight: 30,alignSelf:'flex-end'}}>Time: <Text style={{fontWeight:'bold',fontSize:16,lineHeight: 30}}>{formatUtils.formatTime(item.meta.finish_time+item.meta.reaction_time)}</Text></Text>
                        </View>
                        }
                        {item.meta.finish_time && !item.meta.reaction_time &&
                        <View style={{flex: 1,flexDirection: 'column'}}>
                            <Text style={{
                                fontSize: 16,
                                lineHeight: 65,
                                fontWeight: 'bold',
                            }}>{formatUtils.formatTime(item.meta.finish_time)}</Text>
                        </View>
                        }
                        {jump &&
                        <View style={{flex: 1,flexDirection: 'column'}}>
                            <Text style={{fontSize: 20, lineHeight: 65, fontWeight: 'bold'}}>{formatUtils.formatDistance(jump)}</Text>
                        </View>
                        }
                    </View>
                    }
                    </View>
                {!!activityIndex && activityIndex<(total-1) &&
                <View style={{
                    position:'absolute',
                    top:-48,
                    right:-90,
                    elevation:2
                }}>
                    <Icon type="FontAwesome" name="arrows-h" style={{color:'rgba(0,0,0,.1)',fontSize:180}} />
                </View>
                }

            </View>
        )
    }
}
export default ActivityResultUserListItem;
