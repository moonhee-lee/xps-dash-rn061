import React, { Component } from 'react';

import {Dimensions, View} from 'react-native';
import ActivityRecordDetailList from '../../react/components/ActivityRecordDetailList';

const dimensionWindow = Dimensions.get('window');
const width = Math.round(dimensionWindow.width);

class ActivityRecordDetailLists extends Component {

    render() {
        const { listData, activity, activityRecord } = this.props;
        let typeDefinition = activity.id === 'live_activity' ? activityRecord : activity.type_definition;

        return (
            <View style={{marginTop: 24, width:width,flex:1}}>
                {listData && activity && activity.id && listData.map((item,index) =>
                    <ActivityRecordDetailList
                        typeDefinition={typeDefinition}
                        listData={item}
                        itemIndex={index}
                        key={index}
                    />
                )
                }
            </View>
        )

    }

}

export default ActivityRecordDetailLists;
