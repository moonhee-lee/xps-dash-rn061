import React, {Component} from 'react';

import {TouchableOpacity, View} from 'react-native';
import {Text} from "native-base";
import FadeSlideItem from '../../react/components/FadeSlideItem';
import _ from 'lodash';
import {formatNumber} from "../../lib/format_utils";

class SprintSplitItem extends Component {

    state = {
        isExpended: false
    };

    render() {
        const {index, data} = this.props;
        const {isExpended} = this.state;
        let summary = {
            start: _.first(data).distance,
            end: _.last(data).distance,
            velocity: _.maxBy(data, 'velocity').velocity,
            split: _.sumBy(data, 'time'),
            total: _.last(data).total
        };

        return <View>
            {!isExpended &&
            <FadeSlideItem index={index} style={{height: 40}}>
                <TouchableOpacity onPress={() => this.setState({
                    isExpended: !this.state.isExpended
                })}
                                  noIndent style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    backgroundColor: index % 2 ? '#f7f7f7' : '#fff',
                    width: '100%',
                }}>
                    <View style={{padding: 10, alignItems: 'center', width: '25%'}}>
                        <Text style={{fontSize: 16}}>{summary.end}</Text>
                    </View>

                    <View style={{
                        padding: 10,
                        alignItems: 'flex-end',
                        width: '25%',
                        borderLeftWidth: 1,
                        borderLeftColor: '#d7d7d7'
                    }}>
                        <Text style={{fontSize: 16}}>{formatNumber(summary.velocity)}</Text>
                    </View>

                    <View style={{
                        padding: 10,
                        alignItems: 'flex-end',
                        width: '25%',
                        borderLeftWidth: 1,
                        borderLeftColor: '#d7d7d7'
                    }}>
                        <Text style={{fontSize: 16}}>{formatNumber(summary.split)}</Text>
                    </View>

                    <View style={{
                        padding: 10,
                        alignItems: 'flex-end',
                        width: '25%',
                        borderLeftWidth: 1,
                        borderLeftColor: '#d7d7d7'
                    }}>
                        <Text style={{fontSize: 16}}>{formatNumber(summary.total)}</Text>
                    </View>
                </TouchableOpacity>
            </FadeSlideItem>
            }
            {isExpended && data.map((data) =>
                <FadeSlideItem index={index} style={{height: 40}}>
                    <TouchableOpacity onPress={() => this.setState({
                        isExpended: !this.state.isExpended
                    })}
                                      noIndent style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        backgroundColor: index % 2 ? '#f7f7f7' : '#fff',
                        width: '100%',
                    }}>
                        <View style={{padding: 10, alignItems: 'center', width: '25%'}}>
                            <Text style={{fontSize: 16}}>{data.distance}</Text>
                        </View>
                        <View style={{
                            padding: 10,
                            alignItems: 'flex-end',
                            width: '25%',
                            borderLeftWidth: 1,
                            borderLeftColor: '#d7d7d7'
                        }}>
                            {data.velocity === summary.velocity &&
                            <Text style={{fontSize: 16}}>{formatNumber(data.velocity)} (max)</Text>
                            }
                            {data.velocity !== summary.velocity &&
                            <Text style={{fontSize: 16}}>{formatNumber(data.velocity)}</Text>
                            }
                        </View>
                        <View style={{
                            padding: 10,
                            alignItems: 'flex-end',
                            width: '25%',
                            borderLeftWidth: 1,
                            borderLeftColor: '#d7d7d7'
                        }}>
                            <Text style={{fontSize: 16}}>{formatNumber(data.time)}</Text>
                        </View>
                        <View style={{
                            padding: 10,
                            alignItems: 'flex-end',
                            width: '25%',
                            borderLeftWidth: 1,
                            borderLeftColor: '#d7d7d7'
                        }}>
                            <Text style={{fontSize: 16}}>{formatNumber(data.total)}</Text>
                        </View>
                    </TouchableOpacity>
                </FadeSlideItem>
            )}
        </View>
    }
}

export default SprintSplitItem;
