import React, { Component } from 'react';

import { View } from 'react-native';
import { Badge, Text } from 'native-base';
import { UserAvatar } from '../../react/components/UserAvatar';

import styles from '../../theme';
import * as formatUtils from '../../lib/format_utils';

class ActivitySummaryListItem extends Component {

    render() {
        const { activitySummary, theme, session, index, navigator } = this.props;

        let team = null;
        if(!!session && !!session.team && !!session.team_data)  team = (typeof(session.team)==='object' && session.team)?session.team:session.team_data;

        if(!activitySummary || !activitySummary.user) return (<View></View>)

        let score;
        if(activitySummary.type_definition.activity_type==='jump') {
            score = formatUtils.formatDistance(activitySummary.score);
        } else {
            score = formatUtils.formatTime(activitySummary.score);
        }

        const bgColor = index%2?'#ffffff':'#f7f7f7';

        return (
            <View style={{width:'100%'}}>
                {theme === 'grey' &&
                <View style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    paddingTop: 20,
                    paddingBottom: 20,
                    borderBottomColor: '#E7E7E7',
                    borderBottomWidth: 1,
                    backgroundColor:bgColor,
                    width:'100%'
                }}>
                    <View style={{width: '20%', paddingRight: 15,paddingLeft:15}}>
                        <UserAvatar size={45} user={activitySummary.user} navigator={navigator} />
                    </View>
                    <View style={{width: '50%'}}>
                        <Text style={{fontSize: 18}}>{activitySummary.user.full_name}</Text>
                        {!!team &&
                        <Badge style={styles.badgeLeft}>
                            <Text style={styles.badgeText}>{team.name}</Text>
                        </Badge>
                        }
                    </View>
                    <View style={{paddingTop:10,paddingRight:10,paddingLeft:10}}>
                        <Text style={styles.lightBadge}>
                            <Text style={{fontSize: 16}}>{activitySummary.count}</Text>
                        </Text>
                    </View>
                    <View style={{width: '15%', paddingTop:15}}>
                        <Text style={{fontSize: 16}}>{score}</Text>
                    </View>
                </View>
                }
                {theme !== 'grey' &&
                <View style={styles.viewRow}>
                    <View style={{width: '10%', paddingRight: 20}}>
                        <UserAvatar size={35} user={activitySummary.user} navigator={navigator} />
                    </View>
                    <View style={{width: '50%'}}>
                        <Text style={{fontSize: 13,alignSelf:'flex-start',lineHeight:35,marginLeft:10}}>{activitySummary.user.full_name}</Text>
                    </View>
                    <View style={{width: '20%', paddingTop: 0}}>
                        <Text style={styles.darkBadge}>
                            <Text>{activitySummary.count}</Text>
                        </Text>
                    </View>
                    <View style={{width: '20%', paddingTop: 5}}>
                        <Text style={{fontSize: 16}}>{score}</Text>
                    </View>
                </View>
                }
            </View>
        )
    }
}

export default ActivitySummaryListItem
