import React, { Component } from 'react';
import { connect } from 'react-redux';

import {Dimensions, View} from 'react-native';
import { getTimeRangedMessages } from '../../redux/local_storage';

import { getActivityRecords, getActivityRecordsIsFetching } from '../../redux/reducers';
import { activityRecordSelector } from '../../redux/selectors/activity_records';
import { fetchActivityRecords } from '../../redux/actions/activity_record';
import ActivityRecordGraph from '../../react/components/ActivityRecordGraph';
import ActivityRecordDetailLists from '../../react/components/ActivityRecordDetailLists';
import ActivityRecordUserListItem from '../../react/components/ActivityRecordUserListItem';
import styles from '../../theme';

import * as formatUtils from '../../lib/format_utils';
import * as activityUtils from '../../lib/activity_utils';
import * as graphUtils from '../../lib/graph_utils';
import _ from 'lodash';

const { width,height } = Dimensions.get('window');

import { Container, Content, Spinner, Text } from 'native-base';

export const mapStateToProps = (state, ownProps) => {
    return {
    }
}

export class LiveActivityRecordDetailScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            graphData: null,
            listData: []
        };
    }

    componentDidMount() {
        const { activeUser, liveActivity } = this.props;

        const toGraphESTData = getTimeRangedMessages(activeUser, false);

        liveActivity.data_summary = activeUser.meta_data;

        if(!this.state.graphData && toGraphESTData) {
            const { summaryData, listData} = activityUtils.getSummaryData(liveActivity, activeUser.meta_data);
            // const listData = formatUtils.loadListData(activeUser, liveActivity);
            const graphData = formatUtils.loadGraphData(toGraphESTData, activeUser, liveActivity);
            this.setState({summaryData, listData, graphData});
        }
    }

    render() {
        const { liveActivity, navigator, activeUser } = this.props;
        const { summaryData, listData, graphData } = this.state;

        if(!liveActivity || _.size(liveActivity)===0) return(<View></View>);

        const graphHeight = height*.3;
        const rowHeight = height*.1;

        return (
            <Content>
                {!!liveActivity &&
                <View style={{flex:1}}>

                    <View style={{height:rowHeight}}>
                        <ActivityRecordUserListItem
                            item={liveActivity}
                            user={activeUser.user}
                            activity={liveActivity}
                            activityIndex={0}
                            navigator={navigator}
                        />
                    </View>

                    <View style={{flex:1}}>
                        <ActivityRecordSummary
                            summaryData={summaryData}
                        />
                    </View>

                    <View style={{height:graphHeight}}>
                        <ActivityRecordGraph
                            itemData={graphData}
                            codData={codData}
                            height={graphHeight}
                        />
                    </View>
                    <View style={{flex:1}}>
                        <ActivityRecordDetailLists
                            listData={listData}
                            activity={liveActivity}
                            activityRecord={liveActivity}
                            itemWidth={width}
                        />
                    </View>

                </View>
                }
            </Content>





        )
    }
}

export default connect(
    mapStateToProps,
    {  }
)(LiveActivityRecordDetailScreen);
