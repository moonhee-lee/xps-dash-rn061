import React, { Component } from 'react';
import { View, Text } from 'react-native';
import {Container, Content} from "native-base";
import BottomTabBar from '../../react/components/BottomTabBar';
import { SearchBar, Icon } from 'react-native-elements';

/**
 * react/screens/SearchScreen
 * *
 */
class SearchScreen extends Component {
    render() {
        const { navigator } = this.props;
        return (
            <Container>
                <Content>
                    <View>
                        <SearchBar
                            searchIcon={<Icon name="search" />}
                            //onChangeText={someMethod}
                            //onClear={someMethod}
                            placeholder='Search for Athletes, Teams, Organizations, etc.'
                            placeholderTextColor={'#666'}
                            containerStyle={{backgroundColor:'#f7f7f7'}}
                            inputContainerStyle={{backgroundColor:'#f7f7f7',height:100}}
                            inputStyle={{backgroundColor:'#f7f7f7',color:'#666',fontSize:14}}
                        />
                    </View>
                </Content>
                <BottomTabBar
                    navigator={navigator}
                />
            </Container>
        )
    }
}

export default SearchScreen;
