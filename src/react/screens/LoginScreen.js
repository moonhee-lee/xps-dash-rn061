import React, { Component } from 'react';
import { connect } from 'react-redux';
import validate from 'validate.js';

import { ImageBackground, View, Image } from 'react-native';
import {Input, Content, Item, Text, Button, Icon, Toast, Root} from 'native-base';

import { login } from '../../redux/actions/auth';
import { getAuthState } from '../../redux/reducers/auth';
import styles from '../../theme';
import {LiveActivityRecordDetailScreen, mapStateToProps} from './LiveActivityRecordDetailScreen';

/**
 * react/screens/LoginScreen
 * *
 */
class LoginScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            usernameError: '',
            password: '',
            passwordError: '',
            formError: false,
            loginError: ''
        };
    }

    validateUsername = () => {
        // validate the current username input value, update error states as necessary
        const { username } = this.state;
        const usernameError = validate.single(username, {
            presence: {
                allowEmpty: false,
                message: 'A username is required to sign in.'
            }
        });
        const error = usernameError ? usernameError[0] : null;
        //this.setState({formError:!!error, usernameError: error });
        if(error) {
            Toast.show({
                text: 'A username is required to sign in.',
                position: 'top',
                type: 'danger'
            });
        }``
        return error;
    }

    validatePassword = () => {
        // validate the current password input value, update error states as necessary
        const { password } = this.state;
        const passwordError = validate.single(password, {
            presence: {
                allowEmpty: false,
                message: 'Your password is required to sign in.'
            }
        });
        const error = passwordError ? passwordError[0] : null;
        //this.setState({formError:!!error, passwordError: error });
        if(error) {
            Toast.show({
                text: 'Your password is required to sign in.',
                position: 'top',
                type: 'danger'
            });
        }

        return error;
    }

    clearErrors = () => {
        this.setState({usernameError:'',passwordError:'',loginError:'', formError:false});
    }

    handleSubmit = () => {
        const { login, getAuthState } = this.props;
        const { username, password, formError } = this.state;

        this.clearErrors();
        // validate our fields
        const usernameError = this.validateUsername();
        const passwordError = this.validatePassword();

        if (!usernameError && !passwordError) {
            login(username, password).then((response) => {
                if (response === undefined) {
                    Toast.show({
                        text: 'Cannot connect to cloud services, please connect to the internet to sign in.',
                        position: 'top',
                        type:'danger'
                    });
                } else if(response.status === 400) {
                    //this.setState({ loginError: 'Incorrect User or Password' });
                    Toast.show({
                        text: 'Incorrect User or Password',
                        position: 'top',
                        type:'danger'
                    });
                }
            }).catch((err) => {
                console.log(err);
            });
        }
    }

    handleLongPress = () => {
        this.props.navigator.push({
            screen: 'AdminConfigScreen',
            title: 'System Administration',
            passProps: {
                user: null
            },
        });
    }

    render() {
        const { username, usernameError, password, passwordError, loginError, formError } = this.state;
        return (
            <Root>
                <ImageBackground source={require('../../images/Arizona-30.jpg')} style={{width: '100%', height: '100%'}}>
                    <View style={styles.overlay}>
                    <Content style={{width: '100%', paddingTop: '50%'}}>

                        <View style={[styles.centerContainer, styles.loginLogoBlock]}>
                            <Image
                              style={{width: 80, height: 140}}
                              source={require('../../images/dash-logo-full.png')}
                            />
                        </View>
                        <Item style={styles.loginItemStyle}>
                            <Icon name="user" type="FontAwesome" style={{color:"#e7e7e7",fontSize:16}} />
                            <Input
                                value={username}
                                onChangeText={(username) => this.setState({ username: username.trim().toLowerCase() })}
                                placeholder={'Username'}
                                inputStyle={styles.loginInputStyle}
                                containerStyle={styles.loginContainerStyle}
                                textStyle={styles.loginTextStyle}
                            />
                        </Item>
                        {!!usernameError &&
                        <Text style={styles.errorStyle}>{usernameError}</Text>
                        }

                        <Item style={styles.loginItemStyle}>
                            <Icon name="lock" type="FontAwesome" style={{color:"#e7e7e7",fontSize:16}} />
                            <Input
                                value={password}
                                onChangeText={(password) => this.setState({ password: password.trim() })}
                                placeholder={'Password'}
                                secureTextEntry={true}
                                inputStyle={styles.loginInputStyle}
                                containerStyle={styles.loginInputStyle}
                                textStyle={styles.loginInputStyle}
                            />
                        </Item>
                        {!!passwordError &&
                        <Text style={styles.errorStyle}>{passwordError}</Text>
                        }
                        {!!loginError &&
                        <Text style={styles.errorStyle}>{loginError}</Text>
                        }

                        <Button loginLogoBlock onPress={this.handleSubmit} style={styles.loginButtonStyle} onLongPress={this.handleLongPress}>
                            <Text style={styles.loginButtonText}>Login</Text>
                        </Button>

                    </Content>
                    </View>
                </ImageBackground>
            </Root>
        )
    }
}

export default connect(
    null,
    {
        login, getAuthState
    }
)(LoginScreen);
