import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import { connect } from 'react-redux';
import {Container, Header, Content, List, ListItem, Text, Spinner, Item, View, Icon, Button} from 'native-base';
import _ from 'lodash';

import { getLoggedInUser, getCurrentTeamData, getTeams, getCurrentOrg, getCurrentTeam } from '../../redux/reducers';
import { logout } from '../../redux/actions/auth';
import { fetchTeamsByOrg } from '../../redux/actions/team';
import { startAppAnonymous } from '../../react/screens';
import TopMessageBar from '../../react/components/TopMessageBar';
import styles from '../../theme';
import {ButtonGroup} from "react-native-elements";
import { Alert } from 'react-native';
import SyncButton from '../../react/components/SyncButton';

import { localStorage, fetchLocalDataCount } from '../../lib/local_storage';

export const mapStateToProps = (state) => {
    return {
        user: getLoggedInUser(state),
        currentOrg: getCurrentOrg(state),
        teams: getTeams(state),
        currentTeam: getCurrentTeam(state),
    }
}

/**
 * react/screens/MoreScreen
 * *
 */
class MoreScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            team: null,
            syncItems: 0,
            debugMode: global.debugMode
        };
        this.clearLocalStorage = this.clearLocalStorage.bind(this);
    }

    componentDidMount() {
        const { teams, currentOrg, fetchTeamsByOrg } = this.props;
        if(teams) {
            const team = _.find(teams, (o) => o.id === currentTeam);
            this.setState({team:team});
        }
    }

    componentDidUpdate(prevProps,prevState) {
        const { teams, currentTeam, currentOrg } = this.props;
        if(teams && teams!==prevProps.teams) {
            const team = _.find(teams, (o) => o.id === currentTeam);
            this.setState({team:team});
        }
    }

    handleLogout = () => {
        const { logout } = this.props;
        logout();
        startAppAnonymous();
    }

    loadConfig = () => {
        const { user } = this.props;
        Navigation.push(this.props.componentId, {
            component: {
                name: 'AdminConfigScreen',
                passProps: {
                    user
                },
                options: {
                    topBar: {
                        title: {
                            text: 'XPS Cloud Configuration'
                        }
                    }
                }
            }
        });
    }

    loadFirmwareUpdate = () => {
        Navigation.push(this.props.componentId, {
            component: {
                name: 'FirmwareScreen',
                passProps: {
                },
                options: {
                    topBar: {
                        title: {
                            text: 'XPS Tracker Firmware'
                        }
                    }
                }
            }
        });
    }

    loadFeatures = () => {
        Navigation.push(this.props.componentId, {
            component: {
                name: 'FeatureScreen',
                passProps: {
                },
                options: {
                    topBar: {
                        title: {
                            text: 'Features'
                        }
                    }
                }
            }
        });
    }

    loadSystemStatusScreen = () => {
        Navigation.push(this.props.componentId, {
            component: {
                name: 'SystemStatusScreen',
                passProps: {
                },
                options: {
                    topBar: {
                        title: {
                            text: 'System Configuration'
                        }
                    }
                }
            }
        });
    }

    setDebugMode(index) {
        console.log('changing debug mode: '+index);
        localStorage.setItem('xps:debugMode', index);
        global.debugMode = index;
        this.setState({debugMode:index});
    }


    clearLocalStorage() {
        const { fetchLocalDataCount } = this.props;
        Alert.alert(
            'Clear Local Storage',
            'Are you sure you want to delete all users, sessions and activities in local storage? They most likely have not been synced with the Cloud...',
            [
                {
                    text: 'Yes', onPress: () => {
                        localStorage.removeItem('xps:allOrgMembers');
                        localStorage.removeItem('xps:allSystems');
                        localStorage.removeItem('xps:allTags');
                        localStorage.getAllDataForKey('xps:orgMembers').then(orgMembers => {
                            _.each(orgMembers, (orgMember) => {
                                localStorage.removeItem('xps:orgMembers', orgMember.localID);
                            });
                            fetchLocalDataCount();
                        });
                        localStorage.getAllDataForKey('xps:sessions').then(sessions => {
                            _.each(sessions,(session) => {
                                localStorage.removeItem('xps:sessions', session.localID);
                            });
                            fetchLocalDataCount();
                        });
                        localStorage.getAllDataForKey('xps:activities').then(activities => {
                            _.each(activities,(activity) => {
                                localStorage.removeItem('xps:activities', activity.localID);
                            });
                            fetchLocalDataCount();
                        });
                    }
                },
                {
                    text: 'No', onPress: () => {

                    }
                }
            ],
            {cancelable: true},
        );

    }

    render() {
        const { user, navigator } = this.props;
        const { team, syncItems } = this.state;

        if (!user) {
            return (
                <Container></Container>
            )
        }

        return (
            <Container>
                <Content style={[(syncItems>0?{marginTop:40}:{}),styles.contentStyle,{marginTop:40}]}>
                    <List>
                        {!!user && user.is_staff &&
                        <ListItem onPress={this.loadFirmwareUpdate} >
                            <Text style={{width:'95%'}}>Firmware Update</Text>
                        </ListItem>
                        }
                        {!!user && user.is_staff &&
                        <ListItem onPress={this.loadConfig}>
                            <Text>XPS Cloud Configuration</Text>
                        </ListItem>
                        }
                        <ListItem onPress={this.loadSystemStatusScreen}>
                            <Text>System Configuration</Text>
                        </ListItem>
                        <ListItem onPress={this.loadFeatures}>
                            <Text>Features</Text>
                        </ListItem>
                        {!!user && user.is_staff && !!global.debugMode &&
                        <ListItem onPress={this.clearLocalStorage}>
                            <Text>Clear Local Storage Items</Text>
                        </ListItem>
                        }
                        <ListItem onPress={this.handleLogout}>
                            <Text>Sign Out</Text>
                        </ListItem>
                    </List>
                </Content>
                <TopMessageBar
                    navigator={navigator}
                    type='success'
                    message={'Welcome '+user.first_name}
                />
                {!!user && user.is_staff &&
                <View style={{position: 'absolute', bottom: 20, width: '100%'}}>
                    <Text style={{width: '100%', textAlign: 'center', color: '#666', fontWeight: 'bold', padding: 10}}>Debug
                        Mode:</Text>
                    <Item style={{marginBottom: 10, borderWidth: 0, borderBottomWidth: 0}}>
                        <ButtonGroup
                            onPress={(index) => this.setDebugMode(index)}
                            selectedIndex={this.state.debugMode}
                            buttons={['Off', 'On']}
                            containerStyle={{height: 40, width: '90%', backgroundColor: '#f7f7f7', borderWidth: 0}}
                            buttonStyle={styles.buttonGroupStyle}
                            selectedButtonStyle={styles.buttonGroupSelectedStyle}
                            selectedBackgroundColor={'#f7f7f7'}
                            innerBorderStyle={{width: 0, color: '#f7f7f7'}}
                            textStyle={{color: '#666', fontWeight: 'bold'}}
                            selectedTextStyle={{color: '#fff', fontWeight: 'bold'}}
                        />
                    </Item>
                </View>
                }
                <SyncButton
                    navigator={navigator}
                    setSyncItems={(syncItems) => this.setState({syncItems})}
                />
            </Container>
        )
    }
}

export default connect(
    mapStateToProps,
    {
        logout, fetchTeamsByOrg, fetchLocalDataCount
    }
)(MoreScreen);
