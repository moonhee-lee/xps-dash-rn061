import React, { Component } from 'react';
import { connect } from 'react-redux';

import {View, Text, TouchableOpacity} from 'react-native';
import {Container, Content, Icon} from "native-base";

import { localStorage } from '../../lib/local_storage';

import { testStartSound } from '../../redux/actions/system';

import styles from '../../theme';

const sounds = [
    {id:1,file:"swimStartBeep",type:"m4a",name:"Swim Start"},
    {id:2,file:"buzzer",type:"m4a",name:"Buzzer"},
    {id:3,file:"basketballBuzzer",type:"m4a",name:"Basketball Buzzer"},
    {id:4,file:"pistol",type:"m4a",name:"Starting Pistol"},
    {id:5,file:"bikeHorn",type:"m4a",name:"Bike Horn"},
    {id:6,file:"ahooga",type:"m4a",name:"Ahooga"},
    {id:7,file:"carnHorn",type:"m4a",name:"Car Horn"},
    {id:8,file:"trainHorn",type:"m4a",name:"Train Horn"},
    {id:9,file:"airHorn",type:"m4a",name:"AirHorn"},
];

class BeepStartSoundScreen extends Component {
    constructor(props) {
        super(props);
        this.state = { currentSound:global.beepSound }
    }

    componentDidMount() {
    }

    _onChange(index) {
        const sound = sounds[index];
        global.beepSound = sound;
        this.setState({currentSound:sound});
        localStorage.setItem('xps:beepSound', sound);
    }

    testStartSound(index) {
        const { testStartSound } = this.props;
        const sound = sounds[index];
        testStartSound(sound);
    }

    render() {
        const { currentSound } = this.state;

        return (
            <Container>
                <Content style={styles.contentStyle}>
                    <View>
                        {sounds && sounds.map((sound,index) =>
                            <TouchableOpacity key={index} style={(!!currentSound && currentSound.id===sound.id)?{backgroundColor:'#6AC893'}:{backgroundColor:'#f7f7f7'}} onPress={() => this._onChange(index)}>
                                <View style={styles.sessionList}>
                                    <View style={{width:'100%',flex: 1,flexDirection: 'row'}}>
                                        <Text style={[{flex:10},styles.title,(!!currentSound && currentSound.id===sound.id)?{color:'#fff'}:{}]}>{sound.name}</Text>
                                        {!!currentSound && currentSound.id===sound.id &&
                                        <Icon type="FontAwesome" name="check" style={{flex:1,alignSelf:"flex-end",color:"#fff"}} />
                                        }
                                        <TouchableOpacity key={index} style={{paddingLeft:20}} onPress={() => this.testStartSound(index)}>
                                            <Icon type="FontAwesome" name="play-circle" style={{flex:1,alignSelf:"flex-end",color:"#333"}} />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        )}
                    </View>
                </Content>
            </Container>
        )
    }
}

export default connect(
    null,
    { testStartSound }
)(BeepStartSoundScreen);


