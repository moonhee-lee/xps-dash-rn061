import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Navigation } from 'react-native-navigation';

import {RefreshControl, ScrollView, TouchableOpacity, View} from 'react-native';
import FadeSlideItem from '../../react/components/FadeSlideItem';
import ActivityUserRecordListItem from '../../react/components/ActivityUserRecordListItem';
import { UserAvatar } from '../../react/components/UserAvatar';

import { getActivityRecords, getActivityRecordsIsFetching, getActiveSession, getActiveActivity, getActivityRecordsByUserId, getActivityData } from '../../redux/reducers';
import { activityRecordSelector } from '../../redux/selectors/activity_records';
import { fetchActivityRecords } from '../../redux/actions/activity_record';
import { fetchActivityDataByRecord } from '../../redux/actions/activity_data';
import styles from '../../theme';
import * as formatUtils from '../../lib/format_utils';

import _ from 'lodash';

import {Badge, Container, Content, Spinner, SwipeRow, Text} from 'native-base';
import moment from "moment";

export const mapStateToProps = (state, ownProps) => {
    return {
        activeSession: getActiveSession(state),
        activeActivity: getActiveActivity(state),
        activityRecords: activityRecordSelector(state, ownProps),
        isFetching: getActivityRecordsIsFetching(state),
        activityData: getActivityData(state),
    }
}

export class ActivityRecordDetailListScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing:false
        };
    }

    componentDidMount() {
        const { activityRecords, user, activity, activeSession, fetchActivityRecords, session } = this.props;
        const _session = session?session:activeSession;
        fetchActivityRecords(activity.type_hash, parseInt(_session.id));
    }

    componentDidUpdate(prevProps,prevState) {
        const { activityRecords, activityData } = this.props;

        if(!_.isEqual(activityRecords,prevProps.activityRecords)) {
            _.each(activityRecords,(r) => fetchActivityDataByRecord(r));
        }
        if(!_.isEqual(activityData,prevProps.activityData)) {

        }
    }

    _onPressButton = (item) => {
        const { activeActivity, user } = this.props;
        let display_name = item.display_name;
        if (item.type_definition.skating) {
            display_name = display_name.replace('sprint', item.type_definition.skating + ' skate');
        }
        if (item.type_definition.bobsled) {
            display_name = display_name.replace('sprint', 'bobsled');
        }
        if (item.type_definition.walking) {
            display_name = display_name.replace('sprint', 'walk');
        }
        let display_side = '';
        if (item.type_definition.side !== undefined && item.type_definition.side !== 'both') {
            display_side = ' ' + item.type_definition.side + ' side'
        }

        Navigation.push(this.props.componentId, {
            component: {
                name: 'ActivityRecordDetailScreen',
                passProps: {
                    activity:activeActivity,
                    activityRecord: item,
                    user
                },
                options: {
                    topBar: {
                        title: {
                            text: display_name + display_side + ' Record'
                        },
                        backButton: {
                            title: 'Records'
                        }
                    },
                    bottomTabs: {
                        visible: false,
                        drawBehind: true,
                        animate: true
                    }
                }
            }
        });
    }

    render() {
        const { activityRecords, activityData, navigator, activeSession, activity, isFetching, session } = this.props;
        const _session = session?session:activeSession;

        let team = null;
        if(!!_session && !!_session.team && !!_session.team_data)  team = (typeof(_session.team)==='object' && _session.team)?_session.team:_session.team_data;

        if (isFetching) {
            return (
                <Container style={{ alignSelf: "center" }}>
                    <Spinner />
                </Container>
            )
        }

        return (
            <Container>
                <ScrollView style={[styles.contentStyleNoBottom]}>

                    {!!activityRecords && activityRecords.length > 0 &&
                    activityRecords.map((act, index) =>
                        <FadeSlideItem
                            key={index}
                            index={index}
                        >
                            {act.type_definition.activity_type!=='freeform' && act.labels && !_.find(act.labels,(l)=>l==='incomplete') &&
                            !(act.type_definition.activity_type==='jump' && act.type_definition.triple_jump_type) &&
                            <TouchableOpacity onPress={() => this._onPressButton(act)}>
                                <ActivityUserRecordListItem
                                    id={act.id}
                                    activityRecord={act}
                                    navigator={navigator}
                                    session={_session}
                                    index={index}
                                    incomplete={false}
                                />
                            </TouchableOpacity>
                            }
                            {act.labels && _.find(act.labels,(l)=>l==='incomplete') &&
                            <View>
                                <ActivityUserRecordListItem
                                    id={act.id}
                                    activityRecord={act}
                                    navigator={navigator}
                                    session={_session}
                                    index={index}
                                    incomplete={true}
                                />
                            </View>
                            }
                            {(act.type_definition.activity_type==='freeform' || (act.type_definition.activity_type==='jump' && act.type_definition.triple_jump_type)) &&
                            <View>
                                <ActivityUserRecordListItem
                                    id={act.id}
                                    activityRecord={act}
                                    activityData={ _.filter(activityData,(a) => a.activity === act.id) }
                                    navigator={navigator}
                                    session={_session}
                                    index={index}
                                />
                            </View>
                            }
                        </FadeSlideItem>
                    )}
                </ScrollView>
            </Container>
        )
    }
}

export default connect(
    mapStateToProps,
    { fetchActivityRecords, fetchActivityDataByRecord }
)(ActivityRecordDetailListScreen);
