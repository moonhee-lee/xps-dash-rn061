import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import { connect } from 'react-redux';
import validate from 'validate.js'
import {
    Container,
    Content,
    H2,
    Item,
    Text,
    Button,
    Picker,
    Textarea,
    Spinner,
    View, Icon
} from 'native-base';

import { getCurrentOrg, getTeamsByOrg, getTeamIsFetching, getTeamsByCurrentOrg } from '../../redux/reducers';
import { fetchTeamsByOrg } from '../../redux/actions/team';
import { saveSession } from '../../redux/actions/session';
import { setActiveSession } from '../../redux/actions/active_session';
import styles from '../../theme';
import { Input } from 'react-native-elements';
import SaveBar from '../../react/components/SaveBar';
import {TouchableOpacity} from "react-native";

export const mapStateToProps = (state) => {
    return {
        currentOrg: getCurrentOrg(state),
        teams: getTeamsByCurrentOrg(state),
        isFetching: getTeamIsFetching(state)
    }
}

export class SessionCreateScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            nameError: '',
            description: '',
            descriptionError: '',
            team: '',
            teamError: '',
        };
        Navigation.events().bindComponent(this);
    }

    componentDidMount() {
        const { currentOrg, fetchTeamsByOrg, teams } = this.props;
        if (teams.length === 0) fetchTeamsByOrg(currentOrg.id);
    }

    navigationButtonPressed() {
        Navigation.dismissModal(this.props.componentId);
    }

    validateName = (name) => {
        return validate.single(name, {
            presence: {
                allowEmpty: false,
                message: 'A Session Name is required'
            }
        });
    }

    validateTeam = (team) => {
        return validate.single(team, {
            presence: {
                allowEmpty: false,
                message: 'You must select a Team'
            }
        });
    }

    handleSubmit = () => {
        const { currentOrg } = this.props;
        const { name, team, description } = this.state;
        const nameError = this.validateName(name);
        const teamError = this.validateTeam(team);

        this.setState({
            nameError: nameError ? nameError[0] : '',
            teamError: teamError ? teamError[0] : '',
        });

        if (!nameError && !teamError) {
            const { name, team, description } = this.state;
            const { saveSession, setActiveSession } = this.props;
            const sessionData = {
                name,
                description,
                team,
                org: currentOrg.id
            }

            saveSession(sessionData, this.props.navigator, null, setActiveSession);
        }
    }

    goToSessions() {
        this.props.navigator.resetTo({
            screen: 'SessionListScreen',
            title: 'Sessions',
            animated: true,
            animationType: 'fade',
        });
    }

    render() {
        const { name, nameError, description, descriptionError, team, teamError } = this.state;
        const { teams, isFetching } = this.props;

        if (isFetching) {
            return (
                <Container style={{ alignSelf: "center" }}>
                    <Spinner />
                </Container>
            )
        }

        return (
            <Container>
                <Content style={{width: '100%', padding: 20}}>
                    <Item style={{marginTop:20}}>
                        <Input
                            value={name}
                            onChangeText={(name) => this.setState({ name: name.trim() })}
                            placeholder={'Enter a Session Name'}
                            placeholderTextColor={'#666'}
                            inputStyle={styles.loginInputStyle}
                            containerStyle={styles.loginContainerStyle}
                            textStyle={styles.loginTextStyle}
                        />
                    </Item>
                    {nameError &&
                    <Text style={styles.inputErrorStyle}>{nameError}</Text>
                    }

                    {!!teams && teams.length &&
                    <Item picker style={{marginTop:20}}>
                        <Picker
                            mode="dropdown"
                            style={{ width: '100%',paddingLeft:5 }}
                            textStyle={{fontSize:16}}
                            placeholder="Select a Team"
                            placeholderStyle={{ color: "#666" }}
                            selectedValue={this.state.team}
                            onValueChange={(team) => this.setState({team: team})}
                        >
                            {teams && teams.map( (team) =>
                                <Picker.Item label={team.name} value={team.id} key={team.id} />
                            )}
                        </Picker>
                    </Item>
                    }
                    {teamError &&
                    <Text style={styles.inputErrorStyle}>{teamError}</Text>
                    }

                    <Item style={{borderBottomWidth:0}}>
                        <Textarea bordered color={'#666'} placeholderTextColor={'#666'}
                                  style={{ width: '100%', marginTop: 10, marginBottom: 10, fontSize:16 }}
                                  rowSpan={5}
                                  onChangeText={(description) => this.setState({ description: description.trim() })}
                                  placeholder="Description"
                        />
                    </Item>
                    {descriptionError &&
                    <Text style={styles.inputErrorStyle}>{descriptionError}</Text>
                    }
                    <TouchableOpacity onPress={() => this.goToSessions()}>
                        <View style={styles.footer_grey}>
                            <Icon name='angle-left' type="FontAwesome" />
                            <Text>Or select an existing session</Text>
                        </View>
                    </TouchableOpacity>
                </Content>
                <SaveBar
                    onPressSave={this.handleSubmit}
                />
            </Container>
        )
    }
}


export default connect(
    mapStateToProps,
    { fetchTeamsByOrg, saveSession, setActiveSession }
)(SessionCreateScreen);

