import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Container, Content, Text, List, ListItem, View, Item} from 'native-base';

import styles from '../../theme';
import { localStorage } from '../../lib/local_storage';

export const mapStateToProps = (state, ownProps) => {
    return {
    }
}

class FeatureScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            enableWalkingMode: global.enableWalkingMode,
            enableSingleLegMode: global.enableSingleLegMode,
            enableSkatingMode: global.enableSkatingMode,
            enableBobsledMode: global.enableBobsledMode,
            enableStrideLengthView: global.enableStrideLengthView,
            enableTagPosition: global.enableTagPosition,
            enableManualHorizontalJump: global.enableManualHorizontalJump
        };
    }

    toggleSingleLegMode = () => {
        // TODO Use action instead of global
        let enabled = !global.enableSingleLegMode;
        console.log('changing single leg mode: '+enabled);
        localStorage.setItem('xps:enableSingleLegMode', enabled);
        global.enableSingleLegMode = enabled;
        this.setState({enableSingleLegMode:enabled});

        if (enabled === true && global.enableBobsledMode === true) {
            this.toggleBobsledMode()
        }
    }

    toggleSkatingMode = () => {
        // TODO Use action instead of global
        let enabled = !global.enableSkatingMode;
        console.log('changing skating mode: '+enabled);
        localStorage.setItem('xps:enableSkatingMode', enabled);
        global.enableSkatingMode = enabled;
        this.setState({enableSkatingMode:enabled});

        if (enabled === true) {
            if (global.enableBobsledMode === true) {
                this.toggleBobsledMode();
            }

            if (global.enableWalkingMode === true) {
                this.toggleWalkingMode();
            }
        }
    }

    toggleBobsledMode = () => {
        // TODO Use action instead of global
        let enabled = !global.enableBobsledMode;
        console.log('changing bobsled mode: '+enabled);
        localStorage.setItem('xps:enableBobsledMode', enabled);
        global.enableBobsledMode = enabled;
        this.setState({enableBobsledMode:enabled});

        if (enabled === true) {
            if (global.enableSkatingMode === true) {
                this.toggleSkatingMode();
            }

            if (global.enableSingleLegMode === true) {
                this.toggleSingleLegMode();
            }

            if (global.enableWalkingMode === true) {
                this.toggleWalkingMode();
            }
        }
    }

    toggleWalkingMode = () => {
        // TODO Use action instead of global
        let enabled = !global.enableWalkingMode;
        console.log('changing walking mode: '+enabled);
        localStorage.setItem('xps:enableWalkingMode', enabled);
        global.enableWalkingMode = enabled;
        this.setState({enableWalkingMode:enabled});

        if (enabled === true) {
            if (global.enableBobsledMode === true) {
                this.toggleBobsledMode();
            }

            if (global.enableSkatingMode === true) {
                this.toggleSkatingMode();
            }
        }
    }

    toggleStrideLengthView = () => {
        // TODO Use action instead of global
        let enabled = !global.enableStrideLengthView;
        console.log('changing stride length view: '+enabled);
        localStorage.setItem('xps:enableStrideLengthView', enabled);
        global.enableStrideLengthView = enabled;
        this.setState({enableStrideLengthView:enabled});
    }

    toggleTagPosition = () => {
        // TODO Use action instead of global
        let enabled = !global.enableTagPosition;
        console.log('changing tag position: '+enabled);
        localStorage.setItem('xps:enableTagPosition', enabled);
        global.enableTagPosition = enabled;
        this.setState({enableTagPosition:enabled});
    }

    toggleManualHorizontalJump = () => {
        // TODO Use action instead of global
        let enabled = !global.enableManualHorizontalJump;
        console.log('changing manual horizontal jump: '+enabled);
        localStorage.setItem('xps:enableManualHorizontalJump', enabled);
        global.enableManualHorizontalJump = enabled;
        this.setState({enableManualHorizontalJump:enabled});
    }

    render() {

        const { enableSingleLegMode, enableWalkingMode, enableSkatingMode, enableBobsledMode, enableStrideLengthView, enableTagPosition, enableManualHorizontalJump } = this.state;

        let textWalkingMode = (enableWalkingMode === true ? 'Disable' : 'Enable') + ' Walking Mode';
        let textSingleLegMoe = (enableSingleLegMode === true ? 'Disable' : 'Enable') + ' Single Leg Mode';
        let textSkatingMode = (enableSkatingMode === true ? 'Disable' : 'Enable') + ' Skating Mode';
        let textBobsledMode = (enableBobsledMode === true ? 'Disable' : 'Enable') + ' Bobsled Mode';
        let textStrideLengthView = (enableStrideLengthView === true ? 'Disable' : 'Enable') + ' Stride Length View';
        let textTagPosition = (enableTagPosition === true ? 'Disable' : 'Enable') + ' Tag Position';
        let textManualHorizontalJump = (enableManualHorizontalJump === true ? 'Disable' : 'Enable') + ' Manual Horizontal Jump';

        return (
            <Container>
                <Content style={[styles.contentStyle]}>
                    <List>
                        <ListItem onPress={this.toggleWalkingMode}>
                            <Text>{textWalkingMode}</Text>
                        </ListItem>
                        <ListItem onPress={this.toggleBobsledMode}>
                            <Text>{textBobsledMode}</Text>
                            <Text style={styles.typeLosengeSecondary}>beta</Text>
                        </ListItem>
                        <ListItem onPress={this.toggleSkatingMode}>
                            <Text>{textSkatingMode}</Text>
                        </ListItem>
                        <ListItem onPress={this.toggleSingleLegMode}>
                            <Text>{textSingleLegMoe}</Text>
                            <Text style={styles.typeLosengeSecondary}>beta</Text>
                        </ListItem>
                        <ListItem onPress={this.toggleStrideLengthView}>
                            <Text>{textStrideLengthView}</Text>
                            <Text style={styles.typeLosengeSecondary}>beta</Text>
                        </ListItem>
                        <ListItem onPress={this.toggleTagPosition}>
                            <Text>{textTagPosition}</Text>
                        </ListItem>
                        <ListItem onPress={this.toggleManualHorizontalJump}>
                            <Text>{textManualHorizontalJump}</Text>
                        </ListItem>
                    </List>
                </Content>
            </Container>
        )
    }
}

export default connect(
    mapStateToProps,
    {
    }
)(FeatureScreen);
