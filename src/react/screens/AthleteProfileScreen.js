import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import { connect } from 'react-redux';
import _ from 'lodash';

import { Container, Content, View, Text } from 'native-base';

import { fetchActivitiesByUser } from '../../redux/actions/activity';
import { fetchSessionsByOrg } from '../../redux/actions/session';
import { fetchMembersByOrg } from '../../redux/actions/org_member';
import { sessionsSelector } from '../../redux/selectors/sessions';
import { getActivitiesByUser, getSessionsByOrg, getCurrentOrg, getMembersByOrg, getCurrentTeam, getActivitiesByUserId } from '../../redux/reducers';
import { TouchableOpacity, ImageBackground } from "react-native";
import FadeSlideItem from '../../react/components/FadeSlideItem';
import SessionListItem from '../../react/components/SessionListItem';

import styles from '../../theme';
import * as formatUtils from '../../lib/format_utils';

export const mapStateToProps = (state, ownProps) => {
    return {
        currentOrg: getCurrentOrg(state),
        currentTeam: getCurrentTeam(state),
        activities: getActivitiesByUserId(state, ownProps.athlete.id),
        sessions: sessionsSelector(state),
        orgMembers: getMembersByOrg(state)
    }
}

export class AthleteProfileScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {orgMember:null,sessions:null};
        Navigation.events().bindComponent(this);
    }

    componentDidMount() {
        const { athlete, activities, currentTeam, fetchActivitiesByUser, currentOrg, fetchSessionsByOrg, sessions, orgMembers, fetchMembersByOrg } = this.props;
        if((!sessions || sessions.length===0) && currentOrg && currentOrg.id) {
            fetchSessionsByOrg(currentOrg.id);
        }
        if(currentOrg && currentOrg.id && (!orgMembers || orgMembers.length===0)) {
            fetchMembersByOrg(currentOrg.id);
        }
        console.log('fetching activities');
        fetchActivitiesByUser(athlete.id);
    }

    componentDidUpdate(prevProps, prevState) {
        const { athlete, orgMembers, activities, sessions } = this.props;

        if(orgMembers.length>0 && !this.state.orgMember) {
            console.log('setting org member');
            const orgMember = _.find(orgMembers, (t) => t.user === athlete.id);
            if(orgMember) this.setState({orgMember:orgMember});
        }
        if(activities.length>0 && !this.state.sessions) {
            console.log('setting sessions');
            const _sessions = [];
            _.each(activities,(activity) => {
               const _session = _.find(sessions,(s) => s.id === activity.session);
               if(_session) {
                   _sessions[activity.session] = _session;
               }

            })
            //const sessions = _.uniqBy(activities, 'session');
            this.setState({sessions: _sessions});
        }
    }

    navigationButtonPressed() {
        Navigation.dismissModal(this.props.componentId);
    }

    _onPressSessionButton = (session) => {
        this.props.navigator.push({
            screen: 'SessionDetailScreen',
            title: session.name+' Activities',
            backButtonTitle: '',
            passProps: {
                session: session,
                fromUserProfile: true
            }
        });
    }

    render() {
        const { athlete, activities } = this.props;
        const { orgMember, sessions } = this.state;

        let bests = null;
        if(orgMember && orgMember.user_data && orgMember.user_data.summary_data && orgMember.user_data.summary_data.personal_bests) {
            bests = orgMember.user_data.summary_data.personal_bests;
        }

        return (
            <Container>
                <Content style={{}}>
                    {athlete.image_url &&
                    <View style={{height:300,width:'100%',borderBottomWidth:1,borderBottomColor:'#d6d6d6'}}>
                        <ImageBackground source={{uri: athlete.image_url}} style={{height:'100%',width:'100%'}}>
                            <Text style={{
                                alignSelf: 'center',
                                fontSize: 20,
                                fontWeight: 'bold',
                                padding: 20,
                                position:'absolute',bottom:0,
                                shadowColor:'#fff',
                                shadowOffset:{width:0,height:0},
                                shadowRadius: 3,
                                shadowOpacity:.8
                            }}>{athlete.full_name}</Text>
                        </ImageBackground>
                    </View>
                    }
                    {!athlete.image_url &&
                    <View style={{height:60,width:'100%',borderBottomWidth:1,borderBottomColor:'#d6d6d6',backgroundColor:'#333'}}>
                        <Text style={{
                            alignSelf: 'center',
                            fontSize: 20,
                            fontWeight: 'bold',
                            lineHeight:60,
                            color:'#fff'
                        }}>{athlete.full_name}</Text>
                    </View>
                    }

                    {!!bests &&
                    <View style={{borderTopWidth:3,borderTopColor:'#d7d7d7',borderBottomWidth:3,borderBottomColor:'#ccc'}}>
                            {!!bests.broad_jump_m &&
                            <View style={[styles.viewRowSpaced,{paddingLeft:20,paddingRight:20,width:'100%'}]}>
                                <Text style={[styles.tableRow]}>Best Broad Jump</Text>
                                <Text style={styles.tableRow}>{formatUtils.formatDistance(bests.broad_jump_m)}</Text>
                            </View>
                            }
                            {!!bests.vertical_jump_m &&
                            <View style={[styles.viewRowSpaced,{paddingLeft:20,paddingRight:20,width:'100%'}]}>
                                <Text style={[styles.tableRow]}>Best Vertical Jump</Text>
                                <Text style={styles.tableRow}>{formatUtils.formatDistance(bests.vertical_jump_m)}</Text>
                            </View>
                            }
                            {!!bests.reaction_time_s &&
                            <View style={[styles.viewRowSpaced,{paddingLeft:20,paddingRight:20,width:'100%'}]}>
                                <Text style={[styles.tableRow]}>Best Reaction Time</Text>
                                <Text style={styles.tableRow}>{formatUtils.formatTime(bests.reaction_time_s)}</Text>
                            </View>
                            }
                            {!!bests.max_velocity_mps &&
                            <View style={[styles.viewRowSpaced,{paddingLeft:20,paddingRight:20,width:'100%'}]}>
                                <Text style={[styles.tableRow]}>Best Max Velocity</Text>
                                <Text style={styles.tableRow}>{formatUtils.formatTime(bests.max_velocity_mps)}</Text>
                            </View>
                            }
                    </View>
                    }

                    {!!sessions && sessions.map((item, index) =>
                        <TouchableOpacity
                            onPress={() => this._onPressSessionButton(item)}
                            key={index}>
                            <FadeSlideItem
                                index={index}
                            >
                                <SessionListItem
                                    id={item}
                                    session={item}
                                    navigator={navigator}
                                    index={index}
                                />
                            </FadeSlideItem>
                        </TouchableOpacity>
                    )}
                </Content>
            </Container>
        )
    }
}

export default connect(
    mapStateToProps,
    { fetchActivitiesByUser, fetchMembersByOrg, fetchSessionsByOrg }
)(AthleteProfileScreen);
