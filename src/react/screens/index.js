import React from 'react';
import { Navigation } from 'react-native-navigation';
import { localStorage } from '../../lib/local_storage';

import ActivityLeaderboardScreen from '../../react/screens/ActivityLeaderboardScreen';
import ActivityRecordDetailScreen from '../../react/screens/ActivityRecordDetailScreen';
import ActivityRecordDetailListScreen from '../../react/screens/ActivityRecordDetailListScreen';
import ActivityRecordListScreen from '../../react/screens/ActivityRecordListScreen';
import SessionDetailScreen from '../../react/screens/SessionDetailScreen';
import SessionListScreen from '../../react/screens/SessionListScreen';

import SearchScreen from '../../react/screens/SearchScreen';
import BeepStartSoundScreen from '../../react/screens/BeepStartSoundScreen';

import LiveModeScreen from '../../react/screens/LiveModeScreen';
import LiveModeTriggerScreen from '../../react/screens/LiveModeTriggerScreen';
import SessionCreateScreen from '../../react/screens/SessionCreateScreen';
import LiveActivitySetupScreen from '../../react/screens/LiveActivitySetupScreen';
import LiveAthleteSelectScreen from '../../react/screens/LiveAthleteSelectScreen';
import LiveActivityRecordSaveScreen from '../../react/screens/LiveActivityRecordSaveScreen';
import AthleteCreateScreen from '../../react/screens/AthleteCreateScreen';
import AthleteProfileScreen from '../../react/screens/AthleteProfileScreen';
import AthleteEditProfileScreen from '../../react/screens/AthleteEditProfileScreen';
import LiveActivityRecordDetailScreen from '../../react/screens/LiveActivityRecordDetailScreen';

import TeamsScreen from '../../react/screens/TeamsScreen';
import TeamSelectionScreen from '../../react/screens/TeamSelectionScreen';
import TeamAthletesScreen from '../../react/screens/TeamAthletesScreen';
import LoginScreen from '../../react/screens/LoginScreen';
import CameraScreen from '../../react/screens/CameraScreen';

import MoreScreen from '../../react/screens/MoreScreen';
import AdminConfigScreen from '../../react/screens/AdminConfigScreen';
import SystemStatusScreen from '../../react/screens/SystemStatusScreen';
import MeasuringStickScreen from '../../react/screens/MeasuringStickScreen';
import FirmwareScreen from '../../react/screens/FirmwareScreen';
import SavingOverlayScreen from '../../react/screens/SavingOverlayScreen';
import SyncScreen from '../../react/screens/SyncScreen';
import FeatureScreen from './FeatureScreen';
import UserAgentIOS from "rn-ios-user-agent";
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from '../../theme';

function wrapComponent(name, Screen, store, Provider) {
    Navigation.registerComponent(name, () => (props) => (
        <Provider store={store}>
            <Screen {...props} />
        </Provider>
    ), () => Screen)
}

/**
 * ./react/screens/registerScreens
 *
 * register our screen components with react-native-navigation
 */
export function registerScreens(store, Provider) {
    wrapComponent('ActivityLeaderboardScreen', ActivityLeaderboardScreen, store, Provider);
    wrapComponent('ActivityRecordDetailScreen', ActivityRecordDetailScreen, store, Provider);
    wrapComponent('ActivityRecordDetailListScreen', ActivityRecordDetailListScreen, store, Provider);
    wrapComponent('ActivityRecordListScreen', ActivityRecordListScreen, store, Provider);
    wrapComponent('SessionDetailScreen', SessionDetailScreen, store, Provider);
    wrapComponent('SessionListScreen', SessionListScreen, store, Provider);

    wrapComponent('SearchScreen', SearchScreen, store, Provider);
    wrapComponent('BeepStartSoundScreen', BeepStartSoundScreen, store, Provider);

    wrapComponent('LiveModeScreen', LiveModeScreen, store, Provider);
    wrapComponent('LiveModeTriggerScreen', LiveModeTriggerScreen, store, Provider);
    wrapComponent('SessionCreateScreen', SessionCreateScreen, store, Provider);
    wrapComponent('LiveActivitySetupScreen', LiveActivitySetupScreen, store, Provider);
    wrapComponent('LiveAthleteSelectScreen', LiveAthleteSelectScreen, store, Provider);

    wrapComponent('LiveActivityRecordSaveScreen', LiveActivityRecordSaveScreen, store, Provider);
    wrapComponent('AthleteCreateScreen', AthleteCreateScreen, store, Provider);
    wrapComponent('AthleteProfileScreen', AthleteProfileScreen, store, Provider);
    wrapComponent('AthleteEditProfileScreen', AthleteEditProfileScreen, store, Provider);
    wrapComponent('LiveActivityRecordDetailScreen', LiveActivityRecordDetailScreen, store, Provider);

    wrapComponent('TeamsScreen', TeamsScreen, store, Provider);
    wrapComponent('TeamSelectionScreen', TeamSelectionScreen, store, Provider);
    wrapComponent('TeamAthletesScreen', TeamAthletesScreen, store, Provider);
    wrapComponent('LoginScreen', LoginScreen, store, Provider);
    wrapComponent('MoreScreen', MoreScreen, store, Provider);
    wrapComponent('CameraScreen', CameraScreen, store, Provider);

    wrapComponent('AdminConfigScreen', AdminConfigScreen, store, Provider);
    wrapComponent('SystemStatusScreen', SystemStatusScreen, store, Provider);
    wrapComponent('MeasuringStickScreen', MeasuringStickScreen, store, Provider);
    wrapComponent('FeatureScreen', FeatureScreen, store, Provider);
    wrapComponent('FirmwareScreen', FirmwareScreen, store, Provider);
    wrapComponent('SavingOverlayScreen', SavingOverlayScreen, store, Provider);
    wrapComponent('SyncScreen', SyncScreen, store, Provider);
}

export function setUserAgent() {
    localStorage.getItem('xps:loginState').then((data) => {
        if(data && data.user) {
            console.log('setting user agent: '+ "XPS Dash iOS - User: " + data.user.username + '/' + data.user.id + ' - Org: ' + data.user.org.name);
            UserAgentIOS.set("XPS Dash iOS - User:" + data.user.username + '/' + data.user.id + ' - Org: ' + data.user.org.name);
        }
    }).catch((err) => {});
}

export function startAppAnonymous() {
    //localStorage.removeAllDataForKey('xps:orgMembers');
    //localStorage.removeAllDataForKey('xps:sessions');
    //localStorage.removeAllDataForKey('xps:activities');
    localStorage.removeItem('xps:activeUsers');

    Navigation.setRoot({
        root: {
            component: {
                name: 'LoginScreen'
            }
        }
    });
}

export function startAppLoggedIn() {
    // start the app.

    setUserAgent();

    Promise.all([
        Icon.getImageSource('home', 25),
        Icon.getImageSource('circle-o', 25),
        Icon.getImageSource('ellipsis-h', 25)
    ]).then(icons => {
        Navigation.setRoot({
            root: {
                bottomTabs: {
                    id: 'BOTTOM_TABS',
                    children: [
                        {
                            stack: {
                                children: [
                                    {
                                        component: {
                                            name: 'SessionListScreen',
                                            options: {
                                                topBar: {
                                                    visible: true,
                                                    title: {
                                                        text: 'Sessions',
                                                    }
                                                },
                                                bottomTab: {
                                                    icon: icons[0],
                                                    iconColor: 'grey',
                                                    selectedIconColor: 'blue',
                                                    textColor: 'grey',
                                                    selectedTextColor: 'blue',
                                                }
                                            }
                                        }
                                    }
                                ]
                            }
                        },
                        {
                            component: {
                                name: 'LiveModeTriggerScreen',
                                options: {
                                    bottomTab: {
                                        icon: icons[1],
                                        iconColor: 'grey',
                                        selectedIconColor: 'blue',
                                        textColor: 'grey',
                                        selectedTextColor: 'blue',
                                    }
                                }
                            }
                        },
                        {
                            stack: {
                                children: [
                                    {
                                        component: {
                                            name: 'MoreScreen',
                                            options: {
                                                topBar: {
                                                    visible: true,
                                                    title: {
                                                        text: 'Configuration',
                                                    }
                                                },
                                                bottomTab: {
                                                    icon: icons[2],
                                                    iconColor: 'grey',
                                                    selectedIconColor: 'blue',
                                                    textColor: 'grey',
                                                    selectedTextColor: 'blue',
                                                }
                                            }
                                        }
                                    }
                                ]
                            }
                        }
                    ],
                    options: {
                        bottomTabs: {
                            currentTabIndex: 0,
                        },
                        bottomTab: {
                            iconColor: 'grey',
                            selectedIconColor: 'blue',
                            textColor: 'grey',
                            selectedTextColor: 'blue',
                        }
                    }
                },
                topBar: {
                    title: {
                        text: 'Sessions'
                    }
                }
            }
        });
    });
}
