import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import { connect } from 'react-redux';
import validate from 'validate.js'
import {
    Container,
    Content,
    H2,
    Item,
    Text,
    Button,
    Picker,
    Textarea,
    Spinner,
    View,
} from 'native-base';

import styles from '../../theme';
import { saveOrgMember } from '../../redux/actions/org_member';
import { Input } from 'react-native-elements';
import SaveBar from '../../react/components/SaveBar';


export class AthleteCreateScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            nameError: '',
        };
        Navigation.events().bindComponent(this);
    }

    navigationButtonPressed() {
        Navigation.dismissModal(this.props.componentId);
    }

    validateName = (name) => {
        return validate.single(name, {
            presence: {
                allowEmpty: false,
                message: 'A name is required'
            }
        });
    }

    handleSubmit = () => {
        const { name } = this.state;
        const { session, activity, saveOrgMember, toggleAthlete } = this.props;
        const nameError = this.validateName(name);

        this.setState({
            nameError: nameError ? nameError[0] : '',
        });

        if (!nameError) {
            const nameSplit = name.split(' ');
            const first_name = nameSplit.slice(0, 1).join(' ');
            const last_name = nameSplit.slice(1, nameSplit.length).join(' ');

            saveOrgMember({
                user: {
                    first_name: first_name,
                    last_name: last_name,
                },
                org: session.org,
                teams: [session.team_data.id],
            }, this.props.navigator, toggleAthlete);
        }
    }

    render() {
        const { name, nameError } = this.state;

        return (
            <Container>
                <Content style={{width: '100%', padding: 20}}>
                    <Item style={{marginTop:20}}>
                        <Input
                            value={name}
                            onChangeText={(name) => this.setState({ name: name.trim() })}
                            placeholder={'Enter a Name'}
                            placeholderTextColor={'#666'}
                            inputStyle={styles.loginInputStyle}
                            containerStyle={styles.loginContainerStyle}
                            textStyle={styles.loginTextStyle}
                        />
                    </Item>
                    {nameError &&
                    <Text style={styles.inputErrorStyle}>{nameError}</Text>
                    }
                </Content>
                <SaveBar
                    onPressSave={this.handleSubmit}
                />
            </Container>
        )
    }
}


export default connect(
    null,
    { saveOrgMember }
)(AthleteCreateScreen);

