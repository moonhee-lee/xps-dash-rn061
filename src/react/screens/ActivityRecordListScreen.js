import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import { StyleSheet } from 'react-native';
import { FlatList, TouchableOpacity, View, Text } from 'react-native';

import ActivityRecordListItem from '../../react/components/ActivityRecordListItem';
import {Button, Content, Icon, SwipeRow} from "native-base";


/**
 * react/screens/ActivityRecordListScreen
 *
 * list of the activity groups within a session's activity
 *
 */
class ActivityRecordListScreen extends Component {

    _onPressButton = (itemId) => {
        const { clickCallback } = this.props;
        if (clickCallback) clickCallback(itemId);
    }

    _keyExtractor = (item, index) => item.id.toString();

    _renderItem = ( { item } ) => {
        const { navigator } = this.props;

        return (
            <TouchableOpacity onPress={() => this._onPressButton(item.id)}>
                <ActivityRecordListItem
                    id={item.id}
                    activityRecord={item}
                    navigator={navigator}
                />
            </TouchableOpacity>
        )
    };

    constructor(props) {
        super(props);
        Navigation.events().bindComponent(this);
    }

    navigationButtonPressed() {
        Navigation.dismissModal(this.props.componentId);
    }

    render() {
        const { activityRecords } = this.props;

        return (
            <View>
                <FlatList
                    style={{paddingTop: 20}}
                    data={activityRecords}
                    renderItem={this._renderItem}
                    keyExtractor={this._keyExtractor}
                />
            </View>
        )
    }
}

export default ActivityRecordListScreen;
