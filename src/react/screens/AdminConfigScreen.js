import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Input } from 'react-native-elements';
import {Root, Container, Content, Text, Button, Toast} from 'native-base';

import TopMessageBar from '../../react/components/TopMessageBar';
import { logout } from '../../redux/actions/auth';
import { startAppAnonymous } from '../../react/screens';
import { localStorage } from "../../lib/local_storage";
import { fetchCurrentCloudState } from '../../redux/actions/cloud';
import { connectSystem, saveSystemInfo } from '../../redux/actions/system';
import { fetchTagsBySystem } from '../../redux/actions/tag';
import { getCloudState } from '../../redux/reducers';

import styles from '../../theme';
import { SYSTEM_FETCH_SUCCESS } from '../../redux/constants/actionTypes';

export const mapStateToProps = (state, ownProps) => {
    return {
        cloudState: getCloudState(state),
    }
}

/**
 * react/screens/AdminConfigScreen
 *
 * list of the activity groups within a session's activity
 *
 */
class AdminConfigScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cloud_url: global.xpsConfig.siteUrl,
        };
    }

    handleLogout = () => {
        const { logout } = this.props;
        logout();
        startAppAnonymous();
    }

    componentDidMount() {
        this.checkCloud();
    }

    checkCloud() {
        const { fetchCurrentCloudState } = this.props;
        fetchCurrentCloudState();
    }

    componentDidUpdate() {
        const { cloudState } = this.props;
        if(cloudState.isConnected!==this.state.cloudConnected) this.setState({cloudConnected:cloudState.isConnected});

    }

    handleSubmit = () => {
        const { user } = this.props;
        if(this.state.cloud_url!==global.xpsConfig.siteUrl) {
            global.xpsConfig.siteUrl = this.state.cloud_url;

            localStorage.setItem('xps:appConfig', global.xpsConfig);

            if (user) {
                Toast.show({
                    text: 'Saved Cloud Server URL',
                    position: 'top'
                });
            } else {
                this.props.navigator.pop({
                    animated: true,
                    animationType: 'fade',
                });
            }
            this.handleLogout();
        }
        this.checkCloud();
    }

    render() {

        return (
            <Root>
                <Container style={{
                    flex: 1,
                    flexDirection: 'column',
                    justifyContent: 'center',
                    padding: 0,
                  }}>
                    <Content style={{width: '100%',marginTop:80}}>

                            <Text style={{fontSize:18,fontWeight:'bold',paddingLeft:20}}>Cloud Server URL</Text>
                            <Input
                                value={this.state.cloud_url}
                                onChangeText={(cloud_url) => this.setState({ cloud_url })}
                                placeholder={'loud Server base URL'}
                                autoCorrect={false}
                            />

                        <Button bordered block onPress={this.handleSubmit} style={styles.buttonStyle}>
                            <Text>Save</Text>
                        </Button>
                    </Content>

                    {!!this.state.cloudConnected &&
                    <TopMessageBar
                        navigator={navigator}
                        type='success'
                        message={"Connected to XPS Cloud"}
                    />
                    }

                    {!this.state.cloudConnected &&
                    <TopMessageBar
                        navigator={navigator}
                        type='error'
                        message={"Not connected to XPS Cloud"}
                        onPressReload={this.checkCloud.bind(this)}
                    />
                    }


                </Container>
            </Root>
        )
    }
}

export default connect(
    mapStateToProps,
    {
        saveSystemInfo,
        logout,
        fetchCurrentCloudState
    }
)(AdminConfigScreen);
