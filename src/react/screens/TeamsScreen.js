import React, { Component } from 'react';
import { connect } from 'react-redux';

import { View, Text } from 'react-native';
import {Container, Content} from "native-base";
import BottomTabBar from '../../react/components/BottomTabBar';
import TeamListItem from '../../react/components/TeamListItem'

import { fetchTeamsByOrg } from '../../redux/actions/team'
import { getCurrentOrg, getTeamsByCurrentOrg } from '../../redux/reducers';
import styles from '../../theme';

export const mapStateToProps = (state, ownProps) => {
    return {
        teams: getTeamsByCurrentOrg(state),
        currentOrg: getCurrentOrg(state),
    }
}

class TeamsScreen extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const { currentOrg, fetchTeamsByOrg, teams } = this.props;
        if(!teams || teams.length===0) fetchTeamsByOrg(currentOrg.id);
    }

    _onPressTeam(team) {
        this.props.navigator.push({
            screen: 'TeamAthletesScreen',
            title: team.name+' - Athletes',
            backButtonTitle: '',
            passProps: {
                team: team
            }
        });
    }

    render() {
        const { navigator, teams } = this.props;

        return (
            <Container>
                <Content style={styles.contentStyle}>
                    <View>
                        {!!teams && teams.map((team,index) =>
                        <TeamListItem
                            team={team}
                            key={index}
                            index={index}
                            onPressTeam={() => this._onPressTeam(team)}
                        >
                        </TeamListItem>
                        )}
                    </View>
                </Content>
                <BottomTabBar
                    navigator={navigator}
                />
            </Container>
        )
    }
}

export default connect(
    mapStateToProps,
    { fetchTeamsByOrg }
)(TeamsScreen);
