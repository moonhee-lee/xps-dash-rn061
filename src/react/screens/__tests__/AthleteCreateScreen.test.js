import React from 'react';
import { shallow } from 'enzyme';

import { AthleteCreateScreen } from '../../react/screens/AthleteCreateScreen';
import { FormInput } from 'react-native-elements';

import SaveBar from '../../react/components/SaveBar';

jest.mock("react-native-background-timer", () => {});
jest.mock("rn-ios-user-agent", () => {});

describe('AthleteCreateScreen Component', () => {

    it('renders ActivitySetupScreen when activity_type is empty', () => {
        const props = {
            activity: {id:1,distance:10,measurement_type:'meters',start_type:'',orientation:'',activity_type:''},
            navigator: {
                setOnNavigatorEvent: jest.fn()
            }
        };
        const enzymeWrapper = shallow(<AthleteCreateScreen {...props}/>);
        expect(enzymeWrapper.find(FormInput).length).toBe(1);
        expect(enzymeWrapper.find(SaveBar).length).toBe(1);
    });

});
