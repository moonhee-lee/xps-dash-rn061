import React from 'react';
import { shallow } from 'enzyme';
import {View} from 'react-native';

import Swiper from 'react-native-swiper';
import { Container, Content, Spinner, Text } from 'native-base';

import * as fromReducers from '../../redux/reducers';
import { mapStateToProps, ActivityRecordDetailScreen } from '../../react/screens/ActivityRecordDetailScreen';
import { getActivityRecords, getActivityRecordsIsFetching } from '../../redux/reducers';
import { activityRecordSelector } from '../../redux/selectors/activity_records';
import { fetchActivityRecords } from '../../redux/actions/activity_record';

import ActivityRecordGraphs from '../../react/components/ActivityRecordGraphs';
import ActivityRecordDetailLists from '../../react/components/ActivityRecordDetailLists';
import ActivityRecordUserListItem from '../../react/components/ActivityRecordUserListItem';

jest.mock('../../redux/reducers');
jest.mock('../../redux/selectors/activity_records');
jest.mock('../../redux/actions/activity_record');
jest.mock("react-native-background-timer", () => {});
jest.mock("rn-ios-user-agent", () => {});

jest.useFakeTimers();

describe('mapStateToProps method', () => {

    it('returns currentOrg, activitySummaries', () => {
        const activityRecord = {id:1, user: {id:1}};
        const activityRecordData = [activityRecord];
        const expectedResult = {
            activityRecordData,
            isFetching: false
        };

        fromReducers.getActivityDataIsFetching.mockImplementationOnce(() => false);
        fromReducers.getActivityDataById.mockImplementationOnce(() => activityRecordData);
        expect(mapStateToProps({activityRecord},{activityRecord})).toEqual(expectedResult);
    });

});

describe('ActivityRecordDetailScreen Component', () => {

    it('calls getActivityDataById activityRecord', () => {
        const props = {
            activitySummary:{activity:{id:1},user:{id:1}},
            activity:{activity_type:false,display_name:'Record'},
            activityRecordData:[{id:1,data:false,activity:{activity_type:false,display_name:'Record'}}],
            activityRecord:{id:1,data:false,activity:{activity_type:false,display_name:'Record'},type_definition:{activity_type:'sprint'},data_summary:{max_velocity:5}},
            fetchActivityDataById:jest.fn(),
            getActivityDataById:jest.fn(),
            navigator: {
                setOnNavigatorEvent: jest.fn(),
                setTitle: jest.fn()
            }
        };
        const enzymeWrapper = shallow(<ActivityRecordDetailScreen {...props}/>);
        expect(props.getActivityDataById.mock.calls.length).toBe(0);
        expect(enzymeWrapper.find(Content).length).toBe(1);
        expect(enzymeWrapper.find(View).length).toBe(4);
        expect(enzymeWrapper.find(Swiper).length).toBe(0);
        expect(enzymeWrapper.find(ActivityRecordUserListItem).length).toBe(1);
        expect(enzymeWrapper.find(ActivityRecordGraphs).length).toBe(0);
        expect(enzymeWrapper.find(ActivityRecordDetailLists).length).toBe(1);
    });

    it('does not call fetchActivityDataById activitySummary is empty', () => {
        const props = {
            activitySummary:{},
            activity:{activity_type:false,display_name:'Record'},
            activityRecordData:[{id:1,data:false,activity:{activity_type:false,display_name:'Record'}}],
            activityRecord:{id:1,data:false,activity:{activity_type:false,display_name:'Record'},type_definition:{activity_type:'sprint'},data_summary:{max_velocity:5}},
            fetchActivityDataById:jest.fn(),
            getActivityDataById:jest.fn(),
            navigator: {
                setOnNavigatorEvent: jest.fn(),
                setTitle: jest.fn()
            }
        };
        const enzymeWrapper = shallow(<ActivityRecordDetailScreen {...props}/>);
        expect(props.getActivityDataById.mock.calls.length).toBe(0);
        expect(enzymeWrapper.find(Content).length).toBe(1);
        expect(enzymeWrapper.find(View).length).toBe(4);
        expect(enzymeWrapper.find(ActivityRecordUserListItem).length).toBe(1);
        expect(enzymeWrapper.find(ActivityRecordGraphs).length).toBe(0);
        expect(enzymeWrapper.find(ActivityRecordDetailLists).length).toBe(1);
    });

    it('renders View when activityRecords.length == 1', () => {
        const props = {
            activity:{activity_type:false,display_name:'Record'},
            activityRecordData:[{id:1,data:false,activity:{activity_type:false,display_name:'Record'}}],
            activityRecord:{id:1,data:false,activity:{activity_type:false,display_name:'Record'},type_definition:{activity_type:'sprint'},data_summary:{max_velocity:5}},
            activitySummary:{activity:{id:1},user:{id:1}},
            fetchActivityDataById:jest.fn(),
            getActivityDataById:jest.fn(),
            navigator: {
                setOnNavigatorEvent: jest.fn(),
                setTitle: jest.fn()
            }
        };
        const enzymeWrapper = shallow(<ActivityRecordDetailScreen {...props}/>);
        expect(props.getActivityDataById.mock.calls.length).toBe(0);
        expect(enzymeWrapper.find(Content).length).toBe(1);
        expect(enzymeWrapper.find(View).length).toBe(4);
        expect(enzymeWrapper.find(ActivityRecordUserListItem).length).toBe(1);
        expect(enzymeWrapper.find(ActivityRecordGraphs).length).toBe(0);
        expect(enzymeWrapper.find(ActivityRecordDetailLists).length).toBe(1);
    });

    it('renders View when activityRecords.length > 1', () => {
        const props = {
            activityRecordData: [{id:1,data:false,activity:{activity_type:false,display_name:'Record'}},{id:2,data:false,activity:{activity_type:false,display_name:'Record'}}],
            activity:{activity_type:false,display_name:'Record'},
            activityRecord:{id:1,data:false,activity:{activity_type:false,display_name:'Record'},type_definition:{activity_type:'sprint'},data_summary:{max_velocity:5}},
            activitySummary:{activity:{id:1},user:{id:1}},
            fetchActivityDataById:jest.fn(),
            getActivityDataById:jest.fn(),
            navigator: {
                setOnNavigatorEvent: jest.fn(),
                setTitle: jest.fn()
            }
        };
        const enzymeWrapper = shallow(<ActivityRecordDetailScreen {...props}/>);
        expect(enzymeWrapper.find(Content).length).toBe(1);
        expect(enzymeWrapper.find(View).length).toBe(4);
        expect(enzymeWrapper.find(ActivityRecordUserListItem).length).toBe(1);
        expect(enzymeWrapper.find(ActivityRecordGraphs).length).toBe(0);
        expect(enzymeWrapper.find(ActivityRecordDetailLists).length).toBe(1);
    });

    it('does not render View when activityRecordData is empty', () => {
        const props = {
            activityRecordData: [],
            activity:{activity_type:false,display_name:'Record'},
            activityRecord:{id:1,data:false,activity:{activity_type:false,display_name:'Record'},type_definition:{activity_type:'sprint'},data_summary:{max_velocity:5}},
            activitySummary:{activity:{id:1},user:{id:1}},
            fetchActivityDataById:jest.fn(),
            getActivityDataById:jest.fn(),
            fetchActivityDataByRecord:jest.fn(),
            navigator: {
                setOnNavigatorEvent: jest.fn(),
                setTitle: jest.fn()
            }
        };
        const enzymeWrapper = shallow(<ActivityRecordDetailScreen {...props}/>);
        expect(props.fetchActivityDataById.mock.calls.length).toBe(0);
        expect(enzymeWrapper.find(Content).length).toBe(1);
        expect(enzymeWrapper.find(View).length).toBe(4);
        expect(enzymeWrapper.find(Spinner).length).toBe(0);
        expect(enzymeWrapper.find(ActivityRecordUserListItem).length).toBe(1);
        expect(enzymeWrapper.find(ActivityRecordGraphs).length).toBe(0);
        expect(enzymeWrapper.find(ActivityRecordDetailLists).length).toBe(1);
    });

});
