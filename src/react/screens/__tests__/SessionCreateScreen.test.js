import React from 'react';
import { shallow } from 'enzyme';

import { FlatList, View } from 'react-native';
import { Spinner, Picker, Textarea } from 'native-base';
import { FormInput } from 'react-native-elements';

import * as fromReducers from '../../redux/reducers';
import { mapStateToProps, SessionCreateScreen } from '../../react/screens/SessionCreateScreen';

import { getCurrentOrg, getTeamsByOrg, getTeamIsFetching, getTeamsByCurrentOrg } from '../../redux/reducers';
import { fetchTeamsByOrg } from '../../redux/actions/team';
import { saveSession } from '../../redux/actions/session';

jest.mock('../../redux/reducers');
jest.mock('../../redux/actions/team');
jest.mock('../../redux/actions/session');
jest.mock("react-native-background-timer", () => {});
jest.mock("rn-ios-user-agent", () => {});

describe('mapStateToProps method', () => {

    it('returns currentOrg, teams and isFetching', () => {
        const currentOrg = {id: 1, name: 'Test Org'};
        const teams = [
            {id: 1, name: 'Test Team 1'},
            {id: 2, name: 'Test Team 2'},
        ]
        const expectedResult = {
            currentOrg,
            teams,
            isFetching: false
        };

        fromReducers.getCurrentOrg.mockImplementationOnce(() => currentOrg);
        fromReducers.getTeamsByCurrentOrg.mockImplementationOnce(() => teams);
        fromReducers.getTeamIsFetching.mockImplementationOnce(() => false);
        expect(mapStateToProps({})).toEqual(expectedResult);
    });

});


describe('SessionCreateScreen Component', () => {

    it('renders correctly while fetching', () => {
        const props = {
            teams: [{id: 1}],
            isFetching: true,
            navigator: {
                toggleTabs: jest.fn(),
                setOnNavigatorEvent: jest.fn()
            }
        };
        const enzymeWrapper = shallow(<SessionCreateScreen {...props}/>);
        expect(enzymeWrapper.find(Spinner).length).toBe(1);
    });

    it('calls fetchSessionsByOrg when sessions empty', () => {
        const props = {
            teams: [],
            currentOrg: {id: 1},
            isFetching: true,
            fetchTeamsByOrg: jest.fn(),
            navigator: {
                toggleTabs: jest.fn(),
                setOnNavigatorEvent: jest.fn()
            }
        };
        const enzymeWrapper = shallow(<SessionCreateScreen {...props}/>);
        expect(props.fetchTeamsByOrg.mock.calls.length).toBe(1);
    });

    it('renders View when not fetching.', () => {
        const props = {
            teams: [{id: 1}],
            currentOrg: {id: 1},
            isFetching: false,
            fetchTeamsByOrg: jest.fn(),
            navigator: {
                toggleTabs: jest.fn(),
                setOnNavigatorEvent: jest.fn()
            }
        };
        const enzymeWrapper = shallow(<SessionCreateScreen {...props}/>);
        expect(enzymeWrapper.find(FormInput).length).toBe(1);
        expect(enzymeWrapper.find(Textarea).length).toBe(1);
        expect(enzymeWrapper.find(Picker).length).toBe(1);
    });

    it('does not render team picker when does not have teams.', () => {
        const props = {
            teams: [],
            currentOrg: {id: 1},
            isFetching: false,
            fetchTeamsByOrg: jest.fn(),
            navigator: {
                toggleTabs: jest.fn(),
                setOnNavigatorEvent: jest.fn()
            }
        };
        const enzymeWrapper = shallow(<SessionCreateScreen {...props}/>);
        expect(enzymeWrapper.find(FormInput).length).toBe(1);
        expect(enzymeWrapper.find(Textarea).length).toBe(1);
        expect(enzymeWrapper.find(Picker).length).toBe(0);
    });

});
