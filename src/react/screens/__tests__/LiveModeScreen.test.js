import React from 'react';
import { shallow } from 'enzyme';

import { Spinner, SwipeRow } from 'native-base';

import * as fromReducers from '../../redux/reducers';
import { mapStateToProps, LiveModeScreen } from '../../react/screens/LiveModeScreen';

import { sessionsSelector } from '../../redux/selectors/sessions';
import { activityActiveUsersSelector } from '../../redux/selectors/active_users';
import { getCurrentOrg, getCurrentHub, getSessionsIsFetching } from '../../redux/reducers';
import { fetchSessionsByOrg } from '../../redux/actions/session';
import LiveRecordBar from '../../react/components/LiveRecordBar';
import LiveOptionBar from '../../react/components/LiveOptionBar';

jest.mock('../../redux/reducers');
jest.mock('../../redux/selectors/sessions');
jest.mock('../../redux/selectors/active_users');
jest.mock("react-native-background-timer", () => {});
jest.mock("rn-ios-user-agent", () => {});

describe('mapStateToProps method', () => {

    it('returns currentOrg, sessions and isFetching', () => {
        const currentOrg = {id: 1, name: 'Test Org'};
        const currentActivity = {id: 1, name: 'Test Activity'};
        const currentSystem = {id: 1, name: 'Test System'};
        const tags = {id: 1, name: 'Test Tags'};
        const activeUsers = [{id: 1, name: 'Test Users'}];
        const orgMembers = {id: 1, name: 'Test Members'};

        const expectedResult = {
            currentOrg,
            currentActivity,
            liveActivity: undefined,
            currentSystem,
            tags,
            activeUsers,
            orgMembers,
            isFetching: false
        };

        fromReducers.getCurrentOrg.mockImplementationOnce(() => currentOrg);
        fromReducers.getActivityById.mockImplementationOnce(() => currentActivity);
        fromReducers.getCurrentSystem.mockImplementationOnce(() => currentSystem);
        fromReducers.getCurrentTags.mockImplementationOnce(() => tags);
        activityActiveUsersSelector.mockImplementationOnce(() => activeUsers);
        fromReducers.getMembersByOrg.mockImplementationOnce(() => orgMembers);
        fromReducers.getActiveUsersIsFetching.mockImplementationOnce(() => false);
        expect(mapStateToProps({},expectedResult)).toEqual(expectedResult);
    });

});


describe('LiveModeScreen Component', () => {

    it('renders correctly while fetching', () => {
        const props = {
            liveActivity: {id:1,name:'live_activity',team:1},
            sessions: [{id: 1}],
            activityId: 1,
            currentOrg: {id: 1},
            currentSystem: {state:'connected',name:'system 1'},
            isFetching: true,
            fetchCurrentWifiState: jest.fn(),
            fetchCurrentCloudState: jest.fn(),
            fetchActiveUsers: jest.fn(),
            fetchActivity: jest.fn(),
            fetchLiveActivity: jest.fn(),
            fetchMembersByOrg: jest.fn(),
            fetchTeamsByOrg: jest.fn(),
            setLiveActivityTeam: jest.fn(),
            connectSystem: jest.fn(),
            fetchTagsByOrg: jest.fn(),
            navigator: {
                toggleTabs: jest.fn(),
                setOnNavigatorEvent: jest.fn()
            },
            wifiState: {isConnected:true}
        };
        const enzymeWrapper = shallow(<LiveModeScreen {...props}/>);
        expect(enzymeWrapper.find(Spinner).length).toBe(1);
    });

    it('calls fetchActiveUsers when activeUsers empty', () => {
        const props = {
            liveActivity: {id:1,name:'live_activity',team:1},
            sessions: [],
            activityId: 1,
            activeUsers:null,
            currentOrg: {id: 1},
            currentSystem: {state:'connected',name:'system 1'},
            isFetching: true,
            fetchCurrentWifiState: jest.fn(),
            fetchCurrentCloudState: jest.fn(),
            fetchActiveUsers: jest.fn(),
            fetchActivity: jest.fn(),
            fetchLiveActivity: jest.fn(),
            fetchMembersByOrg: jest.fn(),
            fetchTeamsByOrg: jest.fn(),
            setLiveActivityTeam: jest.fn(),
            connectSystem: jest.fn(),
            fetchTagsByOrg: jest.fn(),
            navigator: {
                toggleTabs: jest.fn(),
                setOnNavigatorEvent: jest.fn()
            },
            wifiState: {isConnected:true}
        };
        const enzymeWrapper = shallow(<LiveModeScreen {...props}/>);
        expect(props.fetchActiveUsers.mock.calls.length).toBe(1);
    });

    it('calls fetchActivity when currentActivity empty', () => {
        const props = {
            liveActivity: {id:1,name:'live_activity',team:1},
            sessions: [],
            activityId: 1,
            currentOrg: {id: 1},
            currentSystem: {state:'connected',name:'system 1'},
            isFetching: true,
            activeUsers: [{id:1},{id:2}],
            currentActivity: null,
            fetchCurrentWifiState: jest.fn(),
            fetchCurrentCloudState: jest.fn(),
            fetchActiveUsers: jest.fn(),
            fetchActivity: jest.fn(),
            fetchLiveActivity: jest.fn(),
            fetchMembersByOrg: jest.fn(),
            fetchTeamsByOrg: jest.fn(),
            setLiveActivityTeam: jest.fn(),
            connectSystem: jest.fn(),
            fetchTagsByOrg: jest.fn(),
            navigator: {
                toggleTabs: jest.fn(),
                setOnNavigatorEvent: jest.fn()
            },
            wifiState: {isConnected:true}
        };
        const enzymeWrapper = shallow(<LiveModeScreen {...props}/>);
        expect(props.fetchActivity.mock.calls.length).toBe(1);
    });

    it('calls fetchLiveActivity when liveActivity empty', () => {
        const props = {
            sessions: [],
            activityId: 1,
            currentOrg: {id: 1},
            currentSystem: {state:'connected',name:'system 1'},
            isFetching: true,
            activeUsers: [{id:1},{id:2}],
            liveActivity: null,
            fetchCurrentWifiState: jest.fn(),
            fetchCurrentCloudState: jest.fn(),
            fetchActiveUsers: jest.fn(),
            fetchActivity: jest.fn(),
            fetchLiveActivity: jest.fn(),
            fetchMembersByOrg: jest.fn(),
            fetchTeamsByOrg: jest.fn(),
            setLiveActivityTeam: jest.fn(),
            connectSystem: jest.fn(),
            fetchTagsByOrg: jest.fn(),
            navigator: {
                toggleTabs: jest.fn(),
                setOnNavigatorEvent: jest.fn()
            },
            wifiState: {isConnected:true}
        };
        const enzymeWrapper = shallow(<LiveModeScreen {...props}/>);
        expect(props.fetchLiveActivity.mock.calls.length).toBe(1);
    });

    it('calls fetchMembersByOrg when orgMembers empty', () => {
        const props = {
            liveActivity: {id:1,name:'live_activity',team:1},
            sessions: [],
            activityId: 1,
            currentOrg: {id: 1},
            currentSystem: {state:'connected',name:'system 1'},
            isFetching: true,
            orgMembers: null,
            activeUsers: [{id:1},{id:2}],
            fetchCurrentWifiState: jest.fn(),
            fetchCurrentCloudState: jest.fn(),
            fetchActiveUsers: jest.fn(),
            fetchActivity: jest.fn(),
            fetchLiveActivity: jest.fn(),
            fetchMembersByOrg: jest.fn(),
            fetchTeamsByOrg: jest.fn(),
            setLiveActivityTeam: jest.fn(),
            connectSystem: jest.fn(),
            fetchTagsByOrg: jest.fn(),
            navigator: {
                toggleTabs: jest.fn(),
                setOnNavigatorEvent: jest.fn()
            },
            wifiState: {isConnected:true}
        };
        const enzymeWrapper = shallow(<LiveModeScreen {...props}/>);
        expect(props.fetchMembersByOrg.mock.calls.length).toBe(1);
    });

    it('renders FlatList when not fetching.', () => {
        const props = {
            liveActivity: {id:1,name:'live_activity',team:1},
            sessions: [{id: 1}],
            activityId: 1,
            currentOrg: {id: 1},
            isFetching: false,
            activeUsers: [{id:1},{id:2}],
            currentSystem: {state:'connected',name:'system 1'},
            fetchCurrentWifiState: jest.fn(),
            fetchCurrentCloudState: jest.fn(),
            fetchActiveUsers: jest.fn(),
            fetchActivity: jest.fn(),
            fetchLiveActivity: jest.fn(),
            fetchMembersByOrg: jest.fn(),
            fetchTagsByOrg: jest.fn(),
            fetchTeamsByOrg: jest.fn(),
            setLiveActivityTeam: jest.fn(),
            connectSystem: jest.fn(),
            navigator: {
                toggleTabs: jest.fn(),
                setOnNavigatorEvent: jest.fn()
            },
            wifiState: {isConnected:true},
            tags: [{name:1}]
        };
        const enzymeWrapper = shallow(<LiveModeScreen {...props}/>);
        expect(enzymeWrapper.find(LiveOptionBar).length).toBe(1);
        expect(enzymeWrapper.find(LiveRecordBar).length).toBe(1);
        expect(enzymeWrapper.find(SwipeRow).length).toBe(2);
    });

    it('does not render FlatList when not fetching and no activeUsers.', () => {
        const props = {
            liveActivity: {id:1,name:'live_activity',team:1},
            sessions: [{id: 1}],
            activityId: 1,
            currentOrg: {id: 1},
            isFetching: false,
            activeUsers: [],
            currentSystem: {state:'connected',name:'system 1'},
            fetchCurrentWifiState: jest.fn(),
            fetchCurrentCloudState: jest.fn(),
            fetchActiveUsers: jest.fn(),
            fetchActivity: jest.fn(),
            fetchLiveActivity: jest.fn(),
            fetchMembersByOrg: jest.fn(),
            fetchTagsByOrg: jest.fn(),
            fetchTeamsByOrg: jest.fn(),
            setLiveActivityTeam: jest.fn(),
            connectSystem: jest.fn(),
            navigator: {
                toggleTabs: jest.fn(),
                setOnNavigatorEvent: jest.fn()
            },
            wifiState: {isConnected:true}
        };
        const enzymeWrapper = shallow(<LiveModeScreen {...props}/>);
        expect(enzymeWrapper.find(LiveOptionBar).length).toBe(1);
        expect(enzymeWrapper.find(LiveRecordBar).length).toBe(1);
        expect(enzymeWrapper.find(SwipeRow).length).toBe(0);
    });

});
