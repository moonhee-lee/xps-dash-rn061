import React from 'react';
import { shallow } from 'enzyme';

import { FlatList } from 'react-native';
import { Spinner } from 'native-base';

import * as fromReducers from '../../redux/reducers';
import { sessionsSelector } from '../../redux/selectors/sessions';
import { mapStateToProps, SessionListScreen } from '../../react/screens/SessionListScreen';

jest.mock('../../redux/reducers');
jest.mock('../../redux/selectors/sessions');
jest.mock("react-native-background-timer", () => {});
jest.mock("rn-ios-user-agent", () => {});

describe('mapStateToProps method', () => {

    it('returns currentOrg, sessions and isFetching', () => {
        const org = {id: 1, name: 'Test Org'};
        const sessions = [
            {id: 1, name: 'Test Session 1'},
            {id: 2, name: 'Test Session 2'},
        ]
        const expectedResult = {
            currentOrg: org,
            sessions,
            isFetching: false
        };

        fromReducers.getCurrentOrg.mockImplementationOnce(() => org);
        fromReducers.getSessionsIsFetching.mockImplementationOnce(() => false);
        sessionsSelector.mockImplementationOnce(() => sessions);
        expect(mapStateToProps({})).toEqual(expectedResult);
    });

});


describe('SessionListScreen Component', () => {

    it('renders correctly while fetching', () => {
        const props = {
            currentOrg: {id: 1},
            sessions: [{id: 1}],
            isFetching: true,
            fetchSessionsByOrg: jest.fn(),
            fetchSessionsByTeam: jest.fn(),
            fetchTeamsByOrg: jest.fn(),
            fetchTagsByOrg: jest.fn(),
            fetchActivities: jest.fn(),
            navigator: {
                toggleTabs: jest.fn(),
                setOnNavigatorEvent: jest.fn()
            }
        };
        const enzymeWrapper = shallow(<SessionListScreen {...props}/>);
        expect(enzymeWrapper.find(Spinner).length).toBe(1);
    });

    it('calls fetchSessionsByOrg when sessions empty', () => {
        const props = {
            sessions: [],
            currentOrg: {id: 1},
            isFetching: true,
            fetchSessionsByOrg: jest.fn(),
            fetchSessionsByTeam: jest.fn(),
            fetchTeamsByOrg: jest.fn(),
            fetchTagsByOrg: jest.fn(),
            fetchActivities: jest.fn(),
            navigator: {
                toggleTabs: jest.fn(),
                setOnNavigatorEvent: jest.fn()
            }
        };
        const enzymeWrapper = shallow(<SessionListScreen {...props}/>);
        expect(props.fetchSessionsByOrg.mock.calls.length).toBe(1);
    });

    it('renders FlatList when not fetching.', () => {
        const props = {
            sessions: [{id: 1}],
            currentOrg: {id: 1},
            isFetching: false,
            fetchSessionsByOrg: jest.fn(),
            fetchSessionsByTeam: jest.fn(),
            fetchTeamsByOrg: jest.fn(),
            fetchTagsByOrg: jest.fn(),
            fetchActivities: jest.fn(),
            navigator: {
                toggleTabs: jest.fn(),
                setOnNavigatorEvent: jest.fn()
            }
        };
        const enzymeWrapper = shallow(<SessionListScreen {...props}/>);
        expect(enzymeWrapper.find(FlatList).length).toBe(1);
    });

    it('does not render FlatList when not fetching and no sessions.', () => {
        const props = {
            sessions: [],
            currentOrg: {id: 1},
            isFetching: false,
            fetchSessionsByOrg: jest.fn(),
            fetchSessionsByTeam: jest.fn(),
            fetchTeamsByOrg: jest.fn(),
            fetchTagsByOrg: jest.fn(),
            fetchActivities: jest.fn(),
            navigator: {
                toggleTabs: jest.fn(),
                setOnNavigatorEvent: jest.fn()
            }
        };
        const enzymeWrapper = shallow(<SessionListScreen {...props}/>);
        expect(enzymeWrapper.find(FlatList).length).toBe(0);
    });

});
