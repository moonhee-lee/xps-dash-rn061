import React from 'react';
import { shallow } from 'enzyme';

import { FlatList } from 'react-native';

import { mapStateToProps, ActivityLeaderboardScreen } from '../../react/screens/ActivityLeaderboardScreen';
import { fetchActivitySummariesByActivity } from '../../redux/actions/activity_summary';
import { activitiesSummariesSelector } from '../../redux/selectors/activity_summaries';

jest.mock('../../redux/reducers');
jest.mock('../../redux/selectors/activity_summaries');
jest.mock('../../redux/actions/activity_summary');
jest.mock("react-native-background-timer", () => {});
jest.mock("rn-ios-user-agent", () => {});

describe('mapStateToProps method', () => {

    it('returns currentOrg, activitySummaries', () => {
        const activitiesSummaries = [
            {id: 1, name: 'Test Summary'},
        ]
        const expectedResult = {activeSession: undefined, activityRecords: [], activitySummaries: [{id: 1, name: "Test Summary"}], athletes: [], currentTeam: undefined, isFetching: undefined}

        activitiesSummariesSelector.mockImplementationOnce(() => activitiesSummaries);
        activitiesSummariesSelector.mockImplementationOnce(() => activitiesSummaries);
        expect(mapStateToProps({},{activity:{hash_type:1},session:{id:1}})).toEqual(expectedResult);
    });
});

describe('ActivityLeaderboardScreen Component', () => {

    it('calls fetchActivitySummariesByActivity when activitySummaries empty', () => {
        const props = {
            session: {id:1},
            activitySummaries: [],
            activity: {id:1,activity_type:'sprint'},
            fetchActivityRecords: jest.fn(),
            navigator: {
                setOnNavigatorEvent: jest.fn()
            }
        };

        const enzymeWrapper = shallow(<ActivityLeaderboardScreen {...props}/>);
        expect(props.fetchActivityRecords.mock.calls.length).toBe(1);
    });

    it('renders FlatList.', () => {
        const props = {
            session: {id: 1},
            activity: {id:1,activity_type:'sprint'},
            activitySummaries: [{id:1,org:1,activity:13,user:{id:2,username:'user1',email:'',full_name:'Michael Kedda',first_name:'Michael',last_name:'Kedda',bio:'',is_staff:false},num_records:12,top_record:{id:1,org:1,activity:13,user:2,tag:3,meta:{end:2553.277,dip1:-0.17978291666666657,dip2:-0.7176719166666665,start:2551.102,height:0.5354580833333333,length:2.1386480000000003,air_time:0.626783424575357},score:null}}],
            fetchActivityRecords: jest.fn(),
            navigator: {
                setOnNavigatorEvent: jest.fn()
            }
        };

        const enzymeWrapper = shallow(<ActivityLeaderboardScreen {...props}/>);
        expect(props.fetchActivityRecords.mock.calls.length).toBe(1);
        expect(enzymeWrapper.find(FlatList).length).toBe(1);

    });

    it('does not render FlatList when no activitySummaries.', () => {
        const props = {
            session: {id: 1},
            activitySummaries: [],
            activity: {id:1,activity_type:'sprint'},
            fetchActivityRecords: jest.fn(),
            navigator: {
                setOnNavigatorEvent: jest.fn()
            }
        };

        const enzymeWrapper = shallow(<ActivityLeaderboardScreen {...props}/>);
        expect(props.fetchActivityRecords.mock.calls.length).toBe(1);
        expect(enzymeWrapper.find(FlatList).length).toBe(0);
    });

});
