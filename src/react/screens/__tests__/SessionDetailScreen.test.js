import React from 'react';
import { shallow } from 'enzyme';

import { Spinner, SwipeRow } from 'native-base';

import * as fromReducers from '../../redux/reducers';

import { mapStateToProps, SessionDetailScreen } from '../../react/screens/SessionDetailScreen';
import { fetchActivitiesBySession } from '../../redux/actions/activity';
import { getActivityIsFetching, getActiveSession } from '../../redux/reducers';
import { activitiesWithLeadersSelector } from '../../redux/selectors/activity_summaries';
import { setActiveSession, setActiveActivity } from '../../redux/actions/active_session';

import TopCreateBar from '../../react/components/TopCreateBar';

jest.mock('../../redux/reducers');
jest.mock('../../redux/selectors/activity_summaries');
jest.mock('../../redux/actions/activity');
jest.mock('../../redux/actions/active_session');
jest.mock("react-native-background-timer", () => {});
jest.mock("rn-ios-user-agent", () => {});

describe('mapStateToProps method', () => {

    it('returns currentOrg, sessions and isFetching', () => {
        const activeSession = {id: 1, name: 'Test Org'};
        const activities = [
            {id: 1, name: 'Test Activity 1', type_definition: {activity_type:'sprint',start_type:'motion',distance:10}},
            {id: 2, name: 'Test Activity 2', type_definition: {activity_type:'sprint',start_type:'motion',distance:10}},
        ];
        const expectedResult = {
            activities,
            isFetching: false
        };

        fromReducers.getActivitiesBySession.mockImplementationOnce(() => activities);
        fromReducers.getActivityIsFetching.mockImplementationOnce(() => false);
        activitiesWithLeadersSelector.mockImplementationOnce(() => activities);
        expect(mapStateToProps({},{session:{id:1}})).toEqual(expectedResult);
    });

});

describe('SessionDetailScreen Component', () => {

    it('renders correctly while fetching', () => {
        const props = {
            activities: [
                {id: 1, name: 'Test Activity 1', type_definition: {activity_type:'sprint',start_type:'motion',distance:10}},
                {id: 2, name: 'Test Activity 2', type_definition: {activity_type:'sprint',start_type:'motion',distance:10}},
            ],
            session: [{id: 1}],
            isFetching: true,
            fetchActivitiesBySession: jest.fn(),
            navigator: {
                toggleTabs: jest.fn(),
                setOnNavigatorEvent: jest.fn()
            }
        };
        const enzymeWrapper = shallow(<SessionDetailScreen {...props}/>);
        expect(enzymeWrapper.find(Spinner).length).toBe(1);
    });

    it('calls fetchSessionsByOrg when activities empty', () => {
        const props = {
            activities: [],
            session: [{id: 1}],
            activeSession:{id: 1},
            isFetching: true,
            fetchActivitiesBySession: jest.fn(),
            navigator: {
                toggleTabs: jest.fn(),
                setOnNavigatorEvent: jest.fn()
            }
        };
        const enzymeWrapper = shallow(<SessionDetailScreen {...props}/>);
        expect(props.fetchActivitiesBySession.mock.calls.length).toBe(1);
        expect(enzymeWrapper.find(SwipeRow).length).toBe(0);
    });

    it('renders SwipeRow when activities is not empty', () => {
        const props = {
            activities: [
                {id: 1, name: 'Test Activity 1', type_definition: {activity_type:'sprint',start_type:'motion',distance:10}},
                {id: 2, name: 'Test Activity 2', type_definition: {activity_type:'sprint',start_type:'motion',distance:10}},
            ],
            session: [{id: 1}],
            isFetching: false,
            fetchActivitiesBySession: jest.fn(),
            navigator: {
                toggleTabs: jest.fn(),
                setOnNavigatorEvent: jest.fn()
            }
        };
        const enzymeWrapper = shallow(<SessionDetailScreen {...props}/>);
        expect(enzymeWrapper.find(SwipeRow).length).toBe(1);
    });



});
