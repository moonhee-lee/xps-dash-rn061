import React, { Component } from 'react';
import { connect } from 'react-redux';

import {View, Text, ScrollView, RefreshControl} from 'react-native';
import {Container, Content} from "native-base";
import BottomTabBar from '../../react/components/BottomTabBar';
import AthleteListItem from '../../react/components/AthleteListItem'

import { fetchUsersByTeam } from '../../redux/actions/user';
import { teamAthletesSelector } from '../../redux/selectors/team_athletes';
import { getCurrentOrg } from '../../redux/reducers';
import styles from '../../theme';

export const mapStateToProps = (state, ownProps) => {
    return {
        athletes: teamAthletesSelector(state, ownProps),
        currentOrg: getCurrentOrg(state),
    }
}

class TeamAthletesScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
        };
    }

    componentDidMount() {
        const { athletes, fetchUsersByTeam, team } = this.props;
        if(!athletes || athletes.length===0) fetchUsersByTeam(team.id);
    }

    componentDidUpdate() {
        const { athletes, fetchUsersByTeam, team } = this.props;
    }

    showAthleteProfile(athlete) {
        this.props.navigator.push({
            title: 'Profile for '+athlete.full_name,
            screen: 'AthleteProfileScreen',
            passProps: {
                athlete:athlete,
            },
        });
    }

    editAthleteProfile(athlete) {
        const { team, currentOrg } = this.props;
        this.props.navigator.push({
            title: 'Profile for '+athlete.full_name,
            screen: 'AthleteEditProfileScreen',
            passProps: {
                athlete:athlete,
                team:team,
                currentOrg
            },
        });
    }

    _onRefresh = () => {
        this.setState({refreshing: true});
        setTimeout(() => {
            const { team, fetchUsersByTeam } = this.props;
            fetchUsersByTeam(team.id);
            this.setState({refreshing: false});
        },1000);
    }

    render() {
        const { navigator, athletes } = this.props;

        return (
            <Container>
                <ScrollView
                    style={styles.contentStyle}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                        />
                    }>
                    <View>
                        {!!athletes && athletes.map((athlete,index) =>
                            <AthleteListItem
                                athlete={athlete}
                                key={index}
                                index={index}
                                navigator={navigator}
                                onPressAthlete={() => this.showAthleteProfile(athlete)}
                                onLongPressAthlete={() => this.editAthleteProfile(athlete)}
                            >
                            </AthleteListItem>
                        )}
                    </View>
                </ScrollView>
                <BottomTabBar
                    navigator={navigator}
                />
            </Container>
        )
    }
}

export default connect(
    mapStateToProps,
    { fetchUsersByTeam }
)(TeamAthletesScreen);
