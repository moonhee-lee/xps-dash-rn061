import React, { Component } from 'react';
import { connect } from 'react-redux';
import { RefreshControl, ScrollView, TouchableOpacity, View } from 'react-native';
import _ from 'lodash';
import { Navigation } from 'react-native-navigation';

import ActivityItem from '../../react/components/ActivityItem';
import {Container, Content, H2, Text, Button, Fab, Spinner, SwipeRow, Icon} from 'native-base';
import { fetchActivitiesBySession } from '../../redux/actions/activity';
import { getActivityIsFetching, getActivitiesBySession } from '../../redux/reducers';
import BottomTabBar from '../../react/components/BottomTabBar';
import * as formatUtils from '../../lib/format_utils';
import styles from '../../theme';
import SyncButton from '../../react/components/SyncButton';


export const mapStateToProps = (state, ownProps) => {
    return {
        activities: getActivitiesBySession(state, ownProps.session.id),
        isFetching: getActivityIsFetching(state),
    }
}

/**
 * react/screens/SessionDetailScreen
 *
 * main "home" screen for the app, when logged in.
 * lists all of the sessions.
 *
 */
export class SessionDetailScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            syncItems: 0,
        };
    }

    componentDidMount() {
        const { fetchActivitiesBySession, session } = this.props;
        fetchActivitiesBySession(parseInt(session.id));
    }

    _onPressActivityButton = (item) => {
        const { session, fromUserProfile } = this.props;
        const title = formatUtils.getActivityTitle(item);
        Navigation.push(this.props.componentId, {
            component: {
                name: 'ActivityRecordDetailListScreen',
                passProps: {
                    activityId: item.id,
                    activity: item,
                    session
                },
                options: {
                    topBar: {
                        title: {
                            text: 'Records for ' + title,
                        },
                        backButton: {
                            title: 'Activities'
                        }
                    }
                }
            }
        });
    }

    _renderRow = (activity, index) => {
        const { navigator } = this.props;

        return (
            <View style={{width:'100%'}}>
                <TouchableOpacity
                    onPress={() => this._onPressActivityButton(activity)}
                    style={{backgroundColor: index % 2 ? '#ffffff' : '#f7f7f7'}}>
                    <ActivityItem
                        id={activity.id}
                        activity={activity}
                        navigator={navigator}
                        index={index}
                    />
                </TouchableOpacity>
            </View>
        )
    }

    _onRefresh = () => {
        const { fetchActivitiesBySession, session } = this.props;
        fetchActivitiesBySession(parseInt(session.id));
    }

    render() {
        const { activities, navigator, isFetching, fromUserProfile } = this.props;
        const { syncItems } = this.state;

        let filteredActivitiesByOptionalMode = activities;
        if (global.enableSingleLegMode === false) {
            filteredActivitiesByOptionalMode = _.filter(activities, (activity) => {
                return (activity.type_definition.side === undefined || activity.type_definition.side === 'both');
            });
        }

        if (global.enableSkatingMode === false) {
            filteredActivitiesByOptionalMode = _.filter(filteredActivitiesByOptionalMode, (activity) => {
                return (activity.type_definition.skating === undefined || activity.type_definition.skating === null);
            });
        }

        if (global.enableBobsledMode === false) {
            filteredActivitiesByOptionalMode = _.filter(filteredActivitiesByOptionalMode, (activity) => {
                return (activity.type_definition.bobsled === undefined || activity.type_definition.bobsled === null);
            });
        }

        if (global.enableWalkingMode === false) {
            filteredActivitiesByOptionalMode = _.filter(filteredActivitiesByOptionalMode, (activity) => {
                return (activity.type_definition.walking === undefined || activity.type_definition.walking === null);
            });
        }

        const _filteredActivities = _.uniqBy(filteredActivitiesByOptionalMode, (activity) => {
            if(activity.type_definition.activity_type==='jump') {
                let uniqKey = activity.type_definition.activity_type+' '+activity.type_definition.orientation+' '+activity.type_definition.side;
                let optionalKey = global.enableSingleLegMode && activity.type_definition.triple_jump_type ? ' '+activity.type_definition.triple_jump_type : '';
                return uniqKey + optionalKey;
            } else if(activity.type_definition.activity_type==='sprint') {
                let uniqKey = activity.type_definition.activity_type+' '+activity.type_definition.start_type+' '+activity.type_definition.distance+' '+activity.type_definition.units+' '+activity.type_definition.side;
                let optionalKey = global.enableSkatingMode && activity.type_definition.skating ? ' ' + activity.type_definition.skating : '';
                optionalKey += global.enableBobsledMode && activity.type_definition.bobsled ? ' bobsled' : '';
                optionalKey += global.enableWalkingMode && activity.type_definition.walking ? ' walking' : '';
                return uniqKey + optionalKey;
            }
            else return activity.type_definition.activity_type+' '+activity.type_definition.start_type+' '+activity.type_definition.distance+' '+activity.type_definition.units;
        });
        const filteredActivities = _.sortBy(_filteredActivities, [
            function (item) { return item.activity_type; },
            function (item) { return item.start_type; },
            function (item) { return item.distance; }
        ], ["asc","asc","asc"]);

        if (isFetching) {
            return (
                <Container style={{ alignSelf: "center" }}>
                    <Spinner />
                </Container>
            )
        }

        return (
            <Container>
                <Content style={[(syncItems>0?{marginTop:40}:{})]}>
                    {!isFetching &&
                    <ScrollView
                        style={[styles.contentStyle]}
                        refreshControl={
                            <RefreshControl
                                refreshing={isFetching}
                                onRefresh={this._onRefresh}
                            />
                        }>
                        {!!filteredActivities && filteredActivities.length>0 && filteredActivities.map((activity, index) =>
                            this._renderRow(activity, index)
                        )}
                        {!filteredActivities || filteredActivities.length === 0 &&
                        <View style={{flex:1, padding: 20}}>
                            <Text style={{fontSize: 16}}>This session doesn't have any completed activities.</Text>
                        </View>
                        }
                    </ScrollView>
                    }
                </Content>
                <SyncButton
                    navigator={navigator}
                    setSyncItems={(syncItems) => this.setState({syncItems})}
                />
            </Container>
        )
    }
}


export default connect(
    mapStateToProps,
    { fetchActivitiesBySession }
)(SessionDetailScreen);
