import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Navigation } from 'react-native-navigation';


import {
    Container,
    View,
} from 'native-base';



export default class LiveModeTriggerScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isModalVisible: false
        };
        Navigation.events().bindComponent(this);
    }

    componentDidAppear() {
        const { isModalVisible } = this.state;

        if (isModalVisible) {
            this.setState({ isModalVisible: false });

            Navigation.mergeOptions('BOTTOM_TABS', {
                bottomTabs: {
                    currentTabIndex: 0,
                }
            });
        } else {
            this.setState({ isModalVisible: true });

            Navigation.showModal({
                stack: {
                    children: [{
                        component: {
                            name: 'LiveModeScreen',
                            passProps: {

                            },
                            options: {
                                topBar: {
                                    title: {
                                        text: 'Live Mode'
                                    },
                                    leftButtons: [{
                                        id: 'closeLiveModeScreen',
                                        text: 'Close',
                                        color: 'black',
                                    }]
                                }
                            }
                        }
                    }]
                }
            });
        }
    }

    render() {
        return (
            <Container></Container>
        )
    }
}
