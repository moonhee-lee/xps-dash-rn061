import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import { connect } from 'react-redux';
import {View, FlatList, TouchableOpacity} from 'react-native';
import {Container, Content, Spinner} from "native-base";
import FadeSlideItem from '../../react/components/FadeSlideItem';

import ActivitySummaryListItem from '../../react/components/ActivitySummaryListItem';
import { fetchActivitySummariesByActivity } from '../../redux/actions/activity_summary';
import { activitiesSummariesSelector, activitiesSummariesSelectorDesc, activitiesSummariesSelectorAsc } from '../../redux/selectors/activity_summaries';
import { activityRecordSelector } from '../../redux/selectors/activity_records';
import { teamAthletesSelector } from '../../redux/selectors/team_athletes';
import { getActivitySummariesByActivity, getCurrentTeam, getActiveSession, getActivityRecordsIsFetching } from '../../redux/reducers';
import { fetchActivityRecords } from '../../redux/actions/activity_record';
import { fetchUsersByTeam } from '../../redux/actions/user';

export const mapStateToProps = (state, ownProps) => {
    return {
        athletes: teamAthletesSelector(state, ownProps),
        activityRecords: activityRecordSelector(state, ownProps),
        activitySummaries: activitiesSummariesSelector(state, ownProps),
        currentTeam: getCurrentTeam(state),
        activeSession: getActiveSession(state),
        isFetching: getActivityRecordsIsFetching(state)
    }
}

/**
 * react/screens/ActivityLeaderboardScreen
 *
 * main "home" screen for the app, when logged in.
 * lists all of the sessions.
 *
 */
export class ActivityLeaderboardScreen extends Component {
    constructor(props) {
        super(props);
        Navigation.events().bindComponent(this);
    }

    componentDidMount() {
        const { activity, fetchActivityRecords, athletes, currentTeam, fetchUsersByTeam, activitySummaries, session } = this.props;
        fetchActivityRecords(activity.type_hash, session.id);
        if ((!athletes || athletes.length === 0) && currentTeam) fetchUsersByTeam(currentTeam);
    }

    componentDidUpdate(prevState,prevProps) {
        const { activity, activitySummaries, session } = this.props;
        if (prevProps && (activitySummaries.length !== prevProps.activitySummaries.length)) {
            fetchActivityRecords(activity.type_hash, session.id);
        }
        console.log(activitySummaries);
    }

    componentDidAppear() {
        const { activity, fetchActivityRecords } = this.props;
        fetchActivityRecords(activity.type_hash, activity.session);
    }

    _onPressButton = (item) => {
        const { activity, activityRecords } = this.props;
        this.props.navigator.push({
            screen: 'ActivityRecordDetailListScreen',
            title: 'Records for '+activity.display_name,
            backButtonTitle: 'Records',
            passProps: {
                activity,
                user: item.user,
                activityRecord: item,
                activityRecords
            },
        });
    }

    _keyExtractor = (item, index) => item.id.toString();

    _renderItem = ( { item, index } ) => {
        const { navigator, session } = this.props;

        return (
            <FadeSlideItem
                index={index}
            >
                <TouchableOpacity onPress={() => this._onPressButton(item)}>
                    <ActivitySummaryListItem
                        activitySummary={item}
                        session={session}
                        theme={'grey'}
                        navigator={navigator}
                        index={index}
                    />
                </TouchableOpacity>
            </FadeSlideItem>
        )
    };

    render() {
        const { activitySummaries, session, isFetching } = this.props;

        if(isFetching) return (
            <Content style={{ alignSelf: "center" }}>
                <Spinner />
            </Content>
        )

        return (
            <Container>
                <Content style={{width: '100%'}}>
                    <View style={{width: '100%'}}>
                        {!!activitySummaries && activitySummaries.length>0 &&
                        <FlatList
                            data={activitySummaries}
                            renderItem={this._renderItem}
                            session={session}
                            keyExtractor={this._keyExtractor}
                        />
                        }
                    </View>
                </Content>
            </Container>
        )
    }
}

export default connect(
    mapStateToProps,
    { fetchActivitySummariesByActivity, fetchActivityRecords, fetchUsersByTeam }
)(ActivityLeaderboardScreen);
