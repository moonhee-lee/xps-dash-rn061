import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import { connect } from 'react-redux';
import _ from 'lodash';

import {
    Container,
    Content, Spinner,
    Text,
    View
} from 'native-base';

import {ScrollView} from 'react-native';

import { teamAthletesSelector } from '../../redux/selectors/team_athletes';
import { saveActiveUser, deleteActiveUser, fetchActiveUsers, removeActiveUserFromActivity, getUserSavedTag } from '../../redux/actions/active_user';
import { allActiveUsersSelector, activityActiveUsersSelector } from '../../redux/selectors/active_users';
import { getActiveUsers, getActiveUsersIsFetching, getActiveActivity, getCurrentTeam, getCurrentOrg, getMembersByOrg } from '../../redux/reducers';
import { fetchMembersByOrg } from '../../redux/actions/org_member';
import { removeTagLiveData } from '../../redux/actions/tag';
import ListCheckboxItem from '../../react/components/ListCheckboxItem';
import AthleteCreateBar from '../../react/components/AthleteCreateBar';
import styles from '../../theme';

export const mapStateToProps = (state, ownProps) => {
    return {
        currentOrg: getCurrentOrg(state),
        athletes: teamAthletesSelector(state, ownProps),
        activityActiveUsers: activityActiveUsersSelector(state, ownProps),
        allActiveUsers: allActiveUsersSelector(state, ownProps),
        isFetching: getActiveUsersIsFetching(state),
        activeActivity: getActiveActivity(state),
        currentTeam: getCurrentTeam(state),
        orgMembers: getMembersByOrg(state),
    }
}

export class LiveAthleteSelectScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedAthletes: new Set()
        };
        Navigation.events().bindComponent(this);
    }

    componentDidMount() {
        const { activityActiveUsers, fetchActiveUsers } = this.props;
        if (activityActiveUsers.length) {
            this.updateSelectedAthleteState();
        } else {
            fetchActiveUsers();
        }
    }

    componentDidUpdate(prevProps, prevState) {
        const { activityActiveUsers, athletes, activeUsers } = this.props;
        if (activityActiveUsers.length !== prevProps.activityActiveUsers.length) {
            this.updateSelectedAthleteState();
        }
    }

    navigationButtonPressed() {
        Navigation.pop(this.props.componentId);
    }

    openAthleteCreate = () => {

    }

    updateSelectedAthleteState = () => {
        const { activityActiveUsers } = this.props;

        let selectedAthletes = new Set();
        _.each(activityActiveUsers, (activityActiveUser) => {
            selectedAthletes.add(activityActiveUser.user.id);
        });

        this.setState({ selectedAthletes });
    }

    findActiveActivityUser = (athlete) => {
        const { allActiveUsers } = this.props;
        return _.find(allActiveUsers, (actUser) => {
            if (actUser.user && actUser.user.id === athlete.id) {
                return actUser;
            }
        });
    }

    toggleAthlete = (athlete) => {
        const { saveActiveUser, removeTagLiveData, activity, removeActiveUserFromActivity, getUserSavedTag, useSavedTag, onPressRemoveAthlete, activeUsers, activityActiveUsers } = this.props;
        const { selectedAthletes } = this.state;
        let isInActivity = false;

        const activityActiveUser = this.findActiveActivityUser(athlete);
        let activeUser = _.find(activeUsers,(activeUser) => activeUser.user.id === athlete.id);
        if(!activeUser) activeUser = _.find(activityActiveUsers,(activeUser) => activeUser.user.id === athlete.id);

        if (athlete.tag) {
            removeTagLiveData(parseInt(athlete.tag.name, 10));
        }

        if (selectedAthletes.has(athlete.id)) {
            selectedAthletes.delete(athlete.id);
            onPressRemoveAthlete(activeUser);
        } else {
            selectedAthletes.add(athlete.id);
            isInActivity = true;
            let activeUser;
            getUserSavedTag(athlete.id).then((tag) => {
                if (activityActiveUser) {
                    activeUser = {
                        ...activityActiveUser,
                        tag,
                        tag_data: null,
                        isInActivity
                    };
                    saveActiveUser(activeUser);
                } else {
                    activeUser = {
                        activity,
                        tag,
                        tag_data: null,
                        user: athlete,
                        isInActivity
                    };
                    saveActiveUser(activeUser);
                }
                useSavedTag(activeUser);
                this.setState({ selectedAthletes });
            }).catch((err) => {
                if (activityActiveUser) {
                    saveActiveUser({
                        ...activityActiveUser,
                        isInActivity
                    });
                } else {
                    saveActiveUser({
                        activity,
                        user: athlete,
                        isInActivity
                    });
                }
                this.setState({ selectedAthletes });
            });
        }
    }

    render() {
        const { athletes, isFetching, activityActiveUsers, navigator, currentOrg, currentTeam } = this.props;
        const { selectedAthletes } = this.state;

        if (isFetching) {
            return (
                <Container style={{ alignSelf: "center" }}>
                    <Spinner />
                </Container>
            )
        }

        const selAthletes = _.filter(athletes, (athlete) => {
            return selectedAthletes.has(athlete.id)
        });

        return (
            <Container style={{flex:1}}>
                <Content style={[styles.contentStyleNoBottom,{flex:1,flexDirection: 'column',borderTopWidth:10,borderTopColor:'#d7d7d7'}]}>
                    <ScrollView contentContainerStyle={{borderTopWidth:10,borderTopColor:'#d7d7d7',flexGrow: 1}}>
                    {!!athletes && athletes.length > 0 && athletes.map( (athlete,index) =>
                        <View key={index}>
                            <ListCheckboxItem
                                key={athlete.id}
                                id={athlete.id}
                                isChecked={selectedAthletes.has(athlete.id)}
                                label={athlete.full_name}
                                item={athlete}
                                handlePressCallback={this.toggleAthlete}
                                index={index}
                                showAvatar={athlete}
                                navigator={navigator}
                            />
                        </View>
                    )}
                    </ScrollView>
                </Content>
                <AthleteCreateBar
                    navigator={navigator}
                    onOpen={this.openAthleteCreate}
                    message={'Add an Athlete'}
                    currentOrg={currentOrg}
                    currentTeam={currentTeam}
                    toggleAthlete={this.toggleAthlete}
                />
            </Container>
        )
    }
}


export default connect(
    mapStateToProps,
    {
        saveActiveUser,
        deleteActiveUser,
        fetchActiveUsers,
        getActiveActivity,
        removeTagLiveData,
        fetchMembersByOrg,
        removeActiveUserFromActivity,
        getUserSavedTag
    }
)(LiveAthleteSelectScreen);
