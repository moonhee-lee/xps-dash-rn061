import React, { Component } from 'react';
import { connect } from 'react-redux';

import _ from 'lodash';
import {
    Container,
    View,
    Text,
    List,
    Button,
    Icon,
    Spinner,
    SwipeRow, Content, Item, Toast, Root,
} from 'native-base';

import { ProgressCircle } from 'react-native-svg-charts'

import * as formatUtils from '../../lib/format_utils';

import styles from '../../theme';
import { Picker } from 'react-native-wheel-datepicker';
import TopMessageBar from '../../react/components/TopMessageBar';

import { getCurrentOrg, getCurrentSystem, getCurrentTags, getActivityById, getMembersByOrg } from '../../redux/reducers';
import { connectSystem, disconnectSystem, startSync, stopSync } from '../../redux/actions/system';
import { fetchActivity, toggleRecording, resetRecording } from '../../redux/actions/activity';
import { activityUsersSelector } from '../../redux/selectors/activity_users';
import { fetchActivityUsersByActivity, deleteActivityUser, saveActivityUser, setActivityBeepStartTime, clearActivityUsersVelocity } from '../../redux/actions/activity_user';
import { fetchActivitySummariesByActivity } from '../../redux/actions/activity_summary';
import { getActivityUsersByActivity, getActivityUsersIsFetching, getTagsInLiveData } from '../../redux/reducers';
import { setupTag, removeTagLiveData, fetchTagsBySystem, fetchTagsByOrg } from '../../redux/actions/tag';
import { saveOrgMember, fetchMembersByOrg } from '../../redux/actions/org_member';
import { getESTPosition } from '../../redux/local_storage';

export const mapStateToProps = (state, ownProps) => {
    return {
        currentOrg: getCurrentOrg(state),
        currentSystem: getCurrentSystem(state),
        tags: getCurrentTags(state),
        currentTags: getTagsInLiveData(state),
    }
}

export class MeasuringStickScreen extends Component {
    constructor(props) {
        super(props);
        this.distanceInterval = null;
        this.rows = [];
        this.state = {
            selectedTag: 1,
            tag: null,
            names: []
        }
    }

    componentDidMount() {
        this.connectSystemAndTags();
    }

    connectSystemAndTags() {
        const { currentSystem, connectSystem, fetchTagsByOrg, currentOrg, currentTags } = this.props;
        if(currentSystem) {
            connectSystem(currentSystem, true);
            fetchTagsByOrg(currentOrg.id);
        }
    }

    componentDidUpdate(prevProps,prevState) {
        const { tags, currentTags } = this.props;
        const { selectedTag, names } = this.state;

        if(!!names && names.length===0) {
            if(tags.length>0) {
                const tagNames = [];
                if (tags) {
                    tags.map((tag) => {
                        tagNames.push(tag.name);
                    })
                    this.setState({names:tagNames});
                }
            }
        }

        if(!this.distanceInterval && currentTags && currentTags[this.state.selectedTag]) {
            this.distanceInterval = setInterval(() => {
                if (this.state.selectedTag) {
                    const position = getESTPosition(this.state.selectedTag);
                    if (position) {
                        this.setState({position: position});
                    }
                }
            }, 500);
        }


    }

    componentWillUnmount() {
        const { currentSystem, disconnectSystem } = this.props;
        console.log('disconnecting system');
        if (currentSystem) {
            if (currentSystem.state === 'connected' || currentSystem.state === 'connecting') disconnectSystem(currentSystem);
        }
        clearInterval(this.distanceInterval);
        this.distanceInterval = null;
    }

    callSetupTag() {
        const { tags, currentActivity, setupTag, currentSystem } = this.props;
        const currentTag = _.find(tags, (t) => t.name === this.state.selectedTag);

        console.log('setting up tag');
        setupTag(currentSystem, currentTag, null, null);

        //send tag reset
        const msg = {cmd_id:5,tag_rst:{tag_id:parseInt(this.state.selectedTag)}};
        global.client.sendMessage(msg);
    }

    saveTag() {
        if(!this.state.selectedTag && this.state.names.length>0) {
            this.setState({selectedTag:this.state.names[0]});
        } else {
            console.log('no tags available to set up');
        }
        this.callSetupTag();
    }

    render() {
        const { selectedTag, names, position } = this.state;
        const { currentSystem, currentTags } = this.props;

        return (
            <Root>
                <Container>
                    <Content style={[styles.contentStyle,(currentSystem.state==='connected' || currentSystem.state==='disconnected' || currentSystem.state==='connecting')?{marginTop:40}:{}]}>
                        {names && names.length > 0 &&
                        <View style={{
                            flex: 1,
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            height: 80, overflow: 'hidden', borderBottomWidth: 2, borderBottomColor: '#d7d7d7'
                        }}>
                            <Text style={{lineHeight: 80, paddingLeft: 20, paddingRight: 20}}>Choose Tag:</Text>
                            <Item picker style={{flex: 1, marginBottom: 10, borderWidth: 0, borderBottomWidth: 0}}>
                                <Picker
                                    style={{flex: 1, backgroundColor: '#e6e6e6'}}
                                    selectedValue={this.state.selectedTag}
                                    pickerData={names}
                                    onValueChange={(tagName) => this.state.selectedTag = tagName}
                                />
                            </Item>
                            <Button success style={{height: '100%'}} onPress={this.saveTag.bind(this)}>
                                <Icon type="FontAwesome" name="check"/>
                            </Button>
                        </View>
                        }

                        {currentTags && currentTags[selectedTag] &&
                        <View style={{flex: 1, alignItems:'center', height:60, width:'100%',borderBottomColor:'#d7d7d7',borderBottomWidth: 2}}>
                            <Text style={{lineHeight:60,alignSelf:'center', fontSize:18,color:'#444444'}}>Tag: {currentTags[selectedTag].name} - {currentTags[selectedTag].status}</Text>
                        </View>
                        }
                        {position && (currentTags[selectedTag].status==='selected' || currentTags[selectedTag].status==='ready') &&
                        <View style={{
                            width:300,height:300,alignSelf:'center',marginTop:40
                        }}>
                            <ProgressCircle
                                style={ { height: 300, width:300, position:'absolute',top:20,left:0 } }
                                progress={ position.y/40 }
                                progressColor={'rgba(0,0,0,.7)'}
                                startAngle={ -Math.PI * 0.8 }
                                endAngle={ Math.PI * 0.8 }
                                strokeWidth={20}
                            />
                            <ProgressCircle
                                style={ { height: 240, width:240, position:'absolute',top:50,left:30 } }
                                progress={ position.z/3 }
                                progressColor={'rgba(0,0,0,.5)'}
                                startAngle={ -Math.PI * 0.8 }
                                endAngle={ Math.PI * 0.8 }
                                strokeWidth={20}
                            />
                            <View
                                style={ { width:300,position:'absolute',top:140,left:0,alignItems:'center' } }
                            >
                                <Text>Length: {formatUtils.formatDistance(position.y)}</Text>
                                <Text>Height: {formatUtils.formatDistance(position.z)}</Text>
                            </View>
                        </View>
                        }
                    </Content>

                    {!!currentSystem && currentSystem.state==='disconnected' &&
                    <TopMessageBar
                        navigator={navigator}
                        type='error'
                        message="Not connected to XPS service broker"
                        onPressReload={this.connectSystemAndTags}
                    />
                    }

                    {!!currentSystem && currentSystem.state==='connecting' &&
                    <TopMessageBar
                        navigator={navigator}
                        type='working'
                        message="Connecting to XPS service broker"
                    />
                    }

                    {!!currentSystem && currentSystem.state==='connected' && currentSystem.serial_num &&
                    <TopMessageBar
                        navigator={navigator}
                        type='success'
                        message={"Connected to system "+currentSystem.serial_num}
                    />
                    }

                    {!!currentSystem && currentSystem.state==='connected' && currentSystem.ip_address &&
                    <TopMessageBar
                        navigator={navigator}
                        type='success'
                        message={"Connected to system "+currentSystem.ip_address}
                    />
                    }

                </Container>
            </Root>
        )
    }
}

export default connect(
    mapStateToProps,
    {
        connectSystem,
        disconnectSystem,
        fetchActivity,
        toggleRecording,
        resetRecording,
        fetchActivityUsersByActivity,
        deleteActivityUser,
        setActivityBeepStartTime,
        removeTagLiveData,
        setupTag,
        saveActivityUser,
        saveOrgMember,
        fetchMembersByOrg,
        fetchTagsBySystem,
        fetchTagsByOrg,
        startSync,stopSync,
        clearActivityUsersVelocity,
        getESTPosition
    }
)(MeasuringStickScreen);
