import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ScrollView, FlatList, TouchableOpacity, RefreshControl } from 'react-native';
import {
    Container,
    Content,
    Button,
    Text,
    Spinner,
} from 'native-base';
import _ from 'lodash';

import FadeSlideItem from '../../react/components/FadeSlideItem';
import SessionListItem from '../../react/components/SessionListItem';
import { sessionsSelector } from '../../redux/selectors/sessions';
import { getCurrentOrg, getSessionsIsFetching, getActiveSession, getTeamsByCurrentOrg, getCurrentTeam, getCurrentTags, getActivities } from '../../redux/reducers';
import { fetchSessionsByOrg } from '../../redux/actions/session';
import { fetchTeamsByOrg, setCurrentTeam } from '../../redux/actions/team';
import { setActiveSession } from '../../redux/actions/active_session';
import { fetchTagsByOrg } from '../../redux/actions/tag';
import { fetchCurrentCloudState } from '../../redux/actions/cloud';
import BottomTabBar from '../../react/components/BottomTabBar';
import SyncButton from '../../react/components/SyncButton';
import styles from '../../theme';
import {Navigation} from 'react-native-navigation';

export const mapStateToProps = (state) => {
    return {
        currentOrg: getCurrentOrg(state),
        currentTeam: getCurrentTeam(state),
        teams: getTeamsByCurrentOrg(state),
        sessions: sessionsSelector(state),
        isFetching: getSessionsIsFetching(state),
        activeSession: getActiveSession(state),
        tags: getCurrentTags(state),
    }
}

/**
 * react/screens/SessionListScreen
 *
 * main "home" screen for the app, when logged in.
 * lists all of the sessions.
 *
 */
export class SessionListScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            syncItems: 0,
            refreshing: false,
        };
    }

    componentDidMount() {
        const { currentOrg, fetchSessionsByOrg, sessions, teams, fetchTeamsByOrg, fetchTagsByOrg, tags } = this.props;
        if (!sessions || sessions.length === 0) fetchSessionsByOrg(currentOrg.id);
        if (!teams || teams.length === 0) fetchTeamsByOrg(currentOrg.id);
        if (!tags || tags.length === 0) fetchTagsByOrg(currentOrg.id);
        fetchCurrentCloudState(true);
    }

    componentDidUpdate(prevProps,prevState) {
        const { teams, setCurrentTeam, currentTeam, sessions, fetchSessionsByOrg, activities, currentOrg } = this.props;

        if(teams.length > 0 && teams.length!==prevProps.teams.length) {
            if(!_.find(teams, (o) => o.id === currentTeam)) {
                console.log('setting team from defaults: '+teams[0].id)
                setCurrentTeam(teams[0].id);
            }
        }
    }

    _onPressSessionButton = (session) => {
        const { setActiveSession } = this.props;

        setActiveSession(session);
        Navigation.push(this.props.componentId, {
            component: {
                name: 'SessionDetailScreen',
                passProps: {
                    session
                },
                options: {
                    topBar: {
                        title: {
                            text: session.name+' Activities'
                        }
                    }
                }
            }
        });
    }

    _keyExtractor = (item, index) => item.id.toString();

    _renderItem = ( { item, index } ) => {
        return (
            <TouchableOpacity onPress={() => this._onPressSessionButton(item)}>
                <FadeSlideItem
                    index={index}
                >
                    <SessionListItem
                        id={item.id}
                        session={item}
                        index={index}
                    />
                </FadeSlideItem>
            </TouchableOpacity>
        )
    };

    _onRefresh = () => {
        this.setState({refreshing: true});
        setTimeout(() => {
            const { currentOrg, fetchSessionsByOrg } = this.props;
            fetchSessionsByOrg(currentOrg.id);
            this.setState({refreshing: false});
        },1000);
    }

    render() {
        const { sessions, isFetching } = this.props;
        const { syncItems } = this.state;
        return (
            <Container>
                {isFetching &&
                <Content style={{ alignSelf: "center" }}>
                    <Spinner />
                </Content>
                }
                {!isFetching &&
                <Content style={[(syncItems>0?{marginTop:40}:{})]}>
                <ScrollView
                    style={[styles.contentStyle]}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                        />
                    }>

                    {!!sessions && sessions.length > 0 &&
                    <FlatList
                        data={sessions}
                        renderItem={this._renderItem}
                        keyExtractor={this._keyExtractor}
                    />
                    }
                </ScrollView>
                </Content>
                }
                <SyncButton
                    setSyncItems={(syncItems) => this.setState({syncItems})}
                />
            </Container>
        )
    }
}

export default connect(
    mapStateToProps,
    { fetchSessionsByOrg, setActiveSession, fetchTeamsByOrg, setCurrentTeam, fetchTagsByOrg, fetchCurrentCloudState }
)(SessionListScreen);
