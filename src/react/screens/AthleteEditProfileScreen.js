import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import { connect } from 'react-redux';
import validate from 'validate.js'
import {
    Container,
    Content,
    H2,
    Item,
    Text,
    Button,
    Picker,
    Textarea,
    Spinner,
    View,
} from 'native-base';
import { TouchableOpacity, Image } from 'react-native';

import styles from '../../theme';
import { saveOrgMember } from '../../redux/actions/org_member';
import { saveLocalUserImage } from '../../redux/actions/user';
import { Input } from 'react-native-elements';
import SaveBar from '../../react/components/SaveBar';
import { fetchUsersByTeam } from '../../redux/actions/user';

export class AthleteEditProfileScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: this.props.athlete.full_name,
            email: this.props.athlete.email,
            bio: this.props.athlete.bio,
            nameError: '',
            image: this.props.athlete.image_url,
            thumbnail: this.props.athlete.thumbnail_url,
            local_image:null,
        };
        Navigation.events().bindComponent(this);
    }

    componentDidUpdate(prevProps,prevState) {
        const { athlete } = this.props;
        if(athlete!==prevProps.athlete) {
            this.setState({name:athlete.first_name+' '+athlete.first_name,email:athlete.email,bio:athlete.bio,image:athlete.image_url,thumbnail: this.props.athlete.thumbnail_url});
        }
    }

    navigationButtonPressed() {
        if(!!this.props.onClose) this.props.onClose();
        Navigation.dismissModal(this.props.componentId);
    }

    validateName = (name) => {
        return validate.single(name, {
            presence: {
                allowEmpty: false,
                message: 'A name is required'
            }
        });
    }

    handleSubmit = () => {
        const { name, email, bio } = this.state;
        const { athlete, session, saveOrgMember } = this.props;
        const { fetchUsersByTeam, team, currentOrg } = this.props;
        const nameError = this.validateName(name);

        this.setState({
            nameError: nameError ? nameError[0] : '',
        });

        if (!nameError) {
            const nameSplit = name.split(' ');
            const first_name = nameSplit.slice(0, 1).join(' ');
            const last_name = nameSplit.slice(1, nameSplit.length).join(' ');

            saveOrgMember({
                user: {
                    first_name: first_name,
                    last_name: last_name,
                    email: email,
                    bio: bio,
                    id: athlete.id,
                },
                id: athlete.id,
                org: currentOrg.id
            }, this.props.navigator).then((data) => {
                let teamId = team;
                if (team && typeof(team) === 'object' && team.id) teamId = team.id;
                console.log('fetching users again');
                fetchUsersByTeam(teamId).then(() => {
                    console.log('got new user data!');
                    this.setState({saving:false});
                    Navigation.dismissModal(this.props.componentId);
                }).catch((error) => {console.log(error);});
            }).catch((error) => {console.log(error);});
            this.setState({
                saving: true
            });
        }
    }

    refreshUsers() {
        const { fetchUsersByTeam, team } = this.props;
        console.log('refreshing user list')
        let teamId = team;
        if (team && typeof(team) === 'object' && team.id) teamId = team.id;
        fetchUsersByTeam(teamId).then((data) => {}).catch((error) => {console.log(error);});
    }

    handleImage(localImage) {
        const { athlete, saveLocalUserImage } = this.props;
        this.setState({local_image:localImage});
        saveLocalUserImage(athlete.id, localImage);
        this.refreshUsers();
        Navigation.dismissModal(this.props.componentId);
    }

    openCamera() {
        const { athlete, team } = this.props;
        this.props.navigator.showModal({
            screen: 'CameraScreen',
            passProps: {
                onHandleCamera: (localImage) => this.handleImage(localImage),
                athlete: athlete,
                team: team
            },
            navigatorStyle: {
                navBarBackgroundColor: '#000',
                navBarTextColor: '#fff',
            },
            animationType: 'slide'
        });
    }

    render() {
        const { name, nameError, email, bio, image, local_image, saving } = this.state;

        return (
            <Container>
                <Content style={{width: '100%', padding: 20}}>
                    <Item style={{marginTop:20}}>
                        <Input
                            value={name}
                            onChangeText={(name) => this.setState({ name: name.trim() })}
                            placeholder={'Enter a Name'}
                            placeholderTextColor={'#666'}
                            inputStyle={styles.loginInputStyle}
                            containerStyle={styles.loginContainerStyle}
                            textStyle={styles.loginTextStyle}
                        />
                    </Item>
                    {nameError &&
                    <Text style={styles.inputErrorStyle}>{nameError}</Text>
                    }

                    <Item style={{marginTop:20}}>
                        <Input
                            value={email}
                            onChangeText={(email) => this.setState({ email: email.trim() })}
                            placeholder={'Enter an Email'}
                            placeholderTextColor={'#666'}
                            inputStyle={styles.loginInputStyle}
                            containerStyle={styles.loginContainerStyle}
                            textStyle={styles.loginTextStyle}
                        />
                    </Item>

                    <Item style={{marginTop:20}}>
                        <Input
                            value={bio}
                            onChangeText={(bio) => this.setState({ bio: bio.trim() })}
                            placeholder={'Enter a Bio'}
                            placeholderTextColor={'#666'}
                            inputStyle={styles.loginInputStyle}
                            containerStyle={styles.loginContainerStyle}
                            textStyle={styles.loginTextStyle}
                        />
                    </Item>

                    <View style={{paddingTop:40}}>
                        {!!local_image &&
                        <TouchableOpacity onPress={() => this.openCamera()} style={{height:200,width:200,alignSelf:'center'}}>
                            <Image source={{uri:local_image}} style={{height:200,width:200,borderRadius:100,borderWidth:4,borderColor:"#d7d7d7"}} />
                        </TouchableOpacity>
                        }
                        {!!image && !local_image &&
                        <TouchableOpacity onPress={() => this.openCamera()} style={{height:200,width:200,alignSelf:'center'}}>
                            <Image source={{uri:image}} style={{height:200,width:200,borderRadius:100,borderWidth:4,borderColor:"#d7d7d7"}} />
                        </TouchableOpacity>
                        }
                        {!image && !local_image &&
                        <TouchableOpacity onPress={() => this.openCamera()} style={{height:200,width:200,alignSelf:'center',borderRadius:100,borderWidth:4,borderColor:"#d7d7d7"}}>
                            <Text style={{paddingTop:80,alignSelf:'center'}}>Add Profile Picture</Text>
                        </TouchableOpacity>
                        }
                    </View>
                </Content>
                {!saving &&
                <SaveBar
                    onPressSave={this.handleSubmit}
                />
                }
                {!!saving &&
                <SaveBar
                    message='Saved'
                    bgColor='#6AC893'
                    onPressSave={this.handleSubmit}
                />
                }
            </Container>
        )
    }
}


export default connect(
    null,
    { saveOrgMember, fetchUsersByTeam, saveLocalUserImage }
)(AthleteEditProfileScreen);

