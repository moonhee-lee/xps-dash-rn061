import React, { Component } from 'react';
import { View, Text } from 'react-native';
import {Container, Content} from "native-base";
import styles from '../../theme';

/**
 * react/screens/SavingOverlayScreen
 * *
 */
class SavingOverlayScreen extends Component {
    render() {
        const { message } = this.props;

        return (
            <View style={styles.saveOverlay}>
                <Text style={styles.saveOverlayText}>{message}</Text>
            </View>
        )
    }
}

export default SavingOverlayScreen;
