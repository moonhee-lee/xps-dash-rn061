import React, { Component } from 'react';
import { connect } from 'react-redux';

import _ from 'lodash';
import {
    Container,
    Text,
    List,
    Button,
    Icon,
    Content, Item, Root, ListItem, Left, Body, Right, Picker, Form, Toast
} from 'native-base';

import { localStorage} from "../../lib/local_storage";

import { View } from 'react-native'

import { getCurrentOrg, getCurrentSystem, getCurrentTags, getActivityById, getMembersByOrg, getTagLiveDataById, getTagsInLiveData, getSystemsByOrg, getWifiState } from '../../redux/reducers';
import { connectSystem, disconnectSystem, startSync, saveSystemInfo } from '../../redux/actions/system';
import { setupTag, removeTagLiveData, fetchTagsBySystem, fetchTagsByOrg } from '../../redux/actions/tag';
import TopMessageBar from '../../react/components/TopMessageBar';
import { fetchSystemsByOrg } from '../../redux/actions/org_system';
import { connectToWifiNetwork, stopWifiReconnect } from '../../redux/actions/wifi';

import styles from '../../theme';

export const mapStateToProps = (state, ownProps) => {
    return {
        currentOrg: getCurrentOrg(state),
        currentSystem: getCurrentSystem(state),
        tags: getCurrentTags(state),
        liveTags: getTagsInLiveData(state),
        orgSystems: getSystemsByOrg(state),
        wifiState: getWifiState(state),
    }
}

export class SystemStatusScreen extends Component {
    constructor(props) {
        super(props);
        this.rows = [];
        this.wifiPassword = 'xco2015!';
        this.state = {
            activeTags: null,
            currentSystem: null,
            networks: [],
            systems: [],
            powerLevel: global.uwbTransmitPowerLevel,
            isFetchingSystems: false,
            isWifiConnecting: false,
        };
    }

    componentDidMount() {
        const { currentOrg, orgSystems, fetchSystemsByOrg, currentSystem, connectSystem } = this.props;
        const { wifiState, connectToWifiNetwork } = this.props;

        if (currentOrg && currentOrg.id && (!orgSystems || _.isEmpty(orgSystems))) {
            fetchSystemsByOrg(currentOrg.id);
            this.setState({isFetchingSystems: true});
        } else {
            if (wifiState.isConnected && wifiState.deviceSSID == currentSystem.ssid) {
                connectSystem(currentSystem, true);
            } else {
                const { ssid, password } = currentSystem;
                connectToWifiNetwork(ssid, password, true);
                this.setState({isWifiConnecting: true});
            }
            this.setupLists();
        }

        if (currentOrg) fetchTagsByOrg(currentOrg);
        if (currentSystem && currentSystem.id) {
            this.setState({currentSystem:currentSystem.id});
        }

    }

    componentDidUpdate(prevProps,prevState) {
        const { orgSystems, currentSystem, wifiState, connectToWifiNetwork, connectSystem } = this.props;
        const { isWifiConnecting, systems } = this.state;

        if(orgSystems && (_.size(orgSystems)!==_.size(prevProps.orgSystems))) {
            if (wifiState.isConnected && wifiState.deviceSSID == currentSystem.ssid) {
                connectSystem(currentSystem, true);
            } else {
                const { ssid, password } = currentSystem;
                connectToWifiNetwork(ssid, password, true);
                this.setState({isWifiConnecting: true});
            }
            this.setupLists();
        }

        // does our local component state thing we're attempting to connect to wifi?
        if (isWifiConnecting) {
            // now check the global wifi states to see if we need to take action.
            if (wifiState.isConnected) {
                // cool, we're connected. set local state and try the hub
                this.setState({isWifiConnecting: false});
                connectSystem(currentSystem, true);
            }
        }
    }

    setupLists() {
        const { orgSystems } = this.props;
        let _networks = _.filter(orgSystems,(system) => !!system.wlan_ssid);
        _networks = _.orderBy(_networks, ['system.name'],['asc']);
        const _systems = _.orderBy(orgSystems, ['name'],['asc']);
        this.setState({networks: _networks,systems:_systems});
    }

    componentWillUnmount() {
        const { currentSystem, disconnectSystem, stopWifiReconnect } = this.props;
        if (currentSystem) {
            if (currentSystem.state === 'connected' || currentSystem.state === 'connecting') disconnectSystem(currentSystem);
            //fetchTagsBySystem(currentSystem.id);
        }
        stopWifiReconnect();
    }

    onChangeSystem(index) {
        const { user, orgSystems } = this.props;
        const { connectSystem, connectToWifiNetwork, saveSystemInfo, fetchTagsByOrg, currentOrg } = this.props;

        const orgSystem = _.find(orgSystems, (orgSystem) => orgSystem.id === index);

        if(orgSystem && orgSystem) {
            disconnectSystem(global.xpsConfig.system);

            global.xpsConfig.system[1].ip_address = orgSystem.hostname;
            global.xpsConfig.system[1].port = 1884;
            global.xpsConfig.system[1].ssid = orgSystem.wlan_ssid;
            global.xpsConfig.system[1].password = orgSystem.wlan_password;
            global.xpsConfig.system[1].serial_num = orgSystem.serial_num;
            global.xpsConfig.system[1].name = orgSystem.name;
            global.xpsConfig.system[1].system = orgSystem.id;
            global.xpsConfig.system[1].id = orgSystem.id;

            if(!!orgSystem.wlan_ssid) {
                const password = orgSystem.wlan_password ? orgSystem.wlan_password : this.wifiPassword;
                connectToWifiNetwork(orgSystem.wlan_ssid, password, true);
                this.setState({isWifiConnecting: true});
            }

            localStorage.setItem('xps:appConfig', global.xpsConfig);

            saveSystemInfo(global.xpsConfig.system);

            // TODO - this is still a hardcoded system ID 1
            connectSystem(global.xpsConfig.system[1],true);
            fetchTagsByOrg(currentOrg.id);

            this.setState({currentSystem:orgSystem.id});
        }
    }

    onChangeWiFi(ssid) {
        const { orgSystems, connectToWifiNetwork } = this.props;
        const orgSystem = _.find(orgSystems, (orgSystem) => orgSystem.wlan_ssid === ssid);
        const password = orgSystem.wlan_password ? orgSystem.wlan_password : this.wifiPassword;

        connectToWifiNetwork(ssid, password, true);
        this.setState({isWifiConnecting: true});
    }

    onChangePowerLevel(value) {
        global.uwbTransmitPowerLevel = value;
        this.setState({powerLevel: value});

        // Set Power Level
        const message = {cmd_id: 4, pwr_lvl: {pwrlvl: global.uwbTransmitPowerLevel}};
        global.client.sendMessage(message, 'control');
    }

    resetTracker() {
        const { currentSystem, disconnectSystem, connectSystem, fetchTagsByOrg, currentOrg } = this.props;
        console.log('send tracker reset');
        const msg = {cmd_id:11, msg_id:4294967295};
        global.client.sendMessage(msg);
        setTimeout(() => {
            console.log('finished resetting system');
            disconnectSystem(currentSystem);
            setTimeout(() => {
                connectSystem(currentSystem,true);
                fetchTagsByOrg(currentOrg.id);
            },500)
        },500);
    }

    render() {
        const { currentSystem, navigator, orgSystems, wifiState } = this.props;
        const { tags, liveTags } = this.props;
        const { isWifiConnecting, networks, systems } = this.state;
        const hubName = currentSystem.name ? currentSystem.name : (currentSystem.ssid ? currentSystem.ssid : currentSystem.ip_address);

        const currentNetwork = _.find(networks,(network) => network.wlan_ssid===wifiState.deviceSSID);

        return (
            <Root>
                <Container>
                    <Content style={[styles.contentStyle,(currentSystem && (currentSystem.state==='connected' || currentSystem.state==='disconnected' || currentSystem.state==='connecting'))?{marginTop:40}:{}]}>
                        {!!currentSystem && currentSystem.id &&
                        <Form style={{flex:1}}>
                            <Item picker style={{flex:1,flexDirection: 'row',justifyContent: 'space-between'}}>
                                <Text style={{width:'25%',paddingLeft:15,fontWeight:'bold'}}>System: </Text>
                                <Picker
                                    mode="dropdown"
                                    iosHeader="Systems"
                                    placeholder="Systems"
                                    style={{width:undefined}}
                                    placeholderStyle={{ color: "#bfc6ea" }}
                                    placeholderIconColor="#007aff"
                                    selectedValue={this.state.currentSystem}
                                    onValueChange={this.onChangeSystem.bind(this)}
                                >
                                    {systems && systems.map((orgSystem,index) =>
                                        <Picker.Item label={orgSystem.name} value={orgSystem.id} key={index} />
                                    )}
                                </Picker>
                            </Item>
                        </Form>
                        }

                        <Form style={{flex:1}}>
                            <Item picker style={{flex:1,flexDirection: 'row',justifyContent: 'space-between'}}>
                                <Text style={{width:'25%',paddingLeft:15,fontWeight:'bold'}}>Network: </Text>
                                <Picker
                                    mode="dropdown"
                                    iosHeader="Change WiFi Network"
                                    placeholder="Change WiFi Network"
                                    style={{width:undefined}}
                                    placeholderStyle={{ color: "#bfc6ea" }}
                                    placeholderIconColor="#007aff"
                                    selectedValue={currentNetwork}
                                    onValueChange={this.onChangeWiFi.bind(this)}
                                >
                                    {networks && networks.map((network,index) =>
                                            <Picker.Item label={network.name}
                                                         value={network.wlan_ssid} key={index} />
                                    )}
                                </Picker>
                            </Item>
                        </Form>

                        <Form style={{flex:1}}>
                            <Item picker style={{flex:1,flexDirection: 'row',justifyContent: 'space-between'}}>
                                <Text style={{width:'25%',paddingLeft:15,fontWeight:'bold'}}>Power Level: </Text>
                                <Picker
                                    mode="dropdown"
                                    iosHeader="Change Power Level"
                                    placeholder="Change Power Level"
                                    style={{width:undefined}}
                                    placeholderStyle={{ color: "#bfc6ea" }}
                                    placeholderIconColor="#007aff"
                                    selectedValue={this.state.powerLevel}
                                    onValueChange={this.onChangePowerLevel.bind(this)}
                                >
                                    {_.range(0, 34, 1).map((value,index) =>
                                        <Picker.Item label={'Level ' +  value}
                                                     value={value} key={index} />
                                    )}
                                </Picker>
                            </Item>
                        </Form>

                        {!!currentSystem &&
                        <List>
                            <ListItem itemHeader first>
                                <Text style={{fontSize:16,fontWeight:'bold'}}>XPS System Details</Text>
                            </ListItem>

                            {currentSystem.name &&
                            <ListItem icon>
                                <Left>
                                    <Button style={{backgroundColor: "#2E9CEB"}}>
                                        <Icon active name="barcode"/>
                                    </Button>
                                </Left>
                                <Body>
                                <Text>Name</Text>
                                </Body>
                                <Right>
                                    <Text>{currentSystem.name}</Text>
                                </Right>
                            </ListItem>
                            }

                            {currentSystem.serial_num &&
                            <ListItem icon>
                                <Left>
                                    <Button style={{backgroundColor: "#2E9CEB"}}>
                                        <Icon active name="barcode"/>
                                    </Button>
                                </Left>
                                <Body>
                                <Text>Serial Number</Text>
                                </Body>
                                <Right>
                                    <Text>{currentSystem.serial_num}</Text>
                                </Right>
                            </ListItem>
                            }

                            <ListItem icon>
                                <Left>
                                    <Button style={{backgroundColor: "#2E9CEB"}}>
                                        <Icon active name="keypad"/>
                                    </Button>
                                </Left>
                                <Body>
                                <Text>IP Address</Text>
                                </Body>
                                <Right>
                                    <Text>{currentSystem.ip_address}</Text>
                                </Right>
                            </ListItem>

                            <ListItem icon>
                                <Left>
                                    <Button style={{backgroundColor: "#2E9CEB"}}>
                                        <Icon active name="key"/>
                                    </Button>
                                </Left>
                                <Body>
                                <Text>Port</Text>
                                </Body>
                                <Right>
                                    <Text>{currentSystem.port}</Text>
                                </Right>
                            </ListItem>

                            {currentSystem.ssid &&
                            <ListItem icon>
                                <Left>
                                    <Button style={{backgroundColor: "#2E9CEB"}}>
                                        <Icon active name="wifi"/>
                                    </Button>
                                </Left>
                                <Body>
                                <Text>SSID</Text>
                                </Body>
                                <Right>
                                    <Text>{currentSystem.ssid}</Text>
                                </Right>
                            </ListItem>
                            }

                            <ListItem icon>
                                <Left>
                                    <Button style={{backgroundColor: "#2E9CEB"}}>
                                        <Icon active name="lock"/>
                                    </Button>
                                </Left>
                                <Body>
                                <Text>State</Text>
                                </Body>
                                <Right>
                                    <Text>{currentSystem.state}</Text>
                                </Right>
                            </ListItem>
                        </List>
                        }

                        <List>
                            <ListItem itemHeader first>
                                <Text style={{fontSize:16,fontWeight:'bold'}}>Device Details</Text>
                            </ListItem>

                            <ListItem icon>
                                <Left>
                                    <Button style={{backgroundColor: "#2E9CEB"}}>
                                        <Icon active name="wifi"/>
                                    </Button>
                                </Left>
                                <Body>
                                <Text>SSID</Text>
                                </Body>
                                <Right>
                                    <Text>{wifiState.deviceSSID}</Text>
                                </Right>
                            </ListItem>

                            <ListItem icon>
                                <Left>
                                    <Button style={{backgroundColor: "#2E9CEB"}}>
                                        <Icon active name="keypad"/>
                                    </Button>
                                </Left>
                                <Body>
                                <Text>IP Address</Text>
                                </Body>
                                <Right>
                                    <Text>{wifiState.deviceIP}</Text>
                                </Right>
                            </ListItem>
                        </List>

                        {!!this.state.activeTags && this.state.activeTags.length &&
                        <List>
                            <ListItem itemHeader last>
                                <Text>Active Tags</Text>
                            </ListItem>

                            {this.state.activeTags.map((tag,index) =>
                            <ListItem icon key={index}>
                                <Left>
                                    <Button style={{backgroundColor: "#6AC893"}}>
                                        <Icon name="checkmark" />
                                    </Button>
                                </Left>
                                <Body>
                                <Text>Tag {tag.name}</Text>
                                </Body>
                            </ListItem>
                            )}
                        </List>
                        }

                        <List>
                            <ListItem itemHeader first>
                                <Text style={{fontSize:16,fontWeight:'bold'}}>XPS FastTrack Details</Text>
                            </ListItem>
                            <ListItem icon>
                                <Left>
                                    <Button style={{backgroundColor: "#2E9CEB"}}>
                                        <Icon active name="git-commit"/>
                                    </Button>
                                </Left>
                                <Body>
                                    <Text>Version</Text>
                                </Body>
                                <Right>
                                    <Text>{global.fasttrackVersion.hash}</Text>
                                </Right>
                            </ListItem>
                        </List>

                        <View style={{width:'100%',flexDirection: 'row',justifyContent:'center',alignItems:'center',padding:40}}>
                            <Button danger style={{}} onPress={this.resetTracker.bind(this)}>
                                <Icon name="exclamation-triangle" type="FontAwesome" style={{color: "#fff", fontSize: 26, lineHeight:26}}/>
                                <Text style={{color:'#fff', lineHeight:26}}>Reset Tracker</Text>
                            </Button>
                        </View>

                    </Content>

                    {!!isWifiConnecting && wifiState.connectionError === '' &&
                        <TopMessageBar
                            navigator={navigator}
                            type='working'
                            message={`Connecting to ${hubName}`}
                        />
                    }
                    {!!isWifiConnecting && wifiState.connectionError !== '' &&
                        <TopMessageBar
                            navigator={navigator}
                            type='error'
                            message={`${hubName} not available (01)`}
                        />
                    }

                    {!isWifiConnecting && !!currentSystem && currentSystem.state==='connecting' &&
                        <TopMessageBar
                            navigator={navigator}
                            type='working'
                            message={`Connecting to ${hubName}`}
                        />
                    }

                    {!isWifiConnecting && !!currentSystem && currentSystem.state==='disconnected' &&
                        <TopMessageBar
                            navigator={navigator}
                            type='error'
                            message={`${hubName} not available (02)`}
                        />
                    }

                    {!isWifiConnecting && !!currentSystem && currentSystem.state==='connected' &&
                        <TopMessageBar
                            navigator={navigator}
                            type='success'
                            message={`Connected to ${hubName}`}
                        />
                    }

                </Container>
            </Root>
        )
    }
}

export default connect(
    mapStateToProps,
    {
        connectSystem,
        disconnectSystem,
        removeTagLiveData,
        setupTag,
        fetchTagsBySystem,
        fetchTagsByOrg,
        startSync,
        fetchSystemsByOrg,
        saveSystemInfo,
        connectToWifiNetwork,
        stopWifiReconnect
    }
)(SystemStatusScreen);
