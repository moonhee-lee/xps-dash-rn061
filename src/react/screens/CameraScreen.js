import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import { CameraKitCameraScreen } from '../../react/components/CameraScreen';
import { connect } from 'react-redux';

import { saveUserImage, fetchUsersByTeam } from '../../redux/actions/user';

export class CameraScreen extends Component {

    constructor(props) {
        super(props);
    }

    onBottomButtonPressed(event) {
        const { athlete, saveUserImage, navigator, fetchUsersByTeam, team, onHandleCamera } = this.props;
        if(event.type==='right' && event.s3Image) {
            saveUserImage(athlete.id,event.s3Image,team.id);
            onHandleCamera(event.localImage);
        }
        if(event.type==='left') {
            Navigation.dismissModal(this.props.componentId);
        }
    }

    render() {
        const { athlete } = this.props;

        return (
            <CameraKitCameraScreen
                actions={{ rightButtonText: 'Done', leftButtonText: 'Cancel' }}
                onBottomButtonPressed={(event) => this.onBottomButtonPressed(event)}
                allowCaptureRetake={true}
                flashImages={{
                    on: require('../../images/flashOn.png'),
                    off: require('../../images/flashOff.png'),
                    auto: require('../../images/flashAuto.png')
                }}
                cameraFlipImage={require('../../images/cameraFlipIcon.png')}
                captureButtonImage={require('../../images/cameraButton.png')}
                cameraOptions={{
                    ratioOverlay:'1:1',
                    ratioOverlayColor: '#00000077'
                }}
            />
        );
    }
}


export default connect(
    null,
    { saveUserImage, fetchUsersByTeam }
)(CameraScreen);




