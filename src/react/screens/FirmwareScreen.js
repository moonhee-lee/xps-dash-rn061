import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import {View, Text, TouchableOpacity, Alert, ScrollView, RefreshControl, ActivityIndicator} from 'react-native';
import {Button, Container, Content, Icon, ListItem} from "native-base";
import TopMessageBar from '../../react/components/TopMessageBar';

import { getCurrentSystem, getFirmware, getCloudState, getWifiState } from '../../redux/reducers';
import { connectSystem, disconnectSystem } from '../../redux/actions/system';
import { fetchFirmware, updateFirmware, saveFirmware } from '../../redux/actions/firmware';
import { fetchCurrentCloudState } from '../../redux/actions/cloud';
import { fetchCurrentWifiState } from '../../redux/actions/wifi';
import { connectToWifiNetwork, disconnectFromWifiNetwork } from '../../redux/actions/wifi';

import styles from '../../theme';
import RNFS from "react-native-fs";
import base64 from 'react-native-base64';

export const mapStateToProps = (state, ownProps) => {
    return {
        currentSystem: getCurrentSystem(state),
        firmware: getFirmware(state),
        cloudState: getCloudState(state),
        wifiState: getWifiState(state),
    }
}

class FirmwareScreen extends Component {
    constructor(props) {
        super(props);
        this.jobId = -1;
        this.connectInterval = null;
        this.state = {filePath:'', output:'', refreshing:false}
    }

    componentDidMount() {
        const { fetchFirmware, fetchCurrentCloudState, fetchCurrentWifiState } = this.props;
        fetchFirmware();
        fetchCurrentCloudState(true);
        fetchCurrentWifiState(true);
        this.connectToSystem();
    }

    componentDidUpdate(prevProps,prevState) {
        const { currentSystem, firmware, cloudState, saveFirmware, wifiState } = this.props;

        if(!_.isEqual(currentSystem,prevProps.currentSystem)) {
            if(currentSystem.firmwareVersion) {
                this.setState({output:'Firmware Updated',updating:false})
            }
        }

        if(!_.isEqual(firmware,prevProps.firmware)) {
            _.each(firmware,(fw) => {
                const url = fw.fw;
                const file = url.substring(url.lastIndexOf('/')+1);
                const filename = `${RNFS.DocumentDirectoryPath}/${file}`;
                this.checkForLocalFile(fw).then(() => {
                    saveFirmware({...fw, filename});
                }).catch((err) => {});
            });
        }
    }

    componentWillUnmount() {
        const { currentSystem, disconnectSystem } = this.props;
        if (currentSystem) {
            if (currentSystem.state === 'connected' || currentSystem.state === 'connecting') disconnectSystem(currentSystem);
        }
        this.stopDownload();
    }

    connectToSystem() {
        const { currentSystem, connectSystem } = this.props;
        if(currentSystem) {
            connectSystem(currentSystem, true);
        }
    }

    sendFirmwareToHub(filename) {
        this.loadFirmware(filename);
        this.connectInterval = setInterval(() => {
            this.checkHubConnected();
        },1000);
    }

    checkHubConnected = () => {
        const { currentSystem } = this.props;
        if(currentSystem.state==='connected') clearInterval(this.connectInterval);
        else this.connectToSystem();
    }

    _onPressFirmware(update) {
        const { currentSystem } = this.props;
        if(currentSystem && currentSystem.state==='connected') {
            Alert.alert(
                'Update XPS Tracker Firmware',
                'Are you sure you want to install ' + update.name + ' on ' + currentSystem.name + '?',
                [
                    {
                        text: 'Yes', onPress: () => {
                            this.sendFirmwareToHub(update.filename);
                        }
                    },
                    {
                        text: 'No', onPress: () => {

                        }
                    }
                ],
                {cancelable: true},
            );
        } else {
            Alert.alert(
                'Unable to connect',
                'Unable to connect to ' + currentSystem.name + ", please make sure you're connected to the hub",
                [
                    {
                        text: 'Okay', onPress: () => {

                        }
                    }
                ],
                {cancelable: true},
            );
        }
    }

    downloadFirmware = (firmware) => {
        const url = firmware.fw;
        const file = url.substring(url.lastIndexOf('/')+1);
        const filename = `${RNFS.DocumentDirectoryPath}/${file}`;

        this.checkForLocalFile(firmware).then((filename) => {
            saveFirmware({...firmware,filename});
        }).catch((err) => {
            const { saveFirmware } = this.props;
            if (this.jobId !== -1) {
                this.setState({ output: 'A download is already in progress' });
            }

            const progress = data => {
                const percentage = ((100 * data.bytesWritten) / data.contentLength) | 0;
                const text = `Progress ${percentage}%`;
                this.setState({ output: text, percentage });
            };

            const begin = res => {
                this.setState({ downloading: true, output: 'Download has begun' });
            };

            const progressDivider = 1;

            this.setState({ filePath: { uri: '' } });

            const ret = RNFS.downloadFile({ fromUrl: url, toFile: filename, begin, progress, progressDivider });

            this.jobId = ret.jobId;

            ret.promise.then(res => {
                this.setState({ downloading: false, output: JSON.stringify(res), filePath: { uri: 'file://' + filename } });
                this.jobId = -1;
                saveFirmware({...firmware,filename});
            }).catch(err => {
                this.jobId = -1;
            });
        });
    }

    loadFirmware = (filename) => {
        this.setState({ output: 'Updating Firmware' });
        RNFS.readFile('file://'+filename,'base64').then( (res) => {
            const { updateFirmware } = this.props;
            //const decoded = base64.decode(res);
            updateFirmware(res).then(() => {
                this.setState({output:'Update Requested',updating:true});
            }).catch((err) => {
                this.setState({output:err,updating:false});
            });
        }).catch((err) => {
            console.log('DownloadFile Error', err);
            return false;
        });
    }

    unlinkLocalFile = (firmware) => {
        const url = firmware.fw;
        const file = url.substring(url.lastIndexOf('/')+1);
        const filename = `${RNFS.DocumentDirectoryPath}/${file}`;
        RNFS.unlink('file://'+filename);
    }

    checkForLocalFile = (firmware) => {
        const url = firmware.fw;
        const file = url.substring(url.lastIndexOf('/')+1);
        const filename = `${RNFS.DocumentDirectoryPath}/${file}`;
        console.log('checking for local file: '+file);
        return RNFS.read('file://'+filename,10,0,'base64');
    }

    stopDownload = () => {
        if (this.jobId !== -1) {
            RNFS.stopDownload(this.jobId);
        }
        this.setState({ downloading: false });
    }

    stopUpdate = () => {
        this.setState({ updating: false });
    }

    disconnectSystem = () => {
        const { disconnectFromWifiNetwork, currentSystem } = this.props;
        disconnectFromWifiNetwork(currentSystem.ssid).then((d) => {}).catch((e) => {});
    }

    connectToWifi  = () => {
        const { connectToWifiNetwork, currentSystem } = this.props;
        connectToWifiNetwork(currentSystem.ssid, currentSystem.password).then((d) => {}).catch((e) => {});
    }

    render() {
        const { currentSystem, firmware, cloudState, wifiState } = this.props;
        const { output, downloading, percentage, updating } = this.state;

        const availableFirmware = _.filter(firmware,(fw) => !!fw.filename);
        const cloudFirmware = _.filter(firmware,(fw) => !fw.filename);

        return (
            <Container>
                <Content style={[styles.contentStyle,(currentSystem && (currentSystem.state==='connected' || currentSystem.state==='disconnected' || currentSystem.state==='connecting'))?{marginTop:40}:{}]}>
                    {!!currentSystem && currentSystem.state==='connected' &&
                    <View>
                        {!!availableFirmware && availableFirmware.map((fw, index) =>
                            <TouchableOpacity onPress={() => this._onPressFirmware(fw)} style={styles.sessionList} key={index}>
                                <Text style={{fontSize:16}}>Install {fw.name}</Text>
                                <Text style={{fontSize:16,color:'#666'}}>Feb 14, 2019</Text>
                            </TouchableOpacity>
                        )}
                    </View>
                    }
                    {!currentSystem || currentSystem.state!=='connected' &&
                    <View>
                        {!!availableFirmware && availableFirmware.map((fw, index) =>
                            <TouchableOpacity onPress={() => this._onPressFirmware(fw)} style={styles.sessionList} key={index}>
                                <Text style={{fontSize:16}}>Install {fw.name}</Text>
                                <Text style={{fontSize:16,color:'#666'}}>Feb 14, 2019</Text>
                            </TouchableOpacity>
                        )}
                    </View>
                    }
                    {cloudState && cloudState.isConnected &&
                    <View>
                        {!!cloudFirmware && cloudFirmware.map((fw, index) =>
                            <TouchableOpacity onPress={() => this.downloadFirmware(fw)} style={styles.sessionList} key={index}>
                                <Text style={{fontSize:16}}>Download {fw.name}</Text>
                                <Text style={{fontSize:16,color:'#666'}}>Feb 14, 2019</Text>
                            </TouchableOpacity>
                        )}
                    </View>
                    }
                    {cloudState && cloudState.isConnected &&
                    <Button bordered block onPress={() => this.connectToWifi()} style={styles.buttonStyle}>
                        <Text>Connect to Hub Wifi</Text>
                    </Button>
                    }
                    {!!wifiState.isConnected && (!currentSystem || currentSystem.state!=='connected') &&
                    <Button bordered block onPress={() => this.connectToSystem()} style={styles.buttonStyle}>
                        <Text>Connect to Hub</Text>
                    </Button>
                    }
                    {!!currentSystem && currentSystem.state === 'connected' &&
                    <Button bordered block onPress={() => this.disconnectSystem()} style={styles.buttonStyle}>
                        <Text>Disconnect from Hub</Text>
                    </Button>
                    }
                </Content>

                {!currentSystem || (!!currentSystem && currentSystem.state==='disconnected' && currentSystem.name) &&
                <TopMessageBar
                    navigator={navigator}
                    type='error'
                    message={"Not connected to "+currentSystem.name}
                    onPressReload={() => this.connectToSystem()}
                />
                }

                {!!currentSystem && currentSystem.state==='connecting' && currentSystem.name &&
                <TopMessageBar
                    navigator={navigator}
                    type='working'
                    message={"Connecting to "+currentSystem.name}
                />
                }

                {!!currentSystem && currentSystem.state==='connected' && currentSystem.name &&
                <TopMessageBar
                    navigator={navigator}
                    type='success'
                    message={"Connected to "+currentSystem.name}
                />
                }
                {!!downloading &&
                <View style={[{paddingTop: '50%',padding:40,backgroundColor:'rgba(0,0,0,.6)'},styles.loading]}>
                    <ActivityIndicator size='large' color="#ffffff"/>
                    <Text style={{padding:20,color:'#ffffff'}}>Downloading Firmware</Text>
                    <Button style={{alignSelf: 'center', padding: 20, marginTop: 20}} light iconLeft
                            onPress={() => this.stopDownload()}>
                        <Text style={{}}>Cancel</Text>
                    </Button>
                </View>
                }
                {!!updating &&
                <View style={[{paddingTop: '50%',padding:40,backgroundColor:'rgba(0,0,0,.6)'},styles.loading]}>
                    <ActivityIndicator size='large' color="#ffffff"/>
                    <Text style={{padding:20,color:'#ffffff'}}>Updating Firmware</Text>
                    <Button style={{alignSelf: 'center', padding: 20, marginTop: 20}} light iconLeft
                            onPress={() => this.stopUpdate()}>
                        <Text style={{}}>Cancel</Text>
                    </Button>
                </View>
                }
            </Container>
        )
    }
}

export default connect(
    mapStateToProps,
    {
        connectSystem,
        disconnectSystem,
        updateFirmware,
        fetchFirmware,
        fetchCurrentCloudState,
        saveFirmware,
        fetchCurrentWifiState,
        connectToWifiNetwork,
        disconnectFromWifiNetwork
    }
)(FirmwareScreen);
