import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import { connect } from 'react-redux';
import _ from 'lodash';

import {Dimensions, Easing, TouchableOpacity, View} from 'react-native';
import { Button, Container, Content, Text } from 'native-base';

import * as formatUtils from '../../lib/format_utils';
import * as activityUtils from '../../lib/activity_utils';
import * as graphUtils from '../../lib/graph_utils';

const { width,height } = Dimensions.get('window');

import { getCurrentOrg, getSessionsIsFetching, getActiveSession, getLiveActivity } from '../../redux/reducers';
import ActivityReviewPlot from '../../react/components/ActivityReviewPlot';
import ActivityRecordGraph from '../../react/components/ActivityRecordGraph';
import ActivityResultUserListItem from '../../react/components/ActivityResultUserListItem';
import ActivityRecordDetailLists from '../../react/components/ActivityRecordDetailLists';
import ActivityRecordUserListItem from '../../react/components/ActivityRecordUserListItem';
import ActivityRecordSummary from '../../react/components/ActivityRecordSummary';

import { resetRecording } from '../../redux/actions/activity';
import { saveLiveActivity } from '../../redux/actions/live_activity';
import { sessionsSelector } from '../../redux/selectors/sessions';
import { fetchSessionsByOrg } from '../../redux/actions/session';
import { getTimeRangedMessages } from '../../redux/local_storage';

import SessionSelectBar from '../../react/components/SessionSelectBar';
import SaveDiscardBar from '../../react/components/SaveDiscardBar';
import styles from '../../theme';

import { calculateDistance } from '../../redux/actions/activity';

export const mapStateToProps = (state, ownProps) => {
    return {
        currentOrg: getCurrentOrg(state),
        sessions: sessionsSelector(state),
        activeSession: getActiveSession(state),
        liveActivity: getLiveActivity(state)
    }
}

export class LiveActivityRecordSaveScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            graphData: null,
            listData: [],
            summaryData: null,
            activeUser: null,
            codData: null
        };
        this.discardResult = this.discardResult.bind(this);
        Navigation.events().bindComponent(this);
    }

    componentDidMount() {
        const { currentOrg, fetchSessionsByOrg, sessions, liveActivity, activityUsers } = this.props;
        if (sessions.length === 0) fetchSessionsByOrg(currentOrg.id);

        if (activityUsers.length === 1) {
            const activeUser = activityUsers[0];

            const toGraphESTData = getTimeRangedMessages(activeUser, false);
            let { summaryData, listData } = activityUtils.getSummaryData(liveActivity, activeUser.meta_data, activeUser.events);

            const isManualHorizontalJump = (global.enableManualHorizontalJump && liveActivity && liveActivity.activity_type==='jump' && liveActivity.orientation==='horizontal');
            if (isManualHorizontalJump) {
                const length = calculateDistance(activeUser);
                summaryData.primary = {
                    label: 'Distance',
                    value: formatUtils.formatDistance(length)
                }
                summaryData.secondary = {
                    label: '',
                    value: formatUtils.formatDistanceImperial(length)
                }
            }

            const graphData = formatUtils.loadGraphData(toGraphESTData, activeUser, liveActivity);
            const codData = graphUtils.generateCoDDistances(activeUser.meta_data);

            this.setState({summaryData, listData, graphData, codData});

        }
    }

    navigationButtonPressed() {
        this.discardResult();
    }

    discardResult() {
        const { resetRecording, liveActivity, onPressDiscard } = this.props;
        resetRecording(liveActivity.id);
        onPressDiscard();
    }

    loadUserActivityRecord(activeUser) {
        const { liveActivity } = this.props;
        Navigation.push(this.props.componentId, {
            component: {
                name: 'LiveActivityRecordDetailScreen',
                passProps: {
                    activeUser,
                    liveActivity
                },
                options: {
                    topBar: {
                        title: {
                            text: 'Activity Detail'
                        }
                    }
                }
            }
        });
    }

    render() {
        const {
            activityUsers, liveActivity, beepStartTime, navigator, currentOrg,
            sessions, activeSession, onPressSave
        } = this.props;

        const { summaryData, listData, codData, graphData } = this.state

        if(!activityUsers || _.size(activityUsers)===0) return (<View></View>);
        const activeUser = activityUsers[0]

        const colors = ['#FF8203','#f0f','#f00', '#0b1', '#060', '#00f'];
        const graphHeight = height*.3;
        const rowHeight = height*.1;

        return (
            <Container>
                <Content style={[styles.contentStyle]}>
                    <View>

                        {_.size(activityUsers) > 1 &&
                        <View>
                            <ActivityReviewPlot
                                activityUsers={activityUsers}
                                currentActivity={liveActivity}
                                colors={colors}
                            />
                            {activityUsers.map( (activeUser,index) =>
                                <View key={index}>
                                    <TouchableOpacity
                                        key={index} onPress={() => this.loadUserActivityRecord(activeUser)}>
                                        <ActivityResultUserListItem
                                            activity={liveActivity}
                                            color={colors[index]}
                                            navigator={navigator}
                                            item={{
                                                meta: activeUser.meta_data,
                                                user: activeUser.user,
                                                activity: activeUser,
                                                beepStartTime: activeUser.beepStartTime
                                            }}
                                        />
                                    </TouchableOpacity>
                                </View>
                            )}
                        </View>
                        }
                        {_.size(activityUsers)===1 &&
                            <View style={{flex:1}}>

                                <View style={{height:rowHeight}}>
                                    <ActivityRecordUserListItem
                                        item={liveActivity}
                                        user={activeUser}
                                        activity={liveActivity}
                                        activityIndex={0}
                                        navigator={navigator}
                                    />
                                </View>

                                <View style={{flex:1}}>
                                    <ActivityRecordSummary
                                        summaryData={summaryData}
                                    />
                                </View>

                                <View style={{height:graphHeight}}>
                                    <ActivityRecordGraph
                                        itemData={graphData}
                                        codData={codData}
                                        height={graphHeight}
                                    />
                                </View>
                                <View style={{flex:1}}>
                                    <ActivityRecordDetailLists
                                        listData={listData}
                                        activity={liveActivity}
                                        activityRecord={liveActivity}
                                        itemWidth={width}
                                    />
                                </View>

                            </View>
                        }
                    </View>
                </Content>
                <SaveDiscardBar
                    onPressSave={onPressSave}
                    onPressDiscard={this.discardResult}
                />
            </Container>
        )
    }
}


export default connect(
    mapStateToProps,
    { resetRecording, saveLiveActivity, fetchSessionsByOrg }
)(LiveActivityRecordSaveScreen);
