import React, { Component } from 'react';
import { connect } from 'react-redux';

import { View, Text } from 'react-native';
import {Container, Content} from "native-base";
import BottomTabBar from '../../react/components/BottomTabBar';
import TeamListItem from '../../react/components/TeamListItem'

import { fetchTeamsByOrg, setCurrentTeam } from '../../redux/actions/team'
import { getCurrentOrg, getTeamsByCurrentOrg, getCurrentTeam } from '../../redux/reducers';

import styles from '../../theme';

export const mapStateToProps = (state, ownProps) => {
    return {
        teams: getTeamsByCurrentOrg(state),
        currentOrg: getCurrentOrg(state),
        currentTeam: getCurrentTeam(state)
    }
}

class TeamsScreen extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const { currentOrg, fetchTeamsByOrg, teams } = this.props;
        if(!teams || teams.length===0) fetchTeamsByOrg(currentOrg.id);
    }

    _onPressTeam(team) {
        const { setCurrentTeam } = this.props;
        console.log('setting team from selection: '+team.id);
        setCurrentTeam(team.id);
    }

    render() {
        const { navigator, teams, currentTeam } = this.props;

        return (
            <Container>
                <Content style={styles.contentStyle}>
                    <View>
                        {!!teams && teams.map((team,index) =>
                        <TeamListItem
                            team={team}
                            key={index}
                            index={index}
                            currentTeam={currentTeam}
                            onPressTeam={() => this._onPressTeam(team)}
                        >
                        </TeamListItem>
                        )}
                    </View>
                </Content>
                <BottomTabBar
                    navigator={navigator}
                />
            </Container>
        )
    }
}

export default connect(
    mapStateToProps,
    { fetchTeamsByOrg, setCurrentTeam }
)(TeamsScreen);
