import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import { connect } from 'react-redux';

import {View, Text, ActivityIndicator} from 'react-native';
import {Container, Content, Icon, Button} from "native-base";

import { getCurrentSystem, getWifiState, getLocalDataTotal, getCloudState, getLocalDataCounts } from '../../redux/reducers';
import { disconnectFromWifiNetwork } from '../../redux/actions/wifi';
import { fetchCurrentCloudState, getCurrentCloudState } from '../../redux/actions/cloud';
import { fetchCurrentWifiState } from '../../redux/actions/wifi';
import { doLocalSync } from '../../lib/local_storage';

import styles from '../../theme';

export const mapStateToProps = (state, ownProps) => {
    return {
        currentSystem: getCurrentSystem(state),
        wifiState: getWifiState(state),
        cloudState: getCloudState(state),
        localDataTotal: getLocalDataTotal(state),
        localDataCounts: getLocalDataCounts(state)
    }
}

class SyncScreen extends Component {
    constructor(props) {
        super(props);
        this.interval = null;
        this.counter = 0;
        this.state = {
            error: null,
            disconnectingWifi: false,
            isSaving: false,
            isCloudConnected: false,
            isCloudConnecting: false,
            isHubWifiConnected: false,
            cloudConnectRetries: 0,
            cloudConnectFailed: false,
            saveFailed: false,
            wifiConnectError: false
        };
        Navigation.events().bindComponent(this);
    }

    componentDidMount() {
        const { cloudState, currentSystem, wifiState } = this.props;

        // set some state variables on mount
        if (cloudState) this.setState({isCloudConnected: cloudState.isConnected});

        if (currentSystem && wifiState && wifiState.isConnected) {
            if (wifiState.deviceSSID === currentSystem.ssid) {
                // we are connected to the hub
                this.setState({ isHubWifiConnected: true });
            }
        }
    }

    componentDidUpdate(prevProps, prevState) {
        const { cloudState, wifiState, doLocalSync } = this.props;
        const { disconnectingWifi, isCloudConnecting, isSaving } = this.state;

        if (disconnectingWifi) {
            // since we triggered a wifi disconnect, we are waiting for either
            // wifiState isConnected to change  OR
            // wifi SSID to change.
            if (wifiState.isConnected !== prevProps.wifiState.isConnected || wifiState.deviceSSID !== prevProps.wifiState.deviceSSID) {
                // once we're in here, we're not on Hub wifi anymore.
                this.setState({ disconnectingWifi: false }, () => {
                    // try to connect to the cloud
                    this.connectCloud();
                });
            }
        }

        if (isCloudConnecting) {
            // if we are attempting to connect to the cloud, we are checking for the isConnected flag to change.
            if (cloudState && cloudState.isConnected === true) {
                clearInterval(this.cloudInterval);
                this.setState({ isCloudConnected: true, isCloudConnecting: false, error: null, isSaving: true });
                doLocalSync();
            }
        }

        if (isSaving && cloudState) {
            if (cloudState.isSaving === false) {
                this.setState({isSaving: false});
            }
            if (cloudState.saveFailed === true) {
                this.setState({saveFailed: true, isSaving: false});
            }
        }
    }

    componentWillUnmount() {
        clearInterval(this.cloudInterval);
    }

    navigationButtonPressed() {
        Navigation.dismissModal(this.props.componentId);
    }

    closeModal = () => {
        Navigation.dismissModal(this.props.componentId);
    }

    connectCloud = () => {
        const { fetchCurrentCloudState } = this.props;

        this.setState(
            {isCloudConnecting: true, cloudConnectRetries: 0},

            () => {
                this.cloudInterval = setInterval(() => {
                    let { cloudConnectRetries } = this.state;

                    cloudConnectRetries++;
                    if (cloudConnectRetries <= 5) {
                        this.setState({ cloudConnectRetries: cloudConnectRetries });
                        fetchCurrentCloudState(true);
                    } else {
                        clearInterval(this.cloudInterval);
                        this.setState({ isCloudConnecting: false, cloudConnectFailed: true });
                    }
                }, 1000);
            }
        );
    }

    uploadData = () => {
        const { doLocalSync } = this.props;
        const { isCloudConnected, isHubWifiConnected } = this.state;

        // we only disconnect from wifi if we are about to upload.
        if (isHubWifiConnected) {
            this.setState(
                { disconnectingWifi: true },
                () => {
                    const { disconnectFromWifiNetwork, currentSystem } = this.props;
                    disconnectFromWifiNetwork(currentSystem.ssid);
                }
            );
            // we'll detect the disconnection in componentDidUpdate and start cloud connection then.
        } else {
            // otherwise, we check if we are ready for the sync.
            if (isCloudConnected) {
                this.setState({error: null, isSaving: true});
                doLocalSync();
            } else {
                this.connectCloud();
            }
        }
    }

    render() {
        const { localDataTotal, localDataCounts, cloudState } = this.props;
        const { error, disconnectingWifi, isSaving, isCloudConnected, isCloudConnecting, wifiConnectError, cloudConnectFailed } = this.state;
        const { countUsers, countSessions, countActivities } = localDataCounts;

        let type = 'Activities';
        if(countSessions) type = 'Sessions';
        if(countUsers) type = 'Users';

        return (
            <Container style={{backgroundColor:'#6AC893'}}>
                <Content style={{flex:1}}>
                    {!!localDataTotal && localDataTotal > 0 &&
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: 'center',
                            alignContent: 'center',
                            paddingTop: '50%'
                        }}>
                            {countUsers > 0 &&
                                <Text style={{
                                    alignSelf: 'center',
                                    fontSize: 20,
                                    padding: 0,
                                    color: '#fff',
                                    textAlign: 'center'
                                }}>{countUsers} User{countUsers > 1 ? 's' : ''} to upload to the Cloud</Text>
                            }
                            {countSessions > 0 &&
                                <Text style={{
                                    alignSelf: 'center',
                                    fontSize: 20,
                                    padding: 0,
                                    color: '#fff',
                                    textAlign: 'center'
                                }}>{countSessions} Session{countSessions > 1 ? 's' : ''} to upload to the Cloud</Text>
                            }
                            {countActivities > 0 &&
                                <Text style={{
                                    alignSelf: 'center',
                                    fontSize: 20,
                                    padding: 0,
                                    color: '#fff',
                                    textAlign: 'center'
                                }}>{countActivities} Activit{countActivities > 1 ? 'ies' : 'y'} to upload to the
                                    Cloud</Text>
                            }

                            {!!disconnectingWifi &&
                                <Button style={{alignSelf: 'center', padding: 20, marginTop: 20}} light iconLeft>
                                    <Icon type="FontAwesome" name="wifi" style={{paddingRight: 10, color: "#333"}}/>
                                    <Text style={{paddingRight: 10}}>Disconnecting from Hub Wifi...</Text>
                                </Button>
                            }

                            {!disconnectingWifi && !isCloudConnecting && !isSaving && !cloudConnectFailed &&
                                <Button style={{alignSelf: 'center', padding: 20, marginTop: 20}} light iconLeft
                                        onPress={this.uploadData}>
                                    <Icon type="FontAwesome" name="cloud-upload" style={{paddingRight: 10, color: "#333"}}/>
                                    <Text style={{paddingRight: 10}}>Upload to Cloud</Text>
                                </Button>
                            }

                            {!!isCloudConnecting &&
                                <Button style={{alignSelf: 'center', padding: 20, marginTop: 20}} light iconLeft
                                        onPress={this.uploadData}>
                                    <Icon type="FontAwesome" name="cloud-upload" style={{paddingRight: 10, color: "#333"}}/>
                                    <Text style={{paddingRight: 10}}>Connecting to Cloud Server...</Text>
                                </Button>
                            }

                            {!disconnectingWifi && !!isSaving &&
                                <Button style={{alignSelf: 'center', padding: 20, marginTop: 20}} light iconLeft>
                                    <Icon type="FontAwesome" name="cloud-upload" style={{paddingRight: 10, color: "#333"}}/>
                                    <Text style={{paddingRight: 10}}>Uploading {type}</Text>
                                </Button>
                            }

                            {!!error && error !== '' &&
                                <Text style={{
                                    alignSelf: 'center',
                                    fontSize: 20,
                                    padding: 20,
                                    color: '#fff',
                                    textAlign: 'center'
                                }}>{error}</Text>
                            }
                            {!!cloudConnectFailed &&
                                <Text style={{
                                    alignSelf: 'center',
                                    fontSize: 20,
                                    padding: 20,
                                    color: '#fff',
                                    textAlign: 'center'
                                }}>Unable to connect to the Cloud.</Text>
                            }
                        </View>
                    }
                    {(!localDataTotal || localDataTotal===0) &&
                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignContent: 'center',
                        paddingTop: '50%'
                    }}>
                        <Text style={{
                            alignSelf: 'center',
                            fontSize: 20,
                            padding: 20,
                            color: '#fff',
                            textAlign: 'center'
                        }}>All items have been uploaded.</Text>
                        <Button style={{alignSelf: 'center', padding: 20,}} light onPress={this.closeModal}>
                            <Text style={{paddingRight: 10, paddingLeft: 10}}>Close</Text>
                        </Button>
                    </View>
                    }

                    {isSaving &&
                    <View style={styles.loading}>
                        <ActivityIndicator size='large' color="#ffffff"/>
                    </View>
                    }
                </Content>
            </Container>
        )
    }
}

export default connect(
    mapStateToProps,
    {
        disconnectFromWifiNetwork,
        doLocalSync,
        fetchCurrentCloudState,
        fetchCurrentWifiState,
        getCurrentCloudState
    }
)(SyncScreen);
