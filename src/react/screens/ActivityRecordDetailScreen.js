import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import { Dimensions, View } from 'react-native';
import { Container, Content, Spinner, Text } from 'native-base';

import { getActivityRecords, getActivityDataIsFetching, getActivityDataById, getUserById } from '../../redux/reducers';
import { activityRecordSelector } from '../../redux/selectors/activity_records';
import { fetchActivityDataById, fetchActivityDataByRecord } from '../../redux/actions/activity_data';
import ActivityRecordGraph from '../../react/components/ActivityRecordGraph';
import ActivityRecordDetailLists from '../../react/components/ActivityRecordDetailLists';
import ActivityRecordSummary from '../../react/components/ActivityRecordSummary'
// import ActivityRecordDetailLists from '../../react/components/ActivityDetailSummary';
import ActivityRecordUserListItem from '../../react/components/ActivityRecordUserListItem';
import styles from '../../theme';

import * as formatUtils from '../../lib/format_utils';
import * as activityUtils from '../../lib/activity_utils';
import * as graphUtils from '../../lib/graph_utils';

const { width,height } = Dimensions.get('window');


export const mapStateToProps = (state, ownProps) => {
    return {
        activityRecordData: getActivityDataById(state, ownProps.activityRecord.id),
        isFetching: getActivityDataIsFetching(state),
        recordUser: getUserById(state,ownProps.activityRecord.user.id)
    }
}

export class ActivityRecordDetailScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            graphData: null,
            listData: []
        };
    }

    componentDidMount() {
        const { activityRecord, activityRecordData, fetchActivityDataById, fetchActivityDataByRecord } = this.props;
        if (!activityRecordData || activityRecordData.length===0) fetchActivityDataByRecord(activityRecord);
    }

    componentDidUpdate(prevProps,prevState) {
        const { activityRecord, activityRecordData } = this.props;
        const { graphData, listData } = this.state;

        if(activityRecordData!==prevProps.activityRecordData && ((activityRecordData[0] && activityRecordData[0].est) || activityRecordData.est)) {
            const codData = graphUtils.generateCoDDistances(activityRecord.data_summary);
            this.setState({codData});
        }
    }

    loadGraphData() {
        const { activityRecordData, activityRecord } = this.props;
        const { data_summary } = activityRecord;
        const { est, events } = activityRecordData;

        let graph = [];

        const graphWidth = width;
        const graphHeight = height;

        const activity_type = activityRecord.type_definition.activity_type;

        if(est && _.size(est)) {
            const graphData = graphUtils.getLineGraphData({
                data: _.values(est),
                events,
                meta: data_summary,
                width: graphWidth,
                height: graphHeight,
                activityType: activity_type,
            });

            if (activity_type === 'jump') {
                graph = graphData.distance;
            }
            if (activity_type !== 'jump') {
                graph = graphData.velocity;
            }

            return graph;
        }
    }

    render() {
        const { activityRecordData, activityRecord, navigator, user, recordUser, isFetching } = this.props;

        const _user = user?user:recordUser;

        if (isFetching || !activityRecordData) {
            return (
                <Container style={{ alignSelf: "center" }}>
                    <Spinner />
                </Container>
            )
        }

        const { summaryData, listData} = activityUtils.getSummaryData(activityRecord.type_definition, activityRecord.data_summary, activityRecord.data_summary.events);
        const graphData = this.loadGraphData();

        let codData = graphUtils.generateCoDDistances(activityRecord.data_summary);
        if(_.isEmpty(codData) && activityRecordData.events) codData = formatUtils.generateCoDDistances(activityRecord.data_summary,activityRecordData.events);

        const graphHeight = height*.3;
        const rowHeight = height*.1;
        const listHeight = height*.5;

        return (
            <Content>
                {!!activityRecord &&
                <View style={{flex:1}}>

                    <View>
                        <ActivityRecordUserListItem
                            item={activityRecord}
                            user={_user}
                            activity={activityRecord}
                            activityIndex={0}
                            navigator={navigator}
                        />
                    </View>

                    <View>
                        <ActivityRecordSummary
                                summaryData={summaryData}
                            />
                    </View>

                    <View style={{height:graphHeight}}>
                        <ActivityRecordGraph
                            itemData={graphData}
                            codData={codData}
                            height={graphHeight}
                        />
                    </View>
                    <View style={{flex:1}}>
                        <ActivityRecordDetailLists
                            listData={listData}
                            activity={activityRecord}
                            activityRecord={activityRecord}
                            itemWidth={width}
                        />
                    </View>

                </View>
                }
            </Content>
        )
    }
}

export default connect(
    mapStateToProps,
    { fetchActivityDataById, fetchActivityDataByRecord }
)(ActivityRecordDetailScreen);
