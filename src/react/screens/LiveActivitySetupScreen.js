import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import { connect } from 'react-redux';
import _ from 'lodash';
import validate from 'validate.js'
import { TouchableOpacity, Dimensions, Alert } from "react-native";
import { Input, Slider } from 'react-native-elements';
import { saveLiveActivity } from '../../redux/actions/live_activity';
import CustomIcon from '../../theme/components/XPSIcon';
import moment from 'moment';
import Carousel, { Pagination } from 'react-native-snap-carousel';

import { ButtonGroup, CheckBox, Divider } from 'react-native-elements';
import DoneBar from '../../react/components/DoneBar';

import {
    Container,
    Content,
    Item,
    Text, View, Icon, Button, ActionSheet
} from 'native-base';

import { localStorage } from '../../lib/local_storage';
import { testStartSound } from '../../redux/actions/system';
import { clearActiveBeepStartTime } from '../../redux/actions/active_user';

import styles from '../../theme';

const dimensionWindow = Dimensions.get('window');
const width = Math.round(dimensionWindow.width);
const height = width*.6;

const CoDDefaults = {end:0.9,mid:2.0,thresh:30};

const startSounds = [
    {id:1,file:"swimStartBeep",type:"m4a",name:"Swim Start"},
    {id:2,file:"buzzer",type:"m4a",name:"Buzzer"},
    {id:3,file:"basketballBuzzer",type:"m4a",name:"Basketball Buzzer"},
    {id:4,file:"pistol",type:"m4a",name:"Starting Pistol"},
    {id:5,file:"bikeHorn",type:"m4a",name:"Bike Horn"},
    {id:6,file:"ahooga",type:"m4a",name:"Ahooga"},
    {id:7,file:"carnHorn",type:"m4a",name:"Car Horn"},
    {id:8,file:"trainHorn",type:"m4a",name:"Train Horn"},
    {id:9,file:"airHorn",type:"m4a",name:"AirHorn"},
];

export class LiveActivitySetupScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {currentSound:global.beepSound?global.beepSound:startSounds[0]}
        Navigation.events().bindComponent(this);
    }

    componentDidMount() {
        const { liveActivity, session } = this.props;
        if (liveActivity && liveActivity.activity_type) {
            let activity_index;
            switch(liveActivity.activity_type) {
                case 'freeform':
                    activity_index = 3;
                    break;
                case 'agility':
                    activity_index = 2;
                    break;
                case 'jump':
                    activity_index = 1;
                    break;
                default:
                    activity_index = 0;
                    break;
            }
            setTimeout(() => {this.setActivityType(liveActivity.activity_type,activity_index)},500);

            this.setState({
                activity_id: 'live_activity',
                activity_type: liveActivity.activity_type,
                activity_typeError: '',
                display_name: liveActivity.display_name,
                distance: liveActivity.distance,
                distanceError: '',
                measurementTypeIndex: (liveActivity.units && liveActivity.units === 'yards') ? 1 : 0,
                measurementTypeError: '',
                startTypeIndex: liveActivity.start_type === 'motion' ? 0 : liveActivity.start_type === 'beep' ? 1 : 2,
                startTypeError: '',
                orientationIndex: liveActivity.orientation === 'vertical' ? 0 : 1,
                orientationError: '',
                formError: false,
                currentIndex:activity_index,
                sliderValue:liveActivity.distance,
                flySliderValue:liveActivity.leadInDistance>1?liveActivity.leadInDistance:5,
                leadInDistance:liveActivity.leadInDistance>1?liveActivity.leadInDistance:5,
                endpointDistance: liveActivity.endpointDistance?liveActivity.endpointDistance:CoDDefaults.end,
                midpointDistance: liveActivity.midpointDistance?liveActivity.midpointDistance:CoDDefaults.mid,
                headingThreshold: liveActivity.headingThreshold?liveActivity.headingThreshold:CoDDefaults.thresh,
                singleLeg: liveActivity.singleLeg?liveActivity.singleLeg:this.defaultSingleLegState(),
                skatingMode: liveActivity.skatingMode?liveActivity.skatingMode:this.defaultSkatingState(),
                bobsledMode: liveActivity.bobsledMode?liveActivity.bobsledMode:this.defaultBobsledState(),
                walkingMode: liveActivity.walkingMode?liveActivity.walkingMode:this.defaultWalkingState()
            });
        } else {
            this.setState({
                activity_id: 'live_activity',
                activity_type: 'sprint',
                activity_typeError: '',
                display_name: '',
                distance: 10,
                distanceError: '',
                measurementTypeIndex: 0,
                measurementTypeError: '',
                startTypeIndex: 0,
                startTypeError: '',
                orientationIndex: 0,
                orientationError: '',
                formError: false,
                currentIndex:0,
                sliderValue:10,
                leadInDistance:5,
                flySliderValue:5,
                endpointDistance: CoDDefaults.end,
                midpointDistance: CoDDefaults.mid,
                headingThreshold: CoDDefaults.thresh,
                singleLeg: this.defaultSingleLegState(),
                skatingMode: this.defaultSkatingState(),
                bobsledMode: this.defaultBobsledState(),
                walkingMode: this.defaultWalkingState()
            });
        }
    }

    componentDidUpdate(prevProps,prevState) {
        const { activity_type, distance, measurementTypeIndex, startTypeIndex, orientationIndex, currentIndex, leadInDistance, singleLeg, skatingMode, bobsledMode, walkingMode } = this.state;

        if(activity_type!==prevState.activity_type) this.saveLiveActivityData();
        if(distance!==prevState.distance) this.saveLiveActivityData();
        if(leadInDistance!==prevState.leadInDistance) this.saveLiveActivityData();
        if(measurementTypeIndex!==prevState.measurementTypeIndex) this.saveLiveActivityData();
        if(startTypeIndex!==prevState.startTypeIndex) this.saveLiveActivityData();
        if(orientationIndex!==prevState.orientationIndex) this.saveLiveActivityData();
        if(!_.isEqual(singleLeg, prevState.singleLeg)) this.saveLiveActivityData();
        if(!_.isEqual(skatingMode, prevState.skatingMode)) this.saveLiveActivityData();
        if(!_.isEqual(bobsledMode, prevState.bobsledMode)) this.saveLiveActivityData();
        if(!_.isEqual(walkingMode, prevState.walkingMode)) this.saveLiveActivityData();
    }

    navigationButtonPressed() {
        Navigation.pop(this.props.componentId);
    }

    defaultSingleLegState = () => {
        return {
            sprint: {
                checked: false,
                leftFooted: false
            },
            jump: {
                checked: false,
                leftFooted: false,
                isTriple: false,
                tripleJumpTypeIndex: 0
            }
        }
    }

    defaultSkatingState = () => {
        return {
            checked: false,
            directionIndex: 0
        }
    };

    defaultBobsledState = () => {
        return {
            checked: false
        }
    };

    defaultWalkingState = () => {
        return {
            checked: false
        }
    };

    saveLiveActivityData() {
        const { saveLiveActivity, currentOrg, activeSession, currentTeam, reloadPageTitle, clearActiveBeepStartTime } = this.props;
        const { activity_id, activity_type, distance, measurementTypeIndex, startTypeIndex, orientationIndex, leadInDistance, endpointDistance, midpointDistance, headingThreshold, singleLeg, skatingMode, bobsledMode, walkingMode } = this.state;

        clearActiveBeepStartTime(activity_id);

        const units = measurementTypeIndex===0?'meters':'yards';
        const measurement_display = measurementTypeIndex===0?'m':'yd';
        const orientation_display = orientationIndex===0?'Vertical':'Horizontal';

        let type_display;
        let start_type;
        switch(activity_type) {
            case 'sprint':
                if(startTypeIndex===2) type_display = 'Fly Sprint';
                else if(startTypeIndex===1) type_display = 'Beep Sprint';
                else type_display = 'Motion Sprint';
                start_type = startTypeIndex;
                break;
            case 'freeform':
                type_display = 'Free Form';
                start_type = 0;
                break;
            case 'agility':
                if(startTypeIndex===2) type_display = 'Fly Agility';
                else if(startTypeIndex===1) type_display = 'Beep Agility';
                else type_display = 'Motion Agility';
                start_type = startTypeIndex;
                break;
            case 'jump':
                type_display = 'Jump';
                start_type = 0;
                break;
            default:
                type_display = 'Agility';
                start_type = startTypeIndex;
                break;
        }

        let triple_jump = global.enableSingleLegMode === true
            && activity_type==='jump'
            && orientationIndex === 1
            && singleLeg.jump.checked
            && singleLeg.jump.isTriple;

        let triple_jump_type = null;
        if (triple_jump) {
            triple_jump_type = singleLeg.jump.tripleJumpTypeIndex === 0 ? 'triple_hop' : 'crossover_hop';
        }

        let enabledSkating = global.enableSkatingMode === true
            && activity_type === 'sprint'
            && skatingMode.checked === true;

        let skating = null;
        if (enabledSkating === true) {
            skating = skatingMode.directionIndex === 0 ? 'forward' : 'backward';
        }

        let enabledBobsled = global.enableBobsledMode === true
            && activity_type === 'sprint'
            && bobsledMode.checked === true;
        let bobsled = enabledBobsled === true ? true : null;

        let enabledWalking = global.enableWalkingMode === true
            && activity_type === 'sprint'
            && walkingMode.checked === true;
        let walking = enabledWalking === true ? true : null;

        let display_name = distance+measurement_display+' '+type_display;
        if(activity_type==='jump') {
            let triple_display = '';
            if (triple_jump_type === 'triple_hop') {
                triple_display = 'Triple ';
            } else if (triple_jump_type === 'crossover_hop') {
                triple_display = 'Crossover ';
            }
            display_name = orientation_display+' '+triple_display+type_display;
        }
        if(activity_type==='freeform') display_name = type_display;
        if(activity_type==='sprint' && enabledSkating === true) {
            let textSkating = _.startCase(skating) + ' Skate';
            display_name = display_name.replace('Sprint', textSkating);
        }
        if(activity_type ==='sprint' && enabledBobsled === true) {
            display_name = display_name.replace('Sprint', 'Bobsled')
        }
        if(activity_type ==='sprint' && enabledWalking === true) {
            display_name = display_name.replace('Sprint', 'Walk')
        }
        let side = 'both';
        if(global.enableSingleLegMode === true && (activity_type==='jump' || activity_type==='sprint')) {
            if(activity_type==='sprint' && singleLeg.sprint.checked === true) {
               side = singleLeg.sprint.leftFooted === true ? 'left' : 'right';
            } else if(activity_type==='jump' && singleLeg.jump.checked === true) {
               side = singleLeg.jump.leftFooted === true ? 'left' : 'right';
            }
            if (side !== 'both') {
                display_name += ' ' + _.startCase(side) + ' Leg';
            }
        }
        const activity = {
            id: activity_id,
            activity_type,
            distance,
            leadInDistance,
            display_name,
            orientation: orientationIndex===0?"vertical":"horizontal",
            orientation_type: orientationIndex===0?"vertical":"horizontal",
            session: (activeSession && activeSession.id)?activeSession.id:{name:moment().format('MMMM DD, YYYY'),team:currentTeam},
            start_type: start_type===0?"motion":(start_type===1?"beep":"fly"),
            org: currentOrg.id,
            team: currentTeam,
            endpointDistance,
            midpointDistance,
            headingThreshold,
            singleLeg,
            side,
            triple_jump_type,
            skatingMode,
            skating,
            bobsledMode,
            bobsled,
            walkingMode,
            walking,
            units
        };

        saveLiveActivity(activity);
    }

    validateDistance = () => {
        const { distance } = this.state;
        return validate.single(distance, {
            presence: {
                allowEmpty: false,
                message: 'Please select a distance'
            },
            numericality: true
        });
    }

    validateMeasurementType = () => {
        const { measurementTypeIndex } = this.state;
        const measurementTypes = [0, 1]; // 0 = meters, 1 = yards.
        return validate.single(measurementTypeIndex, {
            presence: {
                allowEmpty: false,
                message: 'Please select a measurement type'
            },
            inclusion: measurementTypes
        });
    }

    validateStartType = () => {
        const { startTypeIndex } = this.state;
        const startTypes = [0, 1, 2]; // 0 = motion, 1 = beep.
        return validate.single(startTypeIndex, {
            presence: {
                allowEmpty: false,
                message: 'Please select a start type'
            },
            inclusion: startTypes
        });
    }

    validateOrientation = () => {
        const { orientationIndex } = this.state;
        const orientations = [0, 1]; // 0 = vertical, 1 = horizontal.
        return validate.single(orientationIndex, {
            presence: {
                allowEmpty: false,
                message: 'Please select an orientation'
            },
            inclusion: orientations
        });
    }

    closeSetup() {
        this.saveLiveActivityData();
        Navigation.pop(this.props.componentId);
    }

    setActivityType = (activity_type,index) => {
        this.setState({activity_type: activity_type,currentIndex:index});
    }

    _onChangeSound(index) {
        if(index<_.size(startSounds)) {
            const sound = startSounds[index];
            global.beepSound = sound;
            this.setState({currentSound: sound});
            localStorage.setItem('xps:beepSound', sound);
        }
    }

    _testStartSound() {
        const { testStartSound } = this.props;
        testStartSound(global.beepSound);
    }

    parseDistance = (distance) => {
        const _distance = distance.replace(/[^0-9.]/g, '');
        if(!!_distance) return parseFloat(_distance);
        else return '';
    }

    _renderItem = ( index ) => {
        const { measurementTypeIndex, startTypeIndex, endpointDistance, midpointDistance, headingThreshold, singleLeg, skatingMode, bobsledMode, walkingMode } = this.state;

        const sliderWidth = width-50;
        const sliderValueLeftPct = (this.state.sliderValue-1)/99;
        const sliderValueLeft = sliderValueLeftPct*sliderWidth-sliderValueLeftPct*30;
        const flySliderValueLeftPct = (this.state.flySliderValue-1)/10;
        const flySliderValueLeft = flySliderValueLeftPct*width-flySliderValueLeftPct*45;

        const soundLabels = ["Swim Start","Buzzer","Basketball Buzzer","Starting Pistol","Bike Horn","Ahooga","Car Horn","Train Horn","AirHorn","Cancel"];

        return (
            <View style={{width:'100%'}}>
                {index === 0 &&
                <View style={{paddingLeft: 20,paddingRight:20}}>
                    <Text style={{width: '100%', textAlign: 'center', color: '#666', fontWeight: 'bold', padding: 10}}>Distance</Text>
                    <Item style={{marginBottom: 10, borderWidth: 0, borderBottomWidth: 0}}>
                        <ButtonGroup
                            onPress={(index) => this.setState({measurementTypeIndex: index})}
                            selectedIndex={measurementTypeIndex}
                            buttons={['Meters', 'Yards']}
                            containerStyle={{height: 40, width: '90%', backgroundColor: '#f7f7f7', borderWidth: 0}}
                            buttonStyle={styles.buttonGroupStyle}
                            selectedButtonStyle={styles.buttonGroupSelectedStyle}
                            selectedBackgroundColor={'#f7f7f7'}
                            innerBorderStyle={{width: 0, color: '#f7f7f7'}}
                            textStyle={{color: '#666', fontWeight: 'bold'}}
                            selectedTextStyle={{color: '#fff', fontWeight: 'bold'}}
                        />
                    </Item>
                    <View style={{flexDirection: 'row',height:100}}>
                        <View style={{position:'absolute',top:80,left:7,width:sliderWidth,height:20,elevation:10}}>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', width: '100%'}}>
                                <Text style={{fontSize:12,color:'#999999'}}>1</Text>
                                <View style={styles.dividerLine}>{ }</View>
                                <Text style={{fontSize:12,color:'#999999'}}>50</Text>
                                <View style={styles.dividerLine}>{ }</View>
                                <Text style={{fontSize:12,color:'#999999'}}>100</Text>
                            </View>
                        </View>
                        <View style={{ flex: 1, alignItems: 'stretch', justifyContent: 'center' }}>
                            <Slider
                                value={this.state.distance}
                                minimumValue={1}
                                maximumValue={100}
                                step={1}
                                onSlidingComplete={distance => this.setState({ distance })}
                                onValueChange={sliderValue => this.setState({ sliderValue })}
                                style={{height:50,backgroundColor:'#2E9CEB',borderRadius:5}}
                                thumbTintColor={"#c6c6c6"}
                                minimumTrackTintColor={'#2E9CEB'}
                                maximumTrackTintColor={'#2E9CEB'}
                                thumbStyle={{height:50,width:40,padding:0,marginTop:5,borderRadius:0,borderBottomLeftRadius:5,borderBottomRightRadius:5}}
                            />
                            <Text style={{position:'absolute',left:sliderValueLeft,top:0,borderRadius:0,borderTopRightRadius:5,borderTopLeftRadius:5,fontSize:20,width:40,backgroundColor:'#2E9CEB',textAlign:"center",color:'#ffffff'}}>{this.state.sliderValue}</Text>
                        </View>
                    </View>
                    {global.enableWalkingMode === true &&
                    <CheckBox
                        center
                        iconRight
                        containerStyle={{backgroundColor: "transparent", borderColor: "transparent"}}
                        title='Walking'
                        checked={walkingMode.checked}
                        onPress={() => this.setState({
                            walkingMode: {
                                checked: !walkingMode.checked
                            }
                        })}
                    />
                    }
                    {global.enableBobsledMode === true &&
                    <CheckBox
                        center
                        iconRight
                        containerStyle={{backgroundColor: "transparent", borderColor: "transparent"}}
                        title='Bobsled'
                        checked={bobsledMode.checked}
                        onPress={() => this.setState({
                            bobsledMode: {
                                checked: !bobsledMode.checked
                            }
                        })}
                    />
                    }
                    {global.enableSkatingMode === true &&
                    <CheckBox
                        center
                        iconRight
                        containerStyle={{backgroundColor: "transparent", borderColor: "transparent"}}
                        title='Skating'
                        checked={skatingMode.checked}
                        onPress={() => this.setState({
                            skatingMode: {
                                ...skatingMode,
                                checked: !skatingMode.checked
                            }
                        })}
                    />
                    }
                    {global.enableSkatingMode === true && skatingMode.checked &&
                    <View>
                        <Item style={{marginBottom: 10, borderWidth: 0, borderBottomWidth: 0}}>
                            <ButtonGroup
                                onPress={(index) => this.setState({
                                    skatingMode: {
                                        ...skatingMode,
                                        directionIndex: index
                                    }
                                })}
                                selectedIndex={this.state.skatingMode.directionIndex}
                                buttons={['Forward', 'Backward']}
                                containerStyle={{height: 40, width: '90%', backgroundColor: '#f7f7f7', borderWidth: 0}}
                                buttonStyle={styles.buttonGroupStyle}
                                selectedButtonStyle={styles.buttonGroupSelectedStyle}
                                selectedBackgroundColor={'#f7f7f7'}
                                innerBorderStyle={{width: 0, color: '#f7f7f7'}}
                                textStyle={{color: '#666', fontWeight: 'bold'}}
                                selectedTextStyle={{color: '#fff', fontWeight: 'bold'}}
                            />
                        </Item>
                    </View>
                    }
                    {global.enableSingleLegMode === true &&
                    <CheckBox
                        center
                        iconRight
                        containerStyle={{backgroundColor: "transparent", borderColor: "transparent"}}
                        title='Single Leg'
                        checked={singleLeg.sprint.checked}
                        onPress={() => this.setState({
                            singleLeg: {
                                ...singleLeg,
                                sprint: {
                                    ...singleLeg.sprint,
                                    checked: !singleLeg.sprint.checked
                                }
                            }
                        })}
                    />
                    }
                    {global.enableSingleLegMode === true && singleLeg.sprint.checked &&
                    <View>
                        <Item style={{marginBottom: 10, borderWidth: 0, borderBottomWidth: 0}}>
                            <ButtonGroup
                                onPress={(index) => this.setState({
                                    singleLeg: {
                                        ...singleLeg,
                                        sprint: {
                                            ...singleLeg.sprint,
                                            leftFooted: index === 0
                                        }
                                    }
                                })}
                                selectedIndex={this.state.singleLeg.sprint.leftFooted ? 0 : 1}
                                buttons={['Left', 'Right']}
                                containerStyle={{height: 40, width: '90%', backgroundColor: '#f7f7f7', borderWidth: 0}}
                                buttonStyle={styles.buttonGroupStyle}
                                selectedButtonStyle={styles.buttonGroupSelectedStyle}
                                selectedBackgroundColor={'#f7f7f7'}
                                innerBorderStyle={{width: 0, color: '#f7f7f7'}}
                                textStyle={{color: '#666', fontWeight: 'bold'}}
                                selectedTextStyle={{color: '#fff', fontWeight: 'bold'}}
                            />
                        </Item>
                    </View>
                    }

                    <Text style={{width: '100%', textAlign: 'center', color: '#666', fontWeight: 'bold', paddingTop: 10, paddingBottom:10}}>Start
                        on:</Text>
                    <Item style={{marginBottom: 10, borderWidth: 0, borderBottomWidth: 0}}>
                        <ButtonGroup
                            onPress={(index) => this.setState({startTypeIndex: index})}
                            selectedIndex={this.state.startTypeIndex}
                            buttons={['Motion', 'Beep', 'Fly']}
                            containerStyle={{height: 40, width: '90%', backgroundColor: '#f7f7f7', borderWidth: 0}}
                            buttonStyle={styles.buttonGroupStyle}
                            selectedButtonStyle={styles.buttonGroupSelectedStyle}
                            selectedBackgroundColor={'#f7f7f7'}
                            innerBorderStyle={{width: 0, color: '#f7f7f7'}}
                            textStyle={{color: '#666', fontWeight: 'bold'}}
                            selectedTextStyle={{color: '#fff', fontWeight: 'bold'}}
                        />
                    </Item>
                    {startTypeIndex === 1 &&
                    <View>
                        <Text style={{width: '100%', textAlign: 'center', color: '#666', fontWeight: 'bold', paddingTop: 10, paddingBottom:10}}>Starting Sound:</Text>
                        <Button
                            onPress={() =>
                                ActionSheet.show(
                                    {
                                        options: soundLabels,
                                        cancelButtonIndex: _.size(startSounds),
                                        title: "Choose Starting Sound"
                                    },
                                    buttonIndex => {
                                        this._onChangeSound(buttonIndex);
                                    }
                                )}
                            style={{width:'100%',backgroundColor:'#2E9CEB',height:50,alignItems:"center"}}
                        >
                            <Text style={{width:'100%',textAlign:"center"}}>{ this.state.currentSound.name }</Text>
                        </Button>
                        <Button
                            onPress={() => this._testStartSound()}
                            style={{width:'100%',backgroundColor:'#f3f3f3',height:50}}
                        >
                            <Text style={{width:'100%',textAlign:"center",color:'#111'}}>Test</Text>
                        </Button>

                    </View>
                    }
                    {startTypeIndex===2 &&
                    <View>
                        <Text style={{width: '100%', textAlign: 'center', color: '#666', fontWeight: 'bold', padding: 10}}>Lead-in Distance</Text>
                        <View style={{flexDirection: 'row',height:100}}>
                            <View style={{position:'absolute',top:80,left:0,width:'100%',height:20,elevation:10}}>
                                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', width: '100%'}}>
                                    <Text style={styles.flyDistanceMarker}>1</Text>
                                    <Text style={styles.flyDistanceMarker}>2</Text>
                                    <Text style={styles.flyDistanceMarker}>3</Text>
                                    <Text style={styles.flyDistanceMarker}>4</Text>
                                    <Text style={styles.flyDistanceMarker}>5</Text>
                                    <Text style={styles.flyDistanceMarker}>6</Text>
                                    <Text style={styles.flyDistanceMarker}>7</Text>
                                    <Text style={styles.flyDistanceMarker}>8</Text>
                                    <Text style={styles.flyDistanceMarker}>9</Text>
                                    <Text style={styles.flyDistanceMarker}>10</Text>
                                </View>
                            </View>
                            <View style={{ flex: 1, alignItems: 'stretch', justifyContent: 'center' }}>
                                <Slider
                                    value={this.state.leadInDistance}
                                    minimumValue={1}
                                    maximumValue={10}
                                    step={1}
                                    onSlidingComplete={leadInDistance => this.setState({ leadInDistance })}
                                    onValueChange={flySliderValue => this.setState({ flySliderValue })}
                                    style={{height:50,backgroundColor:'#2E9CEB',borderRadius:5}}
                                    thumbTintColor={"#c6c6c6"}
                                    minimumTrackTintColor={'#2E9CEB'}
                                    maximumTrackTintColor={'#2E9CEB'}
                                    thumbStyle={{height:50,width:40,padding:0,marginTop:5,borderRadius:0,borderBottomLeftRadius:5,borderBottomRightRadius:5}}
                                />
                                <Text style={{position:'absolute',left:flySliderValueLeft,top:0,borderRadius:0,borderTopRightRadius:5,borderTopLeftRadius:5,fontSize:20,width:40,backgroundColor:'#2E9CEB',textAlign:"center",color:'#ffffff'}}>{this.state.flySliderValue}</Text>
                            </View>
                        </View>

                    </View>
                    }
                </View>
                }
                {index === 1 &&
                <View style={{padding: 20}}>
                    <Text style={{
                        width: '100%',
                        textAlign: 'center',
                        color: '#666',
                        fontWeight: 'bold',
                        padding: 10
                    }}>Orientation</Text>
                    <Item style={{marginBottom: 10, borderWidth: 0, borderBottomWidth: 0}}>
                        <ButtonGroup
                            onPress={(index) => this.setState({orientationIndex: index})}
                            selectedIndex={this.state.orientationIndex}
                            buttons={['Vertical', 'Horizontal']}
                            containerStyle={{height: 40, width: '90%', backgroundColor: '#f7f7f7', borderWidth: 0}}
                            buttonStyle={styles.buttonGroupStyle}
                            selectedButtonStyle={styles.buttonGroupSelectedStyle}
                            selectedBackgroundColor={'#f7f7f7'}
                            innerBorderStyle={{width: 0, color: '#f7f7f7'}}
                            textStyle={{color: '#666', fontWeight: 'bold'}}
                            selectedTextStyle={{color: '#fff', fontWeight: 'bold'}}
                        />
                    </Item>
                    {global.enableSingleLegMode === true &&
                    <CheckBox
                        center
                        iconRight
                        containerStyle={{backgroundColor: "transparent", borderColor: "transparent"}}
                        title='Single Leg'
                        checked={singleLeg.jump.checked}
                        onPress={() => this.setState({
                            singleLeg: {
                                ...singleLeg,
                                jump: {
                                    ...singleLeg.jump,
                                    checked: !singleLeg.jump.checked
                                }
                            }
                        })}
                    />
                    }
                    {global.enableSingleLegMode === true && singleLeg.jump.checked &&
                    <View>
                        <Item style={{marginBottom: 10, borderWidth: 0, borderBottomWidth: 0}}>
                            <ButtonGroup
                                onPress={(index) => this.setState({
                                    singleLeg: {
                                        ...singleLeg,
                                        jump: {
                                            ...singleLeg.jump,
                                            leftFooted: index === 0
                                        }
                                    }
                                })}
                                selectedIndex={this.state.singleLeg.jump.leftFooted ? 0 : 1}
                                buttons={['Left', 'Right']}
                                containerStyle={{height: 40, width: '90%', backgroundColor: '#f7f7f7', borderWidth: 0}}
                                buttonStyle={styles.buttonGroupStyle}
                                selectedButtonStyle={styles.buttonGroupSelectedStyle}
                                selectedBackgroundColor={'#f7f7f7'}
                                innerBorderStyle={{width: 0, color: '#f7f7f7'}}
                                textStyle={{color: '#666', fontWeight: 'bold'}}
                                selectedTextStyle={{color: '#fff', fontWeight: 'bold'}}
                            />
                        </Item>
                        {this.state.orientationIndex === 1 &&
                        <CheckBox
                            center
                            iconRight
                            containerStyle={{backgroundColor: "transparent", borderColor: "transparent"}}
                            title='Triple Jump'
                            checked={singleLeg.jump.isTriple}
                            onPress={() => this.setState({
                                singleLeg: {
                                    ...singleLeg,
                                    jump: {
                                        ...singleLeg.jump,
                                        isTriple: !singleLeg.jump.isTriple
                                    }
                                }
                            })}
                        />
                        }
                        {this.state.orientationIndex === 1 && this.state.singleLeg.jump.isTriple &&
                        <Item style={{marginBottom: 10, borderWidth: 0, borderBottomWidth: 0}}>
                            <ButtonGroup
                                onPress={(index) => this.setState({
                                    singleLeg: {
                                        ...singleLeg,
                                        jump: {
                                            ...singleLeg.jump,
                                            tripleJumpTypeIndex: index
                                        }
                                    }
                                })}
                                selectedIndex={this.state.singleLeg.jump.tripleJumpTypeIndex}
                                buttons={['Triple Hop', 'Crossover Hop']}
                                containerStyle={{height: 40, width: '90%', backgroundColor: '#f7f7f7', borderWidth: 0}}
                                buttonStyle={styles.buttonGroupStyle}
                                selectedButtonStyle={styles.buttonGroupSelectedStyle}
                                selectedBackgroundColor={'#f7f7f7'}
                                innerBorderStyle={{width: 0, color: '#f7f7f7'}}
                                textStyle={{color: '#666', fontWeight: 'bold'}}
                                selectedTextStyle={{color: '#fff', fontWeight: 'bold'}}
                            />
                        </Item>                        }
                        {this.state.orientationIndex === 1 && this.state.singleLeg.jump.isTriple &&
                          <Text style={{width: '90%', textAlign: 'left', color: '#666', fontWeight: 'bold', paddingLeft: 20}}>Triple jump mode records tracker messages and allows you to save them to the cloud for later processing. It does not include any motion algorithms or display any results.</Text>
                        }
                    </View>
                    }
                </View>
                }
                {index === 2 &&
                <View style={{paddingLeft: 20,paddingRight:20}}>
                    <Text style={{width: '100%', textAlign: 'center', color: '#666', fontWeight: 'bold', padding: 10}}>Distance (m)</Text>
                    <View style={{flexDirection: 'row',height:100}}>
                        <View style={{position:'absolute',top:80,left:7,width:sliderWidth,height:20,elevation:10}}>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', width: '100%'}}>
                                <Text style={{fontSize:12,color:'#999999'}}>1</Text>
                                <View style={styles.dividerLine}>{ }</View>
                                <Text style={{fontSize:12,color:'#999999'}}>50</Text>
                                <View style={styles.dividerLine}>{ }</View>
                                <Text style={{fontSize:12,color:'#999999'}}>100</Text>
                            </View>
                        </View>
                        <View style={{ flex: 1, alignItems: 'stretch', justifyContent: 'center' }}>
                            <Slider
                                value={this.state.distance}
                                minimumValue={1}
                                maximumValue={100}
                                step={1}
                                onSlidingComplete={distance => this.setState({ distance })}
                                onValueChange={sliderValue => this.setState({ sliderValue })}
                                style={{height:50,backgroundColor:'#2E9CEB',borderRadius:5}}
                                thumbTintColor={"#c6c6c6"}
                                minimumTrackTintColor={'#2E9CEB'}
                                maximumTrackTintColor={'#2E9CEB'}
                                thumbStyle={{height:50,width:40,padding:0,marginTop:5,borderRadius:0,borderBottomLeftRadius:5,borderBottomRightRadius:5}}
                            />
                            <Text style={{position:'absolute',left:sliderValueLeft,top:0,borderRadius:0,borderTopRightRadius:5,borderTopLeftRadius:5,fontSize:20,width:40,backgroundColor:'#2E9CEB',textAlign:"center",color:'#ffffff'}}>{this.state.sliderValue}</Text>
                        </View>
                    </View>
                    <Text style={{width: '100%', textAlign: 'center', color: '#666', fontWeight: 'bold', paddingTop: 10, paddingBottom:10}}>Start
                        on:</Text>
                    <Item style={{marginBottom: 10, borderWidth: 0, borderBottomWidth: 0}}>
                        <ButtonGroup
                            onPress={(index) => this.setState({startTypeIndex: index})}
                            selectedIndex={this.state.startTypeIndex}
                            buttons={['Motion', 'Beep', 'Fly']}
                            containerStyle={{height: 40, width: '90%', backgroundColor: '#f7f7f7', borderWidth: 0}}
                            buttonStyle={styles.buttonGroupStyle}
                            selectedButtonStyle={styles.buttonGroupSelectedStyle}
                            selectedBackgroundColor={'#f7f7f7'}
                            innerBorderStyle={{width: 0, color: '#f7f7f7'}}
                            textStyle={{color: '#666', fontWeight: 'bold'}}
                            selectedTextStyle={{color: '#fff', fontWeight: 'bold'}}
                        />
                    </Item>
                    {startTypeIndex === 1 &&
                    <View>
                        <Text style={{width: '100%', textAlign: 'center', color: '#666', fontWeight: 'bold', paddingTop: 10, paddingBottom:10}}>Starting Sound:</Text>
                        <Button
                        onPress={() =>
                            ActionSheet.show(
                                {
                                    options: soundLabels,
                                    cancelButtonIndex: _.size(startSounds),
                                    title: "Choose Starting Sound"
                                },
                                buttonIndex => {
                                    this._onChangeSound(buttonIndex);
                                }
                            )}
                        style={{width:'100%',backgroundColor:'#2E9CEB',height:50,alignItems:"center"}}
                    >
                        <Text style={{width:'100%',textAlign:"center"}}>{ this.state.currentSound.name }</Text>
                        </Button>
                        <Button
                            onPress={() => this._testStartSound()}
                            style={{width:'100%',backgroundColor:'#f3f3f3',height:50}}
                        >
                            <Text style={{width:'100%',textAlign:"center",color:'#111'}}>Test</Text>
                        </Button>

                    </View>
                    }
                    {startTypeIndex===2 &&
                    <View>
                        <Text style={{width: '100%', textAlign: 'center', color: '#666', fontWeight: 'bold', padding: 10}}>Lead-in Distance (m)</Text>
                        <View style={{flexDirection: 'row',height:100}}>
                            <View style={{position:'absolute',top:80,left:0,width:'100%',height:20,elevation:10}}>
                                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', width: '100%'}}>
                                    <Text style={styles.flyDistanceMarker}>1</Text>
                                    <Text style={styles.flyDistanceMarker}>2</Text>
                                    <Text style={styles.flyDistanceMarker}>3</Text>
                                    <Text style={styles.flyDistanceMarker}>4</Text>
                                    <Text style={styles.flyDistanceMarker}>5</Text>
                                    <Text style={styles.flyDistanceMarker}>6</Text>
                                    <Text style={styles.flyDistanceMarker}>7</Text>
                                    <Text style={styles.flyDistanceMarker}>8</Text>
                                    <Text style={styles.flyDistanceMarker}>9</Text>
                                    <Text style={styles.flyDistanceMarker}>10</Text>
                                </View>
                            </View>
                            <View style={{ flex: 1, alignItems: 'stretch', justifyContent: 'center' }}>
                                <Slider
                                    value={this.state.leadInDistance}
                                    minimumValue={1}
                                    maximumValue={10}
                                    step={1}
                                    onSlidingComplete={leadInDistance => this.setState({ leadInDistance })}
                                    onValueChange={flySliderValue => this.setState({ flySliderValue })}
                                    style={{height:50,backgroundColor:'#2E9CEB',borderRadius:5}}
                                    thumbTintColor={"#c6c6c6"}
                                    minimumTrackTintColor={'#2E9CEB'}
                                    maximumTrackTintColor={'#2E9CEB'}
                                    thumbStyle={{height:50,width:40,padding:0,marginTop:5,borderRadius:0,borderBottomLeftRadius:5,borderBottomRightRadius:5}}
                                />
                                <Text style={{position:'absolute',left:flySliderValueLeft,top:0,borderRadius:0,borderTopRightRadius:5,borderTopLeftRadius:5,fontSize:20,width:40,backgroundColor:'#2E9CEB',textAlign:"center",color:'#ffffff'}}>{this.state.flySliderValue}</Text>
                            </View>
                        </View>

                    </View>
                    }
                    <View style={[styles.viewRow,{height:200}]}>
                        <View style={{flex:1}}>
                            <Text style={{width: '100%', textAlign: 'center', color: '#666', fontWeight: 'bold', padding: 10, height:40}}>Endpoint (m)</Text>
                            <Input
                                value={String(endpointDistance)}
                                keyboardType='numeric'
                                onChangeText={(endpoint_distance) => this.setState({ endpointDistance: this.parseDistance(endpoint_distance) })}
                                inputStyle={{}}
                                containerStyle={{borderColor:'#dddddd',borderWidth:1,height:40}}
                                textStyle={{}}
                            />
                        </View>
                        <View style={{flex:1}}>
                            <Text style={{width: '100%', textAlign: 'center', color: '#666', fontWeight: 'bold', padding: 10, height:40}}>Midpoint (m)</Text>
                            <Input
                                value={String(midpointDistance)}
                                keyboardType='numeric'
                                onChangeText={(midpoint_distance) => this.setState({ midpointDistance: this.parseDistance(midpoint_distance) })}
                                inputStyle={{}}
                                containerStyle={{borderColor:'#dddddd',borderWidth:1,height:40}}
                                textStyle={{}}
                            />
                        </View>
                        <View style={{flex:1}}>
                            <Text style={{width: '100%', textAlign: 'center', color: '#666', fontWeight: 'bold', padding: 10, height:40}}>Threshold (°)</Text>
                            <Input
                                value={String(headingThreshold)}
                                keyboardType='numeric'
                                onChangeText={(heading_threshold) => this.setState({ headingThreshold: this.parseDistance(heading_threshold) })}
                                inputStyle={{}}
                                containerStyle={{borderColor:'#dddddd',borderWidth:1,height:40}}
                                textStyle={{}}
                            />
                        </View>
                    </View>
                </View>
                }
                {index === 3 &&
                <View style={{paddingLeft: 20,paddingRight:20}}>
                    <Text style={{width: '100%', textAlign: 'center', color: '#666', fontWeight: 'bold', padding: 10,paddingTop:40}}>Free form mode records tracker messages and allows you to save them to the cloud for later processing.</Text>
                    <Text style={{width: '100%', textAlign: 'center', color: '#666', fontWeight: 'bold', padding: 10}}>It does not include any motion algorithms or display any results.</Text>
                </View>
                }
            </View>
        )
    }

    render() {
        const { activity_type,currentIndex } = this.state;

        const activity_types = ['sprint','jump','agility', 'freeform'];

        return (
            <Container>
                <Content style={{marginBottom:70,width: '100%', backgroundColor:'#f7f7f7'}}>
                    <View style={[styles.viewRowCenter200]}>
                        <TouchableOpacity style={[styles.activityIconRowStyle,activity_type!=='sprint'?{opacity:.9,backgroundColor:'#fff'}:{opacity:1,backgroundColor:'rgba(106,200,147,.3)'}]} onPress={() => this.setActivityType('sprint',0)}>
                            <CustomIcon name='sprint' style={[{color:'#000'},currentIndex===0?styles.iconLarge:styles.iconMed]} />
                            <Text>Sprint</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.activityIconRowStyle,activity_type!=='jump'?{opacity:.9,backgroundColor:'#fff'}:{opacity:1,backgroundColor:'rgba(106,200,147,.3)'}]} onPress={() => this.setActivityType('jump',1)}>
                            <CustomIcon name='jump' style={[{color:'#000'},currentIndex===1?styles.iconLarge:styles.iconMed]} />
                            <Text>Jump</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.activityIconRowStyle,activity_type!=='agility'?{opacity:.9,backgroundColor:'#fff'}:{opacity:1,backgroundColor:'rgba(106,200,147,.3)'}]} onPress={() => this.setActivityType('agility',2)}>
                            <Icon name="random" type="FontAwesome" style={[{paddingTop:25,paddingBottom:7},currentIndex===2?styles.iconLarge2:styles.iconMed2]} />
                            <Text>Agility</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.activityIconRowStyle,activity_type!=='freeform'?{opacity:.9,backgroundColor:'#fff'}:{opacity:1,backgroundColor:'rgba(106,200,147,.3)'}]} onPress={() => this.setActivityType('freeform',3)}>
                            <Icon name="cloud-upload" type="FontAwesome" style={[{paddingTop:25,paddingBottom:7},currentIndex===3?styles.iconLarge2:styles.iconMed2]} />
                            <Text>Free Form</Text>
                        </TouchableOpacity>
                    </View>

                    <Carousel
                        ref={(c) => { this.graphCarousel = c; }}
                        data={activity_types}
                        renderItem={({item,index}) => this._renderItem(index)}
                        sliderWidth={width}
                        itemWidth={width}
                        firstItem={currentIndex}
                        inactiveSlideScale={1}
                        scrollEnabled={false}
                        onSnapToItem={(slideIndex) => this.setActivityType(activity_types[slideIndex],slideIndex)}
                    />
                </Content>
                <DoneBar
                    onPressSave={() => this.closeSetup()}
                    isActive={true}
                />
            </Container>
        )
    }
}

export default connect(
    null,
    { saveLiveActivity, testStartSound, clearActiveBeepStartTime }
)(LiveActivitySetupScreen);
