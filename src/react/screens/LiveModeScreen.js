import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import { connect } from 'react-redux';
import { ActivityIndicator, Alert } from 'react-native';

import _ from 'lodash';
import moment from "moment";
import styles from '../../theme';
import { Picker } from 'react-native-wheel-datepicker';
import { localStorage } from "../../lib/local_storage";
import { Container, View, Text, List, Button, Icon, Spinner, SwipeRow, Content, Item, Toast, Root } from 'native-base';

import LiveAthleteReadyListItem from '../../react/components/LiveAthleteReadyListItem';
import LiveRecordBar from '../../react/components/LiveRecordBar';
import LivePositioningRecordBar from '../../react/components/LivePositioningRecordBar';
import LiveOptionBar from '../../react/components/LiveOptionBar';
import TopMessageBar from '../../react/components/TopMessageBar';

import { getActivityIsSaving, getCurrentOrg, getCurrentSystem, getCurrentTags, getActivityById, getMembersByOrg, getLiveActivity,
    getActiveSession, getActiveActivity, getTeamsByCurrentOrg, getCurrentTeam, getWifiState, getSystemsByOrg } from '../../redux/reducers';
import { connectSystem, disconnectSystem, startSync, stopSync, unReadyTag, setupMessageListener, setTrackerMode, clearTags, sendModeSelect } from '../../redux/actions/system';
import { fetchActiveUsers, saveActiveUser, setActiveBeepStartTime, clearActiveBeepStartTime, removeActiveUserFromActivity } from '../../redux/actions/active_user';
import { setupTag, removeTagLiveData, fetchTagsBySystem, fetchTagsByOrg, clearTagsVelocities, resetMotionAlgos, sendTagSelects, setTagsPositions, clearTagsPositions } from '../../redux/actions/tag';
import { fetchActivity, toggleRecording, resetRecording, resetRecordingAll, getHasActivityDataInBuffer, getHasActivityRawDataInBuffer } from '../../redux/actions/activity';
import { connectToWifiNetwork, stopWifiReconnect, fetchCurrentWifiState } from '../../redux/actions/wifi';
import { allActiveUsersSelector, activityActiveUsersSelector } from '../../redux/selectors/active_users';
import { fetchLiveActivity, setLiveActivityTeam } from '../../redux/actions/live_activity';
import { fetchActivitySummariesByActivity } from '../../redux/actions/activity_summary';
import { saveOrgMember, fetchMembersByOrg } from '../../redux/actions/org_member';
import { fetchTeamsByOrg, setCurrentTeam } from '../../redux/actions/team';
import { fetchSystemsByOrg } from '../../redux/actions/org_system';
import { fetchCurrentCloudState } from '../../redux/actions/cloud';
import { getActiveUsersIsFetching } from '../../redux/reducers';
import { saveActivity, saveFreeformActivity } from '../../redux/actions/activity';
import { saveSystemInfo } from '../../redux/actions/system';

import {
    SYSTEM_STATE_CONNECTING,
    SYSTEM_STATE_CONNECTED,
    SYSTEM_STATE_DISCONNECTING,
    SYSTEM_STATE_DISCONNECTED,
} from '../../redux/constants/systemStates';


export const mapStateToProps = (state, ownProps) => {
    return {
        currentOrg: getCurrentOrg(state),
        activeSession: getActiveSession(state),
        activeActivity: getActiveActivity(state),
        currentActivity: getActivityById(state, ownProps.activityId),
        currentTeam: getCurrentTeam(state),
        liveActivity: getLiveActivity(state),
        currentSystem: getCurrentSystem(state),
        tags: getCurrentTags(state),
        activeUsers: activityActiveUsersSelector(state, ownProps),
        allActiveUsers: allActiveUsersSelector(state, ownProps),
        isFetching: getActiveUsersIsFetching(state),
        isSaving: getActivityIsSaving(state),
        teams: getTeamsByCurrentOrg(state),
        orgMembers: getMembersByOrg(state),
        orgSystems: getSystemsByOrg(state),
        wifiState: getWifiState(state),
    }
}

export class LiveModeScreen extends Component {
    constructor(props) {
        super(props);
        this.rows = [];
        this.tagNames = [];
        this.connectInterval = null;
        this.state = {
            isRecording: false,
            isVideoActive: false,
            isBeepStart: false,
            syncStarted: false,
            syncRunning: false,
            activeTags: [],
            inactiveTags: [],
            recordingStart:null,
            loadingSystem: false,
            recordError:null,
            userTagReady:false,
            beepCleared: false,
            firstLoad: false,
            leadDistanceSet: false,
            isWifiConnecting: false,
            hasWifiFailed: false,
            wifiConnectCounter: 0,
            userSelectedTags: {},
            tagsSelectPending: {},
            recordReady: false,
            isSaving: false,
            modeSelect: null,
            appState: null,
            showSaveScreen: null,
        }
        this.cancelWifiConnect = this.cancelWifiConnect.bind(this);
        this.useSavedTag = this.useSavedTag.bind(this);
        this.onPressRemoveAthlete = this.onPressRemoveAthlete.bind(this);
        Navigation.events().bindComponent(this);
    }

    componentDidMount() {
        const {
            activityId, activeUsers, fetchActiveUsers, currentOrg, currentActivity,
            fetchActivity, liveActivity, fetchLiveActivity, teams, fetchTeamsByOrg,
            orgMembers, fetchMembersByOrg, orgSystems,
            fetchTagsByOrg, tags
        } = this.props;

        fetchActiveUsers();
        if (!currentActivity && activityId) fetchActivity(activityId);
        if (!liveActivity || !liveActivity.id) fetchLiveActivity();
        if (!teams || teams.length === 0) fetchTeamsByOrg(currentOrg.id);
        if (!orgMembers || orgMembers.length === 0) fetchMembersByOrg(currentOrg.id);
        if (!orgSystems || orgSystems.length === 0) fetchSystemsByOrg(currentOrg.id);
        if (!tags || _.size(tags) === 0) fetchTagsByOrg(currentOrg.id);

        this.setState({beepCleared:false, firstLoad: true, modeSelect: null});

        this.doConnections();

        if(liveActivity && liveActivity.display_name) {
            this.reloadPageTitle();
        }

        // if we already know the active users, send out the tag selects
        if (activeUsers) this.handleTagStateChanges(activeUsers);
    }

    openSystemSettings() {
        this.props.navigator.push({
            screen: 'SystemStatusScreen',
            title: 'System Configuration',
            passProps: {
            }
        });
    }

    componentDidUpdate(prevProps, prevState) {
        const {
            activeUsers, setupTag, tags, currentSystem, stopSync, liveActivity, orgSystems, toggleRecording,
            currentTeam, setLiveActivityTeam, wifiState, connectSystem, isSaving, navigator, connectToWifiNetwork, sendTagSelects, fetchCurrentWifiState
        } = this.props;
        const { isRecording, isWifiConnecting } = this.state;
        const isManualHorizontalJump = (global.enableManualHorizontalJump && liveActivity && liveActivity.activity_type==='jump' && liveActivity.orientation==='horizontal');

        if(currentSystem && (currentSystem.ssid==='unassigned' || currentSystem.serial_num==='unassigned')) {
            const orgSystem = _.first(_.values(orgSystems));
            this.changeSystem(orgSystem);
        }

        if(currentSystem && currentSystem.appState !== prevProps.currentSystem.appState) {
            this.setState({appState: currentSystem.appState});
            if(currentSystem.appState === 'active' && prevProps.currentSystem && prevProps.currentSystem.appState === 'background') {
                // TODO - we just went from background (which kicked us off Wifi) to active
                this.doConnections();
            }
        }

        if(currentSystem && currentSystem.offset_data && currentSystem.offset_data!==prevProps.currentSystem.offset_data) {
            this.setState({offsetData: currentSystem.offset_data});
        }

        if(currentSystem && currentSystem.mode>0 && currentSystem.mode!==prevProps.currentSystem.mode) {
            // we have a mode response, start recording
            this.setState({modeSelect:'confirmed'});
            this.setState({isRecording: true, recordingStart: moment(new Date()).format('X')});
            toggleRecording(liveActivity.id, tags, true);
        }

        if(currentSystem && currentSystem.mode===0 && currentSystem.mode!==prevProps.currentSystem.mode) {
            // we have a mode reset
            this.setState({modeSelect:'reset'});
        }

        if(currentSystem && currentSystem.mode==='failed' && currentSystem.mode!==prevProps.currentSystem.mode) {
            // we have a failed mode response
            this.setState({modeSelect:'failed'});
        }

        // does our local component state think we're attempting to connect to wifi?
        if (isWifiConnecting) {
            // now check the global wifi states to see if we need to take action.
            if (wifiState.isConnected && currentSystem && wifiState.deviceSSID === currentSystem.ssid) {
                // cool, we're connected. set local state and try the hub
                this.setState({isWifiConnecting: false});
                connectSystem(currentSystem, true);
            }
            if (wifiState.isConnected === false && wifiState.isConnecting === false) {
                this.setState({isWifiConnecting: false, hasWifiFailed: true});
            }
        }

        // if we were connecting, and now we are connected..
        if (currentSystem && prevProps.currentSystem.state !== SYSTEM_STATE_CONNECTED && currentSystem.state === SYSTEM_STATE_CONNECTED) {
            this.handleSystemConnection(activeUsers);
        }

        // if we were connected, but now we skipped straight to SYSTEM_STATE_CONNECTING
        // then that means we lost connection.. let's decide if we should re-connect
        // or if we should alert the user!
        if (currentSystem && prevProps.currentSystem.state === SYSTEM_STATE_CONNECTED && currentSystem.state === SYSTEM_STATE_CONNECTING) {
            // the system is going to try restarting...but if we're in record mode
            // then we need to do something here to inform the user.
            if (isRecording) {
                // TODO - alert
                console.log('we lost connection in record mode');
            }
        }

        if(liveActivity && liveActivity.team && currentTeam!==liveActivity.team) {
            setLiveActivityTeam(currentTeam);
        }

        // TODO (GLP) - review this section.
        if(liveActivity) {
            if(!!liveActivity.display_name && liveActivity.display_name!==prevProps.liveActivity.display_name) {
                this.reloadPageTitle();
            }
            if(liveActivity.distance!==prevProps.liveActivity.distance || liveActivity.activity_type!==prevProps.liveActivity.activity_type || liveActivity.start_type!==prevProps.liveActivity.start_type || (liveActivity.endpointDistance && liveActivity.endpointDistance!==prevProps.liveActivity.endpointDistance) || (liveActivity.midpointDistance && liveActivity.midpointDistance!==prevProps.liveActivity.midpointDistance) || (liveActivity.headingThreshold && liveActivity.headingThreshold!==prevProps.liveActivity.headingThreshold ||
                liveActivity.side!==prevProps.liveActivity.side || liveActivity.triple_jump_type!==prevProps.liveActivity.triple_jump_type ||
                liveActivity.skating!==prevProps.liveActivity.skating || liveActivity.units!==prevProps.liveActivity.units ||
                liveActivity.walking!==prevProps.liveActivity.walking ||
                liveActivity.bobsled!==prevProps.liveActivity.bobsled)) {
                if (currentSystem && currentSystem.state === SYSTEM_STATE_CONNECTED) {
                    _.each(activeUsers,(activeUser) => {
                        if (activeUser.tag) {
                            setupTag(activeUser.tag, liveActivity, activeUser);
                        }
                    });
                }
                if(liveActivity.start_type!=='beep' && this.state.syncStarted) stopSync(currentSystem);
                this.setState({syncStarted:false,beepCleared:true});
                clearActiveBeepStartTime(liveActivity.id);
            }
            if(liveActivity.activity_type!=='jump') {
                if(liveActivity.start_type==='fly') {
                    if(!this.state.leadDistanceSet) {
                        _.each(activeUsers, (activeUser) => {
                            if (activeUser.tag_data && activeUser.tag_data.motionAlgo && activeUser.tag_data.motionAlgo.setStartDistance) {
                                this.setState({leadDistanceSet: true});
                                activeUser.tag_data.motionAlgo.setStartDistance(parseFloat(liveActivity.leadInDistance), parseInt(activeUser.tag.name));
                            }
                        })
                    }
                } else {
                    if(this.state.leadDistanceSet) {
                        _.each(activeUsers, (activeUser) => {
                            if (activeUser.tag_data && activeUser.tag_data.motionAlgo && activeUser.tag_data.motionAlgo.setStartDistance) {
                                this.setState({leadDistanceSet: false});
                                activeUser.tag_data.motionAlgo.setStartDistance(100, parseInt(activeUser.tag.name));
                            }
                        })
                    }
                }
            }
        }

        // only run through this logic if something has actually changed.
        if(activeUsers && activeUsers.length > 0 && _.isEqual(activeUsers, prevProps.activeUsers) === false) {
            this.handleActiveUserChanges(activeUsers, prevProps.activeUsers);
        }

        if(activeUsers && activeUsers.length!==prevProps.activeUsers.length) {
            sendTagSelects();
        }

        // if we are waiting to save, and the save is complete
        if (this.state.isSaving && !isSaving) {
            this.setState({isSaving: false, showSaveScreen: null});
            navigator.dismissModal();
        }
    }

    componentWillUnmount() {
        const { currentSystem, disconnectSystem, activeUsers, stopWifiReconnect, saveActiveUser,
            resetRecordingAll, fetchCurrentCloudState } = this.props;

        // reset the active user info..
        _.each(activeUsers, (activeUser) => {
            if (activeUser.tag_data) {
                saveActiveUser({
                    ...activeUser,
                    tag_data: null
                });
            }
        });

        //clearTimeout(this.connectInterval);

        resetRecordingAll();
        stopWifiReconnect();
        fetchCurrentCloudState(true);
        disconnectSystem(currentSystem);
    }

    doConnections = () => {
        const { wifiState, currentSystem, connectSystem, connectToWifiNetwork } = this.props;
        // if we are connected to wifi, and we are connected to the right system
        // then we can go on with the regular system/hub connection.
        if (wifiState.isConnected && currentSystem && wifiState.deviceSSID === currentSystem.ssid) {
            connectSystem(currentSystem, true);
        } else if(global.debugMode) {
            connectSystem(currentSystem, true);
        } else {
            if(currentSystem) {
                const {ssid, password} = currentSystem;
                connectToWifiNetwork(ssid, password, true);
                this.setState({
                    isWifiConnecting: true,
                    hasWifiFailed: false,
                });
            }
        }
    }

    changeSystem = (orgSystem) => {
        const { saveSystemInfo, connectSystem, fetchTagsByOrg, currentOrg, connectToWifiNetwork } = this.props;
        const { isWifiConnecting, wifiConnectCounter } = this.state;
        if(orgSystem && orgSystem) {
            disconnectSystem(global.xpsConfig.system);

            global.xpsConfig.system[1].ip_address = orgSystem.hostname;
            global.xpsConfig.system[1].port = 1884;
            global.xpsConfig.system[1].ssid = orgSystem.wlan_ssid;
            global.xpsConfig.system[1].password = orgSystem.wlan_password;
            global.xpsConfig.system[1].serial_num = orgSystem.serial_num;
            global.xpsConfig.system[1].name = orgSystem.name;
            global.xpsConfig.system[1].system = orgSystem.id;
            global.xpsConfig.system[1].id = orgSystem.id;

            if(!isWifiConnecting && !!orgSystem.wlan_ssid) {
                if(wifiConnectCounter<3) {
                    const password = orgSystem.wlan_password ? orgSystem.wlan_password : this.wifiPassword;
                    connectToWifiNetwork(orgSystem.wlan_ssid, password, true);
                    this.setState({isWifiConnecting: true, wifiConnectCounter:wifiConnectCounter+1});
                }
            }

            localStorage.setItem('xps:appConfig', global.xpsConfig);

            saveSystemInfo(global.xpsConfig.system);

            // TODO - this is still a hardcoded system ID 1
            connectSystem(global.xpsConfig.system[1],true);
            fetchTagsByOrg(currentOrg.id);

            this.setState({currentSystem:orgSystem.id});
        }
    };

    handleSystemConnection = (activeUsers) => {
        const { setupTag, tags, liveActivity } = this.props;

        // for all our pending select tags, we can now set them up
        _.each(activeUsers, (activeUser) => {
            if(activeUser.tag) {
                const tag = _.find(tags, (tag) => tag.name===activeUser.tag.name);
                if (tag) {
                    console.log('handleSystemConnection - calling setupTag ' + tag.name);
                    setupTag(tag, liveActivity, activeUser);
                }
            }
        });
    }

    handleTagStateChanges = (activeUsers) => {
        const { setupTag, liveActivity, currentSystem, clearActiveBeepStartTime, startSync } = this.props;
        const { tagsSelectPending, syncStarted, isRecording, beepCleared, userSelectedTags } = this.state;
        const activeUserWithTags = _.filter(activeUsers, (activeUser) => !!activeUser.tag);
        const activeUserWithReadyTags = _.filter(activeUsers, (activeUser) => activeUser.tag_data && (activeUser.tag_data.status === 'ready' || activeUser.tag_data.status === 'recording'));

        // for each active user with a tag, let's send tag selects if we haven't already
        _.each(activeUserWithTags, (activeUser) => {
            const tag = activeUser.tag;
            const tag_data = activeUser.tag_data;

            if(!userSelectedTags[activeUser.id] && tag && tag.name) {
                userSelectedTags[activeUser.id] = tag.name;
            }

            // only do this if the tag has a valid name, and is not already in our pending list
            if (!tag_data && tag && tag.name) {
                // call setState (async) to add to our list, then call the setupTag method
                this.setState({
                    tagsSelectPending: {
                        ...tagsSelectPending,
                        [tag.name]: activeUser
                    }
                }, () => {
                    // we can't send a tag select if we aren't connected yet!
                    if (currentSystem.state === SYSTEM_STATE_CONNECTED) {
                        setupTag(tag, liveActivity, activeUser, true);
                    }
                });
            } else if (tag.name && tag_data && tag_data.status !== 'select_pending') {
                //let {[tag.name]: deleted, ...newTagSelectPending} = tagsSelectPending;
                const newTagSelectPending = {...tagsSelectPending,[tag.name]: activeUser};
                this.setState({tagsSelectPending: newTagSelectPending});
            }
        });

        // decide whether or not we should start the time sync
        if (activeUserWithTags.length && liveActivity.id && currentSystem &&
            currentSystem.state === SYSTEM_STATE_CONNECTED &&
            liveActivity.activity_type !== 'jump') {
            if(liveActivity.start_type === 'beep') {
                if (!syncStarted) {
                    this.setState({syncStarted: true}, () => {
                        startSync(currentSystem);
                    });
                } else if (!beepCleared) {
                    this.setState({beepCleared: true}, () => {
                        clearActiveBeepStartTime(liveActivity.id);
                    });
                }
            } else {
                clearActiveBeepStartTime(liveActivity.id);
            }
        }

        // decide whether or not we are ready to record
        //if (!isRecording) {
            const recordReady = (activeUsers.length > 0 && activeUserWithReadyTags.length === activeUsers.length || global.debugMode) &&
                currentSystem && currentSystem.state === SYSTEM_STATE_CONNECTED;
            this.setState({ recordReady });
        //}

        // are we done recording?
        if (isRecording) {
            const activeCompletedUsers = _.filter(activeUsers, (activeUser) => activeUser.completed === true);
            if (activeCompletedUsers.length === activeUsers.length) {
                this.toggleRecordingState();
            }
        }
    }

    handleActiveUserChanges = (activeUsers, prevActiveUsers) => {
        this.handleTagStateChanges(activeUsers);
    }

    changePollingRate = () => {
        const { activeUsers } = this.props;
        const rates = [10,10,13,20,25,40];
        const numberOfTags = _.clamp(activeUsers.length, 0, 4); // max out at 4

        _.each(activeUsers,(activeUser) => {
            if(activeUser.tag_data && activeUser.tag_data.motionAlgo && activeUser.tag_data.motionAlgo.changePollingRate) {
                activeUser.tag_data.motionAlgo.changePollingRate(rates[numberOfTags]);
            }
        });
    }

    reloadActiveUsers = () => {
        const { fetchActiveUsers } = this.props;
        fetchActiveUsers();
    }

    reloadPageTitle = () => {
        const { liveActivity } = this.props;
        Navigation.mergeOptions(this.props.componentId, {
            topBar: {
                title: {
                    text: liveActivity.display_name
                }
            }
        });
    }

    componentDidAppear() {
        this.reloadPageTitle();
    }

    componentDidDisappear() {
        const { currentSystem } = this.props;

        this.reloadPageTitle();
        resetRecordingAll();
        stopWifiReconnect();
        fetchCurrentCloudState(true);
        disconnectSystem(currentSystem);
    }

    navigationButtonPressed() {
        Navigation.dismissModal(this.props.componentId);
        this.reloadPageTitle();
    }

    onExitLiveMode = () => {
        Navigation.dismissModal(this.props.componentId);
        this.reloadPageTitle();
    }

    onPressRemoveAthlete = (activeUser) => {
        const { saveActiveUser, removeTagLiveData, activeUsers, clearTags, removeActiveUserFromActivity, sendTagSelects } = this.props;
        removeActiveUserFromActivity(activeUser.id);
        saveActiveUser({
            ...activeUser,
            isInActivity: false,
        });
        if (activeUser.tag) {
            removeTagLiveData(parseInt(activeUser.tag.name, 10));
        }
        if (activeUsers.length === 1) clearTags();
        else sendTagSelects();
        if(this.selectedRow && this.selectedRow._root) this.selectedRow._root.closeRow()
    }

    onPressAthleteSelect = () => {
        const { activeUsers } = this.props;
        Navigation.push(this.props.componentId, {
            component: {
                name: 'LiveAthleteSelectScreen',
                passProps: {
                    reloadActiveUsers: this.reloadActiveUsers,
                    useSavedTag: this.useSavedTag,
                    onPressRemoveAthlete: this.onPressRemoveAthlete,
                    activeUsers
                },
                options: {
                    topBar: {
                        title: {
                            text: 'Select Athletes'
                        },
                        rightButtons: [{
                            text: 'Done',
                            id: 'cancel',
                            color: 'black'
                        }]
                    }
                }
            }
        });
    }

    onPressActivitySetup = () => {
        const { currentOrg, liveActivity, activeSession, currentTeam } = this.props;
        Navigation.push(this.props.componentId, {
            component: {
                name: 'LiveActivitySetupScreen',
                passProps: {
                    reloadPageTitle: () => this.reloadPageTitle,
                    currentOrg,
                    liveActivity,
                    activeSession,
                    currentTeam
                },
                options: {
                    topBar: {
                        title: {
                            text: 'Configure Activity'
                        }
                    }
                }
            }
        });
    }

    onPressHomeButton = () => {
        this.props.navigator.resetTo({
            screen: 'SessionListScreen',
            title: 'Sessions',
            animated: true,
            animationType: 'fade',
        });
    }

    onPressSave = () => {
        const { saveActivity, activeUsers, liveActivity, navigator } = this.props;

        saveActivity(liveActivity, activeUsers);
        this.setState({isSaving: true, showSaveScreen: null});
        Navigation.dismissModal('modalLiveActivityRecordSaveScreen');
    }

    onPressDiscard = () => {
        this.setState({showSaveScreen: null})
        Navigation.dismissModal('modalLiveActivityRecordSaveScreen');
    }

    showSaveScreen = () => {
        const {liveActivity, activeUsers, saveActivity } = this.props;
        Navigation.showModal({
            stack: {
                children: [{
                    component: {
                        id: 'modalLiveActivityRecordSaveScreen',
                        name: 'LiveActivityRecordSaveScreen',
                        passProps: {
                            activityUsers:activeUsers,
                            saveActivity,
                            onPressSave: this.onPressSave,
                            onPressDiscard: this.onPressDiscard
                        },
                        options: {
                            topBar: {
                                title: {
                                    text: liveActivity.display_name+' Result'
                                }
                            }
                        }
                    }
                }]
            }
        });
    }

    toggleRecordingState = () => {
        const {
            toggleRecording, resetRecording, liveActivity, activeUsers, saveFreeformActivity,
            saveActivity, clearTagsVelocities, sendModeSelect, resetMotionAlgos, getHasActivityRawDataInBuffer
        } = this.props;
        const { isRecording } = this.state;
        const tags = _.map(activeUsers, (activeUser) => activeUser.tag.name);
        const isManualHorizontalJump = (global.enableManualHorizontalJump && liveActivity && liveActivity.activity_type==='jump' && liveActivity.orientation==='horizontal');

        let incomplete = false;
        //check time on recording - if over 3 seconds, show save dialog to push incomplete activity record

        if (isRecording) {
            const completed = _.filter(activeUsers, (activeUser) => activeUser.completed === true);
            const isBeepSprint = (liveActivity && liveActivity.start_type==='beep') ? liveActivity.activity_type !== 'jump' : false;

            if (completed.length === 0) {
                const recordingEnd = moment(new Date()).format('X');
                if(liveActivity.activity_type==='freeform' ||
                    (liveActivity.activity_type === 'jump' && liveActivity.triple_jump_type)) {
                    const hasRawActivityData = getHasActivityRawDataInBuffer(activeUsers);
                    if (hasRawActivityData) {
                        Alert.alert(
                            'Save Activity',
                            'Do you want to save this free form recording?',
                            [
                                {
                                    text: 'Yes', onPress: () => {
                                        saveFreeformActivity(liveActivity, activeUsers)
                                        this.handleSystemConnection(activeUsers);
                                    }
                                },
                                {
                                    text: 'No', onPress: () => {
                                        resetRecording(liveActivity.id);
                                        this.handleSystemConnection(activeUsers);
                                    }
                                }
                            ],
                            {cancelable: true},
                        );
                    } else {
                        Alert.alert(
                            'No activity data in recording',
                            'We were unable to find any data in the recording, so there is nothing to save.',
                            [
                                {
                                    text: 'Ok', onPress: () => {
                                        resetRecording(liveActivity.id);
                                        this.handleSystemConnection(activeUsers);
                                    }
                                }
                            ],
                            {cancelable: true},
                        );
                    }
                } else {
                    const hasActivityData = getHasActivityDataInBuffer(activeUsers);
                    if (hasActivityData) {
                        this.setState({recordingStart: null});
                        if (recordingEnd - this.state.recordingStart > 2) {
                            incomplete = true;

                            Alert.alert(
                                'No activity detected',
                                'Do you want this recording to be saved and re-analyzed?',
                                [
                                    {
                                        text: 'Yes', onPress: () => {
                                            saveActivity(liveActivity, activeUsers, true)
                                            resetMotionAlgos(activeUsers);
                                            this.handleSystemConnection(activeUsers);
                                        }
                                    },
                                    {
                                        text: 'No', onPress: () => {
                                            resetMotionAlgos(activeUsers);
                                            resetRecording(liveActivity.id);
                                            this.handleSystemConnection(activeUsers);
                                        }
                                    }
                                ],
                                {cancelable: true},
                            );
                        }
                    } else {
                        Alert.alert(
                            'No activity data in recording',
                            'We were unable to find any data in the recording, so there is nothing to save.',
                            [
                                {
                                    text: 'Ok', onPress: () => {
                                        resetMotionAlgos(activeUsers);
                                        resetRecording(liveActivity.id);
                                        this.handleSystemConnection(activeUsers);
                                    }
                                }
                            ],
                            {cancelable: true},
                        );
                    }
                }
            }

            // this means we are going from recording to stop-recording
            // if (data)  TODO - only transition if we have data.
            //console.log('setting recording = false')
            this.setState({isRecording: false,isBeepStart: false});
            toggleRecording(liveActivity.id, tags, false);

            //const completed = _.filter(activeUsers, (activeUser) => activeUser.completed === true);

            if (completed.length) {
                // now we launch into the save state and reset completed flags.
                _.each(activeUsers, (activeUser) => activeUser.completed = false);
                if (!isManualHorizontalJump) {
                    this.showSaveScreen();
                } else {
                    this.setState({showSaveScreen: this.showSaveScreen})
                }
            } else {
                // we just reset
                if(!incomplete &&
                    liveActivity.activity_type!=='freeform' &&
                    !(liveActivity.activity_type==='jump' && liveActivity.triple_jump_type)) resetRecording(liveActivity.id);
            }
            if(liveActivity.activity_type!=='freeform' &&
                !(liveActivity.activity_type==='jump' && liveActivity.triple_jump_type)) resetMotionAlgos(activeUsers);
            clearTagsVelocities(activeUsers);
            sendModeSelect(0);
            this.setState({modeSelect:'waiting'});
        } else {
            const tagNames = _.map(activeUsers, (activeUser) => activeUser.tag.name);
            tagNames.map((tagName)=> {
                // send tag reset message
                const msg = {cmd_id:5,tag_rst:{tag_id:parseInt(tagName)}};
                global.client.sendMessage(msg);
            });
            this.changePollingRate();
            const numTags = activeUsers.length;
            const mode = numTags>4?4:numTags;
            sendModeSelect(mode);
            if(global.debugMode) {
                this.setState({isRecording: true, recordingStart: moment(new Date()).format('X')});
                toggleRecording(liveActivity.id, tags, true);
            } else this.setState({modeSelect:'waiting', showSaveScreen: null});
        }
    }

    handleSprintBeepStart = () => {
        const { currentSystem } = this.props;
        if (currentSystem && currentSystem.isLocal || global.debugMode) {
            const {liveActivity, setActiveBeepStartTime} = this.props;
            global.timedStart.playStartSound(global.beepSound.file,global.beepSound.type).then(
                response => {
                    console.log('EST time: '+response.currentTime);
                    console.log('app time: '+response.currentAppTime);
                    console.log('offset: '+response.timedOffset);
                    console.log('max offset diff: '+response.maxOffsetDiff);
                    console.log('min offset diff: '+response.minOffsetDiff);

                    // 45ms offset for time to play beep
                    const offset = 45;
                    setActiveBeepStartTime(liveActivity.id, response.currentTime + offset)
                },
                error => {
                    console.error(error);
                }
            );
        }
        this.setState({isBeepStart: true});
    }

    saveSelectedTag = (activeUser) => {
        // user has selected a tag. save and select it if necessary. also boot off anyone that was using this tag!
        const { tags, saveActiveUser, activeUsers } = this.props;
        const { userSelectedTags } = this.state;

        // close this row that was slid open.
        if (this.selectedRow) this.selectedRow._root.closeRow();

        // if we don't find it, it's because they didn't change it from the first value
        if(!userSelectedTags[activeUser.id]) {
            userSelectedTags[activeUser.id] = tags[Object.keys(tags)[0]].name;
        }

        const tagName = userSelectedTags[activeUser.id];
        const tag = _.find(tags, (tag) => tag.name === tagName);

        if (tag) {
            // let's see if anyone else is already using this tag
            const otherUser = _.find(activeUsers, (actUser) => actUser.id !== activeUser.id && actUser.tag && parseInt(actUser.tag.name) === parseInt(tagName));

            // now we save this tag to this active user
            saveActiveUser({
                ...activeUser,
                tag,
                tag_data: null,
            });
            this.callSetupTag(activeUser);

            if (otherUser) {
                // someone else has this tag selected, we boot them off here.
                saveActiveUser({
                    ...otherUser,
                    tag: null,
                    tag_data: null,
                });
                this.callSetupTag(otherUser);
            }
        }
    }

    useSavedTag = (activeUser) => {
        // user has selected a tag. save and select it if necessary. also boot off anyone that was using this tag!
        const { liveActivity, saveActiveUser, setupTag, activeUsers } = this.props;

        if(!activeUser.tag) return;

        const tag = activeUser.tag;
        setupTag(tag, liveActivity, activeUser);

        // let's see if anyone else is already using this tag
        const otherUser = _.find(activeUsers, (actUser) => actUser.id !== activeUser.id && actUser.tag && parseInt(actUser.tag.name) === parseInt(tag.name));

        if (otherUser) {
            // someone else has this tag selected, we boot them off here.
            saveActiveUser({
                ...otherUser,
                tag: null,
                tag_data: null,
            });
            setupTag(tag, liveActivity, otherUser);
        }
    }

    callSetupTag = (activeUser) => {
      const { tags, liveActivity, setupTag } = this.props;
        const { userSelectedTags } = this.state;
        const selectedTag = _.find(tags, (t) => t.name === userSelectedTags[activeUser.id]);
        if(selectedTag) setupTag(selectedTag, liveActivity, activeUser);
    }

    cancelWifiConnect = () => {
        this.setState({isWifiConnecting:false});
    }

    render() {
        const { isRecording, userSelectedTags, isVideoActive, recordReady, isBeepStart, isWifiConnecting, hasWifiFailed, modeSelect, offsetData, showSaveScreen } = this.state;
        const { liveActivity, activeUsers, isFetching, currentSystem, navigator, wifiState } = this.props;
        const isBeepSprint = (liveActivity && liveActivity.start_type==='beep') ? liveActivity.activity_type !== 'jump' : false;
        const isManualHorizontalJump = (global.enableManualHorizontalJump && liveActivity && liveActivity.activity_type==='jump' && liveActivity.orientation==='horizontal');
        const { tags } = this.props;
        const tagNames = _.sortBy(_.map(tags, (tag) => tag.name), (tagName) => parseInt(tagName, 10) );

        const hubName = currentSystem && currentSystem.name ? currentSystem.name : (currentSystem && currentSystem.ssid ? currentSystem.ssid : currentSystem?currentSystem.ip_address : '');

        if (isFetching) {
            return (
                <Container style={{ alignSelf: "center" }}>
                    <Spinner />
                </Container>
            )
        }

        return (
            <Root>
                <Container>
                    <Content style={[styles.contentStyle,(currentSystem && (currentSystem.state === SYSTEM_STATE_CONNECTED || currentSystem.state === SYSTEM_STATE_DISCONNECTED || currentSystem.state === SYSTEM_STATE_CONNECTING)) ? {marginTop:40} : {} ]}>
                        {!!activeUsers && activeUsers.length > 0 &&
                        activeUsers.map( (activeUser,index) =>
                            <View key={index}
                                  style={{backgroundColor:index%2?'#ffffff':'#f7f7f7',width:'100%'}}>
                                <SwipeRow
                                    key={activeUser.id}
                                    ref={(c) => { this.rows[activeUser.id] = c }}
                                    onRowOpen={() => {
                                        if (this.selectedRow && this.selectedRow !== this.rows[activeUser.id]) {
                                            if(this.selectedRow._root) {
                                                this.selectedRow._root.closeRow();
                                            }
                                        }
                                        this.selectedRow = this.rows[activeUser.id]
                                    }}
                                    leftOpenValue={75}
                                    rightOpenValue={-150}
                                    style={{elevation:1}}
                                    left={!isRecording &&
                                    <Button success onPress={() => { this.onPressRemoveAthlete(activeUser) }}>
                                        <Icon type='FontAwesome' name="minus" />
                                    </Button>
                                    }
                                    body={
                                        <LiveAthleteReadyListItem
                                            key={activeUser.id}
                                            currentActivity={liveActivity}
                                            activeUser={activeUser}
                                            isRecording={isRecording}
                                            currentSystem={currentSystem}
                                            callSetupTag={() => this.callSetupTag(activeUser)}
                                            navigator={navigator}
                                        />
                                    }
                                    right={!isRecording &&
                                    <View style={{
                                        flex: 1,
                                        flexDirection: 'row',
                                        justifyContent: 'space-between'
                                    }}>
                                        <Item picker style={{
                                            flex: 1,
                                            marginBottom: 10,
                                            width: '50%',
                                            borderWidth: 0,
                                            borderBottomWidth: 0
                                        }}>
                                            <Picker
                                                style={{flex: 1, backgroundColor: '#e6e6e6'}}
                                                selectedValue={userSelectedTags[activeUser.id] ? userSelectedTags[activeUser.id] : (tags[0] ? tags[0].name : '')}
                                                pickerData={tagNames}
                                                onValueChange={(tagName) => this.setState({
                                                    userSelectedTags: {
                                                        ...userSelectedTags,
                                                        [activeUser.id]: tagName
                                                    }
                                                })}
                                            />
                                        </Item>
                                        <Button success style={{height: '100%'}} onPress={() => {
                                            this.saveSelectedTag(activeUser)
                                        }}>
                                            <Icon type="FontAwesome" name="check"/>
                                        </Button>
                                    </View>
                                    }
                                />
                            </View>
                        )}
                        {liveActivity.activity_type!=='jump' && liveActivity.start_type==='beep' && !!offsetData &&
                        <Text style={{padding:10,alignSelf: 'center'}}>Beep Start Time: {offsetData.beepStartTime}</Text>
                        }
                    </Content>

                    {!isRecording &&
                        <LiveOptionBar
                            isRecording={isRecording}
                            isVideoActive={isVideoActive}
                            recordReady={recordReady}
                            onPressAthleteSelect={this.onPressAthleteSelect}
                            onPressActivitySetup={this.onPressActivitySetup}
                            activeUsers={activeUsers}
                            liveActivity={liveActivity}
                        />
                    }
                    {!!isBeepSprint &&
                        <LiveRecordBar
                            modeSelect={modeSelect}
                            isBeepStart={isBeepStart}
                            syncRunning={currentSystem.syncStarted}
                            timingValid={currentSystem.timingValid}
                            isRecording={isRecording}
                            isVideoActive={isVideoActive}
                            recordReady={recordReady}
                            toggleRecordingState={this.toggleRecordingState}
                            handleSprintBeepStart={this.handleSprintBeepStart}
                            isBeepSprint={isBeepSprint}
                        />
                    }
                    {!isBeepSprint && !isManualHorizontalJump &&
                        <LiveRecordBar
                            modeSelect={modeSelect}
                            isRecording={isRecording}
                            recordReady={recordReady}
                            toggleRecordingState={this.toggleRecordingState}
                        />
                    }
                    {!isBeepSprint && isManualHorizontalJump &&
                        <LivePositioningRecordBar
                            modeSelect={modeSelect}
                            isRecording={isRecording}
                            recordReady={recordReady}
                            toggleRecordingState={this.toggleRecordingState}
                            showSaveScreen={showSaveScreen}
                        />
                    }
                    {!!isWifiConnecting && wifiState.connectionError === '' &&
                        <TopMessageBar
                            navigator={navigator}
                            type='working'
                            message={`Connecting to ${hubName}`}
                        />
                    }
                    {!!isWifiConnecting && wifiState.connectionError !== '' &&
                        <TopMessageBar
                            navigator={navigator}
                            type='error'
                            message={`${hubName} not available (01)`}
                        />
                    }

                    {!isWifiConnecting && !!currentSystem && currentSystem.state === SYSTEM_STATE_CONNECTING &&
                        <TopMessageBar
                            navigator={navigator}
                            type='working'
                            message={`Connecting to ${hubName}`}
                        />
                    }

                    {!isWifiConnecting && !!currentSystem && currentSystem.state === SYSTEM_STATE_DISCONNECTED &&
                        <TopMessageBar
                            navigator={navigator}
                            type='error'
                            message={`${hubName} not available (02)`}
                        />
                    }

                    {!isWifiConnecting && !!currentSystem && currentSystem.state === SYSTEM_STATE_CONNECTED &&
                        <TopMessageBar
                            navigator={navigator}
                            type='success'
                            message={`Connected to ${hubName}`}
                        />
                    }

                    {!!isWifiConnecting &&
                        <TopMessageBar
                            navigator={navigator}
                            type='working'
                            message={`Connecting to Wifi`}
                        />
                    }

                    {!!hasWifiFailed &&
                        <TopMessageBar
                            navigator={navigator}
                            type='error'
                            message={`Wifi Connection Failed`}
                            onPressReload={this.doConnections}
                        />
                    }
                </Container>
            </Root>
        )
    }
}

export default connect(
    mapStateToProps,
    {
        connectSystem,
        disconnectSystem,
        fetchActivity,
        toggleRecording,
        resetRecording,
        resetRecordingAll,
        fetchActiveUsers,
        setActiveBeepStartTime,
        clearActiveBeepStartTime,
        removeTagLiveData,
        saveActivity,
        saveFreeformActivity,
        setupTag,
        setTagsPositions,
        clearTagsPositions,
        unReadyTag,
        saveOrgMember,
        saveActiveUser,
        fetchTagsBySystem,
        fetchTagsByOrg,
        startSync,stopSync,
        clearTagsVelocities,
        fetchLiveActivity,
        getLiveActivity,
        fetchTeamsByOrg,
        fetchMembersByOrg,
        fetchSystemsByOrg,
        setLiveActivityTeam,
        setTrackerMode,
        clearTags,
        sendModeSelect,
        setupMessageListener,
        connectToWifiNetwork,
        stopWifiReconnect,
        saveSystemInfo,
        fetchCurrentWifiState,
        fetchCurrentCloudState,
        resetMotionAlgos,
        getHasActivityDataInBuffer,
        getHasActivityRawDataInBuffer,
        sendTagSelects,
        removeActiveUserFromActivity
    }
)(LiveModeScreen);
