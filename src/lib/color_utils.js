function hashCode(str) {
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    return hash;
}

function intToRGB(i){
    var c = (i & 0x00FFFFFF)
        .toString(16)
        .toUpperCase();

    return "00000".substring(0, 6 - c.length) + c;
}

function hexToRGB(a) {
    a = +(
        "0x" +
        a.slice(1)
            .replace(
                a.length < 4
                && /./g,
                '$&$&'
            )
    );

    return [
        a >> 16,
        a >> 8 & 255,
        a & 255
    ]
}

export function createUserColor(name) {
    let hsp;
    let color = '#000';
    let fontColor = '#fff';
    let luminosity = 'dark';
    let rgb;

    if(name) {
        color = '#' + intToRGB(hashCode(name));

        if (color) {
            const colors = hexToRGB(color);
            rgb = colors;
            hsp = Math.sqrt(0.299 * (colors[0] * colors[0]) + 0.587 * (colors[1] * colors[1]) + 0.114 * (colors[2] * colors[2]));
            if (hsp && hsp >= 127.5) {
                fontColor = '#000';
                luminosity = 'light';
            }
        }
    }

    return {color:color,fontColor:fontColor,luminosity:luminosity,rgb:rgb};
}