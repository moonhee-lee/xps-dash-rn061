/* thanks https://github.com/hswolff/BetterWeather */
import * as scale from 'd3-scale';
import * as shape from 'd3-shape';
import * as d3Array from 'd3-array';
import _ from 'lodash';

const d3 = {
    scale,
    shape,
};

/**
 * Create an x-scale.
 * @param {number} start Start time in seconds.
 * @param {number} end End time in seconds.
 * @param {number} width Width to create the scale with.
 * @return {Function} D3 scale instance.
 */
function createScaleX(start, end, width) {
    return d3.scale.scaleTime()
        .domain([start, end])
        .range([0, width]);
}

/**
 * Create a y-scale.
 * @param {number} minY Minimum y value to use in our domain.
 * @param {number} maxY Maximum y value to use in our domain.
 * @param {number} height Height for our scale's range.
 * @return {Function} D3 scale instance.
 */
function createScaleY(minY, maxY, height) {
    return d3.scale.scaleLinear()
        .domain([minY, maxY]).nice()
        // We invert our range so it outputs using the axis that React uses.
        .range([height, 0]);
}

const timeAccessor = (data) => {
    if (data.time) return data.time;
}

const yAccessor = (data, meta, activityType) => {
    if (activityType === 'jump') {
        return data.posM.z;
    } else {
        if(meta && meta.startPosAndTime) {
            return Math.sqrt( Math.pow((data.posM.x-meta.startPosAndTime.x), 2) + Math.pow((data.posM.y-meta.startPosAndTime.y), 2) );
        }
        return Math.sqrt( Math.pow((data.posM.x), 2) + Math.pow((data.posM.y), 2) );
    }
}

const zIMUAccessor = (data, meta, activityType) => {
    if (activityType !== 'jump') {
        return Math.sqrt( Math.pow((data.accBodyMPS2.z), 2) + Math.pow((data.accBodyMPS2.z), 2) );
    }
        return null;
}

const velAccessor = (data) => {
    return Math.sqrt( Math.pow(data.velMPS.x, 2) + Math.pow(data.velMPS.y, 2) );
}

const accelAccessor = (data) => {
    return Math.sqrt( Math.pow(data.accMPS2.x, 2) + Math.pow(data.accMPS2.y, 2) );
}

/**
 * Creates a line graph SVG path that we can then use to render in our
 * React Native application with ART.
 * @param {Array.<Object>} options.data Array of data we'll use to create
 *   our graphs from.
 * @param {function} xAccessor Function to access the x value from our data.
 * @param {function} yAccessor Function to access the y value from our data.
 * @param {number} width Width our graph will render to.
 * @param {number} height Height our graph will render to.
 * @return {Object} Object with data needed to render.
 */
export function createLineGraph({
                                    data,
                                    tagId,
                                    width,
                                    height,
                                    activityType,
                                }) {
    if (!data.length) return;

    const lastDatum = data[data.length - 1];

    const scaleX = createScaleX(
        data[0].time,
        lastDatum.time,
        width
    );

    // Collect all y values.
    const allYValues = data.reduce((all, datum) => {
        all.push(yAccessor(datum, activityType));
        return all;
    }, []);

    // Get the min and max y value.
    const extentY = d3Array.extent(allYValues);
    const scaleY = createScaleY(extentY[0], extentY[1], height);

    const lineShape = d3.shape.line()
        .x((d) => scaleX(timeAccessor(d)))
        .y((d) => scaleY(yAccessor(d, activityType)));

    return {
        tagId,
        path: lineShape(data),
    };
}

export function generateCoDDistances(meta) {
    const distances = [];
    if(meta.changes_of_direction && _.size(meta.changes_of_direction)) {
        _.each(meta.changes_of_direction,(cod) => {
            if(cod.apexXY) {
                distances.push([-100, cod.apexXY.y-meta.startPosAndTime.y]);
            } else {
                if (cod.startPosAndTime.y && cod.endPosAndTime.y) {
                    const _start = cod.startPosAndTime.y - meta.startPosAndTime.y;
                    const _end = cod.endPosAndTime.y - meta.startPosAndTime.y;
                    const _mid = (_start + _end) / 2;
                    distances.push([_start - 1, _end - 1]);
                }
            }
        });
    }
    return distances;
}

export function generateStrideData(meta) {
    if(meta.stride_data_metres && _.size(meta.stride_data_metres)) {
        var distances = _.values(_.mapValues(meta.stride_data_metres, (item) => item.length));
        let rates = _.values(_.mapValues(meta.stride_data_metres, (item) => item.frequency));
        let times = _.values(_.mapValues(meta.stride_data_metres, (item) => item.contact_time));
        return {distances:distances,rates:rates,times:times};
    }
    return false;
}

export function generateStrideListData(meta) {
    if(meta.stride_data_metres && _.size(meta.stride_data_metres)) {
        var lengths = _.values(_.mapValues(meta.stride_data_metres, (item) => item.length));
        let frequencies = _.values(_.mapValues(meta.stride_data_metres, (item) => item.frequency.toFixed(2)));
        let times = _.values(_.mapValues(meta.stride_data_metres, (item) => item.contact_time));
        let sides = _.values(_.mapValues(meta.stride_data_metres, (item) => item.side));
        return {frequencies,times,lengths,sides};
    }
    return false;
}

export function generateSplitListData(meta) {
    if(meta.split_data && _.size(meta.split_data)) {
        var accelerations = meta.split_data.accelerations;
        let velocities = meta.split_data.velocities;
        let times = _.map(meta.split_data.times,(time) => {
            return parseFloat(time/1000)-parseFloat(meta.start/1000)
        });
        return {accelerations,times,velocities};
    }
    return false;
}

export function getLineGraphData({ data, meta, width, height, activityType, events }) {
    if (!data.length) return;

    let splitValuesM = [];
    let accelerationValuesM;
    let velocityValuesM;

    if(meta && meta.split_data) {
        if(meta.split_data.times) {
            splitValuesM = Object.entries(meta.split_data.times).reduce((all, datum) => {
                all.push(parseFloat(datum[1]/1000)-parseFloat(meta.start/1000));
                return all;
            }, []);
            //velocityValuesM = formatUtils.getSplitVelocities(meta);
            //accelerationValuesM = formatUtils.getSplitAccelerations(meta);
            accelerationValuesM = meta.split_data.accelerations;
            velocityValuesM = meta.split_data.velocities;
        } else {
            splitValuesM = Object.entries(meta.split_data).reduce((all, datum) => {
                all.push(parseFloat(datum[1]/1000)-parseFloat(meta.start/1000));
                return all;
            }, []);
        }
    }

    if(events) {
        velocityValuesM = [];
        _.each(events,(event) => {
            if(event.type==='split') velocityValuesM.push(event.velocity)
        });
    }

    let strideValues;
    if(meta) {
        strideValues = generateStrideData(meta);
    } else {
        strideValues = {};
    }

    // Collect all y values.
    const allYValues = data.reduce((all, datum, index) => {
        if(index%20===0) {
            all.push(yAccessor(datum, meta, activityType));
        }
        return all;
    }, []);

    // Collect all y values.
    const allZIMUValues = data.reduce((all, datum, index) => {
        if(index%5===0 && meta) {
            if(datum.time>=((meta.start)-500) && datum.time<=((meta.end)+500)) {
                all.push(zIMUAccessor(datum, meta, activityType));
            }
        }
        return all;
    }, []);

    return {
        splits: splitValuesM,
        velocity: velocityValuesM,
        distance: allYValues,
        //acceleration: accelerationValuesM,
        strides: strideValues,
        strideZ: allZIMUValues,
    };
}
