import _ from 'lodash';
import * as graphUtils from './graph_utils';
import {Dimensions} from "react-native";
import moment from "moment";
const { width,height } = Dimensions.get('window');
import * as formatUtils from './format_utils';

export function getSummaryData(type_definition, data_summary, events) {
    // events?

    let summaryData = { primary: {}, secondary: {}, listItems: []};

    let listData = [];
    let listPage = [];
    let splitListPage = [];
    let codListPage = [];
    let unit = formatUtils.getUnit(type_definition.units);

    switch(type_definition.activity_type) {

        case 'agility':

            const CoDs = formatUtils.generateCoDs(data_summary, events);
            // codListPage.push(CoDs)
            listData.push(CoDs);

            const codCount = _.size(CoDs);

            // summary
            // -------------------------------------------
            summaryData.primary = {
                label: "Time",
                value: formatUtils.formatTime(data_summary.total_time)
            }

            summaryData.listItems.push({label:'Changes of direction',value:codCount});
            summaryData.listItems.push({label:'Top speed',value:formatUtils.formatSpeed(data_summary.max_velocity)});
            if(data_summary.reaction_time) summaryData.listItems.push({label:'Reaction time',value:formatUtils.formatTime(data_summary.reaction_time)});
            if(data_summary.max_acceleration) summaryData.listItems.push({label:'Peak acceleration',value:formatUtils.formatAcceleration(data_summary.max_acceleration)});

            // summaryData.listItems.push({label:'Top speed',value:formatUtils.formatSpeed(max_vel)});
            // summaryData.listItems.push({label:'Fastest 5m',value:fastest_5m_split});

            // const CoDs = formatUtils.generateCoDs(data_summary,events);
            // listData.push(CoDs);

        break;

        case 'sprint':

            // assemble data for splits table
            let splitsListPage = [];
            const splitData = formatUtils.generateSplitListData(data_summary,events);
            const freqs = splitData.frequencies;

            if(splitData) {
                splitListPage.push(splitData);
            }

            let max_vel = 0;
            let split_vel = 0;
            let min_aggregate_split_time = null;
            let max_split = null;
            let fastest_5m_split = null;

            // parse out 5m splits; look for best-split and max-speed analytics
            if(splitData.accelerations && splitData.accelerations.length) {
                let aggregate_5m_time = 0;
                _.each(splitData.accelerations,(split,index) => {
                    split_vel = splitData.velocities[index]
                    // get max velocities from smoothed 1m splits
                    if (parseFloat(split_vel) > parseFloat(max_vel)) {
                        max_vel = split_vel;
                        max_split = index + 1;
                    }

                    // also identify fastest 5m split
                    if(index%5===4) {
                        let start = index >= 4 ? index - 4 : 0;
                        let end = index + 1;
                        let splitsBy5m = _.slice(splitData.times, start, end).map((time) => parseFloat(time));

                        aggregate_5m_time = _.sum(splitsBy5m);

                        if (min_aggregate_split_time) {
                            if (aggregate_5m_time < min_aggregate_split_time) {
                                min_aggregate_split_time = aggregate_5m_time;
                                fastest_5m_split = formatUtils.formatTime(min_aggregate_split_time) + ' (' + (index-3)+'-'+(index+1) + unit + ')';
                            }
                        } else {
                            min_aggregate_split_time = aggregate_5m_time;
                        }
                    }
                });
            }

            // assemble data for strides/steps table
            let stridesListPage = [];
            const strideData = formatUtils.generateStrideListData(data_summary, events);
            if(strideData) {
                stridesListPage.push(strideData);
            }

            listData.push(splitListPage);
            listData.push(stridesListPage);

            // summary
            // -------------------------------------------
            summaryData.primary = {
                label: "Time",
                value: formatUtils.formatTime(data_summary.total_time)
            }

            let textTopSpeed = formatUtils.formatSpeed(max_vel);
            if (max_split) {
                textTopSpeed += ' (' + max_split + unit + ')';
            }
            summaryData.listItems.push({label:'Top speed',value: textTopSpeed});
            if (fastest_5m_split) {
                summaryData.listItems.push({label:'Fastest 5' + unit,value:fastest_5m_split});
            }
            if (freqs) { summaryData.listItems.push({label:'Stride frequency',value:formatUtils.getMedian(splitData.frequencies)}); }

            if(data_summary.reaction_time) summaryData.listItems.push({label:'Reaction time',value:formatUtils.formatTime(data_summary.reaction_time)});
            if(data_summary.max_acceleration) summaryData.listItems.push({label:'Peak acceleration',value:formatUtils.formatAcceleration(data_summary.max_acceleration)});

            if (global.enableStrideLengthView === true) {
                let strides =_.filter(events, (event) => { return event.type === 'stride' });
                if (strides) {
                    summaryData.listItems.push({label:'Stride length (med)', value:formatUtils.formatDistance(formatUtils.median(strides.map(s => s.length)))});
                    summaryData.listItems.push({label:'Contact time (med)', value:formatUtils.formatTime(formatUtils.median(strides.map(s => s.contact_time)))});
                }
            }

        break;

        case 'jump':
            switch(type_definition.orientation) {
                case 'vertical':
                    summaryData.primary = {
                        label: 'Height',
                        value:formatUtils.formatDistance(data_summary.height)
                    }
                    summaryData.secondary = {
                        label: '',
                        value: formatUtils.formatDistanceImperial(data_summary.height, true)
                    }
                break;
                case 'horizontal':
                    summaryData.primary = {
                        label: 'Distance',
                        value: formatUtils.formatDistance((data_summary.length ? data_summary.length : data_summary.distance))
                    }
                    summaryData.secondary = {
                        label: '',
                        value: formatUtils.formatDistanceImperial((data_summary.length ? data_summary.length : data_summary.distance))
                    }
                break;
            }

            // NOTE: temporary format adapter here to accommodate both (disparately structured) live and non-live activity objects
            summaryData.listItems.push({label:'Air time',value:formatUtils.formatTime(data_summary.air_time/1000)});
            summaryData.listItems.push({label:'Takeoff angle',value:formatUtils.formatAngle(data_summary.takeoffAngle ? data_summary.takeoffAngle : data_summary.takeoff_angle)});
            summaryData.listItems.push({label:'Takeoff acceleration',value:formatUtils.formatAcceleration(data_summary.takeoffAcceleration ? data_summary.takeoffAcceleration : data_summary.pushoff_acceleration)});

        break;

    }

    return { summaryData, listData };

}

export function loadGraphData(toGraphESTData, activeUser, liveActivity) {
    const data_summary = activeUser.meta_data;

    let graph = [], cods;

    const graphWidth = width;
    const graphHeight = height;

    const activity_type = liveActivity.activity_type;

    if(toGraphESTData && _.size(toGraphESTData)) {
        const graphData = graphUtils.getLineGraphData({
            data: _.values(toGraphESTData),
            meta: data_summary,
            width: graphWidth,
            height: graphHeight,
            activityType: activity_type,
        });

        if (activity_type === 'jump') {
            graph = graphData.distance;
        }
        if (activity_type !== 'jump') {
            graph = graphData.velocity;
        }
    }
    return graph;
}
