import _ from 'lodash';
import moment from "moment";
import { normalize } from "normalizr";
import * as schema from '../redux/schema';
import * as orgMemberApi from '../redux/api/org_member';
import * as sessionApi from '../redux/api/session';
import * as activityApi from '../redux/api/activity';
import { SAVE_ORG_MEMBER_FAILURE, SAVE_ORG_MEMBER_SUCCESS, SAVE_SESSION_SUCCESS, REMOVE_LOCAL_ORG_MEMBER, REMOVE_LOCAL_SESSION, REMOVE_LOCAL_ACTIVITY, SET_LOCAL_DATA_COUNT, CLOUD_SAVE_REQUESTED, CLOUD_SAVE_COMPLETED, CLOUD_SAVE_FAILED, UPDATE_LOCAL_USER } from "../redux/constants/actionTypes";
import { fetchMembersByOrg } from '../redux/actions/org_member';
import { fetchActiveUsers, updateLocalActiveUserID } from '../redux/actions/active_user';
import { fetchSessionsByOrg } from '../redux/actions/session';
import { fetchActivities } from '../redux/actions/activity';
import { fetchUsersByTeam } from '../redux/actions/user';
import { fetchSystemState } from '../redux/actions/system';
import { getLocalDataTotal, getCurrentOrg } from '../redux/reducers';

export const localStorage = {
    setItem: (key,item,id=false) => {
        if(id) return global.storage.save({
            key,
            id,
            data: item
        });
        else return global.storage.save({
            key,
            data: item
        });
    },
    getItem: async (key,id=false) => {
        //alert(key);
        if(id) return await global.storage.load({
            key,
            id,
            autoSync: false,
        })
        else return await global.storage.load({
            key,
            autoSync: false,
        })
    },
    getAllDataForKey: async (key) => {
        return await global.storage.getAllDataForKey(key)
    },
    getIdsForKey: async (key) => {
        return await global.storage.getIdsForKey(key)
    },
    removeItem: (key,id=false) => {
        if(id) global.storage.remove({key,id});
        else global.storage.remove({key});
    },
    removeAllDataForKey: (key) => {
        global.storage.clearMapForKey(key);
    },
};

export function getSyncID() {
    const time = moment(new Date()).format('X');
    const timestr = String(time).substr(4,7);
    const rand = String(_.random(100,999));
    return parseInt(timestr+rand);
}

export const fetchLocalDataCount = () => (dispatch,getState) => {
    let countUsers = 0;
    let countSessions = 0;
    let countActivities = 0;
    localStorage.getAllDataForKey('xps:orgMembers').then(orgMembers => {
        countUsers += _.size(orgMembers);
        localStorage.getAllDataForKey('xps:sessions').then(sessions => {
            countSessions += _.size(sessions);
            localStorage.getAllDataForKey('xps:activities').then(activities => {
                //_.each(activities,(activity) => {
                //if(!activity.id) alert(JSON.stringify(activity));
                //});
                countActivities += _.size(activities);
                dispatch({
                    type: SET_LOCAL_DATA_COUNT,
                    counts: {
                        countUsers,
                        countSessions,
                        countActivities
                    }
                });
            }).catch(() => {
                dispatch({
                    type: SET_LOCAL_DATA_COUNT,
                    counts: {
                        countUsers,
                        countSessions,
                        countActivities
                    }
                });
            });
        }).catch(() => {
            localStorage.getAllDataForKey('xps:activities').then(activities => {
                countActivities += _.size(activities);
                dispatch({
                    type: SET_LOCAL_DATA_COUNT,
                    counts: {
                        countUsers,
                        countSessions,
                        countActivities
                    }
                });
            }).catch(() => {
                dispatch({
                    type: SET_LOCAL_DATA_COUNT,
                    counts: {
                        countUsers,
                        countSessions,
                        countActivities
                    }
                });
            });
        });
    }).catch(() => {
        localStorage.getAllDataForKey('xps:sessions').then(sessions => {
            countSessions += _.size(sessions);
            localStorage.getAllDataForKey('xps:activities').then(activities => {
                countActivities += _.size(activities);
                dispatch({
                    type: SET_LOCAL_DATA_COUNT,
                    counts: {
                        countUsers,
                        countSessions,
                        countActivities
                    }
                });
            }).catch(() => {
                dispatch({
                    type: SET_LOCAL_DATA_COUNT,
                    counts: {
                        countUsers,
                        countSessions,
                        countActivities
                    }
                });
            });
        }).catch(() => {
            localStorage.getAllDataForKey('xps:activities').then(activities => {
                countActivities += _.size(activities);
                dispatch({
                    type: SET_LOCAL_DATA_COUNT,
                    counts: {
                        countUsers,
                        countSessions,
                        countActivities
                    }
                });
            }).catch(() => {
                dispatch({
                    type: SET_LOCAL_DATA_COUNT,
                    counts: {
                        countUsers,
                        countSessions,
                        countActivities
                    }
                });
            });
        });
    });
}

export const setupLocalSync = (dispatch) => {
    /*
      //for testing - can remove once ready
      const users = [{user:{first_name:"Test2",last_name:"test",org:{id:1},full_name:"Test2 test",image_url:"",thumbnail_url:"",is_staff:false,id:1553187414},org:1,teams:[1],is_admin:false,id:1553187414,localID:"06dd492d5ab2167be8b15f4fdf573dbe"},{user:{first_name:"Test3",last_name:"test",org:{id:1},full_name:"Test3 test",image_url:"",thumbnail_url:"",is_staff:false,id:1553187707},org:1,teams:[1],is_admin:false,id:1553187707,localID:"eed791d755e076fadde9768a0f967c68"},{user:{first_name:"Test4",last_name:"test",org:{id:1},full_name:"Test4 test",image_url:"",thumbnail_url:"",is_staff:false,id:1553187764},org:1,teams:[1],is_admin:false,id:1553187764,localID:"0c61bec09d87808e2cb1f3d1f0b12240"}];
      const session = {id:2019325,name:"March 25, 2019",org:1,localID:"1b66270b809aa6b105fc4de217c68dac"};

      //localStorage.removeItem('xps:orgMembers','41965a3dc1225eb17f861df9f215e162');
      //localStorage.removeItem('xps:sessions','1b66270b809aa6b105fc4de217c68dac);
      //localStorage.removeItem('xps:activities','6f5e0582e611d59a4661ad2b9cdd5629');
      _.each(users,(user) => {
          localStorage.setItem('xps:orgMembers',user,user.localID);
      });
      localStorage.setItem('xps:sessions',user,user.localID);
      localStorage.setItem('xps:activities',user,user.localID);

    const user = {user:{first_name:"Test",last_name:"test",org:{id:1},full_name:"Test test",image_url:"",thumbnail_url:"",is_staff:false,id:1553187414},org:1,teams:[1],is_admin:false,id:1553187414,localID:"06dd492d5ab2167be8b15f4fdf573dbe"};
    const user2 = {user:{first_name:"Test2",last_name:"test",org:{id:1},full_name:"Test2 test",image_url:"",thumbnail_url:"",is_staff:false,id:1553187414},org:1,teams:[1],is_admin:false,id:1553187707,localID:"1b66270b809aa6b105fc4de217c68dac",syncing:true};

    localStorage.setItem('xps:membertest',user,user.localID);
    localStorage.setItem('xps:membertest',user2,user2.localID);
    localStorage.getNonSyncingItems('xps:membertest',user.localID).then((data) => {
        console.log(data);
    });
    localStorage.getNonSyncingItems('xps:membertest',user2.localID).then((data) => {
        console.log(data);
    });
       */

    //localStorage.removeItem('xps:activities','d17e06cc23c6e5d765e18c5c441651b7');
    //first time here - reset all local syncing flags
    localStorage.getAllDataForKey('xps:orgMembers').then(orgMembers => {
        _.each(orgMembers,(orgMember) => {
            orgMember.syncing = false;
            localStorage.setItem('xps:orgMembers', orgMember, orgMember.localID);
        });
        localStorage.getAllDataForKey('xps:sessions').then(sessions => {
            _.each(sessions,(session) => {
                session.syncing = false;
                localStorage.setItem('xps:sessions', session, session.localID);
            });
            localStorage.getAllDataForKey('xps:activities').then(activities => {
                _.each(activities,(activity) => {
                    activity.syncing = false;
                    localStorage.setItem('xps:activities', activity, activity.localID);
                });
            });
        });
    });

    setInterval(() => {
        const systemState = dispatch(fetchSystemState());
        if(!!systemState && systemState !== 'connected' && !!global.storage && !global.debugMode) processLocalOrgMembers(dispatch);
    },5000);
}

export const doLocalSync = () => (dispatch, getState) => {
    dispatch({
        type: CLOUD_SAVE_REQUESTED
    });
    localStorage.getAllDataForKey('xps:orgMembers').then(orgMembers => {
        _.each(orgMembers,(orgMember) => {
            orgMember.syncing = false;
            localStorage.setItem('xps:orgMembers', orgMember, orgMember.localID);
        });
        localStorage.getAllDataForKey('xps:sessions').then(sessions => {
            _.each(sessions,(session) => {
                session.syncing = false;
                localStorage.setItem('xps:sessions', session, session.localID);
            });
            localStorage.getAllDataForKey('xps:activities').then(activities => {
                _.each(activities,(activity) => {
                    activity.syncing = false;
                    localStorage.setItem('xps:activities', activity, activity.localID);
                });
            });
        });
    });

    const _state = getState();
    if(!!global.storage) {
        processLocalOrgMembers(dispatch,_state);
    }
}

function processLocalOrgMembers(dispatch,state) {
    localStorage.getAllDataForKey('xps:orgMembers').then(orgMembers => {
        if(orgMembers.length===0) processLocalSessions(dispatch,state);
        else {
            //console.log(orgMembers);
            //found local users to sync
            _.each(orgMembers, (orgMember) => {
                if(!orgMember.syncing) {
                    orgMember.syncing = true;
                    localStorage.setItem('xps:orgMembers', orgMember, orgMember.localID);
                    const _localOrgUserId = orgMember.user.id;
                    orgMemberApi.saveOrgMember(orgMember).then(
                        response => {
                            localStorage.removeItem('xps:orgMembers',orgMember.localID);
                            dispatch({
                                type: REMOVE_LOCAL_ORG_MEMBER,
                                tempID: _localOrgUserId,
                                localID: orgMember.localID
                            });
                            orgMember.id = response.data.id;
                            orgMember.user.id = response.data.user.id;
                            dispatch({
                                type: UPDATE_LOCAL_USER,
                                orgMember,
                                tempID: _localOrgUserId,
                                localID: orgMember.localID

                            });
                            dispatch(updateLocalActiveUserID(orgMember.user.id,_localOrgUserId));
                            localStorage.getAllDataForKey('xps:activities').then(_a => {
                                _.each(_a,(a) => {
                                    if(a.user===_localOrgUserId) a.user = orgMember.user.id;
                                })
                            });
                            dispatch({
                                type: SAVE_ORG_MEMBER_SUCCESS,
                                orgMember: response.data,
                                response: normalize(response.data, schema.orgMemberSchema)
                            });
                            if(orgMember.teams && orgMember.teams[0]) dispatch(fetchUsersByTeam(orgMember.teams[0]));
                            dispatch(fetchActiveUsers());
                            dispatch(fetchMembersByOrg(orgMember.org));
                            dispatch(fetchLocalDataCount());
                            //dispatch({ type: CLOUD_SAVE_COMPLETED });
                            localStorage.getAllDataForKey('xps:orgMembers').then(_o => {
                                if(_.size(_o)===0) processLocalSessions(dispatch,state);
                            });
                        },
                        error => {
                            orgMember.syncing = false;
                            localStorage.setItem('xps:orgMembers', orgMember, orgMember.localID);
                            dispatch({
                                type: SAVE_ORG_MEMBER_FAILURE,
                                orgMember,
                                errorMessage: 'Something went wrong',
                            });
                            dispatch({ type: CLOUD_SAVE_FAILED });
                        }
                    )
                }
            });
        }
    }).catch((e) => {
        //no local users to sync, going on to sessions
        processLocalSessions(dispatch,state);
    });
}

function processLocalSessions(dispatch,state) {
    localStorage.getAllDataForKey('xps:sessions').then((sessions) => {
        if(sessions.length===0) processLocalActivities(dispatch,state);
        else {
            //found local sessions to sync
            _.each(sessions, (session) => {
                //console.log(JSON.stringify(session));
                if(!session.syncing) {
                    session.syncing = true;
                    localStorage.setItem('xps:sessions', session, session.localID);
                    sessionApi.fetchSessionsByOrg(session.org).then((cloudSessions) => {
                        if(cloudSessions.status && checkStatusCode(cloudSessions)) {
                            const _session = _.find(cloudSessions.data.results, (s) => s.name === session.name);
                            console.log(JSON.stringify(_session));
                            if (!_session) {
                                return sessionApi.saveSession(session).then(
                                    response => {
                                        console.log(response);
                                        if(response.status && checkStatusCode(response)) {
                                            localStorage.removeItem('xps:sessions', session.localID);
                                            dispatch({
                                                type: REMOVE_LOCAL_SESSION,
                                                tempID: session.id,
                                                localID: session.localID

                                            });
                                            dispatch({
                                                type: SAVE_SESSION_SUCCESS,
                                                session: response.data,
                                                response: normalize(response.data, schema.sessionSchema)
                                            });
                                            localStorage.getAllDataForKey('xps:activities').then((activities) => {
                                                _.each(activities, (activity) => {
                                                    if (activity.session === session.id) activity.session = response.data.id;
                                                    localStorage.setItem('xps:activities', activity, activity.localID);
                                                    console.log('changed activity session to ' + response.data.id);
                                                });
                                                dispatch(fetchLocalDataCount());
                                                //dispatch({ type: CLOUD_SAVE_COMPLETED });
                                                localStorage.getAllDataForKey('xps:sessions').then(_s => {
                                                    if (_.size(_s) === 0) processLocalActivities(dispatch, state);
                                                });
                                            })
                                        } else {
                                            sessionSaveError(session,dispatch);
                                        }
                                    },
                                    error => {
                                        sessionSaveError(session,dispatch);
                                    }
                                );
                            } else {
                                localStorage.removeItem('xps:sessions', session.localID);
                                localStorage.getAllDataForKey('xps:activities').then((activities) => {
                                    _.each(activities, (activity) => {
                                        if (activity.session === session.id) activity.session = _session.id;
                                        localStorage.setItem('xps:activities', activity, activity.localID);
                                        console.log('changed activity session to ' + _session.id);
                                    });
                                });
                                localStorage.getAllDataForKey('xps:sessions').then(_s => {
                                    if (_.size(_s) === 0) processLocalActivities(dispatch, state);
                                });
                                //dispatch({ type: CLOUD_SAVE_COMPLETED });
                            }
                        } else {
                            sessionSaveError(session,dispatch);
                        }
                    }).catch((error) => {
                        console.log(error);
                        console.log(JSON.stringify(session));
                        /*return sessionApi.saveSession(session).then(
                            response => {
                                localStorage.removeItem('xps:sessions', session.localID);
                                dispatch({
                                    type: REMOVE_LOCAL_SESSION,
                                    tempID: session.id
                                });
                                dispatch({
                                    type: SAVE_SESSION_SUCCESS,
                                    session: response.data,
                                    response: normalize(response.data, schema.sessionSchema)
                                });
                                localStorage.getAllDataForKey('xps:activities').then((activities) => {
                                    _.each(activities, (activity) => {
                                        if (activity.session === session.id) activity.session = response.data.id;
                                        localStorage.setItem('xps:activities', activity, activity.localID);
                                        console.log('changed activity session to ' + response.data.id);
                                    });
                                });
                                dispatch(fetchLocalDataCount());
                                //dispatch({ type: CLOUD_SAVE_COMPLETED });
                                localStorage.getAllDataForKey('xps:sessions').then(_s => {
                                    if(_.size(_s)===0) processLocalActivities(dispatch,state);
                                });
                            },
                            error => {
                            }
                        );*/
                        sessionSaveError(session,dispatch);
                    });
                }
            });
        }
    }).catch((e) => {
        //no local sessions to sync, going on to activities
        processLocalActivities(dispatch,state);
    });
}

function processLocalActivities(dispatch,state) {
    localStorage.getAllDataForKey('xps:activities').then((activities) => {
        //found local activities to sync
        if(activities.length===0) {
            cloudSyncCompleted(dispatch,state);
        }
        const sortedActivities = _.orderBy(activities,['id'],['asc']);
        _.each(sortedActivities,(activity) => {
            //console.log(JSON.stringify(activity));
            if(!activity.syncing && !!activity.id) {
                activity.syncing = true;
                if(activity.type_definition.activity_type!=='jump' && !activity.data_summary.sprint_time) activity.data_summary.sprint_time = 0;
                if(activity.type_definition.activity_type==='jump' && !activity.data_summary.height) activity.data_summary.height = 0;
                localStorage.setItem('xps:activities', activity, activity.localID);
                //console.log(JSON.stringify(activity));
                //console.log(activity.session);
                sessionApi.fetchSessionById(activity.session).then((session) => {
                    //console.log(session);
                    if(session.status && checkStatusCode(session)) {
                        return activityApi.saveActivity(activity).then(
                            response => {
                                if(response.status && checkStatusCode(response)) {
                                    localStorage.removeItem('xps:activities', activity.localID);
                                    dispatch({
                                        type: REMOVE_LOCAL_ACTIVITY,
                                        tempID: activity.id,
                                        localID: activity.localID
                                    });
                                    dispatch({
                                        type: SAVE_SESSION_SUCCESS,
                                        activity: response.data,
                                        response: normalize(response.data, schema.activitySchema)
                                    });
                                    cloudSyncCompleted(dispatch,state);
                                } else {
                                    activitySaveError(activity,dispatch);
                                }
                            },
                            error => {
                                activitySaveError(activity,dispatch);
                            }
                        );
                    } else {
                        activitySaveError(activity,dispatch);
                    }
                }).catch((error) => {
                    console.log(error);
                    const _date = moment(activity.session,'YYYYMMDD').format('MMMM DD, YYYY');
                    const org = getCurrentOrg(state);
                    sessionApi.fetchSessionsByOrg(org.id).then((sessions) => {
                        console.log(sessions);
                        const session = _.find(sessions.data.results, (s) => s.name === _date);
                        //console.log(session)
                        if(session) {
                            activity.session = session.id;
                            return activityApi.saveActivity(activity).then(
                                response => {
                                    if(response.status && checkStatusCode(response)) {
                                        localStorage.removeItem('xps:activities', activity.localID);
                                        dispatch({
                                            type: REMOVE_LOCAL_ACTIVITY,
                                            tempID: activity.id,
                                            localID: activity.localID
                                        });
                                        dispatch({
                                            type: SAVE_SESSION_SUCCESS,
                                            activity: response.data,
                                            response: normalize(response.data, schema.activitySchema)
                                        });
                                        cloudSyncCompleted(dispatch,state);
                                    } else {
                                        activitySaveError(activity,dispatch);
                                    }
                                },
                                error => {
                                    activitySaveError(activity,dispatch);
                                }
                            );
                        } else {
                            activitySaveError(activity,dispatch);
                        }
                    }).catch((error) => {
                        activitySaveError(activity,dispatch);
                    });
                });
            }
        });
    }).catch((e) => {
        cloudSyncCompleted(dispatch,state);
    });
}

function checkStatusCode(response) {
    switch(parseInt(response.status)) {
        case 200:
        case 201:
        case 202:
            return true;
        default:
            return false;
    }
}

function cloudSyncCompleted(dispatch,state) {
    const currentOrg = getCurrentOrg(state);
    dispatch(fetchLocalDataCount());
    dispatch(fetchSessionsByOrg(currentOrg.id));
    dispatch(fetchActivities());
    dispatch({ type: CLOUD_SAVE_COMPLETED });

}

function sessionSaveError(session,dispatch) {
    session.syncing = false;
    localStorage.setItem('xps:sessions', session, session.localID);
    dispatch({type: CLOUD_SAVE_FAILED});
}

function activitySaveError(activity,dispatch) {
    activity.syncing = false;
    localStorage.setItem('xps:activities', activity, activity.localID);
    dispatch({ type: CLOUD_SAVE_FAILED });
}
