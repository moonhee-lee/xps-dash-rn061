import {
    SYSTEM_SPRINT_MOTION_DISTANCE,
    SYSTEM_SPRINT_ENDED,
    SYSTEM_JUMP_DETECTED,
    SYSTEM_COD_DETECTED
} from '../redux/constants/actionTypes';

export const processEstMessages = (messages, tag, dispatch) => {
    // we tell the C++ algorithm to process the data via the react native bridge
    // and we wait for the callback to return the results.
    let eventData;
    // set third param to sprintDistance for sprints
    if (tag.activityType !== 'jump') eventData = tag.sprintDistance ? tag.sprintDistance : 0.0;
    // set third param to jump type for jumps (0=horizontal, 1=vertical)
    if (tag.activityType === 'jump') eventData = tag.orientationType ? tag.orientationType : 0;

    if (tag.activityType !== 'agility') {
        tag.motionAlgo.processData(tag.name, messages, eventData).then(
            response => {
                const {algoState} = response;
                if (tag.activityType === 'sprint') {
                    if (algoState === 2) {
                        const {sprintData, splitData, strideDataMetres} = response;
                        console.log(response);
                        //console.log(JSON.stringify(response));

                        console.log("Sprint has been ended")
                        // delay to dispatch the action after detected to extend recording time one more second
                        setTimeout(() => {
                            console.log("Dispatch action for 'SYSTEM_SPRINT_ENDED'")
                            dispatch({
                                type: SYSTEM_SPRINT_ENDED,
                                activityUserId: tag.activityUserId,
                                activeUserId: tag.activityUserId,
                                sprintData,
                                splitData,
                                strideDataMetres
                            });

                            // send back sprint start for validation
                            const smsg = {time: sprintData.start, event_id: 'SprintStart'};
                            global.client.sendMessage(smsg,'io_event');
                            // send back sprint end for validation
                            const msg = {time: sprintData.end, event_id: 'SprintEnd'};
                            global.client.sendMessage(msg,'io_event');
                            // send back sprint 1m split time for validation
                            const msg2 = {time: splitData.times[0], event_id: 'Sprint1m'};
                            global.client.sendMessage(msg2,'io_event');
                            // send back sprint 5m split time for validation
                            const msg3 = {time: splitData.times[4], event_id: 'Sprint5m'};
                            global.client.sendMessage(msg3,'io_event');
                        }, 1000)
                    } // else we just ignore these...
                } else if (tag.activityType === 'jump') {
                    // for jumps, we only care about state 1, meaning a full jump has been detected
                    if (algoState === 1) {
                        const {jumpData} = response;
                        console.log(response);
                        console.log("Jump has been detected")
                        // delay to dispatch the action after detected to extend recording time one more second
                        setTimeout(() => {
                            console.log("Dispatch action for 'SYSTEM_JUMP_DETECTED'")
                            dispatch({
                                type: SYSTEM_JUMP_DETECTED,
                                tagName: tag.name,
                                activityUserId: tag.activityUserId,
                                activeUserId: tag.activityUserId,
                                jumpData
                            });
                        }, 1000)
                    }
                    // otherwise we ignore.
                }
            },
            error => {
                console.error(error);
            }
        )
    }

    if (tag.activityType === 'agility') {
        tag.motionAlgo.processData(tag.name, messages, eventData).then(
            response => {
                const {algoState} = response;
                if (algoState === 2) {
                    console.log(response);
                    const {sprintData, splitData, strideDataMetres} = response;
                    dispatch({
                        type: SYSTEM_SPRINT_ENDED,
                        activityUserId: tag.activityUserId,
                        activeUserId: tag.activityUserId,
                        sprintData,
                        splitData,
                        strideDataMetres
                    });
                    // send back sprint start for validation
                    const smsg = {time: sprintData.start, event_id: 'SprintStart'};
                    global.client.sendMessage(smsg,'io_event');
                    // send back sprint end for validation
                    const msg = {time: sprintData.end, event_id: 'SprintEnd'};
                    global.client.sendMessage(msg,'io_event');
                    // send back sprint 1m split time for validation
                    const msg2 = {time: splitData.times[0], event_id: 'Sprint1m'};
                    global.client.sendMessage(msg2,'io_event');
                    // send back sprint 5m split time for validation
                    const msg3 = {time: splitData.times[4], event_id: 'Sprint5m'};
                    global.client.sendMessage(msg3,'io_event');
                }
            },
            error => {
                console.error(error);
            }
        )

        tag.CODAlgo.processData(tag.name, messages).then(
            response => {
                const { algoState } = response;
                if (algoState === 2) {
                    const { CODData } = response;
                    console.log(CODData);
                    dispatch({
                        type: SYSTEM_COD_DETECTED,
                        tagName: tag.name,
                        activityUserId: tag.activityUserId,
                        activeUserId: tag.activityUserId,
                        CODData
                    });
                }
            },
            error => {
                console.error(error);
            }
        )
    }
};

