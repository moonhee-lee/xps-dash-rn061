import _ from 'lodash';
import * as graphUtils from './graph_utils';
import {Dimensions} from "react-native";
import moment from "moment";
const { width,height } = Dimensions.get('window');

export function isNumberOr(value, defaultValue) {
    return !isNaN(value) ? value : defaultValue;
}

export function formatDateTime(timestamp) {
    return moment(timestamp).format('MM/DD/YY h:mma')
}

export function getDistance(position) {
    if (position) {
        return Math.sqrt(position.x * position.x + position.y * position.y + position.z * position.z);
    }
    return null;
}

export function formatDistance(dist) {
    if(dist>=1) {
        return isNumberOr(parseFloat(Math.abs(dist)), 0).toFixed(2)+'m';
    } else {
        return isNumberOr(parseFloat(Math.abs(dist*100)), 0).toFixed(0)+'cm';
    }
}

export function formatPosition(position) {
    return (position) ? '(' + formatNumber(position.x, 1)+',' +formatNumber(position.y, 1)+','+formatNumber(position.z, 1) + ')' : '';
}

export function median(numbers) {
    let middle = (numbers.length + 1) / 2;
    let sorted = _.sortBy(numbers);
    return (sorted.length % 2) ? sorted[middle - 1] : (sorted[middle - 1.5] + sorted[middle - 0.5]) / 2;
}

export function formatDistanceImperial(dist, return_inches=false) {
    var inches = (dist*100*0.393700787).toFixed(0);
    if (return_inches) return inches + '"'; // vertical jump convention is just inches
    var feet = Math.floor(inches / 12);
    var output = '';
    inches %= 12;
    if (feet) { output += feet + '\'' }
    if (inches) { output += inches + '"'}
    return output
}

export function formatNumber(num, precision=2) {
    return isNumberOr(parseFloat(num), 0).toFixed(precision)
}

export function formatSpeed(dist,precision=2) {
    return isNumberOr(parseFloat(Math.abs(dist)), 0).toFixed(precision)+'m/s';
}

export function formatAcceleration(dist,precision=2) {
    return isNumberOr(parseFloat(Math.abs(dist)), 0).toFixed(precision)+'m/s²';
}

export function formatTime(time,precision=2) {
    return isNumberOr(parseFloat(Math.abs(time)), 0).toFixed(precision)+'s';
}

export function formatAngle(angle,precision=0) {
    return isNumberOr(parseFloat(Math.abs(angle)), 0).toFixed(precision)+'°';
}

export function formatAnglePerSecond(angle,precision=0) {
    return isNumberOr(parseFloat(Math.abs(angle)), 0).toFixed(precision)+'°/s';
}

export function generateSplitListData(data_summary,events) {
    //const times = getSplitTimes(data_summary,events);
    //const accelerations = getSplitAccelerations(data_summary,events);
    //const velocities = getSplitVelocities(data_summary,events);

    let times = [];
    let totalTimes = [];
    let accelerations = [];
    let velocities = [];

    const splits = _.filter(events, function(e) { return e.type=='split'; });
    if (splits) {
        _.each(splits,(split, index) => {
            if(split.type==='split') {
                // time since start
                const totalTime = parseFloat(split.time/1000) - parseFloat(data_summary.start/1000);
                // duration of this split

                const time = parseFloat(split.time) - (!!index ? parseFloat(splits[index-1].time) : parseFloat(data_summary.start));


                if(totalTime) totalTimes.push(totalTime.toFixed(2));
                if(time) times.push((time/1000).toFixed(2));
                if(split.acceleration) accelerations.push(split.acceleration.toFixed(2));
                if(split.velocity) velocities.push(split.velocity.toFixed(2));
            }
        });
    }

    return {accelerations,times,velocities, totalTimes}
}

export function generateStrideListData(data_summary,events) {
    //const times = getSplitTimes(data_summary,events);
    //const accelerations = getSplitAccelerations(data_summary,events);
    //const velocities = getSplitVelocities(data_summary,events);

    let times = [];
    let frequencies = [];
    let lengths = [];
    let sides = [];
    _.each(events,(event) => {
        if(event.type==='stride') {
            let time = parseFloat(event.contact_time);
            times.push(time.toFixed(2));
            frequencies.push(event.frequency.toFixed(2));
            lengths.push(event.length.toFixed(2));
            sides.push(event.side);
        }
    });

    return {frequencies,times,lengths,sides}
}

export function generateCoDs(data_summary,events=null) {
    const list = [];

    let lastEvent = {end:data_summary.start}, segmentTime, index = 0;
    if(!events) {
        const cods = data_summary.changes_of_direction;
        _.each(cods,(cod) => {
            if(cod.start > data_summary.start) {
                const time = (cod.end - cod.start) / 1000;
                const degChange = cod.newHeadingDeg - cod.oldHeadingDeg;
                const degPerSecond = cod.averagedOmega?cod.averagedOmega:cod.maxOmegaZDegPS;
                segmentTime = (cod.start - lastEvent.end) / 1000;
                list.push({segment: ['Segment ' + (index + 1), formatTime(segmentTime)]});
                list.push({row: [index + 1, formatAngle(degChange), formatAnglePerSecond(degPerSecond), formatTime(time)]});
                lastEvent = cod;
                index++;
            }
        });
        if(lastEvent) {
            index++;
            segmentTime = (data_summary.end - lastEvent.end) / 1000;
            list.push({segment: ['Segment ' + index, formatTime(segmentTime)]});
        }
    } else {
        const cods = _.filter(events,(e) => e.type==='cod');
        _.each(cods,(event) => {
            if(event.type==='cod') {
                if(event.start > data_summary.start) {
                    const time = (event.end - event.start) / 1000;
                    const degChange = event.newHeadingDeg - event.oldHeadingDeg;
                    const degPerSecond = event.averagedOmega?event.averagedOmega:event.maxOmegaZDegPS;
                    segmentTime = (event.start - lastEvent.end) / 1000;
                    list.push({segment: ['Segment ' + (index + 1), formatTime(segmentTime)]});
                    list.push({row: [index + 1, formatAngle(degChange), formatAnglePerSecond(degPerSecond), formatTime(time)]});
                    lastEvent = event;
                    index++;
                }
            }
        });
        if(lastEvent) {
            index++;
            segmentTime = (data_summary.end - lastEvent.end) / 1000;
            list.push({segment: ['Segment ' + index, formatTime(segmentTime)]});
        }
    }
    return list;
}

export function generateCoDDistances(data_summary,events) {
    const distances = [];
    const start = _.find(events,(e) => e.type==='motion_start');
    const cods = _.filter(events,(e) => e.type==='cod');
    _.each(cods,(event) => {
        if(event.type==='cod') {
            if(event.start>data_summary.start) {
                if (event.apexXY) {
                    distances.push([-100, event.apexXY.y - start.pos.y]);
                } else {
                    const _start = event.startPosAndTime.y - start.pos.y;
                    const _end = event.endPosAndTime.y - start.pos.y;
                    console.log(_start + ' - ' + _end);
                    distances.push([_start, _end]);
                }
            }
        }
    });

    return distances;
}

export function loadListData(activeUser, liveActivity) {
    const data_summary = activeUser.meta_data;

    const activity_type = liveActivity.activity_type;

    let listData = [];
    let listPage = [];
    let splitListPage = [];

    if(activity_type && activity_type === 'jump') {
        listPage.push({label:'Jump Height',value:formatDistance(data_summary.height)});
        listPage.push({label:'Jump Distance',value:formatDistance(data_summary.length)});
        listPage.push({label:'Air time',value:formatTime(data_summary.air_time/1000)});
        listPage.push({label:'Takeoff Acceleration',value:formatAcceleration(data_summary.takeoffAcceleration)});
        listPage.push({label:'Takeoff Angle',value:formatAngle(data_summary.takeoffAngle)});
        //listPage.push({label:'Takeoff Dip',value:formatDistance(data_summary.dip1)});
        //listPage.push({label:'Landing Dip',value:formatDistance(data_summary.dip2)});
        listData.push(listPage);
    }

    if(activity_type && activity_type === 'sprint') {
        listPage = [];

        const splitData = graphUtils.generateSplitListData(data_summary);
        splitListPage = [];
        if(splitData) {
            splitListPage.push(splitData);
        }

        let max_vel = 0, max_split = null;
        if(splitData.accelerations && splitData.accelerations.length) {
            let max_vel = 0, max_split = null, vel=0;
            _.each(splitData.accelerations,(split,index) => {
                if(index%5===4 || index+1===splitData.accelerations.length) {
                    vel += parseFloat(splitData.velocities[index]);
                    if(vel/5>max_vel) {
                        max_vel = vel/5;
                        max_split = (index-4)+'m - '+(index+1)+'m';
                    }
                    vel = 0;
                } else {
                    vel += parseFloat(splitData.velocities[index]);
                }
            });
            listPage.push({label:'Top Speed',value:formatSpeed(max_vel)});
            listPage.push({label:'Fastest 5m',value:max_split});
        } else {
            listPage.push({label:'Top Speed',value:formatSpeed(data_summary.max_velocity)});
        }

        listPage.push({label:'Average Speed',value:formatSpeed(liveActivity.distance/data_summary.sprint_time)});
        if(data_summary.reaction_time) listPage.push({label:'Reaction Time',value:formatTime(data_summary.reaction_time)});
        listPage.push({label:'Sprint Time',value:formatTime(data_summary.sprint_time)});
        if(data_summary.max_acceleration) listPage.push({label:'Max Acceleration',value:formatAcceleration(data_summary.max_acceleration)});

        listData.push(listPage);
        listData.push(splitListPage);

        const strideData = graphUtils.generateStrideListData(data_summary);
        listPage = [];
        if(strideData) {
            listPage.push(strideData);
        }
        listData.push(listPage);
    }


    if(activity_type && activity_type === 'agility') {
        listPage = [];
        listPage.push({label:'Top speed',value:formatSpeed(data_summary.max_velocity)});
        if(data_summary.reaction_time) listPage.push({label:'Reaction time',value:formatTime(data_summary.reaction_time)});
        if(data_summary.max_acceleration) listPage.push({label:'Peak accel',value:formatAcceleration(data_summary.max_acceleration)});
        listData.push(listPage);

        const CoDs = generateCoDs(data_summary);
        listData.push(CoDs);
    }
    return listData;
}

export function loadGraphData(toGraphESTData, activeUser, liveActivity) {
    const data_summary = activeUser.meta_data;

    let graph = [], cods;

    const graphWidth = width;
    const graphHeight = height;

    const activity_type = liveActivity.activity_type;

    if(toGraphESTData && _.size(toGraphESTData)) {
        const graphData = graphUtils.getLineGraphData({
            data: _.values(toGraphESTData),
            meta: data_summary,
            width: graphWidth,
            height: graphHeight,
            activityType: activity_type,
        });

        if (activity_type === 'jump') {
            graph = graphData.distance;
        }
        if (activity_type !== 'jump') {
            graph = graphData.velocity;
        }
    }
    return graph;
}


export function getActivityTitle(activity) {
    let title;
    let unit = getUnit(activity.type_definition.units);
    if(activity.type_definition.activity_type==='freeform') {
        title = 'Free Form';
    } else if(activity.type_definition.activity_type==='sprint') {
        title = _.startCase(activity.type_definition.start_type);
        if (activity.type_definition.skating) {
            title += ' ' + _.startCase(activity.type_definition.skating) + ' Skate';
        } else if (activity.type_definition.walking === true) {
            title += ' Walk';
        } else if (activity.type_definition.bobsled === true) {
            title += ' Bobsled';
        } else {
            title += ' Sprint';
        }
        title += ' (' + activity.type_definition.distance + unit + ')';
    } else if(activity.type_definition.activity_type==='agility') {
        title = _.startCase(activity.type_definition.start_type)+' Agility (' + activity.type_definition.distance + unit + ')';
    } else {
        title = 'Jump (';
        title += (activity.type_definition.side==='right'?'Right, ':activity.type_definition.side==='left'?'Left, ':'');
        title += (activity.type_definition.triple_jump_type==='triple_hop'?'Triple, ':activity.type_definition.triple_jump_type==='crossover_hop'?'Crossover, ':'');
        title += (activity.type_definition.orientation==='vertical'?'Vertical':'Horizontal');
        title += ')';
    }
    return title;
}

export function getUnit(units) {
    return units && units === 'yards' ? 'yd' : 'm';
}

// ---------------------------------------------
// placeholder utilities pending re-org/cleanup
// ---------------------------------------------
export function getMedian(arr) {
    const mid = Math.floor(arr.length / 2),
    nums = [...arr].sort((a, b) => a - b);
    return arr.length % 2 !== 0 ? nums[mid] : (nums[mid - 1] + nums[mid]) / 2;
}
